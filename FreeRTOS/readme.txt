FreeRTOS(TM) Package
====================


Overview
--------
The FreeRTOS(TM) package includes FreeRTOS(TM), the Market Leading, De-facto Standard and Cross Platform
Real Time Operating System(RTOS).

This package confirms to FIT specification and includes the FreeRTOS v8.2.2 Source and port for RX64M (RX600v2)
in its entirety.   

There are 3 folders that make up this package. 

The 'Source' folder contains the FreeRTOS(TM) source files with a 'portable' sub-directory containing generic
memory manager sub-folder, 'MemMang' and 'Renesas/RX600v2' port specific to Renesas Gen-2 core.

Another 'user' folder is added.  These user specified files provides specific 'Hook' functions for a particular board/MCU, including
vApplicationSetupTimerInterrupt, vAssertCalled, vApplicationIdleHook, vApplicationTickHook, vApplicationMallocFailedHoook
and vApplicationStackOverflowHook.

The 'ref' folder contains a reference configuration file, FreeRTOSConfig_reference.h.

Caveat
------
This is not an official FIT module.  It is provided to ease the introduction of FreeRTOS(TM) into FIT based projects 
requiring FreeRTOS(TM).  

Features
--------
* Provides FreeRTOS v9.0.0 source code 
* Provides sample codes for hook functions suitable for adaptation to specific needs
* Provides reference config file, FreeRTOSConfig_reference.h for further customize 
 


File Structure
--------------
FreeRTOS
+---ref
+---Source
|   +---include
|   \---portable
|       +---MemMang
|       \---Renesas
|           \---RX600v2
\---user
