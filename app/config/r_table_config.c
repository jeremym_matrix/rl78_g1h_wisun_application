/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer*
* Copyright (C) 2020 Renesas Electronics Corporation. All rights reserved.
*******************************************************************************/

/*******************************************************************************
 * File Name   : r_table_config.c
 * Version     : 1.0
 * Description : This module contains configuration defines for FAN functions
 ******************************************************************************/
#ifndef _R_TABLE_CONFIG_C
#define _R_TABLE_CONFIG_C

#include <stdint.h>
#include "r_table_config.h"
#include "r_table_size.h"
#ifndef R_HYBRID_PLC_RF
#include "lib/memb.h"
#include "net/uip-ds6-source-route.h"
#include "r_nd.h"
#include "r_nd_cache.h"
#endif /* R_HYBRID_PLC_RF */
#include "mac_neighbors.h"
#include "r_impl_utils.h"

/*** MAC Neighbor Cache ********************/
#if R_MAC_NBR_CACHE_SIZE_OFFSET < 1
#error "The number of additional entries in the MAC neighbor cache must be greater than 1."
#endif

/* global: Substance of MAC Neighbor table */
#ifdef R_HYBRID_PLC_RF
r_mac_neighbor_t g_neighbors[R_MAC_NEIGHBOR_TABLE_SIZE];
#else /* R_HYBRID_PLC_RF */
r_mac_neighbor_t g_neighbors[R_MAX_CHILD_NODES + R_MAX_PARENT_CANDIDATES + R_MAC_NBR_CACHE_SIZE_OFFSET];
#endif /* R_HYBRID_PLC_RF */

/* global: Number of entry of mac neighbor table */
const uint16_t r_mac_neighbor_table_size_glb = ARRAY_SIZE(g_neighbors);

/*******************************************/
#ifndef R_HYBRID_PLC_RF
#if R_BR_AUTHENTICATOR_ENABLED && (R_BR_PAN_SIZE > 64u)
#if defined(__CCRX__)
#pragma section B expRAM
#endif
#endif

/*** NWK Neighbor Caches ************************/
#if R_MAX_PARENT_CANDIDATES < 2
#error "At least two parent candidates must be supported to select and hold a preferred and an alternate parent."
#endif

/** The RPL neighbor cache that contains the candidate parents (size must be at least 2 to hold both parents) */
r_nd_neighbor_cache_entry_t g_nd_rpl_cache[R_MAX_PARENT_CANDIDATES];

#if R_MAX_CHILD_NODES < 1
#error "At least one child node must be supported."
#endif

/** The size of the ARO neighbor cache that contains registered IP addresses (via ARO from child nodes) */
r_nd_addr_reg_entry_t g_nd_aro_cache[R_MAX_CHILD_NODES];

#if R_BR_AUTHENTICATOR_ENABLED && (R_BR_PAN_SIZE > 64u)
#if defined(__CCRX__)
#pragma section
#endif
#endif

/** The size of the RPL neighbor cache that contains the candidate parents */
const uint16_t g_nd_rpl_cache_length = ARRAY_SIZE(g_nd_rpl_cache);

/** The size of the ARO neighbor cache that contains registered IP addresses (via ARO from child nodes) */
const uint16_t g_nd_aro_cache_length = ARRAY_SIZE(g_nd_aro_cache);

/*******************************************/

#if R_BR_AUTHENTICATOR_ENABLED
#if R_BR_AUTHENTICATOR_ENABLED && (R_BR_PAN_SIZE > 64u)
#if defined(__CCRX__)
#pragma section B expRAM
#endif
#endif

/*** Authentication supplicant table *******/
/* global: Substance of auth supplicant table */
r_auth_br_supplicant_t g_supplicants[R_AUTH_BR_SUPPLICANTS_SIZE];

#if R_BR_AUTHENTICATOR_ENABLED && (R_BR_PAN_SIZE > 64u)
#if defined(__CCRX__)
#pragma section
#endif
#endif

/* global: Number of entries of auth supplicant table */
const uint16_t r_auth_br_supplicants_size_glb = ARRAY_SIZE(g_supplicants);

/*******************************************/
#endif /* R_BR_AUTHENTICATOR_ENABLED */

#if R_BORDER_ROUTER_ENABLED

/*** Routing table *************************/
/* Each route is represented by a uip_ds6_source_route_t structure and
   memory for each route is allocated from the sourceroutememb memory
   block. These routes are maintained on the routelist. */

MEMB(sourceroutememb, uip_ds6_source_route_t, R_BR_PAN_SIZE);
/* --------------------------------------------------------------------------------------
  --> This is equivalent to the following description.
        static char    sourceroutememb_memb_count[R_BR_PAN_SIZE];
        static uip_ds6_source_route_t  sourceroutememb_memb_mem[R_BR_PAN_SIZE];
        static struct memb sourceroutememb = { sizeof(uip_ds6_source_route_t),
                                               R_BR_PAN_SIZE,
                                               sourceroutememb_memb_count,
                                               (void *)sourceroutememb_memb_mem }

    struct memb {           <-- = static struct memb sourceroutememb
      unsigned short size;  <-- = sizeof(uip_ds6_source_route_t)
      unsigned short num;   <-- = R_BR_PAN_SIZE
      char *count;          <-- = sourceroutememb_memb_count
      void *mem;            <-- = (void *)sourceroutememb_memb_mem
    };
 -------------------------------------------------------------------------------------- */

/**
/ extern the pointer of routing table struct sourceroutememb
*/
struct memb* g_sourceroutememb = &sourceroutememb;

/*******************************************/
#endif /* R_BORDER_ROUTER_ENABLED */
#endif /* R_HYBRID_PLC_RF */
#endif /* _R_TABLE_CONFIG_C */
