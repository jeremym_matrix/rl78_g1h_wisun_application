/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer*
* Copyright (C) 2020 Renesas Electronics Corporation. All rights reserved.
*******************************************************************************/

/*******************************************************************************
 * File Name   : r_table_size.h
 * Version     : 1.0
 * Description : This module contains configuration of the neighbor cache and
 *               supplicant table sizes
 ******************************************************************************/
#ifndef R_TABLE_SIZE_H
#define R_TABLE_SIZE_H

/* Maximum size of the PAN network, defining amount of supplicants and source routes supported by the Border Router */
#ifndef R_MAX_PAN_SIZE
#define R_BR_PAN_SIZE (64U)            //!< Amount of supported Supplicants and Source Routes
#else
#define R_BR_PAN_SIZE (R_MAX_PAN_SIZE) //!< Amount of supported Supplicants and Source Routes
#endif


/* Maximum number of child nodes supported by the Border Router and Router Node */
#ifndef R_MAX_CHILD_NODES
#define R_MAX_CHILD_NODES (32U)  //!< Size of the ARO neighbor cache (for child node management)
#endif

/* Maximum number of neighbors supported for RPL parent candidate selection by the Border Router and Router Node */
#ifndef R_MAX_PARENT_CANDIDATES
#define R_MAX_PARENT_CANDIDATES (32U)  //!< Size of the RPL neighbor cache (for parent candidate selection)
#endif

/* The number of additional entries in the MAC layer neighbor cache compared to the total size of the NWK neighbor
 * caches (ARO and RPL). Since all NWK neighbors are locked in the MAC neighbor cache, it must be larger than the total
 * size of the NWK caches to allow communication with "new" nodes once the NWK caches are full */
#ifndef R_MAC_NBR_CACHE_SIZE_OFFSET

/** The number of additional entries in the MAC neighbor cache compared to the total size of the ARO and RPL caches */
#define R_MAC_NBR_CACHE_SIZE_OFFSET (5U)
#endif

#endif /* R_TABLE_SIZE_H */
