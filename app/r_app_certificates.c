/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer*
* Copyright (C) 2018 Renesas Electronics Corporation. All rights reserved.
*******************************************************************************/

#include <stddef.h>
#include "r_auth_certs_config.h"
#include <r_stdint.h>
#include "r_impl_utils.h"

#if !R_DEV_DISABLE_AUTH

#if R_AUTH_RAM_CERTS
unsigned char r_auth_certs[R_AUTH_CERTS_CAPACITY] = {
#else
#if defined(__RX)
#pragma address r_auth_certs_capacity = 0xFFF00008 /* Code FLASH Address */
#elif defined(__ICCRL78__)
#pragma location=0x7F00A                           /* Code FLASH Address */
#endif /* defined(__RX) */
const uint16_t r_auth_certs_capacity = R_AUTH_CERTS_CAPACITY;

#if defined(__RX)
#pragma address r_auth_certs = 0xFFF0000A /* Code FLASH Address */
#elif defined(__ICCRL78__)
#pragma location=0x7F00C                  /* Code FLASH Address */
#endif /* defined(__RX) */
const unsigned char r_auth_certs[R_AUTH_CERTS_CAPACITY] = {
#endif
#if R_BORDER_ROUTER_ENABLED
#include "r_encoded_certs.h"  // allows this device to be used as BR and RN (device type is chosen at runtime)
// If this device is only used as BR (not RN), "r_encoded_certs_server_only.h" may be used instead to save memory
#else
#include "r_encoded_certs_client_only.h"  // Use client-only certificate file to save memory
#endif
};

#if R_AUTH_MULTIPLE_CERTS
unsigned char r_auth_certs_use_alternate = 0;

#if R_AUTH_RAM_CERTS
unsigned char r_auth_certs_alternate[R_AUTH_CERTS_CAPACITY] = {
#else
const unsigned char r_auth_certs_alternate[R_AUTH_CERTS_CAPACITY] = {
#endif
#include "r_encoded_certs_alternate.h"
};
#endif /* R_AUTH_MULTIPLE_CERTS */

int R_APP_Certs_Contains(const unsigned char* ptr)
{
    /* Prevent IAR from optimizing this variable (it MUST be present in image file to allow cert replacement) */
    UNUSED_SYM(r_auth_certs_capacity);

#if R_AUTH_MULTIPLE_CERTS
    if (r_auth_certs_use_alternate)
    {
        return ptr >= r_auth_certs_alternate && ptr < &r_auth_certs_alternate[sizeof(r_auth_certs_alternate)];
    }
    else
    {
        return ptr >= r_auth_certs && ptr < &r_auth_certs[sizeof(r_auth_certs)];
    }
#else
    return ptr >= r_auth_certs && ptr < &r_auth_certs[sizeof(r_auth_certs)];
#endif
}

int R_APP_Certs_ValidateSubjectAltNameExtension(const uint8_t* subject_alt_names, uint16_t names_length, int ownCert)
{
    if (subject_alt_names == NULL || names_length == 0)
    {
        return -1;
    }

    return 0;
}

#endif /* !R_DEV_DISABLE_AUTH */
