/**********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
 * other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
 * applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
 * EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
 * SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO
 * THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the
 * following link:
 * http://www.renesas.com/disclaimer
 *
 * Copyright (C) 2020 Renesas Electronics Corporation. All rights reserved.
 *********************************************************************************************************************/

/**
 * @file r_app_main.h
 * @brief Public API of the application layer main module
 */

#ifndef R_APP_MAIN_H
#define R_APP_MAIN_H

#include "r_modem.h"

/******************************************************************************
   Macro definitions
******************************************************************************/
#ifdef __RX
#define __far  _far
#define __near _near
#elif defined(__i386) || defined(__x86_64)
#define __far
#define __near
#endif

#define APP_CMD_BUFSIZE (R_MODEM_HEADER_SIZE + ((1576 * 2) + 1) + R_MODEM_FOOTER_SIZE)

/******************************************************************************
   Exported global variables (to be accessed by other files)
******************************************************************************/
extern unsigned char AppCmdBuf[APP_CMD_BUFSIZE];
extern unsigned short AppCmdSize;

/******************************************************************************
   Exported global functions (to be accessed by other files)
******************************************************************************/
void AppCmd_ProcessCmd(unsigned char* pCmd, uint16_t size);
char AppStricmp(void __far* pStr1, void** ppStr2);
void AppCmd_SkipWhiteSpace(unsigned char** ppBuf);

#endif /* R_APP_MAIN_H */
