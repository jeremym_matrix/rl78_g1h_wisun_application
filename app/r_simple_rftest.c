/*******************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only
 * intended for use with Renesas products. No other uses are authorized.
 * This software is owned by Renesas Electronics Corporation and is protected under
 * all applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES
 * REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY,
 * INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY
 * DISCLAIMED.
 * TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
 * ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
 * FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES
 * FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS
 * AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this
 * software and to discontinue the availability of this software.
 * By using this software, you agree to the additional terms and
 * conditions found by accessing the following link:
 * http://www.renesas.com/disclaimer
 *******************************************************************************/
/*******************************************************************************
 * file name	: r_simple_rftest.c
 * version		: V.1.02
 * description	: Inspection program at the time of shipment of user product.
 *******************************************************************************
 * Copyright (C) 2014-2021 Renesas Electronics Corporation.
 ******************************************************************************/
#ifdef R_SIMPLE_RFTEST_ENABLED
/***********************************************************************
 * includes
 **********************************************************************/
#include "r_simple_rftest.h"

#include <stdio.h>
#include <string.h>	// for memcpy,memset,memcmp
//
#include "phy.h"
#include "phy_def.h"
//
#include "r_app_main.h"
#include "r_nwk_api.h"
#include "hardware.h"

/***********************************************************************
 * macro definitions
 **********************************************************************/
/* log output */ 
// #define R_RFDRVIF_LOG_REASON

/* for test */
#define R_TEST_DATA_LENGTH		(10)	/* test data length */

/* command execution processing */
#define PRINTF	printf

/* status */ 
#define R_STATUS_SUCCESS					(0x00)
/* --- rfdriver --- */
#define	R_STATUS_ILLEGAL_REQUEST			(0x10)
#define R_STATUS_INVALID_PARAMETER			(0x11)
#define R_STATUS_BUSY_LOWPOWER				(0x12)
#define R_STATUS_BUSY_RX					(0x13)
#define R_STATUS_BUSY_TX					(0x14)
#define R_STATUS_TRX_OFF					(0x15)
#define R_STATUS_INVALID_API_OPTION			(0x16)
#define R_STATUS_UNSUPPORTED_ATTRIBUTE		(0x17)
#define R_STATUS_NO_ACK						(0x18)
#define R_STATUS_CHANNEL_BUSY				(0x19)
#define R_STATUS_TRANSMIT_TIME_OVERFLOW		(0x1A)
#define R_STATUS_TX_UNDERFLOW				(0x1B)
#define R_STATUS_ERROR						(0x1C)
/* --- serial command --- */
#define R_STATUS_UNSUPPORTED_COMMAND		(0x20)
#define R_STATUS_INVALID_COMMAND			(0x21)
#define R_STATUS_INVALID_COMMAND_PARAMETER	(0x22)
#define	R_STATUS_UART_RXERROR				(0x23)

/* rf state */
#define R_RFSTATE_INIT						(0x00)
#define R_RFSTATE_IDLE						(0x01)
#define R_RFSTATE_RXAUTO					(0x02)
#define R_RFSTATE_CONTINUOUS				(0x03)

/* prameter Id */
#define R_ParamId_channel					(0x00)
#define R_ParamId_transmitPower				(0x01)
#define R_ParamId_preambleLength			(0x02)
#define R_ParamId_agcWaitGainOffset			(0x03)
#define R_ParamId_antennaSwitchEna			(0x04)
#define R_ParamId_antennaSwitchEnaTiming	(0x05)
#define R_ParamId_gpio0Setting				(0x06)
#define R_ParamId_gpio3Setting				(0x07)
#define R_ParamId_antennaSelectTx			(0x08)

/* prameter range */
#define R_MIN_preambleLength				(RP_MINVAL_PREAMBLELEN)		// 4
#define R_MAX_preambleLength				(RP_MAXVAL_PREAMBLELEN)		// 1000(0x03E8)
#define R_MAX_agcWaitGainOffset				(0x1F)
#define R_MIN_antennaSwitchEnaTiming		(0x0001)
#define R_MAX_antennaSwitchEnaTiming		(0x0154)

unsigned char AppHexStrToNum( unsigned char **ppBuf, void *pData, short dataSize, unsigned char isOctetStr );

/***********************************************************************
 * typedef definitions
 **********************************************************************/
/* command execution processing */
typedef struct {
	const uint8_t *pCmd;
	void (*pFunc)(uint8_t *pBuff);
} r_simple_cmd_func_t;

typedef struct {
	uint8_t paramId;
	uint8_t (*pFunc)(uint8_t *pBuff);
} r_setr_func_t;

typedef struct {
	uint8_t paramId;
	void (*pFunc)(uint8_t handle);
} r_getr_func_t;

/* rfdriver API wrapping function */
typedef struct {
	boolean_t	eventFlag;
	uint8_t		status;
} r_rfdrvIf_msg_pddatacfm_t;

typedef struct {
	uint16_t	rssi;
	uint16_t	psduLength;
	uint8_t		psdu[R_TEST_DATA_LENGTH];
} r_rfdrvIf_msg_pddataind_t;
#define R_MSG_SIZE_DATAI	(sizeof(r_rfdrvIf_msg_pddataind_t))

typedef union {
	uint8_t		phyCurrentChannel;
	uint8_t		phyCurrentPage;
	uint32_t	phyChannelsSupported[2];
	uint8_t		phyChannelsSupportedPage;
	uint16_t 	phyCcaVth;
	uint8_t		phyTransmitPower;
	uint8_t		phyFSKFECRxEna;
	uint8_t		phyFSKFECTxEna;
	uint8_t		phyFSKFECScheme;
	uint16_t 	phyLvlFltrVth;
	uint8_t		phyBackOffSeed;
	uint8_t		phyCrcErrorUpMsg;
	uint8_t		macAddressFilter1Ena;
	uint16_t 	macShortAddr1;
	uint32_t 	macExtendedAddress1[2];
	uint16_t 	macPanId1;
	uint8_t 	macPanCoord1;
	uint8_t 	macFramePend1;
	uint8_t 	macAddressFilter2Ena;
	uint16_t 	macShortAddr2;
	uint32_t 	macExtendedAddress2[2];
	uint16_t 	macPanId2;
	uint8_t 	macPanCoord2;
	uint8_t 	macFramePend2;
	uint8_t 	macMaxCsmaBackOff;
	uint8_t 	macMinBe;
	uint8_t 	macMaxBe;
	uint8_t 	macMaxFrameRetries;
	uint16_t	phyCsmaBackoffPeriod;
	uint16_t	phyCcaDuration;
	uint8_t		phyMRFSKSFD;
	uint16_t	phyFSKPreambleLength;
	uint8_t		phyFSKScramblePSDU;
	uint8_t		phyFSKOpeMode;
	uint8_t		phyFCSLength;
	uint16_t	phyAckReplyTime;
	uint16_t	phyAckWaitDuration;
	uint8_t		phyProfileSpecificMode;
	uint8_t		phyAntennaSwitchEna;
	uint8_t		phyAntennaDiversityRxEna;
	uint8_t		phyAntennaSelectTx;
	uint8_t		phyAntennaSelectAckTx;
	uint8_t		phyAntennaSelectAckRx;
	uint8_t		phyRxTimeoutMode;
	uint8_t		phyFreqBandId;
	uint16_t	phyDataRate;
	uint8_t		phyRegulatoryMode;	// V3.08
	uint8_t		phyPreamble4ByteRxMode;
	uint16_t	phyAgcStartVth;
	uint8_t		phyCcaBandwidth;
	uint8_t		phyEdBandwidth;
	uint16_t	phyAntennaDiversityStartVth;
	uint16_t	phyAntennaSwitchingTime;
	uint8_t		phySfdDetectionExtend;
	uint8_t		phyAgcWaitGainOffset;
	uint8_t		phyCcaVthOffset;
	uint16_t	phyAntennaSwitchEnaTiming;
	uint8_t		phyGpio0Setting;
	uint8_t		phyGpio3Setting;
	uint16_t	phyRmodeTonMax;				// V3.08
	uint16_t	phyRmodeToffMin;			// V3.08
	uint16_t	phyRmodeTcumSmpPeriod;		// V3.08
	uint32_t	phyRmodeTcumLimit;			// V3.08
	uint32_t	phyRmodeTcum;				// V3.08
	uint8_t		phyAckWithCca;				// V3.08
	uint8_t		phyRssiOutputOffset;		// V3.08
	uint32_t	phyFrequency;
	int32_t		phyFrequencyOffset;
} r_rfdrvIf_attr_t;

#define R_NUM_FREQ_BAND		(10)
#define R_MAX_FSK_OPEMODE	(7)
typedef struct {
	uint8_t maxChannel[R_MAX_FSK_OPEMODE];
} r_maxChannelTbl_t;

/* rfdrvIf message buff */
#define R_MSG_MAXSIZE		(R_MSG_SIZE_DATAI)
#define	R_MSG_BUFF_NUM		(2)

typedef struct {
	uint8_t		cont[R_MSG_MAXSIZE];
} r_rfdrvIf_msgcont_t;

typedef struct {
	uint8_t		readBufNum;
	uint8_t		writBufNum 	:7;
	uint8_t		bufFullBit 	:1;
	r_rfdrvIf_msgcont_t buff[R_MSG_BUFF_NUM];
} r_rfdrvIf_msgbuff_t;

static const uint8_t g_MaxFskOpeModeTbl[R_NUM_FREQ_BAND] = 
{
	0x07,0x03,0x03,0x04,0x04,0x06,0x03,0x03,0x01,0x04
};
static const r_maxChannelTbl_t g_MaxChannelTbl[R_NUM_FREQ_BAND] =
{
	/* freqBanId = 0x04 */ { 0x21,0x10,0x10,0x44,0x44,0x44,0x44 },
	/* freqBanId = 0x05 */ { 0xC6,0x62,0x30,0x00,0x00,0x00,0x00 },
	/* freqBanId = 0x06 */ { 0x26,0x12,0x08,0x00,0x00,0x00,0x00 },
	/* freqBanId = 0x07 */ { 0x80,0x3F,0x3F,0x80,0x00,0x00,0x00 },
	/* freqBanId = 0x08 */ { 0x1F,0x0F,0x0F,0x1F,0x00,0x00,0x00 },
	/* freqBanId = 0x09 */ { 0x25,0x24,0x23,0x23,0x24,0x25,0x00 },
	/* freqBanId = 0x0E */ { 0x23,0x80,0x29,0x00,0x00,0x00,0x00 },
	/* freqBanId = 0x0F */ { 0x3A,0x3A,0x3A,0x00,0x00,0x00,0x00 },
	/* freqBanId = 0x10 */ { 0x80,0x00,0x00,0x00,0x00,0x00,0x00 },
	/* freqBanId = 0x11 */ { 0x11,0x11,0x11,0x11,0x00,0x00,0x00 }
};


/***********************************************************************
 * global function prototypes
 **********************************************************************/
void R_Simple_RFTEST_ProcessCommand( uint8_t *pBuff );

/***********************************************************************
 * private function prototypes
 **********************************************************************/
/* command execution function */
static void R_SSCMD_ProcessCmd_RSTR( uint8_t *pCmd );
static void R_SSCMD_ProcessCmd_RXOFFR( uint8_t *pCmd );
static void R_SSCMD_ProcessCmd_RXONR( uint8_t *pCmd );
static void R_SSCMD_ProcessCmd_RCVI( void *pMsg );
static void R_SSCMD_ProcessCmd_TXR( uint8_t *pCmd );
static void R_SSCMD_ProcessCmd_CTXR( uint8_t *pCmd );
static void R_SSCMD_ProcessCmd_PTXR( uint8_t *pCmd );
static void R_SSCMD_ProcessCmd_CTSR( uint8_t *pCmd );
static void R_SSCMD_ProcessCmd_SETR( uint8_t *pCmd );
static uint8_t R_SetR_channel( uint8_t *pCmd );
static uint8_t R_SetR_transmitPower( uint8_t *pCmd );
static uint8_t R_SetR_preambleLength( uint8_t *pCmd );
static uint8_t R_SetR_agcWaitGainOffset( uint8_t *pCmd );
static uint8_t R_SetR_antennaSwitchEna( uint8_t *pCmd );
static uint8_t R_SetR_antennaSwitchEnaTiming( uint8_t *pCmd );
static uint8_t R_SetR_gpio0Setting( uint8_t *pCmd );
static uint8_t R_SetR_gpio3Setting( uint8_t *pCmd );
static uint8_t R_SetR_antennaSelectTx( uint8_t *pCmd );
static void R_SSCMD_ProcessCmd_GETR( uint8_t *pCmd );
static void R_GetR_channel( uint8_t handle );
static void R_printf_32data( uint8_t *p_data );
static void R_GetR_transmitPower( uint8_t handle );
static void R_GetR_preambleLength( uint8_t handle );
static void R_GetR_agcWaitGainOffset( uint8_t handle );
static void R_GetR_antennaSwitchEna( uint8_t handle );
static void R_GetR_antennaSwitchEnaTiming( uint8_t handle );
static void R_GetR_gpio0Setting( uint8_t handle );
static void R_GetR_gpio3Setting( uint8_t handle );
static void R_GetR_antennaSelectTx( uint8_t handle );
static void R_SSCMD_ProcessCmd_SIBR( uint8_t *pCmd );
static void R_SSCMD_ProcessCmd_GIBR( uint8_t *pCmd );
static void R_SSCMD_ProcessCmd_CERRC( uint8_t status );

/* rfdriver API wrapping function */
static uint8_t RfdrvIf_Start( void );
static void RfdrvIf_Reset( void );
static uint8_t RfdrvIf_SetRxOff( void );
static uint8_t RfdrvIf_SetRxOn( void );
static uint8_t RfdrvIf_SendFrame( void );
static uint8_t RfdrvIf_SetAttr( uint8_t attrId, uint8_t attrLength, uint8_t RP_FAR *p_attrValue );
static uint8_t RfdrvIf_GetAttr( uint8_t attrId, uint8_t attrValueLength, uint8_t *p_attrLength,
								uint8_t *p_attrValue );
static uint8_t RfdrvIf_GetAttrLength( uint8_t attrId );
static uint8_t RfdrvIf_SetAttrChannel( uint8_t freqBandId, uint8_t fskOpeMode, uint8_t setChannel, int32_t frequencyOffset );
static uint8_t RfdrvIf_CheckChannelsSupported( uint8_t channel, uint8_t maskChSupported );
static uint8_t RfdrvIf_ConvertChannelsSupportedPage( uint8_t channel );
static uint8_t RfdrvIf_SetAttrTxPower( uint8_t freqBandId, uint8_t transmitPower );
static uint8_t RfdrvIf_ContUnmoduTx( void );
static uint8_t RfdrvIf_Pn9ContModuTx( void );
static uint8_t RfdrvIf_ContTxStop( void );

/* rfdriver callback function */
static void RfdrvIf_PdDataIndCallback( uint8_t *pData, uint16_t dataLength, uint32_t time,
									uint8_t linkQuality, uint16_t rssi, uint8_t selectedAntenna,
									uint16_t rssi_0, uint16_t rssi_1, uint8_t status,
									uint8_t filteredAddr, uint8_t phr );
static void RfdrvIf_PdDataCfmCallback( uint8_t status, uint8_t framePend, uint8_t numBackoffs );
static void RfdrvIf_PlmeCcaCfmCallback( uint8_t status );
static void RfdrvIf_PlmeEdCfmCallback( uint8_t phyState, uint8_t edValue, uint16_t rssi );
static void RfdrvIf_RxOffIndCallback( void );
static void RfdrvIf_FatalErrorIndCallback( uint8_t status );
static void RfdrvIf_WarningIndCallback( uint8_t status );

/* rfdrvIf message buff */
static void RfdrvIf_MessageProcess( void );
static void RfdrvIf_InitMsgBuff( void );
static void RfdrvIf_PutMsgBuff( void *pSrcBuff, uint16_t length );
static uint8_t *RfdrvIf_GetMsgBuff( void );
static void RfdrvIf_RelMsgBuff( void );

/***********************************************************************
 * command table
 **********************************************************************/
static const uint8_t CmdStr_RSTR[]	 = "RSTR";		// "RF Reset" Request Command
static const uint8_t CmdStr_RXOFFR[] = "RXOFFR";	// "RF Idle(RX Off)" Request Command
static const uint8_t CmdStr_RXONR[]  = "RXONR";		// "RF RX ON" Request Command
static const uint8_t CmdStr_TXR[]	 = "TXR";		// "Frame Transmission" Request Command
static const uint8_t CmdStr_CTXR[]	 = "CTXR";		// "Continuous Unmodulated Transmission" Request Command
static const uint8_t CmdStr_PTXR[]	 = "PTXR";		// "PN9 Continuous Modulated Transmission" Request Command
static const uint8_t CmdStr_CTSR[]	 = "CTSR";		// "Continuous Transmisson Stop" Request Command
static const uint8_t CmdStr_SETR[]	 = "SETR";		// "Set Parameter" Request Command
static const uint8_t CmdStr_GETR[]	 = "GETR";		// "Get Parameter" Request Command
static const uint8_t CmdStr_SIBR[]	 = "SIBR";		// "Set IB Value" Request Command
static const uint8_t CmdStr_GIBR[]	 = "GIBR";		// "Get IB value" Request Command

static const r_simple_cmd_func_t CmdFunc_RFTest[] = {
	{ CmdStr_RSTR, 	 	&R_SSCMD_ProcessCmd_RSTR	},
	{ CmdStr_RXOFFR, 	&R_SSCMD_ProcessCmd_RXOFFR	},
	{ CmdStr_RXONR,  	&R_SSCMD_ProcessCmd_RXONR	},
	{ CmdStr_TXR, 	 	&R_SSCMD_ProcessCmd_TXR		},
	{ CmdStr_CTXR,	 	&R_SSCMD_ProcessCmd_CTXR	},
	{ CmdStr_PTXR,	 	&R_SSCMD_ProcessCmd_PTXR	},
	{ CmdStr_CTSR,	 	&R_SSCMD_ProcessCmd_CTSR	},
	{ CmdStr_SETR,	 	&R_SSCMD_ProcessCmd_SETR	},
	{ CmdStr_GETR,	 	&R_SSCMD_ProcessCmd_GETR	},
	{ CmdStr_SIBR,	 	&R_SSCMD_ProcessCmd_SIBR	},
	{ CmdStr_GIBR,	 	&R_SSCMD_ProcessCmd_GIBR	},
};

static const r_setr_func_t gc_SetrFunc[] = {
	{ R_ParamId_channel,				&R_SetR_channel 				},
	{ R_ParamId_transmitPower,			&R_SetR_transmitPower			},
	{ R_ParamId_preambleLength,			&R_SetR_preambleLength			},
	{ R_ParamId_agcWaitGainOffset,		&R_SetR_agcWaitGainOffset		},
	{ R_ParamId_antennaSwitchEna,		&R_SetR_antennaSwitchEna		},
	{ R_ParamId_antennaSwitchEnaTiming,	&R_SetR_antennaSwitchEnaTiming	},
	{ R_ParamId_gpio0Setting,			&R_SetR_gpio0Setting			},
	{ R_ParamId_gpio3Setting,			&R_SetR_gpio3Setting			},
	{ R_ParamId_antennaSelectTx,		&R_SetR_antennaSelectTx			},
};

static const r_getr_func_t gc_GetrFunc[] = {
	{ R_ParamId_channel,				&R_GetR_channel 				},
	{ R_ParamId_transmitPower,			&R_GetR_transmitPower			},
	{ R_ParamId_preambleLength,			&R_GetR_preambleLength			},
	{ R_ParamId_agcWaitGainOffset,		&R_GetR_agcWaitGainOffset		},
	{ R_ParamId_antennaSwitchEna,		&R_GetR_antennaSwitchEna		},
	{ R_ParamId_antennaSwitchEnaTiming,	&R_GetR_antennaSwitchEnaTiming	},
	{ R_ParamId_gpio0Setting,			&R_GetR_gpio0Setting			},
	{ R_ParamId_gpio3Setting,			&R_GetR_gpio3Setting			},
	{ R_ParamId_antennaSelectTx,		&R_GetR_antennaSelectTx			},
};

/***********************************************************************
 * private variables
 **********************************************************************/
static uint8_t gRfState;
static uint8_t gNtfHandle;
static r_rfdrvIf_msgbuff_t gMsgBuff;
static r_rfdrvIf_msg_pddatacfm_t gCbMsgPdDataCfm;
static r_rfdrvIf_msg_pddataind_t gCbMsgPdDataInd;

/***********************************************************************
 * Simple serial command is valid function
 **********************************************************************/
/***********************************************************************
 * function name  : R_Simple_RFTest_IsValid
 * description    : Check if simple RF Test program is enabled 
 * parameters     : none
 * return value   : R_TRUE in case of RF Test program execution 
                    R_FALSE in case of Wi-SUN FAN stack execution 
 **********************************************************************/
r_boolean_t R_Simple_RFTest_IsValid()
{
	/* For RX651: Read Switch SW3-1 of MB-RX604S-02 board (ON = RF Test Mode) */
	/* For RL78G1H: Read Switch SW1-1 of TK-RLG1H+SB2 board (ON = RF TEST Mode) */

  if(RdrvSwitch0_GetCondition() == 0)
	{
		return R_TRUE;
	}
	return R_FALSE;
}


/********************************************************************************
* Function Name     : AppCommandInput
* Description       : Read command from UART
*                   : Process command and message
* Arguments         : None
* Return Value      : None
********************************************************************************/
void AppCommandInput(void)
{
    short ch;
    unsigned char cmdReady = 0;

    /*--------------------------------------------------*/
    /* Read input command from UART                     */
    /*--------------------------------------------------*/
    do
    {
        ch = RdrvUART_GetChar();
        switch (ch)
        {
            case 0x0a:
                if ((AppCmdSize > 0) && (AppCmdSize < sizeof(AppCmdBuf)))
                {
                    AppCmdBuf[AppCmdSize++] = 0;
                    cmdReady = 1;
                }
                else
                {
                    AppCmdSize = 0;
                }
                break;

            case 0x0d:

                /* No Action */
                break;

            default:
                if (ch >= 0)
                {
                    if (AppCmdSize < sizeof(AppCmdBuf))
                    {
                        AppCmdBuf[AppCmdSize++] = (unsigned char)ch;  /* put character to rcv buffer */
                    }
                }
                break;
        }
    }
    while ((ch > 0) && (cmdReady == 0));

    /*--------------------------------------------------*/
    /* Process command                                  */
    /*--------------------------------------------------*/
    if (cmdReady == 1)
    {
        R_Simple_RFTest_ProcessCommand(AppCmdBuf);
        AppCmdSize = 0;
    }
}

/***********************************************************************
 * Simple serial command main function
 **********************************************************************/
/***********************************************************************
 * function name  : R_SimpleSerial_RFTest
 * description    : Simple serial RF Test command program main processing
 * parameters     : none
 * return value   : none
 **********************************************************************/
void R_Simple_RFTest_Main( void )
{
	RfdrvIf_Start();
	while (1)
	{
		AppCommandInput();
		RfdrvIf_MessageProcess();
	}
}

/***********************************************************************
 * command execution function
 **********************************************************************/
/***********************************************************************
 * function name  : R_Simple_RFTest_ProcessCommand
 * description    : command reception processing
 * parameters     : pBuff...pointer to the uart data
 * return value   : none
 **********************************************************************/
void R_Simple_RFTest_ProcessCommand( uint8_t *pBuff )
{
	uint8_t i, size;
	uint8_t status = R_STATUS_UNSUPPORTED_COMMAND;

	size = sizeof(CmdFunc_RFTest)/sizeof(CmdFunc_RFTest[0]);
	for ( i=0; i<size; i++ )
	{
		if ( !AppStricmp( (void __far *)CmdFunc_RFTest[i].pCmd, (void*)&pBuff ))
		{
			CmdFunc_RFTest[i].pFunc( pBuff );
			status = R_STATUS_SUCCESS;
			break;
		}
	}

	if ( status != R_STATUS_SUCCESS )
	{
		R_SSCMD_ProcessCmd_CERRC( status );
	}
}

/***********************************************************************
 * function name  : R_SSCMD_ProcessCmd_RSTR
 * description	  : "RF Reset Request"(RSTR) Command Processing
 * parameters	  : pCmd...pointer to the uart data
 *				  :   RSTR (handle)
 * return value   : none
 * output		  : Confirm command
 *				  :   RSTC (handle) (status) (rfstate)
 **********************************************************************/
static void R_SSCMD_ProcessCmd_RSTR( uint8_t *pCmd )
{
	uint8_t status = R_STATUS_SUCCESS;
	uint8_t res;
	uint8_t handle;

	/* get command parameters */
	/* (handle) */
	res = AppHexStrToNum( &pCmd, &handle, sizeof(handle), R_FALSE );
	if ( res != 0 )
	{
		status = R_STATUS_INVALID_COMMAND;
	}

	/* command execution */
	if ( status == R_STATUS_SUCCESS )
	{
		/* rfdriver reset */
		RfdrvIf_Reset();
	}

	/* "RTSC" command transmission */
	PRINTF( "RSTC " );
	PRINTF( "%02X ", handle );
	PRINTF( "%02X ", status );
	PRINTF( "%02X\n", gRfState );
}

/***********************************************************************
 * function name  : R_SSCMD_ProcessCmd_RXOFFR
 * description	  : "RF Idle(RX Off) Request(RXOFFR)" Command Processing
 * parameters	  : pCmd...pointer to the uart data
 *				  :   RXOFFR (handle)
 * return value   : none
 * output		  : Confirm command
 *				  :   RXOFFC (handle) (status) (rfstate)
 **********************************************************************/
static void R_SSCMD_ProcessCmd_RXOFFR( uint8_t *pCmd )
{
	uint8_t status = R_STATUS_SUCCESS;
	uint8_t res;
	uint8_t handle;

	/* get command parameters */
	/* (handle) */
	res = AppHexStrToNum( &pCmd, &handle, sizeof(handle), R_FALSE );
	if ( res != 0 )
	{
		status = R_STATUS_INVALID_COMMAND;
	}

	/* command execution */
	if ( status == R_STATUS_SUCCESS )
	{
		/* rfdriver Idle Setting Request */
		status = RfdrvIf_SetRxOff();
	}

	/* "RXOFFC" command transmission */
	PRINTF( "RXOFFC " );
	PRINTF( "%02X ", handle );
	PRINTF( "%02X ", status );
	PRINTF( "%02X\n", gRfState );
}

/***********************************************************************
 * function name  : R_SSCMD_ProcessCmd_RXONR
 * description	  : "RF RX ON Request(RXONR)" Command Processing
 * parameters	  : pCmd...pointer to the uart data
 *				  :   RXONR (handle)
 * return value   : none
 * output		  : Confirm command
 *				  :   RXONC (handle) (status) (rfState)
 **********************************************************************/
static void R_SSCMD_ProcessCmd_RXONR( uint8_t *pCmd )
{
	uint8_t status = R_STATUS_SUCCESS;
	uint8_t res;
	uint8_t handle;

	/* get command parameters */
	/* (handle) */
	res = AppHexStrToNum( &pCmd, &handle, sizeof(handle), R_FALSE );
	if ( res != 0 )
	{
		status = R_STATUS_INVALID_COMMAND;
	}

	/* command execution */
	if ( status == R_STATUS_SUCCESS )
	{
		/* rfdriver Reception ON Setting Request */
		status = RfdrvIf_SetRxOn();
	}

	/* "RXONC" command transmission */
	PRINTF( "RXONC " );
	PRINTF( "%02X ", handle );
	PRINTF( "%02X ", status );
	PRINTF( "%02X\n", gRfState );
}

/***********************************************************************
 * function name  : R_SSCMD_ProcessCmd_RCVI
 * description	  : Process "RCVI" command
 * parameters	  : pMsg...pointer to message
 * return value   : none
 * output		  : Indication command
 *				  :   RCVI (handle) (dataLength) (data)
 **********************************************************************/
static void R_SSCMD_ProcessCmd_RCVI( void *pMsg )
{
	uint16_t i;
	uint16_t psduLength;	// for CC-RL
	uint16_t rssi;
	r_rfdrvIf_msg_pddataind_t *pInd = (r_rfdrvIf_msg_pddataind_t *)pMsg;

	/* "RCVI" command transmission */
	PRINTF( "RCVI " );
	PRINTF( "%02X ", gNtfHandle++ );
	rssi = RP_VAL_ARRAY_TO_UINT16( (uint8_t *)&pInd->rssi );	// for CC-RL
	PRINTF( "%04X ", rssi );
	psduLength = RP_VAL_ARRAY_TO_UINT16( (uint8_t *)&pInd->psduLength );	// for CC-RL
	PRINTF( "%04X ", psduLength );

	for ( i=0; i<psduLength; i++ )
	{
		PRINTF( "%02X", pInd->psdu[i] );
	}
	PRINTF( "\n" );
}

/***********************************************************************
 * function name  : R_SSCMD_ProcessCmd_TXR
 * description	  : "Frame Transmission Request(TXR)" Command Processing
 * parameters	  : pCmd...pointer to the uart data
 *				  :   TXR (handle)
 * return value   : none
 * output		  : Confirm command
 *				  :   TXC (handle) (status) (rfstate)
 **********************************************************************/
static void R_SSCMD_ProcessCmd_TXR( uint8_t *pCmd )
{
	uint8_t status = R_STATUS_SUCCESS;
	uint8_t res;
	uint8_t handle;

	/* get command parameters */
	/* (handle) */
	res = AppHexStrToNum( &pCmd, &handle, sizeof(handle), R_FALSE );
	if ( res != 0 )
	{
		status = R_STATUS_INVALID_COMMAND;
	}

	/* command execution */
	if ( status == R_STATUS_SUCCESS )
	{
		/* frame transmission processing */
		status = RfdrvIf_SendFrame();
	}

	/* "TXC" command transmission */
	PRINTF( "TXC " );
	PRINTF( "%02X ", handle );
	PRINTF( "%02X ", status );
	PRINTF( "%02X\n", gRfState );
}

/***********************************************************************
 * function name  : R_SSCMD_ProcessCmd_CTXR
 * description	  : "Continuous Unmodulated Transmission Request(CTXR)"
 *                : Command Processing
 * parameters	  : pCmd...pointer to the uart data
 *				  :   CTXR (handle)
 * return value   : none
 * output		  : Confirm command
 *				  :   CTXC (handle) (status) (rfstate)
 **********************************************************************/
static void R_SSCMD_ProcessCmd_CTXR( uint8_t *pCmd )
{
	uint8_t status = R_STATUS_SUCCESS;
	uint8_t res;
	uint8_t handle;

	/* get command parameters */
	/* (handle) */
	res = AppHexStrToNum( &pCmd, &handle, sizeof(handle), R_FALSE );
	if ( res != 0 )
	{
		status = R_STATUS_INVALID_COMMAND;
	}

	/* command execution */
	if ( status == R_STATUS_SUCCESS )
	{
		status = RfdrvIf_ContUnmoduTx();
	}

	/* "CTXC" command transmission */
	PRINTF( "CTXC " );
	PRINTF( "%02X ", handle );
	PRINTF( "%02X ", status );
	PRINTF( "%02X\n", gRfState );
}

/***********************************************************************
 * function name  : R_SSCMD_ProcessCmd_PTXR
 * description	  : "PN9 Continuous Modulated Transmission Request(PTXR)"
 *				  : Command Processing
 * parameters	  : pCmd...pointer to the uart data
 *				  :   PTXR (handle)
 * return value   : none
 * output		  : Confirm command
 *				  :   PTXC (handle) (status) (rfstate)
 **********************************************************************/
static void R_SSCMD_ProcessCmd_PTXR( uint8_t *pCmd )
{
	uint8_t status = R_STATUS_SUCCESS;
	uint8_t res;
	uint8_t handle;

	/* get command parameters */
	/* (handle) */
	res = AppHexStrToNum( &pCmd, &handle, sizeof(handle), R_FALSE );
	if ( res != 0 )
	{
		status = R_STATUS_INVALID_COMMAND;
	}

	/* command execution */
	if ( status == R_STATUS_SUCCESS )
	{
		status = RfdrvIf_Pn9ContModuTx();
	}

	/* "PTXC" command transmission */
	PRINTF( "PTXC " );
	PRINTF( "%02X ", handle );
	PRINTF( "%02X ", status );
	PRINTF( "%02X\n", gRfState );
}

/***********************************************************************
 * function name  : R_SSCMD_ProcessCmd_CTSR
 * description	  : "Continuous Transmisson Stop Request(CTSR)" Command Processing
 * parameters	  : pCmd...pointer to the uart data
 *				  :   CTSR (handle)
 * return value   : none
 * output		  : Confirm command
 *				  :   CTSC (handle) (status) (rfstate)
 **********************************************************************/
static void R_SSCMD_ProcessCmd_CTSR( uint8_t *pCmd )
{
	uint8_t status = R_STATUS_SUCCESS;
	uint8_t res;
	uint8_t handle;

	/* get command parameters */
	/* (handle) */
	res = AppHexStrToNum( &pCmd, &handle, sizeof(handle), R_FALSE );
	if ( res != 0 )
	{
		status = R_STATUS_INVALID_COMMAND;
	}

	/* command execution */
	if ( status == R_STATUS_SUCCESS )
	{
		status = RfdrvIf_ContTxStop();
	}

	/* "CTSC" command transmission */
	PRINTF( "CTSC " );
	PRINTF( "%02X ", handle );
	PRINTF( "%02X ", status );
	PRINTF( "%02X\n", gRfState );
}

/***********************************************************************
 * function name  : R_SSCMD_ProcessCmd_SETR
 * description	  : "Set Parameter Request(SETR)" Command Processing
 * parameters	  : pCmd...pointer to the uart data
 *				  :   SETR (handle) (paramId) (parameter)
 * return value   : none
 * output		  : Confirm command
 *				  :   SETC (handle) (status) (rfstate) (paramId)
 **********************************************************************/
static void R_SSCMD_ProcessCmd_SETR( uint8_t *pCmd )
{
	uint8_t status = R_STATUS_SUCCESS;
	uint8_t res;
	uint8_t handle;
	uint8_t paramId;
	uint8_t i, size;

	/* get command parameters */
	/* (handle) */
	res = AppHexStrToNum( &pCmd, &handle, sizeof(handle), R_FALSE );
	/* (paramId) */
	res |= AppHexStrToNum( &pCmd, &paramId, sizeof(paramId), R_FALSE );
	if ( res != 0 )
	{
		status = R_STATUS_INVALID_COMMAND;
	}

	/* command execution */
	if ( status == R_STATUS_SUCCESS )
	{
		status = R_STATUS_INVALID_PARAMETER;

		size = sizeof(gc_SetrFunc)/sizeof(gc_SetrFunc[0]);
		for ( i=0; i<size; i++ )
		{
			if ( gc_SetrFunc[i].paramId == paramId )
			{
				status = gc_SetrFunc[i].pFunc( pCmd );
				break;
			}
		}
	}

	/* "SETC" command transmission */
	PRINTF( "SETC " );
	PRINTF( "%02X ", handle );
	PRINTF( "%02X ", status );
	PRINTF( "%02X", gRfState );
	if ( status == R_STATUS_SUCCESS )
	{
		PRINTF( " %02X", paramId );
	}
	PRINTF( "\n" );
}

/***********************************************************************
 * function name  : R_SetR_channel
 * description	  : "Set Parameter Request(SETR)" Command Processing
 * parameters	  : pCmd...pointer to the uart data
 * return value   : status
 **********************************************************************/
static uint8_t R_SetR_channel( uint8_t *pCmd )
{
	uint8_t status = R_STATUS_SUCCESS;
	uint8_t res;
	uint8_t freqBandId, fskOpeMode, channel;
	int32_t frequencyOffset;

	/* get command parameters */
	/* (freqBandId) */
	res = AppHexStrToNum( &pCmd, &freqBandId, sizeof(freqBandId), R_FALSE );
	/* (fskOpeMode) */
	res |= AppHexStrToNum( &pCmd, &fskOpeMode, sizeof(fskOpeMode), R_FALSE );
	/* (channel) */
	res |= AppHexStrToNum( &pCmd, &channel, sizeof(channel), R_FALSE );
	/* (frequencyOffset) */
	res |= AppHexStrToNum( &pCmd, &frequencyOffset, sizeof(frequencyOffset), R_FALSE );
	if ( res != 0 )
	{
		status = R_STATUS_INVALID_COMMAND;
	}

	/* command execution */
	if ( status == R_STATUS_SUCCESS )
	{
		/* Set Attribute Value */
		status = RfdrvIf_SetAttrChannel( freqBandId, fskOpeMode, channel, frequencyOffset );
	}

	return( status );
}

/***********************************************************************
 * function name  : R_SetR_transmitPower
 * description	  : "Set Parameter Request(SETR)" Command Processing
 * parameters	  : pCmd...pointer to the uart data
 * return value   : status
 **********************************************************************/
static uint8_t R_SetR_transmitPower( uint8_t *pCmd )
{
	uint8_t status = R_STATUS_SUCCESS;
	uint8_t res;
	uint8_t freqBandId, transmitPower;

	/* get command parameters */
	/* (freqBandId) */
	res = AppHexStrToNum( &pCmd, &freqBandId, sizeof(freqBandId), R_FALSE );
	/* (transmitPower) */
	res |= AppHexStrToNum( &pCmd, &transmitPower, sizeof(transmitPower), R_FALSE );
	if ( res != 0 )
	{
		status = R_STATUS_INVALID_COMMAND;
	}

	/* command execution */
	if ( status == R_STATUS_SUCCESS )
	{
		/* Set Attribute Value */
		status = RfdrvIf_SetAttrTxPower( freqBandId, transmitPower );
	}

	return( status );
}

/***********************************************************************
 * function name  : R_SetR_preambleLength
 * description	  : "Set Parameter Request(SETR)" Command Processing
 * parameters	  : pCmd...pointer to the uart data
 * return value   : status
 **********************************************************************/
static uint8_t R_SetR_preambleLength( uint8_t *pCmd )
{
	uint8_t status = R_STATUS_SUCCESS;
	uint8_t res;
	uint16_t preambleLength;

	/* get command parameters */
	/* (preambleLength) */
	res = AppHexStrToNum( &pCmd, &preambleLength, sizeof(preambleLength), R_FALSE );
	if ( res != 0 )
	{
		status = R_STATUS_INVALID_COMMAND;
	}

	/* check parameter */
	if ( status == R_STATUS_SUCCESS )
	{
		if (( preambleLength < R_MIN_preambleLength )
			||( R_MAX_preambleLength < preambleLength ))
		{
			status = R_STATUS_INVALID_PARAMETER;
		}
	}

	/* command execution */
	if ( status == R_STATUS_SUCCESS )
	{
		/* Set Attribute Value */
		status = RfdrvIf_SetAttr( RP_PHY_FSK_PREAMBLE_LENGTH, sizeof(preambleLength), (uint8_t *)&preambleLength );
	}

	return( status );
}

/***********************************************************************
 * function name  : R_SetR_agcWaitGainOffset
 * description	  : "Set Parameter Request(SETR)" Command Processing
 * parameters	  : pCmd...pointer to the uart data
 * return value   : status
 **********************************************************************/
static uint8_t R_SetR_agcWaitGainOffset( uint8_t *pCmd )
{
	uint8_t status = R_STATUS_SUCCESS;
	uint8_t res;
	uint8_t agcWaitGainOffset;

	/* get command parameters */
	/* (agcWaitGainOffset) */
	res = AppHexStrToNum( &pCmd, &agcWaitGainOffset, sizeof(agcWaitGainOffset), R_FALSE );
	if ( res != 0 )
	{
		status = R_STATUS_INVALID_COMMAND;
	}

	/* check parameter */
	if ( status == R_STATUS_SUCCESS )
	{
		if ( R_MAX_agcWaitGainOffset < agcWaitGainOffset )
		{
			status = R_STATUS_INVALID_PARAMETER;
		}
	}

	/* command execution */
	if ( status == R_STATUS_SUCCESS )
	{
		/* Set Attribute Value */
		status = RfdrvIf_SetAttr( RP_PHY_AGC_WAIT_GAIN_OFFSET, sizeof(agcWaitGainOffset), (uint8_t *)&agcWaitGainOffset );
	}

	return( status );
}

/***********************************************************************
 * function name  : R_SetR_antennaSwitchEna
 * description	  : "Set Parameter Request(SETR)" Command Processing
 * parameters	  : pCmd...pointer to the uart data
 * return value   : status
 **********************************************************************/
static uint8_t R_SetR_antennaSwitchEna( uint8_t *pCmd )
{
	uint8_t status = R_STATUS_SUCCESS;
	uint8_t res;
	uint8_t antennaSwitchEna;

	/* get command parameters */
	/* (antennaSwitchEna) */
	res = AppHexStrToNum( &pCmd, &antennaSwitchEna, sizeof(antennaSwitchEna), R_FALSE );
	if ( res != 0 )
	{
		status = R_STATUS_INVALID_COMMAND;
	}

	/* check parameter */
	if ( status == R_STATUS_SUCCESS )
	{
		switch (antennaSwitchEna)
		{
			case 0x00:
			case 0x01:
				break;
			default:
				status = R_STATUS_INVALID_PARAMETER;
				break;
		}
	}

	/* command execution */
	if ( status == R_STATUS_SUCCESS )
	{
		/* Set Attribute Value */
		status = RfdrvIf_SetAttr( RP_PHY_ANTENNA_SWITCH_ENA, sizeof(antennaSwitchEna), (uint8_t *)&antennaSwitchEna );
	}

	return( status );
}

/***********************************************************************
 * function name  : R_SetR_antennaSwitchEnaTiming
 * description	  : "Set Parameter Request(SETR)" Command Processing
 * parameters	  : pCmd...pointer to the uart data
 * return value   : status
 **********************************************************************/
static uint8_t R_SetR_antennaSwitchEnaTiming( uint8_t *pCmd )
{
	uint8_t status = R_STATUS_SUCCESS;
	uint8_t res;
	uint16_t antennaSwitchEnaTiming;

	/* get command parameters */
	/* (antennaSwitchEnaTiming) */
	res = AppHexStrToNum( &pCmd, &antennaSwitchEnaTiming, sizeof(antennaSwitchEnaTiming), R_FALSE );
	if ( res != 0 )
	{
		status = R_STATUS_INVALID_COMMAND;
	}

	/* check parameter */
	if ( status == R_STATUS_SUCCESS )
	{
		if (( antennaSwitchEnaTiming < R_MIN_antennaSwitchEnaTiming )
			||( R_MAX_antennaSwitchEnaTiming < antennaSwitchEnaTiming ))
		{
			status = R_STATUS_INVALID_PARAMETER;
		}
	}

	/* command execution */
	if ( status == R_STATUS_SUCCESS )
	{
		/* Set Attribute Value */
		status = RfdrvIf_SetAttr( RP_PHY_ANTENNA_SWITCH_ENA_TIMING, sizeof(antennaSwitchEnaTiming), (uint8_t *)&antennaSwitchEnaTiming );
	}

	return( status );
}

/***********************************************************************
 * function name  : R_SetR_gpio0Setting
 * description	  : "Set Parameter Request(SETR)" Command Processing
 * parameters	  : pCmd...pointer to the uart data
 * return value   : status
 **********************************************************************/
static uint8_t R_SetR_gpio0Setting( uint8_t *pCmd )
{
	uint8_t status = R_STATUS_SUCCESS;
	uint8_t res;
	uint8_t gpio0Setting;

	/* get command parameters */
	/* (gpio0Setting) */
	res = AppHexStrToNum( &pCmd, &gpio0Setting, sizeof(gpio0Setting), R_FALSE );
	if ( res != 0 )
	{
		status = R_STATUS_INVALID_COMMAND;
	}

	/* check parameter */
	if ( status == R_STATUS_SUCCESS )
	{
		switch (gpio0Setting)
		{
			case 0x00:
			case 0x01:
				break;
			default:
				status = R_STATUS_INVALID_PARAMETER;
				break;
		}
	}

	/* command execution */
	if ( status == R_STATUS_SUCCESS )
	{
		/* Set Attribute Value */
		status = RfdrvIf_SetAttr( RP_PHY_GPIO0_SETTING, sizeof(gpio0Setting), (uint8_t *)&gpio0Setting );
	}

	return( status );
}

/***********************************************************************
 * function name  : R_SetR_gpio3Setting
 * description	  : "Set Parameter Request(SETR)" Command Processing
 * parameters	  : pCmd...pointer to the uart data
 * return value   : status
 **********************************************************************/
static uint8_t R_SetR_gpio3Setting( uint8_t *pCmd )
{
	uint8_t status = R_STATUS_SUCCESS;
	uint8_t res;
	uint8_t gpio3Setting;

	/* get command parameters */
	/* (gpio1Setting) */
	res = AppHexStrToNum( &pCmd, &gpio3Setting, sizeof(gpio3Setting), R_FALSE );
	if ( res != 0 )
	{
		status = R_STATUS_INVALID_COMMAND;
	}

	/* check parameter */
	if ( status == R_STATUS_SUCCESS )
	{
		switch (gpio3Setting)
		{
			case 0x00:
			case 0x01:
				break;
			default:
				status = R_STATUS_INVALID_PARAMETER;
				break;
		}
	}

	/* command execution */
	if ( status == R_STATUS_SUCCESS )
	{
		/* Set Attribute Value */
		status = RfdrvIf_SetAttr( RP_PHY_GPIO3_SETTING, sizeof(gpio3Setting), (uint8_t *)&gpio3Setting );
	}

	return( status );
}

/***********************************************************************
 * function name  : R_SetR_antennaSelectTx
 * description	  : "Set Parameter Request(SETR)" Command Processing
 * parameters	  : pCmd...pointer to the uart data
 * return value   : status
 **********************************************************************/
static uint8_t R_SetR_antennaSelectTx( uint8_t *pCmd )
{
	uint8_t status = R_STATUS_SUCCESS;
	uint8_t res;
	uint8_t antennaSelectTx;

	/* get command parameters */
	/* (antennaSelectTx) */
	res = AppHexStrToNum( &pCmd, &antennaSelectTx, sizeof(antennaSelectTx), R_FALSE );
	if ( res != 0 )
	{
		status = R_STATUS_INVALID_COMMAND;
	}

	/* check parameter */
	if ( status == R_STATUS_SUCCESS )
	{
		switch (antennaSelectTx)
		{
			case 0x00:
			case 0x01:
				break;
			default:
				status = R_STATUS_INVALID_PARAMETER;
				break;
		}
	}

	/* command execution */
	if ( status == R_STATUS_SUCCESS )
	{
		/* Set Attribute Value */
		status = RfdrvIf_SetAttr( RP_PHY_ANTENNA_SELECT_TX, sizeof(antennaSelectTx), (uint8_t *)&antennaSelectTx );
	}

	return( status );
}

/***********************************************************************
 * function name  : R_SSCMD_ProcessCmd_GETR
 * description	  : "Set Parameter Request(GETR)" Command Processing
 * parameters	  : pCmd...pointer to the uart data
 *				  :   GETR (handle) (paramId)
 * return value   : none
 * output		  : Confirm command
 *				  :   GETC (handle) (status) (rfstate) (paramId) (parameter)
 **********************************************************************/
static void R_SSCMD_ProcessCmd_GETR( uint8_t *pCmd )
{
	uint8_t status = R_STATUS_SUCCESS;
	uint8_t res;
	uint8_t handle;
	uint8_t paramId;
	uint8_t i, size;

	/* get command parameters */
	/* (handle) */
	res = AppHexStrToNum( &pCmd, &handle, sizeof(handle), R_FALSE );
	/* (paramId) */
	res |= AppHexStrToNum( &pCmd, &paramId, sizeof(paramId), R_FALSE );
	if ( res != 0 )
	{
		status = R_STATUS_INVALID_COMMAND;
	}

	/* command execution */
	if ( status == R_STATUS_SUCCESS )
	{
		status = R_STATUS_INVALID_PARAMETER;

		size = sizeof(gc_GetrFunc)/sizeof(gc_GetrFunc[0]);
		for ( i=0; i<size; i++ )
		{
			if ( gc_GetrFunc[i].paramId == paramId )
			{
				gc_GetrFunc[i].pFunc( handle );
				status = R_STATUS_SUCCESS;
				break;
			}
		}
	}

	/* "GETC" command transmission (error) */
	if ( status != R_STATUS_SUCCESS )
	{
		PRINTF( "GETC " );
		PRINTF( "%02X ", handle );
		PRINTF( "%02X ", status );
		PRINTF( "%02X\n", gRfState );
	}
}

/***********************************************************************
 * function name  : R_GetR_channel
 * description	  : "Get Parameter Request(GETR)" Command Processing
 * parameters	  : handle...handle value
 * return value   : none
 * output         : Confirm command
 *                :   GETC (handle) (status) (paramId) (freqBandId) (fskOpeMode) (channel)
 **********************************************************************/
static void R_GetR_channel( uint8_t handle )
{
	uint8_t status;
	uint8_t attrLength = 0;
	uint8_t freqBandId, fskOpeMode, channel;
	uint32_t frequencyOffset;
	uint32_t frequency;

	/* command execution */
	/* Get Attribute Value */
	status = RfdrvIf_GetAttr( RP_PHY_FREQ_BAND_ID, sizeof(freqBandId), &attrLength, (uint8_t *)&freqBandId );
	if ( status == R_STATUS_SUCCESS )
	{
		status = RfdrvIf_GetAttr( RP_PHY_FSK_OPE_MODE, sizeof(fskOpeMode), &attrLength, (uint8_t *)&fskOpeMode );
	}
	if ( status == R_STATUS_SUCCESS )
	{
		status = RfdrvIf_GetAttr( RP_PHY_CURRENT_CHANNEL, sizeof(channel), &attrLength, (uint8_t *)&channel );
	}
	if ( status == R_STATUS_SUCCESS )
	{
		status = RfdrvIf_GetAttr( RP_PHY_FREQUENCY_OFFSET, sizeof(frequencyOffset), &attrLength, (uint8_t *)&frequencyOffset );
	}
	if ( status == R_STATUS_SUCCESS )
	{
		status = RfdrvIf_GetAttr( RP_PHY_FREQUENCY, sizeof(frequency), &attrLength, (uint8_t *)&frequency );
	}

	/* "GETC" command transmission */
	PRINTF( "GETC " );
	PRINTF( "%02X ", handle );
	PRINTF( "%02X ", status );
	PRINTF( "%02X", gRfState );
	if ( status == R_STATUS_SUCCESS )
	{
		PRINTF( " " );
		PRINTF( "%02X ", R_ParamId_channel );
		PRINTF( "%02X ", freqBandId );
		PRINTF( "%02X ", fskOpeMode );
		PRINTF( "%02X", channel );
		PRINTF( " " );
		R_printf_32data( (uint8_t *)&frequencyOffset );
		PRINTF( " " );
		R_printf_32data( (uint8_t *)&frequency );
	}
	PRINTF( "\n" );
}

/***************************************************************************************************************
 * function name  : R_printf_32data
 * description	  : display 32bit data
 * parameters	  : uint8_t *p_data...pointer to data
 * return value   : none
 **************************************************************************************************************/
static void R_printf_32data( uint8_t *p_data )
{
	uint16_t i;
	uint16_t size = sizeof(uint32_t);

	p_data += size;
	for ( i=0; i<size; i++ )
	{
		PRINTF( "%02X", *(--p_data) );
	}
}

/***********************************************************************
 * function name  : R_GetR_transmitPower
 * description	  : "Get Parameter Request(GETR)" Command Processing
 * parameters	  : handle...handle value
 * return value   : none
 * output         : Confirm command
 *                :   GETC (handle) (status) (paramId) (freqBandId) (transmitPower)
 **********************************************************************/
static void R_GetR_transmitPower( uint8_t handle )
{
	uint8_t status;
	uint8_t attrLength = 0;
	uint8_t freqBandId, transmitPower;

	/* command execution */
	/* Get Attribute Value */
	status = RfdrvIf_GetAttr( RP_PHY_FREQ_BAND_ID, sizeof(freqBandId), &attrLength, (uint8_t *)&freqBandId );
	if ( status == R_STATUS_SUCCESS )
	{
		status = RfdrvIf_GetAttr( RP_PHY_TRANSMIT_POWER, sizeof(transmitPower), &attrLength, (uint8_t *)&transmitPower );
	}

	/* "GETC" command transmission */
	PRINTF( "GETC " );
	PRINTF( "%02X ", handle );
	PRINTF( "%02X ", status );
	PRINTF( "%02X", gRfState );
	if ( status == R_STATUS_SUCCESS )
	{
		PRINTF( " " );
		PRINTF( "%02X ", R_ParamId_transmitPower );
		PRINTF( "%02X ", freqBandId );
		PRINTF( "%02X", transmitPower );
	}
	PRINTF( "\n" );
}

/***********************************************************************
 * function name  : R_GetR_preambleLength
 * description	  : "Get Parameter Request(GETR)" Command Processing
 * parameters	  : handle...handle value
 * return value   : none
 * output         : Confirm command
 *                :   GETC (handle) (status) (paramId) (preambleLength)
 **********************************************************************/
static void R_GetR_preambleLength( uint8_t handle )
{
	uint8_t status;
	uint8_t attrLength = 0;
	uint16_t preambleLength;

	/* command execution */
	/* Get Attribute Value */
	status = RfdrvIf_GetAttr( RP_PHY_FSK_PREAMBLE_LENGTH, sizeof(preambleLength), &attrLength, (uint8_t *)&preambleLength );

	/* "GETC" command transmission */
	PRINTF( "GETC " );
	PRINTF( "%02X ", handle );
	PRINTF( "%02X ", status );
	PRINTF( "%02X", gRfState );
	if ( status == R_STATUS_SUCCESS )
	{
		PRINTF( " " );
		PRINTF( "%02X ", R_ParamId_preambleLength );
		PRINTF( "%04X", preambleLength );
	}
	PRINTF( "\n" );
}

/***********************************************************************
 * function name  : R_GetR_agcWaitGainOffset
 * description	  : "Get Parameter Request(GETR)" Command Processing
 * parameters	  : handle...handle value
 * return value   : none
 * output         : Confirm command
 *                :   GETC (handle) (status) (paramId) (agcWaitGainOffset)
 **********************************************************************/
static void R_GetR_agcWaitGainOffset( uint8_t handle )
{
	uint8_t status;
	uint8_t attrLength = 0;
	uint8_t agcWaitGainOffset;

	/* command execution */
	/* Get Attribute Value */
	status = RfdrvIf_GetAttr( RP_PHY_AGC_WAIT_GAIN_OFFSET, sizeof(agcWaitGainOffset), &attrLength, (uint8_t *)&agcWaitGainOffset );

	/* "GETC" command transmission */
	PRINTF( "GETC " );
	PRINTF( "%02X ", handle );
	PRINTF( "%02X ", status );
	PRINTF( "%02X", gRfState );
	if ( status == R_STATUS_SUCCESS )
	{
		PRINTF( " " );
		PRINTF( "%02X ", R_ParamId_agcWaitGainOffset );
		PRINTF( "%02X", agcWaitGainOffset );
	}
	PRINTF( "\n" );
}

/***********************************************************************
 * function name  : R_GetR_antennaSwitchEna
 * description	  : "Get Parameter Request(GETR)" Command Processing
 * parameters	  : handle...handle value
 * return value   : none
 * output         : Confirm command
 *                :   GETC (handle) (status) (paramId) (antennaSwitchEna)
 **********************************************************************/
static void R_GetR_antennaSwitchEna( uint8_t handle )
{
	uint8_t status;
	uint8_t attrLength = 0;
	uint8_t antennaSwitchEna;

	/* command execution */
	/* Get Attribute Value */
	status = RfdrvIf_GetAttr( RP_PHY_ANTENNA_SWITCH_ENA, sizeof(antennaSwitchEna), &attrLength, (uint8_t *)&antennaSwitchEna );

	/* "GETC" command transmission */
	PRINTF( "GETC " );
	PRINTF( "%02X ", handle );
	PRINTF( "%02X ", status );
	PRINTF( "%02X", gRfState );
	if ( status == R_STATUS_SUCCESS )
	{
		PRINTF( " " );
		PRINTF( "%02X ", R_ParamId_antennaSwitchEna );
		PRINTF( "%02X", antennaSwitchEna );
	}
	PRINTF( "\n" );
}

/***********************************************************************
 * function name  : R_GetR_antennaSwitchEnaTiming
 * description	  : "Get Parameter Request(GETR)" Command Processing
 * parameters	  : handle...handle value
 * return value   : none
 * output         : Confirm command
 *                :   GETC (handle) (status) (paramId) (antennaSwitchEnaTiming)
 **********************************************************************/
static void R_GetR_antennaSwitchEnaTiming( uint8_t handle )
{
	uint8_t status;
	uint8_t attrLength = 0;
	uint16_t antennaSwitchEnaTiming;

	/* command execution */
	/* Get Attribute Value */
	status = RfdrvIf_GetAttr( RP_PHY_ANTENNA_SWITCH_ENA_TIMING, sizeof(antennaSwitchEnaTiming), &attrLength, (uint8_t *)&antennaSwitchEnaTiming );

	/* "GETC" command transmission */
	PRINTF( "GETC " );
	PRINTF( "%02X ", handle );
	PRINTF( "%02X ", status );
	PRINTF( "%02X", gRfState );
	if ( status == R_STATUS_SUCCESS )
	{
		PRINTF( " " );
		PRINTF( "%02X ", R_ParamId_antennaSwitchEnaTiming );
		PRINTF( "%04X", antennaSwitchEnaTiming );
	}
	PRINTF( "\n" );
}

/***********************************************************************
 * function name  : R_GetR_gpio0Setting
 * description	  : "Get Parameter Request(GETR)" Command Processing
 * parameters	  : handle...handle value
 * return value   : none
 * output         : Confirm command
 *                :   GETC (handle) (status) (paramId) (gpio0Setting)
 **********************************************************************/
static void R_GetR_gpio0Setting( uint8_t handle )
{
	uint8_t status;
	uint8_t attrLength = 0;
	uint8_t gpio0Setting;

	/* command execution */
	/* Get Attribute Value */
	status = RfdrvIf_GetAttr( RP_PHY_GPIO0_SETTING, sizeof(gpio0Setting), &attrLength, (uint8_t *)&gpio0Setting );

	/* "GETC" command transmission */
	PRINTF( "GETC " );
	PRINTF( "%02X ", handle );
	PRINTF( "%02X ", status );
	PRINTF( "%02X", gRfState );
	if ( status == R_STATUS_SUCCESS )
	{
		PRINTF( " " );
		PRINTF( "%02X ", R_ParamId_gpio0Setting );
		PRINTF( "%02X", gpio0Setting );
	}
	PRINTF( "\n" );
}

/***********************************************************************
 * function name  : R_GetR_gpio3Setting
 * description	  : "Get Parameter Request(GETR)" Command Processing
 * parameters	  : handle...handle value
 * return value   : none
 * output         : Confirm command
 *                :   GETC (handle) (status) (paramId) (gpio3Setting)
 **********************************************************************/
static void R_GetR_gpio3Setting( uint8_t handle )
{
	uint8_t status;
	uint8_t attrLength = 0;
	uint8_t gpio3Setting;

	/* command execution */
	/* Get Attribute Value */
	status = RfdrvIf_GetAttr( RP_PHY_GPIO3_SETTING, sizeof(gpio3Setting), &attrLength, (uint8_t *)&gpio3Setting );

	/* "GETC" command transmission */
	PRINTF( "GETC " );
	PRINTF( "%02X ", handle );
	PRINTF( "%02X ", status );
	PRINTF( "%02X", gRfState );
	if ( status == R_STATUS_SUCCESS )
	{
		PRINTF( " " );
		PRINTF( "%02X ", R_ParamId_gpio3Setting );
		PRINTF( "%02X", gpio3Setting );
	}
	PRINTF( "\n" );
}

/***********************************************************************
 * function name  : R_GetR_antennaSelectTx
 * description	  : "Get Parameter Request(GETR)" Command Processing
 * parameters	  : handle...handle value
 * return value   : none
 * output         : Confirm command
 *                :   GETC (handle) (status) (paramId) (antennaSelectTx)
 **********************************************************************/
static void R_GetR_antennaSelectTx( uint8_t handle )
{
	uint8_t status;
	uint8_t attrLength = 0;
	uint8_t antennaSelectTx;

	/* command execution */
	/* Get Attribute Value */
	status = RfdrvIf_GetAttr( RP_PHY_ANTENNA_SELECT_TX, sizeof(antennaSelectTx), &attrLength, (uint8_t *)&antennaSelectTx );

	/* "GETC" command transmission */
	PRINTF( "GETC " );
	PRINTF( "%02X ", handle );
	PRINTF( "%02X ", status );
	PRINTF( "%02X", gRfState );
	if ( status == R_STATUS_SUCCESS )
	{
		PRINTF( " " );
		PRINTF( "%02X ", R_ParamId_antennaSelectTx );
		PRINTF( "%02X", antennaSelectTx );
	}
	PRINTF( "\n" );
}

/***********************************************************************
 * function name  : R_SSCMD_ProcessCmd_SIBR
 * description	  : "Set IB Value Request(SIBR)" Command Processing
 * parameters	  : pCmd...pointer to the uart data
 *				  :   SIBR (handle) (attrId) (attrLength) (attrValue)
 * return value   : none
 * output		  : Confirm command
 *				  :   SIBC (handle) (status) (rfstate) (attrId)
 **********************************************************************/
static void R_SSCMD_ProcessCmd_SIBR( uint8_t *pCmd )
{
	uint8_t status = R_STATUS_SUCCESS;
	uint8_t res;
	uint8_t handle;
	uint8_t attrId;
	uint8_t attrLength;
	r_rfdrvIf_attr_t attrValue;

	/* get command parameters */
	/* (handle) */
	res = AppHexStrToNum( &pCmd, &handle, sizeof(handle), R_FALSE );
	/* (attrId) */
	res |= AppHexStrToNum( &pCmd, &attrId, sizeof(attrId), R_FALSE );
	/* (attrLength) */
	res |= AppHexStrToNum( &pCmd, &attrLength, sizeof(attrLength), R_FALSE );
	if ( res == 0 )
	{
		if ( attrLength > sizeof(attrValue) )
		{
			status = R_STATUS_INVALID_COMMAND_PARAMETER;
		}
		else
		{
			/* (attrValue) */
			res = AppHexStrToNum( &pCmd, &attrValue, attrLength, R_FALSE );
			if ( res != 0 )
			{
				status = R_STATUS_INVALID_COMMAND;
			}
		}
	}
	else
	{
		status = R_STATUS_INVALID_COMMAND;
	}

	/* command execution */
	if ( status == R_STATUS_SUCCESS )
	{
		/* Set Attribute Value */
		status = RfdrvIf_SetAttr( attrId, attrLength, (uint8_t *)&attrValue );
	}

	/* "SIBC" command transmission */
	PRINTF( "SIBC " );
	PRINTF( "%02X ", handle );
	PRINTF( "%02X ", status );
	PRINTF( "%02X", gRfState );
	if ( status == R_STATUS_SUCCESS )
	{
		PRINTF( " " );
		PRINTF( "%02X", attrId );
	}
	PRINTF( "\n" );
}

/***********************************************************************
 * function name  : R_SSCMD_ProcessCmd_GIBR
 * description	  : "Get IB Value Request(GIBR)" Command Processing
 * parameters	  : pCmd...pointer to the uart data
 *				  :   GIBR (handle) (attrId)
 * return value   : none
 * output		  : Confirm command
 *				  :   GIBC (handle) (status) (rfstate)
 *				  : 	    (attrId) (attrLength) (attrValue)
 **********************************************************************/
static void R_SSCMD_ProcessCmd_GIBR( uint8_t *pCmd )
{
	uint8_t status = R_STATUS_SUCCESS;
	uint8_t res;
	uint8_t handle;
	uint8_t attrId;
	uint8_t attrLength = 0;
	r_rfdrvIf_attr_t attrValue;
	uint8_t *pOffset;
	uint16_t i;

	/* get command parameters */
	/* (handle) */
	res = AppHexStrToNum( &pCmd, &handle, sizeof(handle), R_FALSE );
	/* (attrId) */
	res |= AppHexStrToNum( &pCmd, &attrId, sizeof(attrId), R_FALSE );
	if ( res != 0 )
	{
		status = R_STATUS_INVALID_COMMAND;
	}

	/* command execution */
	if ( status == R_STATUS_SUCCESS )
	{
		/* Get Attribute Value */
		status = RfdrvIf_GetAttr( attrId, sizeof(attrValue), &attrLength, (uint8_t *)&attrValue );
	}

	/* "GIBC" command transmission */
	PRINTF( "GIBC " );
	PRINTF( "%02X ", handle );
	PRINTF( "%02X ", status );
	PRINTF( "%02X", gRfState );
	if ( status == R_STATUS_SUCCESS )
	{
		PRINTF( " " );
		PRINTF( "%02X ", attrId );
		PRINTF( "%02X ", attrLength );
		pOffset = &attrValue.phyCurrentChannel + attrLength - 1;
		for ( i=0; i<attrLength; i++ )
		{
			PRINTF( "%02X", *pOffset-- );
		}
	}
	PRINTF( "\n" );
}

/***********************************************************************
 * function name  : R_SSCMD_ProcessCmd_CERRC
 * description    : Process "CEERC" command
 * parameters     : status...command status
 * return value   : none
 * output         : Confirm command
 *                :   CEERC (handle) (status) (rfstate)
 **********************************************************************/
static void R_SSCMD_ProcessCmd_CERRC( uint8_t status )
{
	uint8_t handle = 0;

	/* "CERRC" command transmission */
	PRINTF( "CERRC " );
	PRINTF( "%02X ", handle );
	PRINTF( "%02X ", status );
	PRINTF( "%02X\n", gRfState );
}

/*******************************************************************************
 * "Rfdriver API wrapping function"
 ******************************************************************************/
/***********************************************************************
 * function name  : RfdrvIf_Start
 * description    : RF driver initialization
 * parameters     : none
 * return value   : retStatus
 **********************************************************************/
uint8_t RfdrvIf_Start( void )
{
	uint8_t retStatus;
	int16_t phyStatus;
	uint8_t phySfdDetectionExtend;

	/* rfdriver API */
	phyStatus = RpInit( RfdrvIf_PdDataIndCallback, RfdrvIf_PdDataCfmCallback,
						RfdrvIf_PlmeCcaCfmCallback, RfdrvIf_PlmeEdCfmCallback,
						RfdrvIf_RxOffIndCallback, RfdrvIf_FatalErrorIndCallback,
						RfdrvIf_WarningIndCallback, RpCalcLqiCallback, RP_NULL );

	/* convert phyStatus */
	switch ( phyStatus )
	{
		case RP_SUCCESS:
			retStatus = R_STATUS_SUCCESS;
			break;
		case RP_INVALID_PARAMETER:
			retStatus = R_STATUS_INVALID_PARAMETER;
			break;
		default:
			retStatus = R_STATUS_ERROR;
			break;
	}

	/* The pattern length of SFD is setting in the 2FSK/2GFSK reception. */
	if ( retStatus == R_STATUS_SUCCESS )
	{
		phySfdDetectionExtend = 0x01;	// 0x01:Received using SFD of 4 bytes.
		RpSetPibReq(RP_PHY_SFD_DETECTION_EXTEND, sizeof(phySfdDetectionExtend), &phySfdDetectionExtend);
	}

	/* init message buffer */
	RfdrvIf_InitMsgBuff();

	gNtfHandle = 0;

	/* init state */
	gRfState = R_RFSTATE_IDLE;

	return( retStatus );
}

/***********************************************************************
 * function name  : RfdrvIf_Reset
 * description    : RF driver reset
 * parameters     : none
 * return value   : none
 **********************************************************************/
static void RfdrvIf_Reset( void )
{
	uint8_t phySfdDetectionExtend;

	/* rfdriver API */
	RpResetReq();

	/* The pattern length of SFD is setting in the 2FSK/2GFSK reception. */
	phySfdDetectionExtend = 0x01;	// 0x01:Received using SFD of 4 bytes.
	RpSetPibReq(RP_PHY_SFD_DETECTION_EXTEND, sizeof(phySfdDetectionExtend), &phySfdDetectionExtend);

	/* init message buffer */
	RfdrvIf_InitMsgBuff();

	gNtfHandle = 0;

	/* change current state */
	gRfState = R_RFSTATE_IDLE;
}

/***********************************************************************
 * function name  : RfdrvIf_SetRxOff
 * description    : Idle setting request
 * parameters     : none
 * return value   : retStatus
 **********************************************************************/
static uint8_t RfdrvIf_SetRxOff( void )
{
	uint8_t retStatus;
	int16_t phyStatus;

	/* check current state */
	if ( gRfState == R_RFSTATE_CONTINUOUS )
	{
		retStatus = R_STATUS_ILLEGAL_REQUEST;
		return( retStatus );
	}

	/* rfdriver API */
	phyStatus = RpSetRxOffReq();

	/* convert phyStatus */
	switch ( phyStatus )
	{
		case RP_SUCCESS:
			retStatus = R_STATUS_SUCCESS;
			break;
		case RP_BUSY_LOWPOWER:
			retStatus = R_STATUS_BUSY_LOWPOWER;
			break;
		default:
			retStatus = R_STATUS_ERROR;
			break;
	}

	/* change current state */
	if ( retStatus == R_STATUS_SUCCESS )
	{
		gRfState = R_RFSTATE_IDLE;
	}

	return( retStatus );
}

/***********************************************************************
 * function name  : RfdrvIf_SetRxOn
 * description    : Reception on setting request
 * parameters     : none
 * return value   : retStatus
 **********************************************************************/
static uint8_t RfdrvIf_SetRxOn( void )
{
	uint8_t retStatus;
	int16_t phyStatus;

	/* check current state */
	if ( gRfState == R_RFSTATE_CONTINUOUS )
	{
		retStatus = R_STATUS_ILLEGAL_REQUEST;
		return( retStatus );
	}

	/* rfdriver API */
	RpSetRxOffReq();

	/* rfdriver API */
	phyStatus = RpSetRxOnReq( (uint8_t)RP_RX_AUTO_RCV, (uint32_t)0 );

	/* convert phyStatus */
	switch ( phyStatus )
	{
		case RP_SUCCESS:
		case RP_RX_ON:
			retStatus = R_STATUS_SUCCESS;
			break;
		case RP_INVALID_PARAMETER:
			retStatus = R_STATUS_INVALID_PARAMETER;
			break;
		case RP_BUSY_RX:
			retStatus = R_STATUS_BUSY_RX;
			break;
		case RP_BUSY_TX:
			retStatus = R_STATUS_BUSY_TX;
			break;
		case RP_BUSY_LOWPOWER:
			retStatus = R_STATUS_BUSY_LOWPOWER;
			break;
		case RP_TRX_OFF:
			retStatus = R_STATUS_TRX_OFF;
			break;
		default:
			retStatus = R_STATUS_ERROR;
			break;
	}

	/* change current state */
	if ( retStatus == R_STATUS_SUCCESS )
	{
		gRfState = R_RFSTATE_RXAUTO;
	}

	return( retStatus );
}

/***********************************************************************
 * function name  : RfdrvIf_SendFrame
 * description    : Frame transmission request
 * parameters     : none
 * return value   : retStatus
 **********************************************************************/
static uint8_t RfdrvIf_SendFrame( void )
{
	uint8_t retStatus;
	int16_t phyStatus;
	uint16_t psduLength = (uint16_t)R_TEST_DATA_LENGTH;
	uint8_t psdu[R_TEST_DATA_LENGTH];
	uint16_t i;
	r_rfdrvIf_msg_pddatacfm_t *pCfm = &gCbMsgPdDataCfm;

	/* check current state */
	if ( gRfState == R_RFSTATE_CONTINUOUS )
	{
		retStatus = R_STATUS_ILLEGAL_REQUEST;
		return( retStatus );
	}

	/* rfdriver API */
	RpSetRxOffReq();

	/* clear event flag */
	pCfm->eventFlag = R_FALSE;

	/* set test data */
	for( i=0; i<psduLength; i++ )
	{
		psdu[i] = i;
	}

	/* rfdriver API */
	phyStatus = RpPdDataReq( &(psdu[0]), psduLength, (uint8_t)0, (uint32_t)0 );

	/* convert phyStatus */
	switch ( phyStatus )
	{
		case RP_PENDING:
			retStatus = R_STATUS_SUCCESS;
			break;
		case RP_INVALID_PARAMETER:
			retStatus = R_STATUS_INVALID_PARAMETER;
			break;
		case RP_BUSY_RX:
			retStatus = R_STATUS_BUSY_RX;
			break;
		case RP_BUSY_TX:
			retStatus = R_STATUS_BUSY_TX;
			break;
		case RP_BUSY_LOWPOWER:
			retStatus = R_STATUS_BUSY_LOWPOWER;
			break;
		case RP_INVALID_API_OPTION:
			retStatus = R_STATUS_INVALID_API_OPTION;
			break;
		default:
			retStatus = R_STATUS_ERROR;
			break;
	}

	if ( retStatus == R_STATUS_SUCCESS )
	{
		/* wait for processing completion */
		do {
		} while ( pCfm->eventFlag == R_FALSE );

		/* frame transmission complete status */
		retStatus = pCfm->status;
	}

	/* return to reception on */
	if ( gRfState == R_RFSTATE_RXAUTO )
	{
		/* rfdriver API */
		RpSetRxOnReq( (uint8_t)RP_RX_AUTO_RCV, (uint32_t)0 );
	}

	return( retStatus );
}

/***********************************************************************
 * function name  : RfdrvIf_SetAttr
 * description    : Attribute value setting function
 * parameters     : attrId...Attribute ID
 *                : attrLength...Size of the attribute value to be set
 *                : p_attrValue...Pointer to the beginning of the area 
 *                :               storing the attribute value to be set
 * return value   : retStatus
 **********************************************************************/
static uint8_t RfdrvIf_SetAttr( uint8_t attrId, uint8_t attrLength,
								uint8_t RP_FAR *p_attrValue )
{
	uint8_t retStatus;
	int16_t phyStatus;

	/* check current state */
	if ( gRfState == R_RFSTATE_CONTINUOUS )
	{
		retStatus = R_STATUS_ILLEGAL_REQUEST;
		return( retStatus );
	}

	/* null check */
	if ( p_attrValue == NULL )
	{
		retStatus = R_STATUS_INVALID_PARAMETER;
		return( retStatus );
	}

	/* rfdriver API */
	RpSetRxOffReq();

	/* rfdriver API */
	phyStatus = RpSetPibReq( attrId, attrLength, p_attrValue );

	/* convert phyStatus */
	switch ( phyStatus )
	{
		case RP_SUCCESS:
			retStatus = R_STATUS_SUCCESS;
			break;
		case RP_UNSUPPORTED_ATTRIBUTE:
			retStatus = R_STATUS_UNSUPPORTED_ATTRIBUTE;
			break;
		case RP_INVALID_PARAMETER:
			retStatus = R_STATUS_INVALID_PARAMETER;
			break;
		case RP_BUSY_RX:
			retStatus = R_STATUS_BUSY_RX;
			break;
		case RP_BUSY_TX:
			retStatus = R_STATUS_BUSY_TX;
			break;
		case RP_BUSY_LOWPOWER:
			retStatus = R_STATUS_BUSY_LOWPOWER;
			break;
		default:
			retStatus = R_STATUS_ERROR;
			break;
	}

	/* return to reception on */
	if ( gRfState == R_RFSTATE_RXAUTO )
	{
		/* rfdriver API */
		RpSetRxOnReq( (uint8_t)RP_RX_AUTO_RCV, (uint32_t)0 );
	}

	return( retStatus );
}

/***********************************************************************
 * function name  : RfdrvIf_GetAttr
 * description    : Attribute value getting function
 * parameters     : attrId...Attribute ID
 *                : attrValueLength...Attribute value Length
 *                : p_attrLength...Attribute value Length(out)
 *                : p_attrValue...Attribute value(out)
 * return value   : retStatus
 **********************************************************************/
static uint8_t RfdrvIf_GetAttr( uint8_t attrId, uint8_t attrValueLength,
						uint8_t *p_attrLength, uint8_t *p_attrValue )
{
	uint8_t retStatus;
	int16_t phyStatus;

	/* check current state */
	if ( gRfState == R_RFSTATE_CONTINUOUS )
	{
		retStatus = R_STATUS_ILLEGAL_REQUEST;
		return( retStatus );
	}

	/* null check */
	if (( p_attrLength == NULL )
		||( p_attrValue == NULL ))
	{
		retStatus = R_STATUS_INVALID_PARAMETER;
		return( retStatus );
	}

	/* get attribute value length */
	*p_attrLength = RfdrvIf_GetAttrLength( attrId );

	/* length check of the area storing attribute value */
	if ( attrValueLength < *p_attrLength )
	{
		retStatus = R_STATUS_INVALID_PARAMETER;
		return( retStatus );
	}

	/* rfdriver API */
	RpSetRxOffReq();

	/* rfdriver API */
	phyStatus = RpGetPibReq( attrId, *p_attrLength, p_attrValue );

	/* convert phyStatus */
	switch ( phyStatus )
	{
		case RP_SUCCESS:
			retStatus = R_STATUS_SUCCESS;
			break;
		case RP_UNSUPPORTED_ATTRIBUTE:
			retStatus = R_STATUS_UNSUPPORTED_ATTRIBUTE;
			break;
		case RP_INVALID_PARAMETER:
			retStatus = R_STATUS_INVALID_PARAMETER;
			break;
		case RP_BUSY_RX:
			retStatus = R_STATUS_BUSY_RX;
			break;
		case RP_BUSY_TX:
			retStatus = R_STATUS_BUSY_TX;
			break;
		case RP_BUSY_LOWPOWER:
			retStatus = R_STATUS_BUSY_LOWPOWER;
			break;
		default:
			retStatus = R_STATUS_ERROR;
			break;
	}

	/* clear attribute value length */
	if ( retStatus != R_STATUS_SUCCESS )
	{
		*p_attrLength = 0;
	}

	/* return to reception on */
	if ( gRfState == R_RFSTATE_RXAUTO )
	{
		/* rfdriver API */
		RpSetRxOnReq( (uint8_t)RP_RX_AUTO_RCV, (uint32_t)0 );
	}

	return( retStatus );
}

/***********************************************************************
 * function name  : RfdrvIf_GetAttrLength
 * description    : get attribute length
 * parameters     : attrId...attribute ID
 * return value   : attribute length
 **********************************************************************/
static uint8_t RfdrvIf_GetAttrLength( uint8_t attrId )
{
	uint8_t attrLength;

	switch ( attrId )
	{
		case RP_PHY_RMODE_TCUM_LIMIT:
		case RP_PHY_RMODE_TCUM:
		case RP_PHY_FREQUENCY:
		case RP_PHY_FREQUENCY_OFFSET:
			attrLength = sizeof(uint32_t);
			break;
		case RP_PHY_CCA_VTH:
		case RP_PHY_LVLFLTR_VTH:
		case RP_MAC_SHORT_ADDRESS1:
		case RP_MAC_PANID1:
		case RP_MAC_SHORT_ADDRESS2:
		case RP_MAC_PANID2:
		case RP_PHY_CSMA_BACKOFF_PERIOD:
		case RP_PHY_CCA_DURATION:
		case RP_PHY_FSK_PREAMBLE_LENGTH:
		case RP_PHY_ACK_REPLY_TIME:
		case RP_PHY_ACK_WAIT_DURATION:
		case RP_PHY_DATA_RATE:
		case RP_PHY_AGC_START_VTH:
		case RP_PHY_ANTENNA_DIVERSITY_START_VTH:
		case RP_PHY_ANTENNA_SWITCHING_TIME:
		case RP_PHY_ANTENNA_SWITCH_ENA_TIMING:
		case RP_PHY_RMODE_TON_MAX:
		case RP_PHY_RMODE_TOFF_MIN:
		case RP_PHY_RMODE_TCUM_SMP_PERIOD:
			attrLength = sizeof(uint16_t);
			break;
		case RP_PHY_CHANNELS_SUPPORTED:
		case RP_MAC_EXTENDED_ADDRESS1:
		case RP_MAC_EXTENDED_ADDRESS2:
			attrLength = 8;
			break;
		default:
			attrLength = sizeof(uint8_t);
			break;
	}

	return( attrLength );
}

/***********************************************************************
 * function name  : RfdrvIf_SetAttrChannel
 * description    : 
 * parameters     : freqBandId...
 *				  : fskOpeMode...
 *				  : setChannel...
 * return value   : retStatus
 **********************************************************************/
static uint8_t RfdrvIf_SetAttrChannel( uint8_t freqBandId, uint8_t fskOpeMode,
										uint8_t setChannel, int32_t frequencyOffset )
{
	uint8_t retStatus = R_STATUS_SUCCESS;
	uint8_t indexFreqBandId, indexFskOpeMode;
	uint8_t maxFskOpeMode, maxChannel;
	uint8_t maskChSupported;

	/* range check phyFreqBandId */
	if (( RP_PHY_FREQ_BAND_863MHz <= freqBandId )
		&&( freqBandId <= RP_PHY_FREQ_BAND_920MHz ))
	{
		indexFreqBandId = freqBandId - 4;
	}
	else if (( RP_PHY_FREQ_BAND_920MHz_Others <= freqBandId )
		&&( freqBandId <= RP_PHY_FREQ_BAND_921MHz ))
	{
		indexFreqBandId = freqBandId - 8;
	}
	else
	{
		#ifdef R_RFDRVIF_LOG_REASON
		PRINTF( "RangeError FreqBandId\n" );
		#endif // #ifdef R_RFDRVIF_LOG_REASON
		retStatus = R_STATUS_INVALID_PARAMETER;
		return( retStatus );
	}

	/* set Attribute phyFreqBandId Value */
	retStatus = RfdrvIf_SetAttr( RP_PHY_FREQ_BAND_ID, sizeof(freqBandId), (uint8_t *)&freqBandId );
	if ( retStatus != R_STATUS_SUCCESS )
	{
		#ifdef R_RFDRVIF_LOG_REASON
		PRINTF( "SetAttrError FreqBandId\n" );
		#endif // #ifdef R_RFDRVIF_LOG_REASON
		return( retStatus );
	}

	/* range check phyFskOpeMode */
	maxFskOpeMode = g_MaxFskOpeModeTbl[indexFreqBandId];
	if (( fskOpeMode == 0 )
		||( maxFskOpeMode < fskOpeMode ))
	{
		#ifdef R_RFDRVIF_LOG_REASON
		PRINTF( "RangeError FskOpeMode\n" );
		#endif // #ifdef R_RFDRVIF_LOG_REASON
		retStatus = R_STATUS_INVALID_PARAMETER;
		return( retStatus );
	}

	/* Set Attribute phyFskOpeMode Value */
	retStatus = RfdrvIf_SetAttr( RP_PHY_FSK_OPE_MODE, sizeof(fskOpeMode), (uint8_t *)&fskOpeMode );
	if ( retStatus != R_STATUS_SUCCESS )
	{
		#ifdef R_RFDRVIF_LOG_REASON
		PRINTF( "SetAttrError FskOpeMode\n" );
		#endif // #ifdef R_RFDRVIF_LOG_REASON
		return( retStatus );
	}

	/* range check phyCurrentChannel */
	indexFskOpeMode = fskOpeMode - 1;
	maxChannel = g_MaxChannelTbl[indexFreqBandId].maxChannel[indexFskOpeMode];
	if ( maxChannel < setChannel )
	{
		#ifdef R_RFDRVIF_LOG_REASON
		PRINTF( "RangeError maxChannel\n" );
		#endif // #ifdef R_RFDRVIF_LOG_REASON
		retStatus = R_STATUS_INVALID_PARAMETER;
		return( retStatus );
	}

	/* check support phyCurrentChannel */
	maskChSupported = R_FALSE;
	if (( freqBandId == RP_PHY_FREQ_BAND_920MHz_Others )
		&&( fskOpeMode == 1 ))
	{
		maskChSupported = R_TRUE;
	}
	retStatus = RfdrvIf_CheckChannelsSupported( setChannel, maskChSupported );
	if ( retStatus != R_STATUS_SUCCESS )
	{
		#ifdef R_RFDRVIF_LOG_REASON
		PRINTF( "Non-Support Channel\n" );
		#endif // #ifdef R_RFDRVIF_LOG_REASON
		return( retStatus );
	}

	/* set Attribute phyFrequencyOffset Value */
	retStatus = RfdrvIf_SetAttr( RP_PHY_FREQUENCY_OFFSET, sizeof(frequencyOffset), (uint8_t *)&frequencyOffset );
	if ( retStatus != R_STATUS_SUCCESS )
	{
		return( retStatus );
	}

	/* set Attribute phyCurrentChannel Value */
	retStatus = RfdrvIf_SetAttr( RP_PHY_CURRENT_CHANNEL, sizeof(setChannel), (uint8_t *)&setChannel );
	if ( retStatus != R_STATUS_SUCCESS )
	{
		#ifdef R_RFDRVIF_LOG_REASON
		PRINTF( "SetAttrError Channel\n" );
		#endif // #ifdef R_RFDRVIF_LOG_REASON
		return( retStatus );
	}

	return( retStatus );
}

/***********************************************************************
 * function name  : RfdrvIf_CheckChannelsSupported
 * description    : 
 * parameters     : channel..
 * return value   : retStatus
 **********************************************************************/
static uint8_t RfdrvIf_CheckChannelsSupported( uint8_t channel, uint8_t maskChSupported )
{
	uint8_t retStatus;
	uint8_t attrLength, channel8bit;
	uint32_t channelsSupported[2];
	uint32_t channel32bit;
	uint8_t channelsSupportedPage;

	/* set Attribute phyChannelsSupportedPage */
	channelsSupportedPage = RfdrvIf_ConvertChannelsSupportedPage( channel );
	retStatus = RfdrvIf_SetAttr( RP_PHY_CHANNELS_SUPPORTED_PAGE, sizeof(channelsSupportedPage), (uint8_t *)&channelsSupportedPage );
	if ( retStatus != R_STATUS_SUCCESS )
	{
		return( retStatus );
	}

	/* get Attribute phyChannelsSupported */
	retStatus = RfdrvIf_GetAttr( RP_PHY_CHANNELS_SUPPORTED, 8, &attrLength, (uint8_t *)&(channelsSupported[0]) );
	if (( maskChSupported )&&( channelsSupportedPage == 0 ))
	{
		channelsSupported[0] |= 0x00000100;
	}
	if ( retStatus != R_STATUS_SUCCESS )
	{
		return( retStatus );
	}

	channel8bit = channel - (channelsSupportedPage * 64);

	/* ChannelNumber bit 0-31 */
	if ( channel8bit < 32 )
	{
		channel32bit = (uint32_t)1 << channel8bit;
		channelsSupported[0] &= channel32bit;
		if ( channelsSupported[0] == 0 )
		{
			retStatus = R_STATUS_INVALID_PARAMETER;
		}
	}
	/* ChannelNumber bit 32-63 */
	else
	{
		channel8bit -= 32;
		channel32bit = (uint32_t)1 << channel8bit;
		channelsSupported[1] &= channel32bit;
		if ( channelsSupported[1] == 0 )
		{
			retStatus = R_STATUS_INVALID_PARAMETER;
		}
	}

	return( retStatus );
}

/***********************************************************************
 * function name  : RfdrvIf_ConvertChannelsSupportedPage
 * description    : 
 * parameters     : channel..
 * return value   : retStatus
 **********************************************************************/
static uint8_t RfdrvIf_ConvertChannelsSupportedPage( uint8_t channel )
{
	uint8_t channelsSupportedPage;

	/* channel 0-63 */
	if ( channel < 64 )
	{
		channelsSupportedPage = 0x00;
	}
	else
	{
		/* channel 64-127 */
		if ( channel < 128 )
		{
			channelsSupportedPage = 0x01;
		}
		else
		{
			/* channel 128-191 */
			if ( channel < 192 )
			{
				channelsSupportedPage = 0x02;
			}
			/* channel 192-255 */
			else
			{
				channelsSupportedPage = 0x03;
			}
		}
	}

	return( channelsSupportedPage );
}

/***********************************************************************
 * function name  : RfdrvIf_SetAttrTxPower
 * description    : 
 * parameters     : freqBandId...
 *				  : txPower...
 * return value   : retStatus
 **********************************************************************/
static uint8_t RfdrvIf_SetAttrTxPower( uint8_t freqBandId, uint8_t transmitPower )
{
	uint8_t retStatus = R_STATUS_SUCCESS;
	uint8_t maxGainSet;

	/* range check phyFreqBandId */
	switch ( freqBandId )
	{
		case RP_PHY_FREQ_BAND_920MHz:			// (9) 920 - 928 (Japan)
		case RP_PHY_FREQ_BAND_920MHz_Others:	// (14)	920 - 928 (Japan) other settings
		case RP_PHY_FREQ_BAND_921MHz:			// (17)	921 - xxx (CH)
			maxGainSet = 101;
			break;
		case RP_PHY_FREQ_BAND_863MHz:			// (4) 863 - 870 (Europe)
		case RP_PHY_FREQ_BAND_870MHz:			// (15)	870 - xxx (Europe2)
			maxGainSet = 102;
			break;
		case RP_PHY_FREQ_BAND_917MHz:			// (8) 917 - 923.5 (Korea)
			maxGainSet = 102;
			break;
		case RP_PHY_FREQ_BAND_896MHz:			// (5) 896 - 901 (US FCC Part 90)
		case RP_PHY_FREQ_BAND_901MHz:			// (6) 901 - 902 (US FCC Part 24)
		case RP_PHY_FREQ_BAND_915MHz:			// (7) 902 - 928 (US)
		case RP_PHY_FREQ_BAND_902MHz:			// (16)	902 - xxx (PH/MY/AZ)
			maxGainSet = 104;
			break;
		default:
			retStatus = R_STATUS_INVALID_PARAMETER;
			break;
	}

	/* range check phyTransmitPower */
	if ( retStatus == R_STATUS_SUCCESS )
	{
		if ( maxGainSet < transmitPower )
		{
			retStatus = R_STATUS_INVALID_PARAMETER;
		}
	}

	/* set Attribute phyFreqBandId Value */
	if ( retStatus == R_STATUS_SUCCESS )
	{
		retStatus = RfdrvIf_SetAttr( RP_PHY_FREQ_BAND_ID, sizeof(freqBandId), (uint8_t *)&freqBandId );
	}

	/* set Attribute phyTransmitPower Value */
	if ( retStatus == R_STATUS_SUCCESS )
	{
		retStatus = RfdrvIf_SetAttr( RP_PHY_TRANSMIT_POWER, sizeof(transmitPower), (uint8_t *)&transmitPower );
	}

	return( retStatus );
}

/***********************************************************************
 * function name  : RfdrvIf_ContUnmoduTx
 * description    : Start continuous unmodulated Transmisson
 * parameters     : none
 * return value   : retStatus
 **********************************************************************/
static uint8_t RfdrvIf_ContUnmoduTx( void )
{
	uint8_t retStatus = R_STATUS_SUCCESS;

	/* check current state */
	if ( gRfState != R_RFSTATE_IDLE )
	{
		retStatus = R_STATUS_ILLEGAL_REQUEST;
		return( retStatus );
	}

	/* rfdriver API */
	RptConTxNoModu();

	/* change current state */
	gRfState = R_RFSTATE_CONTINUOUS;

	return( retStatus );
}

/***********************************************************************
 * function name  : RfdrvIf_Pn9ContModuTx
 * description    : Start PN9 continuous modulated Transmisson
 * parameters     : none
 * return value   : retStatus
 **********************************************************************/
static uint8_t RfdrvIf_Pn9ContModuTx( void )
{
	uint8_t retStatus = R_STATUS_SUCCESS;

	/* check current state */
	if ( gRfState != R_RFSTATE_IDLE )
	{
		retStatus = R_STATUS_ILLEGAL_REQUEST;
		return( retStatus );
	}

	/* rfdriver API */
	RptPulseTxPrbs9();

	/* change current state */
	gRfState = R_RFSTATE_CONTINUOUS;

	return( retStatus );
}

/***********************************************************************
 * function name  : RfdrvIf_ContTxStop
 * description    : Continuous Transmisson Stop
 * parameters     : none
 * return value   : retStatus
 **********************************************************************/
static uint8_t RfdrvIf_ContTxStop( void )
{
	uint8_t retStatus = R_STATUS_SUCCESS;

	/* check current state */
	if ( gRfState != R_RFSTATE_CONTINUOUS )
	{
		retStatus = R_STATUS_ILLEGAL_REQUEST;
		return( retStatus );
	}

	/* rfdriver API */
	RptConTrxStop();

	/* change current state */
	gRfState = R_RFSTATE_IDLE;

	return( retStatus );
}

/*******************************************************************************
 * "Rfdriver Callback function" (User-Defined Function)
 ******************************************************************************/
/***********************************************************************
 * function name  : RfdrvIf_PdDataIndCallback
 * description    : Frame Reception Complete Callback Function
 * parameters     : pData...Pointer to the beginning of the area storing the reception data
 *                : dataLength...Reception data size
 *                : time...SFD reception time
 *                : linkQuality...LQI value
 *                : rssi...RSSI value
 *                : selectedAntenna...Selected antenna
 *                : rssi_0...RSSI value
 *                : rssi_1...RSSI value
 *                : status...Reception status
 *                : filteredAddr...Number of the function filtered by the address filter function.
 *                :                1(filter1), 2(filter2), 3(filter1 & 2)
 *                : phr...Received PHR information
 * return value   : none
 **********************************************************************/
static void RfdrvIf_PdDataIndCallback( uint8_t *pData, uint16_t dataLength, uint32_t time,
									uint8_t linkQuality, uint16_t rssi, uint8_t selectedAntenna,
									uint16_t rssi_0, uint16_t rssi_1, uint8_t status,
									uint8_t filteredAddr, uint8_t phr )
{
	uint8_t retStatus;
	r_rfdrvIf_msg_pddataind_t *pMsg = &gCbMsgPdDataInd;

	/* convert phyStatus */
	switch ( status )
	{
		case RP_NO_FRMPENBIT_ACK:
		case RP_FRMPENBIT_ACK:
			retStatus = R_STATUS_SUCCESS;
			break;
		default:
			retStatus = R_STATUS_ERROR;
			break;
	}

	if ( retStatus == R_STATUS_SUCCESS )
	{
		if ( dataLength <= R_TEST_DATA_LENGTH )
		{
			/* store message in ring buffer */
			memset( pMsg, 0, sizeof(r_rfdrvIf_msg_pddataind_t) );
			pMsg->rssi = rssi;
			pMsg->psduLength = dataLength;
			memcpy( &(pMsg->psdu[0]), pData, pMsg->psduLength );
			RfdrvIf_PutMsgBuff( pMsg, R_MSG_SIZE_DATAI );
		}
	}

	/* rfdriver API "Receive Buffer Release" */
	RpRelRxBuf( pData );
}

/***********************************************************************
 * function name  : RfdrvIf_PdDataCfmCallback
 * description    : Frame Transmission Complete Callback Function
 * parameters     : status...Frame transmission complete status
 *                : framepend...Pending bit of the reply ACK frame
 *                : numBackoffs...Number of times CCA has been executed
 * return value   : none
 **********************************************************************/
static void RfdrvIf_PdDataCfmCallback( uint8_t status, uint8_t framePend, uint8_t numBackoffs )
{
	uint8_t retStatus;
	r_rfdrvIf_msg_pddatacfm_t *pCfm = &gCbMsgPdDataCfm;

	/* convert phyStatus */
	switch ( status )
	{
		case RP_SUCCESS:
			retStatus = R_STATUS_SUCCESS;
			break;
		case RP_NO_ACK:
			retStatus = R_STATUS_NO_ACK;
			break;
		case RP_BUSY:
			retStatus = R_STATUS_CHANNEL_BUSY;
			break;
		case RP_TRANSMIT_TIME_OVERFLOW:
			retStatus = R_STATUS_TRANSMIT_TIME_OVERFLOW;
			break;
		case RP_TX_UNDERFLOW:
			retStatus = R_STATUS_TX_UNDERFLOW;
			break;
		default:
			retStatus = R_STATUS_ERROR;
			break;
	}

	/* set message content */
	pCfm->status = retStatus;

	/* set event flag */
	pCfm->eventFlag = R_TRUE;
}

/***********************************************************************
 * function name  : RfdrvIf_PlmeCcaCfmCallback
 * description    : CCA Complete Callback Function
 * parameters     : status...CCA status
 * return value   : none
 **********************************************************************/
static void RfdrvIf_PlmeCcaCfmCallback( uint8_t status )
{
}

/***********************************************************************
 * function name  : RfdrvIf_PlmeEdCfmCallback
 * description    : ED (Energy Detection) Complete Callback Function
 * parameters     : phyState...ED complete status
 *                : edValue...ED result
 *                : rssi...RSSI value
 * return value   : none
 **********************************************************************/
static void RfdrvIf_PlmeEdCfmCallback( uint8_t phyState, uint8_t edValue, uint16_t rssi )
{
}

/***********************************************************************
 * function name  : RfdrvIf_RxOffIndCallback
 * description    : Reception OFF Notification Callback Function
 * parameters     : none
 * return value   : none
 **********************************************************************/
static void RfdrvIf_RxOffIndCallback( void )
{
}

/***********************************************************************
 * function name  : RfdrvIf_FatalErrorIndCallback
 * description    : Error Notification Callback Function
 * parameters     : status...Fatal error reason
 * return value   : none
 **********************************************************************/
static void RfdrvIf_FatalErrorIndCallback( uint8_t status )
{
}

/***********************************************************************
 * function name  : RfdrvIf_WarningIndCallback
 * description    : Warning Notification Callback Function
 * parameters     : status...Cause of the warning
 * return value   : none
 **********************************************************************/
static void RfdrvIf_WarningIndCallback( uint8_t status )
{
}

/*******************************************************************************
 * "RfdrvIf Message Buff"
 ******************************************************************************/
/***********************************************************************
 * function name  : RfdrvIf_MessageProcess
 * description	  : message reception processing
 * parameters     : none
 * return value   : none
 **********************************************************************/
void RfdrvIf_MessageProcess( void )
{
	uint8_t *pMsg;

	pMsg = RfdrvIf_GetMsgBuff();
	if ( pMsg != NULL )
	{
		R_SSCMD_ProcessCmd_RCVI( pMsg );
		RfdrvIf_RelMsgBuff();
	}
}

/***********************************************************************
 * function name  : RfdrvIf_InitMsgBuff
 * description    : ring buffer initialization processing
 * parameters     : none
 * return value   : none
 **********************************************************************/
static void RfdrvIf_InitMsgBuff( void )
{
	r_rfdrvIf_msgbuff_t *pBuff;

	pBuff = (r_rfdrvIf_msgbuff_t *)&gMsgBuff;

	pBuff->readBufNum = 0;
	pBuff->writBufNum = 0;
	pBuff->bufFullBit = R_FALSE;
}

/***********************************************************************
 * function name  : RfdrvIf_PutMsgBuff
 * description    : message storage processing to the ring buffer
 * parameters     : pSrcBuff...
 *                : length...
 * return value   : none
 **********************************************************************/
static void RfdrvIf_PutMsgBuff( void *pSrcBuff, uint16_t length )
{
	r_rfdrvIf_msgbuff_t *pBuff;

	pBuff = (r_rfdrvIf_msgbuff_t *)&gMsgBuff;

	if ( pBuff->bufFullBit == R_FALSE )
	{
		memcpy( (void *)&(pBuff->buff[pBuff->writBufNum].cont[0]), (void *)pSrcBuff, length );

		pBuff->writBufNum++;
		if ( pBuff->writBufNum >= R_MSG_BUFF_NUM )
		{
			pBuff->writBufNum = 0;
		}
		if ( pBuff->writBufNum == pBuff->readBufNum )
		{
			pBuff->bufFullBit = R_TRUE;
		}
	}
}

/***********************************************************************
 * function name  : RfdrvIf_GetMsgBuff
 * description    : message acquisition process from the ring buffer
 * parameters     : none
 * return value   : pointer of the acquired message
 **********************************************************************/
static uint8_t *RfdrvIf_GetMsgBuff( void )
{
	uint8_t *pSrc = NULL;
	r_rfdrvIf_msgbuff_t *pBuff = NULL;

	pBuff = (r_rfdrvIf_msgbuff_t *)&gMsgBuff;

	if (( pBuff->readBufNum != (uint8_t)(pBuff->writBufNum ))
		|| ( pBuff->bufFullBit == R_TRUE ))
	{
		pSrc = (uint8_t *)&(pBuff->buff[pBuff->readBufNum].cont[0]);
	}
	return( pSrc );
}

/***********************************************************************
 * function name  : RfdrvIf_RelMsgBuff
 * description    : release of the buffer has stored the message
 * parameters     : none
 * return value   : none
 **********************************************************************/
static void RfdrvIf_RelMsgBuff( void )
{
	r_rfdrvIf_msgbuff_t *pBuff = NULL;

	pBuff = (r_rfdrvIf_msgbuff_t *)&gMsgBuff;

	pBuff->readBufNum++;
	if ( pBuff->readBufNum >= R_MSG_BUFF_NUM )
	{
		pBuff->readBufNum = 0;
	}
	pBuff->bufFullBit = R_FALSE;
}

/*******************************************************************************
 * Copyright (C) 2014-2021 Renesas Electronics Corporation.
 ******************************************************************************/
#endif /* R_SIMPLE_RFTEST_ENABLED */
