/*
 * WPA crypto interface functions using mbedtls
 *
 * This is based in portions on WPA code, specifically the aes-wrap/aes-unwrap
 * functions.
 *
 * The following copyright notice is produced verbatim. The README refers to
 * the WPA Readme
 *
 * This software may be distributed under the terms of the BSD license.
 * See README for more details.
 */


#include "includes.h"
#include "common.h"

#include "mbedtls/config.h"
#include "mbedtls/md.h"
#include "mbedtls/aes.h"

#define AES_BLOCK_SIZE 16

// LCOV_EXCL_START

/**
 * aes_unwrap - Unwrap key with AES Key Wrap Algorithm (RFC3394)
 * @kek: Key encryption key (KEK)
 * @kek_len: Length of KEK in octets
 * @n: Length of the plaintext key in 64-bit units; e.g., 2 = 128-bit = 16
 * bytes
 * @cipher: Wrapped key to be unwrapped, (n + 1) * 64 bits
 * @plain: Plaintext key, n * 64 bits
 * Returns: 0 on success, -1 on failure (e.g., integrity verification failed)
 */
int aes_unwrap(const u8* kek, size_t kek_len, int n, const u8* cipher, u8* plain)
{
    u8 a[8], * r, b[AES_BLOCK_SIZE];
    int i, j;
    uint32_t t;
    mbedtls_aes_context aes_ctx;

    /* 1) Initialize variables. */
    os_memcpy(a, cipher, 8);
    r = plain;
    os_memcpy(r, cipher + 8, 8 * n);

    mbedtls_aes_init(&aes_ctx);
    if (mbedtls_aes_setkey_dec(&aes_ctx, kek, kek_len * 8) != 0)
    {
        return -1;
    }

    /* 2) Compute intermediate values.
     * For j = 5 to 0
     *     For i = n to 1
     *         B = AES-1(K, (A ^ t) | R[i]) where t = n*j+i
     *         A = MSB(64, B)
     *         R[i] = LSB(64, B)
     */
    for (j = 5; j >= 0; j--)
    {
        r = plain + (n - 1) * 8;
        for (i = n; i >= 1; i--)
        {
            os_memcpy(b, a, 8);
            t = n * j + i;
            b[7] ^= t;
            b[6] ^= t >> 8;
            b[5] ^= t >> 16;
            b[4] ^= t >> 24;

            os_memcpy(b + 8, r, 8);
            mbedtls_internal_aes_decrypt(&aes_ctx, b, b);
            os_memcpy(a, b, 8);
            os_memcpy(r, b + 8, 8);
            r -= 8;
        }
    }

    mbedtls_aes_free(&aes_ctx);

    /* 3) Output results.
     *
     * These are already in @plain due to the location of temporary
     * variables. Just verify that the IV matches with the expected value.
     */
    for (i = 0; i < 8; i++)
    {
        if (a[i] != 0xa6)
        {
            return -1;
        }
    }

    return 0;
}

/**
  * Wrap keys with AES Key Wrap Algorithm (RFC3394)
  * @param kek  Key encryption key (KEK)
  * @param kek_len  Length of KEK in octets
  * @param n  Length of the plaintext key in 64-bit units; e.g., 2 = 128-bit = 16 bytes
  * @param plain Plaintext key to be wrapped, n * 64 bits
  * @param cipher Wrapped key, (n + 1) * 64 bits
  * @return 0 on success, -1 on failure
  */
int aes_wrap(const u8* kek, size_t kek_len, int n, const u8* plain, u8* cipher)
{
    u8* a, * r, b[AES_BLOCK_SIZE];
    int i, j;
    uint32_t t;
    mbedtls_aes_context aes_ctx;

    a = cipher;
    r = cipher + 8;

    /* 1) Initialize variables. */
    os_memset(a, 0xa6, 8);
    os_memcpy(r, plain, 8 * n);

    mbedtls_aes_init(&aes_ctx);

    /* 2) Calculate intermediate values.
     * For j = 0 to 5
     *     For i=1 to n
     *         B = AES(K, A | R[i])
     *         A = MSB(64, B) ^ t where t = (n*j)+i
     *         R[i] = LSB(64, B)
     */
    if (mbedtls_aes_setkey_enc(&aes_ctx, kek, kek_len * 8) != 0)
    {
        return -1;
    }
    for (j = 0; j <= 5; j++)
    {
        r = cipher + 8;
        for (i = 1; i <= n; i++)
        {
            os_memcpy(b, a, 8);
            os_memcpy(b + 8, r, 8);
            mbedtls_internal_aes_encrypt(&aes_ctx, b, b);
            os_memcpy(a, b, 8);
            t = n * j + i;
            a[7] ^= t;
            a[6] ^= t >> 8;
            a[5] ^= t >> 16;
            a[4] ^= t >> 24;
            os_memcpy(r, b + 8, 8);
            r += 8;
        }
    }

    mbedtls_aes_free(&aes_ctx);

    /* 3) Output the results.
     *
     * These are already in @cipher due to the location of temporary
     * variables.
     */

    return 0;
}

// LCOV_EXCL_STOP

/**
 * Calculate message digest of vector of data
 * @param md_info message digest configuration
 * @param num_elem number of elements in vector
 * @param data vector with data pointers
 * @param len vector with data lengths
 * @param mac pointer to out buffer for the MAC
 * @return 0 on success
 */
static int mbedtls_md_vector(const mbedtls_md_info_t* md_info,
                             size_t                   num_elem,
                             const unsigned char*     data[],
                             const size_t*            len,
                             unsigned char*           mac)
{
    mbedtls_md_context_t ctx;
    size_t i;
    int ret;

    if (md_info == NULL)
    {
        return MBEDTLS_ERR_MD_BAD_INPUT_DATA;
    }

    mbedtls_md_init(&ctx);

    // Last parameter is 0 to avoid allocating space for hmac if not used. Saves some memory
    ret = mbedtls_md_setup(&ctx, md_info, 0);

    if (ret != 0)
    {
        return ret;
    }

    mbedtls_md_starts(&ctx);

    for (i = 0; i < num_elem; i++)
    {
        mbedtls_md_update(&ctx, data[i], len[i]);
    }

    mbedtls_md_finish(&ctx, mac);
    mbedtls_md_free(&ctx);

    return 0;
}

/**
 * Calculate SHA1 hash on vector of data
 * @param num_elem number of elements in vector
 * @param data vector with data pointers
 * @param len vector with data lengths
 * @param mac pointer to out buffer for the MAC
 * @return 0 on success
 */
int sha1_vector(size_t num_elem, const u8* addr[], const size_t* len, u8* mac)
{
    return mbedtls_md_vector(mbedtls_md_info_from_type(MBEDTLS_MD_SHA1), num_elem, addr, len, mac);
}

/**
 * Calculate HMAC on vector of data
 * @param md_info HMAC configuration
 * @param key the key
 * @param key_len the length of the key
 * @param num_elem number of elements in vector
 * @param data vector with data pointers
 * @param len vector with data lengths
 * @param mac pointer to out buffer for the HMAC
 * @return 0 on success
 */
static int mbedtls_md_hmac_vector(const mbedtls_md_info_t* md_info,
                                  const unsigned char*     key,
                                  size_t                   key_len,
                                  size_t                   num_elem,
                                  const unsigned char*     data[],
                                  const size_t*            len,
                                  unsigned char*           mac)
{
    mbedtls_md_context_t ctx;
    size_t i;
    int ret;

    if (md_info == NULL)
    {
        return MBEDTLS_ERR_MD_BAD_INPUT_DATA;
    }

    mbedtls_md_init(&ctx);

    // Last parameter is 1 to allocate space for hmac
    ret = mbedtls_md_setup(&ctx, md_info, 1);

    if (ret != 0)
    {
        return ret;
    }

    mbedtls_md_hmac_starts(&ctx, key, key_len);

    for (i = 0; i < num_elem; i++)
    {
        mbedtls_md_hmac_update(&ctx, data[i], len[i]);
    }

    mbedtls_md_hmac_finish(&ctx, mac);
    mbedtls_md_free(&ctx);

    return 0;
}

/**
 * Calculate MD5 HMAC of data
 * @param key the key
 * @param key_len the length of the key
 * @param data the data buffer
 * @param data_len the length of the data buffer
 * @param mac pointer to out buffer for the MAC
 * @return 0 on success
 */
int hmac_md5(const u8* key, size_t key_len, const u8* data, size_t data_len, u8* mac)
{
    return mbedtls_md_hmac_vector(mbedtls_md_info_from_type(MBEDTLS_MD_MD5), key, key_len, 1, &data, &data_len, mac);
}

/**
 * Calculate SHA1 HMAC on vector of data
 * @param key the key
 * @param key_len the length of the key
 * @param num_elem number of elements in vector
 * @param data vector with data pointers
 * @param len vector with data lengths
 * @param mac pointer to out buffer for the HMAC
 * @return 0 on success
 */
int hmac_sha1_vector(const u8* key, size_t key_len, size_t num_elem, const u8* data[], const size_t* len, u8* mac)
{
    return mbedtls_md_hmac_vector(mbedtls_md_info_from_type(MBEDTLS_MD_SHA1), key, key_len, num_elem, data, len, mac);
}


/**
 * Calculate SHA1 HMAC of data
 * @param key the key
 * @param key_len the length of the key
 * @param data the data buffer
 * @param data_len the length of the data buffer
 * @param mac pointer to out buffer for the MAC
 * @return 0 on success
 */
int hmac_sha1(const u8* key, size_t key_len, const u8* data, size_t data_len, u8* mac)
{
    return hmac_sha1_vector(key, key_len, 1, &data, &data_len, mac);
}
