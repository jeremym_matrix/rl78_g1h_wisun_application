/******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
*
* Copyright (C) 2017 Renesas Electronics Corporation. All rights reserved.
******************************************************************************/

/*!
   \file      r_auth_internal.h
   \version   1.00
   \brief     Internal Wi-SUN Authentication structures
 */

#ifndef R_AUTH_INTERNAL_H
#define R_AUTH_INTERNAL_H

#include <stddef.h>
#include <stdint.h>

#include "r_auth_config.h"
#include "r_nwk_api_base.h"
#include "r_apl_global.h"

/** The EAPOL relay message header */
typedef struct
{
    uint8_t sup[8];
    uint8_t kmp_id;
    uint8_t msg[];
} eapol_relay_t;

void R_AUTH_InternalReset();
void R_AUTH_InternalMemoryReset();
int  R_AUTH_InternalRandom(void* unused, unsigned char* output, size_t output_len);

/**
 * Push relevant authentication state to NWK task
 * @param nwk
 * @return
 */
r_result_t R_AUTH_SetAuthStateCache(r_apl_global_t* nwk);

#define GTK(igtk)           (nwk->auth.gtks[igtk])
#define GTK_IS_VALID(igtk)  (nwk->auth.gtks_valid & (1u << (igtk)))
#define MSGTYPE(eapol_type) (((eapol_type) == R_EAPOL_TYPE_8021X || (eapol_type) == R_EAPOL_TYPE_CONVERT_FROM_EAP) ? "EAP" : "RSN")

#endif /* R_AUTH_INTERNAL_H */
