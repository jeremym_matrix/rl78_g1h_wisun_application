/*
 * WPA SSL/TLS interface functions using mbedtls
 *
 * This is based in portions on WPA code, specifically the empty TLS wrapper
 * and the GNUTLS wrapper
 *
 * The following copyright notice is produced verbatim. The README refers to
 * the WPA Readme
 *
 * This software may be distributed under the terms of the BSD license.
 * See README for more details.
 */

#include "r_tls_mbedtls.h"
#include "r_auth_tls.h"
#include "r_auth_certs_config.h"

// WPA includes
#include "includes.h"
#include "common.h"
#include "crypto/tls.h"

// mbed TLS includes
#include "mbedtls/ssl.h"
#include "mbedtls/ssl_internal.h"
#include "mbedtls/debug.h"
#include "mbedtls/oid.h"

#include "r_auth_internal.h"

#ifdef MBEDTLS_DEBUG_C
#include <stdio.h>
#endif

// R_LOG_PREFIX is processed by RLog code generator
#define R_LOG_PREFIX AUTH
#include "r_log_internal.h"
#if R_LOG_THRESHOLD > R_LOG_SEVERITY_OFF
#include "r_loggen_r_tls_mbedtls.h"
#endif

#define R_WISUN_CRT_VERSION 3

#ifdef MBEDTLS_DEBUG_C
// Debugging callback (not used on target device as debug string literals do not fit into ROM)
static void my_debug(void* ctx, int level, const char* file, int line, const char* str)
{
    printf("%s", str);  // unused on physical devices
}
#endif

/**
 * Private Internet Extensions
 * { iso(1) identified-organization(3) dod(6) internet(1)
 *                      private(4) enterprise(1) Wi-SUN(45605) FieldAreaNetwork(1)}
 */
#define MBEDTLS_OID_WISUN                 MBEDTLS_OID_ISO_IDENTIFIED_ORG MBEDTLS_OID_ORG_DOD "\x01\x04\x01\x82\xe4\x25\x01"

/**
 * Private Internet Extensions
 * { iso(1) identified-organization(3) dod(6) internet(1)
 *                      security(5) mechanisms(5) pkix(7) on(8) id-on-hardwareModuleName(4)}
 */
#define MBEDTLS_OID_ID_ON_HARDWARE_MODULE MBEDTLS_OID_PKIX "\x08\x04"

/**
 * mbedTLS read callback (see mbedtls_ssl_recv_t)
 * @param ctx context provided to mbedtls_ssl_set_bio
 * @param buf data buffer
 * @param len size of data buffer
 * @return number of bytes written or (negative) mbedTLS error code
 */
static int mbedtls_recv_func(void* ctx, unsigned char* buf, size_t len)
{
    struct tls_connection* conn = ctx;
    const u8* end;

    if ((conn == NULL) || (buf == NULL))
    {
        return MBEDTLS_ERR_SSL_BAD_INPUT_DATA;
    }

    if (conn->pull_buf == NULL)
    {
        return MBEDTLS_ERR_SSL_WANT_READ;
    }

    end = wpabuf_head_u8(conn->pull_buf) + wpabuf_len(conn->pull_buf);
    if ((size_t)(end - conn->pull_buf_offset) < len)
    {
        len = end - conn->pull_buf_offset;
    }

    os_memcpy(buf, conn->pull_buf_offset, len);
    conn->pull_buf_offset += len;

    if (conn->pull_buf_offset == end)
    {
        wpa_printf(MSG_DEBUG, "%s - pull_buf consumed", __func__);
        wpabuf_free(conn->pull_buf);
        conn->pull_buf = NULL;
        conn->pull_buf_offset = NULL;
    }
    else
    {
        wpa_printf(MSG_DEBUG, "%s - %lu bytes remaining in pull_buf", __func__,
                   (unsigned long)(end - conn->pull_buf_offset));
    }

    if (buf[0] == MBEDTLS_SSL_MSG_ALERT)
    {
        conn->read_alerts++;
    }

    return len;
}

/**
 * mbedTLS write callback (see mbedtls_ssl_send_t)
 * @param ctx context provided to mbedtls_ssl_set_bio
 * @param buf data buffer
 * @param len size of data buffer
 * @return number of bytes actually sent or (negative) mbedTLS error code
 */
static int mbedtls_send_func(void* ctx, const unsigned char* buf, size_t len)
{
    struct tls_connection* conn = ctx;

    if ((conn == NULL) || (buf == NULL))
    {
        return MBEDTLS_ERR_SSL_BAD_INPUT_DATA;
    }

    if (wpabuf_resize(&conn->push_buf, len) < 0)
    {
        conn->failed++;
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR
        r_loggen_137();
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR */
        return MBEDTLS_ERR_SSL_ALLOC_FAILED;
    }

    if (buf[0] == MBEDTLS_SSL_MSG_ALERT)
    {
        conn->write_alerts++;
    }

    wpabuf_put_data(conn->push_buf, buf, len);

    return len;
}

int mbedtls_x509_crt_check_subject_alt_name_usage(const mbedtls_x509_crt* crt,
                                                  const char*             usage_oid,
                                                  uint8_t                 usage_len)
{
    const mbedtls_x509_sequence* cur;
    const uint8_t* buffer;

    /*
     * Although the SANE extension can have multiple elements, the TPS
     * specifies clearly that "one and only one" alternative name of
     * type OtherName of type id-on-hardwareModule Name is allowed
     */
    cur = &crt->subject_alt_names;
    if (cur->next != NULL)
    {
        return MBEDTLS_ERR_X509_BAD_INPUT_DATA;
    }

    buffer = cur->buf.p;

    /*
     * The first element in the sequence must be the OID we are looking for.
     * The second byte is the length of the OID. The oid starts at position 2
     * in the subject alternate name
     */
    if (buffer != NULL && buffer[0] == MBEDTLS_ASN1_OID &&
        buffer[1] == usage_len && memcmp(buffer + 2, usage_oid, usage_len) == 0)
    {
        return 0;
    }

    return MBEDTLS_ERR_X509_BAD_INPUT_DATA;
}

/**
 * Validate that the passed certificate conforms to the FANTPS requirements
 * @param crt Parsed certificate provided by mbedTLS
 * @param chain_index The index of the certificate in the verification chain
 * @param[out] flags Flags to indicate the validation result. 0 means that the certificate validation succeeded.
 * @param ownCert True, if the passed certificate is our own. False, if the certificate was sent by another device.
 * @return Always 0. A different return value indicates a fatal error and stops authentication
 */
int tls_validate_wisun_cert(mbedtls_x509_crt* crt, int chain_index, uint32_t* flags, int ownCert)
{
    const char* ext_oid = MBEDTLS_OID_WISUN;
    uint8_t ext_len = MBEDTLS_OID_SIZE(MBEDTLS_OID_WISUN);
    const char* sane_oid = MBEDTLS_OID_ID_ON_HARDWARE_MODULE;
    uint8_t sane_len = MBEDTLS_OID_SIZE(MBEDTLS_OID_ID_ON_HARDWARE_MODULE);
    uint8_t sane_header_len = sane_len + 2;  // OID Type and one-byte length

    if ((crt == NULL) || (flags == NULL))
    {
        return MBEDTLS_ERR_X509_BAD_INPUT_DATA;
    }

    if (chain_index != 0)
    {
        return 0;  // Return if this is not the device certificate (intermediate certificate)
    }

    /* Check certificate version number and expiration */
    if ((crt->version != R_WISUN_CRT_VERSION) ||
        (crt->valid_to.year != 9999) || (crt->valid_to.mon != 12) || (crt->valid_to.day != 31) ||
        (crt->valid_to.hour != 23) || (crt->valid_to.min != 59) || (crt->valid_to.sec != 59))
    {
        *flags |= MBEDTLS_X509_BADCERT_OTHER;
        return 0;
    }

    /* Check subject and issuer IDs. According to the TPS, they have to be empty */
    if ((crt->subject.oid.p != NULL) || (crt->subject_id.p != NULL) || (crt->issuer_id.p != NULL))
    {
        *flags |= MBEDTLS_X509_BADCERT_OTHER;
        return 0;
    }

    if ((mbedtls_x509_crt_check_extended_key_usage(crt, ext_oid, ext_len) != 0) ||
        (mbedtls_x509_crt_check_subject_alt_name_usage(crt, sane_oid, sane_len) != 0))
    {
        *flags |= MBEDTLS_X509_BADCERT_OTHER;
        return 0;
    }

    /* Since the exact content of the SANE is vendor-defined, it is validated by a custom application layer function */
    uint16_t sane_buf_len = (crt->subject_alt_names.buf.len > sane_header_len) ? crt->subject_alt_names.buf.len - sane_header_len : 0;
    uint8_t* sane_buf = os_malloc(sane_buf_len);
    if ((sane_buf != NULL) && (crt->subject_alt_names.buf.p != NULL))
    {
        os_memcpy(sane_buf, crt->subject_alt_names.buf.p + sane_header_len, sane_buf_len);
    }
    if (R_APP_Certs_ValidateSubjectAltNameExtension(sane_buf, sane_buf_len, ownCert) != 0)
    {
        *flags |= MBEDTLS_X509_BADCERT_OTHER;
    }
    os_free(sane_buf);

    return 0;
}

/**
 * mbedTLS certificate verification callback for incoming certificates (see x509_crt.c)
 * @param ssl Context provided to mbedtls_ssl_conf_verify
 * @param crt Parsed certificate provided by mbedTLS
 * @param chain_index The index of the certificate in the verification chain
 * @param[out] flags Flags to indicate the validation result. 0 means that the certificate validation succeeded.
 * @return Always 0. A different return value indicates a fatal error and stops authentication
 */
int mbedtls_validate_cert(void* ssl, mbedtls_x509_crt* crt, int chain_index, uint32_t* flags)
{
    return tls_validate_wisun_cert(crt, chain_index, flags, R_FALSE);
}

/**
 * WPA TLS initialization callback
 * @param conf the TLS configuration
 * @return pointer to the allocated context or NULL or error
 */
void* tls_init(const struct tls_config* conf)
{
    r_tls_context_t* context;
    mbedtls_ssl_context* ssl;
    mbedtls_ssl_config* ssl_config;

    context = os_zalloc(sizeof(*context));
    if (context == NULL)
    {
        return NULL;
    }

    ssl = &context->ssl;
    ssl_config = &context->ssl_config;

    mbedtls_ssl_init(ssl);
    mbedtls_ssl_config_init(ssl_config);

    /* Set reasonable default values (including internally the version of SSL used). */
    mbedtls_ssl_config_defaults(ssl_config, MBEDTLS_SSL_IS_CLIENT, MBEDTLS_SSL_TRANSPORT_STREAM,
                                MBEDTLS_SSL_PRESET_DEFAULT);

    // Note: mbedtls_ssl_setup must be called after configuration is complete -> defer call

#ifdef MBEDTLS_DEBUG_C
    mbedtls_ssl_conf_dbg(ssl_config, my_debug, NULL);
#endif

    return context;
}

/**
 * WPA TLS initialization callback
 * @param ssl_ctx to the allocated context from tls_init
 */
void tls_deinit(void* ssl_ctx)
{
    if (ssl_ctx)
    {
        r_tls_context_t* context = ssl_ctx;
        mbedtls_pk_free(&context->pkey);
        mbedtls_x509_crt_free(&context->own_cert);
        mbedtls_x509_crt_free(&context->cacert);
        mbedtls_ssl_config_free(&context->ssl_config);
        mbedtls_ssl_free(&context->ssl);
        os_free(context);
    }
}

/**
 * WPA callback function
 * @param tls_ctx unused
 * @return 0
 */
int tls_get_errors(void* tls_ctx)
{
    /* Same "implementation" as in tls_gnutls.c */
    return 0;
}

/**
 * WPA callback function to establish a TLS connection
 * @param tls_ctx the TLS context
 * @return pointer to the connection structure or NULL on error
 */
struct tls_connection* tls_connection_init(void* tls_ctx)
{
    if (tls_ctx == NULL)
    {
        return NULL;
    }

    r_tls_context_t* context = tls_ctx;
    mbedtls_ssl_context* ssl = &context->ssl;
    mbedtls_ssl_config* ssl_config = &context->ssl_config;
    struct tls_connection* conn;

    conn = os_zalloc(sizeof(*conn));
    if (conn == NULL)
    {
        return NULL;
    }

    mbedtls_ssl_set_bio(ssl, conn, mbedtls_send_func, mbedtls_recv_func, NULL);
    mbedtls_ssl_conf_verify(ssl_config, mbedtls_validate_cert, ssl);
    return conn;
}

/**
 * Close a TLS connection
 * @param tls_ctx the TLS context
 * @param conn the TLS connection
 */
void tls_connection_deinit(void* tls_ctx, struct tls_connection* conn)
{
    if (conn)
    {
        wpabuf_free(conn->push_buf);
        conn->push_buf = NULL;
        wpabuf_free(conn->pull_buf);
        conn->pull_buf = NULL;
        os_free(conn);
    }
}

/**
 * Return if a connection is established
 * @param tls_ctx the TLS context
 * @param conn the TLS connection
 * @return 0 if not established
 */
int tls_connection_established(void* tls_ctx, struct tls_connection* conn)
{
    return (conn == NULL) ? 0 : conn->established;
}

/**
 * Common TLS setup function for both server and client
 * @param tls_ctx the TLS context
 * @param params the TLS connection parameters
 * @return 0 on success, -1 on error
 */
static int tls_connection_set_common_params(void* tls_ctx, const struct tls_connection_params* params)
{
    if ((tls_ctx == NULL) || (params == NULL) || (params->client_cert == NULL))
    {
        return -1;
    }

    r_tls_context_t* context = tls_ctx;
    mbedtls_ssl_config* ssl_config = &context->ssl_config;
    mbedtls_x509_crt* cacert = &context->cacert;
    mbedtls_x509_crt* own_cert = &context->own_cert;
    mbedtls_pk_context* pkey = &context->pkey;
    int16_t certIndex = (uint8_t)(params->private_key ? *params->private_key : 1);
    const uint8_t* encodedCerts = (const uint8_t*)params->client_cert;
    int foundCA = 0;
    int foundKey = 0;
    int foundCert = 0;

    mbedtls_ssl_conf_rng(ssl_config, R_AUTH_InternalRandom, NULL);

    mbedtls_pk_init(pkey);
    mbedtls_x509_crt_init(cacert);
    mbedtls_x509_crt_init(own_cert);

    while (*encodedCerts != 0xff)
    {
        uint8_t index = *encodedCerts++;
        uint16_t blockSize = ((uint16_t)encodedCerts[0]) << 8 | encodedCerts[1];
        encodedCerts += 2;
        if (index == 0)
        {
            if (mbedtls_x509_crt_parse(cacert, encodedCerts, blockSize))
            {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR
                r_loggen_423();
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR */
                return -1;
            }
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
            r_loggen_426();
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
            foundCA = 1;
        }
        else if (index == certIndex)
        {
            if (foundKey)
            {
                if (mbedtls_x509_crt_parse(own_cert, encodedCerts, blockSize))
                {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR
                    r_loggen_435();
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR */
                    return -1;
                }
                if (context->ssl_config.endpoint == MBEDTLS_SSL_IS_CLIENT)
                {
                    uint32_t flags = 0;
                    // Ignore return value since it is always 0
                    tls_validate_wisun_cert(own_cert, 0, &flags, R_TRUE);
                    if (flags != 0)
                    {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR
                        r_loggen_445();
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR */
                        return -1;
                    }
                }
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
                r_loggen_449();
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
                foundCert = 1;
            }
            else
            {
                if (mbedtls_pk_parse_key(pkey, encodedCerts, blockSize, NULL, 0))
                {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR
                    r_loggen_456();
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR */
                    return -1;
                }
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
                r_loggen_459();
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
                foundKey = 1;
            }
        }
        encodedCerts += blockSize;
    }
    if (!(foundCA && foundKey && foundCert))
    {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR
        r_loggen_467();
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR */
        return -1;
    }
    if (mbedtls_ssl_conf_own_cert(ssl_config, own_cert, pkey) != 0)
    {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR
        r_loggen_472();
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR */
        return -1;
    }
    mbedtls_ssl_conf_ca_chain(ssl_config, cacert, NULL);

    return 0;
}

/**
 * WPA callback to set TLS connection params
 * @details This function is only called by clients
 * @param tls_ctx the TLS context
 * @param conn the TLS connection
 * @param params the TLS connection parameters
 * @return 0 on success, -1 on error
 */
int tls_connection_set_params(void* tls_ctx, struct tls_connection* conn, const struct tls_connection_params* params)
{
    if (tls_ctx == NULL)
    {
        return -1;
    }

    r_tls_context_t* context = tls_ctx;
    mbedtls_ssl_context* ssl = &context->ssl;
    mbedtls_ssl_config* ssl_config = &context->ssl_config;

    mbedtls_ssl_conf_endpoint(ssl_config, MBEDTLS_SSL_IS_CLIENT);

    if (tls_connection_set_common_params(tls_ctx, params) != 0)
    {
        return -1;
    }

    // final configuration call from client -> call mbedTLS setup
    mbedtls_ssl_setup(ssl, ssl_config);

    return 0;
}


/**
 * WPA callback to set TLS connection params
 * @details This function is only called by servers
 * @param tls_ctx the TLS context
 * @param params the TLS connection parameters
 * @return 0 on success, -1 on error
 */
int tls_global_set_params(void* tls_ctx, const struct tls_connection_params* params)
{
    if (tls_ctx == NULL)
    {
        return -1;
    }

    r_tls_context_t* context = tls_ctx;
    mbedtls_ssl_config* ssl_config = &context->ssl_config;

    mbedtls_ssl_conf_endpoint(ssl_config, MBEDTLS_SSL_IS_SERVER);
    return tls_connection_set_common_params(tls_ctx, params);
}

/** Unused, always return 0 */
int tls_global_set_verify(void* tls_ctx, int check_crl)
{
    /* Nothing to verify here. Simply return 0. */
    return 0;
}

/**
 * Return the relevant random information from the TLS exchange
 * @param tls_ctx the TLS context
 * @param conn the TLS connection
 * @param[out] keys output the keys
 * @return 0 on success, -1 on error
 */
int tls_connection_get_random(void* tls_ctx, struct tls_connection* conn, struct tls_random* keys)
{
    if (conn == NULL || keys == NULL)
    {
        return -1;
    }

    os_memset(keys, 0, sizeof(*keys));

    /*
     * randbytes is a 64 byte variable that contains the client_random data in the first
     * 32 bytes and server_random in the last 32 bytes. There is no need to make a copy.
     * Just set the pointers to the right place.
     */
    keys->client_random = &conn->randbytes[0];
    keys->server_random = &conn->randbytes[32];
    keys->client_random_len = 32;
    keys->server_random_len = 32;

    return 0;
}

/**
 * Export the TLS connection keys
 * @param tls_ctx the TLS context
 * @param conn the TLS connection
 * @param label label for the PRF
 * @param out output buffer
 * @param out_len size of output buffer
 * @return 0 on success, -1 on error
 */
int tls_connection_export_key(void* tls_ctx, struct tls_connection* conn, const char* label, u8* out, size_t out_len)
{
    if ((tls_ctx == NULL) || (conn == NULL) || (label == NULL) || (out == NULL))
    {
        return -1;
    }

    r_tls_context_t* context = tls_ctx;
    mbedtls_ssl_context* ssl = &context->ssl;

    /*
     * We need to use the internal implementation of tls_prf, which is not public. Fortunately,
     * we can simply use the function pointer stored in the ssl context.
     */
    int res = context->tls_prf(ssl->session->master, 48, label, conn->randbytes, 64, out, out_len);
#if R_DEV_AUTH_DEBUG_ENABLED
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
    r_loggen_595(out);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
#endif
    return res;
}

#if R_BORDER_ROUTER_ENABLED
/**
 * Perform the TLS connection handshake
 * @param tls_ctx the TLS context
 * @param conn the TLS connection
 * @param in_data incoming data
 * @param appl_data unused
 * @return wpabuf to send or NULL on ERROR
 */
struct wpabuf* tls_connection_handshake(void*                  tls_ctx,
                                        struct tls_connection* conn,
                                        const struct wpabuf*   in_data,
                                        struct wpabuf**        appl_data)
{
    if ((tls_ctx == NULL) || (conn == NULL))
    {
        return NULL;
    }

    r_tls_context_t* context = tls_ctx;
    mbedtls_ssl_context* ssl = &context->ssl;

    struct wpabuf* out_data;
    int ret;

    if (appl_data)
    {
        *appl_data = NULL;
    }

    /* If there is data coming from the application, make sure it is available before the
     * call to the next step in the handshake.
     */
    if (in_data && wpabuf_len(in_data) > 0)
    {
        if (conn->pull_buf)
        {
            wpa_printf(MSG_DEBUG, "%s - %lu bytes remaining in "
                       "pull_buf", __func__,
                       (unsigned long)wpabuf_len(conn->pull_buf));
            wpabuf_free(conn->pull_buf);
        }

        conn->pull_buf = wpabuf_dup(in_data);
        if (conn->pull_buf == NULL)
        {
            return NULL;
        }

        conn->pull_buf_offset = wpabuf_head(conn->pull_buf);
    }

    do
    {
        ret = mbedtls_ssl_handshake_step(ssl);

        /*
         * Make a copy of the random bytes as soon as they become available. They are complete after the Server Hello has
         * been parsed/written.
         */
        if (ssl->state == MBEDTLS_SSL_SERVER_HELLO + 1)
        {
            os_memcpy(&conn->randbytes, &ssl->handshake->randbytes[0], 64);
        }

        /*
         * Make a copy of the function used to compute the prf as soon as it becomes available.
         * This pointer is set to the appropriate function SHA256 (for example) and it is used
         * internally by mbedtls. After a couple more rounds of the handshake state machine,
         * this pointer is cleared. Therefore, we cannot set it at the beginning or the end
         * of the handshake. We have to do that as soon as it becomes available.
         */
        if (ssl->handshake && ssl->handshake->tls_prf)
        {
            context->tls_prf = ssl->handshake->tls_prf;
        }

    }
    while (ret == 0 && ssl->state != MBEDTLS_SSL_HANDSHAKE_OVER);

    if (ret != 0 && ret != MBEDTLS_ERR_SSL_WANT_READ && ret != MBEDTLS_ERR_SSL_WANT_WRITE && ret != MBEDTLS_ERR_SSL_HELLO_VERIFY_REQUIRED)
    {
        conn->failed++;
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR
        r_loggen_683(-ret);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR */
        return NULL;
    }

    if (ssl->state == MBEDTLS_SSL_HANDSHAKE_OVER)
    {
        conn->established = 1;

        /* Make sure EAP-TLS does not fail if we do not have anything else to send */
        if (!conn->push_buf)
        {
            conn->push_buf = wpabuf_alloc(0);
        }
    }

    out_data = conn->push_buf;
    conn->push_buf = NULL;
    return out_data;
}

/**
* Perform the TLS connection handshake for the server
* @param tls_ctx the TLS context
* @param conn the TLS connection
* @param in_data incoming data
* @param appl_data unused
* @return wpabuf to send or NULL on ERROR
*/

struct wpabuf* tls_connection_server_handshake(void*                  tls_ctx,
                                               struct tls_connection* conn,
                                               const struct wpabuf*   in_data,
                                               struct wpabuf**        appl_data)
{
    return tls_connection_handshake(tls_ctx, conn, in_data, appl_data);
}
#endif // R_BORDER_ROUTER_ENABLED


/**
 * Return if connection is resumed
 *
 * @details We do not want to support session tickets and resumption of handshakes,
 * so this function always returns 0 to indicate that this is not a resumed connection.
 * @param tls_ctx the TLS context
 * @param conn the TLS connection
 * @return 0 if not resumed
 */
int tls_connection_resumed(void* tls_ctx, struct tls_connection* conn)
{
    return 0;
}

/**
 * Return if connection is failed
 * @param tls_ctx the TLS context
 * @param conn the TLS connection
 * @return 0 if not failed
 */
int tls_connection_get_failed(void* tls_ctx, struct tls_connection* conn)
{
    if (conn == NULL)
    {
        return -1;
    }

    return conn->failed;
}

/**
 * Return the number of write alerts
 * @param tls_ctx the TLS context
 * @param conn the TLS connection
 * @return the number of write alerts
 */
int tls_connection_get_write_alerts(void* tls_ctx, struct tls_connection* conn)
{
    if (conn == NULL)
    {
        return -1;
    }

    return conn->write_alerts;
}

/**
 * Part of the SSL setup
 * @param tls_ctx the TLS context
 * @param conn the TLS connection
 * @param verify_peer indicate if peer must be verified
 * @param flags unused
 * @param session_ctx unused
 * @param session_ctx_len unused
 * @return 0 on succes
 */
int tls_connection_set_verify(void* tls_ctx, struct tls_connection* conn, int verify_peer, unsigned int flags,
                              const u8* session_ctx, size_t session_ctx_len)
{

    if (tls_ctx == NULL)
    {
        return -1;
    }

    r_tls_context_t* context = tls_ctx;
    mbedtls_ssl_context* ssl = &context->ssl;
    mbedtls_ssl_config* ssl_config = &context->ssl_config;

    mbedtls_ssl_conf_authmode(ssl_config, verify_peer == 1 ? MBEDTLS_SSL_VERIFY_REQUIRED : MBEDTLS_SSL_VERIFY_NONE);

    // final configuration call from server -> call mbedTLS setup
    mbedtls_ssl_setup(ssl, ssl_config);

    return 0;
}


// unused functions
void tls_connection_set_success_data(struct tls_connection* conn, struct wpabuf* data)
{
}
void tls_connection_set_success_data_resumed(struct tls_connection* conn)
{
}
const struct wpabuf* tls_connection_get_success_data(struct tls_connection* conn)
{
    return NULL;
}
void tls_connection_remove_session(struct tls_connection* conn)
{
}
int tls_get_library_version(char* buf, size_t buf_len)
{
    return 0;
}
int tls_connection_get_eap_fast_key(void* tls_ctx, struct tls_connection* conn, u8* out, size_t out_len)
{
    return -1;
}
int tls_get_version(void* ssl_ctx, struct tls_connection* conn, char* buf, size_t buflen)
{
    return -1;
}
int tls_get_cipher(void* tls_ctx, struct tls_connection* conn, char* buf, size_t buflen)
{
    return -1;
}
int tls_connection_enable_workaround(void* tls_ctx, struct tls_connection* conn)
{
    return -1;
}
int tls_connection_client_hello_ext(void* tls_ctx, struct tls_connection* conn, int ext_type, const u8* data, size_t data_len)
{
    return -1;
}
int tls_connection_set_cipher_list(void* tls_ctx, struct tls_connection* conn, u8* ciphers)
{
    return -1;
}
struct wpabuf* tls_connection_encrypt(void* tls_ctx, struct tls_connection* conn, const struct wpabuf* in_data)
{
    return NULL;
}
struct wpabuf* tls_connection_decrypt(void* tls_ctx, struct tls_connection* conn, const struct wpabuf* in_data)
{
    return NULL;
}
int tls_connection_shutdown(void* tls_ctx, struct tls_connection* conn)
{
    return -1;
}
#if R_BORDER_ROUTER_ENABLED
char* tls_connection_peer_serial_num(void* tls_ctx, struct tls_connection* conn)
{
    return NULL;
}
#endif // R_BORDER_ROUTER_ENABLED

/******************************************************************************
* Supplicant API (No usage of WPA)
******************************************************************************/

/**
 * Create and initialize a new TLS connection and set mbed TLS callbacks
 * @param tls_ctx The TLS context
 * @return Pointer to the new TLS connection structure or NULL on error
 */
static struct tls_connection* tls_client_connection_init(r_tls_context_t* tls_ctx)
{
    if (tls_ctx == NULL)
    {
        return NULL;
    }

    r_tls_context_t* context = tls_ctx;
    mbedtls_ssl_context* ssl = &context->ssl;
    struct tls_connection* conn;

    conn = os_zalloc(sizeof(*conn));
    if (conn == NULL)
    {
        return NULL;
    }

    mbedtls_ssl_set_bio(ssl, conn, mbedtls_send_func, mbedtls_recv_func, NULL);
    return conn;
}

/**
 * Initialize a new TLS session.
 * @param client_cert The encoded client certificate
 * @param cert_index The certificate index
 * @param[out] tls The TLS state
 * @return 0 on success, -1 on failure
 */
int tls_client_init(const uint8_t* client_cert, const uint8_t* cert_index, struct r_tls_state_s* tls)
{

    if (tls == NULL)
    {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN
        r_loggen_903();
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN */
        return -1;
    }
    tls->ctx = tls_init(NULL);
    if (tls->ctx == NULL)
    {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR
        r_loggen_909();
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR */
        return -1;
    }
    tls->conn = tls_client_connection_init(tls->ctx);
    if (tls->conn == NULL)
    {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR
        r_loggen_915();
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR */
        tls_client_reset(tls);
        tls->ctx = NULL;
        return -1;
    }
    struct tls_connection_params tls_params;

    /* set certificate information (misuse file name pointers) */
    tls_params.client_cert = (const char*)(client_cert);
    tls_params.private_key = (const char*)(cert_index);
    if (tls_connection_set_params(tls->ctx, tls->conn, &tls_params))
    {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR
        r_loggen_927();
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR */
        tls_client_reset(tls);
        return -1;
    }
    return 0;
}

/**
 * Destroy a TLS session. All state variables are freed set to NULL
 * @param[in, out] tls The TLS state
 */
void tls_client_reset(struct r_tls_state_s* tls)
{
    if (tls == NULL)
    {
        return;
    }
    tls_connection_deinit(NULL, tls->conn);
    tls->conn = NULL;
    tls_deinit(tls->ctx);
    tls->ctx = NULL;
}

/**
 * Determine whether the TLS connections is established or not
 * @param connection The TLS connection
 * @return 0, if the connection is not established. Non-zero otherwise
 */
int tls_client_connection_established(struct tls_connection* connection)
{
    return tls_connection_established(NULL, connection);
}

/**
 * Export the TLS connection key
 * @param tls The TLS state
 * @param[out] out The output buffer to hold the connection key
 * @param out_len The size of the output buffer
 * @return 0 on success. -1 on error
 */
int tls_client_export_key(struct r_tls_state_s* tls, uint8_t* out, size_t out_len)
{
    return tls_connection_export_key(tls->ctx, tls->conn, "client EAP encryption", out, out_len);
}

/**
 * Perform the TLS connection handshake for the client
 * @param tls The TLS state
 * @param in_data Incoming data. Ownership is transferred to this function so the data MUST NOT be used afterwards.
 * @return 0 on success. mbed TLS error code otherwise.
 */
int tls_client_handshake(struct r_tls_state_s* tls, struct wpabuf* in_data)
{
    if (tls == NULL || tls->ctx == NULL || tls->conn == NULL)
    {
        return -1;
    }
    struct tls_connection* conn = tls->conn;
    struct mbedtls_ssl_context* ssl = &tls->ctx->ssl;

    /* If there is data coming from the application, make sure it is available before the next handshake step */
    if (in_data && wpabuf_len(in_data) > 0)
    {
        if (conn->pull_buf)
        {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
            r_loggen_992(__func__, (uint32_t)wpabuf_len(conn->pull_buf));
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
            wpabuf_free(conn->pull_buf);
        }

        conn->pull_buf = in_data;
        conn->pull_buf_offset = wpabuf_head(conn->pull_buf);
    }

    int ret;
    do
    {
        ret = mbedtls_ssl_handshake_step(ssl);

        /*
         * Make a copy of the random bytes as soon as they become available. They are complete after the Server Hello has
         * been parsed/written.
         */
        if (ssl->state == MBEDTLS_SSL_SERVER_HELLO + 1)
        {
            memcpy(&conn->randbytes, &ssl->handshake->randbytes[0], 64);
        }

        /*
         * Make a copy of the function used to compute the prf as soon as it becomes available.
         * This pointer is set to the appropriate function SHA256 (for example) and it is used
         * internally by mbedtls. After a couple more rounds of the handshake state machine,
         * this pointer is cleared. Therefore, we cannot set it at the beginning or the end
         * of the handshake. We have to do that as soon as it becomes available.
         */
        if (ssl->handshake && ssl->handshake->tls_prf)
        {
            tls->ctx->tls_prf = ssl->handshake->tls_prf;
        }

    }
    while (ret == 0 && ssl->state != MBEDTLS_SSL_HANDSHAKE_OVER);

    if (ret != 0 && ret != MBEDTLS_ERR_SSL_WANT_READ && ret != MBEDTLS_ERR_SSL_WANT_WRITE && ret != MBEDTLS_ERR_SSL_HELLO_VERIFY_REQUIRED)
    {
        conn->failed++;
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR
        r_loggen_1032(-ret);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR */
        return ret;
    }

    if (ssl->state == MBEDTLS_SSL_HANDSHAKE_OVER)
    {
        conn->established = 1;
    }
    return ret;
}
