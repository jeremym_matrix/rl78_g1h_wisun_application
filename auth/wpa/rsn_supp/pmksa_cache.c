/*
 * WPA Supplicant - RSN PMKSA cache
 * Copyright (c) 2004-2009, 2011-2015, Jouni Malinen <j@w1.fi>
 *
 * This software may be distributed under the terms of the BSD license.
 * See README for more details.
 */

#include "includes.h"

#include "common.h"
#include "eapol_supp/eapol_supp_sm.h"
#include "wpa.h"
#include "wpa_i.h"
#include "pmksa_cache.h"

#if defined(IEEE8021X_EAPOL) && !defined(CONFIG_NO_WPA)

static const int pmksa_cache_max_entries = 32;

struct rsn_pmksa_cache
{
    struct rsn_pmksa_cache_entry* pmksa; /* PMKSA cache */
    int pmksa_count;                     /* number of entries in PMKSA cache */
    struct wpa_sm*                sm;    /* TODO: get rid of this reference(?) */

    void  (* free_cb)(struct rsn_pmksa_cache_entry* entry, void* ctx,
                      enum pmksa_free_reason reason);
    void* ctx;
};

// LCOV_EXCL_START

static void pmksa_cache_set_expiration(struct rsn_pmksa_cache* pmksa);

static void pmksa_cache_set_expiration(struct rsn_pmksa_cache* pmksa)
{
    int sec;
    struct rsn_pmksa_cache_entry* entry;
    struct os_reltime now;

    if (pmksa->pmksa == NULL)
    {
        return;
    }
    os_get_reltime(&now);
    sec = pmksa->pmksa->expiration - now.sec;
    if (sec < 0)
    {
        sec = 0;
    }

    entry = pmksa->sm->cur_pmksa ? pmksa->sm->cur_pmksa :
            pmksa_cache_get(pmksa, pmksa->sm->bssid, NULL, NULL, 0);
    if (entry)
    {
        sec = pmksa->pmksa->reauth_time - now.sec;
        if (sec < 0)
        {
            sec = 0;
        }
    }
}


/**
 * pmksa_cache_add - Add a PMKSA cache entry
 * @pmksa: Pointer to PMKSA cache data from pmksa_cache_init()
 * @pmk: The new pairwise master key
 * @pmk_len: PMK length in bytes, usually PMK_LEN (32)
 * @pmkid: Calculated PMKID
 * @kck: Key confirmation key or %NULL if not yet derived
 * @kck_len: KCK length in bytes
 * @aa: Authenticator address
 * @spa: Supplicant address
 * @network_ctx: Network configuration context for this PMK
 * @akmp: WPA_KEY_MGMT_* used in key derivation
 * @cache_id: Pointer to FILS Cache Identifier or %NULL if not advertised
 * Returns: Pointer to the added PMKSA cache entry or %NULL on error
 *
 * This function create a PMKSA entry for a new PMK and adds it to the PMKSA
 * cache. If an old entry is already in the cache for the same Authenticator,
 * this entry will be replaced with the new entry. PMKID will be calculated
 * based on the PMK and the driver interface is notified of the new PMKID.
 */
struct rsn_pmksa_cache_entry* pmksa_cache_add(struct rsn_pmksa_cache* pmksa, const u8* pmk, size_t pmk_len,
                                              const u8* pmkid, const u8* kck, size_t kck_len,
                                              const u8* aa, const u8* spa, void* network_ctx, int akmp,
                                              const u8* cache_id)
{
    struct rsn_pmksa_cache_entry* entry;
    struct os_reltime now;

    if (pmk_len > PMK_LEN_MAX)
    {
        return NULL;
    }

    entry = os_zalloc(sizeof(*entry));
    if (entry == NULL)
    {
        return NULL;
    }
    os_memcpy(entry->pmk, pmk, pmk_len);
    entry->pmk_len = pmk_len;
    if (pmkid)
    {
        os_memcpy(entry->pmkid, pmkid, PMKID_LEN);
    }
    else
    {
        rsn_pmkid(pmk, pmk_len, aa, spa, entry->pmkid, akmp);
    }
    os_get_reltime(&now);
    entry->expiration = now.sec + pmksa->sm->dot11RSNAConfigPMKLifetime;
    entry->reauth_time = now.sec + pmksa->sm->dot11RSNAConfigPMKLifetime *
                         pmksa->sm->dot11RSNAConfigPMKReauthThreshold / 100;
    entry->akmp = akmp;
    os_memcpy(entry->aa, aa, ETH_ALEN);
    entry->network_ctx = network_ctx;

    return pmksa_cache_add_entry(pmksa, entry);
}


struct rsn_pmksa_cache_entry* pmksa_cache_add_entry(struct rsn_pmksa_cache*       pmksa,
                                                    struct rsn_pmksa_cache_entry* entry)
{
    struct rsn_pmksa_cache_entry* pos, * prev;

    if (pmksa->pmksa_count >= pmksa_cache_max_entries && pmksa->pmksa)
    {
        return NULL;
    }

    /* Add the new entry; order by expiration time */
    pos = pmksa->pmksa;
    prev = NULL;
    while (pos)
    {
        if (pos->expiration > entry->expiration)
        {
            break;
        }
        prev = pos;
        pos = pos->next;
    }
    if (prev == NULL)
    {
        entry->next = pmksa->pmksa;
        pmksa->pmksa = entry;
        pmksa_cache_set_expiration(pmksa);
    }
    else
    {
        entry->next = prev->next;
        prev->next = entry;
    }
    pmksa->pmksa_count++;
    wpa_printf(MSG_DEBUG, "RSN: Added PMKSA cache entry for " MACSTR
               " network_ctx=%p", MAC2STR(entry->aa), entry->network_ctx);
    wpa_sm_add_pmkid(pmksa->sm, entry->network_ctx, entry->aa, entry->pmkid,
                     entry->fils_cache_id_set ? entry->fils_cache_id : NULL,
                     entry->pmk, entry->pmk_len);

    return entry;
}

/**
 * pmksa_cache_deinit - Free all entries in PMKSA cache
 * @pmksa: Pointer to PMKSA cache data from pmksa_cache_init()
 */
void pmksa_cache_deinit(struct rsn_pmksa_cache* pmksa)
{
    struct rsn_pmksa_cache_entry* entry, * prev;

    if (pmksa == NULL)
    {
        return;
    }

    entry = pmksa->pmksa;
    pmksa->pmksa = NULL;
    while (entry)
    {
        prev = entry;
        entry = entry->next;
        os_free(prev);
    }
    pmksa_cache_set_expiration(pmksa);
    os_free(pmksa);
}


/**
 * pmksa_cache_get - Fetch a PMKSA cache entry
 * @pmksa: Pointer to PMKSA cache data from pmksa_cache_init()
 * @aa: Authenticator address or %NULL to match any
 * @pmkid: PMKID or %NULL to match any
 * @network_ctx: Network context or %NULL to match any
 * @akmp: Specific AKMP to search for or 0 for any
 * Returns: Pointer to PMKSA cache entry or %NULL if no match was found
 */
struct rsn_pmksa_cache_entry* pmksa_cache_get(struct rsn_pmksa_cache* pmksa,
                                              const u8* aa, const u8* pmkid,
                                              const void* network_ctx,
                                              int akmp)
{
    struct rsn_pmksa_cache_entry* entry = pmksa->pmksa;
    while (entry)
    {
        if ((aa == NULL || os_memcmp(entry->aa, aa, ETH_ALEN) == 0) &&
            (pmkid == NULL ||
             os_memcmp(entry->pmkid, pmkid, PMKID_LEN) == 0) &&
            (!akmp || akmp == entry->akmp) &&
            (network_ctx == NULL || network_ctx == entry->network_ctx))
        {
            return entry;
        }
        entry = entry->next;
    }
    return NULL;
}

/**
 * pmksa_cache_init - Initialize PMKSA cache
 * @free_cb: Callback function to be called when a PMKSA cache entry is freed
 * @ctx: Context pointer for free_cb function
 * @sm: Pointer to WPA state machine data from wpa_sm_init()
 * Returns: Pointer to PMKSA cache data or %NULL on failure
 */
struct rsn_pmksa_cache* pmksa_cache_init(void (* free_cb)(struct rsn_pmksa_cache_entry* entry,
                                                          void* ctx, enum pmksa_free_reason reason),
                                         void* ctx, struct wpa_sm* sm)
{
    struct rsn_pmksa_cache* pmksa;

    pmksa = os_zalloc(sizeof(*pmksa));
    if (pmksa)
    {
        pmksa->free_cb = free_cb;
        pmksa->ctx = ctx;
        pmksa->sm = sm;
    }

    return pmksa;
}

// LCOV_EXCL_STOP
#endif /* IEEE8021X_EAPOL */
