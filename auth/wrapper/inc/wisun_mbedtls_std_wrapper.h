#ifndef WISUN_MBEDTLS_STD_WRAPPER_H
#define WISUN_MBEDTLS_STD_WRAPPER_H

#include <stddef.h>

void  authwrap_free(void* ptr);
void* authwrap_calloc(size_t nmemb, size_t size);
int   os_snprintf(char* str, size_t size, const char* format, ...);


#endif /* WISUN_MBEDTLS_STD_WRAPPER_H */
