/***********************************************************************/
/*                                                                     */
/*  FILE        : r_eeprom_wrapper.c                                   */
/*  DATE        :Tue, Oct 31, 2006                                     */
/*  DESCRIPTION :                                                      */
/*  CPU TYPE    :                                                      */
/*                                                                     */
/*  NOTE:THIS IS A TYPICAL EXAMPLE.                                    */
/*                                                                     */
/***********************************************************************/

#if R_FLASH_ENABLED == 2

#include <stdint.h>
#include <stddef.h>

#include "iodefine.h"

#include "r_os_wrapper.h"
#include "r_rx_compiler.h"
#include "r_sci_iic_rx_if.h"

#include "r_eeprom_wrapper.h"

/* State definitions of EEPROM access */
typedef enum
{
    IDLE = 0U,          /* Being in idle state */
    BUSY,               /* I2C communication being performed */
    INITIALIZE,         /* Initialization of Simple I2C FIT module */
    DEVICE_WRITE,       /* Writing to the EEPROM */
    DEVICE_READ,        /* Reading from the EEPROM */
    RETRY_WAIT_DEV_WR,  /* Waiting for retry writing EEPROM */
    RETRY_WAIT_DEV_RD,  /* Waiting for retry reading EEPROM */
    CLOSE,              /* Close the Simple I2C FIT module */
    FINISH,             /* Communication completed */
    ERROR               /* Error occurred */
} access_status_t;

/* Defines the number of retries when a NACK is detected. */
#define RETRY_TMO    5

/* Defines the number of busy waits till time out is detected (approximately 5 seconds) */
#define BUSY_TMO     0x01000000

/* Defines the number of software loops to wait until next communication starts when retrying*/
#define RETRY_WAIT_TIME 1000u

/* SCI channel definition */
#define SCI_IIC_CH  12u

/* Port definitions used for IIC communication */
#define PORT_EE_WRITE           PORTA.PODR.BIT.B1
#define PORT_EE_WRITE_DIR       PORTA.PDR.BIT.B1

#define PORT_SCL                PORTE.PODR.BIT.B2
#define PORT_SCL_DIR            PORTE.PDR.BIT.B2
#define PORT_SCL_MODE           PORTE.PMR.BIT.B2

#define PORT_SDA                PORTE.PODR.BIT.B1
#define PORT_SDA_DIR            PORTE.PDR.BIT.B1
#define PORT_SDA_MODE           PORTE.PMR.BIT.B1

#define PERIPHERAL_FUNCTION     1
#define PORT_FUNCTION           0

#define PORT_OUTPUT             1
#define PORT_INPUT              0

#define PORT_HI                 1
#define PORT_LO                 0

/* EEPROM device address */
const uint8_t EE_PROM_ADDR = (0xAE >> 1);

/* Information structure of EEPROM M24C64 */
sci_iic_info_t iic_info_device_M24C64;

/* Variable for EEPROM access type and current state */
volatile uint8_t access_state;
volatile uint8_t access_type;

/* Prototype definitions */
sci_iic_return_t r_eeprom_access(access_status_t type, sci_iic_info_t *iic_info_deviceA);
void Callback_device(void);

/* Open EEPROM communication */
/* ------------------------- */
sci_iic_return_t r_eeprom_open(void)
{
    sci_iic_return_t res;

    /* Force Stop condition */
    r_eeprom_force_stop();

    /* Unlock register protection */
    SYSTEM.PRCR.WORD = 0xA503;
    /* Enable SCI12 */
    MSTP(SCI12) = 0;
    /* Lock register protection */
    SYSTEM.PRCR.WORD = 0xA500;

    /* Initial EEPROM write signal */
    PORT_EE_WRITE_DIR = PORT_OUTPUT;
    PORT_EE_WRITE = PORT_HI;

    /* Configure the EEPROM device information structure */
    iic_info_device_M24C64.p_slv_adr = (uint8_t*)&EE_PROM_ADDR;
    iic_info_device_M24C64.p_data1st = NULL;
    iic_info_device_M24C64.p_data2nd = NULL;
    iic_info_device_M24C64.dev_sts = SCI_IIC_NO_INIT;
    iic_info_device_M24C64.cnt1st = 0;
    iic_info_device_M24C64.cnt2nd = 0;
    iic_info_device_M24C64.callbackfunc = &Callback_device;
    iic_info_device_M24C64.ch_no = SCI_IIC_CH;

    res = r_eeprom_access((access_status_t)INITIALIZE, &iic_info_device_M24C64);

    return res;
}

/* EEPROM Write operation */
/* ---------------------- */
sci_iic_return_t r_eeprom_write(uint8_t *addr, uint8_t *data, uint32_t size)
{
    sci_iic_return_t res;

    /* Configure the EEPROM device information structure */
    iic_info_device_M24C64.p_slv_adr = (uint8_t*)&EE_PROM_ADDR;
    iic_info_device_M24C64.p_data1st = addr;
    iic_info_device_M24C64.p_data2nd = data;
    iic_info_device_M24C64.cnt1st = 2;
    iic_info_device_M24C64.cnt2nd = size;
    iic_info_device_M24C64.callbackfunc = &Callback_device;
    iic_info_device_M24C64.ch_no = SCI_IIC_CH;

    PORT_EE_WRITE = PORT_LO;

    res = r_eeprom_access((access_status_t)DEVICE_WRITE, &iic_info_device_M24C64);

    PORT_EE_WRITE = PORT_HI;

    /* Wait for 5ms to fulfill write timing of M24C64 EEPROM */
    R_OS_DelayTaskMs(5);

    return res;
}

/* EEPROM Read operation */
/* --------------------- */
sci_iic_return_t r_eeprom_read(uint8_t *addr, uint8_t *data, uint32_t size)
{
    sci_iic_return_t res;

    /* Configure the EEPROM device information structure */
    iic_info_device_M24C64.p_slv_adr = (uint8_t*)&EE_PROM_ADDR;
    iic_info_device_M24C64.p_data1st = addr;
    iic_info_device_M24C64.p_data2nd = data;
    iic_info_device_M24C64.cnt1st = 2;
    iic_info_device_M24C64.cnt2nd = size;
    iic_info_device_M24C64.callbackfunc = &Callback_device;
    iic_info_device_M24C64.ch_no = SCI_IIC_CH;

    res = r_eeprom_access((access_status_t)DEVICE_READ, &iic_info_device_M24C64);

    return res;
}

/* Close EEPROM communication */
/* -------------------------- */
sci_iic_return_t r_eeprom_close(void)
{
    sci_iic_return_t res;

    /* Configure the EEPROM device information structure */
    iic_info_device_M24C64.p_slv_adr = (uint8_t*)&EE_PROM_ADDR;
    iic_info_device_M24C64.p_data1st = NULL;
    iic_info_device_M24C64.p_data2nd = NULL;
    iic_info_device_M24C64.cnt1st = 0;
    iic_info_device_M24C64.cnt2nd = 0;
    iic_info_device_M24C64.callbackfunc = &Callback_device;
    iic_info_device_M24C64.ch_no = SCI_IIC_CH;

    res = r_eeprom_access((access_status_t)CLOSE, &iic_info_device_M24C64);

    return res;
}

/* EEPROM access state machine */
/* --------------------------- */
sci_iic_return_t r_eeprom_access(access_status_t operation, sci_iic_info_t *iic_info_device)
{
    /* For verifying the return value of the API function */
    sci_iic_return_t ret;

    /* Variable for the number of retries */
    uint32_t retry_cnt = 0;

    /* Counter for adjusting the retry interval */
    uint32_t retry_wait_cnt = 0;

    /* Counter for busy waiting */
    uint32_t busy_wait_cnt = 0;

    /* store current EEPROM operation */
    access_type = operation;
    access_state = access_type;

    while(1)
    {
        switch(access_state)
        {
            /* Idle state */
            case IDLE:
                return ret;
                break;

            /* I2C communication being performed */
            case BUSY:
                busy_wait_cnt++;
                if (BUSY_TMO < busy_wait_cnt)
                {
                    busy_wait_cnt = 0;
                    ret = SCI_IIC_ERR_BUS_BUSY;
                    access_state = ERROR; /* Proceed to error processing */
                }
                break;

            /* Initializes the simple I2C mode FIT module. */
            case INITIALIZE:
                /* SCI open processing */
                ret = R_SCI_IIC_Open(iic_info_device);
                if (SCI_IIC_SUCCESS == ret)
                {
                    access_state = IDLE; /* Then the state becomes “I2C communication being performed”. */
                }
                else
                {
                    /* Error processing at the R_SCI_IIC_Open() function call */
                    access_state = ERROR; /* Proceed to error processing */
                }
                break;

            /* Write data (Start a Master Transmit Access) */
            case DEVICE_WRITE:
                /* Starts master transmission */
                ret = R_SCI_IIC_MasterSend(iic_info_device);
                if (SCI_IIC_SUCCESS == ret)
                {
                    access_state = BUSY; /* The state becomes “I2C communication being performed”. */
                }
                else if (SCI_IIC_ERR_BUS_BUSY == ret)
                {
                    access_state = RETRY_WAIT_DEV_WR; /* Proceed to a wait for retry */
                }
                else
                {
                    /* Error processing at R_SCI_IIC_MasterSend() function call */
                    access_state = ERROR; /* Proceed to error processing */
                }
                break;

            /* Read data (Start a Master Receive Access) */
            case DEVICE_READ:
                /* Starts master reception */
                ret = R_SCI_IIC_MasterReceive (iic_info_device);
                if (SCI_IIC_SUCCESS == ret)
                {
                    access_state = BUSY; /* The state becomes “I2C communication being performed”. */
                }
                else if (SCI_IIC_ERR_BUS_BUSY == ret)
                {
                    access_state = RETRY_WAIT_DEV_RD; /* Proceed to a wait for retry */
                }
                else
                {
                    /* Error processing at R_SCI_IIC_MasterReceive() function call */
                    access_state = ERROR; /* Proceed to error processing */
                }
                break;

            /* Waits for retry writing / retry reading the device */
            case RETRY_WAIT_DEV_WR:
            case RETRY_WAIT_DEV_RD:
                retry_wait_cnt++;
                if (RETRY_TMO < retry_cnt)
                {
                    retry_wait_cnt = 0;
                    access_state = ERROR; /* Proceed to error processing */
                    break;
                }
                if (RETRY_WAIT_TIME < retry_wait_cnt)
                {
                    retry_cnt++;
                    retry_wait_cnt = 0;

                    /* Close the Simple I2C FIT module */
                    ret = R_SCI_IIC_Close(iic_info_device);
                    if (SCI_IIC_SUCCESS != ret)
                    {
                        /* Error processing at the R_SCI_IIC_Open() function call */
                        access_state = ERROR; /* Proceed to error processing */
                        break;
                    }

                    /* Force Stop Condition */
                    r_eeprom_force_stop();

                    /*  Re-Initialize the Simple I2C FIT module */
                    iic_info_device->dev_sts = SCI_IIC_NO_INIT;
                    ret = R_SCI_IIC_Open(iic_info_device);
                    if (SCI_IIC_SUCCESS != ret)
                    {
                        /* Error processing at the R_SCI_IIC_Open() function call */
                        access_state = ERROR; /* Proceed to error processing */
                        break;
                    }

                    switch (access_state)
                    {
                        case RETRY_WAIT_DEV_WR:
                            access_state = DEVICE_WRITE; /* Proceed write processing to the device */
                            break;
                        case RETRY_WAIT_DEV_RD:
                            access_state = DEVICE_READ;  /* Proceed read processing from the device */
                            break;
                        default:
                            /* No operation is performed. */
                            break;
                    }
                }
                break;

            case CLOSE:
                /* SCI close processing */
                ret = R_SCI_IIC_Close(iic_info_device);
                if (SCI_IIC_SUCCESS == ret)
                {
                    access_state = IDLE; /* Then the state becomes “idle”. */
                }
                else
                {
                    /* Error processing at the R_SCI_IIC_Close() function call */
                    access_state = ERROR; /* Proceed to error processing */
                }
                break;

            /* Finished operation */
            case FINISH:
                access_state = IDLE; /* Then the state becomes “idle”. */
                break;

            /* Error occurred */
            case ERROR:
                return ret;
                break;

            default:
                /* No operation is performed. */
                break;
        }
    }
}

/* Force EEPROM stop */
/* ------------------*/
void r_eeprom_force_stop(void)
{
    /* Manually force Stop Condition to terminate all pending communications
     * and set the EEPROM in a defined state */

    //SSCL12 (port PE2)
    PORT_SCL = PORT_HI;
    PORT_SCL_DIR = PORT_OUTPUT;
    PORT_SCL_MODE = PORT_FUNCTION;

    //SSDA12 (port PE1)
    PORT_SDA = PORT_LO;
    PORT_SDA_DIR = PORT_OUTPUT;
    PORT_SDA_MODE = PORT_FUNCTION;

    for(uint32_t wait=0; wait < RETRY_WAIT_TIME; wait++);
    PORT_SCL = PORT_LO;
    for(uint32_t wait=0; wait < RETRY_WAIT_TIME; wait++);
    PORT_SCL = PORT_HI;
    for(uint32_t wait=0; wait < RETRY_WAIT_TIME; wait++);
    PORT_SDA = PORT_HI;
}

/* IIC Callback function */
/* ----------------------*/
void Callback_device(void)
{
    sci_iic_return_t ret;
    sci_iic_mcu_status_t iic_status;
    sci_iic_info_t iic_info_ch;

    iic_info_ch.ch_no = SCI_IIC_CH;

    /* Obtains the simple I2C status. */
    ret = R_SCI_IIC_GetStatus(&iic_info_ch, &iic_status);
    if (SCI_IIC_SUCCESS != ret)
    {
        /* Error processing at the R_SCI_IIC_GetStatus() function call */
        access_state = ERROR; /* Proceed to error processing */
    }
    else
    {
        if (1 == iic_status.BIT.NACK)
        {
            /* Processing when NACK is detected with the iic_status flag verification. */
            if( access_type == DEVICE_READ)
            {
                access_state = FINISH;
            }
            else
            {
                access_state = RETRY_WAIT_DEV_WR;
            }
        }
        else
        {
            if( access_type == DEVICE_READ)
            {
                access_state = RETRY_WAIT_DEV_RD;
            }
            else
            {
                access_state = FINISH;
            }
        }
    }
}

#endif /* R_FLASH_ENABLED == 2 */


