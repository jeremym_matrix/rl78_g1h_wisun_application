/***********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products.
 * No other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all 
 * applicable laws, including copyright laws. 
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM 
 * EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES 
 * SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO 
 * THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of 
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the 
 * following link:
 * http://www.renesas.com/disclaimer 
 *
 * Copyright (C) 2016(2019) Renesas Electronics Corporation. All rights reserved.
 **********************************************************************************************************************/
/***********************************************************************************************************************
 * File Name    : r_sci_iic_rx65n.c
 * Description  : Functions for using SCI_IIC on RX devices. 
 **********************************************************************************************************************/
/***********************************************************************************************************************
 * History : DD.MM.YYYY Version  Description
 *         : 01.10.2016 2.00     First Release
 *         : 31.08.2017 2.20     Changed include path becuase changed file structure.
 *                               Changed about the calculation processing for address of PFS, PCR, PDR, ODR0, ODR1,
 *                                DSCR and PMR register.
 *                               Corrected the drive capacity control setting process by r_sci_iic_io_open() function.
 *         : 27.04.2018 2.30     Added "for", "while" and "do while" comment.
 *         : 20.05.2019 2.41     Added support for GNUC and ICCRX.
 *                               Fixed coding style.
 **********************************************************************************************************************/
/***********************************************************************************************************************
 Includes   <System Includes> , "Project Includes"
 **********************************************************************************************************************/

#if R_FLASH_ENABLED == 2

/* Fixed width integers */
#include <stdint.h>
/* Boolean defines */
#include <stdbool.h>

/* Access to peripherals and board defines. */
#include "mcu_info.h"
#include "r_rx_compiler.h"
#include "r_rx_intrinsic_functions.h"

#include "iodefine.h"

/* Defines for SCI_IIC support */
#include "r_sci_iic_rx_pin_config.h"
#include "r_sci_iic_rx_private.h"
#include "r_sci_iic_rx65n_private.h"
#include "r_sci_iic_rx_if.h"
#include "r_sci_iic_rx_config.h"

/***********************************************************************************************************************
 Private global variables and functions
 **********************************************************************************************************************/
static void sci_iic_set_frequency (sci_iic_info_t * p_sci_iic_info);
static void sci_iic_clear_ir_flag (sci_iic_info_t * p_sci_iic_info);

/***********************************************************************************************************************
 Exported global variables (to be accessed by other files)
 **********************************************************************************************************************/

/* rom info */
const sci_iic_ch_rom_t g_sci_iic_ch12_rom =
{
    (sci_regs_t)&SCI12,
    (volatile uint32_t R_BSP_EVENACCESS_SFR *)&SYSTEM.MSTPCRB.LONG, BIT4_MASK,
    &ICU.IPR[IPR_SCI12_TXI12].BYTE,
    &ICU.IR[IR_SCI12_TXI12].BYTE,
    (volatile uint32_t R_BSP_EVENACCESS_SFR *)&ICU.GRPBL0.LONG, BIT16_MASK,
    &ICU.IER[IER_SCI12_TXI12].BYTE, BIT5_MASK,
    (volatile uint32_t R_BSP_EVENACCESS_SFR *)&ICU.GENBL0.LONG, BIT16_MASK,
    SCI_IIC_CFG_CH12_INT_PRIORITY,
    SCI_IIC_CFG_CH12_SSCL_GP,
    SCI_IIC_CFG_CH12_SSCL_PIN,
    SCI_IIC_MPC_SSCL12_ENABLE,
    SCI_IIC_CFG_CH12_SSDA_GP,
    SCI_IIC_CFG_CH12_SSDA_PIN,
    SCI_IIC_MPC_SSDA12_ENABLE,
    SCI_IIC_CFG_CH12_SSDA_DELAY_SELECT,
    SCI_IIC_CFG_CH12_BITRATE_BPS,
    SCI_IIC_CFG_CH12_DIGITAL_FILTER,
    SCI_IIC_CFG_CH12_FILTER_CLOCK
};

/* channel control block */
sci_iic_ch_ctrl_t g_sci_iic_ch12_ctrl =
{
    &g_sci_iic_ch12_rom,
    SCI_IIC_EV_INIT,
    SCI_IIC_MODE_NONE,
    SCI_IIC_STS_NO_INIT,
    SCI_IIC_STS_NO_INIT,
    (uint8_t)INT_ZERO,
    (sci_iic_info_t *)NULL
};

/*----------------------------------------------------------------------------*/
/*   Array for start addresses of information for each SCI IIC channel        */
/*----------------------------------------------------------------------------*/
const sci_iic_hdl_t g_sci_iic_handles[SCI_IIC_NUM_CH_MAX] =
{
    #if SCI_IIC_CFG_CH0_INCLUDED
    &g_sci_iic_ch0_ctrl,
    #else
    NULL,
    #endif
    #if SCI_IIC_CFG_CH1_INCLUDED
    &g_sci_iic_ch1_ctrl,
    #else
    NULL,
    #endif
    #if SCI_IIC_CFG_CH2_INCLUDED
    &g_sci_iic_ch2_ctrl,
    #else
    NULL,
    #endif
    #if SCI_IIC_CFG_CH3_INCLUDED
    &g_sci_iic_ch3_ctrl,
    #else
    NULL,
    #endif
    #if SCI_IIC_CFG_CH4_INCLUDED
    &g_sci_iic_ch4_ctrl,
    #else
    NULL,
    #endif
    #if SCI_IIC_CFG_CH5_INCLUDED
    &g_sci_iic_ch5_ctrl,
    #else
    NULL,
    #endif
    #if SCI_IIC_CFG_CH6_INCLUDED
    &g_sci_iic_ch6_ctrl,
    #else
    NULL,
    #endif
    #if SCI_IIC_CFG_CH7_INCLUDED
    &g_sci_iic_ch7_ctrl,
    #else
    NULL,
    #endif
    #if SCI_IIC_CFG_CH8_INCLUDED
    &g_sci_iic_ch8_ctrl,
    #else
    NULL,
    #endif
    #if SCI_IIC_CFG_CH9_INCLUDED
    &g_sci_iic_ch9_ctrl,
    #else
    NULL,
    #endif
    #if SCI_IIC_CFG_CH10_INCLUDED
    &g_sci_iic_ch10_ctrl,
    #else
    NULL,
    #endif
    #if SCI_IIC_CFG_CH11_INCLUDED
    &g_sci_iic_ch11_ctrl,
    #else
    NULL,
    #endif
    #if SCI_IIC_CFG_CH12_INCLUDED
    &g_sci_iic_ch12_ctrl
    #else
    NULL
    #endif
};

/***********************************************************************************************************************
 * Function Name: r_sci_iic_check_arguments
 * Description  : check to parameter.
 * Arguments    : sci_iic_info_t * p_sci_iic_info    ; IIC Information
 *              ; sci_iic_api_mode_t called_api      ; Internal Mode
 * Return Value : none
 **********************************************************************************************************************/
sci_iic_return_t r_sci_iic_check_arguments (sci_iic_info_t * p_sci_iic_info, sci_iic_api_mode_t called_api)
{

    if (NULL == p_sci_iic_info)
    {
        return SCI_IIC_ERR_INVALID_ARG;
    }

    if (SCI_IIC_MODE_RECEIVE == called_api)
    {

        if (((NULL == p_sci_iic_info->p_slv_adr) || (NULL == p_sci_iic_info->p_data2nd))
                || ((uint32_t)NULL == p_sci_iic_info->cnt2nd))
        {
            return SCI_IIC_ERR_INVALID_ARG;
        }

    }

    if ((SCI_IIC_MODE_SEND == called_api) || (SCI_IIC_MODE_RECEIVE == called_api))
    {

        if (NULL == p_sci_iic_info->callbackfunc)
        {
            return SCI_IIC_ERR_INVALID_ARG;
        }

    }

    if (SCI_IIC_NUM_CH12 < p_sci_iic_info->ch_no)
    {
        return SCI_IIC_ERR_INVALID_CHAN;
    }

    return SCI_IIC_SUCCESS;
} /* End of function r_sci_iic_check_arguments() */

/***********************************************************************************************************************
 * Function Name: r_sci_iic_power_on
 * Description  : Turns on power to a SCI_IIC channel.
 * Arguments    : channel -
 *                    Which channel to use.
 * Return Value : none
 **********************************************************************************************************************/
void r_sci_iic_power_on (uint8_t channel)
{
    R_BSP_VOLATILE_EVENACCESS const sci_iic_ch_rom_t * prom = g_sci_iic_handles[channel]->prom;
    uint32_t mstp;
    uint32_t stop_mask;

    /* Enable writing to MSTP registers. */
    SYSTEM.PRCR.WORD = 0xA503;

    /* Enable selected SCI_IIC Channel. */
    /* Bring module out of stop state. */
    mstp = (*prom->pmstp);
    stop_mask = prom->stop_mask;
    (*prom->pmstp) = mstp & (~stop_mask);

    /* Disable writing to MSTP registers. */
    SYSTEM.PRCR.WORD = 0xA500;

} /* End of function r_sci_iic_power_on() */

/***********************************************************************************************************************
 * Function Name: r_sci_iic_power_off
 * Description  : Turns off power to a SCI_IIC channel.
 * Arguments    : channel -
 *                    Which channel to use.
 * Return Value : none
 **********************************************************************************************************************/
void r_sci_iic_power_off (uint8_t channel)
{
    R_BSP_VOLATILE_EVENACCESS const sci_iic_ch_rom_t * prom = g_sci_iic_handles[channel]->prom;
    
    uint32_t mstp;
    uint32_t stop_mask;

    /* Enable writing to MSTP registers. */
    SYSTEM.PRCR.WORD = 0xA503;

    /* Disable selected SCI_IIC Channel. */
    /* Put module in stop state. */
    mstp = (*prom->pmstp);
    stop_mask = prom->stop_mask;
    (*prom->pmstp) = mstp | stop_mask;

    /* Disable writing to MSTP registers. */
    SYSTEM.PRCR.WORD = 0xA500;

} /* End of function r_sci_iic_power_off() */

/***********************************************************************************************************************
 * Function Name: r_sci_iic_io_open
 * Description  : Open Port Processing
 *                Sets ports to input mode. Ports input pull-up becomes "Off".
 * Arguments    : uint8_t port_gr      ;   port group
 *                uint8_t pin_num      ;   pin number
 * Return Value : None
 **********************************************************************************************************************/
void r_sci_iic_io_open (uint8_t port_gr, uint8_t pin_num)
{
    R_BSP_VOLATILE_EVENACCESS uint8_t * const ppcr = (uint8_t *)((uint32_t)SCI_IIC_PRV_PCR_BASE_REG + (uint32_t)port_gr);
    R_BSP_VOLATILE_EVENACCESS uint8_t * const ppdr = (uint8_t *)((uint32_t)SCI_IIC_PRV_PDR_BASE_REG + (uint32_t)port_gr);
    R_BSP_VOLATILE_EVENACCESS uint8_t * const podr0 = (uint8_t *)((uint32_t)SCI_IIC_PRV_ODR0_BASE_REG
                                                           + (uint32_t)(port_gr * 2));
    R_BSP_VOLATILE_EVENACCESS uint8_t * const podr1 = (uint8_t *)((uint32_t)SCI_IIC_PRV_ODR1_BASE_REG
                                                           + (uint32_t)(port_gr * 2));
    R_BSP_VOLATILE_EVENACCESS uint8_t * const pdscr = (uint8_t *)((uint32_t)SCI_IIC_PRV_DSCR_BASE_REG + (uint32_t)port_gr);

    /* Sets the port register */
    if ((PORT3_GR != port_gr) && (PORTF_GR != port_gr))
    {
        (*pdscr) |= (1U << pin_num); /* drive capacity control : High-drive output */
    }
    (*ppcr) &= (~(1U << pin_num)); /* input pull-up resister : off */
    (*ppdr) &= (~(1U << pin_num)); /* input mode */

    /* Set N-channel open-drain output */
    if (pin_num <= 3)
    {
        (*podr0) |= (0x01 << (2 * pin_num));
    }
    else
    {
        (*podr1) |= (0x01 << (2 * (pin_num & 0x03)));
    }

    /* dummy read */
    if (*podr1)
    {
        R_BSP_NOP();
    }
} /* End of function r_sci_iic_io_open() */

/***********************************************************************************************************************
 * Function Name: r_sci_iic_init_io_register
 * Description  : Initializes SCI_IIC register.
 * Arguments    : sci_iic_info_t * p_sci_iic_info     ;   IIC Information
 * Return Value : None
 **********************************************************************************************************************/
void r_sci_iic_init_io_register (sci_iic_info_t * p_sci_iic_info)
{
    sci_regs_t pregs = g_sci_iic_handles[p_sci_iic_info->ch_no]->prom->regs;
    R_BSP_VOLATILE_EVENACCESS const sci_iic_ch_rom_t * prom = g_sci_iic_handles[p_sci_iic_info->ch_no]->prom;

    /* Sets SCLn and SDAn to non-driven state. */
    /* SCR - Serial Control Register 
     b5     TE - Transmit Enable - Serial transmission is enabled. */
    pregs->SCR.BYTE = SCI_IIC_SCR_INIT;

    /* Sets a transfer clock. */
    /* Includes I/O register read operation at the end of the following function. */
    sci_iic_set_frequency(p_sci_iic_info);

    /* SEMR - Serial Extended Mode Register
     b5    Digital Noise Filter Function Enable -
     Noise cancellation function for the SSCLn and SSDAn input signals enabled. */
    pregs->SEMR.BIT.NFEN = prom->df_sel;

    /* SNFR - Noise Filter Setting Register
     b7;b3   Reserved - These bits are read as 0.
     b2:b0   NFCS - The clock signal divided by 1 is used with the noise filter.
     b0      IICM  - Simple I2C Mode Select - Serial Interface Mode */
    pregs->SNFR.BIT.NFCS = prom->df_clk;

    /* Resets all SCI_IIC registers and the internal status. */
    /* SIMR1 - I2C Mode Register 1
     b7;b3   IICDL - SSDA Delay Output Select
     b2:b1   Reserved - These bits are read as 0.
     b0      IICM  - Simple I2C Mode Select - Serial Interface Mode */
    pregs->SIMR1.BIT.IICM = 0; /* Do not use Simple IIC mode. */

    /* SIMR1 - I2C Mode Register 1
     b7:b3  IICDL - SSDA Delay Output Select - 2 to 3 cycles */
    pregs->SIMR1.BIT.IICDL = prom->ssda_delay;

    /* SIMR2 - I2C Mode Register 2
     b5      IICACKT - ACK Transmission Data - NACK transmission and reception of ACK/NACK */
    pregs->SIMR2.BIT.IICACKT = SCI_IIC_NACK_TRANS;

    /* SIMR2 - I2C Mode Register 2
     b1      IICCSC - Clock Synchronization - Enable Clock Synchronization */
    pregs->SIMR2.BIT.IICCSC = SCI_IIC_SYNCHRO;

    /* SIMR2 - I2C Mode Register 2
     b0      IICINTM - I2C Interrupt Mode Select - Use reception and transmission interrupts. */
    pregs->SIMR2.BIT.IICINTM = SCI_IIC_RCV_TRS_INTERRUPT;

    /* SISR - I2C Status Register
     b0      IICACKR - ACK Reception Data Flag - ACK received */
    pregs->SISR.BIT.IICACKR = SCI_IIC_ACK_RCV;

    /* SPMR - SPI Mode Register
     b7      CKPH   - Clock Phase Select - Clock is not delayed.
     b6      CKPOL  - Clock Polarity Select - Clock polarity is not inverted.
     b5      Reserved - These bits are read as 0.
     b4      MFF    - Mode Fault Flag - No mode-fault error.
     b3      Reserved - These bits are read as 0.
     b2      MSS - Master Slave Select - master mode.
     b1      CTSE - CTS Enable - CTS function is disabled (RTS output function is enabled).
     b0      SSE - SS Pin Function Enable - SS pin function is disabled. */
    pregs->SPMR.BYTE = 0x00;
} /* End of function r_sci_iic_init_io_register() */

/***********************************************************************************************************************
 * Function Name: r_sci_iic_clear_io_register
 * Description  : Reset SCI I/O register.
 * Arguments    : sci_iic_info_t * p_sci_iic_info     ;   IIC Information
 * Return Value : None
 **********************************************************************************************************************/
void r_sci_iic_clear_io_register (sci_iic_info_t * p_sci_iic_info)
{
    sci_regs_t pregs = g_sci_iic_handles[p_sci_iic_info->ch_no]->prom->regs;

    /* Initializes SCI I/O register */
    pregs->SCR.BYTE = SCI_IIC_SCR_INIT;
    pregs->SMR.BYTE = SCI_IIC_SMR_INIT;
    pregs->SCMR.BYTE = SCI_IIC_SCMR_INIT;
    pregs->BRR = SCI_IIC_BRR_INIT;
    pregs->SEMR.BYTE = SCI_IIC_SEMR_INIT;
    pregs->SNFR.BYTE = SCI_IIC_SNFR_INIT;
    pregs->SIMR1.BYTE = SCI_IIC_SIMR1_INIT;
    pregs->SIMR2.BYTE = SCI_IIC_SIMR2_INIT;
    pregs->SIMR3.BYTE = SCI_IIC_SIMR3_INIT;
    pregs->SISR.BYTE = SCI_IIC_SISR_INIT;
    pregs->SPMR.BYTE = SCI_IIC_SPMR_INIT;
} /* End of function r_sci_iic_clear_io_register() */

/***********************************************************************************************************************
 * Function Name: r_sci_iic_int_disable
 * Description  : Disables interrupt.
 *              : Sets interrupt source priority.
 *              : Clears interrupt request register.
 * Arguments    : sci_iic_info_t * p_sci_iic_info     ;   IIC Information
 * Return Value : None
 **********************************************************************************************************************/
void r_sci_iic_int_disable (sci_iic_info_t * p_sci_iic_info)
{
    R_BSP_VOLATILE_EVENACCESS const sci_iic_ch_rom_t * prom = g_sci_iic_handles[p_sci_iic_info->ch_no]->prom;
    uint8_t icu_txi;
    uint8_t txi_en_mask;
    uint32_t icu_tei;
    uint32_t tei_en_mask;

    /* Disables interrupt. */
    /* Disables TXI interrupt request enable register. */
    icu_txi = (*prom->picu_txi);
    txi_en_mask = prom->txi_en_mask;
    (*prom->picu_txi) = icu_txi & (~txi_en_mask);

    /* Disables TEI (in Group BL0) interrupt request enable register. */
    icu_tei = (*prom->picu_tei);
    tei_en_mask = prom->tei_en_mask;
    (*prom->picu_tei) = icu_tei & (~tei_en_mask);

    /* Clears interrupt source priority. */
    (*prom->pipr) = 0; /* Clears TXI interrupt source priority register. */

    /* dummy read */
    if (*prom->pipr)
    {
        R_BSP_NOP();
    }

     /* Clears the interrupt request register. */
    /* Includes I/O register read operation at the end of the following function. */
    sci_iic_clear_ir_flag(p_sci_iic_info);
} /* End of function r_sci_iic_int_disable() */

/***********************************************************************************************************************
 * Function Name: r_sci_iic_int_enable
 * Description  : Clears interrupt request register.
 *              : Enables interrupt.
 * Arguments    : sci_iic_info_t * p_sci_iic_info     ;   IIC Information
 * Return Value : None
 **********************************************************************************************************************/
void r_sci_iic_int_enable (sci_iic_info_t * p_sci_iic_info)
{
    R_BSP_VOLATILE_EVENACCESS const sci_iic_ch_rom_t * prom = g_sci_iic_handles[p_sci_iic_info->ch_no]->prom;

    uint8_t icu_txi;
    uint8_t txi_en_mask;
    uint32_t icu_tei;
    uint32_t tei_en_mask;
    uint8_t ipr_set_val;

    /* Clears the interrupt request register. */
    sci_iic_clear_ir_flag(p_sci_iic_info);

    /* Enables interrupt. */
    /* Enables TXI interrupt request enable register. */
    icu_txi = (*prom->picu_txi);
    txi_en_mask = prom->txi_en_mask;
    (*prom->picu_txi) = icu_txi | txi_en_mask;

    /* Enables TEI (in Group BL0) interrupt request enable register. */
    icu_tei = (*prom->picu_tei);
    tei_en_mask = prom->tei_en_mask;
    (*prom->picu_tei) = icu_tei | tei_en_mask;

    /* Sets interrupt source priority. */
    ipr_set_val = prom->ipr_set_val;
    (*prom->pipr) = ipr_set_val; /* Sets TXI interrupt source priority register. */

    /* dummy read */
    if (*prom->pipr)
    {
        R_BSP_NOP();
    }

} /* End of function r_sci_iic_int_enable() */

/***********************************************************************************************************************
 * Function Name: sci_iic_set_frequency
 * Description  : Set IIC Frequency Processing.
 *                Sets SMR,BRR registers.
 * Arguments    : sci_iic_info_t * p_sci_iic_info     ;   IIC Information
 * Return Value : None
 **********************************************************************************************************************/
static void sci_iic_set_frequency (sci_iic_info_t * p_sci_iic_info)
{
    volatile uint16_t brr_n = 32U; /* default: 64*2^(2*0-1) = 32 */
    volatile uint8_t cks_value = 0U; /* default: PCLK/1 */
    volatile uint32_t brr_value = 0U;
    uint16_t brr_n_tmp;
    uint8_t cks_value_tmp;

    sci_regs_t pregs = g_sci_iic_handles[p_sci_iic_info->ch_no]->prom->regs;
    R_BSP_VOLATILE_EVENACCESS const sci_iic_ch_rom_t * prom = g_sci_iic_handles[p_sci_iic_info->ch_no]->prom;

    /*  ---- Calculation BRR ---- */
    /* Macro definition does not have the type of declaration. The cast for the calculation of the floating point, 
     has been cast in the double type. After that, it is cast to type long to fit on the left-hand side. 
     Calculation results are not affected by casting, it is no problem. */
    if ((SCI_IIC_NUM_CH10 == p_sci_iic_info->ch_no) || (SCI_IIC_NUM_CH11 == p_sci_iic_info->ch_no))
    {
        brr_n_tmp = brr_n;
        brr_value = (uint32_t) ((double) ((double) ((double) BSP_PCLKA_HZ / (brr_n_tmp * (prom->bitrate)))) - 0.1);
    }
    else
    {
        brr_n_tmp = brr_n;
        brr_value = (uint32_t) ((double) ((double) ((double) BSP_PCLKB_HZ / (brr_n_tmp * (prom->bitrate)))) - 0.1);
    }

    /* Set clock source */
    /* WAIT_LOOP */
    while (brr_value > 255)
    {
        /* Counter over 0xff */
        switch (brr_n)
        {
            case 32 :
                brr_n = 128; /* 64*(2^(2*1-1)) */
                cks_value = 1; /* clock select: PCLK/4 */
            break;

            case 128 :
                brr_n = 512; /* 64*(2^(2*2-1))  */
                cks_value = 2; /* clock select: PCLK/16 */
            break;

            case 512 :
                brr_n = 2048; /* 64*(2^(2*3-1)) */
                cks_value = 3; /* clock select: PCLK/64 */
            break;

            default :

                /* nothing to do */
            break;
        }

        /*  ---- Calculation BRR ---- */
        /* Macro definition does not have the type of declaration. The cast for the calculation of the floating point, 
         has been cast in the double type. After that, it is cast to type long to fit on the left-hand side. 
         Calculation results are not affected by casting, it is no problem. */
        if ((SCI_IIC_NUM_CH10 == p_sci_iic_info->ch_no) || (SCI_IIC_NUM_CH11 == p_sci_iic_info->ch_no))
        {
            brr_n_tmp = brr_n;
            brr_value = (uint32_t) ((double) (((double) BSP_PCLKA_HZ / (brr_n_tmp * (prom->bitrate)))) - 0.1);
        }
        else
        {
            brr_n_tmp = brr_n;
            brr_value = (uint32_t) ((double) (((double) BSP_PCLKB_HZ / (brr_n_tmp * (prom->bitrate)))) - 0.1);
        }

        /* When the clock source of the on-chip baud rate generator is PCLK/64 and when the value
         of brr_value is greater than 255. */
        if ((3 == cks_value) && (255 < brr_value))
        {
            brr_value = 255;
        }
    }

    cks_value_tmp = cks_value;
    pregs->SMR.BYTE |= cks_value_tmp; /* Sets SMR */
    pregs->BRR = brr_value; /* Sets BRR */
} /* End of function sci_iic_set_frequency() */

/***********************************************************************************************************************
 * Function Name: r_sci_iic_mpc_setting
 * Description  : Set SCI_IIC multi-function pin controller.
 * Arguments    : uint8_t port_gr      ;   port group
 *                uint8_t pin_num      ;   pin number
 *                uint8_t set_value    ;   value of pin function select setting
 * Return Value : None
 **********************************************************************************************************************/
void r_sci_iic_mpc_setting (uint8_t port_gr, uint8_t pin_num, uint8_t set_value)
{
    R_BSP_VOLATILE_EVENACCESS uint8_t * const ppmr = (uint8_t *)((uint32_t)SCI_IIC_PRV_PMR_BASE_REG + (uint32_t)port_gr);
    R_BSP_VOLATILE_EVENACCESS uint8_t * const ppfs = (uint8_t *)((uint32_t)SCI_IIC_PRV_PFS_BASE_REG
                                                           + (uint32_t)((port_gr * 8) + pin_num));

    if ((*ppfs) != set_value)
    {
        (*ppmr) &= (~(1U << pin_num)); /* Uses as a GPIO (Input port). */

        /* Disable protection for MPC using PWPR register. */
        /* Enable writing of PFSWE bit. */
        MPC.PWPR.BIT.B0WI = 0;
        /* Enable writing to PFS registers. */
        MPC.PWPR.BIT.PFSWE = 1;

        /* Pin function select to "SSCL/SSDA" or "Hi-Z" pin. */
        (*ppfs) = set_value;

        /* Enable protection for MPC using PWPR register. */
        /* Enable writing of PFSWE bit. It could be assumed that the B0WI bit is still cleared from a call to
           protection disable function, but it is written here to make sure that the PFSWE bit always gets
           cleared. */
        MPC.PWPR.BIT.B0WI = 0;
        /* Disable writing to PFS registers. */
        MPC.PWPR.BIT.PFSWE = 0;
        /* Disable writing of PFSWE bit. */
        MPC.PWPR.BIT.B0WI = 1;

        /* Verifies "set_value" (setting value for the PFS register). */
        /* If "set_value" is a value other than 0 (default), sets the pin mode control bit to be used 
         as I/O port for peripheral functions. */
        if (SCI_IIC_MPC_SSCL_INIT != set_value)
        {
            (*ppmr) |= (1U << pin_num); /* Uses as SCI_IIC (SSCL/SSDA). */
        }

        /* dummy read */
        if (*ppmr)
        {
            R_BSP_NOP();
        }
    }
} /* End of function r_sci_iic_mpc_setting() */

/***********************************************************************************************************************
 * Function Name: sci_iic_clear_ir_flag
 * Description  : Clears Interrupt Request Flag Processing.
 *                Clears interrupt request register.
 * Arguments    : sci_iic_info_t * p_sci_iic_info     ;   IIC Information
 * Return Value : None
 **********************************************************************************************************************/
static void sci_iic_clear_ir_flag (sci_iic_info_t * p_sci_iic_info)
{
    sci_regs_t pregs = g_sci_iic_handles[p_sci_iic_info->ch_no]->prom->regs;
    R_BSP_VOLATILE_EVENACCESS const sci_iic_ch_rom_t * prom = g_sci_iic_handles[p_sci_iic_info->ch_no]->prom;

    /* Checks IR flag. */
    /* If IR flag is set, clears IR flag. */
    if (SCI_IIC_IR_SET == (*prom->pir_txi))
    {
        /* Initializes ICIER->SCR register. */
        pregs->SCR.BYTE = SCI_IIC_SCR_INIT;

        /* WAIT_LOOP */
        while (SCI_IIC_SCR_INIT != pregs->SCR.BYTE)
        {
            /* nothing to do */
        }

        /* Clears TXI1 interrupt request register. */
        (*prom->pir_txi) = SCI_IIC_IR_CLR;

        /* dummy read */
        if (*prom->pir_txi)
        {
            R_BSP_NOP();
        }

        /* Re-initializes SCI_IIC register because cleared ICCR1.ICE bit. */
        /* Includes I/O register read operation at the end of the following function. */
        r_sci_iic_init_io_register(p_sci_iic_info);
    }
} /* End of function sci_iic_clear_ir_flag() */


/***********************************************************************************************************************
 * Function Name: sci_iic_int_sci_iic12_txi_isr
 * Description  : Interrupt TXI handler for channel 12.
 *                Types of interrupt requests transmission end.
 * Arguments    : None
 * Return Value : None
 **********************************************************************************************************************/
#pragma interrupt sci_iic_int_sci_iic12_txi_isr(save, vect=VECT_SCI12_TXI12)
void sci_iic_int_sci_iic12_txi_isr (void)
{
    r_sci_iic_txi_isr_processing(SCI_IIC_NUM_CH12);

    /* Calls advance function */
    r_sci_iic_advance(g_sci_iic_handles[SCI_IIC_NUM_CH12]->psci_iic_info_ch);
} /* End of function sci_iic_int_sci_iic12_txi_isr() */


/***********************************************************************************************************************
 * Function Name: sci_iic_int_sci_iic12_tei_isr
 * Description  : Interrupt TEI handler for channel 12.
 *                Types of interrupt requests transfer error or event generation.
 *              : The event generations are arbitration-lost, NACK detection, timeout detection, 
 *              : start condition detection, and stop condition detection.
 * Arguments    : None
 * Return Value : None
 **********************************************************************************************************************/
void sci_iic_int_sci_iic12_tei_isr (void)
{
    r_sci_iic_tei_isr_processing(SCI_IIC_NUM_CH12);

    /* Calls advance function */
    r_sci_iic_advance(g_sci_iic_handles[SCI_IIC_NUM_CH12]->psci_iic_info_ch);
} /* End of function sci_iic_int_sci_iic12_tei_isr() */

#endif /* R_FLASH_ENABLED == 2 */

