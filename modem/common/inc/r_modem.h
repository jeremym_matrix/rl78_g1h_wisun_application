#ifndef R_MODEM_H
#define R_MODEM_H

#include <stddef.h>
#include <stdint.h>
#include <stdarg.h>

#define CRC_SIZE            4U                               //!< also part of @ref R_MODEM_FOOTER_SIZE
#define R_MODEM_HEADER_SIZE (1 + (sizeof(r_modem_header_t))) //!< Delimiter + Header Fields
#define R_MODEM_FOOTER_SIZE (1 + CRC_SIZE)                   //!< CRC + Delimiter

#define RECORD_DELIMITER    0x7eU
#define ESCAPE_BYTE         0x7dU
#define ESCAPE_XOR          0x20U


/// 1 byte type
typedef enum
{
    R_MODEM_TYPE_SYSTEM    = 0,
    R_MODEM_TYPE_G3        = 1,
    R_MODEM_TYPE_PRIME     = 2,
    R_MODEM_TYPE_MM        = 4,
    R_MODEM_TYPE_NTRCTY    = 5,
    R_MODEM_TYPE_WISUN_FAN = 6,
} r_modem_type_t;


/// 1 bit IDC (bit 7)
typedef enum
{
    R_MODEM_IDC_CHANNEL_0 = 0,
    R_MODEM_IDC_CHANNEL_1 = 0x80,
} r_modem_idc_t;

/// 3 bits IDA (bits 6-4)
typedef enum
{
    R_MODEM_IDA_REQUEST    = 0,     //!< NWK Request (APL -> NWK)
    R_MODEM_IDA_CONFIRM    = 0x10,  //!< NWK Confirm (NWK -> APL)
    R_MODEM_IDA_INDICATION = 0x20,  //!< NWK Indication (NWK -> APL)
    R_MODEM_IDA_RESPONSE   = 0x30,  //!< Reserved by Japan (Response type)
    R_MODEM_IDA_PRINTF     = 0x40,  //!< printf output string (e.g. Demonstrator message)
} r_modem_ida_t;

/// 4 bits IDP (bits 3-0)
typedef enum
{
    R_MODEM_IDP_CONTROLLER = 0,
    R_MODEM_IDP_MM_PHY     = 1,
    R_MODEM_IDP_LMAC       = 2,
    R_MODEM_IDP_UMAC       = 3,
    R_MODEM_IDP_ADP        = 4,
    R_MODEM_IDP_EAP        = 5,
    R_MODEM_IDP_NWK        = 6,
    R_MODEM_IDP_RFMAC      = 0xb,
} r_modem_idp_t;

#define R_MODEM_IDC(id) ((id) & (0x80U))
#define R_MODEM_IDA(id) ((id) & (0x40U | 0x20U | 0x10U))
#define R_MODEM_IDP(id) ((id) & 0x3fU)

typedef struct
{
    uint8_t reserved;
    uint8_t type; /// see ::r_modem_type_t
    uint8_t id;   /// see ::r_modem_idc_t, ::r_modem_ida_t, and ::r_modem_idp_t
    uint8_t cmd;
} r_modem_header_t;

/** Pointer to callback function to process data received by the modem/HDLC module */
typedef void (* r_modem_rx_cb)(uint16_t size);

/** Pointer to callback function to send modem/HDLC data */
typedef void (* r_modem_tx_cb)(uint8_t byte);

/** The state of the modem/HDLC module */
typedef struct
{
    uint8_t*      rx_buf;
    uint16_t      rx_buf_size;
    uint16_t      rx_buf_capacity;
    uint8_t       rx_disabled;
    uint32_t      rx_crc;
    uint8_t       rx_escaped;
    uint8_t       rx_error;
    r_modem_rx_cb rx_cb;
    r_modem_tx_cb tx_cb;
} r_modem_ctx;

/// Process an incoming byte
void r_modem_rx(uint8_t byte, r_modem_ctx* ctx);

/// Start RX after
void r_modem_rx_start(uint8_t* buf, uint16_t size, r_modem_ctx* ctx);

/**
 * Encode and send data via the modem interface
 * @param data Pointer to the data to encode and send
 * @param size Size of the data buffer in bytes
 * @param crc The current CRC (before sending the specified data)
 * @param ctx The modem context
 * @return The updated CRC (after sending the specified data)
 */
uint32_t r_modem_tx(const void* data, size_t size, uint32_t crc, r_modem_ctx* ctx);

/**
 * Transmit the message (adding delimiters, CRC, and encoding)
 * @param type The type (see ::r_modem_type_t)
 * @param id The id consisting of ::r_modem_idc_t, ::r_modem_ida_t, and ::r_modem_idp_t
 * @param cmd The command
 * @param data The data
 * @param size The size of data
 */
void r_modem_msg_tx(uint8_t type, uint8_t id, uint8_t cmd, const void* data, size_t size, r_modem_ctx* ctx);

/**
 * Encapsulate the specified format string into the modem/HDLC encoding and print it
 * @details printing behavior is analog to printf
 * @param format C string that contains a format string that follows the same specifications as format in printf
 * @return The number of characters (excluding the modem/HDLC characters) written
 */
int R_Modem_print(const char* format, ...);

/**
 * Encapsulate the specified format string into the modem/HDLC encoding and print it
 * @details printing behavior is analog to vprintf
 * @param format C string that contains a format string that follows the same specifications as format in printf
 * @param argumentList A value identifying a variable arguments list initialized with va_start
 * @return The number of characters (excluding the modem/HDLC characters) written
 */
int R_Modem_vprint(const char* format, va_list argumentList);

#endif /* R_MODEM_H */
