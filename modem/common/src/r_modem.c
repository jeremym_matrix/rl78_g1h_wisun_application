#include "r_modem.h"

#include "r_modem_crc_table.h"
#include "r_byte_swap.h"
#include "r_impl_utils.h"

#include <string.h>
#include <stdio.h>

// we have no logging but this allows the user to provide a suitable function for debugging
#ifdef R_DEV_MODEM_PRINTLN
#include "r_header_utils.h"
void R_DEV_MODEM_PRINTLN(const char* fmt, ...) R_HEADER_UTILS_PRINTF_LIKE(1, 2);
#define ERR R_DEV_MODEM_PRINTLN
#else
#define ERR(...) do {} while (0);
#endif

static uint32_t crc_update(uint32_t crc, uint8_t byte)
{
    uint32_t idx = ((crc >> 24u) ^ byte) & 0xffu;
    return (r_modem_crc_table[idx] ^ (crc << 8u)) & 0xffffffffu;
}

static void rx_buf_append(uint8_t byte, r_modem_ctx* ctx)
{
    ctx->rx_buf[ctx->rx_buf_size++] = byte;
    ctx->rx_crc = crc_update(ctx->rx_crc, byte);
}

void r_modem_rx(uint8_t byte, r_modem_ctx* ctx)
{
    if (ctx->rx_disabled)
    {
        return;  // still processing previous message -> drop
    }

    if (ctx->rx_buf_size >= ctx->rx_buf_capacity)
    {
        ERR("r_modem_rx: error buffer too small");
        ctx->rx_error = 1;
        return;
    }

    if (ctx->rx_escaped)
    {
        rx_buf_append(byte ^ ESCAPE_XOR, ctx);
        ctx->rx_escaped = 0;
        return;
    }

    if (byte == ESCAPE_BYTE)
    {
        if (ctx->rx_escaped)
        {
            ERR("r_modem_rx: error ESCAPE follows ESCAPE");
            ctx->rx_error = 1;
            return;
        }
        ctx->rx_escaped = 1;
        return;
    }

    if (byte == RECORD_DELIMITER && ctx->rx_escaped)
    {
        ERR("r_modem_rx: error RECORD_DELIMITER follows ESCAPE");
        ctx->rx_error = 1;
        return;
    }

    if (byte != RECORD_DELIMITER)
    {
        rx_buf_append(byte, ctx);
        return;
    }

    // delimiter handling
    int_fast8_t error = ctx->rx_error;
    if (ctx->rx_buf_size < sizeof(r_modem_header_t) + CRC_SIZE)
    {
        if (ctx->rx_buf_size != 0)
        {
            ERR("r_modem_rx: error frame too small: %u", ctx->rx_buf_size);
        }
        error = 1;
    }

    if (!error)
    {
        // we've calculated the CRC over the buffer including the sent CRC -> result must be zero
        if (ctx->rx_crc != 0)
        {
            ERR("r_modem_rx: error in CRC");
            error = 1;
        }
    }

    if (error)
    {
        // drop frame
        r_modem_rx_start(ctx->rx_buf, ctx->rx_buf_capacity, ctx);
        return;
    }

    // buffer is ready for processing
    ctx->rx_buf_size -= CRC_SIZE;
    ctx->rx_disabled = 1;
    ctx->rx_cb(ctx->rx_buf_size);
}

void r_modem_rx_start(uint8_t* buf, uint16_t size, r_modem_ctx* ctx)
{
    ctx->rx_disabled = 0;
    ctx->rx_error = 0;
    ctx->rx_escaped = 0;
    ctx->rx_crc = 0;
    ctx->rx_buf_size = 0;
    ctx->rx_buf = buf;
    ctx->rx_buf_capacity = size;
}

uint32_t r_modem_tx(const void* data, size_t size, uint32_t crc, r_modem_ctx* ctx)
{
    for (size_t i = 0; i < size; i++)
    {
        uint8_t byte = ((const uint8_t*)data)[i];
        crc = crc_update(crc, byte);
        if (byte == RECORD_DELIMITER || byte == ESCAPE_BYTE)
        {
            ctx->tx_cb(ESCAPE_BYTE);
            ctx->tx_cb(byte ^ ESCAPE_XOR);
        }
        else
        {
            ctx->tx_cb(byte);
        }
    }
    return crc;
}

void r_modem_msg_tx(uint8_t type, uint8_t id, uint8_t cmd, const void* data, size_t size, r_modem_ctx* ctx)
{
    ctx->tx_cb(RECORD_DELIMITER);
    uint32_t crc = 0;
    r_modem_header_t header = { 0, type, id, cmd};
    crc = r_modem_tx(&header, sizeof(header), crc, ctx);
    crc = r_modem_tx(data, size, crc, ctx);
    uint8_t crcbuf[CRC_SIZE];
    UInt32ToArr(crc, crcbuf);
    r_modem_tx(crcbuf, sizeof(crcbuf), crc, ctx);
    ctx->tx_cb(RECORD_DELIMITER);
}
