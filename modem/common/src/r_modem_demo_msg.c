/**********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
 * other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
 * applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
 * EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
 * SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO
 * THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the
 * following link:
 * http://www.renesas.com/disclaimer
 *
 * Copyright (C) 2020 Renesas Electronics Corporation. All rights reserved.
 *********************************************************************************************************************/

/**
 * @file r_modem_demo_msg.c
 * @brief HDLC/Modem module for Demonstrator messages
 */

#include <stdio.h>

#include "r_modem.h"
#include "hardware.h"
#include "r_app_main.h"
#include "r_mem_tools.h"
#include "r_byte_swap.h"
#include "r_impl_utils.h"

#include "r_log.h"
#include "r_log_flags.h"

/** The maximum number of characters that may be printed by a single call to any R_Modem_print function variant */
#define R_MODEM_PRINT_MAX_BYTES 256

typedef struct
{
    uint8_t  hdlcHeaderWritten; //!< Flag to indicate whether the HDLC has already been written or not
    uint32_t crc;               //!< The CRC of the current HDLC message
} r_modem_demo_msg_state_t;

static r_modem_ctx ctx;
static r_modem_demo_msg_state_t state;

void print_hdlc_footer();

static void resume_rx(uint8_t* buf, size_t size)
{
    R_memset(buf, 0xff, size);
    r_modem_rx_start(buf, size, &ctx);
}

/** Transmit the byte */
static void r_modem_cb_tx(uint8_t byte)
{
    RdrvUART_PutChar(byte);
}

/** Process received HDLC message */
static void r_modem_cb_rx(uint16_t size)
{
    const r_modem_header_t* hdr = (const r_modem_header_t*)AppCmdBuf;

#if defined(__ICCRL78__)

/* Mask Compiler Warning[Pa089]: enumerated type mixed with another enumerated type. */
#pragma diag_suppress=Pa089
/* Reason: The hdr->id contains different bit fields represented by individual enum declarations.
   It is the desired behavior to combine these for simplicity and effectiveness. */
#endif

    if (hdr->type != R_MODEM_TYPE_WISUN_FAN || hdr->id != (R_MODEM_IDC_CHANNEL_0 | R_MODEM_IDA_PRINTF))
    {
        resume_rx(ctx.rx_buf, ctx.rx_buf_capacity);  // Discard frame
        return;
    }

    /* HDLC message is valid -> Pass payload to application */
    AppCmd_ProcessCmd(&ctx.rx_buf[sizeof(*hdr)], size - sizeof(*hdr));

    resume_rx(ctx.rx_buf, ctx.rx_buf_capacity);  // Clear RX buffer and wait for next message
}

void R_Modem_Demo_Init(uint8_t* buf, size_t size)
{
    // Init callbacks
    ctx.rx_cb = r_modem_cb_rx;
    ctx.tx_cb = r_modem_cb_tx;

    resume_rx(buf, size);
}

void R_Modem_Demo_ReadCommand(void)
{
    for (short ch = RdrvUART_GetChar(); ch != EOF; ch = RdrvUART_GetChar())
    {
        r_modem_rx((uint8_t)ch, &ctx);
    }
}

int R_Modem_print(const char* format, ...)
{
    /* Init argument list */
    va_list argumentList;
    va_start(argumentList, format);

    int totalBytesWritten = R_Modem_vprint(format, argumentList);

    va_end(argumentList);  // Perform any cleanup necessary so that the function can return
    return totalBytesWritten;
}

int R_Modem_vprint(const char* format, va_list argumentList)
{
    /* Prepend HDLC header if not already done for the current message */
    if (!state.hdlcHeaderWritten)
    {
        ctx.tx_cb(RECORD_DELIMITER);
        state.crc = 0;
        r_modem_header_t header = { 0, R_MODEM_TYPE_WISUN_FAN, R_MODEM_IDA_PRINTF, 0};
        state.crc = r_modem_tx(&header, sizeof(header), state.crc, &ctx);
        state.hdlcHeaderWritten = 1;
    }

    /* Write input string to buffer */
    char stringBuf[R_MODEM_PRINT_MAX_BYTES];
    int charsWritten = vsnprintf(stringBuf, sizeof(stringBuf), format, argumentList);

    int success = charsWritten >= 0 && (unsigned)charsWritten <= sizeof(stringBuf);
    if (charsWritten >= 0)
    {
        /* Adapt number of written characters if input string was truncated because output buffer is too small */
        charsWritten = MIN((unsigned)charsWritten, sizeof(stringBuf));
        state.crc = r_modem_tx(stringBuf, charsWritten, state.crc, &ctx);  // Send input string buffer via modem interface
    }

    /* Append HDLC footer if this is the end of the message (or any error occurred during write) */
    if (!success || (charsWritten > 0 && stringBuf[charsWritten - 1] == '\n'))
    {
        print_hdlc_footer();
    }

    return charsWritten;
}


void R_Modem_sendLogIndication()
{
#if R_LOG_THRESHOLD > R_LOG_SEVERITY_OFF
    uint8_t buf[R_MODEM_PRINT_MAX_BYTES];  // buffer to store log records
    size_t numBytes = 0;

    while ((numBytes = R_LOG_GetLog(buf, ARRAY_SIZE(buf))) > 0)
    {
        /* Prepend HDLC header if not already done for the current message */
        if (!state.hdlcHeaderWritten)
        {
            ctx.tx_cb(RECORD_DELIMITER);
            state.crc = 0;
            r_modem_header_t header = { 0, R_MODEM_TYPE_WISUN_FAN, R_MODEM_IDA_INDICATION, 0};
            state.crc = r_modem_tx(&header, sizeof(header), state.crc, &ctx);
            state.hdlcHeaderWritten = 1;
        }

        state.crc = r_modem_tx(buf, numBytes, state.crc, &ctx);  // Send input string buffer via modem interface
    }
    print_hdlc_footer();
#endif /* R_LOG_THRESHOLD > R_LOG_SEVERITY_OFF */
}

#if R_LOG_THRESHOLD > R_LOG_SEVERITY_OFF
void R_Modem_Demo_sendLogData(const uint8_t* buf, size_t bufSize)
{
    r_modem_msg_tx(R_MODEM_TYPE_WISUN_FAN, R_MODEM_IDA_INDICATION, 0, buf, bufSize, &ctx);
}

#endif /* R_LOG_THRESHOLD > R_LOG_SEVERITY_OFF */

void print_hdlc_footer()
{
    uint8_t crcbuf[CRC_SIZE];
    UInt32ToArr(state.crc, crcbuf);
    r_modem_tx(crcbuf, sizeof(crcbuf), 0, &ctx);
    ctx.tx_cb(RECORD_DELIMITER);
    MEMZERO_S(&state);  // Reset struct to write HDLC header on next function call and reset CRC
}
