/**********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
 * other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
 * applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
 * EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
 * SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO
 * THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the
 * following link:
 * http://www.renesas.com/disclaimer
 *
 * Copyright (C) 2020 Renesas Electronics Corporation. All rights reserved.
 *********************************************************************************************************************/

/**
 * @file r_modem_nwk_msg.c
 * @brief HDLC/Modem module for NWK API messages
 */

#include "r_modem_nwk_msg.h"
#include <string.h>
#include "r_impl_utils.h"
#include "r_modem.h"
#include "hardware.h"
#include "r_os_wrapper.h"
#include "r_nwk_api.h"

#if !R_MODEM_SERVER
#include "r_log.h"
#include "r_log_flags.h"
#endif

#if defined(__ICCRL78__)
void RdrvSendOSMsg_Initialize(void);
void RdrvSendOSMsg_Trigger(void);
#endif

// we have no logging but this allows the user to provide a suitable function for debugging
#ifdef R_DEV_MODEM_PRINTLN
#include "r_header_utils.h"
void R_DEV_MODEM_PRINTLN(const char* fmt, ...) R_HEADER_UTILS_PRINTF_LIKE(1, 2);
#define ERR         R_DEV_MODEM_PRINTLN
    #if 0
        #define DBG R_DEV_MODEM_PRINTLN
    #else
        #define DBG(...) do {} while (0);
    #endif
#else
#define ERR(...)         do {} while (0);
#define DBG(...)         do {} while (0);
#endif


#if (!R_MODEM_SERVER && !R_MODEM_CLIENT) || (R_MODEM_SERVER && R_MODEM_CLIENT)
#error "Exactly one of R_MODEM_SERVER or R_MODEM_CLIENT must be defined"
#endif

#if R_MODEM_SERVER
#define MY_TASK ID_apl_tsk
#define MY_MBX  ID_apl_mbx
#else
#define MY_TASK ID_nwk_tsk
#define MY_MBX  ID_nwk_mbx

static r_nwk_msg_t* pending_request;
#endif

r_modem_ctx g_modem_nwk_ctx;

static struct
{
    r_os_msg_header_t hdr;
    uint16_t          size;
} uart_message_received;

static union
{
    r_nwk_msg_t msg;  // modem header will override start but os_hdr is always bigger (static assert below)
    struct
    {
        uint8_t          padding0[offsetof(r_nwk_msg_t, message) - sizeof(r_modem_header_t)];
        r_modem_header_t modemHeader;
        // this is aligned with msg.message
        uint8_t          padding1[R_NWK_MSG_MAX_PAYLOAD_SIZE + R_MODEM_FOOTER_SIZE];
    } buf;
} rx;
STATIC_ASSERT(sizeof(r_modem_header_t) <= sizeof(rx.msg.hdr), r_modem_header_t_must_be_smaller_or_equal_than_r_os_msg_header_t);

static void resume_rx()
{
    memset(&rx, 0xff, sizeof(rx));
    r_modem_rx_start((uint8_t*)&rx.buf.modemHeader, sizeof(r_modem_header_t) + R_NWK_MSG_MAX_PAYLOAD_SIZE + R_MODEM_FOOTER_SIZE, &g_modem_nwk_ctx);
}

static void handle_rx(uint16_t rxSize)
{
    const r_modem_header_t* hdr = &rx.buf.modemHeader;
    if (hdr->type != R_MODEM_TYPE_WISUN_FAN
#if R_MODEM_SERVER
        || hdr->id != (R_MODEM_IDC_CHANNEL_0 | R_MODEM_IDA_REQUEST | R_MODEM_IDP_NWK)
#elif R_MODEM_CLIENT
        || (
            hdr->id != (R_MODEM_IDC_CHANNEL_0 | R_MODEM_IDA_CONFIRM | R_MODEM_IDP_NWK)
            && hdr->id != (R_MODEM_IDC_CHANNEL_0 | R_MODEM_IDA_INDICATION | R_MODEM_IDP_NWK)
            )
#endif
        )
    {
        // drop
        ERR("modem_nwk_rx: error in frame header");
        resume_rx();
        return;
    }

    // save the values we need before overwriting the modem header with the msg header
    uint8_t param = hdr->cmd;
#if R_MODEM_CLIENT
    uint8_t ida = R_MODEM_IDA(hdr->id);
#endif
    r_nwk_msg_t* msg = &rx.msg;

    MEMZERO_S(&msg->hdr);  // includes: R_OS_MsgMarkIgnoreFree
    msg->hdr.param_id = param;
    msg->hdr.task_id = MY_TASK;
#if R_MODEM_SERVER
    DBG("request: %02x %02x", param, msg->message.setReq.attrId);
    R_OS_SendMsg(&msg->hdr, ID_nwk_mbx);
    R_OS_Sleep();  // Wait until the network layer wakes this task up
    r_modem_msg_tx(R_MODEM_TYPE_WISUN_FAN, R_MODEM_IDA_CONFIRM | R_MODEM_IDP_NWK, param, &msg->message, R_NWK_MessageSize(msg), &g_modem_nwk_ctx);
    DBG("confirm: %02x %02x", param, msg->message.setReq.attrId);
#else
    if (ida == R_MODEM_IDA_CONFIRM)
    {
        if (pending_request)
        {
            // copy to existing message and wake app task
            memcpy(&pending_request->message, &rx.msg.message, rxSize);
            DBG("confirm: %02x %02x %02x", param, pending_request->message.setCfm.attrId, pending_request->message.setCfm.result);
            R_OS_WakeupTask(pending_request->hdr.task_id);
            pending_request = NULL;
        }
    }
    else
    {
        DBG("indication: %02x", param);
#if R_LOG_THRESHOLD > R_LOG_SEVERITY_OFF
        if (param == R_NWK_API_MSG_LOG_IND)
        {
            /* Log indications from the modem server are injected into the log buffer of the local RLog module */
            R_LOG_AppendRawData(msg->message.logInd.data, msg->message.logInd.size);
        }
        else
#endif
        {
            /* All other indications are sent to the mailbox of the application task */
            r_nwk_msg_t* msg_apl = (r_nwk_msg_t*)R_OS_MsgAlloc(R_HEAP_ID_NWK_IND, offsetof(r_nwk_msg_t, message) + rxSize);
            if (msg_apl)
            {
                msg_apl->hdr.param_id = param;
                memcpy(&msg_apl->message, &rx.msg.message, rxSize);
                R_OS_SendMsg(&msg_apl->hdr, ID_apl_mbx);
            }
            else
            {
                ERR("Warning: Discard indication from NWK API due to insufficient memory");
            }
        }
    }
#endif /* if R_MODEM_SERVER */
    resume_rx();
}

static void process_message(uint16_t size)
{
    // since UART is disabled automatically and will only be enabled by handling the message, this 'singleton' is enough
    uart_message_received.size = size;

#if defined(__ICCRL78__)
    RdrvSendOSMsg_Trigger();
#else
    R_OS_SendMsgFromISR(&uart_message_received.hdr, MY_MBX);
#endif
}

#if __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wmissing-noreturn"
#endif
void r_modem_nwk_msg_start()
{
#if defined(__ICCRL78__)
    RdrvSendOSMsg_Initialize();
#endif

    // Init callbacks
    g_modem_nwk_ctx.rx_cb = process_message;
    g_modem_nwk_ctx.tx_cb = RdrvUART_PutChar;

    resume_rx();

    while (1)
    {
        r_os_msg_header_t* hdr = R_OS_ReceiveMsg(MY_MBX, R_OS_TIMEOUT_FOREVER);
        if (!hdr)
        {
            continue;  // Retry if no message is available
        }
        if (hdr == &uart_message_received.hdr)
        {
            handle_rx(uart_message_received.size);
            continue;
        }

        r_nwk_msg_t* msg = (r_nwk_msg_t*)hdr;
#if R_MODEM_SERVER
        // can only be indication, as the confirm of a request is handled directly in handle_rx
        uint8_t ida = R_MODEM_IDA_INDICATION;
        DBG("indication: %02x", msg->hdr.param_id);
#else
        uint8_t ida = R_MODEM_IDA_REQUEST;
        DBG("request: %02x %02x", msg->hdr.param_id, msg->message.setReq.attrId);
#endif
        r_modem_msg_tx(R_MODEM_TYPE_WISUN_FAN, ida | R_MODEM_IDP_NWK, msg->hdr.param_id, &msg->message, R_NWK_MessageSize(msg), &g_modem_nwk_ctx);
#if R_MODEM_SERVER
        R_OS_MsgFree(&msg->hdr);
#else
        pending_request = msg;
        // return, NWK-API will sleep and we will get confirm via serial
#endif
    }
}
#if __clang__
#pragma clang diagnostic pop
#endif

void r_modem_nwk_cb_trigger_os_msg()
{
    R_OS_SendMsgFromISR(&uart_message_received.hdr, MY_MBX);
}
