/****************************************************************************** 
* DISCLAIMER 

* This software is supplied by Renesas Electronics Corporation and is only 
* intended for use with Renesas products. No other uses are authorized. 

* This software is owned by Renesas Electronics Corporation and is protected under 
* all applicable laws, including copyright laws. 

* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES 
* REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, 
* INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
* PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY 
* DISCLAIMED. 

* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS 
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE 
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES 
* FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS 
* AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. 

* Renesas reserves the right, without notice, to make changes to this 
* software and to discontinue the availability of this software. 
* By using this software, you agree to the additional terms and 
* conditions found by accessing the following link: 
* http://www.renesas.com/disclaimer 
******************************************************************************/ 

/* Copyright (C) 2014. Renesas Electronics Corporation, All Rights Reserved. */

/****************************************************************************** 
* File Name : RL78G1H.h 
* Version : 1.00 
* Device(s) : RL78/G1H 
* Tool-Chain :  
* OS : RI78V4
* H/W Platform : 
* Description : 
* Operation : 
* Limitations : None 
****************************************************************************** 
* History : DD.MM.YYYY Version Description 
* 
******************************************************************************/ 

#ifndef	__RL78G1H_H__
#define	__RL78G1H_H__


/***************************************************************************
 * Macros
 **************************************************************************/
/*	Port */
/*	LEDs */
#define PORT_LED0_PM			PM6_bit.no0		// PM6.0
#define PORT_LED0_PORT			P6_bit.no0		// P6.0
#define PORT_LED1_PM			PM6_bit.no1		// PM6.1
#define PORT_LED1_PORT			P6_bit.no1		// P6.1
#define PORT_LED2_PM			PM6_bit.no2		// PM6.2
#define PORT_LED2_PORT			P6_bit.no2		// P6.2

#define	PORT_INPUT			1
#define	PORT_OUTPUT			0
#define	PORT_HI				1
#define	PORT_LO				0
#define	PORT_PULLUP_ON		1
#define	PORT_PULLUP_OFF		0

/*  UART Registers */
#define	UART_RX				RXD1
#define	UART_TX				TXD1
#define	UART_RX_STATUS		SSR03			// unit0,ch3(Rx)
#define	UART_TX_STATUS		SSR02			// unit0,ch3(Tx)

#define	UART_TX_INT_MASK	STMK1
#define	UART_RX_INT_MASK	SRMK1
#define	UART_ERR_INT_MASK	SREMK1

/*	UART Ports */
#define	UART_RX_PM			PM0_bit.no3
#define	UART_TX_PM			PM0_bit.no2
#define	UART_RX_PORT		P0_bit.no3
#define	UART_TX_PORT		P0_bit.no2

/*
*******************************************************************************
**	Register bit define
*******************************************************************************
*/
/*
	Peripheral Enable Register 0 (PER0)
*/
/* Control of serial array unit 0 input clock(SAU0EN) */
#define SAU0_CLOCK_CLR				0x04	/* for clear the bits */
#define SAU0_CLOCK_STOP				0x00	/* stops supply of input clock */
#define SAU0_CLOCK_SUPPLY			0x04	/* supplies input clock */

/* Control of serial array unit 1 input clock(SAU1EN) */
#define SAU1_CLOCK_CLR				0x08	/* for clear the bits */
#define SAU1_CLOCK_STOP				0x00	/* stops supply of input clock */
#define SAU1_CLOCK_SUPPLY			0x08	/* supplies input clock */

/*
	Serial Clock Select Register m (SPSm)
*/
/* Operating mode and clear mode selection(PRS003~PRS000) */
#define SAU_CK00_FCLK_0				0x0000	/* ck00-fclk */
#define SAU_CK00_FCLK_1				0x0001	/* ck00-fclk/2^1 */
#define SAU_CK00_FCLK_2				0x0002	/* ck00-fclk/2^2 */
#define SAU_CK00_FCLK_3				0x0003	/* ck00-fclk/2^3 */
#define SAU_CK00_FCLK_4				0x0004	/* ck00-fclk/2^4 */
#define SAU_CK00_FCLK_5				0x0005	/* ck00-fclk/2^5 */
#define SAU_CK00_FCLK_6				0x0006	/* ck00-fclk/2^6 */
#define SAU_CK00_FCLK_7				0x0007	/* ck00-fclk/2^7 */
#define SAU_CK00_FCLK_8				0x0008	/* ck00-fclk/2^8 */
#define SAU_CK00_FCLK_9				0x0009	/* ck00-fclk/2^9 */
#define SAU_CK00_FCLK_10			0x000A	/* ck00-fclk/2^10 */
#define SAU_CK00_FCLK_11			0x000B	/* ck00-fclk/2^11 */
/* Operating mode and clear mode selection(PRS013~PRS010) */
#define SAU_CK01_FCLK_0				0x0000	/* ck01-fclk */
#define SAU_CK01_FCLK_1				0x0010	/* ck01-fclk/2^1 */
#define SAU_CK01_FCLK_2				0x0020	/* ck01-fclk/2^2 */
#define SAU_CK01_FCLK_3				0x0030	/* ck01-fclk/2^3 */
#define SAU_CK01_FCLK_4				0x0040	/* ck01-fclk/2^4 */
#define SAU_CK01_FCLK_5				0x0050	/* ck01-fclk/2^5 */
#define SAU_CK01_FCLK_6				0x0060	/* ck01-fclk/2^6 */
#define SAU_CK01_FCLK_7				0x0070	/* ck01-fclk/2^7 */
#define SAU_CK01_FCLK_8				0x0080	/* ck01-fclk/2^8 */
#define SAU_CK01_FCLK_9				0x0090	/* ck01-fclk/2^9 */
#define SAU_CK01_FCLK_10			0x00A0	/* ck01-fclk/2^10 */
#define SAU_CK01_FCLK_11			0x00B0	/* ck01-fclk/2^11 */

/*                                                                    
	Serial Mode Register mn (SMRmn)                              
*/

#define	SAU_SMRMN_INITIALVALUE		0x0020 

/* Selection of macro clock (MCK) of channel n(CKSmn) */
#define SAU_CLOCK_SELECT_CLR		0x8000	/* for clear the bits */
#define SAU_CLOCK_SELECT_CK00		0x0000	/* operation clock CK0 set by PRS register */ 
#define SAU_CLOCK_SELECT_CK01		0x8000	/* 0peration clock CK1 set by PRS register */
/* Selection of transfer clock (TCLK) of channel n(CCSmn) */
#define SAU_CLOCK_MODE_CLR			0x4000	/* for clear the bits */
#define SAU_CLOCK_MODE_CKS			0x0000	/* divided operation clock MCK specified by CKSmn bit */  
#define SAU_CLOCK_MODE_TI0N			0x4000	/* clock input from SCK pin (slave transfer in CSI mode) */
/* Selection of start trigger source(STSmn) */
#define SAU_TRIGGER_CLR				0x0100	/* for clear the bits */
#define SAU_TRIGGER_SOFTWARE		0x0000	/* only software trigger is valid */
#define SAU_TRIGGER_RXD				0x0100	/* valid edge of RXD pin */
/* Controls inversion of level of receive data of channel n in UART mode(SISmn0) */
#define SAU_EDGE_FALL				0x0000	/* falling edge is detected as the start bit */
#define SAU_EDGE_RISING				0x0040	/* rising edge is detected as the start bit */
/* Setting of operation mode of channel n(MDmn2,MDmn1) */
#define SAU_CSI						0x0000	/* CSI mode */
#define SAU_UART					0x0002	/* UART mode */
#define SAU_IIC						0x0004	/* simplified IIC mode */
/* Selection of interrupt source of channel n(MDmn0) */
#define SAU_TRANSFER_END			0x0000	/* transfer end interrupt */
#define SAU_BUFFER_EMPTY			0x0001	/* buffer empty interrupt */

/*
	Serial Communication Operation Setting Register mn (SCRmn)
*/
/* Setting of operation mode of channel n(TXEmn,RXEmn) */
#define SAU_NOT_COMMUNICATION		0x0000	/* does not start communication */
#define SAU_RECEPTION				0x4000	/* reception only */
#define SAU_TRANSMISSION			0x8000	/* transmission only */
#define SAU_RECEPTION_TRANSMISSION	0xc000	/* reception and transmission */
/* Selection of data and clock phase in CSI mode(DAPmn,CKPmn) */
#define SAU_TIMING_1				0x0000	/* type 1 */
#define SAU_TIMING_2				0x1000	/* type 2 */
#define SAU_TIMING_3				0x2000	/* type 3 */
#define SAU_TIMING_4				0x3000	/* type 4 */
/* Selection of masking of error interrupt signal(EOCmn) */
#define SAU_INTSRE_MASK				0x0000	/* masks error interrupt INTSREx */
#define SAU_INTSRE_ENABLE			0x0400	/* enables generation of error interrupt INTSREx */
/* Setting of parity bit in UART mode(PTCmn1,PTCmn0) */
#define SAU_PARITY_NONE				0x0000	/* none parity */
#define SAU_PARITY_ZERO				0x0100	/* zero parity */
#define SAU_PARITY_EVEN				0x0200	/* even parity */
#define SAU_PARITY_ODD				0x0300	/* odd parity */
/* Selection of data transfer sequence in CSI and UART modes(DIRmn) */
#define SAU_MSB						0x0000	/* MSB */
#define SAU_LSB						0x0080	/* LSB */
/* Setting of stop bit in UART mode(SLCmn1,SLCmn0) */
#define SAU_STOP_NONE				0x0000	/* none stop bit */
#define SAU_STOP_1					0x0010	/* 1 stop bit */
#define SAU_STOP_2					0x0020	/* 2 stop bits */
/* Setting of data length in CSI and UART modes(DLSmn2~DLSmn0) */
#define SAU_LENGTH_5				0x0004	/* 5-bit data length */
#define SAU_LENGTH_7				0x0006	/* 7-bit data length */
#define SAU_LENGTH_8				0x0007	/* 8-bit data length */

/*
	Serial Output Level Register m (SOLm)
*/
/* Selects inversion of the level of the transmit data of channel n in UART mode */
#define SAU_CHANNEL0_NORMAL			0x0000	/* normal bit level */
#define SAU_CHANNEL0_INVERTED		0x0001	/* inverted bit level */
#define SAU_CHANNEL1_NORMAL			0x0000	/* normal bit level */  
#define SAU_CHANNEL1_INVERTED		0x0002	/* inverted bit level */
#define SAU_CHANNEL2_NORMAL			0x0000	/* normal bit level */  
#define SAU_CHANNEL2_INVERTED		0x0004	/* inverted bit level */
#define SAU_CHANNEL3_NORMAL			0x0000	/* normal bit level */  
#define SAU_CHANNEL3_INVERTED		0x0008	/* inverted bit level */

/*
	Noise Filter Enable Register 0 (NFEN0)
*/
/* Use of noise filter of RXD3/P14 pin */
#define SAU_RXD3_FILTER_OFF			0x00	/* noise filter off */
#define SAU_RXD3_FILTER_ON			0x40	/* noise filter on */
#define SAU_RXD2_FILTER_OFF			0x00	/* noise filter off */
#define SAU_RXD2_FILTER_ON			0x10	/* noise filter on */
#define SAU_RXD1_FILTER_OFF			0x00	/* noise filter off */
#define SAU_RXD1_FILTER_ON			0x04	/* noise filter on */
#define SAU_RXD0_FILTER_OFF			0x00	/* noise filter off */
#define SAU_RXD0_FILTER_ON			0x01	/* noise filter on */

/*
	Format of Serial Status Register mn (SSRmn)
*/
/* Communication status indication flag of channel n(TSFmn) */
#define SAU_UNDER_EXECUTE			0x0040	/* communication is under execution */
#define SAU_DATA_STORED				0x0020	/* valid data is stored SDRmn register */

/* Parity error detection flag of channel n(PEFmn) */
#define	SAU_FRAMING_ERROR			0x0004	/* a framing error occurs during UART reception */
#define SAU_PARITY_ERROR			0x0002	/* a parity error occurs during UART reception or ACK is not detected during I2C transmission */
#define	SAU_OVERRUN_ERROR			0x0001	/* a overrun error during UART reception */

/*
	Serial Channel Start Register m (SSm)
*/
/* Operation start trigger of channel 0(SSm0) */
#define SAU_CH0_START_TRG_OFF		0x0000	/* no trigger operation */
#define SAU_CH0_START_TRG_ON		0x0001	/* sets SEm0 to 1 and enters the communication wait status */
/* Operation start trigger of channel 1(SSm1) */
#define SAU_CH1_START_TRG_OFF		0x0000	/* no trigger operation */
#define SAU_CH1_START_TRG_ON		0x0002	/* sets SEm1 to 1 and enters the communication wait status */
/* Operation start trigger of channel 2(SSm2) */
#define SAU_CH2_START_TRG_OFF		0x0000	/* no trigger operation */
#define SAU_CH2_START_TRG_ON		0x0004	/* sets SEm2 to 1 and enters the communication wait status */
/* Operation start trigger of channel 3(SSm3) */
#define SAU_CH3_START_TRG_OFF		0x0000	/* no trigger operation */
#define SAU_CH3_START_TRG_ON		0x0008	/* sets SEm3 to 1 and enters the communication wait status */

/*
	Serial Channel Stop Register m (STm)
*/
/* Operation stop trigger of channel 0(STm0) */
#define SAU_CH0_STOP_TRG_OFF		0x0000	/* no trigger operation */
#define SAU_CH0_STOP_TRG_ON			0x0001	/* operation is stopped (stop trigger is generated) */
/* Operation stop trigger of channel 1(STm1) */
#define SAU_CH1_STOP_TRG_OFF		0x0000	/* no trigger operation */
#define SAU_CH1_STOP_TRG_ON			0x0002	/* operation is stopped (stop trigger is generated) */
/* Operation stop trigger of channel 2(STm2) */
#define SAU_CH2_STOP_TRG_OFF		0x0000	/* no trigger operation */
#define SAU_CH2_STOP_TRG_ON			0x0004	/* operation is stopped (stop trigger is generated) */
/* Operation stop trigger of channel 3(STm3) */
#define SAU_CH3_STOP_TRG_OFF		0x0000	/* no trigger operation */
#define SAU_CH3_STOP_TRG_ON			0x0008	/* operation is stopped (stop trigger is generated) */

/*
	Format of Serial Flag Clear Trigger Register mn (SIRmn)
*/
/* Clear trigger of overrun error flag of channel n(OVCTmn) */
#define	SAU_SIRMN_OVCTMN		0x0001
/* Clear trigger of parity error flag of channel n(PECTmn) */
#define	SAU_SIRMN_PECTMN		0x0002
/* Clear trigger of framing error of channel n(FECTMN) */
#define	SAU_SIRMN_FECTMN		0x0004

/*
	Serial Output Enable Register m (SOEm)
*/
/* Serial output enable/disable of channel 0(SOEm0) */
#define SAU_CH0_OUTPUT_ENABLE		0x0001	/* stops output by serial communication operation */
#define SAU_CH0_OUTPUT_DISABLE		0x0000	/* enables output by serial communication operation */
/* Serial output enable/disable of channel 1(SOEm1) */
#define SAU_CH1_OUTPUT_ENABLE		0x0002	/* stops output by serial communication operation */
#define SAU_CH1_OUTPUT_DISABLE		0x0000	/* enables output by serial communication operation */
/* Serial output enable/disable of channel 2(SOEm2) */
#define SAU_CH2_OUTPUT_ENABLE		0x0004	/* stops output by serial communication operation */
#define SAU_CH2_OUTPUT_DISABLE		0x0000	/* enables output by serial communication operation */
/* Serial output enable/disable of channel 3(SOEm3) */
#define SAU_CH3_OUTPUT_ENABLE		0x0008	/* stops output by serial communication operation */
#define SAU_CH3_OUTPUT_DISABLE		0x0000	/* enables output by serial communication operation */

/*
	Serial Output Register m (SOm)
*/
/* Serial data output of channel 0(SOm0) */
#define SAU_CH0_DATA_OUTPUT_0		0x0000	/* Serial data output value is 0*/
#define SAU_CH0_DATA_OUTPUT_1		0x0001	/* Serial data output value is 1*/
/* Serial data output of channel 1(SOm1) */
#define SAU_CH1_DATA_OUTPUT_0		0x0000	/* Serial data output value is 0*/
#define SAU_CH1_DATA_OUTPUT_1		0x0002	/* Serial data output value is 1*/
/* Serial data output of channel 2(SOm2) */
#define SAU_CH2_DATA_OUTPUT_0		0x0000	/* Serial data output value is 0*/
#define SAU_CH2_DATA_OUTPUT_1		0x0004	/* Serial data output value is 1*/
/* Serial data output of channel 3(SOm3) */
#define SAU_CH3_DATA_OUTPUT_0		0x0000	/* Serial data output value is 0*/
#define SAU_CH3_DATA_OUTPUT_1		0x0008	/* Serial data output value is 1*/
/* Serial clock output of channel 0(CKOm0) */
#define SAU_CH0_CLOCK_OUTPUT_0		0x0000	/* Serial clock output value is 0*/
#define SAU_CH0_CLOCK_OUTPUT_1		0x0100	/* Serial clock output value is 1*/
/* Serial clock output of channel 1(CKOm1) */
#define SAU_CH1_CLOCK_OUTPUT_0		0x0000	/* Serial clock output value is 0*/
#define SAU_CH1_CLOCK_OUTPUT_1		0x0200	/* Serial clock output value is 1*/
/* Serial clock output of channel 2(CKOm2) */
#define SAU_CH2_CLOCK_OUTPUT_0		0x0000	/* Serial clock output value is 0*/
#define SAU_CH2_CLOCK_OUTPUT_1		0x0400	/* Serial clock output value is 1*/
/* Serial clock output of channel 3(CKOm3) */
#define SAU_CH3_CLOCK_OUTPUT_0		0x0000	/* Serial clock output value is 0*/
#define SAU_CH3_CLOCK_OUTPUT_1		0x0800	/* Serial clock output value is 1*/

#define SAU_IIC_MASTER_FLAG_CLEAR	0x00
#define SAU_IIC_SEND_FLAG			0x01
#define SAU_IIC_RECEIVE_FLAG		0x02
#define SAU_IIC_SENDED_ADDRESS_FLAG	0x03
/*
	Peripheral Enable Register 0 (PER0)
*/
/* Control of timer array unit input clock(IIC0EN) */
#define IIC0_CLOCK_CLR				0x10	/* for clear the bits */
#define IIC0_CLOCK_STOP				0x00	/* stops supply of input clock */
#define IIC0_CLOCK_SUPPLY			0x10	/* supplies input clock */

/*
	IIC	control	register 0 (IICC0)
*/
/* IIC operation enable	(IICE0)	*/
#define	IIC0_OPERATION				0x80
#define	IIC0_OPERATION_DISABLE		0x00	/* stop	operation */
#define	IIC0_OPERATION_ENABLE		0x80	/* enable operation	*/
/* Exit	from communications	(LREL0)	*/
#define	IIC0_COMMUNICATION			0x40
#define	IIC0_COMMUNICATION_NORMAL	0x00	/* normal operation	*/
#define	IIC0_COMMUNICATION_EXIT		0x40	/* exit	from current communication */
/* Wait	cancellation (WREL0) */
#define	IIC0_WAITCANCEL				0x20
#define	IIC0_WAIT_NOTCANCEL			0x00	/* do not cancel wait */
#define	IIC0_WAIT_CANCEL			0x20	/* cancel wait */
/* Generation of interrupt when	stop condition (SPIE0) */
#define	IIC0_STOPINT				0x10
#define	IIC0_STOPINT_DISABLE		0x00	/* disable */
#define	IIC0_STOPINT_ENABLE			0x10	/* enable */
/* Wait	and	interrupt generation (WTIM0) */
#define	IIC0_WAITINT				0x08
#define	IIC0_WAITINT_CLK8FALLING	0x00	/* generate	at the eighth clocks falling edge	*/	 
#define	IIC0_WAITINT_CLK9FALLING	0x08	/* generated at	the	ninth clocks falling edge	*/ 
/* Acknowledgement control (ACKE0) */
#define	IIC0_ACK					0x04
#define	IIC0_ACK_DISABLE			0x00	/* enable acknowledgement */
#define	IIC0_ACK_ENABLE				0x04	/* disable acknowledgement */
/* Start condition trigger (STT0) */
#define	IIC0_STARTCONDITION			0x02
#define	IIC0_START_NOTGENERATE		0x00	/* do not generate start condition */
#define	IIC0_START_GENERATE			0x02	/* generate	start condition	*/
/* Stop	condition trigger (SPT0) */
#define	IIC0_STOPCONDITION			0x01
#define	IIC0_STOP_NOTGENERATE		0x00	/* do not generate stop	condition */
#define	IIC0_STOP_GENERATE			0x01	/* generate	stop condition */

/*
	IIC	Status Register	0 (IICS0)
*/
/* Master device status	(MSTS0)	*/
#define	IIC0_MASTERSTATUS			0x80
#define	IIC0_STATUS_NOTMASTER		0x00	/* slave device	status or communication	standby	status */
#define	IIC0_STATUS_MASTER			0x80	/* master device communication status */
/* Detection of	arbitration	loss (ALD0)	*/
#define	IIC0_ARBITRATION			0x40
#define	IIC0_ARBITRATION_NO			0x00	/* arbitration win or no arbitration */
#define	IIC0_ARBITRATION_LOSS		0x40	/* arbitration loss	*/
/* Detection of	extension code reception (EXC0)	*/
#define	IIC0_EXTENSIONCODE			0x20
#define	IIC0_EXTCODE_NOT			0x00	/* extension code not received */
#define	IIC0_EXTCODE_RECEIVED		0x20	/* extension code received */
/* Detection of	matching addresses (COI0) */
#define	IIC0_ADDRESSMATCH			0x10
#define	IIC0_ADDRESS_NOTMATCH		0x00	/* addresses do	not	match */
#define	IIC0_ADDRESS_MATCH			0x10	/* addresses match */
/* Detection of	transmit/receive status	(TRC0) */
#define	IIC0_STATUS					0x08
#define	IIC0_STATUS_RECEIVE			0x00	/* receive status */ 
#define	IIC0_STATUS_TRANSMIT		0x08	/* transmit	status */
/* Detection of	acknowledge	signal (ACKD0) */
#define	IIC0_ACKDETECTION			0x04
#define	IIC0_ACK_NOTDETECTED		0x00	/* ACK signal was not detected */
#define	IIC0_ACK_DETECTED			0x04	/* ACK signal was detected */
/* Detection of	start condition	(STD0) */
#define	IIC0_STARTDETECTION			0x02
#define	IIC0_START_NOTDETECTED		0x00	/* start condition not detected	*/
#define	IIC0_START_DETECTED			0x02	/* start condition detected	*/
/* Detection of	stop condition (SPD0) */
#define	IIC0_STOPDETECTION			0x01
#define	IIC0_STOP_NOTDETECTED		0x00	/* stop	condition not detected */
#define	IIC0_STOP_DETECTED			0x01	/* stop	condition detected */

/*
	IIC	Flag Register 0	(IICF0)
*/
/* STT0	clear flag (STCF) */
#define	IIC0_STARTFLAG				0x80
#define	IIC0_STARTFLAG_GENERATE		0x00	/* generate	start condition	*/
#define	IIC0_STARTFLAG_UNSUCCESSFUL	0x80	/* start condition generation unsuccessful */
/* IIC bus status flag (IICBSY)	*/
#define	IIC0_BUSSTATUS				0x40
#define	IIC0_BUS_RELEASE			0x00	/* bus release status */
#define	IIC0_BUS_COMMUNICATION		0x40	/* bus communication status	*/
/* Initial start enable	trigger	(STCEN)	*/
#define	IIC0_STARTWITHSTOP			0x02
#define	IIC0_START_WITHSTOP			0x00	/* generation of a start condition without detecting a stop	condition */
#define	IIC0_START_WITHOUTSTOP		0x02	/* generation of a start condition upon	detection of a stop	condition */
/* Communication reservation function disable bit (IICRSV) */
#define	IIC0_RESERVATION			0x01
#define	IIC0_RESERVATION_ENABLE		0x00	/* enable communication	reservation	*/	
#define	IIC0_RESERVATION_DISABLE	0x01	/* disable communication reservation */		

/*
	IIC	clock selection	register 0 (IICCL0)
*/

#define IICCL0_INITIALVALUE   			0x00
/* Detection of	SCL0 pin level (CLD0) */
#define	IIC0_SCLLEVEL				0x20
#define	IIC0_SCL_LOW				0x00	/* clock line at low level */
#define	IIC0_SCL_HIGH				0x20	/* clock line at high level	*/
/* Detection of	SDA0 pin level (DAD0) */
#define	IIC0_SDALEVEL				0x10
#define	IIC0_SDA_LOW				0x00	/* data	line at	low	level */
#define	IIC0_SDA_HIGH				0x10	/* data	line at	high level */
/* Operation mode switching	(SMC0) */
#define	IIC0_OPERATIONMODE			0x08
#define	IIC0_MODE_STANDARD			0x00	/* operates	in standard	mode */
#define	IIC0_MODE_HIGHSPEED			0x08	/* operates	in high-speed mode */
/* Digital filter operation	control	(DFC0) */
#define	IIC0_DIGITALFILTER			0x04
#define	IIC0_FILTER_OFF				0x00	/* digital filter off */ 
#define	IIC0_FILTER_ON				0x04	/* digital filter on */
/* Operation mode switching	(CL01, CL00) */
#define	IIC0_CLOCKSELECTION			0x03
/*	Combine	of (SMC0, CL01,	CL00)*/
#define	IIC0_CLOCK0					0x00
#define	IIC0_CLOCK1					0x01
#define	IIC0_CLOCK2					0x02
#define	IIC0_CLOCK3					0x03
#define	IIC0_CLOCK4					0x08
#define	IIC0_CLOCK5					0x09
#define	IIC0_CLOCK6					0x0a
#define	IIC0_CLOCK7					0x0b

/*
	IIC	function expansion register	0 (IICX0)
*/
/* IIC clock expension (CLX0) */
#define	IIC0_CLOCKEXPENSION			0x01
#define	IIC0_EXPENSION0				0x00
#define	IIC0_EXPENSION1				0x01
/*	Operation clock	(CLX0, SMC0, CL01, CL00)

				|	IIC0_EXPENSION0	|	IIC0_EXPENSION1	|								 
	------------|-------------------|-------------------|----------------------
	IIC0_CLOCK0	|		fCLK/88		|					|	transfer clock
				|		normal		|					|	mode
	------------|-------------------|-------------------|----------------------
	IIC0_CLOCK1	|		fCLK/172	|					|	transfer clock
				|		normal		|					|	mode
	------------|-------------------|-------------------|----------------------
	IIC0_CLOCK2	|		fCLK/344	|					|	transfer clock
				|		normal		|					|	mode
	------------|-------------------|-------------------|----------------------
	IIC0_CLOCK3	|		fCLK/44		|					|	transfer clock
				|		normal		|					|	mode
	------------|-------------------|-------------------|----------------------
	IIC0_CLOCK4	|		fCLK/48		|		fCLK/24		|	transfer clock
				|	  	fast	 	|		fast		|	mode
	------------|-------------------|-------------------|----------------------			   
	IIC0_CLOCK5	|		fCLK/48		|		fCLK/24		|	transfer clock
				|	  	fast		|		fast		|	mode
	------------|-------------------|-------------------|----------------------
	IIC0_CLOCK6	|		fCLK/96		|		fCLK/48		|	transfer clock
				|	  	fast		|		fast		|	mode
	------------|-------------------|-------------------|----------------------
	IIC0_CLOCK7	|		fCLK/24		|		fCLK/24		|	transfer clock
				|	  	fast		|		fast		|	mode
	------------|-------------------|-------------------|----------------------
*/

#define ADDRESS_COMPLETE   			0x80
#define IIC0_MASTER_FLAG_CLEAR 			0x00

/*
*******************************************************************************
**  Macro define
*******************************************************************************
*/
/* baudrate setting for RP_CPU_CLK == 32 */

#define UART_BAUDRATE_115kBPS	        0
#define UART_BAUDRATE_230kBPS	        1
#define UART_BAUDRATE_500kBPS	        2

/* 115200 baud */
#define UART_CLOCK_SELECT_115kBPS	0x01    /* fCLK/2 = 16MHz */
#define UART_DIVISION_RATE_115kBPS	0x8800  /* SDR[15:9] = 0x44 = 68 */

/* 230400 baud */
#define UART_CLOCK_SELECT_230kBPS	0x00    /* fCLK = 32MHz */
#define UART_DIVISION_RATE_230kBPS	0x8800  /* SDR[15:9] = 0x44 = 68 */

/* 500000 baud */
#define UART_CLOCK_SELECT_500kBPS	0x00    /* fCLK = 32MHz */
#define UART_DIVISION_RATE_500kBPS	0x3E00  /* SDR[15:9] = 0x1F = 31 */


enum TransferMode { SEND, RECEIVE };
/* Start user code for macro definition. Do not edit comment generated here */
/* End user code for macro definition. Do not edit comment generated here */

/*
*******************************************************************************
**	Register bit define
*******************************************************************************
*/
/*
	Peripheral Enable Register 0 (PER0)
*/
/* Control of timer array unit input clock(TAU0EN) */
#define TAU_CLOCK_CLR				0x01	/* for clear the bits */
#define TAU_CLOCK_STOP				0x00	/* stops supply of input clock */
#define TAU_CLOCK_SUPPLY			0x01	/* supplies input clock */

/*
	Timer Clock Select Register 0 (TPS0)
*/
/* Initial Value */
#define TAU_TPS0_INITIALVALUE		0x0000
/* Operating mode and clear mode selection(PRS003 & PRS002 & PRS001 & PRS000) */
#define TAU_CK00_FCLK_0				0x0000	/* ck00-fclk */
#define TAU_CK00_FCLK_1				0x0001	/* ck00-fclk/2^1 */
#define TAU_CK00_FCLK_2				0x0002	/* ck00-fclk/2^2 */
#define TAU_CK00_FCLK_3				0x0003	/* ck00-fclk/2^3 */
#define TAU_CK00_FCLK_4				0x0004	/* ck00-fclk/2^4 */
#define TAU_CK00_FCLK_5				0x0005	/* ck00-fclk/2^5 */
#define TAU_CK00_FCLK_6				0x0006	/* ck00-fclk/2^6 */
#define TAU_CK00_FCLK_7				0x0007	/* ck00-fclk/2^7 */
#define TAU_CK00_FCLK_8				0x0008	/* ck00-fclk/2^8 */
#define TAU_CK00_FCLK_9				0x0009	/* ck00-fclk/2^9 */
#define TAU_CK00_FCLK_10			0x000A	/* ck00-fclk/2^10 */
#define TAU_CK00_FCLK_11			0x000B	/* ck00-fclk/2^11 */
#define TAU_CK00_FCLK_12			0x000C	/* ck00-fclk/2^12 */
#define TAU_CK00_FCLK_13			0x000D	/* ck00-fclk/2^13 */
#define TAU_CK00_FCLK_14			0x000E	/* ck00-fclk/2^14 */
#define TAU_CK00_FCLK_15			0x000F	/* ck00-fclk/2^15 */
/* Operating mode and clear mode selection(PRS013 & PRS012 & PRS011 & PRS010) */
#define TAU_CK01_FCLK_0				0x0000	/* ck01-fclk */
#define TAU_CK01_FCLK_1				0x0010	/* ck01-fclk/2^1 */
#define TAU_CK01_FCLK_2				0x0020	/* ck01-fclk/2^2 */
#define TAU_CK01_FCLK_3				0x0030	/* ck01-fclk/2^3 */
#define TAU_CK01_FCLK_4				0x0040	/* ck01-fclk/2^4 */
#define TAU_CK01_FCLK_5				0x0050	/* ck01-fclk/2^5 */
#define TAU_CK01_FCLK_6				0x0060	/* ck01-fclk/2^6 */
#define TAU_CK01_FCLK_7				0x0070	/* ck01-fclk/2^7 */
#define TAU_CK01_FCLK_8				0x0080	/* ck01-fclk/2^8 */
#define TAU_CK01_FCLK_9				0x0090	/* ck01-fclk/2^9 */
#define TAU_CK01_FCLK_10			0x00A0	/* ck01-fclk/2^10 */
#define TAU_CK01_FCLK_11			0x00B0	/* ck01-fclk/2^11 */
#define TAU_CK01_FCLK_12			0x00C0	/* ck01-fclk/2^12 */
#define TAU_CK01_FCLK_13			0x00D0	/* ck01-fclk/2^13 */
#define TAU_CK01_FCLK_14			0x00E0	/* ck01-fclk/2^14 */
#define TAU_CK01_FCLK_15			0x00F0	/* ck01-fclk/2^15 */

/*
	Timer Mode Register 0n (TMR0n)
*/
/* Initial Value */
#define TAU_TMR0_INITIALVALUE		0x0000
/* Selection of macro clock (MCK) of channel n(CKS0n) */
#define TAU_CLOCK_SELECT_CLR		0x8000	/* for clear the bits */
#define TAU_CLOCK_SELECT_CK00		0x0000	/* operation clock CK0 set by PRS register */ 
#define TAU_CLOCK_SELECT_CK01		0x8000	/* operation clock CK1 set by PRS register */
/* Selection of count clock (CCK) of channel n(CCS0n) */
#define TAU_CLOCK_MODE_CLR			0x1000	/* for clear the bits */
#define TAU_CLOCK_MODE_CKS			0x0000	/* macro clock MCK specified by CKS0n bit */  
#define TAU_CLOCK_MODE_TI0N			0x1000	/* valid edge of input signal input from TI0n pin */
/* Selection of slave/master of channel n(MASTER0n) */
#define TAU_COMBINATION_CLR			0x0800	/* for clear the bits */
#define TAU_COMBINATION_SLAVE		0x0000	/* operates as slave channel with combination operation function */  
#define TAU_COMBINATION_MASTER		0x0800	/* operates as master channel with combination operation function */
/* Setting of start trigger or capture trigger of channel n(STS0n2 & STS0n1 & STS0n0) */
#define TAU_TRIGGER_CLR				0x0700	/* for clear the bits */
#define TAU_TRIGGER_SOFTWARE		0x0000	/* only software trigger start is valid */
#define TAU_TRIGGER_TI0N_VALID		0x0100	/* valid edge of TI0n pin input is used as both the start trigger and capture trigger */
#define TAU_TRIGGER_TI0N_BOTH		0x0200	/* both the edges of TI0n pin input are used as a start trigger and a capture trigger */
#define TAU_TRIGGER_MASTER_INT		0x0400	/* interrupt signal of the master channel is used */
/* Selection of TI0n pin input valid edge(CIS0n1 & CIS0n0) */
#define TAU_TI0N_EDGE_CLR			0x00C0	/* for clear the bits */
#define TAU_TI0N_EDGE_FALLING		0x0000	/* falling edge */
#define TAU_TI0N_EDGE_RISING		0x0040	/* rising edge */
#define TAU_TI0N_EDGE_BOTH_LOW		0x0080	/* both edges (when low-level width is measured) */
#define TAU_TI0N_EDGE_BOTH_HIGH		0x00C0	/* both edges (when high-level width is measured) */
/* Operation mode of channel n(MD0n3 & MD0n2 & MD0n1 & MD0n0) */
#define TAU_MODE_CLR				0x000F	/* for clear the bits */
#define TAU_MODE_INTERVAL_TIMER		0x0000	/* interval timer mode */
#define TAU_MODE_EVENT_COUNT 		0x0006	/* event counter mode */
#define TAU_MODE_CAPTURE			0x0004	/* capture mode */
#define TAU_MODE_HIGHLOW_MEASURE	0x000C	/* high-/low-level width measurement mode */
#define TAU_MODE_PWM_MASTER			0x0001	/* PWM Function (Master Channel) mode */
#define TAU_MODE_PWM_SLAVE			0x0009	/* PWM Function (Slave Channel) mode */
#define TAU_MODE_ONESHOT			0x0008	/* one-shot pulse output mode */
/* Setting of starting counting and interrupt(MD0n0) */
#define TAU_START_INT_CLR			0x0001	/* for clear the bits */
#define TAU_START_INT_UNUSED		0x0000	/* timer interrupt is not generated when counting is started (timer output does not change, either) */  
#define TAU_START_INT_USED			0x0001	/* timer interrupt is generated when counting is started (timer output also changes) */

/*
	Timer Status Register 0n (TSR0n)
*/
/* Initial Value */
#define TAU_TSR0_INITIALVALUE		0x0000
/* Counter overflow status of channel n(OVF) */
#define TAU_OVERFLOW_STATUS			0x0001
#define TAU_OVERFLOW_NOT_OCCURS		0x0000	/* overflow does not occur */
#define TAU_OVERFLOW_OCCURS			0x0001	/* overflow occurs */

/*
	Timer Channel Enable Status Register 0 (TE0)
*/
/* Initial Value */
#define TAU_TE0_INITIALVALUE		0x0000
/* Indication of operation enable/stop status of channel 0(TE00) */
#define TAU_CH0_OPERATION_STOP		0x0000	/* operation is stopped */
#define TAU_CH0_OPERATION_ENABLE	0x0001	/* operation is enabled */
/* Indication of operation enable/stop status of channel 1(TE01) */
#define TAU_CH1_OPERATION_STOP		0x0000	/* operation is stopped */
#define TAU_CH1_OPERATION_ENABLE	0x0002	/* operation is enabled */
/* Indication of operation enable/stop status of channel 2(TE02) */
#define TAU_CH2_OPERATION_STOP		0x0000	/* operation is stopped */
#define TAU_CH2_OPERATION_ENABLE	0x0004	/* operation is enabled */
/* Indication of operation enable/stop status of channel 3(TE03) */
#define TAU_CH3_OPERATION_STOP		0x0000	/* operation is stopped */
#define TAU_CH3_OPERATION_ENABLE	0x0008	/* operation is enabled */
/* Indication of operation enable/stop status of channel 4(TE04) */
#define TAU_CH4_OPERATION_STOP		0x0000	/* operation is stopped */
#define TAU_CH4_OPERATION_ENABLE	0x0010	/* operation is enabled */
/* Indication of operation enable/stop status of channel 5(TE05) */
#define TAU_CH5_OPERATION_STOP		0x0000	/* operation is stopped */
#define TAU_CH5_OPERATION_ENABLE	0x0020	/* operation is enabled */
/* Indication of operation enable/stop status of channel 6(TE06) */
#define TAU_CH6_OPERATION_STOP		0x0000	/* operation is stopped */
#define TAU_CH6_OPERATION_ENABLE	0x0040	/* operation is enabled */
/* Indication of operation enable/stop status of channel 7(TE07) */
#define TAU_CH7_OPERATION_STOP		0x0000	/* operation is stopped */
#define TAU_CH7_OPERATION_ENABLE	0x0080	/* operation is enabled */

/*
	Timer Channel Start Register 0 (TS0)
*/
/* Initial Value */
#define TAU_TS0_INITIALVALUE		0x0000
/* Operation enable (start) trigger of channel 0(TS00) */
#define TAU_CH0_START_TRG_OFF		0x0000	/* no trigger operation */
#define TAU_CH0_START_TRG_ON		0x0001	/* operation is enabled (start software trigger is generated) */
/* Operation enable (start) trigger of channel 1(TS01) */
#define TAU_CH1_START_TRG_OFF		0x0000	/* no trigger operation */
#define TAU_CH1_START_TRG_ON		0x0002	/* operation is enabled (start software trigger is generated) */
/* Operation enable (start) trigger of channel 2(TS02) */
#define TAU_CH2_START_TRG_OFF		0x0000	/* no trigger operation */
#define TAU_CH2_START_TRG_ON		0x0004	/* operation is enabled (start software trigger is generated) */
/* Operation enable (start) trigger of channel 3(TS03) */
#define TAU_CH3_START_TRG_OFF		0x0000	/* no trigger operation */
#define TAU_CH3_START_TRG_ON		0x0008	/* operation is enabled (start software trigger is generated) */
/* Operation enable (start) trigger of channel 4(TS04) */
#define TAU_CH4_START_TRG_OFF		0x0000	/* no trigger operation */
#define TAU_CH4_START_TRG_ON		0x0010	/* operation is enabled (start software trigger is generated) */
/* Operation enable (start) trigger of channel 5(TS05) */
#define TAU_CH5_START_TRG_OFF		0x0000	/* no trigger operation */
#define TAU_CH5_START_TRG_ON		0x0020	/* operation is enabled (start software trigger is generated) */
/* Operation enable (start) trigger of channel 6(TS06) */
#define TAU_CH6_START_TRG_OFF		0x0000	/* no trigger operation */
#define TAU_CH6_START_TRG_ON		0x0040	/* operation is enabled (start software trigger is generated) */
/* Operation enable (start) trigger of channel 7(TS07) */
#define TAU_CH7_START_TRG_OFF		0x0000	/* no trigger operation */
#define TAU_CH7_START_TRG_ON		0x0080	/* operation is enabled (start software trigger is generated) */

/*
	Timer Channel Stop Register 0 (TT0)
*/
/* Initial Value */
#define TAU_TT0_INITIALVALUE		0x0000
/* Operation stop trigger of channel 0(TT00) */
#define TAU_CH0_STOP_TRG_OFF		0x0000	/* no trigger operation */
#define TAU_CH0_STOP_TRG_ON			0x0001	/* operation is stopped (stop trigger is generated) */
/* Operation stop trigger of channel 1(TT01) */
#define TAU_CH1_STOP_TRG_OFF		0x0000	/* no trigger operation */
#define TAU_CH1_STOP_TRG_ON			0x0002	/* operation is stopped (stop trigger is generated) */
/* Operation stop trigger of channel 2(TT02) */
#define TAU_CH2_STOP_TRG_OFF		0x0000	/* no trigger operation */
#define TAU_CH2_STOP_TRG_ON			0x0004	/* operation is stopped (stop trigger is generated) */
/* Operation stop trigger of channel 3(TT03) */
#define TAU_CH3_STOP_TRG_OFF		0x0000	/* no trigger operation */
#define TAU_CH3_STOP_TRG_ON			0x0008	/* operation is stopped (stop trigger is generated) */
/* Operation stop trigger of channel 4(TT04) */
#define TAU_CH4_STOP_TRG_OFF		0x0000	/* no trigger operation */
#define TAU_CH4_STOP_TRG_ON			0x0010	/* operation is stopped (stop trigger is generated) */
/* Operation stop trigger of channel 5(TT05) */
#define TAU_CH5_STOP_TRG_OFF		0x0000	/* no trigger operation */
#define TAU_CH5_STOP_TRG_ON			0x0020	/* operation is stopped (stop trigger is generated) */
/* Operation stop trigger of channel 6(TT06) */
#define TAU_CH6_STOP_TRG_OFF		0x0000	/* no trigger operation */
#define TAU_CH6_STOP_TRG_ON			0x0040	/* operation is stopped (stop trigger is generated) */
/* Operation stop trigger of channel 7(TT07) */
#define TAU_CH7_STOP_TRG_OFF		0x0000	/* no trigger operation */
#define TAU_CH7_STOP_TRG_ON			0x0080	/* operation is stopped (stop trigger is generated) */

/*
	Timer Input Select Register 0 (TIS0)
*/
/* Initial Value */
#define TAU_TIS0_INITIALVALUE		0x00
/* Selection of timer input/subsystem clock used with channel 0(TIS00) */
#define TAU_CH0_INPUT_TI0N			0x00	/* input signal of timer input pin (TI00) */
#define TAU_CH0_INPUT_FXT			0x01	/* subsystem clock divided by 4 (fXT/4) */
/* Selection of timer input/subsystem clock used with channel 1(TIS01) */
#define TAU_CH1_INPUT_TI0N			0x00	/* input signal of timer input pin (TI01) */
#define TAU_CH1_INPUT_FXT			0x02	/* subsystem clock divided by 4 (fXT/4) */
/* Selection of timer input/subsystem clock used with channel 2(TIS02) */
#define TAU_CH2_INPUT_TI0N			0x00	/* input signal of timer input pin (TI02) */
#define TAU_CH2_INPUT_FXT			0x04	/* subsystem clock divided by 4 (fXT/4) */
/* Selection of timer input/subsystem clock used with channel 3(TIS03) */
#define TAU_CH3_INPUT_TI0N			0x00	/* input signal of timer input pin (TI03) */
#define TAU_CH3_INPUT_FXT			0x08	/* subsystem clock divided by 4 (fXT/4) */
/* Selection of timer input/subsystem clock used with channel 4(TIS04) */
#define TAU_CH4_INPUT_TI0N			0x00	/* input signal of timer input pin (TI04) */
#define TAU_CH4_INPUT_FXT			0x10	/* subsystem clock divided by 4 (fXT/4) */
/* Selection of timer input/subsystem clock used with channel 5(TIS05) */
#define TAU_CH5_INPUT_TI0N			0x00	/* input signal of timer input pin (TI05) */
#define TAU_CH5_INPUT_FXT			0x20	/* subsystem clock divided by 4 (fXT/4) */
/* Selection of timer input/subsystem clock used with channel 6(TIS06) */
#define TAU_CH6_INPUT_TI0N			0x00	/* input signal of timer input pin (TI06) */
#define TAU_CH6_INPUT_FXT			0x40	/* subsystem clock divided by 4 (fXT/4) */
/* Selection of timer input/subsystem clock used with channel 7(TIS07) */
#define TAU_CH7_INPUT_TI0N			0x00	/* input signal of timer input pin (TI07) */
#define TAU_CH7_INPUT_FXT			0x80	/* subsystem clock divided by 4 (fXT/4) */

/*
	Timer Output Enable Register 0 (TOE0)
*/
/* Initial Value */
#define TAU_TOE0_INITIALVALUE		0x0000
/* Timer output enable/disable of channel 0(TOE00) */
#define TAU_CH0_OUTPUT_ENABLE		0x0001	/* the TO00 operation enabled by count operation (timer channel output bit) */
#define TAU_CH0_OUTPUT_DISABLE		0x0000	/* the TO00 operation stopped by count operation (timer channel output bit) */
/* Timer output enable/disable of channel 1(TOE01) */
#define TAU_CH1_OUTPUT_ENABLE		0x0002	/* the TO01 operation enabled by count operation (timer channel output bit) */
#define TAU_CH1_OUTPUT_DISABLE		0x0000	/* the TO01 operation stopped by count operation (timer channel output bit) */
/* Timer output enable/disable of channel 2(TOE02) */
#define TAU_CH2_OUTPUT_ENABLE		0x0004	/* the TO02 operation enabled by count operation (timer channel output bit) */
#define TAU_CH2_OUTPUT_DISABLE		0x0000	/* the TO02 operation stopped by count operation (timer channel output bit) */
/* Timer output enable/disable of channel 3(TOE03) */
#define TAU_CH3_OUTPUT_ENABLE		0x0008	/* the TO03 operation enabled by count operation (timer channel output bit) */
#define TAU_CH3_OUTPUT_DISABLE		0x0000	/* the TO03 operation stopped by count operation (timer channel output bit) */
/* Timer output enable/disable of channel 4(TOE04) */
#define TAU_CH4_OUTPUT_ENABLE		0x0010	/* the TO04 operation enabled by count operation (timer channel output bit) */
#define TAU_CH4_OUTPUT_DISABLE		0x0000	/* the TO04 operation stopped by count operation (timer channel output bit) */
/* Timer output enable/disable of channel 5(TOE05) */
#define TAU_CH5_OUTPUT_ENABLE		0x0020	/* the TO05 operation enabled by count operation (timer channel output bit) */
#define TAU_CH5_OUTPUT_DISABLE		0x0000	/* the TO05 operation stopped by count operation (timer channel output bit) */
/* Timer output enable/disable of channel 6(TOE06) */
#define TAU_CH6_OUTPUT_ENABLE		0x0040	/* the TO06 operation enabled by count operation (timer channel output bit) */
#define TAU_CH6_OUTPUT_DISABLE		0x0000	/* the TO06 operation stopped by count operation (timer channel output bit) */
/* Timer output enable/disable of channel 7(TOE07) */
#define TAU_CH7_OUTPUT_ENABLE		0x0080	/* the TO07 operation enabled by count operation (timer channel output bit) */
#define TAU_CH7_OUTPUT_DISABLE		0x0000	/* the TO07 operation stopped by count operation (timer channel output bit) */

/*
	Timer Output Register 0 (TO0)
*/
/* Initial Value */
#define TAU_TO0_INITIALVALUE		0x0000
/* Timer output of channel 0(TO00) */
#define TAU_CH0_OUTPUT_VALUE_0		0x0000	/* timer output value is 0 */
#define TAU_CH0_OUTPUT_VALUE_1		0x0001	/* timer output value is 1 */
/* Timer output of channel 1(TO01) */
#define TAU_CH1_OUTPUT_VALUE_0		0x0000	/* timer output value is 0 */
#define TAU_CH1_OUTPUT_VALUE_1		0x0002	/* timer output value is 1 */
/* Timer output of channel 2(TO02) */
#define TAU_CH2_OUTPUT_VALUE_0		0x0000	/* timer output value is 0 */
#define TAU_CH2_OUTPUT_VALUE_1		0x0004	/* timer output value is 1 */
/* Timer output of channel 3(TO03) */
#define TAU_CH3_OUTPUT_VALUE_0		0x0000	/* timer output value is 0 */
#define TAU_CH3_OUTPUT_VALUE_1		0x0008	/* timer output value is 1 */
/* Timer output of channel 4(TO04) */
#define TAU_CH4_OUTPUT_VALUE_0		0x0000	/* timer output value is 0 */
#define TAU_CH4_OUTPUT_VALUE_1		0x0010	/* timer output value is 1 */
/* Timer output of channel 5(TO05) */
#define TAU_CH5_OUTPUT_VALUE_0		0x0000	/* timer output value is 0 */
#define TAU_CH5_OUTPUT_VALUE_1		0x0020	/* timer output value is 1 */
/* Timer output of channel 6(TO06) */
#define TAU_CH6_OUTPUT_VALUE_0		0x0000	/* timer output value is 0 */
#define TAU_CH6_OUTPUT_VALUE_1		0x0040	/* timer output value is 1 */
/* Timer output of channel 7(TO07) */
#define TAU_CH7_OUTPUT_VALUE_0		0x0000	/* timer output value is 0 */
#define TAU_CH7_OUTPUT_VALUE_1		0x0080	/* timer output value is 1 */

/*
	Timer Output Level Register 0 (TOL0)
*/
/* Initial Value */
#define TAU_TOL0_INITIALVALUE		0x0000
/* Control of timer output level of channel 0(TOL00) */
#define TAU_CH0_OUTPUT_LEVEL_H		0x0000	/* positive logic output (active-high) */
#define TAU_CH0_OUTPUT_LEVEL_L		0x0001	/* inverted output (active-low) */
/* Control of timer output level of channel 1(TOL01) */
#define TAU_CH1_OUTPUT_LEVEL_H		0x0000	/* positive logic output (active-high) */
#define TAU_CH1_OUTPUT_LEVEL_L		0x0002	/* inverted output (active-low) */
/* Control of timer output level of channel 2(TOL02) */
#define TAU_CH2_OUTPUT_LEVEL_H		0x0000	/* positive logic output (active-high) */
#define TAU_CH2_OUTPUT_LEVEL_L		0x0004	/* inverted output (active-low) */
/* Control of timer output level of channel 3(TOL03) */
#define TAU_CH3_OUTPUT_LEVEL_H		0x0000	/* positive logic output (active-high) */
#define TAU_CH3_OUTPUT_LEVEL_L		0x0008	/* inverted output (active-low) */
/* Control of timer output level of channel 4(TOL04) */
#define TAU_CH4_OUTPUT_LEVEL_H		0x0000	/* positive logic output (active-high) */
#define TAU_CH4_OUTPUT_LEVEL_L		0x0010	/* inverted output (active-low) */
/* Control of timer output level of channel 5(TOL05) */
#define TAU_CH5_OUTPUT_LEVEL_H		0x0000	/* positive logic output (active-high) */
#define TAU_CH5_OUTPUT_LEVEL_L		0x0020	/* inverted output (active-low) */
/* Control of timer output level of channel 6(TOL06) */
#define TAU_CH6_OUTPUT_LEVEL_H		0x0000	/* positive logic output (active-high) */
#define TAU_CH6_OUTPUT_LEVEL_L		0x0040	/* inverted output (active-low) */
/* Control of timer output level of channel 7(TOL07) */
#define TAU_CH7_OUTPUT_LEVEL_H		0x0000	/* positive logic output (active-high) */
#define TAU_CH7_OUTPUT_LEVEL_L		0x0080	/* inverted output (active-low) */

/*
	Timer Output Mode Register 0 (TOM0)
*/
/* Initial Value */
#define TAU_TOM0_INITIALVALUE		0x0000
/* Control of timer output mode of channel 0(TOM00) */
#define TAU_CH0_OUTPUT_TOGGLE		0x0000	/* toggle operation mode */
#define TAU_CH0_OUTPUT_COMBIN		0x0001	/* combination operation mode */
/* Control of timer output mode of channel 1(TOM01) */
#define TAU_CH1_OUTPUT_TOGGLE		0x0000	/* toggle operation mode */
#define TAU_CH1_OUTPUT_COMBIN		0x0002	/* combination operation mode */
/* Control of timer output mode of channel 2(TOM02) */
#define TAU_CH2_OUTPUT_TOGGLE		0x0000	/* toggle operation mode */
#define TAU_CH2_OUTPUT_COMBIN		0x0004	/* combination operation mode */
/* Control of timer output mode of channel 3(TOM03) */
#define TAU_CH3_OUTPUT_TOGGLE		0x0000	/* toggle operation mode */
#define TAU_CH3_OUTPUT_COMBIN		0x0008	/* combination operation mode */
/* Control of timer output mode of channel 4(TOM04) */
#define TAU_CH4_OUTPUT_TOGGLE		0x0000	/* toggle operation mode */
#define TAU_CH4_OUTPUT_COMBIN		0x0010	/* combination operation mode */
/* Control of timer output mode of channel 5(TOM05) */
#define TAU_CH5_OUTPUT_TOGGLE		0x0000	/* toggle operation mode */
#define TAU_CH5_OUTPUT_COMBIN		0x0020	/* combination operation mode */
/* Control of timer output mode of channel 6(TOM06) */
#define TAU_CH6_OUTPUT_TOGGLE		0x0000	/* toggle operation mode */
#define TAU_CH6_OUTPUT_COMBIN		0x0040	/* combination operation mode */
/* Control of timer output mode of channel 7(TOM07) */
#define TAU_CH7_OUTPUT_TOGGLE		0x0000	/* toggle operation mode */
#define TAU_CH7_OUTPUT_COMBIN		0x0080	/* combination operation mode */

/*
	Input Switch Control Register (ISC)
*/
/* Initial Value */
#define TAU_ISC_INITIALVALUE		0x00
/* Switching channel 7 input of timer array unit(ISC1) */
#define TAU_CH7_STATUS				0x02	/* for clear the bits */
#define TAU_CH7_NO_INPUT			0x00	/* timer input is not used */
#define TAU_CH7_RXD3_INPUT			0x02	/* input signal of RxD3 pin is used as timer input*/

/*
	Noise Filter Enable Register 1 (NFEN1)
*/
/* Initial Value */
#define TAU_NFEN1_INITIALVALUE		0x00
/* Enable/disable using noise filter of TI07/TO07/P145 pin input signal(TNFEN07) */
#define TAU_CH7_NOISE_CLR			0x80	/* for clear the bits */
#define TAU_CH7_NOISE_OFF			0x00	/* noise filter OFF */
#define TAU_CH7_NOISE_ON			0x80	/* noise filter ON */
/* Enable/disable using noise filter of TI06/TO06/P06 pin input signal(TNFEN06) */
#define TAU_CH6_NOISE_CLR			0x40	/* for clear the bits */
#define TAU_CH6_NOISE_OFF			0x00	/* noise filter OFF */
#define TAU_CH6_NOISE_ON			0x40	/* noise filter ON */
/* Enable/disable using noise filter of TI05/TO05/P05 pin input signal(TNFEN05) */
#define TAU_CH5_NOISE_CLR			0x20	/* for clear the bits */
#define TAU_CH5_NOISE_OFF			0x00	/* noise filter OFF */
#define TAU_CH5_NOISE_ON			0x20	/* noise filter ON */
/* Enable/disable using noise filter of TI04/TO04/P42 pin input signal(TNFEN04) */
#define TAU_CH4_NOISE_CLR			0x10	/* for clear the bits */
#define TAU_CH4_NOISE_OFF			0x00	/* noise filter OFF */
#define TAU_CH4_NOISE_ON			0x10	/* noise filter ON */
/* Enable/disable using noise filter of TI03/TO03/INTP4/P31 pin input signal(TNFEN03) */
#define TAU_CH3_NOISE_CLR			0x08	/* for clear the bits */
#define TAU_CH3_NOISE_OFF			0x00	/* noise filter OFF */
#define TAU_CH3_NOISE_ON			0x08	/* noise filter ON */
/* Enable/disable using noise filter of TI02/TO02/P17 pin input signal(TNFEN02) */
#define TAU_CH2_NOISE_CLR			0x04	/* for clear the bits */
#define TAU_CH2_NOISE_OFF			0x00	/* noise filter OFF */
#define TAU_CH2_NOISE_ON			0x04	/* noise filter ON */
/* Enable/disable using noise filter of TI01/TO01/INTP5/P16 pin input signal(TNFEN01) */
#define TAU_CH1_NOISE_CLR			0x02	/* for clear the bits */
#define TAU_CH1_NOISE_OFF			0x00	/* noise filter OFF */
#define TAU_CH1_NOISE_ON			0x02	/* noise filter ON */
/* Enable/disable using noise filter of TI00/P00 pin input signal(TNFEN00) */
#define TAU_CH0_NOISE_CLR			0x01	/* for clear the bits */
#define TAU_CH0_NOISE_OFF			0x00	/* noise filter OFF */
#define TAU_CH0_NOISE_ON			0x01	/* noise filter ON */

/*
*******************************************************************************
**  Macro define
*******************************************************************************
*/
/* 16-bit timer data register 00 (TDR00) */
#define TAU_TDR00_VALUE	0xF9F			// 1ms

/* Clock divisor for channel0 */
#define TAU_CHANNEL0_DIVISOR	4
/* Start user code for macro definition. Do not edit comment generated here */
/* End user code for macro definition. Do not edit comment generated here */
/*
*******************************************************************************
**  Function define
*******************************************************************************
*/

#define _FAR_ __far

#endif	/* __RL78G13_H__ */

