/****************************************************************************** 
* DISCLAIMER 

* This software is supplied by Renesas Electronics Corporation and is only 
* intended for use with Renesas products. No other uses are authorized. 

* This software is owned by Renesas Electronics Corporation and is protected under 
* all applicable laws, including copyright laws. 

* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES 
* REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, 
* INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
* PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY 
* DISCLAIMED. 

* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS 
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE 
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES 
* FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS 
* AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. 

* Renesas reserves the right, without notice, to make changes to this 
* software and to discontinue the availability of this software. 
* By using this software, you agree to the additional terms and 
* conditions found by accessing the following link: 
* http://www.renesas.com/disclaimer 
******************************************************************************/ 

/* Copyright (C) 2014. Renesas Electronics Corporation, All Rights Reserved. */

/****************************************************************************** 
* File Name : hardwarer.c
* Version : 1.00 
* Device(s) : RL78/G1H
* Tool-Chain :
* OS :
* H/W Platform : 
* Description : Hardware dependency routines for RL78/G1H
* Operation : 
* Limitations : None 
****************************************************************************** 
* History : DD.MM.YYYY Version Description 
* 
******************************************************************************/ 
#include <ior5f11fll.h>
#include <ior5f11fll_ext.h>
#include "r_frequency_hopping.h"

#if R_MODEM_SERVER || R_MODEM_CLIENT
#include "r_modem.h"
#endif

//#include "iodefine.h" 
/****************************************************************************** 
Pragma Define
******************************************************************************/ 

#if defined (__CCRL__)
/* Stack area for interrupt handles is defined below("unsigned short RdrvUARTHdlStack[16]") */
#pragma interrupt RdrvUART_RxInt(vect=INTSR1)
#pragma interrupt RdrvUART_RxErrInt(vect=INTSRE1)

/* Stack area for interrupt handle is defined below("unsigned short RdrvITHdlStack[16]") */
#pragma interrupt RdrvIT_FreqHoppInt(vect=INTIT)
#endif

/****************************************************************************** 
Includes 
******************************************************************************/ 
#include    "intrinsics.h"
#include    "hardware.h"
#include    "uart_interface.h"
#include    "phy.h"
#include    "stdio.h"
#include    "r_stdint.h"
#include    "r_nwk_api.h"

/****************************************************************************** 
Macro definitions 
******************************************************************************/
#if R_MODEM_SERVER || R_MODEM_CLIENT
#define RDRV_UART_BUFSIZ                (1)
#else
#define RDRV_UART_BUFSIZ                (R_UDP_MAX_UDP_DATA_LENGTH + 128)
#endif

/* Interrupt Level */
#define RP_APL_INTLEVEL_UARTRXs         (0) // Max Level
#define RP_APL_INTLEVEL_UARTRXs_b0      (RP_APL_INTLEVEL_UARTRXs & 0x01)
#define RP_APL_INTLEVEL_UARTRXs_b1      ((RP_APL_INTLEVEL_UARTRXs & 0x02) >> 1)

/* Clock conversion base */
#define MS2SEC     1000u 
#define SEC2MIN    60u

/****************************************************************************** 
Private global variables and functions 
******************************************************************************/ 
static void RdrvUART_Initialize(unsigned char brgc);
static void RdrvLED_Initialize(void);

//static void RdrvUART_RxInt(void);
//static void RdrvUART_RxErrInt(void);
//static void RdrvTM02_FreqHoppInt(void);

static void Rp_UART_RxInt(void);
static void Rp_UART_RxErrInt(void);

#if 0
static void RdrvSwitch_Initialize(void);
#endif

volatile static unsigned short freqHoppTmIntervalMs;
volatile static short clock_correction_rtc;

/****************************************************************************** 
Exported global variables and functions (to be accessed by other files) 
******************************************************************************/
unsigned char           RdrvUART_RXBuf[RDRV_UART_BUFSIZ];                   /* UART RX Buffer */
unsigned short          RdrvUART_RXBuf_RdPtr;                               /* UART RX Buffer read pointer */
unsigned short          RdrvUART_RXBuf_WrPtr;                               /* UART RX Buffer write pointer */

#if RTOS_USE
unsigned short          RdrvUARTHdlStack[16];                               /* StackArea for UART interrupt handler */
unsigned short          RdrvITHdlStack[16];                                 /* StackArea for IT interrupt handler */
#endif /* RTOS_USE */

/****************************************************************************** 
Inline Assembler functions for special Stack Pointer handling
******************************************************************************/ 
#if RTOS_USE

void RpIntStackChange(uint16_t pstk)
{
asm ("movw bc, sp");
asm ("movw sp, ax"); // move SP
asm ("push bc    "); //store original SP to the buffer
asm ("subw sp, #0x02");
}

void RpIntStackBack(void)
{
asm ("addw sp, #0x02");
asm ("pop ax");
asm ("movw sp, ax"); // move SP to the original
}

#endif /* RTOS_USE */

/******************************************************************************
Function Name:       RdrvPeripheralInitialize
Parameters:          none.
Return value:        none.
Description:         Initialize peripheral settings.
******************************************************************************/
void
RdrvPeripheralInitialize(void)
{
    __disable_interrupt();                  // Disable interrupt

    // -------------------------------------------------
    //   I/O port settings
    // -------------------------------------------------

    // Port settings
    PM7     = 0x38;         // P73-P75     ... Input(SW2,SW3,SW4)
    PU7     = 0x38;         // P73-P75     ... Pullup ON(SW2,SW3,SW4)

    PM14_bit.no0 = 1;       // P140        ... Input(SW1-1)
    PU14_bit.no0 = 1;       // PU140       ... Pullup ON(SW1-1)
    
    // -------------------------------------------------
    //   UART settings
    // -------------------------------------------------
    // Initialize
#if !R_MODEM_SERVER        
    RdrvUART_Initialize(UART_BAUDRATE_500kBPS);
#else
    RdrvUART_Initialize(UART_BAUDRATE_230kBPS);
#endif
    // -------------------------------------------------
    //   LED settings
    // -------------------------------------------------
    // Initialize
    RdrvLED_Initialize();

    // -------------------------------------------------
    //   Interrupt Settings
    // -------------------------------------------------
    __enable_interrupt();                   // Enable Interrupt
}

/******************************************************************************
Function Name:       RdrvUART_Initialize
Parameters:          bgrc
                        Baudrate
Return value:        none.
Description:         Initialize UART settings.
******************************************************************************/
static void
RdrvUART_Initialize(unsigned char brgc)
{
    SAU0EN = 1;                 // Enable Serial array unit(UART1=SAU0)

    __no_operation();           // Wait for safety
    __no_operation();
    __no_operation();
    __no_operation();           

    /* UART1 initial setting */
    ST0  |= 0x000C;             // UART1 receive & transmit disable
    
    STMK1 = 1U;    /* disable INTST1 interrupt */
    STIF1 = 0U;    /* clear INTST1 interrupt flag */
    SRMK1 = 1U;    /* disable INTSR1 interrupt */
    SRIF1 = 0U;    /* clear INTSR1 interrupt flag */
    SREMK1 = 1U;   /* disable INTSRE1 interrupt */
    SREIF1 = 0U;   /* clear INTSRE1 interrupt flag */

    /* Set INTST1 low priority */
    STPR11 = 1U;
    STPR01 = 1U;
    /* Set INTSR1 low priority */
    SRPR11 = RP_APL_INTLEVEL_UARTRXs_b1;
    SRPR01 = RP_APL_INTLEVEL_UARTRXs_b0;
    /* Set INTSRE1 low priority */
    SREPR11 = 1U;
    SREPR01 = 1U;
    
    SMR02 = SAU_SMRMN_INITIALVALUE | SAU_CLOCK_SELECT_CK00 | SAU_TRIGGER_SOFTWARE | SAU_UART | SAU_BUFFER_EMPTY;
    SCR02 = SAU_TRANSMISSION | SAU_INTSRE_MASK | SAU_PARITY_NONE | SAU_LSB | SAU_STOP_1 | SAU_LENGTH_8;

    NFEN0 |= 0x04;              // noise filter on(UART1)
    SIR03 = 0x0007;
    
    SMR03 = SAU_SMRMN_INITIALVALUE | SAU_CLOCK_SELECT_CK00 | SAU_TRIGGER_RXD | SAU_UART | SAU_TRANSFER_END;
    SCR03 = SAU_RECEPTION | SAU_INTSRE_ENABLE | SAU_PARITY_NONE | SAU_LSB | SAU_STOP_1 | SAU_LENGTH_8;

    if(brgc == UART_BAUDRATE_500kBPS)
    {
        SPS0 = (UART_CLOCK_SELECT_500kBPS<<4) | (UART_CLOCK_SELECT_500kBPS);    
        SDR02 = UART_DIVISION_RATE_500kBPS;
        SDR03 = UART_DIVISION_RATE_500kBPS;
    }
    else if(brgc == UART_BAUDRATE_230kBPS)
    {
        SPS0 = (UART_CLOCK_SELECT_230kBPS<<4) | (UART_CLOCK_SELECT_230kBPS);    
        SDR02 = UART_DIVISION_RATE_230kBPS;
        SDR03 = UART_DIVISION_RATE_230kBPS;
    }
    else
    {   // default UART_BAUDRATE_115kBPS
        SPS0 = (UART_CLOCK_SELECT_115kBPS<<4) | (UART_CLOCK_SELECT_115kBPS);    
        SDR02 = UART_DIVISION_RATE_115kBPS;
        SDR03 = UART_DIVISION_RATE_115kBPS;
    }

    SO0  |= SAU_CH2_DATA_OUTPUT_1;
    SOL0 |= SAU_CHANNEL2_NORMAL;    /* output level normal */
    SOE0 |= SAU_CH2_OUTPUT_ENABLE;    /* enable UART1 output */

    /* Set RxD1 pin */
    PMC0 &= 0xF7U;
    PM0 |= 0x08U;
    /* Set TxD1 pin */
    PMC0 &= 0xFBU;
    P0 |= 0x04U;
    PM0 &= 0xFBU;

    /* TXD pin setting */
    UART_TX_PORT    = PORT_HI;

    STIF1 = 0U;    /* clear INTST1 interrupt flag */
    SRIF1 = 0U;    /* clear INTSR1 interrupt flag */
    SREIF1 = 0U;   /* clear INTSRE1 interrupt flag */

    UART_RX_INT_MASK    = 0;        // INTSR1 enable
    UART_ERR_INT_MASK   = 0;        // INTSRE1 enable
    UART_TX_INT_MASK    = 1;        // INTST1 disable

    SOE0 |= SAU_CH2_OUTPUT_ENABLE;    /* enable UART1 output */
    SS0 |= SAU_CH2_START_TRG_ON;    /* UART1 receive enable */
    SS0 |= SAU_CH3_START_TRG_ON;    /* UART1 transmit enable */

    /* Init UART TX/RX Buffer pointer */
    RdrvUART_RXBuf_RdPtr = 0x00;
    RdrvUART_RXBuf_WrPtr = 0x00;
}


/******************************************************************************
Function Name:       RdrvUART_GetLen
Parameters:          none.
Return value:        Length of received strings.
Description:         Return Length of received strings.
******************************************************************************/
short
RdrvUART_GetLen(void)
{
    short   len;

    len = (short)RdrvUART_RXBuf_WrPtr - (short)RdrvUART_RXBuf_RdPtr;

    if (len < 0)
    {
        len += RDRV_UART_BUFSIZ;
    }

    return len;
}


/******************************************************************************
Function Name:       RdrvUART_GetChar
Parameters:          none.
Return value:        Received character.
Description:         Get character from UART.
******************************************************************************/
short
RdrvUART_GetChar(void)
{
    unsigned char   ch;

    if (RdrvUART_RXBuf_RdPtr == RdrvUART_RXBuf_WrPtr)
    {
        return EOF;

    }
    else
    {
        ch = RdrvUART_RXBuf[RdrvUART_RXBuf_RdPtr];
        RdrvUART_RXBuf_RdPtr++;
        if(RdrvUART_RXBuf_RdPtr >= RDRV_UART_BUFSIZ){
            RdrvUART_RXBuf_RdPtr = 0;
        }

        return (short)ch;
    }
}


/******************************************************************************
Function Name:       RdrvUART_PutChar
Parameters:          ch
                       Character to be sent.
Return value:        none.
Description:         Put character to UART.
******************************************************************************/
void RdrvUART_PutChar(unsigned char ch)
{
    /* Wait while Tx register is not empty */
    while((UART_TX_STATUS & (SAU_DATA_STORED))!=0);

    /* Send character immediately */
    UART_TX = ch;
}


/******************************************************************************
Function Name:       RdrvUART_RxInt
Parameters:          none.
Return value:        none.
Description:         UART Rx interrupt handler.
******************************************************************************/
#if defined(RDRV_UART_RECEIVE_HANDLER)

typedef void (*RCallbackT)(void);
extern volatile RCallbackT pFuncUartRcvHandler;

#pragma interrupt RdrvUART_RxInt
static void RdrvUART_RxInt(void)
{
    (*pFuncUartRcvHandler)();
}

#else

#if defined (__ICCRL78__)
#pragma vector = INTSR1_vect
__interrupt void RdrvUART_RxInt(void)
#else
void RdrvUART_RxInt(void)
#endif
{
    #if RTOS_USE && defined(__CCRL__)
    RpIntStackChange((uint16_t)(&RdrvUARTHdlStack[16]));    // (no need '-1')
    #endif

    Rp_UART_RxInt();

    #if RTOS_USE && defined(__CCRL__)
    RpIntStackBack();
    #endif
}

static void Rp_UART_RxInt(void)
{
#if R_MODEM_SERVER || R_MODEM_CLIENT
    extern r_modem_ctx g_modem_nwk_ctx;
    r_modem_rx(UART_RX, &g_modem_nwk_ctx);
#else
    volatile unsigned char      ch;
    short                       len;

    /* Read character from UART */
    ch = UART_RX;

    /* Get number of characters in buffer */
    len = (short)RdrvUART_RXBuf_WrPtr - (short)RdrvUART_RXBuf_RdPtr;
    if(len < 0)
    {
        len += RDRV_UART_BUFSIZ;
    }

    /* Buffer overflow */
    if(len > RDRV_UART_BUFSIZ)
    {
        return;
    }

    /* Update poiner and Store character to buffer */
    RdrvUART_RXBuf[RdrvUART_RXBuf_WrPtr] = ch;

    RdrvUART_RXBuf_WrPtr++;
    if(RdrvUART_RXBuf_WrPtr >= RDRV_UART_BUFSIZ){
        RdrvUART_RXBuf_WrPtr = 0;
    }
#endif /* R_MODEM_SERVER || R_MODEM_CLIENT */
}

#endif


/******************************************************************************
Function Name:       RdrvUART_RxErrInt
Parameters:          none.
Return value:        none.
Description:         UART RxErr interrupt handler.
******************************************************************************/
#if defined (__ICCRL78__)
#pragma vector = INTSRE1_vect
__interrupt void RdrvUART_RxErrInt(void)
#else
void RdrvUART_RxErrInt(void)
#endif
{
    #if RTOS_USE && defined(__CCRL__)
    RpIntStackChange((uint16_t)(&RdrvUARTHdlStack[16]));    // (no need '-1')
    #endif

    Rp_UART_RxErrInt();

    #if RTOS_USE && defined(__CCRL__)
    RpIntStackBack();
    #endif
}

static void Rp_UART_RxErrInt(void)
{
    volatile unsigned char      ch;

    /* Read UART status */
    ch = (unsigned char)UART_RX_STATUS;

    if (ch & (SAU_PARITY_ERROR | SAU_FRAMING_ERROR | SAU_OVERRUN_ERROR)) {

        /* Error -> read dummy data then Exit */
        ch = UART_RX;

        SIR03 = (unsigned short)(SSR03 & 0x0007);
    }
}

/******************************************************************************
Function Name:       RdrvIT_FreqHoppInt
Parameters:          none.
Return value:        none.
Description:         INTIT interrupt handler.
******************************************************************************/
#if defined (__ICCRL78__)
#pragma vector = INTIT_vect
__interrupt void RdrvIT_FreqHoppInt(void)
#else
void RdrvIT_FreqHoppInt(void)
#endif
{
    #if RTOS_USE && defined(__CCRL__)
    RpIntStackChange((uint16_t)(&RdrvITHdlStack[16]));    
    #endif
 
    unsigned short msec_inc_step = 0;
    
    /* Compensate deviation from RTC if realized */ 
    if(clock_correction_rtc > 0)
    {
        /* we are behind the RTC reference -> adjust time increment accordingly */
        msec_inc_step = R_FREQ_HOPP_TIMER_INTERVAL_MS + clock_correction_rtc;
        clock_correction_rtc = 0;
    }
    else if (clock_correction_rtc < 0)
    {
        /* we are before the RTC reference -> do not expand time interval, 
           successively adjust frequency hopping timer interval */ 
        clock_correction_rtc += R_FREQ_HOPP_TIMER_INTERVAL_MS;
    }
    else
    {
        /* we are in sync with RTC reference -> do default increment  */
        msec_inc_step = R_FREQ_HOPP_TIMER_INTERVAL_MS;
    }
    
    /* check if frequency hopping timer interval has to be expanded */
    if(msec_inc_step)
    {
        /* Expand frequency hopping timer interval */
        freqHoppTmIntervalMs += msec_inc_step;
    
        /* Compensate inaccuracy of 1ms Timer Interval: 
           Interval time, 1/32.768 [kHz] x (32 + 1) = 1.00708 [ms]
           To compensate 7.08ms clock delay in 1 sec, increase schedule
           twice seven times a second, 1000ms / 7 = 143 ms */ 
        if((freqHoppTmIntervalMs % 143u) == 0)
        {
            freqHoppTmIntervalMs++;
            msec_inc_step++;
        }
    
        /* Track frequency hopping time interval of 1000ms */ 
        if( freqHoppTmIntervalMs >= MS2SEC)
        {
            freqHoppTmIntervalMs -= MS2SEC;
        }
 
        __enable_interrupt();           // Enable Interrupts
 
        /* Call Frequency Hopping Timer handler */
        RmFreqHoppTimer_Handler(msec_inc_step);
    }
    
    #if RTOS_USE && defined(__CCRL__)
    RpIntStackBack();
    #endif
}

/******************************************************************************
Function Name:       RdrvRTC_TimeCorrectionInt
Parameters:          none.
Return value:        none.
Description:         INTRTC interrupt handler.
******************************************************************************/
#if defined (__ICCRL78__)
#pragma vector = INTRTC_vect
__interrupt void RdrvRTC_TimeCorrectionInt(void)
#else
void RdrvRTC_TimeCorrectionInt(void)
#endif
{
     __enable_interrupt();           // Enable Interrupts

    /* check if frequency hopping timer is behind, before of in sync with
       the RTC time reference */ 
    if((freqHoppTmIntervalMs > (MS2SEC/2)) && (freqHoppTmIntervalMs < MS2SEC))
    {
        /* we are behind the RTC time reference */
        clock_correction_rtc = MS2SEC - freqHoppTmIntervalMs;
    } 
    else if ((freqHoppTmIntervalMs > 0) && (freqHoppTmIntervalMs <= (MS2SEC/2)))
    {
        /* we are before the RTC time reference */ 
        clock_correction_rtc = 0 - freqHoppTmIntervalMs;
    } 
    else
    { 
        /* we are in sync with the RTC time reference */
        clock_correction_rtc = 0;
    }
}

/******************************************************************************
Function Name:       RdrvLED_Initialize
Parameters:          none.
Return value:        none.
Description:         Initialize LED ports.
******************************************************************************/
static void
RdrvLED_Initialize(void)
{
    /* Turn off all LEDs */
    RdrvLED_ALL_OFF();

    /* Set port mode */
    PORT_LED0_PM = PORT_OUTPUT;
    PORT_LED1_PM = PORT_OUTPUT;
    PORT_LED2_PM = PORT_OUTPUT;
}


/******************************************************************************
Function Name:       RdrvLED_ALL_ON
Parameters:          none.
Return value:        none.
Description:         Turn on all LEDs.
******************************************************************************/
void
RdrvLED_ALL_ON(void)
{
    PORT_LED0_PORT = PORT_LO;   /* LED0 on */
    PORT_LED1_PORT = PORT_LO;   /* LED1 on */
    PORT_LED2_PORT = PORT_LO;   /* LED2 on */
}


/******************************************************************************
Function Name:       RdrvLED_ALL_OFF
Parameters:          none.
Return value:        none.
Description:         Turn off all LEDs.
******************************************************************************/
void
RdrvLED_ALL_OFF(void)
{
    PORT_LED0_PORT = PORT_HI;   /* LED0 off */
    PORT_LED1_PORT = PORT_HI;   /* LED1 off */
    PORT_LED2_PORT = PORT_HI;   /* LED2 off */
}


/******************************************************************************
Function Name:       RdrvLED0_ON
Parameters:          none.
Return value:        none.
Description:         Turn on LED0.
******************************************************************************/
void
RdrvLED0_ON(void)
{
    PORT_LED0_PORT = PORT_LO;   /* LED0 on */
}


/******************************************************************************
Function Name:       RdrvLED0_OFF
Parameters:          none.
Return value:        none.
Description:         Turn off LED0.
******************************************************************************/
void
RdrvLED0_OFF(void)
{
    PORT_LED0_PORT = PORT_HI;   /* LED0 off */
}


/******************************************************************************
Function Name:       RdrvLED1_ON
Parameters:          none.
Return value:        none.
Description:         Turn on LED1.
******************************************************************************/
void
RdrvLED1_ON(void)
{
    PORT_LED1_PORT = PORT_LO;   /* LED1 on */
}


/******************************************************************************
Function Name:       RdrvLED1_OFF
Parameters:          none.
Return value:        none.
Description:         Turn off LED1.
******************************************************************************/
void
RdrvLED1_OFF(void)
{
    PORT_LED1_PORT = PORT_HI;   /* LED1 off */
}


/******************************************************************************
Function Name:       RdrvLED2_ON
Parameters:          none.
Return value:        none.
Description:         Turn on LED2.
******************************************************************************/
void
RdrvLED2_ON(void)
{
    PORT_LED2_PORT = PORT_LO;   /* LED2 on */
}


/******************************************************************************
Function Name:       RdrvLED2_OFF
Parameters:          none.
Return value:        none.
Description:         Turn off LED2.
******************************************************************************/
void
RdrvLED2_OFF(void)
{
    PORT_LED2_PORT = PORT_HI;   /* LED2 off */
}


/******************************************************************************
Function Name:       RdrvLED0_GetCondition
Parameters:          none.
Return value:        1 : On
                     0 : Off
Description:         Get LED0 condition.
******************************************************************************/
unsigned char
RdrvLED0_GetCondition(void)
{
    unsigned char   ret;

    ret = 0;

    if(PORT_LED0_PORT == PORT_LO)
    {
        ret = 0x01;
    }

    return(ret);
}


/******************************************************************************
Function Name:       RdrvLED1_GetCondition
Parameters:          none.
Return value:        1 : On
                     0 : Off
Description:         Get LED1 condition.
******************************************************************************/
unsigned char
RdrvLED1_GetCondition(void)
{
    unsigned char   ret;

    ret = 0;

    if(PORT_LED1_PORT == PORT_LO)
    {
        ret = 0x01;
    }

    return(ret);
}


/******************************************************************************
Function Name:       RdrvLED2_GetCondition
Parameters:          none.
Return value:        1 : On
                     0 : Off
Description:         Get LED2 condition.
******************************************************************************/
unsigned char
RdrvLED2_GetCondition(void)
{
    unsigned char   ret;

    ret = 0;

    if(PORT_LED2_PORT == PORT_LO)
    {
        ret = 0x01;
    }

    return(ret);
}


/******************************************************************************
Function Name:       RdrvSwitch0_GetCondition
Parameters:          none.
Return value:        1 : On
                     0 : Off
Description:         Get switch0 condition.
******************************************************************************/
unsigned char
RdrvSwitch0_GetCondition(void)
{
    unsigned char   ret;

    ret = 0;
    
    /* Read Switch SW1-1 of TK-RLG1H+SB2 board (Device Port P140) */
    if(P14_bit.no0 == PORT_HI)
    {
        ret = 0x01;
    }

    return(ret);
}

#if defined(MCU_R78G1H)
void RdrvFREQHOPPTIMER_Initialize(void)
{
    RTCEN = 1;         /* Enables input clock supply */
    ITMC = 0;          /* Disable 12-bit interval timer */

    /* Timer settings(12bit intevral timer) for Frequency Hopping */
    /* Mask INTIT interrupt */
    ITMK = 1;       /* INTIT disabled */
    ITIF = 0;       /* INTIT interrupt flag clear */
    
    /* Set INTIT low priority */
    ITPR1 = 1;      /* Level_3 */
    ITPR0 = 1;
    
    RTCEN = 1;          /* Enable input clock source */
    OSMC &= (~0x10);    /* The subsystem clock is selected */

    ITMC  = 32;         /* Set interval time, 1/32.768 [kHz] x (32 + 1) = 1.00708 [ms] */

    ITMK = 0;           /* INTIT enabled */
 
    /* RTC initialisation for time correction */
    /* Mask INTRTC interrupt */
    RTCMK = 1;          /* INTRTC disabled */
    RTCIF = 0;          /* INTRTC interrupt flag clear */

    /* Set INTRTC priority */
    RTCPR1 = 1;     /* Level_2 */
    RTCPR0 = 0;
        
    SEC = 0;
    MIN = 0;
    
    RTCEN = 1;
    RTCC0 = 0x02;       /* Periodic interrupt generation every 1sec */
    
    RTCMK = 0;          /* INTRTC enabled */
   
    /* reset frequency hopping timer interval */ 
    freqHoppTmIntervalMs = 0;
    /* reset RTC clock correction */ 
    clock_correction_rtc = 0;

    /* Start timers */
    ITMC  |= 0x8000;     /* Start 12-bit interval timer operation */
    RTCC0 |= 0x80;       /* Start RTC timer operation */

}
#endif

#if defined(MCU_R78G1H)
void RdrvFREQHOPPTIMER_Stop(void)
{
    ITMC  = 0;     /* Stop 12-bit interval timer operation */
    RTCC0 = 0;     /* Stop RTC timer operation */
}
#endif

#if R_MODEM_SERVER
#if defined(MCU_R78G1H)
void RdrvSendOSMsg_Initialize(void)
{
    /* Use INTP7 interrupt to trigger OS messages from UART interrupt
       at a lower interrupt priority level */
      
    /* Mask INTP7 interrupt */
    PMK7 = 1;       /* INTP7 disabled */
    PIF7 = 0;       /* INTP7 interrupt request flag clear */
    
    /* Set interrupt priority to Level 1 */
    PPR07 = 1;
    PPR17 = 0;

    /* Enable INTP7 interrupt */
    PMK7 = 0;       /* INTP7 enable */
}
#endif

#if defined(MCU_R78G1H)
void RdrvSendOSMsg_Trigger(void)
{
    /* Trigger INTP7 interrupt */
    PIF7 = 1;       /* INTP7 interrupt request flag set */
}
#endif

/******************************************************************************
Function Name:       RdrvSendOSMsgTrg_Int
Parameters:          none.
Return value:        none.
Description:         Use INTP7 interrupt to trigger OS messages from UART
                     interrupt at a lower interrupt priority level 
******************************************************************************/
extern void r_modem_nwk_cb_trigger_os_msg(void);

#if defined (__ICCRL78__)
#pragma vector = INTP7_vect
__interrupt void RdrvSendOSMsgTrg_Int(void)
#else
void RdrvSendOSMsgTrg_Int(void)
#endif
{
    __enable_interrupt();           // Enable Interrupts

    r_modem_nwk_cb_trigger_os_msg();
}

#endif /* R_MODEM_SERVER */





