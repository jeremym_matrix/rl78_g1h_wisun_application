/*-------------------------------------------------------------------------
 *      This module contains the function `__low_level_init', a function
 *      that is called before the `main' function of the program.  Normally
 *      low-level initializations - such as setting the prefered interrupt
 *      level or setting the watchdog - can be performed here.
 *
 *      Note that this function is called before the data segments are
 *      initialized, this means that this function can't rely on the
 *      values of global or static variables.
 *
 *      When this function returns zero, the startup code will inhibit the
 *      initialization of the data segments.  The result is faster startup,
 *      the drawback is that neither global nor static data will be
 *      initialized.
 *
 *-------------------------------------------------------------------------
 *      Copyright 2011 IAR Systems AB.
 *      $Revision: 205 $
 *-------------------------------------------------------------------------*/

#ifdef __cplusplus
extern "C" {
#endif

#include <ior5f11fll.h>
#include <ior5f11fll_ext.h>

__root int __low_level_init ( void )
{
  /*==================================*/
  /*  Initialize hardware.            */
  /*==================================*/

    // -------------------------------------------------
    //   System clock 
    // -------------------------------------------------
    CMC		= 0x14;			// Sub Clock(XT1) and the lowest margin
    CSC		= 0x80;			// X1 Stop
    OSMC	= 0x00;			// 

#if RP_CPU_CLK == 32
    HOCODIV	= 0x00;			// 32MHz(C2H is 0xE8 : FRQSEL3=1)
#elif RP_CPU_CLK == 16
    HOCODIV	= 0x01;			// 16MHz(C2H is 0xE9 : FRQSEL3=1)
#elif RP_CPU_CLK == 8
    HOCODIV	= 0x02;			//  8MHz(C2H is 0xAA : FRQSEL3=1)
#elif RP_CPU_CLK == 4
    HOCODIV	= 0x03;			//  4MHz(C2H is 0x2B : FRQSEL3=1)
#endif

    CKC		= 0x00;			// 
    MCM0	= 0;			// OCO clock
  /*==================================*/
  /* Choose if segment initialization */
  /* should be done or not.           */
  /* Return: 0 to skip segment init   */
  /*         1 to do segment init     */
  /*==================================*/
  return ( 1 );
}

#ifdef __cplusplus
}
#endif
