/***********************************************************************/
/*                                                                     */
/*  FILE        :Main.c                                                */
/*  DATE        :                                                      */
/*  DESCRIPTION :Main Program                                          */
/*  CPU TYPE    :                                                      */
/*                                                                     */
/*  NOTE:THIS IS A TYPICAL EXAMPLE.                                    */
/*                                                                     */
/***********************************************************************/
/* Scheduler include files. */
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"

#include <ior5f11fll.h>
#include <ior5f11fll_ext.h>

//#include "iodefine.h"

/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/
/* Set option bytes */
#pragma location = "OPTBYTE"
__root const uint8_t opbyte0 = 0xEFU;
#pragma location = "OPTBYTE"
__root const uint8_t opbyte1 = 0xFFU;
#pragma location = "OPTBYTE"
__root const uint8_t opbyte2 = 0xE8U;
#pragma location = "OPTBYTE"
__root const uint8_t opbyte3 = 0x84U;

/* Set security ID */
#pragma location = "SECUID"
__root const uint8_t secuid[10] =
    {0x00U, 0x00U, 0x00U, 0x00U, 0x00U, 0x00U, 0x00U, 0x00U, 0x00U, 0x00U};

/* Prototypes for the standard FreeRTOS callback/hook functions implemented
within this file. */

void RdrvPeripheralInitialize(void);
void Processing_Before_Start_Kernel(void);
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*-----------------------------------------------------------*/
void main(void)
{
    /* Prepare the necessary tasks, FreeRTOS's resources... required to be executed at the beginning
       after vTaskStarScheduler() is called. Other tasks can also be created after starting scheduler at any time */
    Processing_Before_Start_Kernel();

    /* Call the kernel startup (should not return) */
    vTaskStartScheduler();

    /* Infinite loop is intended here. */
    while(1)
    {
        /* Infinite loop. Put a breakpoint here if you want to catch an exit of main(). */
    }
}

