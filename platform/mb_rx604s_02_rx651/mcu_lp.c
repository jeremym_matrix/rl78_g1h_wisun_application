/****************************************************************************** 
 * DISCLAIMER 
 *
 * This software is supplied by Renesas Electronics Corp. and is only 
 * intended for use with Renesas products. No other uses are authorized. 
 *
 * This software is owned by Renesas Electronics Corp. and is protected under 
 * all applicable laws, including copyright laws. 
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES 
 * REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, 
 * INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY 
 * DISCLAIMED. 
 *
 * TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS 
 * ELECTRONICS CORP. NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE 
 * FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES 
 * FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS 
 * AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. 
 *
 * Renesas reserves the right, without notice, to make changes to this 
 * software and to discontinue the availability of this software. 
 * By using this software, you agree to the additional terms and 
 * conditions found by accessing the following link: 
 * http://www.renesas.com/disclaimer 
 *****************************************************************************/ 

/* Copyright (C) 2014. Renesas Electronics Corp., All Rights Reserved. */

/****************************************************************************** 
* File Name : mcu_lp.c 
* Version : 1.00 
* Device(s) : RX651
* Tool-Chain : 
* OS : 
* H/W Platform : 
* Description : This is the MCU Power Control code. 
* Operation : 
* Limitations : None 
****************************************************************************** 
* History : DD.MM.YYYY Version Description 
*
******************************************************************************/ 

/****************************************************************************** 
	Includes
******************************************************************************/ 
#include	<mcu_lp.h>
#include    <platform/mb_rx604s_02_rx651/platform.h>
#include	<machine.h>

/****************************************************************************** 
Typedef definitions 
******************************************************************************/ 
#if defined(__IAR_SYSTEMS_ICC__)
#define _DI()		asm("	clrpsw i")
static void PUSH_PSW(unsigned long *a)
{
	asm("mvfc psw, r2");
	asm("mov.l r2, [r1]");
}
static void POP_PSW(unsigned long *a)
{
	asm("mov.l [r1], r2");
	asm("mvtc r2, psw");
}
#elif defined(__RX)
#pragma inline_asm _DI
static void _DI(void)
{
	clrpsw i
}
#pragma inline_asm PUSH_PSW
static void PUSH_PSW(unsigned long *a)
{
	mvfc psw, r2
	mov.l r2, [r1]
}
#pragma inline_asm POP_PSW
static void POP_PSW(unsigned long *a)
{
	mov.l [r1], r2
	mvtc r2, psw
}
#endif
#define MCULP_DI(a)		PUSH_PSW(&a);_DI()
#define MCULP_EI(a)		POP_PSW(&a)

/* MCU status (reserved) */
#define RP_MCU_SLEEP			0x0001
#define RP_MCU_SLEEPSTPCLK		0x0002
#define RP_MCU_STANBY			0x0004
#define RP_MCU_DEEP_STANBY		0x0008

#define RP_MCU_STOP_OCO			RP_MCU_DEEP_STANBY
#define RP_MCU_HALT_OCO			RP_MCU_STANBY
#define RP_MCU_SNOOZE_OCO		RP_MCU_SLEEP

/* MCU states */
#define RP_MCU_NORMAL			0x0000

/* return value */
#ifndef RP_SUCCESS
#define RP_SUCCESS				0x07
#endif
#ifndef RP_SAME_STATUS
#define RP_SAME_STATUS			0xf8
#endif

/****************************************************************************** 
Imported global variables and functions (from other files) 
******************************************************************************/ 

/****************************************************************************** 
Exported global variables and functions (to be accessed by other files) 
******************************************************************************/ 

/****************************************************************************** 
Private global variables and functions 
******************************************************************************/ 
static signed short 	_mcu_normal(void);
static signed short 	_mcu_low(unsigned short nxt_mcust);
static unsigned short 	_cur_mcust;

/****************************************************************************** 
Macro definitions 
******************************************************************************/ 

/******************************************************************************
Function Name:       RpMcuInitLowPower
Parameters:          none
Return value:        none
Description:         Initialized MCU Low Power Function's Value.
******************************************************************************/
void
RpMcuInitLowPower(void)
{
	_cur_mcust = RP_MCU_NORMAL;
}

/******************************************************************************
Function Name:       RpMcuLowPower
Parameters:          nxt_mcust:Setting MCU Low Power Mode
Return value:        RP_SUCCESS:Setting success
					 RP_SAME_STATUS:Same status(not setting)
Description:         MCU Low Power Function.
					 Function Type : Blocking
******************************************************************************/
signed short
RpMcuLowPower(unsigned short nxt_mcust)
{
	signed short rtn;
	unsigned long bkup_psw;

	MCULP_DI(bkup_psw);
	if (nxt_mcust == RP_MCU_NORMAL)
	{
		/* Set normal mode */
		rtn = _mcu_normal();
	}
	else
	{
		/* Set low power mode */
		rtn = _mcu_low(nxt_mcust);
	}
	MCULP_EI(bkup_psw);

	return (rtn);
}

/******************************************************************************
Function Name:       RpMcuGetLowPowerMode
Parameters:          none
Return value:        RP_MCU_NORMAL:
					 RP_MCU_STOP_MAIN8:
					 RP_MCU_WAIT_SUB:
					 RP_MCU_WAIT_OCO:
					 RP_MCU_WAIT_MAIN16:
Description:         Get MCU Low Power Mode Function.
******************************************************************************/
unsigned short
RpMcuGetLowPowerMode(void)
{
	return ((unsigned short)_cur_mcust);
}

/******************************************************************************
Function Name:       _mcu_normal
Parameters:          none
Return value:        RP_SUCCESS:Setting RP_MCU_NORMAL success
					 RP_SAME_STATUS:already RP_MCU_NORMAL(not setting)
Description:         MCU goes back to RP_MCU_NORMAL Function.
******************************************************************************/
static signed short
_mcu_normal(void)
{
	signed short rtn;

	if (_cur_mcust != RP_MCU_NORMAL)
	{
		if(_cur_mcust & (RP_MCU_STANBY | RP_MCU_DEEP_STANBY))
		{
			
		}
		_cur_mcust = RP_MCU_NORMAL;
		rtn = RP_SUCCESS;
	}
	else
	{
		rtn = RP_SAME_STATUS;
	}

	return(rtn);
}

/******************************************************************************
Function Name:       _mcu_low
Parameters:          none
Return value:        RP_SUCCESS:Setting Low Power Mode success(already go back RP_MCU_NORMAL)
					 RP_SAME_STATUS:Same status(not setting)
Description:         MCU goes to Low Power Function.
******************************************************************************/
static signed short
_mcu_low(unsigned short nxt_mcust)
{
	signed short rtn;

	if (nxt_mcust != _cur_mcust)
	{
		/* MCU Stand by Mode Action */
		_cur_mcust = nxt_mcust;
		SYSTEM.PRCR.BIT.PRC1 = 1;
		if(nxt_mcust & (RP_MCU_SLEEP | RP_MCU_SLEEPSTPCLK))
		{
			SYSTEM.SBYCR.BIT.SSBY = 0;
		}
		else
		{
			SYSTEM.SBYCR.BIT.SSBY = 1;
			if (nxt_mcust & RP_MCU_DEEP_STANBY)
			{								// Deep Stanby Mode?
				SYSTEM.DPSBYCR.BIT.DPSBY = 1;
				SYSTEM.DPSBYCR.BIT.DEEPCUT = 0;
				SYSTEM.DPSIER2.BIT.DRTCIIE = 1;
			}
			else if(nxt_mcust & RP_MCU_STANBY)
			{								// Stanby Mode?
				SYSTEM.DPSBYCR.BIT.DPSBY = 0;
			}
		}
		SYSTEM.PRCR.BIT.PRC1 = 0;
		wait();
		rtn = RP_SUCCESS;
	}
	else
	{
		rtn = RP_SAME_STATUS;
	}

	return(rtn);
}

/*
 *	Copyright (C) 2014 Renesas Electronics Corporation.
 *	and Renesas Solutions Corporation. All rights reserved.
 */

