/****************************************************************************** 
* DISCLAIMER 

* This software is supplied by Renesas Electronics Corporation and is only 
* intended for use with Renesas products. No other uses are authorized. 

* This software is owned by Renesas Electronics Corporation and is protected under 
* all applicable laws, including copyright laws. 

* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES 
* REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, 
* INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
* PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY 
* DISCLAIMED. 

* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS 
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE 
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES 
* FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS 
* AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. 

* Renesas reserves the right, without notice, to make changes to this 
* software and to discontinue the availability of this software. 
* By using this software, you agree to the additional terms and 
* conditions found by accessing the following link: 
* http://www.renesas.com/disclaimer 
******************************************************************************/ 

/* Copyright (C) 2014. Renesas Electronics Corporation, All Rights Reserved. */

/****************************************************************************** 
* File Name : mcu_lp.h 
* Version : 1.00 
* Device(s) : RX651 
* Tool-Chain :  
* OS : 
* H/W Platform : 
* Description : 
* Operation : 
* Limitations : None 
****************************************************************************** 
* History : DD.MM.YYYY Version Description 
* 
******************************************************************************/ 

#ifndef _MCU_LP_H
#define _MCU_LP_H

/*
 ****************************************************************************
 *		defines
 ****************************************************************************
 */

/* MCU status (reserved) */
#define RP_MCU_SLEEP			0x0001
#define RP_MCU_SLEEPSTPCLK		0x0002
#define RP_MCU_STANBY			0x0004
#define RP_MCU_DEEP_STANBY		0x0008

#define RP_MCU_STOP_OCO			RP_MCU_DEEP_STANBY
#define RP_MCU_HALT_OCO			RP_MCU_STANBY
#define RP_MCU_SNOOZE_OCO		RP_MCU_SLEEP

/* MCU states */
#define RP_MCU_NORMAL			0x0000

/* return value */
#ifndef RP_SUCCESS
#define RP_SUCCESS				0x07
#endif
#ifndef RP_SAME_STATUS
#define RP_SAME_STATUS			0xf8
#endif

/*
 ****************************************************************************
 *		prototypes
 ****************************************************************************
 */
void RpMcuInitLowPower(void);
signed short RpMcuLowPower(unsigned short nxt_mcust);
unsigned short RpMcuGetLowPowerMode(void);

#endif	/* _MCU_LP_H */

/*
 *	Copyright (C) 2014 Renesas Electronics Corporation.
 *	and Renesas Solutions Corporation. All rights reserved.
 */

