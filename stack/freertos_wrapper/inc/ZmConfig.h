/******************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corp. and is only
 * intended for use with Renesas products. No other uses are authorized.
 * This software is owned by Renesas Electronics Corp. and is protected under
 * all applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES
 * REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY,
 * INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY
 * DISCLAIMED.
 * TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
 * ELECTRONICS CORP. NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
 * FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES
 * FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS
 * AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this
 * software and to discontinue the availability of this software.
 * By using this software, you agree to the additional terms and
 * conditions found by accessing the following link:
 * http://www.renesas.com/disclaimer
 *****************************************************************************/

/******************************************************************************
 *  File Name       : ZmConfig.h
 *  Description     : Wi-SUN MAC protocol stack configuration macros.
 ******************************************************************************
 *  Copyright (C) 2014(2015) Renesas Electronics Corporation.
 *****************************************************************************/

#ifndef _RM_MAC_CONFIG_H_
#define _RM_MAC_CONFIG_H_


/******************************************************************************
 *  Macro definitions
 *****************************************************************************/

/******************************************************************************
 *  MAC Layer Configuration
 *****************************************************************************/

/******************************************************************************
 *  Configuration (IPv6)
 *****************************************************************************/
// mac config
#define RM_NUM_IND_TX          1    /* Number of indirect transmission queue */

/* RmConfigParams */
#define RM_MAX_PHY_PACKET_SIZE RP_aMaxPHYPacketSize
#define RM_CONTINUE_SCAN       1            /* Enable to continue active or passive scan. */
/* on the remaining channels, when "PANDescriptorList". */
/* reaches full during the scan. */

#define RM_RX_MPL_TMOUT                           10
#define RM_RCV_COORDREALIGNCMD_WITHOUT_ORPHANSCAN RM_FALSE
#define RM_RCV_DISASSOCNOTIFCMD                   RM_FALSE

// roa config

/** Number of elements for the ROA queue - must be enough for indications to upper layers and MAC-internal messages */
#define ROA_NUM_QBLK  (80 + RM_NUM_IND_TX)
#define ROA_NUM_EVENT 1  /* *.cfg flag[1] mac_flg = "1" flgs */

#endif /* _R_MAC_CONFIG_H_ */

/******************************************************************************
 *  Copyright (C) 2014(2015) Renesas Electronics Corporation.
 *****************************************************************************/
