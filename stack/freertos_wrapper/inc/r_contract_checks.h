#ifndef R_CONTRACT_CHECKS_H
#define R_CONTRACT_CHECKS_H

#if R_CONTRACT_CHECK_FULL
void r_contract_check_failed(const char* expr, const char* file, unsigned line, const char* function);
#define R_CONTRACT_CHECK_RET(expr, ret) \
    do { if (!(expr)) { r_contract_check_failed(#expr, __FILE__, __LINE__, __func__); return ret; } } while (0)
#elif R_CONTRACT_CHECK_SIMPLE
void r_contract_check_failed();
#define R_CONTRACT_CHECK_RET(expr, ret) do { if (!(expr)) { r_contract_check_failed(); return ret; } } while (0)
#else
#define R_CONTRACT_CHECK_RET(expr, ret) do { if (!(expr)) { return ret; } } while (0)
#endif

// allow each module to suppress contract checks individually
#if NO_EXTERNAL_CONTRACT_CHECK
#define CCHK_RET(expr, ret) do {} while (0)
#else
#define CCHK_RET(expr, ret) R_CONTRACT_CHECK_RET(expr, ret)
#endif

// internal contract checks (for static functions) are disabled by default
#if INTERNAL_CONTRACT_CHECK
#define ICCHK_RET(expr, ret) R_CONTRACT_CHECK_RET(expr, ret)
#else
#define ICCHK_RET(expr, ret) do {} while (0)
#endif

#define CCHK(expr)           CCHK_RET(expr, R_RESULT_INVALID_PARAMETER)
#define CCHKP(expr)          CCHK_RET(expr, NULL)
#define CCHKV(expr)          CCHK_RET(expr,  /* void */ )
#define CCHKB(expr)          CCHK_RET(expr, R_FALSE)
#define CCHK0(expr)          CCHK_RET(expr, 0)

#endif /* R_CONTRACT_CHECKS_H */
