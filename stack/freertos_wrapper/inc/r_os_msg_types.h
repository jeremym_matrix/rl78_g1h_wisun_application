/******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized.
* This software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES
* REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY,
* INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
* PARTICULAR PURPOSE AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY
* DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES
* FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS
* AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this
* software and to discontinue the availability of this software.
* By using this software, you agree to the additional terms and
* conditions found by accessing the following link:
* http://www.renesas.com/disclaimer
* Copyright (C) 2019 Renesas Electronics Corporation. All rights reserved.
******************************************************************************/

/**
 * @file      r_os_msg_types.h
 * @version   1.0
 * @brief     Base type for OS messages
 */

#ifndef R_OS_MSG_TYPES_H
#define R_OS_MSG_TYPES_H

#include "r_stdint.h"

typedef uint8_t r_os_id_t;  //!< OS ID type

#define R_OS_INVALID_ID ((r_os_id_t)0xFF)

typedef struct r_os_msg_header_s
{
    void*     kernel_msghead;
    r_os_id_t mem_id;
    r_os_id_t task_id;
    r_os_id_t param_id;
} r_os_msg_header_t;      //!< OS message header

typedef void* r_os_msg_t; //!< OS message


typedef enum
{
    R_OS_RESULT_SUCCESS,
    R_OS_RESULT_OUT_OF_MEMORY,
    R_OS_RESULT_FAIL,
    R_OS_RESULT_TIMEOUT,
} r_os_result_t;

#define R_OS_TIMEOUT_POLL    (0)
#define R_OS_TIMEOUT_FOREVER (0xFFFFFFFF)

#endif /* R_OS_MSG_TYPES_H */
