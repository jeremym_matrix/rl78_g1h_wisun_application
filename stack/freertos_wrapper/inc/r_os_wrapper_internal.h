/**
 * @file
 * For internal use only
 */

#ifndef OS_WRAPPER_INTERNAL_H
#define OS_WRAPPER_INTERNAL_H

#include "FreeRTOS.h"
#include "task.h"

#define MEM_ENTER_CRITICAL() taskENTER_CRITICAL()
#define MEM_EXIT_CRITICAL()  taskEXIT_CRITICAL()
void*  r_os_wrapper_internal_heap_addr(unsigned id);
size_t r_os_wrapper_internal_heap_size(unsigned id);

#endif /* OS_WRAPPER_INTERNAL_H */
