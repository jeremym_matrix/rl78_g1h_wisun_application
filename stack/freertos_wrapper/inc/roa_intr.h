/*******************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only
 * intended for use with Renesas products. No other uses are authorized.
 * This software is owned by Renesas Electronics Corporation and is protected under
 * all applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES
 * REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY,
 * INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY
 * DISCLAIMED.
 * TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
 * ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
 * FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES
 * FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS
 * AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this
 * software and to discontinue the availability of this software.
 * By using this software, you agree to the additional terms and
 * conditions found by accessing the following link:
 * http://www.renesas.com/disclaimer
 *******************************************************************************/
#ifndef __RM_roa_intr_H
#define __RM_roa_intr_H
/*******************************************************************************
 * file name    : roa_intr.h
 * description  : The header file of OS abstraction module for internal use.
 *******************************************************************************
 * Copyright (C) 2014-2016 Renesas Electronics Corporation.
 ******************************************************************************/

/* --- --- */
#if ROA_NEW_IF
    #define ROA_EVENT_ID_TO_OS_EVENT_ID(roaid)     \
    (roaid + ROA_OS_EVENT_ID_OFFSET - ROA_MIN_EVENT_ID)
    #define ROA_MPL_ID_TO_OS_MPL_ID(roaid)     \
    (roaid + ROA_OS_MPL_ID_OFFSET - ROA_MIN_MPL_ID)
    #define ROA_EVENT_ID_TO_TABLE_INDEX(roaid) (roaid - ROA_MIN_EVENT_ID)
#else
    #define ROA_EVENT_ID_TO_OS_EVENT_ID(osid)  (osid)
    #define ROA_MPL_ID_TO_OS_MPL_ID(osid)      (osid)
    #define ROA_EVENT_ID_TO_TABLE_INDEX(osid)  (osid - ROA_OS_EVENT_ID_OFFSET)
#endif


/******************************************************************************
 *  Copyright (C) 2014(2015) Renesas Electronics Corporation.
 *****************************************************************************/
#endif // #ifndef __RM_roa_intr_H
