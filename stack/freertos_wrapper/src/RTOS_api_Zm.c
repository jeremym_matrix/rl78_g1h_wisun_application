/******************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corp. and is only
 * intended for use with Renesas products. No other uses are authorized.
 * This software is owned by Renesas Electronics Corp. and is protected under
 * all applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES
 * REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY,
 * INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY
 * DISCLAIMED.
 * TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
 * ELECTRONICS CORP. NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
 * FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES
 * FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS
 * AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this
 * software and to discontinue the availability of this software.
 * By using this software, you agree to the additional terms and
 * conditions found by accessing the following link:
 * http://www.renesas.com/disclaimer
 *****************************************************************************/

/******************************************************************************
 * File Name   : RTOS_api_Zm.c
 * Description :
 ******************************************************************************
 * Copyright (C) 2014-2016 Renesas Electronics Corporation.
 *****************************************************************************/

/***********************************************************************
 * Check macro definition
 **********************************************************************/


/***********************************************************************
 * includes
 **********************************************************************/
#if defined(__RX)
#include <iodefine.h>
#endif /* #if defined(__RX) */

#include "mac_api.h"
#include "r_impl_utils.h"
#ifdef R_HYBRID_PLC_RF
#include "ZmConfig.h"
#include "mac_intr.h"
#else
#include "r_nwk.h"
#endif

#undef R_ASSERT_FAIL_RETURN_VALUE
#define R_ASSERT_FAIL_RETURN_VALUE /* void */

/***********************************************************************
 * extern
 **********************************************************************/
extern RmConfigParamsT RmConfigParams;
extern RmResourcesT RmResources;
extern RoaResourcesT RoaResources;

#ifdef R_HYBRID_PLC_RF

/* Allocate memory for RF MAC layer. */
static uint8_t g_memory[sizeof(RmMacCbT) + RM_NUM_IND_TX * sizeof(RmDataXferElemT) + R_MAC_NEIGHBOR_TABLE_SIZE * sizeof(r_mac_neighbor_t) + R_MAX_FRAME_SIZE];
#endif

/***********************************************************************
 *  Function Name  : rmUpMsgCallback_mac_task
 *  -------------------------------------------------------------------
 *  Parameters     : RmMsgT
 *  -------------------------------------------------------------------
 *  Return value   : None
 *  -------------------------------------------------------------------
 *  Description    : call back function (for IPv6)
 *                 :
 **********************************************************************/
static void rmUpMsgCallback_mac_tsk(void* pMsg)
{
#ifdef R_HYBRID_PLC_RF
    R_OS_SendMsg(pMsg, ID_nwk_mbx);
#else
    r_nwk_config_params_t* p_gNwkConfigParams = R_NWKDual_GetNwkConfigParams();
    R_ASSERT(p_gNwkConfigParams != NULL);

    R_OS_SendMsg(pMsg, p_gNwkConfigParams->p_nwkTableResources->p_sysConfigTBL->nwk_mbxId);
#endif
}


/***********************************************************************
 * Name         : mac_tsk
 * Parameters   : VP_INT
 * Returns      : nothing
 * Description  : mac task
 * Note         : !!DO NOT CHANGE!!
 **********************************************************************/
void mac_tsk(void* UNUSEDP(stacd))
{
    static const RmCallbaksT rmcbacks = {rmUpMsgCallback_mac_tsk};

    RmInitParamT data;
#ifdef R_HYBRID_PLC_RF
    RoaInit(&RoaResources);

    /* Initialize memory for RF MAC layer. */
    r_rfmac_memsetup_info_t p_mem_info;
    p_mem_info.alloc_byte_size = sizeof(g_memory);
    p_mem_info.p_start = (void*)g_memory;
    p_mem_info.size_info.posTableSize = R_MAC_NEIGHBOR_TABLE_SIZE;
    p_mem_info.size_info.indTxQueueSize = RM_NUM_IND_TX;

    void* p_end;

    if (rmInitMemory(p_mem_info, &p_end) != RM_MAC_SUCCESS)
    {
        return;
    }

    /* --- --- */
    data.pCbacks = &rmcbacks;
    data.pResources = &RmResources;
    data.pConfig = &RmConfigParams;

    /* --- --- */
    data.flgId = ID_mac_flg;
    data.rxMplId = R_HEAP_ID_MAC_IND;
    data.warningMplId = R_HEAP_ID_WARN_IND;
    data.fatalMplId = R_HEAP_ID_ERR_IND;
    data.semid = ID_rfdrv_sem;
#else  /* ifdef R_HYBRID_PLC_RF */
    r_nwk_config_params_t* p_gNwkConfigParams = NULL;
    const r_sys_config_t* p_gSysConfig;

    RoaInit(&RoaResources);

    p_gNwkConfigParams =  R_NWKDual_GetNwkConfigParams();
    R_ASSERT(p_gNwkConfigParams != NULL);
    p_gSysConfig = p_gNwkConfigParams->p_nwkTableResources->p_sysConfigTBL;

    /* --- --- */
    data.pCbacks = &rmcbacks;
    data.pResources = &RmResources;
    data.pConfig = &RmConfigParams;

    /* --- --- */
    data.flgId = p_gSysConfig->mac_flgId;
    data.rxMplId = p_gSysConfig->mac_rx_mplId;
    data.warningMplId = p_gSysConfig->mac_warning_ind_mplId;
    data.fatalMplId = p_gSysConfig->mac_fatal_ind_mplId;
    data.semid = p_gSysConfig->mac_rfdrv_semId;
#endif /* ifdef R_HYBRID_PLC_RF */

    #if defined(__RX__)
    vrst_mpf(data.rxMplId);
    #endif

    RmMain(&data);
}

/******************************************************************************
 * Copyright (C) 2014-2016 Renesas Electronics Corporation.
 *****************************************************************************/
