/******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
*
* Copyright (C) 2018 Renesas Electronics Corporation. All rights reserved.
******************************************************************************/

/*!
   \file      r_os_wrapper.c
   \version   1.00
   \brief     Wrapper for FreeRTOS API
 */

// Note: all header files that #include r_os_wrapper_config.h must be included later
#include "FreeRTOS.h"
#include "queue.h"
#include "event_groups.h"
#include "semphr.h"
#include "task.h"
#include "r_impl_utils.h"


/*
 * We need 32 bits ticks because
 * "an event group is 8 if configUSE_16_BIT_TICKS is set to 1, or 24 if configUSE_16_BIT_TICKS is set to 0"
 * [https://www.freertos.org/xEventGroupCreate.html] and roa expects 16 bits for event groups.
 * Additionally, we use 32 bits for timeouts
 */
#if configUSE_16_BIT_TICKS != 0
#error configUSE_16_BIT_TICKS!=0 not supported
#endif

/******************************************************************************
* Support for the r_os_wrapper_id_config file
******************************************************************************/
#define SIZE_TO_NSTACKTYPES(size) ELEMENTS(size, StackType_t)

struct r_os_msg_header_s;

// first round: allocate dynamically sized buffers
// align stacks on StackType_t, heaps on uint32_t
#include "r_os_wrapper_def.h"
#undef R_OS_TASK_LIST_ENTRY
#define R_OS_TASK_LIST_ENTRY(id, name, func, stack, priority, suspended) \
    static StackType_t task_stack_storage_##id[SIZE_TO_NSTACKTYPES(stack)];

// "All memory used by the manager is allocated at link time, it is aligned on a 32 bit boundary"
// "Each memory block holds 8 bytes, and there are up to 32767 blocks available"
#undef R_OS_HEAP_LIST_ENTRY
#define R_OS_HEAP_LIST_ENTRY(id, size) \
    static uint32_t heap_storage_##id[ELEMENTS(size, uint32_t)]; \
    STATIC_ASSERT(size < (uint32_t)8 * 32767, Heap_size_too_big_for_##id);

// include the configuration file to make the declarations
#include "r_os_wrapper_config.h"

// some static checks
#ifndef R_HEAP_COUNT
#error R_HEAP_COUNT must be set (#define) to the total number of heaps
#endif
#if R_OS_HEAP_BASEID == 0
#error R_OS_HEAP_BASEID must be greater than 0
#endif



// clean #include guard and macros for second round
#undef R_OS_WRAPPER_CONFIG_H
#undef R_OS_MAILBOX_LIST_BEGIN
#undef R_OS_MAILBOX_LIST_ENTRY
#undef R_OS_MAILBOX_LIST_END
#undef R_OS_FLAG_LIST_BEGIN
#undef R_OS_FLAG_LIST_ENTRY
#undef R_OS_FLAG_LIST_END
#undef R_OS_SEMAPHORE_LIST_BEGIN
#undef R_OS_SEMAPHORE_LIST_ENTRY
#undef R_OS_SEMAPHORE_LIST_END
#undef R_OS_TIMER_LIST_BEGIN
#undef R_OS_TIMER_LIST_ENTRY
#undef R_OS_TIMER_LIST_END
#undef R_OS_TASK_LIST_BEGIN
#undef R_OS_TASK_LIST_ENTRY
#undef R_OS_TASK_LIST_END
#undef R_OS_HEAP_LIST_BEGIN
#undef R_OS_HEAP_LIST_ENTRY
#undef R_OS_HEAP_LIST_END


// second round

typedef struct
{
    size_t    capacity;
    uint32_t* address;
} heap_def_t;

#define R_OS_HEAP_LIST_BEGIN static heap_def_t heaps[] = {
#define R_OS_HEAP_LIST_ENTRY(id, size) {size, heap_storage_##id},
#define R_OS_HEAP_LIST_END   };

typedef struct
{
    struct r_os_msg_header_s* head;
    struct r_os_msg_header_s* tail;
    SemaphoreHandle_t         sem_count;
    StaticSemaphore_t         semaphore_buffer_count;
} mailbox_t;

#define R_OS_MAILBOX_LIST_BEGIN static mailbox_t mailboxes[] = {
#define R_OS_MAILBOX_LIST_ENTRY(id) {NULL},
#define R_OS_MAILBOX_LIST_END   };


#define R_OS_FLAG_LIST_BEGIN    static EventGroupHandle_t events[] = {
#define R_OS_FLAG_LIST_ENTRY(id) NULL,
#define R_OS_FLAG_LIST_END      }; \
    static StaticEventGroup_t eventBuffers[ARRAY_SIZE(events)];


typedef struct
{
    uint8_t max;
    uint8_t initial;
} semaphore_def_t;

#define R_OS_SEMAPHORE_LIST_BEGIN static const semaphore_def_t semaphore_defs[] = {
#define R_OS_SEMAPHORE_LIST_ENTRY(id, max, initial) {max, initial},
#define R_OS_SEMAPHORE_LIST_END   }; \
    static SemaphoreHandle_t semaphores[ARRAY_SIZE(semaphore_defs)]; \
    static StaticSemaphore_t semaphore_buffers[ARRAY_SIZE(semaphore_defs)];


typedef struct
{
    TimerCallbackFunction_t func;
    uint32_t                period_millis;
    uint8_t                 oneshot;
} timer_def_t;

#define R_OS_TIMER_LIST_BEGIN static const timer_def_t timer_defs[] = {
#define R_OS_TIMER_LIST_ENTRY(id, func, period, oneshot) {func, period, oneshot},
#define R_OS_TIMER_LIST_END   }; \
    static TimerHandle_t timers[ARRAY_SIZE(timer_defs)]; \
    static StaticTimer_t timer_buffers[ARRAY_SIZE(timer_defs)];


// Implementation note: uItron slp_tsk/wup_tsk is counting. The FreeRTOS counterpart is not -> race condition
// Instead of suspend/resume, use wait/post a dedicated semaphore
typedef struct
{
    char* const  name;
    void (* func)(void*);
    StackType_t* stack_storage;
    uint16_t     stack;
    uint8_t      priority;
    uint8_t      suspended;
} task_def_t;

typedef struct
{
    TaskHandle_t      task;
    SemaphoreHandle_t sleep;
    StaticSemaphore_t semaphore_buffer_sleep;
} task_info_t;

// FreeRTOS expects the stack depth in terms of StackType_t
#define R_OS_TASK_LIST_BEGIN static const task_def_t task_defs[] = {
#define R_OS_TASK_LIST_ENTRY(id, name, func, stack, priority, suspended) \
    {name, func, task_stack_storage_##id, SIZE_TO_NSTACKTYPES(stack), priority, suspended},
#define R_OS_TASK_LIST_END   }; \
    static task_info_t tasks[ARRAY_SIZE(task_defs)]; \
    static StaticTask_t task_buffers[ARRAY_SIZE(task_defs)];


// Use our implementation macros instead of the 'prototypes' in R_OS_WRAPPER_DEF_H
#define R_OS_WRAPPER_DEF_H
#include "r_os_wrapper.h"

/******************************************************************************
* Stack includes
******************************************************************************/

#include "r_heap.h"
#include "r_os_wrapper_internal.h"
#include "r_contract_checks.h"


r_os_msg_header_t* R_OS_MsgAlloc(r_os_id_t memId, size_t size)
{
    CCHKP(BETWEEN(memId, R_OS_HEAP_BASEID, R_OS_HEAP_BASEID + ARRAY_SIZE(heaps)));
    r_os_msg_header_t* msg = r_alloc(memId, size);
    if (msg)
    {
        msg->mem_id = memId;
    }
    return msg;
}

void R_OS_MsgFree(r_os_msg_header_t* msg)
{
    if (!msg || msg->mem_id == 0 || msg->mem_id == R_OS_INVALID_ID)
    {
        return;  // NULL or not from us
    }

    CCHKV(BETWEEN(msg->mem_id, R_OS_HEAP_BASEID, R_OS_HEAP_BASEID + ARRAY_SIZE(heaps)));
    r_free(msg->mem_id, msg);
}

void R_OS_MsgMarkIgnoreFree(r_os_msg_header_t* msg)
{
    if (msg)
    {
        msg->mem_id = 0;  // use zero, so statically allocated messages just work
    }
}


/******************************************************************************
* Heap
******************************************************************************/

STATIC_ASSERT(R_HEAP_COUNT == ARRAY_SIZE(heaps), Number_of_heaps_must_equal_R_HEAP_COUNT);

// functions for r_heap (id is zero based!)
void* r_os_wrapper_internal_heap_addr(unsigned id)
{
    CCHKP(id < R_HEAP_COUNT);
    return heaps[id].address;
}

size_t r_os_wrapper_internal_heap_size(unsigned id)
{
    CCHK0(id < R_HEAP_COUNT);
    return heaps[id].capacity;
}


/******************************************************************************
* Mailboxes
******************************************************************************/

/* FreeRTOS Queues are external: they require specifying their maximum length
 * but do not impose requirements on the queued items. It is not possible
 * to specify the mailbox length for uItron and they seemingly use the
 * 'kernel_msg_head' for an internal list implementation which requires all
 * mailbox items to start with this special header. Therefore, the send_msg
 * function of uItron cannot fail and the code does not check success. Since
 * the code assumes the uItron style we provide a mailbox implementation with
 * the same interface and requirements.
 *
 * The semaphore holds the number of messages and allows waiting on new entries.
 * Head and tail are used to implement O(1) enqueue and dequeue primitives. The
 * messages form a linked list based on r_os_msg_header_t#kernel_msghead
 */

r_os_msg_header_t* R_OS_ReceiveMsg(r_os_id_t mbxId, uint32_t timeOutMs)
{
    BaseType_t res = xSemaphoreTake(mailboxes[mbxId].sem_count, pdMS_TO_TICKS(timeOutMs));
    if (res != pdTRUE)
    {
        return NULL;
    }
    taskENTER_CRITICAL();
    r_os_msg_header_t* msg = mailboxes[mbxId].head;
    mailboxes[mbxId].head = msg->kernel_msghead;
    if (mailboxes[mbxId].head == NULL)
    {
        mailboxes[mbxId].tail = NULL;
    }
    taskEXIT_CRITICAL();
    msg->kernel_msghead = NULL;  // not necessary (for debugging)
    return msg;
}

void R_OS_SendMsg(r_os_msg_header_t* msg, r_os_id_t mbxId)
{
    if (msg == NULL)
    {
        return;
    }
    msg->kernel_msghead = NULL;
    taskENTER_CRITICAL();
    if (mailboxes[mbxId].tail)
    {
        mailboxes[mbxId].tail->kernel_msghead = msg;
    }
    mailboxes[mbxId].tail = msg;
    if (mailboxes[mbxId].head == NULL)
    {
        mailboxes[mbxId].head = msg;
    }
    taskEXIT_CRITICAL();
    xSemaphoreGive(mailboxes[mbxId].sem_count);
}

void R_OS_SendMsgFromISR(r_os_msg_header_t* msg, r_os_id_t mbxId)
{
    if (msg == NULL)
    {
        return;
    }
    msg->kernel_msghead = NULL;
    if (mailboxes[mbxId].tail)
    {
        mailboxes[mbxId].tail->kernel_msghead = msg;
    }
    mailboxes[mbxId].tail = msg;
    if (mailboxes[mbxId].head == NULL)
    {
        mailboxes[mbxId].head = msg;
    }
    xSemaphoreGiveFromISR(mailboxes[mbxId].sem_count, NULL);
}


/******************************************************************************
* Task Control
******************************************************************************/

void R_OS_DelayTaskMs(uint32_t delayMs)
{
    vTaskDelay(pdMS_TO_TICKS(delayMs));
}

r_os_id_t R_OS_GetTaskId(void)
{
    TaskHandle_t current = xTaskGetCurrentTaskHandle();
    for (r_os_id_t i = 0; i < ARRAY_SIZE(tasks); i++)
    {
        if (tasks[i].task == current)
        {
            return i;
        }
    }
    // Note: this is also called from timer handler, which has no task id from us
    return R_OS_INVALID_ID;
}

void R_OS_StartTask(r_os_id_t tskId)
{
    vTaskResume(tasks[tskId].task);
}

r_os_result_t R_OS_Sleep(void)
{
    xSemaphoreTake(tasks[R_OS_GetTaskId()].sleep, portMAX_DELAY);
    return R_OS_RESULT_SUCCESS;
}

r_os_result_t R_OS_SleepMaxTicks(unsigned ticks)
{
    if (xSemaphoreTake(tasks[R_OS_GetTaskId()].sleep, ticks) == pdTRUE)
    {
        return R_OS_RESULT_SUCCESS;
    }
    return R_OS_RESULT_FAIL;
}

r_os_result_t R_OS_WakeupTask(r_os_id_t tskId)
{
    xSemaphoreGive(tasks[tskId].sleep);
    return R_OS_RESULT_SUCCESS;
}

void* R_OS_GetTaskHandle(r_os_id_t tskId)
{
    return tskId < ARRAY_SIZE(tasks) ? tasks[tskId].task : NULL;
}

void R_OS_ChangeOwnTaskPriority(uint16_t priority)
{
    vTaskPrioritySet(NULL, priority);
}

/******************************************************************************
* Timers
******************************************************************************/

void R_OS_StartCyclic(r_os_id_t cycId)
{
    xTimerStart(timers[cycId], 0);
}

void R_OS_StopCyclic(r_os_id_t cycId)
{
    xTimerStop(timers[cycId], 0);
}


/******************************************************************************
* Event Flags
******************************************************************************/

void R_OS_SetFlag(r_os_id_t id, uint16_t bits)
{
    xEventGroupSetBits(events[id], bits);
}

void R_OS_SetFlagFromISR(r_os_id_t id, uint16_t bits)
{
    /* xHigherPriorityTaskWoken must be initialised to pdFALSE. */
    BaseType_t xHigherPriorityTaskWoken = pdFALSE;
    xEventGroupSetBitsFromISR(events[id], bits, &xHigherPriorityTaskWoken);
}

r_os_result_t R_OS_WaitForFlag(r_os_id_t id, uint16_t bits, int clearOnExit, int waitForAll, uint32_t timeOutMs, uint16_t* resultBits)
{
    EventBits_t res = xEventGroupWaitBits(events[id], bits, clearOnExit ? pdTRUE : pdFALSE, waitForAll ? pdTRUE : pdFALSE, pdMS_TO_TICKS(timeOutMs));
    *resultBits = (uint16_t)res;
    if (waitForAll)
    {
        return (res & bits) == bits ? R_OS_RESULT_SUCCESS : R_OS_RESULT_TIMEOUT;
    }
    return (res & bits) ? R_OS_RESULT_SUCCESS : R_OS_RESULT_TIMEOUT;
}


/******************************************************************************
* Semaphores
******************************************************************************/

void R_OS_PostSemaphore(r_os_id_t id)
{
    xSemaphoreGive(semaphores[id]);
}
void R_OS_WaitSemaphore(r_os_id_t id)
{
    xSemaphoreTake(semaphores[id], portMAX_DELAY);
}


/******************************************************************************
* Initialization
******************************************************************************/

r_os_result_t R_OS_Init()
{
    // mailboxes
    for (size_t i = 0; i < ARRAY_SIZE(mailboxes); i++)
    {
#if !defined R_HYBRID_PLC_RF || (defined R_HYBRID_PLC_RF & !defined R_MODEM_CLIENT)
        if (i != ID_mac_mbx)  // MAC mailbox uses ROA implementation
#endif
        {
            mailboxes[i].sem_count = xSemaphoreCreateCountingStatic(0xfff0, 0, &mailboxes[i].semaphore_buffer_count);
            if (mailboxes[i].sem_count == NULL)
            {
                return R_OS_RESULT_FAIL;
            }
        }
    }

    // events
    // The number of events is configured to be 1 for WiSUN
    // PRQA S 2877 2
    for (size_t i = 0; i < ARRAY_SIZE(events); i++)
    {
        events[i] = xEventGroupCreateStatic(&eventBuffers[i]);
        if (events[i] == NULL)
        {
            return R_OS_RESULT_FAIL;
        }
    }

    // semaphores
    // The number of semaphores is configured to be 1 for WiSUN
    // PRQA S 2877 2
    for (size_t i = 0; i < ARRAY_SIZE(semaphore_defs); i++)
    {
        semaphores[i] = xSemaphoreCreateCountingStatic(semaphore_defs[i].max, semaphore_defs[i].initial, &semaphore_buffers[i]);
        if (semaphores[i] == NULL)
        {
            return R_OS_RESULT_FAIL;
        }
    }

    // timers
    // The number of timers is configured to be 1 for WiSUN
    // PRQA S 2877 2
    for (size_t i = 0; i < ARRAY_SIZE(timer_defs); i++)
    {
        timers[i] = xTimerCreateStatic("",
                                       pdMS_TO_TICKS(timer_defs[i].period_millis),
                                       timer_defs[i].oneshot ? pdFALSE : pdTRUE,
                                       (void*)i,
                                       timer_defs[i].func,
                                       &timer_buffers[i]);
        if (timers[i] == NULL)
        {
            return R_OS_RESULT_FAIL;
        }
    }

    // tasks
    for (size_t i = 0; i < ARRAY_SIZE(task_defs); i++)
    {
        tasks[i].task = xTaskCreateStatic(task_defs[i].func,
                                          task_defs[i].name,
                                          task_defs[i].stack,
                                          NULL,
                                          task_defs[i].priority,
                                          task_defs[i].stack_storage,
                                          &task_buffers[i]);
        if (tasks[i].task == NULL)
        {
            return R_OS_RESULT_FAIL;
        }
        tasks[i].sleep = xSemaphoreCreateCountingStatic(10, 0, &tasks[i].semaphore_buffer_sleep);
        if (tasks[i].sleep == NULL)
        {
            return R_OS_RESULT_FAIL;
        }
        if (task_defs[i].suspended)
        {
            vTaskSuspend(tasks[i].task);  // will be started with R_OS_StartTask, not with R_OS_WakeupTask
        }
    }
    return R_OS_RESULT_SUCCESS;
}

// from https://www.freertos.org/a00110.html (description of configSUPPORT_STATIC_ALLOCATION)
void vApplicationGetIdleTaskMemory(StaticTask_t** ppxIdleTaskTCBBuffer,
                                   StackType_t**  ppxIdleTaskStackBuffer,
                                   uint32_t*      pulIdleTaskStackSize)
{
    static StaticTask_t xIdleTaskTCB;
    static StackType_t uxIdleTaskStack[configMINIMAL_STACK_SIZE];
    *ppxIdleTaskTCBBuffer = &xIdleTaskTCB;
    *ppxIdleTaskStackBuffer = uxIdleTaskStack;
    *pulIdleTaskStackSize = configMINIMAL_STACK_SIZE;
}

void vApplicationGetTimerTaskMemory(StaticTask_t** ppxTimerTaskTCBBuffer,
                                    StackType_t**  ppxTimerTaskStackBuffer,
                                    uint32_t*      pulTimerTaskStackSize)
{
    static StaticTask_t xTimerTaskTCB;
    static StackType_t uxTimerTaskStack[configTIMER_TASK_STACK_DEPTH];
    *ppxTimerTaskTCBBuffer = &xTimerTaskTCB;
    *ppxTimerTaskStackBuffer = uxTimerTaskStack;
    *pulTimerTaskStackSize = configTIMER_TASK_STACK_DEPTH;
}
