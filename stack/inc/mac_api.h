/*******************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only
 * intended for use with Renesas products. No other uses are authorized.
 * This software is owned by Renesas Electronics Corporation and is protected under
 * all applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES
 * REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY,
 * INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY
 * DISCLAIMED.
 * TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
 * ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
 * FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES
 * FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS
 * AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this
 * software and to discontinue the availability of this software.
 * By using this software, you agree to the additional terms and
 * conditions found by accessing the following link:
 * http://www.renesas.com/disclaimer
 *******************************************************************************/
#ifndef __RM_mac_api_H
#define __RM_mac_api_H
/*******************************************************************************
 * file name    : mac_api.h
 * description  : The header file of MAC sub layer APIs.
 *******************************************************************************
 * Copyright (C) 2014-2016 Renesas Electronics Corporation.
 ******************************************************************************/

/* --- --- */
#include "r_stdint.h"

#include    "mac_types.h"
#include    "mac_msg.h"
#include    "mac_mlme.h"
#include    "mac_mcps.h"
#include    "roa.h"

/******************************************************************************
    Type Definitions
******************************************************************************/

R_HEADER_UTILS_PRAGMA_PACK_1

typedef void (* RmUpMsgCallbackT)(void* pMsg);

typedef struct
{
    RmUpMsgCallbackT upMsg;
} RmCallbaksT;

typedef void (* RmInterMsgHandlerT)(void* pmac, void* pMsg);
typedef void (* RmMcpsHookFuncT)(void* pMac, RmMacEnumT status, void* pArg);

typedef struct
{
    RmOctetT*       pdata;            /* MAC Frame Data            */
    RmUBYTE*        pSecData;         /* MAC Secuired Frame Data */
    RmUINT16        secDataLength;    /* length of MAC sec Frame */
    RmSecParamT     secParam;         /* Security Parameter        */
    RmTimeT         time;             /* send time                 */
#ifdef R_HYBRID_PLC_RF
    RmShortT        handle;           /* msdu handle           */
#endif /* R_HYBRID_PLC_RF */
    RmShortT        dataLength;       /* length of MAC Frame   */
    RmBitmap8T      txOptions;        /* transmitte option         */
    RmShortTimeT    persistTime;      /* persist time          */
    RmOctetT        NFR;              /* number of frame sent  */
    RmMcpsHookFuncT hookFunc;         /* the function to call
                                                when transmission is completed */
    RmBooleanT      isPendInAck;      /* the pending bit is 1 in
                                                recevied ack */
    RmOctetT*       rcvData;          /* for enhanced ack tx */
    RmShortT        rcvLength;        /* for enhanced ack tx */
    RmTimeT         rcvTime;          /* for enhanced ack tx */
    RmOctetT        rcvLinkQuality;   /* for enhanced ack tx */
    RmBooleanT      rcvIsPendInAck;   /* for enhanced ack tx */
    RmPhyEnumT      bkupRxStatus;     /* for enhanced ack tx */
    RmTimeT         bkupNextTimeout;  /* for enhanced ack tx */
    RmOctetT        DSN;              /* for data confirm DSN */

    RmOctetT        numBackoffs;      /* for data confirm backoffnum */
    RmOctetT        mm_numBackoffs;   /* for data confirm backoffnum */

    RmOctetT        noSQNorNoSA;      /* for data confirm whether No SQN or SA */
    RmShortT        ackPayloadLength; /* for data confirm Ack Payload */
    RmOctetT        frmVersion;       /* for retry transmit */
} RmDataXferElemT;

typedef struct
{
    RmOctetT         numTxPend; /* number of TX Pendings                     */
    RmDataXferElemT* pTxPends;  /* pointer to TX Pending management elements */

    /********************************************/
    /*  mac.c                                   */
    /********************************************/
    RmUBYTE* p_MacCB;
} RmResourcesT;


typedef struct
{
    RmBooleanT   isContScan;                            /* Enable/Disable to continue active or passive scan
                                                            on the remaining channels, when "PANDescriptorList"
                                                            reaches full during the scan.
                                                            0: disable  other:enable */
    RmShortTimeT rxMplTmout;                            /* msec, Wait time to get Rx buffer for a notification */
    RmBooleanT   isRcvCoordRealignCmdWithoutOrphanScan; /* enable to receive CoordRealignment command whithout orphan scan request */
                                                        /* 0:disable, 1:enable */
    RmBooleanT   isRcvDisassocNotifCmd;                 /* enable to receive DisassociateNotification command */
                                                        /* 0:disable, 1:enable */
    /********************************************/
    RmShortT     MaxPHYPacketSize;

    /********************************************/
}  RmConfigParamsT;

typedef struct
{
    const RmCallbaksT* pCbacks;
    RmResourcesT*      pResources;
    RmConfigParamsT*   pConfig;
    RoaID              flgId;
    RoaID              rxMplId;
    RoaID              warningMplId;
    RoaID              fatalMplId;
    RoaID              semid;
} RmInitParamT;

#ifdef R_HYBRID_PLC_RF
typedef struct
{
    uint16_t posTableSize;   /* The number of POS table for RFMAC. */
    uint16_t indTxQueueSize; /* Size of the indirect transmission queue. */
} r_rfmac_memsetup_size_info_t;

typedef struct
{
    r_rfmac_memsetup_size_info_t size_info;
    void* p_start;            /* Start address of allocated memory for RF MAC */
    uint32_t alloc_byte_size; /* Byte size of allocated memory for RF MAC */
} r_rfmac_memsetup_info_t;
#endif

R_HEADER_UTILS_PRAGMA_PACK_DEFAULT

void RmMain(RmInitParamT* pInitParam);

RmMacEnumT RmMcpsDataRequest(void* pmac, void* pMsg);
RmMacEnumT RmMlmeGetRequest(void* pmac, void* pMsg);
RmMacEnumT RmMlmeSetRequest(void* pmac, void* pMsg);
RmMacEnumT RmMlmeResetRequest(void* pmac, void* pMsg);
#ifdef R_HYBRID_PLC_RF
RmMACEnumT rmInitMemory(const r_rfmac_memsetup_info_t p_mem_info, void** pp_end);
RmMACEnumT rmDeInitMemory(void);
RmMacEnumT RmMlmeScanRequest(void* pmac, void* pMsg);
RmMacEnumT RmMlmeStartRequest(void* pmac, void* pMsg);
RmMacEnumT RmMlmeBeaconRequest(void* pmac, void* pMsg);
#endif /* R_HYBRID_PLC_RF */

#ifdef WIN32
void RmGetTrace(unsigned char* pdata, unsigned char FAR* typ);
#else   /* #ifdef WIN32 */
void RmGetTrace(unsigned char* pdata, unsigned char __far* typ);
#endif  /* #ifdef WIN32 */

/******************************************************************************
 *  Copyright (C) 2014-2016 Renesas Electronics Corporation.
 *****************************************************************************/
#endif // #ifndef __RM_mac_api_H
