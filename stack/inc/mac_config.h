/*******************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only
 * intended for use with Renesas products. No other uses are authorized.
 * This software is owned by Renesas Electronics Corporation and is protected under
 * all applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES
 * REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY,
 * INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY
 * DISCLAIMED.
 * TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
 * ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
 * FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES
 * FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS
 * AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this
 * software and to discontinue the availability of this software.
 * By using this software, you agree to the additional terms and
 * conditions found by accessing the following link:
 * http://www.renesas.com/disclaimer
 *******************************************************************************/
#ifndef __RM_mac_config_H
#define __RM_mac_config_H
/*******************************************************************************
 * file name    : mac_config.h
 * description  : The configuration file of IEEE802.15.4 MAC Sub Layer.
 *******************************************************************************
 * Copyright (C) 2014-2016 Renesas Electronics Corporation.
 ******************************************************************************/

/* --- --- */
#if defined(__RL78__)
    #define FAR far
#else
    #ifndef FAR
        #define FAR
    #endif
#endif

/* --- --- */
/* The maximum waiting time for the data transfer. (msec) */
#define RM_TX_TIMEOUT                           (3000U)       /* 3 sec   */

/*The warm up time of TX (slot) . This depends on the RF-IC. */
#define RM_TX_WARMUP_TIME                       (12UL)

/*The warm up time of RX (slot) . This depends on the RF-IC. */
#define RM_RX_WARMUP_TIME                       (12UL)

/* After transmission/recive is started, an  offset time for the
   delay time until a start is actually introduced to the LSI */
#define RM_TRX_DELAY_TIME                       (10UL)

/* Additional time for frame Total wait time */
#define RM_ADDITIONAL_TIME_FOR_FRAME_TOTAL_WAIT (0UL)

/* Symbols Per Octet */
#define RM_MACPIB_SYMBOLSPEROCTET               (16UL) /* Temporary */

/* Margin of Task performance */
#define RM_MARGIN_TASK_PERFORMANCE              (400UL) /* Temporary */

/* Frame Version set value */
#define RM_MAC2003_FRAME_VERSION                (0x00)
#define RM_MAC2011_FRAME_VERSION                (0x01)
#define RM_MAC2015_FRAME_VERSION                (0x02)

/* Sequence Number Suppression set value */
#define RM_MAC_SEQ_NUM_SUPPRESSED               (RM_FALSE)

#define RM_SPECIFIC_NORMAL                      (0x00)
#define RM_SPECIFIC_WSUN                        (0x01)

#define RM_SEC_DEFAULT_EXTADDRESS_H             (0xFFFFFFFFUL)
#define RM_SEC_DEFAULT_EXTADDRESS_L             (0xFFFFFFFFUL)

#ifdef R_HYBRID_PLC_RF
#define RM_DEFAULT_DUPLICATE_DETECTION_TTL      (3u)
#define RM_SOFT_VERSION_MAJOR                   (0u)
#define RM_SOFT_VERSION_MINOR                   (3u)
#define RM_SOFT_VERSION_PATCH                   (0u)
#define RM_PHY_VERSION_LENGTH                   (10u)
#else
#define RM_DEFAULT_DUPLICATE_DETECTION_TTL      (5u)
#endif

/* For White List table definition */
#define R_WHITELIST_TABLE_LENGTH (16u)  //!< Size for White List table

/******************************************************************************
 *  Copyright (C) 2014-2016 Renesas Electronics Corporation.
 *****************************************************************************/
#endif // #ifndef __RM_mac_config_H
