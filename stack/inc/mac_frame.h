/******************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corp. and is only
 * intended for use with Renesas products. No other uses are authorized.
 * This software is owned by Renesas Electronics Corp. and is protected under
 * all applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES
 * REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY,
 * INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY
 * DISCLAIMED.
 * TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
 * ELECTRONICS CORP. NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
 * FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES
 * FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS
 * AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this
 * software and to discontinue the availability of this software.
 * By using this software, you agree to the additional terms and
 * conditions found by accessing the following link:
 * http://www.renesas.com/disclaimer
 *****************************************************************************/

#ifndef __RM_mac_frame_H
#define __RM_mac_frame_H
/******************************************************************************
 *  File Name       : mac_frame.h
 *  Description     : The definition for MAC DATA/COMMAND/ACK/BEACON frame
 ******************************************************************************
 *  Copyright (C) 2010-2016 Renesas Electronics Corporation.
 *****************************************************************************/

/* --- --- */
#include "r_stdint.h"
#ifdef R_HYBRID_PLC_RF
#include "r_config.h"
#include "r_typedefs.h"
#endif /* R_HYBRID_PLC_RF */

/* --- --- */
uint16_t RmFrmGet16Bit(uint8_t* pdata);
void     RmFrmSet16Bit(uint8_t* pdata, uint16_t hostData);
uint32_t RmFrmGet32Bit(uint8_t* pdata);
void     RmFrmSet32Bit(uint8_t* pdata, uint32_t hostData);
uint32_t RmFrmGet64Bit(uint8_t* pdata);
void     RmFrmSet64Bit(uint8_t* pdata, uint32_t hostData);

/* NOTE: The 802.15.4 frame uses a little-endian */
#ifdef R_HYBRID_PLC_RF
#define htons(_x) RmHtoLeS(_x)
#define htonl(_x) RmHtoLeL(_x)

#define ntohs(_x) htons(_x)
#define ntohl(_x) htonl(_x)
#endif /* R_HYBRID_PLC_RF */

typedef  uint16_t RmFrmFrameControlT;

/* MHR direct decode macros */
#define RmFrmGetFrameControl(x)                   (RmLetoHS(RmFrmGet16Bit(x)))
#define RmFrmGetFrameTypeInFCL(x)                 ((x) & 0x0007U)
#define RmFrmIsSecurityEnableInFCL(x)             ((x) & 0x0008U)
#define RmFrmIsFramePendingInFCL(x)               ((x) & 0x0010U)
#define RmFrmIsAcknowledgmentRequestInFCL(x)      ((x) & 0x0020U)
#define RmFrmIsIntraPANInFCL(x)                   ((x) & 0x0040U)
#define RmFrmGetDstAddressModeInFCL(x)            (((x) >> 10) & 0x0003U)
#define RmFrmGetSrcAddressModeInFCL(x)            (((x) >> 14) & 0x0003U)
#define RmFrmGetFrameVersionInFCL(x)              (((x) >> 12) & 0x0003U)
#define RmFrmGetSequenceNumberSuppressionInFCL(x) ((x) & 0x0100U)
#define RmFrmGetIEListPresentInFCL(x)             ((x) & 0x0200U)
#define RmFrmIsReservedBitInFCL(x)                ((x) & 0x0080U)

#define RmFrmMakeFrameControl(panid)

#define RM_FRM_FrameType_Beacon         (0U)
#define RM_FRM_FrameType_Data           (1U)
#define RM_FRM_FrameType_Acknowledgment (2U)
#define RM_FRM_FrameType_MACCommand     (3U)

/* The structure that the result which decoded MHR is stored */
typedef struct
{
    RmFrmFrameControlT frameControl;
    uint8_t            sequenceNumber;
    uint8_t            dstAddrMode;
    uint16_t           dstPANId;
    RmDeviceAddressT   dstAddr;
    uint8_t            srcAddrMode;
    uint16_t           srcPANId;
    RmDeviceAddressT   srcAddr;
    uint8_t            fVersion;
    boolean_t          sqnSuppression;
    boolean_t          IEListPresent;
} RmFrmMhrT;

#ifdef R_HYBRID_PLC_RF

/* MHR decode function */
RmOctetT RmPurseMhr(RmOctetT* psdu, RmFrmMhrT* pMhr);

/* MHR encode function */
RmShortT RmFrmBuildMhr(RmOctetT* pMhr, RmOctetT frameType,
                       RmOctetT dstAddrMode,
                       RmPANIdentifierT dstPANId,
                       RmDeviceAddressT dstAddr,
                       RmOctetT srcAddrMode,
                       RmPANIdentifierT srcPANId,
                       RmDeviceAddressT srcAddr,
                       RmOctetT seqNo,
                       RmBitmap8T txOptions,
                       RmOctetT fVersion,
                       RmBooleanT SqnSuppression,
                       RmShortT HeaderIELength,
                       RmOctetT* HeaderIE,
                       RmShortT PayloadIELength,
                       RmOctetT* PayloadIE,
                       RmBooleanT IECopySW,
                       RmBooleanT macProfileSpecificMode);
#endif /* R_HYBRID_PLC_RF */

/* PAN Identifier in MAC Frame             */
typedef uint16_t RmFrmPANIdentifierT;

/* Decode Macro                            */
#define RmFrmGetPANIdentifier(pFrame)         RmLetoHS(RmFrmGet16Bit(pFrame))

/* Encode Macro                            */
#define RmFrmMakePANIdentifier(panid)         RmHtoLeS(panid)

/* Encode Macro (for aligment) */
#define RmFrmSetPANIdentifier(pFrame, hostSa) RmFrmSet16Bit(pFrame, hostSa)

#ifdef R_HYBRID_PLC_RF

/* Short Address in MAC Frame             */
typedef RmShortAddressT RmFrmShortAddressT;

/* Decode Macro */
#define RmFrmGetShortAddress(pFrameSa)       (RmLetoHS(RmFrmGet16Bit(pFrameSa)))

/* Encode Macro */
#define RmFrmMakeShortAddress(hostSa)        RmHtoLeS(hostSa)

/* Encode Macro (for aligment) */
#define RmFrmSetShortAddress(pFrame, hostSa) RmFrmSet16Bit(pFrame, hostSa)
#endif /* R_HYBRID_PLC_RF */

/* Extended Address in MAC Frame             */
typedef RmExtendedAddressT RmFrmExtendedAddressT;

/* Decode Macro */
#define RmFrmGetExtendedAddress(hostEa, pFrameEa)   \
    do {RmMemcpy(hostEa, pFrameEa, 8);} while (0)

#define RmFrmSetExtendedAddress(pFrame, hostEa)    \
    do { \
        RmFrmSet32Bit(pFrame, RmHtoLeL(hostEa[0])); \
        RmFrmSet32Bit((pFrame + 4), RmHtoLeL(hostEa[1])); \
    } while (0)

/**
 * Parse the beginning of the MHR including addressing information but not including IEs/AuxSec
 * @param psdu the data frame
 * @param[out] pMhr the MHR information structure to be filled
 * @return the number of parsed bytes
 */
uint8_t RmParseMhr(uint8_t* psdu, RmFrmMhrT* pMhr);

/// @return the number of bytes of the frame or 0 if an error occurred
uint16_t RmFANFrmBuild(uint8_t*         pMhr,
                       uint16_t         size,
                       uint8_t          frameType,
                       uint8_t          frameVersion,
                       uint8_t          dstAddrMode,
                       uint16_t         dstPANId,
                       RmDeviceAddressT dstAddr,
                       uint8_t          srcAddrMode,
                       uint16_t         srcPANId,
                       RmDeviceAddressT srcAddr,
                       boolean_t        ackrequired,
                       boolean_t        security,
                       uint8_t          keyIndex,
                       boolean_t        seqNumberSuppression,
                       uint8_t          seqNumber,
                       boolean_t        panIDCompression,
                       const r_ie_wh_t* headerIE,
#ifdef R_HYBRID_PLC_RF
                       const r_ie_wp_t* payloadIE,
                       uint16_t         msduLength,
                       const uint8_t*   msdu);
#else
                       const r_ie_wp_t* payloadIE);
#endif

/**
 * Secure an existing Acknowledgement by adding security fields (like AUX Security Header) and encrypting it
 * @param ack The ACK that should be secured
 * @param ack_len The length of the specified ACK in bytes
 * @param keyIndex The zero-based key index to be used for encryption
 * @return The length of the secured ACK or 0 if an error occurred
 */
uint16_t RmFANFrmBuildSecuredAck(uint8_t* ack, uint16_t ack_len, uint8_t keyIndex);

#if R_EDFE_MINIMAL_RECEIVER
/**
 * Secure an existing EDFE final frame  adding security fields (like AUX Security Header) and encrypting it
 * @param edfe The EDFE final frame that should be secured
 * @param edfe_len The length of the specified EDFE final frame in bytes
 * @param keyIndex The zero-based key index to be used for encryption
 * @param srcAddress The source address
 * @return The length of the secured ACK or 0 if an error occurred
 */
uint16_t RmFANFrmBuildSecuredEdfeFinalFrame(uint8_t* edfe, uint16_t edfe_len, uint8_t keyIndex, uint8_t* srcAddress);
#endif /* R_EDFE_MINIMAL_RECEIVER */

/// the shared frame transmit buffer
#ifdef R_HYBRID_PLC_RF
extern uint8_t* r_tx_frame_buffer;
#else
extern uint8_t r_tx_frame_buffer[R_MAX_FRAME_SIZE];
#endif

#ifdef R_HYBRID_PLC_RF

/* 7.2.2.1 definition of Beacon Frame                             */
/* |MHR|SF Spec|GTS Field|Pend Data Field|Beacon Payload|         */
/* Superframe Specification Define                                */
#define RM_FRM_MacBreaconMhrLen        (13U) /* Max length of MHR              */
#define RM_FRM_MacBreaconSFSLen        (2U)  /* Max length of Super frame spec */
#define RM_FRM_MacBreaconGTSFiledsLen  (1U)
#define RM_FRM_MacBreaconAddrSpecLen   (1U)
#define RM_FRM_MacBreaconAddrListLen   (7U * 8U)
#define RM_FRM_MacBreaconAddrFieldsLen (RM_FRM_MacBreaconAddrSpecLen + RM_FRM_MacBreaconAddrListLen)

/* Max length of breacon payload */
#define RM_FRM_MacBreaconPayload       (RM_aMaxBeaconPayloadLength)

/* Max buffer length of breacon frame */
#define RM_FRM_BreaconFrameBufLen      (RM_FRM_MacBreaconMhrLen + \
                                        RM_FRM_MacBreaconSFSLen + \
                                        RM_FRM_MacBreaconGTSFiledsLen + \
                                        RM_FRM_MacBreaconAddrFieldsLen + \
                                        RM_FRM_MacBreaconPayload)

#define RM_FRM_EnhancedAckFrameBufLen  (127)        /* temp */

#define RM_FRM_BreaconFrameLen         (RM_FRM_BreaconFrameBufLen)

/* direct decode macro             */
#define RmFrmGetBeaconOrderInSFSpec(spec)         ((spec) & 0x000f)
#define RmFrmGetGetSuperframeOrderSFSpec(spec)    (((spec) >> 4) & 0x000f)
#define RmFrmGetFinalCAPSlotInSFSpec(spec)        (((spec) >> 8) & 0x000f)
#define RmFrmIsBatteryLifeExtensionInSFSpec(spec) ((spec) & 0x1000)
#define RmFrmIsPANCoordinatorInSFSpec(spec)       ((spec) & 0x4000)
#define RmFrmIsAssociationPermitInSFSpec(spec)    ((spec) & 0x8000)

/* endcode macros                  */
#define RmFrmMakeSFSpec(bo, so, f, b, p, a) \
    htons((((a) != 0) ? 0x8000U : 0U) | (((p) != 0) ? 0x4000U : 0U) | (((b) != 0) ? 0x1000U : 0U) | ((f) << 8) | ((so) << 4) | (bo))

/* 7.2.2.1.8 Beacon payload field               */
/* Nothing is defined about the Beacon Payload. */
typedef RmBeaconPayloadT              RmFrmBeaconPayloadT;

/* 7.2.2.2 Data Frame                           */
typedef uint8_t*                      RmFrmDataMACPayloadT;

/* 7.2.2.4 Command Frame                        */
/* 7.2.2.4.2 Command frame identifier field     */
typedef RmOctetT                      RmFrmCommandFrameIdentifierT;
typedef RmCommandFrameIdentifierEnumT RmFrmCommandFrameIdentifierEnumT;

/* 7.2.2.4.3 Command payload field(7.3 Mac Commands) */
/* 7.3.1.1 Association request command               */
/*       |MHR|Cmd Frame Identifier|Capability info|  */

#define RM_FRM_CmdPayloadLen_AssociationRequest        (1U)
#define RM_FRM_CmdPayloadLen_AssociationResponse       (3U)
#define RM_FRM_CmdPayloadLen_DiassociationNotification (1U)
#define RM_FRM_CmdPayloadLen_DataRequest               (0U)
#define RM_FRM_CmdPayloadLen_PANIDConflictNotification (0U)
#define RM_FRM_CmdPayloadLen_OrphanNotification        (0U)
#define RM_FRM_CmdPayloadLen_CoordinatorRealignment    (7U)

/* 7.3.2.4 Beacon request command                            */

#define RM_FRM_CmdPayloadLen_BeaconRequest (0U)

/* The macro to build the generic PSDU (Data frame). */
#define  RM_FRM_BUILD_DATA(pFbdData, \
                           dstAddrMode, \
                           dstPANId, \
                           dstAddr, \
                           srcAddrMode, \
                           srcPANId, \
                           srcAddr, \
                           seqNo, \
                           txOptions, \
                           fVersion, \
                           SqnSuppression, \
                           HeaderIELength, \
                           HeaderIE, \
                           PayloadIELength, \
                           PayloadIE, \
                           macProfileSpecificMode) \
    do { \
        (pFbdData) += RmFrmBuildMhr(pFbdData, RM_FRM_FrameType_Data, \
                                    dstAddrMode, dstPANId, dstAddr, \
                                    srcAddrMode, srcPANId, srcAddr, \
                                    seqNo, txOptions, fVersion, SqnSuppression, HeaderIELength, HeaderIE, PayloadIELength, PayloadIE, RM_FALSE, macProfileSpecificMode); \
        seqNo++; \
    } while (0)

/* The macro to build the generic PSDU (Command frame). */
#define RM_FRM_BUILD_COMMAND(pFbdData, dstAddrMode, dstPANId, dstAddr, srcAddrMode, \
                             srcPANId, srcAddr, seqNo, cmdId, txOptions, fVersion, SqnSuppression, HeaderIELength, HeaderIE, PayloadIELength, PayloadIE, macProfileSpecificMode) \
    do { \
        pFbdData += RmFrmBuildMhr(pFbdData, RM_FRM_FrameType_MACCommand, \
                                  (dstAddrMode), (dstPANId), (dstAddr), (srcAddrMode), (srcPANId), (srcAddr), \
                                  (seqNo), (txOptions), (fVersion), (SqnSuppression), (HeaderIELength), (HeaderIE), (PayloadIELength), (PayloadIE), RM_TRUE, (macProfileSpecificMode)); \
        *pFbdData = ((RmOctetT)cmdId);    pFbdData++; \
        seqNo++; \
    } while (0)

/* Structure of IEs (Figure 48n,48o,48q) */
#define RM_LEN_IE_DESCRIPTOR                    (2)
#define RM_LEN_MLMEIE_DESCRIPTOR                (2)

#define RM_MASK_IE_DESCRIPTOR_TYPE              (0x8000)
#define RM_VAL_IE_DESCRIPTOR_TYPE_HEADER        (0x0000)
#define RM_VAL_IE_DESCRIPTOR_TYPE_PAYLOAD       (0x8000)

#define RM_MASK_HEADERIE_DESCRIPTOR_LENGTH      (0x007F)
#define RM_SHIFT_HEADERIE_DESCRIPTOR_LENGTH     (0)
#define RM_MASK_HEADERIE_DESCRIPTOR_ELEMENTID   (0x7F80)
#define RM_SHIFT_HEADERIE_DESCRIPTOR_ELEMENTID  (7)

#define RM_MASK_PAYLOADIE_DESCRIPTOR_LENGTH     (0x07FF)
#define RM_SHIFT_PAYLOADIE_DESCRIPTOR_LENGTH    (0)
#define RM_MASK_PAYLOADIE_DESCRIPTOR_GROUPID    (0x7800)
#define RM_SHIFT_PAYLOADIE_DESCRIPTOR_GROUPID   (11)

#define RM_MASK_MLMEIE_DESCRIPTOR_TYPE          (0x8000)
#define RM_VAL_MLMEIE_DESCRIPTOR_TYPE_SHORT     (0x0000)
#define RM_VAL_MLMEIE_DESCRIPTOR_TYPE_LONG      (0x8000)

#define RM_MASK_MLMEIE_DESCRIPTOR_SHORT_LENGTH  (0x00FF)
#define RM_SHIFT_MLMEIE_DESCRIPTOR_SHORT_LENGTH (0)
#define RM_MASK_MLMEIE_DESCRIPTOR_SHORT_SUBID   (0x7F00)
#define RM_SHIFT_MLMEIE_DESCRIPTOR_SHORT_SUBID  (8)
#define RM_MASK_MLMEIE_DESCRIPTOR_LONG_LENGTH   (0x07FF)
#define RM_SHIFT_MLMEIE_DESCRIPTOR_LONG_LENGTH  (0)
#define RM_MASK_MLMEIE_DESCRIPTOR_LONG_SUBID    (0x7800)
#define RM_SHIFT_MLMEIE_DESCRIPTOR_LONG_SUBID   (11)

/* Structure of LE RIT IE (Figure 48u) */
#define RM_LEN_LERITIE_TIMEOF1STLISTEN          (1)
#define RM_LEN_LERITIE_NUMOFREPEATLISTEN        (1)
#define RM_LEN_LERITIE_REPEATLISTENINTERVAL     (2)

/* Structure of EB Filter IE (Figure 48ll) */
#define RM_LEN_EBFILTERIE_HEADER                (1)
#define RM_LEN_EBFILTERIE_LINKQUALITY           (1)
#define RM_LEN_EBFILTERIE_PERCENTFILTER         (1)
#define RM_LEN_EBFILTERIE_LISTOFPIBIDS          (1)

#define RM_MASK_EBFILTERIE_PERMITJOININGON      (0x01)
#define RM_VAL_EBFILTERIE_PERMITJOINING_ON      (0x01)
#define RM_VAL_EBFILTERIE_PERMITJOINING_OFF     (0x00)
#define RM_MASK_EBFILTERIE_INCLINKQUALITYFILTER (0x02)
#define RM_VAL_EBFILTERIE_LINKQUALITYFILTER_ON  (0x02)
#define RM_VAL_EBFILTERIE_LINKQUALITYFILTER_OFF (0x00)
#define RM_MASK_EBFILTERIE_INCPERCENTFILTER     (0x04)
#define RM_VAL_EBFILTERIE_PERCENTFILTER_ON      (0x04)
#define RM_VAL_EBFILTERIE_PERCENTFILTER_OFF     (0x00)
#define RM_MASK_EBFILTERIE_NUMOFENTRIES         (0x18)
#define RM_SHIFT_EBFILTERIE_NUMOFENTRIES        (3)
#define RM_ARRAYSIZE_EBFILTERIE_NUMOFENTRIES    (4)

/* Structure of MAC Metrics IE (Figure 48mm) */
#define RM_LEN_MACMETRICSIE_METRICSID           (1)
#define RM_LEN_MACMETRICSIE_COUNT               (4)

/* Structure of MAC AllMetrics IE (Figure 48nn) */
#define RM_LEN_ALLMACMETRICSIE_COUNT            (4)
#define RM_ARRAYSIZE_ALLMACMETRICSIE_COUNT      (10)


/* Element IDs(Header IEs) (Table 4b) */
#define RM_VAL_HEADERIE_DESCRIPTOR_ELEMENTID_LECSL       (0x1A)
#define RM_VAL_HEADERIE_DESCRIPTOR_ELEMENTID_LERIT       (0x1B)
#define RM_VAL_HEADERIE_DESCRIPTOR_ELEMENTID_DSMEPANDESC (0x1C)
#define RM_VAL_HEADERIE_DESCRIPTOR_ELEMENTID_RZTIME      (0x1D)
#define RM_VAL_HEADERIE_DESCRIPTOR_ELEMENTID_ACKNACKTC   (0x1E)
#define RM_VAL_HEADERIE_DESCRIPTOR_ELEMENTID_GACK        (0x1F)
#define RM_VAL_HEADERIE_DESCRIPTOR_ELEMENTID_LLNINFO     (0x20)
#define RM_VAL_HEADERIE_DESCRIPTOR_ELEMENTID_LISTTERM1   (0x7E)
#define RM_VAL_HEADERIE_DESCRIPTOR_ELEMENTID_LISTTERM2   (0x7F)

#define RM_LEN_HEADERIE_DESCRIPTOR_ELEMENTID_LECSL       (4)
#define RM_LEN_HEADERIE_DESCRIPTOR_ELEMENTID_LERIT       (4)
#define RM_LEN_HEADERIE_DESCRIPTOR_ELEMENTID_RZTIME      (2)
#define RM_LEN_HEADERIE_DESCRIPTOR_ELEMENTID_ACKNACKTC   (2)
#define RM_LEN_HEADERIE_DESCRIPTOR_ELEMENTID_GACK        (4)
#define RM_LEN_HEADERIE_DESCRIPTOR_ELEMENTID_LISTTERM    (0)

/* Group IDs(Payload MLME IEs) (Table 4c) */
#define RM_VAL_PAYLOADIE_DESCRIPTOR_GROUPID_ESDU         (0x0)
#define RM_VAL_PAYLOADIE_DESCRIPTOR_GROUPID_MLME         (0x1)
#define RM_VAL_PAYLOADIE_DESCRIPTOR_GROUPID_LISTTERM     (0xF)

#define RM_LEN_PAYLOADIE_DESCRIPTOR_GROUPID_LISTTERM     (0)

/* Sub-IDs(Payload IEs) (Table 4d) */
#define RM_VAL_MLMEIE_DESCRIPTOR_SUBID_TSCHSYNC          (0x1A)
#define RM_VAL_MLMEIE_DESCRIPTOR_SUBID_TSCHSLOTANDLINK   (0x1B)
#define RM_VAL_MLMEIE_DESCRIPTOR_SUBID_TSCHTIMESLOT      (0x1C)
#define RM_VAL_MLMEIE_DESCRIPTOR_SUBID_HOPPINGTIME       (0x1D)
#define RM_VAL_MLMEIE_DESCRIPTOR_SUBID_EBFILTER          (0x1E)
#define RM_VAL_MLMEIE_DESCRIPTOR_SUBID_MACMETRICS1       (0x1F)
#define RM_VAL_MLMEIE_DESCRIPTOR_SUBID_MACMETRICS2       (0x20)

#define RM_VAL_MLMEIE_DESCRIPTOR_SUBID_CHANNELHOPSEQ     (0x9)

#define RM_LEN_MLMEIE_DESCRIPTOR_SUBID_TSCHSYNC          (6)
#define RM_LEN_MLMEIE_DESCRIPTOR_SUBID_HOPPINGTIME       (5)
#define RM_LEN_MLMEIE_DESCRIPTOR_SUBID_MACMETRICS1       (5)
#define RM_LEN_MLMEIE_DESCRIPTOR_SUBID_MACMETRICS2       (40)
#define RM_LEN_PAYLOADIE_DESCRIPTOR_SUBID_EBFILTER       (7)

/* EB Filter IE (Figure 48ll) */
typedef struct tagRmIEContentEBFilter_T
{
    RmBooleanT bPermitJoiningOn;                                   /* Permit Joining On */
    RmBooleanT bIncludeLinkQualityFilter;                          /* Include Link Quality Filter */
    RmBooleanT bIncludePercentFilter;                              /* Include Percent Filter */
    RmOctetT   Numberofentries;                                    /* Number of entries in PIB filter list */
    RmOctetT   LinkQuality;                                        /* Link Quality */
    RmOctetT   PercentFilter;                                      /* Percent Filter */
    RmOctetT   ListofPIBIDs[RM_ARRAYSIZE_EBFILTERIE_NUMOFENTRIES]; /* List of PIB IDs */
} RmIEContentEBFilter_T;

/* MAC Metrics IE (Figure 48mm) */
typedef struct tagRmIEContentMACMetrics1_T
{
    RmOctetT MetricsID;  /* Metrics ID */
    RmLongT  Count;      /* Count */
} RmIEContentMACMetrics1_T;

/* AllMAC Metrics IE (Figure 48nn) */
typedef struct tagRmIEContentMACMetrics2_T
{
    RmLongT Count[RM_ARRAYSIZE_ALLMACMETRICSIE_COUNT];  /* Count */
} RmIEContentMACMetrics2_T;


/* !!!RMS removed(2)!!! */


#define RM_IE_ELEMENT_H_LERIT       (0x0000)  /* LE RIT HeaderIE */
#define RM_IE_ELEMENT_H_LISTTERM1   (0x00FE)  /* List Termination1 Header IE */
#define RM_IE_ELEMENT_H_LISTTERM2   (0x00FF)  /* List Termination2 Header IE */
#define RM_IE_ELEMENT_H_UNSUPPORTED (0x0FFF)  /* Unsupported Header IE */
#define RM_IE_ELEMENT_P_EBFILTER    (0x8000)  /* EB Filter Payload IE */
#define RM_IE_ELEMENT_P_MACMETRICS1 (0x8001)  /* MAC Metrics1 Payload IE */
#define RM_IE_ELEMENT_P_MACMETRICS2 (0x8002)  /* MAC Metrics2 Payload IE */
#define RM_IE_ELEMENT_P_LISTTERM    (0x80FF)  /* List Termination Payload IE */
#define RM_IE_ELEMENT_P_UNSUPPORTED (0x8FFF)  /* Unsupported Payload IE */


#define RmFrmGetIEDescriptor(pFrame)    (ntohs(RmFrmGet16Bit(pFrame)))
#define RmFrmGetMACMetricsCount(pFrame) (htonl(RmFrmGet32Bit(pFrame)))

/* !!!RMS added(2)!!! BEGIN */
#define RmFrmSetHeaderIEDescriptor(pFrame, Length, ElementID)       \
    do { \
        RmShortT Data; \
        Data = (((Length << RM_SHIFT_HEADERIE_DESCRIPTOR_LENGTH) & RM_MASK_HEADERIE_DESCRIPTOR_LENGTH) | \
                ((ElementID << RM_SHIFT_HEADERIE_DESCRIPTOR_ELEMENTID) & RM_MASK_HEADERIE_DESCRIPTOR_ELEMENTID) | \
                RM_VAL_IE_DESCRIPTOR_TYPE_HEADER); \
        RmFrmSet16Bit(pFrame, htonl(Data)); \
    } while (0)

#define RmFrmSetHeaderIEListTermination1(pFrame)                    \
    do { \
        RmFrmSetHeaderIEDescriptor(pFrame, RM_LEN_HEADERIE_DESCRIPTOR_ELEMENTID_LISTTERM, RM_VAL_HEADERIE_DESCRIPTOR_ELEMENTID_LISTTERM1); \
        pFrame += RM_LEN_HEADERIE_DESCRIPTOR_ELEMENTID_LISTTERM + RM_LEN_IE_DESCRIPTOR; \
    } while (0)

#define RmFrmSetHeaderIEListTermination2(pFrame)                    \
    do { \
        RmFrmSetHeaderIEDescriptor(pFrame, RM_LEN_HEADERIE_DESCRIPTOR_ELEMENTID_LISTTERM, RM_VAL_HEADERIE_DESCRIPTOR_ELEMENTID_LISTTERM2); \
        pFrame += RM_LEN_HEADERIE_DESCRIPTOR_ELEMENTID_LISTTERM + RM_LEN_IE_DESCRIPTOR; \
    } while (0)

/* !!!RMS added(2)!!! END */

#define RmFrmSetPayloadIEDescriptor(pFrame, Length, GroupID)        \
    do { \
        RmShortT Data; \
        Data = (((Length << RM_SHIFT_PAYLOADIE_DESCRIPTOR_LENGTH) & RM_MASK_PAYLOADIE_DESCRIPTOR_LENGTH) | \
                ((GroupID << RM_SHIFT_PAYLOADIE_DESCRIPTOR_GROUPID) & RM_MASK_PAYLOADIE_DESCRIPTOR_GROUPID) | \
                RM_VAL_IE_DESCRIPTOR_TYPE_PAYLOAD); \
        RmFrmSet16Bit(pFrame, htonl(Data)); \
    } while (0)

#define RmFrmSetPayloadIESubIEDescriptor(pFrame, Length, SubID)     \
    do { \
        RmShortT Data; \
        Data = (((Length << RM_SHIFT_MLMEIE_DESCRIPTOR_SHORT_LENGTH) & RM_MASK_MLMEIE_DESCRIPTOR_SHORT_LENGTH) | \
                ((SubID << RM_SHIFT_MLMEIE_DESCRIPTOR_SHORT_SUBID) & RM_MASK_MLMEIE_DESCRIPTOR_SHORT_SUBID) | \
                RM_VAL_MLMEIE_DESCRIPTOR_TYPE_SHORT); \
        RmFrmSet16Bit(pFrame, htonl(Data)); \
    } while (0)

#define RmFrmSetPayloadIEListTermination(pFrame)                    \
    do { \
        RmFrmSetPayloadIEDescriptor(pFrame, RM_LEN_PAYLOADIE_DESCRIPTOR_GROUPID_LISTTERM, RM_VAL_PAYLOADIE_DESCRIPTOR_GROUPID_LISTTERM); \
        pFrame += RM_LEN_PAYLOADIE_DESCRIPTOR_GROUPID_LISTTERM + RM_LEN_IE_DESCRIPTOR; \
    } while (0)

#define RmFrmSetEBFilterPayloadIE(pFrame, EBFilterIELength, EBRFilters)         \
    do { \
        RmFrmSetPayloadIEDescriptor(pFrame, EBFilterIELength + RM_LEN_IE_DESCRIPTOR, RM_VAL_PAYLOADIE_DESCRIPTOR_GROUPID_MLME); \
        pFrame += RM_LEN_IE_DESCRIPTOR; \
        RmFrmSetPayloadIESubIEDescriptor(pFrame, EBFilterIELength, RM_VAL_MLMEIE_DESCRIPTOR_SUBID_EBFILTER); \
        pFrame += RM_LEN_MLMEIE_DESCRIPTOR; \
        *(pFrame + 0) = EBRFilters; \
        pFrame += 1; \
    } while (0)

RmMacEnumT RmFrmGetLengthOfIEs(RmOctetT* pPsdu, RmShortT PsduLength, RmShortT* pHeaderIELength, RmShortT* pPayloadIELength);
RmMacEnumT RmFrmReadIE(RmOctetT* pPsdu, RmShortT IEBlockLength, RmShortT* pIEID, void* pIEContent, RmShortT* pIEContentLength);
#endif /* R_HYBRID_PLC_RF */
/******************************************************************************
 *  Copyright (C) 2010-2016 Renesas Electronics Corporation.
 *****************************************************************************/
#endif // #ifndef __RM_mac_frame_H
