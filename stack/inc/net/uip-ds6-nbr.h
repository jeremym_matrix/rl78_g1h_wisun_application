/*
 * Copyright (c) 2013, Swedish Institute of Computer Science.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *
 */

/**
 * Contiki uip-ds6-nbr module to r_nd module wrapper
 */

#ifndef UIP_DS6_NEIGHBOR_H_
#define UIP_DS6_NEIGHBOR_H_

#include "r_nd.h"
#include "r_nwk_api.h"
#include "r_io_vec.h"
#include "r_ipv6_helper.h"
#include "r_ipv6_headers.h"
#include "r_icmpv6.h"
#include "r_icmpv6_nd.h"

#include "net/uip.h"


/*--------------------------------------------------*/

// Map contiki type to r_ type
typedef r_nd_neighbor_cache_entry_t uip_ds6_nbr_t;

void                uip_ds6_neighbors_init(void);
uip_ds6_nbr_t*      uip_ds6_nbr_lookup(const uip_ipaddr_t* ipaddr);
uip_ds6_nbr_t*      uip_ds6_nbr_ll_lookup(const uip_lladdr_t* lladdr);
uip_ds6_nbr_t*      uip_ds6_nbr_ll_lookup_or_add(const uip_lladdr_t* lladdr);
const uip_lladdr_t* uip_ds6_nbr_lladdr_from_ipaddr(const uip_ipaddr_t* ipaddr);


#endif /* UIP_DS6_NEIGHBOR_H_ */
/** @} */
