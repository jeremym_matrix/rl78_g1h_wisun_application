/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer*
* Copyright (C) 2011 Renesas Electronics Corporation. All rights reserved.
*******************************************************************************/

/*******************************************************************************
 * File Name   : r_6lowpan.h
 * Version     : 1.0
 * Description : Header file for the 6LOWPAN layer initialization
 ******************************************************************************/

/*!
   \file      r_6lowpan.h
   \version   1.0
   \brief     Header file for the 6LOWPAN layer initialization
 */

#ifndef R_6LOWPAN_H
#define R_6LOWPAN_H

/******************************************************************************
   Macro definitions
******************************************************************************/
#define R_6LOWPAN_DISPATCH_FRAG1          (0xC0) //!< Fragmentation header (first fragment)
#define R_6LOWPAN_DISPATCH_FRAGN          (0xE0) //!< Fragmentation header (following fragment)
#define R_6LOWPAN_DISPATCH_IPHC           (0x60) //!< IPv6 IPHC compression header (RFC6282)
#define R_6LOWPAN_DISPATCH_IPV6           (0x41) //!< IPv6 uncompressed dispatch
#define R_6LOWPAN_DISPATCH_HC1            (0x42) //!< IPv6 HC1 compression header (RFC4944)
#define R_6LOWPAN_DISPATCH_ESC            (0x40) //!< Escape header (as defined in RFC6282)
#define R_6LOWPAN_DISPATCH_BC0            (0x50) //!< Broadcast header
#define R_6LOWPAN_DISPATCH_MESH           (0x80) //!< Mesh header

/* Fragmentation related defines */
#define R_FRAG_FIRST_FRAGMENT_HEADER_SIZE (4u)  //!< Size in bytes of the fragmentation header for the first fragment
#define R_FRAG_OTHER_FRAGMENT_HEADER_SIZE (5u)  //!< Size in bytes of the fragmentation header for the other fragments

/******************************************************************************
   Typedef definitions
******************************************************************************/

/*!
    \enum r_6lowpan_address_type_t
    \brief 6LoWPAN address type
 */
typedef enum
{
    R_6LP_HDR_ADDRESS_MODE_NONE     = 0, //!< No address
    R_6LP_HDR_ADDRESS_MODE_SHORT    = 2, //!< Short addressing mode
    R_6LP_HDR_ADDRESS_MODE_EXTENDED = 3  //!< Extended addressing mode

} r_6lp_hdr_address_type_t;

/******************************************************************************
   Exported global functions (to be accessed by other files)
******************************************************************************/

/*!
    \fn void R_6LP_Init(void)
    \brief This function calls 6LOWPAN initialization functions
    \return None
 */
void R_6LP_Init(void* vp_nwkGlobal);  // void* -> r_nwk_cb_t*

/*!
    \fn void R_6LP_Reset(void)
    \brief This function calls 6LOWPAN initialization functions
    \return None
 */
void R_6LP_Reset(void* vp_nwkGlobal);  // void* -> r_nwk_cb_t*

#endif /* R_6LOWPAN_H */
