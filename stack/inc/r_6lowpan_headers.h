/******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
*
* Copyright (C) 2011 Renesas Electronics Corporation. All rights reserved.
******************************************************************************/

/******************************************************************************
* File Name     : r_6lowpan_headers.h
* Version       : 1.01
* Device(s)     : RX651
* Tool-chain    :
* OS            :
* H/W platform  :
* Description   : Definitions for 6LowPAN headers
* Operation     :
* Limitations   :
******************************************************************************/

/*!
   \file      r_6lowpan_headers.h
   \version   1.01
   \brief     Definitions for 6LowPAN headers
 */

/******************************************************************************
 * History       : DD.MM.YYYY Version Description
 *               : 05.07.2011 1.00    First Release
 *               : 23.08.2011 1.01    Changed to IAR
 *               : 22.02.2012 1.02    QAC Compliant
 *******************************************************************************/

#ifndef R_6LP_HEADERS_H
#define R_6LP_HEADERS_H

/******************************************************************************
   Includes   <System Includes> , "Project Includes"
******************************************************************************/
#include "r_6lowpan.h"

/******************************************************************************
   Macro definitions
******************************************************************************/
#define R_HC_VALUE_UNDEFINED (0u)  //!< Undefined value for a header field
/* Currently not used.
#define SIZE_OF_MESH_HDR      (5u)  //!< Size of mesh header in bytes
#define SIZE_OF_BROADCAST_HDR (2u)  //!< Size of broadcast header in bytes
#define SIZE_OF_COMMAND_HDR   (2u)  //!< Size of command frame header
*/
/******************************************************************************
   Typedef definitions
******************************************************************************/

#if 0  // Currently not used.
/*!
    \enum r_hc_cmd_frame_id_t
    \brief Command frame header identifiers
 */
typedef enum
{
    R_HC_CFH_UNDEFINED          = R_HC_VALUE_UNDEFINED, //!< Undefined command frame header
    R_HC_CFH_MESH_ROUTING       = 1,                    //!< Mesh routing message
    R_HC_CFH_BOOTSTRAP_PROTOCOL = 2                     //!< LowPAN bootstrap protocol message

} r_hc_cmd_frame_id_t;

/*!
    \enum r_hc_cfa_description_id_t
    \brief CFA value field descriptions
 */
typedef enum
{
    R_HC_CFA_ALLOW_TX     = 0, //!< Request to allow a transmission
    R_HC_CFA_STOP_TX      = 1, //!< Request to stop a transmission
    R_HC_CFA_RESP_SUCCESS = 2, //!< Response with SUCCESS
    R_HC_CFA_RESP_FAIL    = 3  //!< Response with FAIL

} r_hc_cfa_description_id_t;

/*!
    \enum r_hc_frag_ack_req_t
    \brief Fragment acknowledge request
 */
typedef enum
{
    R_HC_FRG_ACK_REQ = 0, //!< Acknowledge request
    R_HC_FRG_NOT_ACK_REQ  //!< No acknowledge request

} r_hc_frag_ack_req_t;

/*!
    \enum r_hc_ack_ecn_t
    \brief Fragment acknowledge explicit congestion notification
 */
typedef enum
{
    R_HC_ACK_ECN = 0, //!< Explicit Congestion Notification (ECN)
    R_HC_ACK_NOT_ECN  //!< No Explicit Congestion Notification (ECN)

} r_hc_ack_ecn_t;

/*!
    \enum r_hc_mesh_addr_flg_t
    \brief Mesh address type flags
 */
typedef enum
{
    R_HC_MSH_ADDR_EXT   = 0x00, //!< Extended 64-bit address
    R_HC_MSH_ADDR_SHORT = 0x01  //!< Short 16-bit address

} r_hc_mesh_addr_flg_t;

/*!
    \enum r_hc_compress_flg_t
    \brief Compress or not compress header flags
 */
typedef enum
{
    R_HC_HEADER_COMPR = 0, //!< Compress IPv6/UDP header
    R_HC_HEADER_NOT_COMPR  //!< Do not compress IPv6/UDP header

} r_hc_compress_flg_t;

/*!
    \struct r_hc_mac_t
    \brief MAC Header data
 */
typedef struct
{
    uint16_t payLen;        //!< MAC layer payload length
    uint8_t  srcMode;       //!< Source addressing mode
    uint8_t  dstMode;       //!< Destination addressing mode
    uint8_t  frmSrcAddr[8]; //!< IEEE802.15.4 Src Address (2/8 bytes)
    uint8_t  frmDstAddr[8]; //!< IEEE802.15.4 Dst Address (2/8 bytes)
    uint16_t srcPanId;      //!< Source PAN ID
    uint16_t dstPanId;      //!< Destination PAN ID

} r_hc_mac_t;

/*!
    \union r_mesh_hdr_address
    \brief Mesh Header address
 */
typedef union
{
    uint8_t shortAddr[2]; // 16-bit short address
    uint8_t longAddr[8];  // Extended EUI-64

} r_mesh_hdr_address;

/*!
    \struct r_hc_mesh_t
    \brief Mesh Header data
 */
typedef struct
{
    r_hc_mesh_addr_flg_t v;         //!< Very first (originator) address type
    r_hc_mesh_addr_flg_t f;         //!< Final destination address type
    uint8_t              hopsLft;   //!< Hops left
    r_mesh_hdr_address   origAddr;  //!< Originator address
    r_mesh_hdr_address   finalAddr; //!< Final destination address
    r_boolean_t          hdrPres;   //!< Header present flag

} r_hc_mesh_t;
#endif  /* #if 0        // Currently not used. */

/*!
    \struct r_frag_header_data_t
    \brief Fragmentation header data
 */
typedef struct
{
    uint16_t    dtgSize;       //!< Datagram size
    uint16_t    dtgTag;        //!< Datagram tag
    uint8_t     dtgOffset;     //!< Datagram offset
    r_boolean_t firstFragment; //!< This is the first fragment
    r_boolean_t hdrPresent;    //!< Header present flag

} r_frag_header_data_t;
#if 0  // Currently not used.
/*!
    \struct r_hc_brdcst_t
    \brief Broadcast Header data
 */
typedef struct
{
    uint8_t     sequence; //!< Sequence number
    r_boolean_t hdrPres;  //!< Header present flag

} r_hc_brdcst_t;

/*!
    \struct r_hc_cmdfrm_t
    \brief Command Frame Header
 */
typedef struct
{
    r_hc_cmd_frame_id_t cmdId;   //!< Command ID
    r_boolean_t         hdrPres; //!< Header present flag

} r_hc_cmdfrm_t;

/*!
    \struct r_hc_ipv6_hdr_t
    \brief IPv6 header data
 */
typedef struct
{
    Ip6hdr  ipv6Hdr; //!< IPv6 header data
    UDP_hdr udpHdr;  //!< UDP header data

} r_hc_ipv6_hdr_t;
#endif  /* #if 0        // Currently not used. */

/*!
    \struct r_ipv6_header_data_t
    \brief IPv6 header data
 */
typedef struct
{
    r_boolean_t hdrPresent;  //!< Header present flag

} r_ipv6_header_data_t;

#if 0  // Currently not used.
/*!
    \struct r_hc_hdr_t
    \brief Frame header data
 */
typedef struct
{
    r_hc_mesh_t          meshHdr;   //!< Mesh header data
    r_frag_header_data_t fragHdr;   //!< Fragmentation header data
    r_hc_brdcst_t        brcHdr;    //!< Broadcast header data
    r_hc_cmdfrm_t        cmdHdr;    //!< Command frame header data
    r_ipv6_header_data_t ipv6Hdr;   //!< IPv6 header data
    uint16_t             hdrOffset; //!< Offset to begin of IpV6 header (if present) or payload

} r_hc_hdr_t;
#endif  /* #if 0        // Currently not used. */

/*!
    \struct r_header_data_t
    \brief Frame header data
 */
typedef struct
{
#if 0                               // Currently not used.
    r_hc_mesh_t          meshHdr;   //!< Mesh header data
#endif  /* #if 0        // Currently not used. */
    r_frag_header_data_t fragHdr;   //!< Fragmentation header data
    r_ipv6_header_data_t ipv6Hdr;   //!< IPv6 header data
    uint16_t             hdrOffset; //!< Offset to begin of IpV6 header (if present) or payload

} r_header_data_t;

/******************************************************************************
   Functions Prototypes
******************************************************************************/

#if 0  // Currently not used.
/*!
    \fn uint8_t R_HC_PackMeshHeader (const r_hc_mesh_t* p_hdr_data,
                                     uint8_t adp_ib_max_hops,
                                     uint8_t* p_hdr)
    \brief Pack mesh header information from a structure to an array
 */
uint8_t R_HC_PackMeshHeader(const r_hc_mesh_t* p_hdr_data,
                            uint8_t            adp_ib_max_hops,
                            uint8_t*           p_hdr);

/*!
    \fn uint16_t R_HC_PackHeaders (const r_hc_hdr_t* p_hdr_data,
                                   uint8_t adp_ib_max_hops,
                                   uint8_t* p_hdr)
    \brief Pack ADP headers information from a structure to an array
 */
uint16_t R_HC_PackHeaders(const r_hc_hdr_t* p_hdr_data,
                          uint8_t           adp_ib_max_hops,
                          uint8_t*          p_hdr);

/*!
    \fn r_result_t R_HC_UnpackHeaders(const uint8_t* p_hdr,
                              r_hc_hdr_t* p_hdr_data);
    \brief Unpack ADP headers information from an array to a structure
 */
r_result_t R_HC_UnpackHeaders(const uint8_t* p_hdr,
                              r_hc_hdr_t*    p_hdr_data);

/*!
    \fn void R_HC_CleanHeaders (r_hc_hdr_t* p_hdr_data)
    \brief Cleanup ADP headers structure
 */
void R_HC_CleanHeaders(r_hc_hdr_t* p_hdr_data);

#endif  /* #if 0        // Currently not used. */

/*!
   \fn          r_result_t unpackHeaders(const uint8_t* incomingFrame,
                                         const uint16_t frameLength,
                                         r_header_data_t* hdrData)
   \brief       Helper function to unpack 6LOWPAN headers (e.g. frag, IPv6...)
 */
r_result_t unpackHeaders(const uint8_t*   incomingFrame,
                         const uint16_t   frameLength,
                         r_header_data_t* hdrData);

#endif /* R_6LP_HEADERS_H */
