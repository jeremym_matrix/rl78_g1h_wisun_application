/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
* � 2012-2013 Renesas Electronics Corporation All rights reserved.
*******************************************************************************/

/*******************************************************************************
 * File Name   : r_byte_swap.h
 * Version     : 1.0
 * Description : Header file for the endian conversion module
 ******************************************************************************/

/*!
   \file      r_byte_swap.h
   \version   1.00
   \brief     Header file for the endian conversion module
 */

#ifndef R_BYTE_SWAP_H
#define R_BYTE_SWAP_H
/******************************************************************************
Macro definitions
******************************************************************************/
#ifdef R_HYBRID_PLC_RF
#define R_BYTE_UInt16ToArr UInt16ToArr
#define R_BYTE_UInt32ToArr UInt32ToArr
#define R_BYTE_ArrToUInt16 ArrToUInt16
#define R_BYTE_ArrToUInt32 ArrToUInt32
#endif /* R_HYBRID_PLC_RF */
/******************************************************************************
Typedef definitions
******************************************************************************/

/******************************************************************************
Exported global functions (to be accessed by other files)
******************************************************************************/
/***********************************************************************
* Function Name     : R_Swap64
* Description       : Convert endian for 64 bit data
* Argument          : uint8_t p_inArr[] Array used for the conversion (input)
*                   : uint8_t p_outarr[] Array used for the conversion (output)
* Return Value      : none
***********************************************************************/
/*!
   \fn          void R_Swap64(uint8_t p_inArr[], uint8_t p_outArr[])
   \brief       Convert endian for 64 bit data
   \param[in]   p_inAtrr pointer to an 8-bit array to input value
   \param[out]  p_outArr Pointer to an 8-bit array to store the converted value
   \return      void
 */
void R_Swap64(const uint8_t p_inArr[], uint8_t p_outArr[]);

/***********************************************************************
* Function Name     : UInt16ToArr
* Description       : uint16_t to array conversion
* Argument          : const uint16_t val Value to be converted
*                   : uint8_t p_arr[] Array used for the conversion
* Return Value      : none
***********************************************************************/
/*!
   \fn          void UInt16ToArr (uint16_t val, uint8_t p_arr[])
   \brief       Convert an unsigned 16-bit integer to array of 8-bit values
   \details     Convert an unsigned 16-bit integer to array of 8-bit values, taking into account the endianess as defined in _BIG_ENDIAN_
   \param[in]   val Value to be converted
   \param[out]  p_arr Pointer to an 8-bit array to store the converted value
   \return      void
 */
void UInt16ToArr(uint16_t val,
                 uint8_t  p_arr[]);

/***********************************************************************
* Function Name     : UInt16ToArr
* Description       : uint32_t to array conversion
* Argument          : const uint32_t val Value to be converted
*                   : uint8_t p_arr[] Array used for the conversion
* Return Value      : none
***********************************************************************/
/*!
   \fn          void UInt32ToArr (uint32_t val, uint8_t p_arr[])
   \brief       Convert an unsigned 32-bit integer to array of 8-bit values
   \details     Convert an unsigned 32-bit integer to array of 8-bit values, taking into account the endianess as defined in _BIG_ENDIAN_
   \param[in]   val Value to be converted
   \param[out]  p_arr Pointer to an 8-bit array to store the converted value
   \return      void
 */
void UInt32ToArr(uint32_t val,
                 uint8_t  p_arr[]);

#if defined(__CYGWIN32__) || defined(__ICCRX__) || defined(__CCRX__)

/***********************************************************************
* Function Name     : UInt64ToArr
* Description       : uint64_t to array conversion
* Argument          : const uint64_t val Value to be converted
*                   : uint8_t p_arr[] Array used for the conversion
* Return Value      : none
***********************************************************************/
/*!
   \fn          void UInt64ToArr (uint64_t val, uint8_t p_arr[])
   \brief       Convert an unsigned 64-bit integer to array of 8-bit values
   \details     Convert an unsigned 64-bit integer to array of 8-bit values, taking into account the endianess as defined in _BIG_ENDIAN_
   \param[in]   val Value to be converted
   \param[out]  p_arr Pointer to an 8-bit array to store the converted value
   \return      void
 */
void UInt64ToArr(const uint64_t val,
                 uint8_t        p_arr[]);
#endif

/***********************************************************************
* Function Name     : ArrToUInt16
* Description       : Array to uint16_t conversion
* Argument          : uint8_t p_arr[] Array used for the conversion
* Return Value      : uint16_t value
***********************************************************************/
/*!
   \fn          uint16_t ArrToUInt16 (const uint8_t p_arr[])
   \brief       Convert an array of 8-bit values to unsigned 16-bit integer
   \details     Convert an array of 8-bit values to unsigned 16-bit integer, taking into account the endianess as defined in _BIG_ENDIAN_
   \param[in]   p_arr Pointer to an 8-bit array to store the converted value
   \return      16-bit value
 */
uint16_t ArrToUInt16(const uint8_t p_arr[]);

/***********************************************************************
* Function Name     : ArrToUInt32
* Description       : Array to uint32_t conversion
* Argument          : uint8_t p_arr[] Array used for the conversion
* Return Value      : uint32_t value
***********************************************************************/
/*!
   \fn          uint32_t ArrToUInt32 (const uint8_t p_arr[])
   \brief       Convert an array of 8-bit values to unsigned 32-bit integer
   \details     Convert an array of 8-bit values to unsigned 32-bit integer, taking into account the endianess as defined in _BIG_ENDIAN_
   \param[in]   p_arr Pointer to an 8-bit array to store the converted value
   \return      32-bit value
 */
uint32_t ArrToUInt32(const uint8_t p_arr[]);

#if defined(__CYGWIN32__) || defined(__ICCRX__) || defined(__CCRX__)
/***********************************************************************
* Function Name     : ArrToUInt64
* Description       : Array to uint64_t conversion
* Argument          : uint8_t p_arr[] Array used for the conversion
* Return Value      : uint64_t value
***********************************************************************/
/*!
   \fn          uint64_t ArrToUInt64(const uint8_t p_arr[])
   \brief       Convert an array of 8-bit values to unsigned 64-bit integer
   \details     Convert an array of 8-bit values to unsigned 64-bit integer, taking into account the endianess as defined in _BIG_ENDIAN_
   \param[in]   p_arr Pointer to an 8-bit array to store the converted value
   \return      64-bit value
 */
uint64_t ArrToUInt64(const uint8_t p_arr[]);
#endif

#endif /* R_BYTE_SWAP_H */
