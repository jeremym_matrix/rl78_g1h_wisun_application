/******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
*
* Copyright (C) 2017 Renesas Electronics Corporation. All rights reserved.
******************************************************************************/

/******************************************************************************
* File Name     : r_dhcpv6.h
* Version       : 1.00
* OS            : none
* H/W platform  :
* Description   : Header file for DHCPv6 according to 20130125-FANWG-FANTPS-1v00
* Operation     :
* Limitations   :
******************************************************************************/

/*!
   \file      r_dhcpv6.h
   \version   1.00
   \brief     Header file for DHCPv6 according to 20130125-FANWG-FANTPS-1v00
 */

/******************************************************************************
 * History       : DD.MM.YYYY Version Description
 *               : 31.01.2017 1.00    First Release
 *****************************************************************************/

/*
 * Prevent nested inclusions
 */
#ifndef R_DHCPV6_H
#define R_DHCPV6_H

#ifndef R_DHCPV6_CLIENT_ENABLED
#define R_DHCPV6_CLIENT_ENABLED 1
#endif

#include "r_stdint.h"
#include "r_nwk_api_base.h"

/***********************************************************************
* Common functionality for DHCPv6 client and server
***********************************************************************/

enum
{
    R_DHCPV6_CLIENT_PORT = 546,  //!< The DHCPv6 client port number
    R_DHCPV6_SERVER_PORT = 547,  //!< The DHCPv6 server port number
};

/**
 * DHCPv6 Address Assignment
 */
typedef struct
{
    uint8_t  address[16];        //!< The IPv6 address
    uint32_t preferred_lifetime; //!< Preferred lifetime in seconds
    uint32_t valid_lifetime;     //!< Valid lifetime in seconds
} r_dhcpv6_address_assignment_t;

/**
 * Application callback to provide the data to be written into a DHCPv6 Vendor-specific Information Option
 * @details This function will be called by the DHCPv6 module when a Vendor-specific Information Option is sent. The
 * transmission of the DHCP message continues once this function returns.
 * @param[out] enterprise_number The vendor's registered Enterprise Number as registered with IANA
 * @param[out] option_data An opaque object of option_data_size octets, interpreted by vendor-specific code on the
 * receiving DHCPv6 clients and servers. The option data must be formatted according to RFC 3315, i.e., it MUST be
 * encoded as a sequence of code/length/value fields of identical format to the DHCP options field.
 * @param max_option_data_size The maximum number of bytes that may be written to the option_data buffer
 * @return The number of bytes written to the option_data buffer
 */
uint16_t R_DHCPV6_VendorOptionSendCallback(uint32_t* enterprise_number, uint8_t* option_data, uint16_t max_option_data_size);

/**
 * Application callback to process data from an incoming DHCPv6 Vendor-specific Information Option
 * @details This function will be called by the DHCPv6 module when a Vendor-specific Information Option is received
 * @warning The option data is freed once this function returns so any subsequent access is illegal!
 * If the option data should be used outside/after execution of this callback, it MUST be copied.
 * @param src The EUI-64 of the sender of the DHCPv6 message that contained the Vendor-specific Information Option
 * @param enterprise_number The vendor's registered Enterprise Number as registered with IANA
 * @param option_data The option data from the DHCPv6 Vendor-specific Information Option (format according to RFC 3315)
 * @param option_len The size of the provided option_data buffer in bytes
 */
void R_DHCPV6_VendorOptionRecvCallback(const r_eui64_t* src, uint32_t enterprise_number,
                                       const uint8_t* option_data, uint16_t option_len);


#if R_DHCPV6_CLIENT_ENABLED
/***********************************************************************
* DHCPv6 client and relay agent function declarations
***********************************************************************/

/*!
    \fn r_result_t R_DHCPV6_ClientInit(const uint8_t* eui64, void *vp_nwkGlobal);
    \brief Initialize the DHCPv6 client and relay agent
    \details Note that R_DHCPV6_ClientSolicit() must be called to start the solicit process
    \param eui64 The EUI-64 of the node
    \param vp_nwkGlobal Pointer to the nwkGlobal structure of type r_nwk_cb_t
    \return R_RESULT_SUCCESS on success
*/
r_result_t R_DHCPV6_ClientInit(const uint8_t* eui64, void* vp_nwkGlobal);  // void* -> r_nwk_cb_t*

#if !R_MODEM_SERVER
/**
 * Enable or disable the transmission and processing of DHCPv6 Vendor-specific Information Option
 * @details Incoming and outgoing vendor-specific option data is read/written by the corresponding callback functions.
 * @param enabled True, if a DHCPv6 Vendor-specific Information Option should be included in messages to the server and
 * a Vendor-specific Information Option should be requested from the DHCPv6 server.
 * False, if no Vendor-specific Information Option should be transmitted or processed.
 */
void R_DHCPV6_ClientSetVendorSpecificOptionSetting(r_boolean_t enabled);

/**
 * Determine whether the usage of Vendor-specific Information Option is enabled or not
 * @return True if the usage of Vendor-specific Information Option in DHCPv6 messages is enabled. False otherwise.
 */
r_boolean_t R_DHCPV6_ClientGetVendorSpecificOptionSetting();
#endif /* !R_MODEM_SERVER */

/*!
    \fn r_result_t R_DHCPV6_ClientSolicit(void);
    \brief Start a DHCPv6 solicitation process
    \return R_RESULT_SUCCESS on successful process initiation
*/
r_result_t R_DHCPV6_ClientSolicit(void);

/*!
    \fn r_result_t R_DHCPV6_ClientStop(void);
    \brief Stop the DHCPv6 client and relay agent (may be called before R_DHCPV6_ClientInit())
    \return R_RESULT_SUCCESS on success
*/
void R_DHCPV6_ClientStop(void);

/*!
    \fn r_result_t R_DHCPV6_ProcessClientMessage(const uint8_t *source, const uint8_t* dest, const uint8_t* buf, uint16_t size);
    \brief Process incoming client message
    \param source The IPv6 source address of the received message
    \param dest The IPv6 destination address of the received message
    \param buf The received message buffer
    \param size The size of the received message
    \return R_RESULT_SUCCESS on success
*/
r_result_t R_DHCPV6_ProcessClientMessage(const uint8_t* source, const uint8_t* dest, const uint8_t* buf, uint16_t size);

/**
 * Process an incoming DHCPv6 relay message -> Forward it to the BR (solicit) or destination (reply)
 * @param source The IPv6 source address of the received message
 * @param buf The received message
 * @param size The size of the received message in bytes
 * @return R_RESULT_SUCCESS on success
 */
r_result_t R_DHCPV6_ProcessRelayMessage(const uint8_t* source, const uint8_t* buf, uint16_t size);
#endif /* R_DHCPV6_CLIENT_ENABLED */

#if R_DHCPV6_SERVER_ENABLED
/***********************************************************************
* DHCPv6 server function declarations
***********************************************************************/

/*!
    \struct r_dhcpv6_server_map_entry_t
    \brief DHCPv6 Server EUI->IPv6 map entry
 */
typedef struct
{
    uint8_t eui64[8];  //!< The EUI-64 (map key)
    uint8_t ipv6[16];  //!< The IPv6 address (map value)
} r_dhcpv6_server_map_entry_t;

/*!
    \struct r_dhcpv6_server_map_t
    \brief DHCPv6 Server EUI->IPv6 map
 */
typedef struct
{
    const r_dhcpv6_server_map_entry_t* entries; //!< The map entries
    uint8_t size;                               //!< The number of map entries
} r_dhcpv6_server_map_t;

/*! \name DHCPv6 Server Functions
 * The server functionality is only enabled when setting R_DHCPV6_SERVER_ENABLED=1.
 * @{
 */

/*!
    \brief Lookup callback function for assigning DHCP addresses
    \param eui64 The EUI-64 of the client
    \param user Opaque pointer provided to R_DHCPV6_ServerInit()
    \param[out] out_assignments Pointer to buffer for address assignments
    \param capacity Number of available entries in out_assignments
    \return The number of address assignments written to out_assignments (must be <= capacity)
*/
typedef uint8_t (* r_dhcpv6_server_lookup_cb)(const uint8_t* eui64, void* user, r_dhcpv6_address_assignment_t* out_assignments, uint8_t capacity);

/*!
    \fn r_result_t R_DHCPV6_ServerInit(const uint8_t* eui64, r_dhcpv6_server_lookup_cb lookup, void* lookupUser, void *vp_nwkGlobal);
    \brief Initialize the DHCPv6 server
    \param eui64 The EUI-64 of the node
    \param lookup The lookup function
    \param lookupUser The opaque user pointer passed to the lookup function
    \return R_RESULT_SUCCESS on success
*/
r_result_t R_DHCPV6_ServerInit(const uint8_t* eui64, r_dhcpv6_server_lookup_cb lookup, void* lookupUser);  // void* -> r_nwk_cb_t*

/*!
    \fn r_result_t R_DHCPV6_ServerStop(void);
    \brief Stop the DHCPv6 server (may be called before R_DHCPV6_ServerInit())
    \return R_RESULT_SUCCESS on success
*/
void R_DHCPV6_ServerStop(void);

/*!
    \fn uint8_t R_DHCPV6_ServerLookupAssignPrefix(const uint8_t* eui64, void* user, r_dhcpv6_address_assignment_t* out_assignments, uint8_t capacity);
    \brief DHCPv6 server lookup function that assigns addresses as PREFIX | EUI-64 (with toggled U/L bit) with PREFIX specified by \p user
    \param eui64 The EUI-64 of the client
    \param user Opaque pointer provided to R_DHCPV6_ServerInit(); must point to a uint8_t[8]
    \param[out] out_assignments Pointer to buffer for address assignments
    \param capacity Number of available entries in out_assignments
    \return The number of address assignments written to out_assignments (0 or 1)
*/
uint8_t R_DHCPV6_ServerLookupAssignPrefix(const uint8_t* eui64, void* user, r_dhcpv6_address_assignment_t* out_assignments, uint8_t capacity);

/*!
    \fn uint8_t R_DHCPV6_ServerLookupByMap(const uint8_t* eui64, void* user, r_dhcpv6_address_assignment_t* out_assignments, uint8_t capacity);
    \brief DHCPv6 server lookup functions that assigns addresses based on a r_dhcpv6_server_map_t specified by \p user
    \param eui64 The EUI-64 of the client
    \param user Opaque pointer provided to R_DHCPV6_ServerInit(); must point to a r_dhcpv6_server_map_t
    \param[out] out_assignments Pointer to buffer for address assignments
    \param capacity Number of available entries in out_assignments
    \return The number of address assignments written to out_assignments (0 or 1)
*/
uint8_t R_DHCPV6_ServerLookupByMap(const uint8_t* eui64, void* user, r_dhcpv6_address_assignment_t* out_assignments, uint8_t capacity);

/*!
    \fn r_result_t R_DHCPV6_ProcessServerMessage(const uint8_t *source, const uint8_t* dest, const uint8_t* buf, uint16_t size);
    \brief Process incoming server message for DHCPv6 server
    \param src The IPv6 source address of the received message
    \param in The received message buffer
    \param in_size The size of the received message
    \return R_RESULT_SUCCESS if the server message has been processed successfully. Appropriate error code otherwise
*/
r_result_t R_DHCPV6_ServerProcessMsg(const uint8_t* src, const uint8_t* in, uint16_t in_size);

//! @}
#endif /* R_DHCPV6_SERVER_ENABLED */

#endif /* R_DHCPV6_H */
