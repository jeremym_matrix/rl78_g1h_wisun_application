/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer*
* Copyright (C) 2011 Renesas Electronics Corporation. All rights reserved.
*******************************************************************************/

/*******************************************************************************
 * File Name     : r_frag_rx.h
 * Version     : 1.0
 * Description : This module implements fragmentation reassembly
 ******************************************************************************/

/*!
   \file      r_frag_rx.h
   \version   1.0
   \brief     This module implements fragmentation reassembly
 */

/******************************************************************************
   Macro definitions
******************************************************************************/

/******************************************************************************
   Typedef definitions
******************************************************************************/

/******************************************************************************
   Exported global variables
******************************************************************************/

/******************************************************************************
   Exported global functions (to be accessed by other files)
******************************************************************************/

/**
 * Init the fragmentation rx module
 * @param vp_nwkGlobal pointer to r_nwk_cb_t
 */
void R_FRAG_RxInit(void* vp_nwkGlobal);  // void* -> r_nwk_cb_t*

/**
 * Resets the fragmentation rx module
 * @param vp_nwkGlobal pointer to r_nwk_cb_t
 */
void R_FRAG_RxReset(void* vp_nwkGlobal);         // void* -> r_nwk_cb_t*

r_boolean_t R_FRAG_RxIsIdle(void* vp_nwkGlobal); // void* -> r_nwk_cb_t*

/**
 * Frees buffer previously allocated
 * @param buffer address of reassembly buffer to be freed (may be NULL)
 */
void R_FRAG_FreeIPv6Buffer(uint8_t* buffer);
