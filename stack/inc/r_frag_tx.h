/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer*
* Copyright (C) 2011 Renesas Electronics Corporation. All rights reserved.
*******************************************************************************/

/*******************************************************************************
 * File Name   : r_frag_tx.h
 * Version     : 1.0
 * Description : Header file for the fragmentation tx module
 ******************************************************************************/

/*!
   \file      r_frag_tx.h
   \version   1.0
   \brief     Header file for the fragmentation tx module
 */

#ifndef R_FRAG_TX_H
#define R_FRAG_TX_H

/******************************************************************************
   Macro definitions
******************************************************************************/

/******************************************************************************
   Typedef definitions
******************************************************************************/

/******************************************************************************
   Exported global variables
******************************************************************************/

/******************************************************************************
   Exported global functions (to be accessed by other files)
******************************************************************************/

/*!
    \fn void R_FRAG_TxInit(void);
    \brief This function initializes the fragmentation tx module
    \return None
 */
void R_FRAG_TxInit(void* vp_nwkGlobal);  // void* -> r_nwk_cb_t*

/*!
    \fn void R_FRAG_TxReset(void);
    \brief This function resets the fragmentation tx module
    \return None
 */
void R_FRAG_TxReset(void* vp_nwkGlobal);  // void* -> r_nwk_cb_t*

#endif /* R_FRAG_TX_H */
