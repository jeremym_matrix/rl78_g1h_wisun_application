/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer*
* Copyright (C) 2011 Renesas Electronics Corporation. All rights reserved.
*******************************************************************************/

/*******************************************************************************
 * File Name   : r_hc_6282.h
 * Version     : 1.1
 * Description : Header file for the header compression and uncompression module
 ******************************************************************************/

/*!
   \file      r_hc_6282.h
   \version   1.1
   \brief     Header file for the header compression and uncompression module
   \details   This module implements header compression und decompression features
                      as described in <a href="http://tools.ietf.org/html/rfc6282">IETF's RFC6282</a>.
                      The functions are described separately, the design specification can be accessed
                      <a href="../../eapOut/index.html">here</a>. \n \n

                      To use this module, the corresponding initialization function must be called. The
                      function takes pointers to context management functions that have to be provided
                      from the higher level system.

                      A definition for R_HC_MAX_CONTEXT_INDEX has to be provided by an external configuration
                      file. It describes the maximum allowed context index in the system.

                      The following limitations apply: \n
                      - IPv6 tunneling is not supported, neither in compression nor uncompression
                      - Clarifications from G3 ADP IOT have been applied (i.e. no further compression
                        after IPv6 fragmentation header, generalized adding of Pad1/PadN in uncompression
                        and treating of IPv6 fragmentation header's reserved field as length field). These
                        modifications have to be double-checked once official clarifications from IETF
                        become available.
 */

#ifndef R_HC_6282_H
#define R_HC_6282_H

/******************************************************************************
   Macro definitions
******************************************************************************/
#define R_HC_MAX_UNCOMPRESSD_HDR_LENGTH (255)  //!< Buffer size used for uncompressed headers possibly including extended headers

/******************************************************************************
   Typedef definitions
******************************************************************************/

/*!
    \enum r_hc_result_t
    \brief Header compression result values
 */
typedef enum
{
    R_HC_RESULT_SUCCESS              = 0x00, //!< Processing successful
    R_HC_RESULT_FAILED               = 0x01, //!< Process failed
    R_HC_RESULT_BAD_INPUT_ARGUMENTS  = 0x02, //!< Bad function input arguments
    R_HC_RESULT_ILLEGAL_NULL_POINTER = 0x03, //!< Input to a function was null pointer
    R_HC_RESULT_BUFFER_ERROR         = 0x04, //!< Write access out of array boundary or iovec error
    R_HC_RESULT_UNKNOWN              = 0xFF  //!< Result not yet set

} r_hc_result_t;

/******************************************************************************
   Exported global variables
******************************************************************************/

/******************************************************************************
   Exported global functions (to be accessed by other files)
******************************************************************************/

/*!
    \fn r_hc_result_t R_HC_Compress(const uint8_t* input,
                                    r_iovec_class_t* outIovec,
                                    uint8_t* compressedHdrLocation,
                                    const uint16_t compressedHdrMaxSize,
                                    uint16_t* uncompHdrSize,
                                    const r_boolean_t allowUdpCheckSumElide,
                                    const uint8_t* srcMacAddress,
                                    const r_6lowpan_address_type_t srcAddrMode,
                                    const uint8_t* dstMacAddress,
                                    const r_6lowpan_address_type_t dstAddrMode);
    \brief The function performs the RFC6282 IPv6 header and extension headers compression (<a href="../../eap_headerCompression/index.htm">Procedure described here</a>)
    \param[in] input Pointer to the ipv6 frame
    \param[out] outIovec iovec describing the compressed header and payload
    \param[in] compressedHdrLocation Pointer to a memory location where the function can write the compressed headers
    \param[in] compressedHdrMaxSize Length of compressedHdrLocation
    \param[out] uncompHdrSize The size of the uncompressed headers for the headers that have been compressed
    \param[in] allowUdpCheckSumElide R_TRUE if eliding of the udp checksum is allowed, else R_FALSE
    \param[in] srcMacAddress The short or extended source MAC address used for sending this frame
    \param[in] srcAddrMode The type of the source MAC address used for sending this frame
    \param[in] dstMacAddress The short or extended destination MAC address used for sending this frame
    \param[in] dstAddrMode The type of the destination MAC address used for sending this frame
    \return R_HC_RESULT_ILLEGAL_NULL_POINTER if any of the pointers passed to the function are NULL (except for srcMacAddress and dstMacAddress), R_HC_RESULT_BAD_INPUT_ARGUMENTS
            if srcAddrMode or dstAddrMode is R_6LOWPAN_ADDRESS_MODE_NONE and the srcMacAddress and dstMacAddress pointers are NULL, R_HC_RESULT_FAILED if any of the functions
            called by this function does not return R_HC_RESULT_SUCCESS and R_HC_RESULT_SUCCESS in all other cases
            if srcAddrMode or dstAddrMode is R_6LP_HDR_ADDRESS_MODE_NONE and the srcMacAddress and dstMacAddress pointers are NULL, R_HC_RESULT_FAILED if any of the functions
            called by this function does not return R_HC_RESULT_SUCCESS and R_HC_RESULT_SUCCESS in all other cases
 */

r_hc_result_t R_HC_Compress(const uint8_t*                 input,
                            const uint16_t                 inputLength,
                            r_iovec_class_t*               outIovec,
                            uint8_t*                       compressedHdrLocation,
                            const uint16_t                 compressedHdrMaxSize,
                            uint16_t*                      uncompHdrSize,
                            const r_boolean_t              allowUdpCheckSumElide,
                            const uint8_t*                 srcMacAddress,
                            const r_6lp_hdr_address_type_t srcAddrMode,
                            const uint8_t*                 dstMacAddress,
                            const r_6lp_hdr_address_type_t dstAddrMode);

/*!
    \fn r_hc_result_t R_HC_Uncompress(const uint8_t inputVector[],
                                      const uint16_t inputLength,
                                      uint8_t* hdrOutBuffer,
                                      const uint16_t hdrOutBufferLength,
                                      r_hc_upd_uncomp_t* udpUncompMem,
                                      const uint16_t datagramSize,
                                      r_iovec_class_t* ioVecOut,
                                      const uint8_t srcMacAddress[],
                                      const r_6lowpan_address_type_t srcAddrMode,
                                      const uint8_t dstMacAddress[],
                                      const r_6lowpan_address_type_t dstAddrMode);
    \brief This function performs IPv6, extension and UDP header uncompression following RFC6282 (<a href="../../eap_headerCompression/index.htm">Procedure described here</a>)
    \param[in] inputVector Pointer to begin of (un)compressed IPv6 header (first byte must always be the dispatch byte)
    \param[in] inputLength Length of the input vector.
    \param[out] hdrOutBuffer Memory location where the uncompressed IPv6 header will be written.
    \param[out] hdrOutBufferLength Length of the hdrOutBuffer.
    \param[out] udpUncompMem Structure to temporarily store UDP header data for later reconstruction.
    \param[in] datagramSize Total size of IPv6 datagram (incl. IPv6 header) if known from e.g. fragmentation process. Must be set to zero if not known.
    \param[out] ioVecOut IoVec structure containing uncompressed header in first element and, if present, payload in second element.
    \param[in] srcMacAddress The short or extended source MAC address used for sending this frame
    \param[in] srcAddrMode The type of the source MAC address used for sending this frame
    \param[in] dstMacAddress The short or extended destination MAC address used for sending this frame
    \param[in] dstAddrMode The type of the destination MAC address used for sending this frame
    \return R_HC_RESULT_SUCCESS, R_HC_RESULT_FAILED, R_HC_RESULT_BAD_INPUT_ARGUMENTS or R_HC_RESULT_ILLEGAL_NULL_POINTER
 */
r_hc_result_t R_HC_Uncompress(const uint8_t                  inputVector[],
                              const uint16_t                 inputLength,
                              uint8_t*                       hdrOutBuffer,
                              const uint16_t                 hdrOutBufferLength,
                              r_hc_upd_uncomp_t*             udpUncompMem,
                              const uint16_t                 datagramSize,
                              r_iovec_class_t*               ioVecOut,
                              const uint8_t                  srcMacAddress[],
                              const r_6lp_hdr_address_type_t srcAddrMode,
                              const uint8_t                  dstMacAddress[],
                              const r_6lp_hdr_address_type_t dstAddrMode);

/*!
    \fn r_hc_result_t R_HC_ReconstructUDPHeader(uint8_t datagram[],
                                                const r_hc_upd_uncomp_t* udpUncompMem,
                                                const r_boolean_t integrityVerified);
    \brief This function performs IPv6, extension and UDP header uncompression following RFC6282 (<a href="../../eap_headerCompression/index.htm">Procedure described here</a>)
    \param[out] datagram Uncompressed IPv6 datagram. Memory locations reserved for uncompressed UDP header have to be set to zero.
    \param[in] udpUncompMem Structure to temporarily store UDP header data for later reconstruction.
    \param[in] integrityVerified Flag that indicates if the calling layer can guarantee for proper integrity check of the data.
    \return R_HC_RESULT_SUCCESS, R_HC_RESULT_FAILED or R_HC_RESULT_ILLEGAL_NULL_POINTER
 */
r_hc_result_t R_HC_ReconstructUDPHeader(uint8_t                  datagram[],
                                        const r_hc_upd_uncomp_t* udpUncompMem,
                                        const r_boolean_t        integrityVerified);

/*!
    \fn r_hc_result_t R_HC_SetTxPanId(const uint16_t panId);
    \brief This function stores the given PAN ID for usage of RFC6282 with modifications specified in ITU G.9903, i.e.
           the address is no longer built as 0000:00ff:fe00:XXXX, but as YYYY:00ff:fe00:XXXX with YYYY the PAN ID and XXXX the short address.
           If not called, the default PAN ID 0x0000 will be used.
    \param[in] panId PAN ID to be used for compression.
    \return R_HC_RESULT_SUCCESS
 */
r_hc_result_t R_HC_SetTxPanId(const uint16_t panId);

/*!
    \fn r_hc_result_t R_HC_SetRxPanId(const uint16_t panId);
    \brief This function stores the given PAN ID for usage of RFC6282 with modifications specified in ITU G.9903, i.e.
           the address is no longer built as 0000:00ff:fe00:XXXX, but as YYYY:00ff:fe00:XXXX with YYYY the PAN ID and XXXX the short address.
           If not called, the default PAN ID 0x0000 will be used.
    \param[in] panId PAN ID to be used for uncompression.
    \return R_HC_RESULT_SUCCESS
 */
r_hc_result_t R_HC_SetRxPanId(const uint16_t panId);

#if R_HC_IPHC_STATEFUL_COMPRESSION_ENABLED
/*!
    \fn void R_HC_Init(uint8_t (*R_HC_GetContextIdFromPrefix)(const uint8_t* prefixValue, uint8_t* prefixLength),
                        uint8_t* (*R_HC_GetPrefixFromContextId)(const uint8_t contextId, uint8_t* contextLength));
    \brief This function sets call-back functions used to obtain contexts from indices or vice versa.
    \param[in] R_HC_GetContextIdFromPrefix Pointer to function for obtaining a context index from a prefix.
    \param[in] R_HC_GetPrefixFromContextId Pointer to function for obtaining a prefix from a context index.
    \return None
 */
r_hc_result_t R_HC_Init(uint8_t (* R_HC_GetContextIdFromPrefix)(const uint8_t* prefixValue,
                                                                uint8_t* prefixLength),
                        uint8_t* (*R_HC_GetPrefixFromContextId)(const uint8_t contextId, uint8_t* contextLength));
#endif

#endif /* R_HC_6282_H */
