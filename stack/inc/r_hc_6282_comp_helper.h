/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer*
* Copyright (C) 2011 Renesas Electronics Corporation. All rights reserved.
*******************************************************************************/

/*******************************************************************************
 * File Name   : r_hc_6282_comp_helper.h
 * Version     : 1.0
 * Description : Header file for the header compression helper functions
 ******************************************************************************/

/*!
   \file      r_hc_6282_comp_helper.h
   \version   1.0
   \brief     Header file for the header compression helper functions
 */

#ifndef R_HC_6282_COMP_HELPER_H
#define R_HC_6282_COMP_HELPER_H

/******************************************************************************
   Macro definitions
******************************************************************************/
#define R_HC_MAX_PACKED_UDP_LENGTH          (7u)  //!< UDP header encoding + ports + checksum
#define R_HC_MAX_PACKED_FRAG_LENGTH         (7u)  //!< Fragmentation header encoding (minus next header)
#define R_HC_MAX_COMPRESSED_IPV6_HDR_LENGTH (39u) //!< Maximum length of the compresses header in bytes

/******************************************************************************
   Typedef definitions
******************************************************************************/

/*!
    \enum r_hc_address_type_t
    \brief Address types enumeration
 */
typedef enum
{
    R_HC_ADDRESS_TYPE_SOURCE      = 0, //!< Source address
    R_HC_ADDRESS_TYPE_DESTINATION = 1  //!< Destination address

} r_hc_address_type_t;

/******************************************************************************
   Exported global variables
******************************************************************************/

/******************************************************************************
   Exported global functions (to be accessed by other files)
******************************************************************************/

/*!
        \fn r_hc_result_t R_HC_SetIphcTfField(const uint8_t dscp,
                                              const uint8_t ecn,
                                              const uint32_t flowLabel,
                                              r_hc_traffic_class_field_t* tfField);
        \brief The function sets the TF field of the base IPHC header according to DSCP, ECN and flow label
        \param[in]  dscp        The DSCP element of the IPv6 traffic class element
        \param[in]  ecn         The ECN element of the IPv6 traffic class element
        \param[in]  flowLabel   The flow label field of the IPv6 header
        \param[out] tfField     TF field
        \return R_HC_RESULT_ILLEGAL_NULL_POINTER if any of the pointers passed to the function are NULL, R_HC_RESULT_BAD_INPUT_ARGUMENTS
            if the combination of DSCP, ECN and flow label is not valid, R_HC_RESULT_SUCCESS in all other cases
 */
r_hc_result_t R_HC_SetIphcTfField(const uint8_t               dscp,
                                  const uint8_t               ecn,
                                  const uint32_t              flowLabel,
                                  r_hc_traffic_class_field_t* tfField);

/*!
        \fn r_hc_result_t R_HC_SetIphcNextHeaderField(const uint8_t nextHdr,
                                          const uint8_t nhLocation[],
                                          r_hc_next_header_field_t* nhField,
                                          const uint16_t sumWrittenBytes,
                                          const uint16_t compressedHdrMaxSize);
        \brief The function sets the next header field of the base IPHC header according to the number of bytes already written to the output buffer and size of the next header
        \param[in]  nextHdr                 The type of the next header
        \param[in]  nhLocation              Pointer to the next header
        \param[out] nhField                 Pointer to the next header field
        \param[in]  sumWrittenBytes         Total number of written bytes in the buffer containing the compressed header
        \param[in]  compressedHdrMaxSize    The maximum size in bytes that can be written to the compressed header buffer
        \return R_HC_RESULT_ILLEGAL_NULL_POINTER if any of the pointers passed to the function are NULL, R_HC_RESULT_SUCCESS in all other cases
 */
r_hc_result_t R_HC_SetIphcNextHeaderField(const uint8_t             nextHdr,
                                          const uint8_t             nhLocation[],
                                          r_hc_next_header_field_t* nhField,
                                          const uint16_t            sumWrittenBytes,
                                          const uint16_t            compressedHdrMaxSize);

/*!
        \fn r_hc_result_t R_HC_SetIphcHlimField(const uint8_t hopLimit,
                                                r_hc_hop_limit_field_t* hlimField)
        \brief The function sets the HLIM field of the base IPHC header
        \param[in]  hopLimit    The hop limit field of the IPv6 header
        \param[out] hlimField   Pointer to a location where the output will be written
        \return R_HC_RESULT_ILLEGAL_NULL_POINTER if any of the pointers passed to the function are NULL, R_HC_RESULT_SUCCESS in all other cases
 */
r_hc_result_t R_HC_SetIphcHlimField(const uint8_t           hopLimit,
                                    r_hc_hop_limit_field_t* hlimField);

/*!
        \fn r_hc_result_t R_HC_SetIphcCidSacDacField(const uint8_t* srcAddr,
                                                     const uint8_t* dstAddr,
                                                     const r_boolean_t m,
                                                     uint8_t* cidField,
                                                     r_iphc_context_id_ext_t* cidElems,
                                                     r_hc_address_compression_t* sacField,
                                                     r_hc_address_compression_t* dacField,
                                                     uint8_t* srcPrefixLength,
                                                     uint8_t* dstPrefixLength);
        \brief The function sets the CID, SAC and DAC fields of the base IPHC header
        \param[in]  srcAddr     Pointer to the IPv6 source address
        \param[in]  dstAddr     Pointer to the IPv6 destination address
        \param[in]  m           Flag set to R_TRUE if the destination address is a multicast address
        \param[out] cidField    CID field
        \param[out] cidElems    Pointer to structure where the CID elements will be written
        \param[out] sacField    Pointer to a memory location where the SAC field will be written
        \param[out] dacField    Pointer to a memory location where the DAC field will be written
        \param[out] srcPrefixLength Length of the prefix used by the source address; only valid for context based encoding
        \param[out] dstPrefixLength Length of the prefix used by the destination address; only valid for context based encoding
        \return R_HC_RESULT_ILLEGAL_NULL_POINTER if any of the pointers passed to the function are NULL, R_HC_RESULT_SUCCESS in all other cases
 */
r_hc_result_t R_HC_SetIphcCidSacDacField(const uint8_t*              srcAddr,
                                         const uint8_t*              dstAddr,
                                         const r_boolean_t           m,
                                         uint8_t*                    cidField,
                                         r_iphc_context_id_ext_t*    cidElems,
                                         r_hc_address_compression_t* sacField,
                                         r_hc_address_compression_t* dacField,
                                         uint8_t*                    srcPrefixLength,
                                         uint8_t*                    dstPrefixLength);

/*!
        \fn r_hc_result_t R_HC_SetStatelessAddressMode(const uint8_t* ipAddr,
                                                       r_hc_address_mode_t* samDam);
        \brief This helper function sets the SAM or DAM field of the base IPHC header for the stateless case
        \param[in]  ipAddr  Pointer to either the source or destination IPv6 address
        \param[out] samDam  Pointer to a location where the output will be written
        \return R_HC_RESULT_ILLEGAL_NULL_POINTER if any of the pointers passed to the function are NULL, R_HC_RESULT_SUCCESS in all other cases
 */
r_hc_result_t R_HC_SetStatelessAddressMode(const uint8_t*       ipAddr,
                                           r_hc_address_mode_t* samDam);

#if R_HC_IPHC_STATEFUL_COMPRESSION_ENABLED
/*!
        \fn r_hc_result_t R_HC_SetStatefullAddressMode(const uint8_t* ipAddr,
                                                       r_hc_address_mode_t* samDam,
                                                       const r_6lowpan_address_type_t addrMode,
                                                       const uint8_t* macAddress,
                                                       const uint8_t prefixLength);
        \brief This helper function sets the SAM or DAM field of the base IPHC header for the statefull case or sets the SAC or DAC back to stateless if needed
        \param[in]  ipAddr      Pointer to either the source or destination IPv6 address
        \param[out] samDam      Pointer to a location where the output will be written
        \param[in]  addrMode    The mode of the address passed in ipAddr. Either R_HC_ADDRESS_TYPE_SOURCE, R_HC_ADDRESS_TYPE_DESTINATION or R_HC_ADDRESS_TYPE_NONE
        \param[in]  macAddress  Pointer to the MAC address, can be NULL if R_HC_ADDRESS_TYPE_NONE is used
        \param[in]  prefixLength Length of the prefix in bits
        \return R_HC_RESULT_ILLEGAL_NULL_POINTER if any of the pointers passed to the function are NULL, R_HC_RESULT_FAILED if the addressType has a wrong value,
            R_HC_RESULT_SUCCESS in all other cases
 */
r_hc_result_t R_HC_SetStatefullAddressMode(const uint8_t*                 ipAddr,
                                           r_hc_address_mode_t*           samDam,
                                           const r_6lp_hdr_address_type_t addrMode,
                                           const uint8_t*                 macAddress,
                                           const uint8_t                  prefixLength);
#endif

/*!
        \fn r_hc_result_t R_HC_SetIphcMField(const uint8_t dstAddress[],
                        uint8_t* m);
        \brief The function sets the M field of the base IPHC header
        \param[in]  dstAddress  Pointer to the destination IPv6 address
        \param[out] m           Pointer to a location where the output will be written
        \return R_HC_RESULT_ILLEGAL_NULL_POINTER if any of the pointers passed to the function are NULL, R_HC_RESULT_SUCCESS in all other cases
 */
r_hc_result_t R_HC_SetIphcMField(const uint8_t dstAddress[],
                                 uint8_t*      m);

/*!
        \fn r_hc_result_t R_HC_SetIphcAddrModeField(const uint8_t ipAddress[],
                                                    const uint8_t* macAddress,
                                                    const r_6lowpan_address_type_t addrMode,
                                                    const r_boolean_t m,
                                                    const r_hc_address_compression_t sacDac,
                                                    r_hc_address_mode_t* samDam,
                                                    uint8_t prefixLength);
        \brief Top level function for setting the address mode field
        \param[in]  ipAddress   Pointer to the source or destination IPv6 address
        \param[in]  macAddress  Pointer to the MAC address of the device. Can be the long or short address
        \param[in]  addrMode    The MAC addressing mode used for sending this message
        \param[in]  m           The base IPHC header M field
        \param[in]  sacDac      Pointer to the base IPHC SAC or DAC field
        \param[out] samDam      Pointer to a location where the SAM or DAM will be written
        \param[in]  prefixLength Length in bits of the prefix used for the ip address
        \return R_HC_RESULT_ILLEGAL_NULL_POINTER if any of the pointers passed to the function are NULL, R_HC_RESULT_SUCCESS in all other cases
 */
r_hc_result_t R_HC_SetIphcAddrModeField(const uint8_t                    ipAddress[],
                                        const uint8_t*                   macAddress,
                                        const r_6lp_hdr_address_type_t   addrMode,
                                        const r_boolean_t                m,
                                        const r_hc_address_compression_t sacDac,
                                        r_hc_address_mode_t*             samDam,
                                        uint8_t                          prefixLength);

#endif /* R_HC_6282_COMP_HELPER_H */
