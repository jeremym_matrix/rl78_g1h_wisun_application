/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer*
* Copyright (C) 2011 Renesas Electronics Corporation. All rights reserved.
*******************************************************************************/

/*******************************************************************************
 * File Name   : r_hc_6282_comp_next_hdr.h
 * Version     : 1.0
 * Description : This module contains functions for the next header compression
 ******************************************************************************/

/*!
   \file      r_hc_6282_comp_next_hdr.h
   \version   1.0
   \brief     This module contains functions for the next header compression
 */

#ifndef R_HC_6282_COMP_NEXT_HDR_H
#define R_HC_6282_COMP_NEXT_HDR_H

/******************************************************************************
   Macro definitions
******************************************************************************/

/******************************************************************************
   Typedef definitions
******************************************************************************/

/******************************************************************************
   Exported global variables
******************************************************************************/

/******************************************************************************
   Exported global functions (to be accessed by other files)
******************************************************************************/

/*!
    \fn r_hc_result_t R_HC_CompressNextHeader(const uint8_t header[],
                                          r_hc_next_header_field_t* nhField,
                                          uint16_t* readBytes,
                                          uint8_t output[],
                                          uint16_t* writtenBytes,
                                          uint16_t* sumWrittenBytes,
                                          const uint8_t thisHdrType,
                                          const uint8_t nextHdrType,
                                          const uint16_t compressedHdrMaxSize);
    \brief The function performs the compression of the next header fields except for the UDP header
    \param[in] header Pointer to the next header
    \param[in] nhField Pointer to the type of the next header
    \param[out] readBytes Pointer to a memory location where the number of read bytes from the header location will be written
    \param[in] output Pointer to a location where the compressed header will be written
    \param[in] writtenBytes Pointer to a location where the number of bytes written to output by this function will be written
    \param[in] sumWrittenBytes Pointer to a location where the total number of bytes written to output will be incremented with writtenBytes
    \param[in] thisHdrType The type of this header
    \param[in] nextHdrType The type of the next header
    \param[in] compressedHdrMaxSize The maximum size that can be written at output
    \param[in] allowUdpCheckSumElide Allow eliding of UDP checksum
    \return R_HC_RESULT_ILLEGAL_NULL_POINTER if any of the pointers passed to the function are NULL,
            R_HC_RESULT_FAILED if any of the functions called by this function does not return R_HC_RESULT_SUCCESS and R_HC_RESULT_SUCCESS in all other cases
 */
r_hc_result_t R_HC_CompressNextHeader(const uint8_t             header[],
                                      r_hc_next_header_field_t* nhField,
                                      uint16_t*                 readBytes,
                                      uint8_t                   output[],
                                      uint16_t*                 writtenBytes,
                                      uint16_t*                 sumWrittenBytes,
                                      const uint8_t             thisHdrType,
                                      const uint8_t             nextHdrType,
                                      const uint16_t            compressedHdrMaxSize,
                                      const r_boolean_t         allowUdpCheckSumElide);

/*!
    \fn r_hc_result_t R_HC_CompressNextHeaderFields(const r_ipv6_hdr_t* ipv6Hdr,
                                            const r_hc_next_header_field_t nhField,
                                            const uint8_t* nhLocation,
                                            uint16_t* readBytes,
                                            uint8_t* output,
                                            uint16_t* sumWrittenBytes,
                                            uint16_t* uncompHdrSize,
                                            const uint16_t compressedHdrMaxSize,
                                            const r_boolean_t allowUdpCheckSumElide);
    \brief Top level function for the compression of the next header fields (<a href="../../eap_headerCompression/index.htm">Procedure described here</a>)
    \param[in] ipv6Hdr Pointer to an unpacked IPv6 header
    \param[in] nhField Type of the next header field
    \param[in] nhLocation Pointer to the next header
    \param[out] readBytes Pointer to a memory location where the number of read bytes from the header location will be written
    \param[in] output Pointer to a location where the compressed header will be written
    \param[in] sumWrittenBytes Pointer to a location where the total number of bytes written to output will be incremented with writtenBytes
    \param[in] uncompHdrSize Pointer to a location where the size of the uncompressed header will be written to
    \param[in] compressedHdrMaxSize The maximum size that can be written at output
    \param[in] allowUdpCheckSumElide flag set to R_TRUE if the function is allowed to elide the UDP checksum
    \return R_HC_RESULT_ILLEGAL_NULL_POINTER if any of the pointers passed to the function are NULL, R_HC_RESULT_BAD_INPUT_ARGUMENTS if input arguments have values that are not valid,
            R_HC_RESULT_FAILED or R_HC_RESULT_BUFFER_ERROR if any of the functions called by this function does not return R_HC_RESULT_SUCCESS and R_HC_RESULT_SUCCESS in all other cases
 */
r_hc_result_t R_HC_CompressNextHeaderFields(const r_ipv6_hdr_t*            ipv6Hdr,
                                            const r_hc_next_header_field_t nhField,
                                            const uint8_t*                 nhLocation,
                                            uint16_t*                      readBytes,
                                            uint8_t*                       output,
                                            uint16_t*                      sumWrittenBytes,
                                            uint16_t*                      uncompHdrSize,
                                            const uint16_t                 compressedHdrMaxSize,
                                            const r_boolean_t              allowUdpCheckSumElide);

/*!
    \fn r_hc_result_t R_HC_CompressUdpHeader(const r_ipv6_hdr_t* ipv6Hdr,
                                     const uint8_t* header,
                                     uint16_t* readBytes,
                                     uint8_t* output,
                                     uint16_t* writtenBytes,
                                     uint16_t* sumWrittenBytes,
                                     const uint16_t compressedHdrMaxSize,
                                     const r_boolean_t allowUdpCheckSumElide);
    \brief The function performs the compression of the UDP header
    \param[in] ipv6Hdr Pointer to an unpacked IPv6 header
    \param[in] header Pointer to a location where next header is located
    \param[out] readBytes Pointer to a memory location where the number of read bytes from the header location will be written
    \param[in] output Pointer to a location where the compressed header will be written
    \param[in] writtenBytes Pointer to a location where the number of bytes written to output by this function will be written
    \param[in] sumWrittenBytes Pointer to a location where the total number of bytes written to output will be incremented with writtenBytes
    \param[in] compressedHdrMaxSize The maximum size that can be written at output
    \param[in] allowUdpCheckSumElide flag set to R_TRUE if the function is allowed to elide the UDP checksum
    \return R_HC_RESULT_ILLEGAL_NULL_POINTER if any of the pointers passed to the function are NULL,
            R_HC_RESULT_FAILED or R_HC_RESULT_BUFFER_ERROR if any of the functions called by this function does not return R_HC_RESULT_SUCCESS and R_HC_RESULT_SUCCESS in all other cases
 */
r_hc_result_t R_HC_CompressUdpHeader(const r_ipv6_hdr_t* ipv6Hdr,
                                     const uint8_t*      header,
                                     uint16_t*           readBytes,
                                     uint8_t*            output,
                                     uint16_t*           writtenBytes,
                                     uint16_t*           sumWrittenBytes,
                                     const uint16_t      compressedHdrMaxSize,
                                     const r_boolean_t   allowUdpCheckSumElide);

#endif /* R_HC_6282_COMP_NEXT_HDR_H */
