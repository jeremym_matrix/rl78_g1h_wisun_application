/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer*
* Copyright (C) 2011 Renesas Electronics Corporation. All rights reserved.
*******************************************************************************/

/*******************************************************************************
 * File Name   : r_hc_6282_uncomp_address.h
 * Version     : 1.0
 * Description : Header file for stateless/stateful address uncompression
 ******************************************************************************/

/*!
   \file      r_hc_6282_uncomp_address.h
   \version   1.0
   \brief     Header file for stateless/stateful address uncompression
 */

#ifndef R_HC_6282_UNCOMP_ADDRESS_H
#define R_HC_6282_UNCOMP_ADDRESS_H

/******************************************************************************
   Macro definitions
******************************************************************************/

/******************************************************************************
   Typedef definitions
******************************************************************************/

/*!
    \enum r_hc_uncompress_address_location_t
    \brief Enumeration of address types for uncompression
 */
typedef enum
{
    R_HC_ADDRESS_LOCATION_SOURCE      = 0x00, //!< Source address uncompression
    R_HC_ADDRESS_LOCATION_DESTINATION = 0x01  //!< Destination address uncompression

} r_hc_uncompress_address_location_t;

/******************************************************************************
   Exported global variables
******************************************************************************/

/******************************************************************************
   Exported global functions (to be accessed by other files)
******************************************************************************/

/*!
    \fn r_hc_result_t R_HC_UncompressUnicastAddress(uint8_t ipv6Addr[],
                                            uint16_t* sumReadBytes,
                                            const uint8_t input[],
                                            const r_hc_address_compression_t addressCompression,
                                            const r_hc_address_mode_t addressMode,
                                            const uint8_t contextId,
                                            const uint8_t* macAddr,
                                            const r_6lowpan_address_type_t addrType,
                                            const r_hc_uncompress_address_location_t addrLocation);
    \brief This function performs uncompression of IPv6 unicast addresses following RFC6282. (<a href="../../eap_headerCompression/index.htm">Procedure described here</a>)
    \param[out] ipv6Addr Buffer where the uncompressed address is written to.
    \param[out] sumReadBytes Counter that will be incremented by the number of bytes read from input.
    \param[in] input Input vector containing in-line information provided for address uncompression.
    \param[in] addressCompression Indicates if stateless or stateful compression is used.
    \param[in] addressMode Indicates which parts of the address are provided in-line.
    \param[in] contextId Gives the ID of the context to be used when stateful compression is used.
    \param[in] macAddr Provides the address taken from encapsulating MAC header in case of address eliding.
    \param[in] addrType Indicates the kind of used MAC layer address (short, extended, none).
    \param[in] addrLocation Indicates if the address to be reconstructed is a SRC or DST address.
    \return R_HC_RESULT_SUCCESS, R_HC_RESULT_FAILED, R_HC_RESULT_BAD_INPUT_ARGUMENTS or R_HC_RESULT_ILLEGAL_NULL_POINTER
 */
r_hc_result_t R_HC_UncompressUnicastAddress(uint8_t                                  ipv6Addr[],
                                            uint16_t*                                sumReadBytes,
                                            const uint8_t                            input[],
                                            const r_hc_address_compression_t         addressCompression,
                                            const r_hc_address_mode_t                addressMode,
                                            const uint8_t                            contextId,
                                            const uint8_t*                           macAddr,
                                            const r_6lp_hdr_address_type_t           addrType,
                                            const r_hc_uncompress_address_location_t addrLocation);

/*!
    r_hc_result_t R_HC_UncompressMulticastAddress(uint8_t ipv6Addr[],
                                              uint16_t* sumReadBytes,
                                              const uint8_t input[],
                                              const r_hc_address_compression_t addressCompression,
                                              const r_hc_address_mode_t addressMode,
                                              const uint8_t contextId);
    \brief This function performs uncompression of IPv6 multicast addresses following RFC6282. (<a href="../../eap_headerCompression/index.htm">Procedure described here</a>)
    \param[out] ipv6Addr Buffer where the uncompressed address is written to.
    \param[out] sumReadBytes Counter that will be incremented by the number of bytes read from input.
    \param[in] input Input vector containing in-line information provided for address uncompression.
    \param[in] addressCompression Indicates if stateless or stateful compression is used.
    \param[in] addressMode Indicates which parts of the address are provided in-line.
    \param[in] contextId Gives the ID of the context to be used when stateful compression is used.
        \return R_HC_RESULT_SUCCESS, R_HC_RESULT_FAILED, R_HC_RESULT_BAD_INPUT_ARGUMENTS or R_HC_RESULT_ILLEGAL_NULL_POINTER
 */
r_hc_result_t R_HC_UncompressMulticastAddress(uint8_t                          ipv6Addr[],
                                              uint16_t*                        sumReadBytes,
                                              const uint8_t                    input[],
                                              const r_hc_address_compression_t addressCompression,
                                              const r_hc_address_mode_t        addressMode,
                                              const uint8_t                    contextId);

#endif /* R_HC_6282_UNCOMP_ADDRESS_H */
