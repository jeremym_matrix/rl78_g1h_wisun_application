/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer*
* Copyright (C) 2011 Renesas Electronics Corporation. All rights reserved.
*******************************************************************************/

/*******************************************************************************
 * File Name   : r_hc_6282_uncomp_next_hdr.h
 * Version     : 1.0
 * Description : Header file for next header uncompression
 ******************************************************************************/

/*!
   \file      r_hc_6282_uncomp_next_hdr.h
   \version   1.0
   \brief     Header file for next header uncompression
 */

#ifndef R_HC_6282_UNCOMP_NEXT_HDR_H
#define R_HC_6282_UNCOMP_NEXT_HDR_H

/******************************************************************************
   Macro definitions
******************************************************************************/

/******************************************************************************
   Typedef definitions
******************************************************************************/

/******************************************************************************
   Exported global variables
******************************************************************************/

/******************************************************************************
   Exported global functions (to be accessed by other files)
******************************************************************************/

/*!
    \fn r_hc_result_t R_HC_DecodeCompressedNextHeader(const uint8_t nhcField,
                                              uint8_t* hdrType,
                                              r_boolean_t* lastHdr);
    \brief This function decodes the next compressed header as described in RFC6282.
    \param[in] nhcField NHC field of the compressed header.
    \param[out] hdrType Type of the decoded header.
    \param[out] lastHdr Flag indicating if the decoded header is the last compressed header.
    \return R_HC_RESULT_SUCCESS, R_HC_RESULT_FAILED or R_HC_RESULT_ILLEGAL_NULL_POINTER
 */
r_hc_result_t R_HC_DecodeCompressedNextHeader(const uint8_t nhcField,
                                              uint8_t*      hdrType,
                                              r_boolean_t*  lastHdr);

/*!
    \fn r_hc_result_t R_HC_UncompressExtensionHeader(const uint8_t input[],
                                             uint8_t hdrOutBuffer[],
                                             uint16_t hdrOutBufferLength,
                                             uint16_t* sumReadBytes,
                                             uint16_t* sumWrittenBytes,
                                             const r_boolean_t lastHdr);
    \brief This function performs uncompression of IPv6 extension headers following RFC6282. (<a href="../../eap_headerCompression/index.htm">Procedure described here</a>)
    \param[in] input Input vector containing in-line information of extension headers.
    \param[out] hdrOutBuffer Memory location where the uncompressed IPv6 extension header will be written.
    \param[out] hdrOutBufferLength Length of the hdrOutBuffer.
    \param[out] sumReadBytes Counter that will be incremented by the number of bytes read from input.
    \param[out] sumWrittenBytes Counter that will be incremented by the number of bytes written to hdrOutBuffer.
    \param[in] lastHdr Flag indicating if the header to be uncompressed is the last compressed header.
    \return R_HC_RESULT_SUCCESS, R_HC_RESULT_FAILED, R_HC_RESULT_BAD_INPUT_ARGUMENTS or R_HC_RESULT_ILLEGAL_NULL_POINTER
 */
r_hc_result_t R_HC_UncompressExtensionHeader(const uint8_t     input[],
                                             uint8_t           hdrOutBuffer[],
                                             uint16_t          hdrOutBufferLength,
                                             uint16_t*         sumReadBytes,
                                             uint16_t*         sumWrittenBytes,
                                             const r_boolean_t lastHdr);

/*!
    \fn r_hc_result_t R_HC_UncompressUDPHeader(const uint8_t input[],
                                       uint16_t* sumReadBytes,
                                       r_hc_upd_uncomp_t* udpUncompMem,
                                       uint16_t* sumWrittenBytes,
                                       const uint16_t inLength,
                                       const r_boolean_t computeLength);
    \brief This function performs uncompression of UDP headers following RFC6282. (<a href="../../eap_headerCompression/index.htm">Procedure described here</a>)
    \param[in] input Input vector containing in-line information of udp header.
    \param[out] sumReadBytes Counter that will be incremented by the number of bytes read from input.
    \param[out] udpUncompMem Structure to temporarily store UDP header data for later reconstruction.
    \param[out] sumWrittenBytes Counter that will be incremented by the number of bytes written to hdrOutBuffer.
    \param[in] inLength Either length of UDP datagram (incl. header) or length of remaining input vector
    \param[in] computeLength Indicates if length has to be computed from remaining input vector or if it has to be set only.
    \return R_HC_RESULT_SUCCESS, R_HC_RESULT_FAILED, R_HC_RESULT_BAD_INPUT_ARGUMENTS or R_HC_RESULT_ILLEGAL_NULL_POINTER
 */
r_hc_result_t R_HC_UncompressUDPHeader(const uint8_t      input[],
                                       uint16_t*          sumReadBytes,
                                       r_hc_upd_uncomp_t* udpUncompMem,
                                       uint16_t*          sumWrittenBytes,
                                       const uint16_t     inLength,
                                       const r_boolean_t  computeLength);

#endif /* R_HC_6282_UNCOMP_NEXT_HDR_H */
