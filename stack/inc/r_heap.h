/**
   @file
   @version   1.0
   @brief     Heap header file
   @attention This file may not be included in other header files
   @details   This file contains only macros (no global symbols) to allow having short names.
 */

#ifndef R_HEAP_H
#define R_HEAP_H

#if R_DEV_USE_STDLIB_MALLOC
#include "stdlib.h"
#define r_alloc(heap, size)        malloc(size)
#define r_zalloc(heap, size)       calloc(1, size)
#define r_realloc(heap, ptr, size) realloc(ptr, size)
#define r_free(heap, ptr)          free(ptr)

#define my_alloc(size)             malloc(size)
#define my_zalloc(size)            calloc(1, size)
#define my_realloc(ptr, size)      realloc(ptr, size)
#define my_free(ptr)               free(ptr)
#elif R_DEV_USE_UMM_POISON
#include "umm_malloc.h"
#include "r_os_wrapper_config.h"

void* umm_poison_malloc(UMM_HEAP_ID_P_ size_t size);
void* umm_poison_calloc(UMM_HEAP_ID_P_ size_t num, size_t size);
void* umm_poison_realloc(UMM_HEAP_ID_P_ void* ptr, size_t size);
void  umm_poison_free(UMM_HEAP_ID_P_ void* ptr);
int   umm_poison_check(UMM_HEAP_ID_P);

#define UMM_HEAP_ID(heap)          ((heap) - R_OS_HEAP_BASEID)

#define r_alloc(heap, size)        umm_poison_malloc(UMM_HEAP_ID(heap), size)
#define r_zalloc(heap, size)       umm_poison_calloc(UMM_HEAP_ID(heap), 1, size)
#define r_realloc(heap, ptr, size) umm_poison_realloc(UMM_HEAP_ID(heap), ptr, size)
#define r_free(heap, ptr)          umm_poison_free(UMM_HEAP_ID(heap), ptr)

#define my_alloc(size)             umm_poison_malloc(UMM_HEAP_ID(MY_HEAP_ID), size)
#define my_zalloc(size)            umm_poison_calloc(UMM_HEAP_ID(MY_HEAP_ID), 1, size)
#define my_realloc(ptr, size)      umm_poison_realloc(UMM_HEAP_ID(MY_HEAP_ID), ptr, size)
#define my_free(ptr)               umm_poison_free(UMM_HEAP_ID(MY_HEAP_ID), ptr)
#else  /* if R_DEV_USE_STDLIB_MALLOC */
#include "umm_malloc.h"
#include "r_os_wrapper_config.h"

#define UMM_HEAP_ID(heap)          ((heap) - R_OS_HEAP_BASEID)

#define r_alloc(heap, size)        umm_malloc(UMM_HEAP_ID(heap), size)
#define r_zalloc(heap, size)       umm_calloc(UMM_HEAP_ID(heap), 1, size)
#define r_realloc(heap, ptr, size) umm_realloc(UMM_HEAP_ID(heap), ptr, size)
#define r_free(heap, ptr)          umm_free(UMM_HEAP_ID(heap), ptr)

#define my_alloc(size)             umm_malloc(UMM_HEAP_ID(MY_HEAP_ID), size)
#define my_zalloc(size)            umm_calloc(UMM_HEAP_ID(MY_HEAP_ID), 1, size)
#define my_realloc(ptr, size)      umm_realloc(UMM_HEAP_ID(MY_HEAP_ID), ptr, size)
#define my_free(ptr)               umm_free(UMM_HEAP_ID(MY_HEAP_ID), ptr)
#endif /* if R_DEV_USE_STDLIB_MALLOC */

#endif /* R_HEAP_H */
