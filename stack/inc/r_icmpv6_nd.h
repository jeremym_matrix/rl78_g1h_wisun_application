/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer*
* Copyright (C) 2011 Renesas Electronics Corporation. All rights reserved.
*******************************************************************************/

/*******************************************************************************
 * File Name    : r_icmpv6_nd.h
 * Version      : 1.00
 * Description  : Header for the module that implements the ICMPv6 message
                  creation and parsing for neighbor discovery
 ******************************************************************************/

/*!
   \file    r_icmpv6_nd.h
   \version 1.0
   \brief   Header for the module that implements the ICMPv6 message
            creation and parsing for neighbor discovery
 */

/*******************************************************************************
   User Includes (Project level includes)
*******************************************************************************/
#include "r_icmpv6.h"

/* Multi inclusion prevention macro */
#ifndef R_ICMPV6_ND_H
#define R_ICMPV6_ND_H

/******************************************************************************
   Defines
 *******************************************************************************/
#define R_ICMP_6CO_LENGTH                   (2)                  //!< Length of the 6LowPAN context option (x 8 bytes)
#define R_ICMP_ARO_LENGTH                   (2)                  //!< Length of the address registration option (x 8 bytes)

/* Field offsets */
#define R_ND_OPTION_LENGTH_FIELD_OFFSET     (1)  //!< Offset of the length field in an option message buffer
#define R_ND_OPTION_ARO_REG_LIFETIME_OFFSET (4)  //!< Offset of the registration lifetime field in an ARO option
#define R_ND_OPTION_ARO_EUI64_OFFSET        (6)  //!< Offset of the EUI-64 lifetime field in an ARO option

/******************************************************************************
   Message Type Definition
 *******************************************************************************/

/**
 * The common header of all ICMPv6 options
 */
typedef struct
{
    uint8_t type;   //!< Identifier of the type of option
    uint8_t length; //!< The length of the option (including the type and length fields) in units of 8 octets
} r_icmpv6_opt_header_t;

/**
 * ICMPv6 Address Registration Option (ARO) as defined in RFC 6775
 */
typedef struct
{
    r_icmpv6_opt_header_t header;                  //!< The common ICMPv6 option header
    uint8_t               status;                  //!< The status of a registration in the NA response. MUST be set to 0 in NS messages.
    uint8_t               reserved[3];             //!< MUST be initialized to zero by the sender and MUST be ignored by the receiver.
    uint8_t               registrationLifetime[2]; //!< The amount of time in a unit of 60 seconds that the router should retain the neighbor cache entry for the sender
    r_eui64_t             eui64;                   //!< Used to uniquely identify the interface of the registered address
} r_icmpv6_opt_aro_t;

/**
 * ICMPv6 Extended Address Registration Option (EARO) as defined in RFC 8505
 */
typedef struct
{
    r_icmpv6_opt_header_t header;                  //!< The common ICMPv6 option header
    uint8_t               status;                  //!< The status of a registration in the NA response. MUST be set to 0 in NS messages.
    uint8_t               opaque;                  //!< An octet opaque to ND. The 6LN MAY pass it transparently to another process. It MUST be set to 0 when not used.
    uint8_t               flags;                   //!< 'Rsvd', 'I', 'R', and 'T' flags
    uint8_t               tid;                     //!< A Transaction ID that is incremented with each transaction of one or more registrations
    uint8_t               registrationLifetime[2]; //!< The amount of time in a unit of 60 seconds that the router should retain the neighbor cache entry for the sender
    r_eui64_t             eui64;                   //!< Used to uniquely identify the interface of the registered address
} r_icmpv6_opt_earo_t;

#define R_ICMPV6_EARO_R_FLAG (2)
#define R_ICMPV6_EARO_T_FLAG (1)

/******************************************************************************
   Enumeration definitions
******************************************************************************/

/*!
    \enum r_icmp_option_nd_type_t
    \brief Enumeration type for the defined values of different ICMP options used in LoWPAN ND
 */
typedef enum
{
    R_ICMP_OPTION_ADDRESS_REGISTRATION      = 33, //!< Address Registration Option
    R_ICMP_OPTION_6LOWPAN_CONTEXT           = 34, //!< 6LOWPAN Context Option
    R_ICMP_OPTION_AUTHORATIVE_BORDER_ROUTER = 35  //!< Authorative Border Router Option

} r_icmp_option_nd_type_t;

/*!
    \enum r_aro_status_t
    \brief Enumeration type for the potential status values provided by an address registration option
 */
typedef enum
{
    R_ARO_STATUS_SUCCESS             = 0, //!< Address registration successful
    R_ARO_STATUS_DUPLICATE_ADDRESS   = 1, //!< Address was already registered by another node
    R_ARO_STATUS_NEIGHBOR_CACHE_FULL = 2, //!< Address could not be registered because neighbor cache is full
    R_ARO_STATUS_LAST
} r_aro_status_t;

/******************************************************************************
   Exported global functions (to be accessed by other files)
******************************************************************************/

void R_ICMP_AppendAddressRegistrationOption(r_iovec_class_t*     outIovec,
                                            const r_aro_status_t status,
                                            const uint16_t       registrationLifetime,
                                            const uint8_t* const eui64,
                                            uint8_t* const       buffer);

void R_ICMP_AppendExtendedAddressRegistrationOption(r_iovec_class_t*     outIovec,
                                                    const r_aro_status_t status,
                                                    const uint16_t       registrationLifetime,
                                                    uint8_t              flags,
                                                    uint8_t              transactionId,
                                                    const uint8_t* const eui64,
                                                    uint8_t* const       buffer);


#endif /* ifndef R_ICMPV6_ND_H */
