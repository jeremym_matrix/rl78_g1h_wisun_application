/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer*
* Copyright (C) 2011 Renesas Electronics Corporation. All rights reserved.
*******************************************************************************/

/*******************************************************************************
 * File Name    : r_icmp_process.h
 * Version      : 1.00
 * Description  : Header for the module for processing of ICMP messages
 ******************************************************************************/

/*!
   \file    r_icmpv6_process.h
   \version 1.0
   \brief   Header for the module for processing of ICMP messages
 */

#ifndef R_ICMPV6_PROCESS_H
#define R_ICMPV6_PROCESS_H

/******************************************************************************
   Includes   <System Includes> , "Project Includes"
******************************************************************************/

/******************************************************************************
   Typedef definitions
******************************************************************************/

/******************************************************************************
   Macro definitions
******************************************************************************/

/******************************************************************************
   Variable Externs
******************************************************************************/

/******************************************************************************
   Functions Prototypes
******************************************************************************/

/*!
    \brief ICMP message processing
    \param[in]  eui64       EUI-64 of the interface on which the message arrived
    \param[in]  ipAddress   Own IP address, to be used as source address
    \param[in]  p_ipv6Hdr   Pointer to the IPv6 header of the frame containing the ICMP message
    \param[in]  message     ICMP message that is to be processed
    \param[in]  length      Length of the ICMP message that is to be processed
    \param[in]  secured     Is the ICMP message encrypted
    \param[in]  lqi         LQI of the ICMP message
    \param[in]  vp_nwkGlobal Pointer to NWK Global
    \return R_ICMP_RESULT_SUCCESS, R_ICMP_RESULT_FAILED or R_ICMP_RESULT_NULL_POINTER
 */
r_icmp_result_t R_ICMP_Process(const uint8_t       eui64[],
                               const uint8_t       ipAddress[],
                               const r_ipv6_hdr_t* p_ipv6Hdr,
                               uint8_t             message[],
                               const uint16_t      length,
                               r_boolean_t         secured,
                               const uint8_t       lqi,
                               void*               vp_nwkGlobal); // void* -> r_nwk_cb_t*

#endif /* R_ICMPV6_PROCESS_H */
