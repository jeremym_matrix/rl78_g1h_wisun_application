/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer*
* Copyright (C) 2011 Renesas Electronics Corporation. All rights reserved.
*******************************************************************************/

/*******************************************************************************
 * File Name   : r_io_vec.h
 * Version     : 1.0
 * Description : This module implements input/output vector structures
 ******************************************************************************/

/*!
   @file
   @version   1.00
   @brief     This module implements input/output vector structures
 */

/*
 * Prevent nested inclusions
 */
#ifndef R_IO_VEC_H
#define R_IO_VEC_H

#include "r_nwk_api_base.h"

/******************************************************************************
   Macro definitions
******************************************************************************/

/*! Creates IO vector element */
#define IOVEC_CREATE(name, length) \
    r_iovec_element_t ioVecVector##name[length]; \
    r_iovec_class_t name = { 0 };

/*! Initializes IO vector element */
#define IOVEC_INIT(name) \
    (&name)->ioVec = ioVecVector##name; \
    (&name)->numElements = sizeof(ioVecVector##name) / sizeof(r_iovec_element_t); \
    ioVecReset(&name);

/******************************************************************************
   Typedef definitions
******************************************************************************/

/*!
    Structure for collecting pointers to data chunks
 */
typedef struct
{
    const uint8_t* address; //!< Address where the data starts
    uint32_t       length;  //!< Length of data in bytes
} r_iovec_element_t;

/*!
    Class structure for collecting pointers to data chunks
 */
typedef struct
{
    uint8_t            numElements;  //!< Maximum number of elements available
    uint8_t            usedElements; //!< Used number of elements in class
    uint32_t           totalLength;  //!< Total length in bytes in all elements
    r_iovec_element_t* ioVec;        //!< Actual ioVec Elements
} r_iovec_class_t;

/******************************************************************************
   Exported global variables
******************************************************************************/

/******************************************************************************
   Exported global functions (to be accessed by other files)
******************************************************************************/

/*!
    This function initializes the iovec class (setting pointers and lengths to NULL/zero)
    @param[in] ioVecClass Pointer to iovec class to be initialized
    @return None
 */
void ioVecReset(r_iovec_class_t* ioVecClass);

/*!
    This function sets a new element in an iovec class as long as elements are available
    The new element is appended after the last used element
    @param[in] ioVecClass Pointer to iovec class
    @param[in] address Pointer for the new element
    @param[in] length Length for the new element
    @return R_RESULT_SUCCESS when successfully written, R_RESULT_FAILED otherwise
 */
r_result_t ioVecAppendElement(r_iovec_class_t* ioVecClass,
                              const uint8_t*   address,
                              const uint16_t   length);

/*!
    This function sets a new element in an iovec class as long as elements are available
    The new element is prepended before the first used element
    @param[in] ioVecClass Pointer to iovec class
    @param[in] address Pointer for the new element
    @param[in] length Length for the new element
    @return R_RESULT_SUCCESS when successfully written, R_RESULT_FAILED otherwise
 */
r_result_t ioVecPrependElement(r_iovec_class_t* ioVecClass,
                               const uint8_t*   address,
                               const uint16_t   length);

/*!
    Remove the last element of an iovec
    @param[in] ioVecClass Pointer to iovec class
    @param[out] address If != NULL, set to the address of the removed element
    @param[out] length If != NULL, set to the length for the removed element
    @return R_RESULT_SUCCESS when successfully removed, R_RESULT_FAILED otherwise
 */
r_result_t ioVecPopElement(r_iovec_class_t* ioVecClass, const uint8_t** address, uint16_t* length);

/*!
    This function copies all ioVec elements of a class into a continuous memory provided by buffer
    @param[in]  ioVecClass Pointer to iovec class
    @param[out] buffer Pointer to continuous buffer
    @param[in]  bufferLength Length of continuous buffer
    @param[out] writtenBytes Number of bytes written to continuous buffer
    @return R_RESULT_SUCCESS when successfully written, R_RESULT_FAILED otherwise
 */
r_result_t ioVecToContinuous(const r_iovec_class_t* ioVecClass,
                             uint8_t*               buffer,
                             const uint32_t         bufferLength,
                             uint32_t*              writtenBytes);

/*!
    This function appends all elements of srcIoVecClass to dstIoVecClass, if a sufficient number of elements is
    available in dstIoVecClass
    @param[in]  srcIoVecClass Pointer to source iovec class
    @param[in]  dstIoVecClass Pointer to destination iovec class
    @return R_RESULT_SUCCESS when successfully copied, R_RESULT_FAILED otherwise
 */
r_result_t ioVecCopy(const r_iovec_class_t* srcIoVecClass,
                     r_iovec_class_t*       dstIoVecClass);

#endif /* R_IO_VEC_H */
