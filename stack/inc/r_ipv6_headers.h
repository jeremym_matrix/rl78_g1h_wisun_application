/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
* (c) 2020 Renesas Electronics Corporation All rights reserved.
******************************************************************************/

/**
 * @file r_ipv6_headers.h
 * @version 1.0
 * @brief Header for the module that implements the packing and unpacking of IPv6 headers
 */

#ifndef R_IPV6_HEADERS_H
#define R_IPV6_HEADERS_H

#include <stdint.h>
#include "r_nwk_api_base.h"
#include "r_udpv6.h"

/******************************************************************************
   Macro definitions
******************************************************************************/

/******************************************************************************
   Typedef definitions
******************************************************************************/

/**
 * IPv6 next header enumeration
 */
typedef enum
{
    R_IPV6_NEXT_HDR_HOP_BY_HOP  = 0x00, //!< Hop by hop options extension header
    R_IPV6_NEXT_HDR_TCP         = 0x06, //!< TCP header
    R_IPV6_NEXT_HDR_UDP         = 0x11, //!< UDP header
    R_IPV6_NEXT_HDR_IPV6        = 0x29, //!< IPv6 header
    R_IPV6_NEXT_HDR_ROUTING     = 0x2B, //!< Routing header
    R_IPV6_NEXT_HDR_FRAGMENT    = 0x2C, //!< Fragment header
    R_IPV6_NEXT_HDR_ESP         = 0x32, //!< Encap Security payload
    R_IPV6_NEXT_HDR_AH          = 0x33, //!< Authentication header
    R_IPV6_NEXT_HDR_ICMPV6      = 0x3A, //!< ICMPv6 header
    R_IPV6_NEXT_HDR_DST_OPTIONS = 0x3C, //!< Destination options
    R_IPV6_NEXT_HDR_NONE        = 0x3B, //!< No next header
    R_IPV6_NEXT_HDR_MOBILITY    = 0x87  //!< Mobility header

} r_ipv6_next_header_t;

#define R_IPV6_NEXT_HDR_OPT_RPL 0x63  //!< RPL Option
#define R_IPV6_NEXT_HDR_OPT_MPL 0x6D  //!< MPL Option

/**
 * IPv6 header structure
 */
typedef struct
{
    uint32_t flowLabel;     //!< Flow Label
    uint16_t payloadLength; //!< Length of the IPv6 payload, i.e., the rest of the packet following this IPv6 header, in bytes
    uint8_t  nextHdr;       //!< Next header type
    uint8_t  version;       //!< Version, must be set to 6
    uint8_t  trafficClass;  //!< Traffic class
    uint8_t  hopLimit;      //!< Hop limit
    uint8_t  src[16];       //!< 128 bit IPv6 source address
    uint8_t  dst[16];       //!< 128 bit IPv6 destination address
} r_ipv6_hdr_t;

typedef struct
{
    uint8_t  nextHdr;
    uint8_t  reserved_1;
    uint16_t fragOffset_8;
    uint8_t  mFlag;
    uint32_t identification;
} r_ipv6_exthdr_fragment_t;

/**
 * Traffic class encoding structure
 */
typedef struct
{
    uint8_t dscp; //!< DSCP field
    uint8_t ecn;  //!< ECN field
} r_ipv6_traffic_class_t;

/******************************************************************************
   Exported global variables
******************************************************************************/

/******************************************************************************
   Exported global functions (to be accessed by other files)
******************************************************************************/

/**
 * Serialize an IPv6 header structure to a byte buffer
 * @param ipv6Hdr Pointer to the IPv6 header structure that should be serialized
 * @param[out] output Output buffer to hold the serialized IPv6 header
 * @param[out] sumWrittenBytes The number of bytes written to the output buffer
 */
void R_IPV6_PackHeader(const r_ipv6_hdr_t* ipv6Hdr, uint8_t* output, uint16_t* sumWrittenBytes);

/**
 * Deserialize a byte buffer containing an IPv6 header to the corresponding structure
 * @param input Pointer to a buffer where the packed IPv6 header is located
 * @param[out] ipv6Hdr Output buffer to hold the unpacked IPv6 header
 */
void R_IPV6_UnpackHeader(const uint8_t* input, r_ipv6_hdr_t* ipv6Hdr);

/**
 * Serialize a traffic class header to a byte buffer
 * @param trafficClass Pointer to the traffic class header structure that should be serialized
 * @param[out] output Output buffer to hold the serialized traffic class header
 */
void R_IPV6_PackTrafficClassHeader(const r_ipv6_traffic_class_t* trafficClass, uint8_t output[]);

/**
 * Deserialize a byte buffer containing a traffic class header to the corresponding structure
 * @param input Pointer to a buffer where the packed traffic class header is located
 * @param[out] trafficClass Output buffer to hold the unpacked traffic class header
 */
void R_IPV6_UnpackTrafficClassHeader(const uint8_t input[], r_ipv6_traffic_class_t* trafficClass);

/**
 * Compute the UDP checksum over the specified input arguments
 * @param ipv6SrcAddr The IPv6 source address
 * @param ipv6DstAddr The IPv6 destination address
 * @param udpPayload The UDP payload
 * @param udpHdr The unpacked UDP header
 * @param[out] checkSum The computed UDP checksum
 * @return R_RESULT_SUCCESS if the checksum was successfully computed. Appropriate error code otherwise
 */
r_result_t R_IPV6_ComputeUdpCheckSum(const uint8_t*     ipv6SrcAddr,
                                     const uint8_t*     ipv6DstAddr,
                                     const uint8_t*     udpPayload,
                                     const r_udp_hdr_t* udpHdr,
                                     uint16_t*          checkSum);

/**
 * Build an IPv6 pseudo header that can be used for checksum calculations
 * @param p_ipv6Hdr Pointer to a real IPv6 header on which the pseudo-header is based
 * @param length Length of the data following the IPv6 header
 * @param[out] p_buffer Output buffer to hold the created pseudo header
 */
void R_IPV6_BuildPseudoHeader(const r_ipv6_hdr_t* p_ipv6Hdr, uint16_t length, uint8_t p_buffer[]);


r_result_t R_IPV6_CheckIpv6NextHeader(const r_ipv6_hdr_t* p_ipv6Hdr,
                                      const uint8_t*      p_rcvdIpPacket,
                                      uint16_t            rcvdIpPacketLen,
                                      uint8_t             nextHeader,
                                      const uint8_t*      p_data,
                                      uint16_t            dataLen,
                                      r_boolean_t         secured,
                                      void*               vp_nwkGlobal); // void* -> r_nwk_cb_t*

#endif /* R_IPV6_HEADERS_H */
