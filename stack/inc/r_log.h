/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
* (C) 2014-2015 Renesas Electronics Corporation All rights reserved.
*******************************************************************************/

/******************************************************************************
* File Name     : r_log.h
* Description   : Logging management functions
******************************************************************************/
/******************************************************************************
* History       : DD.MM.YYYY Version Description
*               : 30.01.2015 1.00    First Release
*******************************************************************************/

#ifndef R_LOG_H
#define R_LOG_H

#include <stddef.h>
#include <stdint.h>

/** The size of the RLog version ID in bytes */
#define R_LOG_VERSION_ID_SIZE 16

/*!
    \fn void R_LOG_Init(uint8_t* buffer, size_t size, uint8_t severityThreshold)
    \brief Initialize and activate the logging layer.
    \details This function is used to provide a buffer of arbitrary size to the logging layer that
     will be used as a ring buffer.
    \param buffer The buffer to be used.
    \param size The size of the buffer.
    \param severityThreshold The threshold used to filter logging messages.
    \return void
 */
void R_LOG_Init(uint8_t* buffer, size_t size, uint8_t severityThreshold);

/*!
    \fn void R_LOG_Reset()
    \brief Reset the logging layer by deleting all stored logging messages.
    \details This function clears the buffer but does not change any other settings, e.g., the
     severity threshold.
    \return void
 */
void R_LOG_Reset();


/*!
    \fn void R_LOG_SetSeverityThreshold(uint8_t severityThreshold)
    \brief Set the current severity threshold.
    \details This function changes the threshold for future logging messages but does not change
     the ones already stored.
    \param severityThreshold The new threshold used to filter logging messages.
    \return void
 */
void R_LOG_SetSeverityThreshold(uint8_t severityThreshold);


/*!
    \fn uint8_t R_LOG_GetSeverityThreshold()
    \brief Return the current severity threshold.
    \return The current severity threshold.
 */
uint8_t R_LOG_GetSeverityThreshold();

/*!
    \fn size_t R_LOG_GetLog(uint8_t* dest, size_t size)
    \brief Copy and delete (part of) the current logging buffer.
    \details This function copies up to size bytes from the logging buffer to dest
    and deletes the number of copied bytes from the logging buffer.
    \param dest The destination for copying the bytes from the logging buffer.
    \param size The maximum number of bytes to copy
    \return The number of bytes copied to dest (<= size) or 0 at the end of the buffer
 */
size_t R_LOG_GetLog(uint8_t* dest, size_t size);

/**
 * Retrieve the RLog version ID of this project. This ID is computed over all generated RLog resources (all log
 * functions and all configuration parameters and may be used to determine compatibility between this project and a
 * specific RLog decoder (cat or logcat)
 * @param[out] out The output buffer to hold the RLog version ID
 * @param outSize The size of the output buffer
 * @return The number of bytes written to the output buffer or 0 if the buffer was too small to hold the version ID
 */
size_t R_LOG_GetVersionID(uint8_t* out, size_t outSize);

/**
 * Append raw data to the internal log buffer assuming that it represents 1-n valid log entries
 * @warning The specified data must correspond to the log entry format expected by the logging framework and its current
 * configuration. Otherwise, the log buffer will be corrupted or the log decoder might crash. Use with caution!
 * @param data The data that should be appended to the log buffer
 * @param size The size of the data in bytes
 */
void R_LOG_AppendRawData(uint8_t* data, size_t size);

#endif /* R_LOG_H */
