/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
* (C) 2014-2015 Renesas Electronics Corporation All rights reserved.
*******************************************************************************/

/******************************************************************************
* File Name     : r_log_handler_internal.h
* Description   : The logging handler API used by generated functions
******************************************************************************/
/******************************************************************************
* History       : DD.MM.YYYY Version Description
*               : 30.01.2015 1.00    First Release
*******************************************************************************/

#ifndef R_LOG_HANDLER_INTERNAL_H
#define R_LOG_HANDLER_INTERNAL_H

#include "r_log_flags.h"

/*!
*  The logging handler API. This API may not be used outside the automatically generated logging
*  functions
*/

#if R_LOG_THRESHOLD > R_LOG_SEVERITY_OFF

#include <stddef.h>
#include "r_log_project.h"

/*! \defgroup r_log_handler_internal The internal API used by the generated logging function
    \note This API may only be used by the automatically generated logging functions
    @{
*/

/*!
    \fn int r_log_start(r_log_id_t id, unsigned char severity, size_t totalSize);
    \brief Indicate the start of a new log record.
    \details This function is used by the generated custom logging functions to indicate the start
     of a new log record. If the function returns 0, the log record may not be filled (e.g., because
     logging is disabled or this call is filtered out by the severity threshold). Otherwise, this
     function may be followed by an arbitrary number of calls to r_log_append() and one final call
     to r_log_finish()
    \note This function may not be used outside the automatically generated logging functions.
    \param id The unique ID of this logging statement used to map to the corresponding descriptor.
    \param severity The severity of the logging statement.
    \param totalSize The total size of the data added by the following r_log_append() calls.
    \return 0 if not successful and no calls to r_log_append() and r_log_finish() may follow
 */
int r_log_start(r_log_id_t id, unsigned char severity, size_t totalSize);

/*!
    \fn void r_log_append(const void* data, size_t size);
    \brief Append data to the current log record.
    \details This function is used by the generated custom logging functions to append data to the
     current log record. It may be followed by more calls to r_log_append() and must be followed by
     one final call to r_log_finish().
    \note This function may not be used outside the automatically generated logging functions.
    \param data Pointer to the buffer of data to be stored
    \param size The number of bytes to store
    \return void
 */
void r_log_append(const void* data, size_t size);

/*!
    \fn void r_log_finish();
    \brief Terminate the current log record.
    \details This function is used by the generated custom logging functions to terminate the
     current log record. No more calls to r_log_append() for the current record may follow.
    \note This function may not be used outside the automatically generated logging functions.
    \return void
 */
void r_log_finish();

#endif /* R_LOG_THRESHOLD > R_LOG_SEVERITY_OFF */

#endif /* R_LOG_HANDLER_INTERNAL_H */
