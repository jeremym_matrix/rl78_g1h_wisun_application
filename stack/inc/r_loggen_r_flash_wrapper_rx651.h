/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
* (C) 2014-2015 Renesas Electronics Corporation All rights reserved.
*******************************************************************************/

#ifndef R_LOGGEN_R_FLASH_WRAPPER_RX651_H
#define R_LOGGEN_R_FLASH_WRAPPER_RX651_H

/* THIS FILE IS AUTOMATICALLY GENERATED - DO NOT EDIT */

#include "r_log_internal.h"


#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR
static inline void r_loggen_170()
{
    R_LOG_DISABLE_CONTEXT_SWITCH();
    if (r_log_start(191, R_LOG_SEVERITY_ERR, 0))
    {
        r_log_finish();
    }
    R_LOG_ENABLE_CONTEXT_SWITCH();
}
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR */

#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR
static inline void r_loggen_174()
{
    R_LOG_DISABLE_CONTEXT_SWITCH();
    if (r_log_start(192, R_LOG_SEVERITY_ERR, 0))
    {
        r_log_finish();
    }
    R_LOG_ENABLE_CONTEXT_SWITCH();
}
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR */

#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR
static inline void r_loggen_178()
{
    R_LOG_DISABLE_CONTEXT_SWITCH();
    if (r_log_start(193, R_LOG_SEVERITY_ERR, 0))
    {
        r_log_finish();
    }
    R_LOG_ENABLE_CONTEXT_SWITCH();
}
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR */

#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR
static inline void r_loggen_182()
{
    R_LOG_DISABLE_CONTEXT_SWITCH();
    if (r_log_start(194, R_LOG_SEVERITY_ERR, 0))
    {
        r_log_finish();
    }
    R_LOG_ENABLE_CONTEXT_SWITCH();
}
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR */

#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
static inline void r_loggen_297()
{
    R_LOG_DISABLE_CONTEXT_SWITCH();
    if (r_log_start(195, R_LOG_SEVERITY_DBG, 0))
    {
        r_log_finish();
    }
    R_LOG_ENABLE_CONTEXT_SWITCH();
}
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */

#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR
static inline void r_loggen_305()
{
    R_LOG_DISABLE_CONTEXT_SWITCH();
    if (r_log_start(196, R_LOG_SEVERITY_ERR, 0))
    {
        r_log_finish();
    }
    R_LOG_ENABLE_CONTEXT_SWITCH();
}
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR */

#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR
static inline void r_loggen_330()
{
    R_LOG_DISABLE_CONTEXT_SWITCH();
    if (r_log_start(197, R_LOG_SEVERITY_ERR, 0))
    {
        r_log_finish();
    }
    R_LOG_ENABLE_CONTEXT_SWITCH();
}
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR */

#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
static inline void r_loggen_334()
{
    R_LOG_DISABLE_CONTEXT_SWITCH();
    if (r_log_start(198, R_LOG_SEVERITY_DBG, 0))
    {
        r_log_finish();
    }
    R_LOG_ENABLE_CONTEXT_SWITCH();
}
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */

#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR
static inline void r_loggen_345()
{
    R_LOG_DISABLE_CONTEXT_SWITCH();
    if (r_log_start(199, R_LOG_SEVERITY_ERR, 0))
    {
        r_log_finish();
    }
    R_LOG_ENABLE_CONTEXT_SWITCH();
}
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR */

#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
static inline void r_loggen_349()
{
    R_LOG_DISABLE_CONTEXT_SWITCH();
    if (r_log_start(200, R_LOG_SEVERITY_DBG, 0))
    {
        r_log_finish();
    }
    R_LOG_ENABLE_CONTEXT_SWITCH();
}
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */

#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
static inline void r_loggen_370()
{
    R_LOG_DISABLE_CONTEXT_SWITCH();
    if (r_log_start(201, R_LOG_SEVERITY_DBG, 0))
    {
        r_log_finish();
    }
    R_LOG_ENABLE_CONTEXT_SWITCH();
}
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */

#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR
static inline void r_loggen_443()
{
    R_LOG_DISABLE_CONTEXT_SWITCH();
    if (r_log_start(202, R_LOG_SEVERITY_ERR, 0))
    {
        r_log_finish();
    }
    R_LOG_ENABLE_CONTEXT_SWITCH();
}
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR */

#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
static inline void r_loggen_446()
{
    R_LOG_DISABLE_CONTEXT_SWITCH();
    if (r_log_start(203, R_LOG_SEVERITY_DBG, 0))
    {
        r_log_finish();
    }
    R_LOG_ENABLE_CONTEXT_SWITCH();
}
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */

#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR
static inline void r_loggen_465()
{
    R_LOG_DISABLE_CONTEXT_SWITCH();
    if (r_log_start(204, R_LOG_SEVERITY_ERR, 0))
    {
        r_log_finish();
    }
    R_LOG_ENABLE_CONTEXT_SWITCH();
}
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR */

#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
static inline void r_loggen_468()
{
    R_LOG_DISABLE_CONTEXT_SWITCH();
    if (r_log_start(205, R_LOG_SEVERITY_DBG, 0))
    {
        r_log_finish();
    }
    R_LOG_ENABLE_CONTEXT_SWITCH();
}
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */

#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
static inline void r_loggen_502()
{
    R_LOG_DISABLE_CONTEXT_SWITCH();
    if (r_log_start(206, R_LOG_SEVERITY_DBG, 0))
    {
        r_log_finish();
    }
    R_LOG_ENABLE_CONTEXT_SWITCH();
}
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */

#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
static inline void r_loggen_528(const uint8_t arg0, const uint8_t* arg1, const uint8_t arg2, const uint32_t arg3)
{
    R_LOG_DISABLE_CONTEXT_SWITCH();
    if (r_log_start(207, R_LOG_SEVERITY_DBG, sizeof(arg0) + 8 + sizeof(arg2) + sizeof(arg3)))
    {
        r_log_append(&arg0, sizeof(arg0));
        r_log_append(arg1, 8);
        r_log_append(&arg2, sizeof(arg2));
        r_log_append(&arg3, sizeof(arg3));
        r_log_finish();
    }
    R_LOG_ENABLE_CONTEXT_SWITCH();
}
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */

#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
static inline void r_loggen_543()
{
    R_LOG_DISABLE_CONTEXT_SWITCH();
    if (r_log_start(208, R_LOG_SEVERITY_DBG, 0))
    {
        r_log_finish();
    }
    R_LOG_ENABLE_CONTEXT_SWITCH();
}
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */

#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
static inline void r_loggen_582(const uint8_t arg0, const uint8_t* arg1, const uint8_t arg2, const uint32_t arg3)
{
    R_LOG_DISABLE_CONTEXT_SWITCH();
    if (r_log_start(209, R_LOG_SEVERITY_DBG, sizeof(arg0) + 8 + sizeof(arg2) + sizeof(arg3)))
    {
        r_log_append(&arg0, sizeof(arg0));
        r_log_append(arg1, 8);
        r_log_append(&arg2, sizeof(arg2));
        r_log_append(&arg3, sizeof(arg3));
        r_log_finish();
    }
    R_LOG_ENABLE_CONTEXT_SWITCH();
}
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */

#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR
static inline void r_loggen_586(const uint8_t arg0)
{
    R_LOG_DISABLE_CONTEXT_SWITCH();
    if (r_log_start(210, R_LOG_SEVERITY_ERR, sizeof(arg0)))
    {
        r_log_append(&arg0, sizeof(arg0));
        r_log_finish();
    }
    R_LOG_ENABLE_CONTEXT_SWITCH();
}
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR */

#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
static inline void r_loggen_600()
{
    R_LOG_DISABLE_CONTEXT_SWITCH();
    if (r_log_start(211, R_LOG_SEVERITY_DBG, 0))
    {
        r_log_finish();
    }
    R_LOG_ENABLE_CONTEXT_SWITCH();
}
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */

#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR
static inline void r_loggen_610(const uint8_t arg0)
{
    R_LOG_DISABLE_CONTEXT_SWITCH();
    if (r_log_start(212, R_LOG_SEVERITY_ERR, sizeof(arg0)))
    {
        r_log_append(&arg0, sizeof(arg0));
        r_log_finish();
    }
    R_LOG_ENABLE_CONTEXT_SWITCH();
}
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR */

#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR
static inline void r_loggen_638(const uint8_t arg0)
{
    R_LOG_DISABLE_CONTEXT_SWITCH();
    if (r_log_start(213, R_LOG_SEVERITY_ERR, sizeof(arg0)))
    {
        r_log_append(&arg0, sizeof(arg0));
        r_log_finish();
    }
    R_LOG_ENABLE_CONTEXT_SWITCH();
}
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR */

#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
static inline void r_loggen_641(const uint8_t arg0, const uint8_t* arg1, const uint8_t arg2, const uint32_t arg3)
{
    R_LOG_DISABLE_CONTEXT_SWITCH();
    if (r_log_start(214, R_LOG_SEVERITY_DBG, sizeof(arg0) + 8 + sizeof(arg2) + sizeof(arg3)))
    {
        r_log_append(&arg0, sizeof(arg0));
        r_log_append(arg1, 8);
        r_log_append(&arg2, sizeof(arg2));
        r_log_append(&arg3, sizeof(arg3));
        r_log_finish();
    }
    R_LOG_ENABLE_CONTEXT_SWITCH();
}
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */

#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
static inline void r_loggen_665()
{
    R_LOG_DISABLE_CONTEXT_SWITCH();
    if (r_log_start(215, R_LOG_SEVERITY_DBG, 0))
    {
        r_log_finish();
    }
    R_LOG_ENABLE_CONTEXT_SWITCH();
}
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */

#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
static inline void r_loggen_684(const uint8_t arg0, const uint8_t* arg1, const uint8_t arg2, const uint32_t arg3)
{
    R_LOG_DISABLE_CONTEXT_SWITCH();
    if (r_log_start(216, R_LOG_SEVERITY_DBG, sizeof(arg0) + 8 + sizeof(arg2) + sizeof(arg3)))
    {
        r_log_append(&arg0, sizeof(arg0));
        r_log_append(arg1, 8);
        r_log_append(&arg2, sizeof(arg2));
        r_log_append(&arg3, sizeof(arg3));
        r_log_finish();
    }
    R_LOG_ENABLE_CONTEXT_SWITCH();
}
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */

#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
static inline void r_loggen_699()
{
    R_LOG_DISABLE_CONTEXT_SWITCH();
    if (r_log_start(217, R_LOG_SEVERITY_DBG, 0))
    {
        r_log_finish();
    }
    R_LOG_ENABLE_CONTEXT_SWITCH();
}
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */

#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
static inline void r_loggen_732(const uint8_t arg0, const uint8_t* arg1, const uint8_t arg2, const uint32_t arg3)
{
    R_LOG_DISABLE_CONTEXT_SWITCH();
    if (r_log_start(218, R_LOG_SEVERITY_DBG, sizeof(arg0) + 8 + sizeof(arg2) + sizeof(arg3)))
    {
        r_log_append(&arg0, sizeof(arg0));
        r_log_append(arg1, 8);
        r_log_append(&arg2, sizeof(arg2));
        r_log_append(&arg3, sizeof(arg3));
        r_log_finish();
    }
    R_LOG_ENABLE_CONTEXT_SWITCH();
}
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */

#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR
static inline void r_loggen_736(const uint8_t arg0)
{
    R_LOG_DISABLE_CONTEXT_SWITCH();
    if (r_log_start(219, R_LOG_SEVERITY_ERR, sizeof(arg0)))
    {
        r_log_append(&arg0, sizeof(arg0));
        r_log_finish();
    }
    R_LOG_ENABLE_CONTEXT_SWITCH();
}
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR */

#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
static inline void r_loggen_750()
{
    R_LOG_DISABLE_CONTEXT_SWITCH();
    if (r_log_start(220, R_LOG_SEVERITY_DBG, 0))
    {
        r_log_finish();
    }
    R_LOG_ENABLE_CONTEXT_SWITCH();
}
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */

#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR
static inline void r_loggen_759(const uint8_t arg0)
{
    R_LOG_DISABLE_CONTEXT_SWITCH();
    if (r_log_start(221, R_LOG_SEVERITY_ERR, sizeof(arg0)))
    {
        r_log_append(&arg0, sizeof(arg0));
        r_log_finish();
    }
    R_LOG_ENABLE_CONTEXT_SWITCH();
}
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR */

#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR
static inline void r_loggen_780(const uint8_t arg0)
{
    R_LOG_DISABLE_CONTEXT_SWITCH();
    if (r_log_start(222, R_LOG_SEVERITY_ERR, sizeof(arg0)))
    {
        r_log_append(&arg0, sizeof(arg0));
        r_log_finish();
    }
    R_LOG_ENABLE_CONTEXT_SWITCH();
}
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR */

#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
static inline void r_loggen_783(const uint8_t arg0, const uint8_t* arg1, const uint8_t arg2, const uint32_t arg3)
{
    R_LOG_DISABLE_CONTEXT_SWITCH();
    if (r_log_start(223, R_LOG_SEVERITY_DBG, sizeof(arg0) + 8 + sizeof(arg2) + sizeof(arg3)))
    {
        r_log_append(&arg0, sizeof(arg0));
        r_log_append(arg1, 8);
        r_log_append(&arg2, sizeof(arg2));
        r_log_append(&arg3, sizeof(arg3));
        r_log_finish();
    }
    R_LOG_ENABLE_CONTEXT_SWITCH();
}
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */

#endif /* R_LOGGEN_R_FLASH_WRAPPER_RX651_H */
