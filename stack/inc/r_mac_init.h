/******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized.
* This software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES
* REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY,
* INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
* PARTICULAR PURPOSE AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY
* DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES
* FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS
* AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this
* software and to discontinue the availability of this software.
* By using this software, you agree to the additional terms and
* conditions found by accessing the following link:
* http://www.renesas.com/disclaimer
******************************************************************************/

/* Copyright (C) 2011 Renesas Electronics Corporation. All rights reserved.  */

/******************************************************************************
* File Name    :
* Version      : 1.00
* Description  :
******************************************************************************
* History : DD.MM.YYYY Version Description
*         : 07.02.2011 1.00    First Release
******************************************************************************/

/*!
   \file
   \version   1.0
   \brief
 */

#ifndef _R_MAC_INIT_H
#define _R_MAC_INIT_H

/******************************************************************************
   Includes   <System Includes> , "Project Includes"
******************************************************************************/

/******************************************************************************
   Typedef definitions
******************************************************************************/

/******************************************************************************
   Macro definitions
******************************************************************************/

/* Short address related constants */
#define R_MAC_SHORT_ADDRESS (0xFFFEu)  //!< Short address that MAC would need/accept for using extended addressing */

/******************************************************************************
   Variable Externs
******************************************************************************/


/******************************************************************************
   Functions Prototypes
******************************************************************************/

void R_MAC_Init(void* vp_nwkGlobal);  // void* -> r_nwk_cb_t*
void R_MAC_Reset(void* vp_nwkGlobal); // void* -> r_nwk_cb_t*

r_result_t R_MAC_Start(void*              vp_nwkGlobal,
                       r_nwk_start_req_t* p_request);

r_result_t R_MAC_Associate(void*             vp_nwkGlobal,
                           r_nwk_join_req_t* p_request);

#endif /* _R_MAC_INIT_H */
