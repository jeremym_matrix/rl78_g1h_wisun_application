/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer*
* Copyright (C) 2020 Renesas Electronics Corporation. All rights reserved.
*******************************************************************************/

/**
 * @file      r_nwk_mac_nbr_api.h
 * @version   1.0
 * @brief     API to access to the MAC neighbor management from within the NWK task
 */

#ifdef R_HYBRID_PLC_RF
#include "r_typedefs.h"
#else
#include "r_nwk_api_base.h"
#endif

#ifndef R_NWK_MAC_NBR_WRAPPER_H
#define R_NWK_MAC_NBR_WRAPPER_H

/**
* Add a lock for the specified MAC address to protect the corresponding neighbor cache entry from eviction
* @details A MAC address will remain locked until all locks are removed (i.e. the number of locks reaches zero)
* @param macAddr The MAC address of the neighbor cache entry for which the lock should be added
* @param vp_nwkGlobal The global NWK information structure
* @return R_RESULT_SUCCESS if the lock was successfully added to the MAC cache. Appropriate error code otherwise
*/
r_result_t R_MAC_LockNeighbor(const r_eui64_t* macAddr, void* vp_nwkGlobal);

/**
 * Remove a lock for the specified MAC address that protects the corresponding neighbor cache entry from eviction
 * @details A MAC address will remain locked until all locks are removed (i.e. the number of locks reaches zero)
 * @param macAddr The MAC address of the neighbor cache entry for which the lock should be removed
 * @param vp_nwkGlobal The global NWK information structure
 * @return R_RESULT_SUCCESS if the lock was successfully removed from the MAC cache. Appropriate error code otherwise
 */
r_result_t R_MAC_UnlockNeighbor(const r_eui64_t* macAddr, void* vp_nwkGlobal);

#endif /* R_NWK_MAC_NBR_WRAPPER_H */
