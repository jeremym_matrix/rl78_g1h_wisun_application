/******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized.
* This software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES
* REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY,
* INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
* PARTICULAR PURPOSE AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY
* DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES
* FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS
* AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this
* software and to discontinue the availability of this software.
* By using this software, you agree to the additional terms and
* conditions found by accessing the following link:
* http://www.renesas.com/disclaimer
******************************************************************************/

/* Copyright (C) 2014 Renesas Electronics Corporation. All rights reserved.  */

/******************************************************************************
* File Name    :
* Version      : 1.00
* Description  :
******************************************************************************
* History : DD.MM.YYYY Version Description
*         : 07.02.2011 1.00    First Release
******************************************************************************/

/**
   @file
   @version   1.0
   @brief     This module provides unified OS services access
 */

#ifndef _R_NWK_OS_WRAPPER_H
#define _R_NWK_OS_WRAPPER_H

/******************************************************************************
   Includes   <System Includes> , "Project Includes"
******************************************************************************/
#ifdef R_HYBRID_PLC_RF
#include "r_typedefs.h"
#else
#include "r_nwk_api_base.h"
#endif
#include "r_os_wrapper.h"

/******************************************************************************
   Function Prototypes
******************************************************************************/

/**
 * Receive OS message from OS event with timeout
 * @param[out] pp_msg Pointer to OS message where to write the message got from OS event
 * @param mbxIdOS mailbox ID where to get the message from
 * @param timeOutMsTimeout in ms to wait for the OS event from the OS event
 * @return R_RESULT_SUCCESS, R_RESULT_INVALID_PARAMETER or R_RESULT_FAIL
 */
r_result_t R_NWK_OS_ReceiveMsg(r_os_msg_t* pp_msg, r_os_id_t mbxId, uint32_t timeOutMs);

/**
 * Send OS message to OS mailbox (includes support for the ROA MAC mailbox using ::RoaSndMsg)
 * @param msgOS message to send to OS event
 * @param mbxIdOS mailbox ID where to send the message to
 */
void R_NWK_OS_SendMsg(void* msg, r_os_id_t mbxId);

/**
 * Send OS message to OS mailbox from ISR (includes support for the ROA MAC mailbox using ::RoaSndMsgFromISR)
 * @param msgOS message to send to OS event
 * @param mbxIdOS mailbox ID where to send the message to
 */
void R_NWK_OS_SendMsgFromISR(void* msg, const r_os_id_t mbxId);

/**
 * Allocate a message on the specified heap
 * @param memId OS memory ID from where to get the memory block
 * @param size the size of the new message
 * @return pointer to the newly allocated message (::r_os_msg_hdr_t) or NULL if out of memory
 */
void* R_NWK_OS_AllocMsg(r_os_id_t memId, size_t size);

/**
 * Release memory of OS message
 * @param msg pointer to OS message to be freed
 */
void R_NWK_OS_MsgFree(void* msg);

/**
 * Delay execution of OS task
 * @param delayMsDelay in ms for the OS task
 */
void R_NWK_OS_DelayTaskMs(uint32_t delayMs);

/**
 * Get task Id
 * @return Task Id
 */
r_os_id_t R_NWK_OS_GetTaskId(void);


/**
 * Sleep task
 * @return R_RESULT_SUCCESS, R_RESULT_FAILED
 */
r_result_t R_NWK_OS_Sleep(void);


/**
 * Wakeup task
 * @return R_RESULT_SUCCESS, R_RESULT_FAILED
 */
r_result_t R_NWK_OS_WakeupTask(r_os_id_t tskId);

/**
 * Start task
 */
void R_NWK_OS_StartTask(r_os_id_t tskId);

/**
 * Start cyclic task
 */
void R_NWK_OS_StartCyclic(r_os_id_t cycId);

/**
 * Stop cyclic task
 */
void R_NWK_OS_StopCyclic(r_os_id_t cycId);

#endif /* _R_NWK_OS_WRAPPER_H */
