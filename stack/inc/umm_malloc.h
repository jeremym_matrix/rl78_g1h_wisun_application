/* ----------------------------------------------------------------------------
 * umm_malloc.h - a memory allocator for embedded systems (microcontrollers)
 *
 * See copyright notice in LICENSE.TXT
 * ----------------------------------------------------------------------------
 */

#ifndef UMM_MALLOC_H
#define UMM_MALLOC_H

// REE: always use multiple heaps
#ifndef UMM_MULTIPLE_HEAPS
#define UMM_MULTIPLE_HEAPS
#include <stddef.h>
#endif

#include "umm_malloc_cfg.h"

/* ------------------------------------------------------------------------ */

void  umm_init(UMM_HEAP_ID_P);
void* umm_malloc(UMM_HEAP_ID_P_ size_t size);
void* umm_calloc(UMM_HEAP_ID_P_ size_t num, size_t size);
void* umm_realloc(UMM_HEAP_ID_P_ void* ptr, size_t size);
void  umm_free(UMM_HEAP_ID_P_ void* ptr);


/* ------------------------------------------------------------------------ */

/*
 * -D UMM_INFO :
 *
 * Enables a dump of the heap contents and a function to return the total
 * heap size that is unallocated - note this is not the same as the largest
 * unallocated block on the heap!
 */

#ifdef UMM_INFO
typedef struct UMM_HEAP_INFO_t
{
    unsigned short int totalEntries;
    unsigned short int usedEntries;
    unsigned short int freeEntries;

    unsigned short int totalBlocks;
    unsigned short int usedBlocks;
    unsigned short int freeBlocks;

    unsigned short int maxFreeContiguousBlocks;
    unsigned short int maxAllocatedContiguousBlocks;
} UMM_HEAP_INFO;

extern UMM_HEAP_INFO ummHeapInfo;

typedef struct UMM_INFO_WATERMARK_t
{
    unsigned short int watermark;
    unsigned short int currentLevel;
} UMM_INFO_WATERMARK;

#ifndef MAX
#define MAX(a, b) ((a < b) ? b : a)
#endif

#ifdef UMM_MULTIPLE_HEAPS
extern UMM_INFO_WATERMARK ummWatermark[UMM_MALLOC_CFG_HEAP_COUNT];  // Number of allocated blocks per heap
#else
extern UMM_INFO_WATERMARK ummWatermark;
#endif // UMM_MULTIPLE_HEAPS

void*              umm_info(UMM_HEAP_ID_P_ void* ptr, int force);
size_t             umm_free_heap_size(UMM_HEAP_ID_P);
size_t             umm_heap_size(UMM_HEAP_ID_P);
size_t             umm_high_watermark(UMM_HEAP_ID_P);
unsigned short int umm_map(UMM_HEAP_ID_P_ unsigned short int startBlockNo, int* freeFlag, size_t* blockSize);

#endif /* ifdef UMM_INFO */

/* ------------------------------------------------------------------------ */

#endif /* UMM_MALLOC_H */
