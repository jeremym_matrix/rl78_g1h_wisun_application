/*
 * Configuration for umm_malloc
 */

#ifndef _UMM_MALLOC_CFG_H
#define _UMM_MALLOC_CFG_H

/*
 * There are a number of defines you can set at compile time that affect how
 * the memory allocator will operate.
 * You can set them in your config file umm_malloc_cfg.h.
 * In GNU C, you also can set these compile time defines like this:
 *
 * -D UMM_TEST_MAIN
 *
 * Set this if you want to compile in the test suite
 *
 * -D UMM_BEST_FIT (default)
 *
 * Set this if you want to use a best-fit algorithm for allocating new
 * blocks
 *
 * -D UMM_FIRST_FIT
 *
 * Set this if you want to use a first-fit algorithm for allocating new
 * blocks
 *
 * -D UMM_DBG_LOG_LEVEL=n
 *
 * Set n to a value from 0 to 6 depending on how verbose you want the debug
 * log to be
 *
 * ----------------------------------------------------------------------------
 *
 * Support for this library in a multitasking environment is provided when
 * you add bodies to the UMM_CRITICAL_ENTRY and UMM_CRITICAL_EXIT macros
 * (see below)
 *
 * ----------------------------------------------------------------------------
 */

#include <stddef.h>
#include <stdint.h>
#include "r_os_wrapper_config.h"
#include "r_os_wrapper_internal.h"

/* support multiple heaps */
#ifdef UMM_MULTIPLE_HEAPS

#define UMM_HEAP_ID_P  unsigned int heap_id /* a heap as sole parameter in a declaration */
#define UMM_HEAP_ID_P_ UMM_HEAP_ID_P,       /* a heap as first of multiple parameters */
#define UMM_HEAP_ID_A  heap_id              /* a heap as sole argument to a function call */
#define UMM_HEAP_ID_A_ UMM_HEAP_ID_A,       /* a heap as first of multiple arguments */

#else

#define UMM_HEAP_ID_P void
#define UMM_HEAP_ID_P_
#define UMM_HEAP_ID_A
#define UMM_HEAP_ID_A_

#endif

#define UMM_MALLOC_CFG_HEAP_ADDR  r_os_wrapper_internal_heap_addr
#define UMM_MALLOC_CFG_HEAP_SIZE  r_os_wrapper_internal_heap_size
#define UMM_MALLOC_CFG_HEAP_COUNT R_HEAP_COUNT

/* A couple of macros to make packing structures less compiler dependent */

#define UMM_H_ATTPACKPRE
#if __GNUC__
#define UMM_H_ATTPACKSUF __attribute__ ((__packed__))
#else
#define UMM_H_ATTPACKSUF
#endif

#define UMM_BEST_FIT
#undef  UMM_FIRST_FIT

/*
 * A couple of macros to make it easier to protect the memory allocator
 * in a multitasking system. You should set these macros up to use whatever
 * your system uses for this purpose. You can disable interrupts entirely, or
 * just disable task switching - it's up to you
 *
 * NOTE WELL that these macros MUST be allowed to nest, because umm_free() is
 * called from within umm_malloc()
 */

#define UMM_CRITICAL_ENTRY() MEM_ENTER_CRITICAL()
#define UMM_CRITICAL_EXIT()  MEM_EXIT_CRITICAL()

/*
 * -D UMM_INTEGRITY_CHECK :
 *
 * Enables heap integrity check before any heap operation. It affects
 * performance, but does NOT consume extra memory.
 *
 * If integrity violation is detected, the message is printed and user-provided
 * callback is called: `UMM_HEAP_CORRUPTION_CB()`
 *
 * Note that not all buffer overruns are detected: each buffer is aligned by
 * 4 bytes, so there might be some trailing "extra" bytes which are not checked
 * for corruption.
 */

/*
#define UMM_INTEGRITY_CHECK
*/

#ifdef UMM_INTEGRITY_CHECK
int umm_integrity_check(UMM_HEAP_ID_P);
#define INTEGRITY_CHECK()        umm_integrity_check(UMM_HEAP_ID_A)
extern void umm_corruption(void);
#define UMM_HEAP_CORRUPTION_CB() printf("Heap Corruption!")
#else
#define INTEGRITY_CHECK()        0
#endif

/*
 * -D UMM_POISON :
 *
 * Enables heap poisoning: add predefined value (poison) before and after each
 * allocation, and check before each heap operation that no poison is
 * corrupted.
 *
 * Other than the poison itself, we need to store exact user-requested length
 * for each buffer, so that overrun by just 1 byte will be always noticed.
 *
 * Customizations:
 *
 *    UMM_POISON_SIZE_BEFORE:
 *      Number of poison bytes before each block, e.g. 2
 *    UMM_POISON_SIZE_AFTER:
 *      Number of poison bytes after each block e.g. 2
 *    UMM_POISONED_BLOCK_LEN_TYPE
 *      Type of the exact buffer length, e.g. `short`
 *
 * NOTE: each allocated buffer is aligned by 4 bytes. But when poisoning is
 * enabled, actual pointer returned to user is shifted by
 * `(sizeof(UMM_POISONED_BLOCK_LEN_TYPE) + UMM_POISON_SIZE_BEFORE)`.
 * It's your responsibility to make resulting pointers aligned appropriately.
 *
 * If poison corruption is detected, the message is printed and user-provided
 * callback is called: `UMM_HEAP_CORRUPTION_CB()`
 */

/*
#define UMM_POISON_CHECK
*/
#if R_DEV_USE_UMM_POISON
#include <stdlib.h>
#define UMM_POISON_CHECK            1
#endif

#define UMM_POISON_SIZE_BEFORE      4
#define UMM_POISON_SIZE_AFTER       4
#define UMM_POISONED_BLOCK_LEN_TYPE uint32_t

#ifdef UMM_POISON_CHECK
void* umm_poison_malloc(UMM_HEAP_ID_P_ size_t size);
void* umm_poison_calloc(UMM_HEAP_ID_P_ size_t num, size_t size);
void* umm_poison_realloc(UMM_HEAP_ID_P_ void* ptr, size_t size);
void  umm_poison_free(UMM_HEAP_ID_P_ void* ptr);
int   umm_poison_check(UMM_HEAP_ID_P);
#define POISON_CHECK() umm_poison_check(UMM_HEAP_ID_A)
#else
#define POISON_CHECK() 0
#endif

#endif /* _UMM_MALLOC_CFG_H */
