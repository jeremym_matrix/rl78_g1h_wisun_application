/***********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products.
 * No other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all 
 * applicable laws, including copyright laws. 
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING THIS SOFTWARE, WHETHER EXPRESS, 
 * IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE 
 * AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
 * TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS 
 * AFFILIATED COMPANIES SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR 
 * ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of 
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the 
 * following link:
 * http://www.renesas.com/disclaimer 
 **********************************************************************************************************************/
/***********************************************************************************************************************
 * file name	: phy.h
 * description	: This is the RF driver's header code.
 ***********************************************************************************************************************
 * Copyright (C) 2014-2021 Renesas Electronics Corporation.
 **********************************************************************************************************************/
#ifndef __PHY_H__
#define __PHY_H__
/***************************************************************************************************************
 * includes
 **************************************************************************************************************/
#ifdef RP_WISUN_FAN_STACK
	#include "r_stdint.h"
#else
	#include "r_types.h"
#endif

#if defined(__CCRL__) && !defined(__FAR_ROM__)
	#define memcpy(s1, s2, n)	_COM_memcpy(s1, s2, n);
#endif

/***************************************************************************************************************
 * configuration
 **************************************************************************************************************/
#define R_FREQUENCY_OFFSET_ENABLED
// #define R_FSB_FAN_ENABLED

// #define RP_ENABLE_TxTotalTimeLimitControl	// under development
// #define RP_ENABLE_LimitTimerControlSwitch	// for UserF

// #define RP_DEBUG_WRITE_MONITOR
// #define RP_LOG_IRQ
// #define RP_LOG_EVENT
#if defined(RP_LOG_IRQ) || defined(RP_LOG_EVENT)
	#define RP_LOG_NUMBER	(100)
#endif // #if defined(RP_LOG_IRQ) || defined(RP_LOG_EVENT)

#if defined(__RL78__) || defined(__RX)
	#if !defined(RP_DMA_WRITE_RAM_ENA)
		#define RP_DMA_WRITE_RAM_ENA	(1)
	#endif
	#if !defined(RP_DMA_READ_RAM_ENA)
		#define RP_DMA_READ_RAM_ENA		(1)
	#endif
#endif

#ifndef RP_aMaxPHYPacketSize
	#define	RP_aMaxPHYPacketSize	(255)	// octes include FCS length
#endif

#ifndef RP_aMinPHYPacketSize
	#define RP_aMinPHYPacketSize	(5)
#endif

#ifndef RP_MAX_NUM_TX_LMT_BUF
	#define RP_MAX_NUM_TX_LMT_BUF	(61)
#endif

#ifdef RP_ST_ENV
	#define RP_ENABLE_TxTotalTimeLimitControl
#endif // #ifdef RP_ST_ENV

/***************************************************************************************************************
 * macro definitions
 **************************************************************************************************************/
#if !defined(RP_FAR)
	#if defined(__RL78__)
		#define RP_FAR		__far
	#elif defined(__RX)
		#define RP_FAR		_far
	#elif defined(__arm)
		#define RP_FAR
	#endif
#endif

#ifndef RP_TRUE
	#define	RP_TRUE		(1)
#endif
#ifndef RP_FALSE
	#define	RP_FALSE	(0)
#endif
#ifndef RP_NULL
	#define RP_NULL 	(0)
#endif
#define RP_ERROR		(0xFF)	// Error

/*******************************************************
 * [IB] attribute macro name
 ******************************************************/
#define RP_PHY_CURRENT_CHANNEL				(0)
#define RP_PHY_CHANNELS_SUPPORTED			(1)
#define RP_PHY_TRANSMIT_POWER				(2)
#define	RP_PHY_CURRENT_PAGE					(3)
#define RP_PHY_FSK_FEC_TX_ENA				(4)
#define RP_PHY_FSK_FEC_RX_ENA				(5)
#define RP_PHY_FSK_FEC_SCHEME				(6)
#define	RP_PHY_CCA_VTH						(7)
#define	RP_PHY_LVLFLTR_VTH					(8)
#define RP_PHY_BACKOFF_SEED					(9)
//
#define RP_PHY_CRCERROR_UPMSG				(10)
#define RP_MAC_ADDRESS_FILTER1_ENA			(11)
#define RP_MAC_SHORT_ADDRESS1				(12)
#define RP_MAC_PANID1						(13)
#define RP_MAC_EXTENDED_ADDRESS1			(14)
#define RP_MAC_PAN_COORD1					(15)
#define RP_MAC_FRAME_PEND1					(16)
#define RP_MAC_ADDRESS_FILTER2_ENA			(17)
#define RP_MAC_SHORT_ADDRESS2				(18)
#define RP_MAC_PANID2						(19)
//
#define RP_MAC_EXTENDED_ADDRESS2			(20)
#define RP_MAC_PAN_COORD2					(21)
#define RP_MAC_FRAME_PEND2					(22)
#define RP_MAC_MAXCSMABACKOFF				(23)
#define RP_MAC_MINBE						(24)
#define RP_MAC_MAXBE						(25)
#define RP_MAC_MAX_FRAME_RETRIES			(26)
#define RP_PHY_CCA_DURATION					(27)
#define RP_PHY_FSK_PREAMBLE_LENGTH			(28)
#define RP_PHY_MRFSK_SFD					(29)
//
#define RP_PHY_FSK_SCRAMBLE_PSDU			(30)
#define RP_PHY_FSK_OPE_MODE					(31)
#define RP_PHY_FCS_LENGTH					(32)
#define RP_PHY_ACK_REPLY_TIME				(33)
#define RP_PHY_ACK_WAIT_DURATION			(34)
#define RP_PHY_PROFILE_SPECIFIC_MODE		(35)
#define RP_PHY_ANTENNA_SWITCH_ENA			(36)
#define RP_PHY_ANTENNA_DIVERSITY_RX_ENA		(37)
#define RP_PHY_ANTENNA_SELECT_TX			(38)
#define RP_PHY_ANTENNA_SELECT_ACKTX			(39)
//
#define RP_PHY_ANTENNA_SELECT_ACKRX			(40)
#define RP_PHY_RX_TIMEOUT_MODE				(41)
#define RP_PHY_FREQ_BAND_ID					(42)
#define RP_PHY_DATA_RATE					(43)
#define RP_PHY_REGULATORY_MODE				(44)
#define RP_PHY_CHANNELS_SUPPORTED_PAGE		(45)
#define RP_PHY_CSMA_BACKOFF_PERIOD			(46)
#define RP_PHY_PREAMBLE_4BYTE_RX_MODE		(47)
#define RP_PHY_AGC_START_VTH				(48)
#define RP_PHY_CCA_BANDWIDTH				(49)
//
#define RP_PHY_ED_BANDWIDTH					(50)
#define RP_PHY_ANTENNA_DIVERSITY_START_VTH	(51)
#define RP_PHY_ANTENNA_SWITCHING_TIME		(52)
#define RP_PHY_SFD_DETECTION_EXTEND			(53)
#define RP_PHY_AGC_WAIT_GAIN_OFFSET			(54)
#define RP_PHY_CCA_VTH_OFFSET				(55)
#define RP_PHY_ANTENNA_SWITCH_ENA_TIMING	(56)
#define RP_PHY_GPIO0_SETTING				(57)
#define RP_PHY_GPIO3_SETTING				(58)
#define RP_PHY_RMODE_TON_MAX				(59)
//
#define RP_PHY_RMODE_TOFF_MIN				(60)
#define RP_PHY_RMODE_TCUM_SMP_PERIOD		(61)
#define RP_PHY_RMODE_TCUM_LIMIT				(62)
#define RP_PHY_RMODE_TCUM					(63)
#define RP_PHY_ACK_WITH_CCA					(64)
#define RP_PHY_RSSI_OUTPUT_OFFSET			(65)
#ifdef R_FREQUENCY_OFFSET_ENABLED
	#define RP_PHY_FREQUENCY				(66)
	#define RP_PHY_FREQUENCY_OFFSET			(67)
#endif // #ifdef R_FREQUENCY_OFFSET_ENABLED

/*******************************************************
 * [API] return value
 ******************************************************/
#define RP_SUCCESS					(0x07)
#define RP_INVALID_PARAMETER		(0x05)
#define RP_BUSY_LOWPOWER			(0xF3)
#define RP_RX_ON					(0x06)
#define RP_BUSY_RX					(0x01)
#define RP_BUSY_TX					(0x02)
#define RP_BUSY_OTHER				(0x0E)
#define RP_PENDING					(0x0C)
#define RP_UNSUPPORTED_ATTRIBUTE 	(0x0A)
#define RP_INVALID_API_OPTION		(0x0F)
#define RP_SAME_STATUS				(0xF8)
#define RP_UNSUPPORTED_CHANNEL		(0xF0)
#define RP_NOT_GET_RXBUF			(0xF9)
#define RP_INVALID_SET_TIME			(0xFA)
//
#define RP_TRX_OFF					(0x08)

/*******************************************************
 * [API] power mode
 ******************************************************/
// RpSetPowerMode/RpGetPowerMode
#define RP_RF_IDLE					(0x00)
#define RP_RF_LOWPOWER				(0x01)

/*******************************************************
 * [API] options
 ******************************************************/
// RpSetRxOnReq
#define	RP_RX_TIME					(0x01)
#define RP_RX_SLOTTED				(0x08)
#define	RP_RX_TMOUT					(0x20)
#define RP_RX_AUTO_RCV				(0x80)
//
// RpPdDataReq
#define	RP_TX_TIME					(0x01)
#define	RP_TX_CCA					(0x04)
#define RP_TX_SLOTTED				(0x08)
#define RP_TX_ACK					(0x10)
#define RP_TX_CSMACA_RX				(0x20)
#define RP_TX_CSMACA				(0x40)
#define RP_TX_AUTO_RCV				(0x80)

#ifdef RP_ENABLE_TxTotalTimeLimitControl
	// Tx Limit Mode
	#define RP_PHY_TTLMT_INIT					(0)
	#define RP_PHY_TTLMT_REF_REST_TXABLE_TIME	(1)
	#define RP_PHY_TTLMT_REF_REST_LIMIT_TX_TIME	(2)
	#define RP_PHY_TTLMT_REF_REST_INTERVAL_TIME	(3)
#endif // #ifdef RP_ENABLE_TxTotalTimeLimitControl

/*******************************************************
 * [callback] status
 ******************************************************/
// RpPdDataIndCallback
#define RP_NO_FRMPENBIT_ACK			(0x00)
#define RP_FRMPENBIT_ACK			(0x04)
#define RP_CRC_BAD					(0x80)
#define RP_CHANNEL_BUSY				(0x05)

// RpPdDataCfmCallback
#define RP_NO_ACK					(0xF2)
#define RP_BUSY						(0x00)
#define RP_TRANSMIT_TIME_OVERFLOW	(0x0D)
#define RP_TX_UNDERFLOW				(0xF4)

// RpPlmeCcaCfmCallback
#define RP_IDLE						(0x04)

// RpWarningIndCallback
#define RP_PHY_INTER_MSG_WARN		(0x20U)
#define RP_WARN_NOT_GET_RXBUF		(0x01U | RP_PHY_INTER_MSG_WARN)
#define RP_WARN_RXRAMOVERRUN		(0x02U | RP_PHY_INTER_MSG_WARN)
#define RP_WARN_RX_PROCESS_DELAY	(0x04U | RP_PHY_INTER_MSG_WARN)
#define RP_WARN_TX_ACK_STOP			(0x08U | RP_PHY_INTER_MSG_WARN)

// RpAckCheckCallback
#define RP_INVALID_FRAME			(0x00)
#define RP_VALID_FRAME				(0x01)
#define RP_NEED_ACK					(0x02)
#define RP_VALID_FRAME2				(0x10)
#define RP_NEED_ACK2				(0x20)

// Frame Version set value RpAckCheckCallback
#define RP_MAC2003_FRAME_VERSION	(0x00)
#define RP_MAC2011_FRAME_VERSION	(0x01)
#define RP_MAC2012_FRAME_VERSION	(0x02)
//
#define RP_AddressingMode_No_Address		(0x00u)
#define RP_AddressingMode_8bit_Address		(0x01u)
#define RP_AddressingMode_Short_Address		(0x02u)
#define RP_AddressingMode_Extended_Address	(0x03u)
//
#define RP_FRM_FrameType_Beacon				(0u)
#define RP_FRM_FrameType_Data				(1u)
#define RP_FRM_FrameType_Acknowledgment		(2u)
#define RP_FRM_FrameType_MACCommand			(3u)

/*****************************************************************************
 * typedef definitions
 ****************************************************************************/
/*******************************************************
 * [callback] callback function typedef
 ******************************************************/
typedef void (*RpPdDataIndCallbackT)( uint8_t *pData, uint16_t dataLength, uint32_t time,
									 uint8_t linkQuality, uint16_t rssirslt, uint8_t selectedAntenna,
									 uint16_t rssi_0, uint16_t rssi_1, uint8_t status,
									 uint8_t filteredAddress, uint8_t phr );
typedef void (*RpPdDataCfmCallbackT)( uint8_t status, uint8_t framepend, uint8_t numBackoffs );
typedef void (*RpPlmeCcaCfmCallbackT)( uint8_t status );
typedef void (*RpPlmeEdCfmCallbackT)( uint8_t status, uint8_t val, uint16_t rssi_val );
typedef void (*RpRxOffIndCallbackT)( void );
typedef void (*RpFatalErrorIndCallbackT)( uint8_t err_ind );
typedef void (*RpWarningIndCallbackT)( uint8_t err_ind );
typedef uint8_t (*RpCalcLqiCallbackT)( uint16_t rssi, uint8_t rssiSelect );
typedef uint8_t (*RpAckCheckCallbackT)( uint8_t *pPsdu, uint16_t psduLen, uint8_t *pAck, uint16_t *pAckLen );

/*****************************************************************************
 * function prototype definitions
 ****************************************************************************/
/*******************************************************
 * [API]
 ******************************************************/
int16_t RpInit( RpPdDataIndCallbackT pDataIndCallback, RpPdDataCfmCallbackT pDataCfmCallback,
			   	RpPlmeCcaCfmCallbackT pCcaCfmCallback, RpPlmeEdCfmCallbackT pEdCfmCallback,
			   	RpRxOffIndCallbackT pRxOffIndCallback, RpFatalErrorIndCallbackT pFatalErrorIndCallback,
			   	RpWarningIndCallbackT pWarningIndCallback, RpCalcLqiCallbackT pCalcLqiCallback,
			   	RpAckCheckCallbackT pAckCheckCallback );
void RpResetReq( void );
int16_t RpSetRxOffReq( void );
int16_t RpSetRxOnReq( uint8_t options, uint32_t time );
int16_t RpPdDataReq( uint8_t *pData, uint16_t dataLength, uint8_t options, uint32_t time );
int16_t RpPlmeEdReq( void );
int16_t RpPlmeCcaReq( void );
int16_t RpSetPibReq( uint8_t id, uint8_t valLen, void RP_FAR *pVal );
int16_t RpGetPibReq( uint8_t id, uint8_t valLen, void *pVal );
int16_t RpRelRxBuf( uint8_t *p_buf );
int16_t RpSetPowerMode( uint8_t nxtRf );
uint16_t RpGetPowerMode( void );
uint32_t RpGetTime( void );
void RptConTxNoModu( void );
void RptPulseTxPrbs9( void );
void RptConTrxStop( void );
void RpTxLimitTimerForward( uint32_t calledByXmsec );
//
void RpSetLowPowerProhibit( void );
void RpClrLowPowerProhibit( void );
uint8_t RpGetLowPowerProhibit( void );
#if defined(__RL78__)
	uint8_t RpGetInterruptLevel( void );
#endif
//
#ifdef RP_ENABLE_TxTotalTimeLimitControl
	uint32_t RpTxTotalTimeLimitControl( uint8_t mode );
#endif // #ifdef RP_ENABLE_TxTotalTimeLimitControl
#ifdef RP_ENABLE_LimitTimerControlSwitch
	void RpLimitTimerControlSwitch( uint8_t timerOn );
#endif // #ifdef RP_ENABLE_LimitTimerControlSwitch
//
void RpWupTsk( uint8_t eventId );	// for protocol stack

#if defined(RP_WISUN_FAN_STACK) || defined(RP_ST_ENV)
	int16_t RpPlmeSetReq( uint8_t attrId, void RP_FAR *p_attrValue );
	void RpPlmeGetReq( uint8_t id, void *pVal );
#endif // #if defined(RP_WISUN_FAN_STACK) || defined(RP_ST_ENV)

#if defined(RP_WISUN_FAN_STACK)
	int16_t RpGetPhyStatus(void);
	void RpWait4us(void);
	uint8_t RpAckCheckCallbackFAN(uint8_t* pData, uint16_t dataLen, uint8_t* pAck, uint16_t* pAckLen);
	uint8_t RpCalcLqiCallback(uint16_t rssiEdValue, uint8_t rssiEdSelect);
#endif // #if defined(RP_WISUN_FAN_STACK)

/*******************************************************
 * [MCU]
 ******************************************************/
void RpIntpHdr( void );	// interrupt

#endif // #ifndef __PHY_H__
/***********************************************************************************************************************
 * Copyright (C) 2014-2021 Renesas Electronics Corporation.
 **********************************************************************************************************************/

