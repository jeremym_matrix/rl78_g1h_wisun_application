/***********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products.
 * No other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all 
 * applicable laws, including copyright laws. 
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING THIS SOFTWARE, WHETHER EXPRESS, 
 * IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE 
 * AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
 * TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS 
 * AFFILIATED COMPANIES SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR 
 * ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of 
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the 
 * following link:
 * http://www.renesas.com/disclaimer 
 **********************************************************************************************************************/
/***********************************************************************************************************************
 * file name	: phy_api.c
 * description	: This is the RF driver's api code.
 ***********************************************************************************************************************
 * Copyright (C) 2014-2021 Renesas Electronics Corporation.
 **********************************************************************************************************************/
/***************************************************************************************************************
 * includes
 **************************************************************************************************************/
#include <string.h>
//
#include "phy.h"
#include "phy_def.h"
#include "phy_table.h"
//
#if defined(__arm)
	#include "cpx3.h"
	#include "phy_drv.h"
#endif

/***************************************************************************************************************
 * extern definitions
 **************************************************************************************************************/
extern const RP_CONFIG_CB RpConfig;

/***************************************************************************************************************
 * private function prototypes
 **************************************************************************************************************/
static uint32_t RpCsmaBytesToTime( uint16_t txTotalLength );
static void RpSetAdrfRegConverion( uint8_t convert );
static int16_t RpCurStatCheck( uint16_t status );
static void RpChkTmoutTrxStateOff( void );
static int16_t RpSetStateRxOn( uint8_t options, uint32_t time );
static uint8_t RpChkRxTriggerTimer( uint32_t time );
static void RpClrAutoRxFunc( void );
static int16_t RpAvailableRcvOnCsmaca( void );
static void RpInitRfic( void );
static void RpWrEvaReg1( uint16_t aData );
static uint8_t RpSetChannelCommonVal( uint8_t channel, uint8_t freqBandId, const uint32_t *pTblPtr );
static void RpSetCcaDurationTimeUpdate( uint8_t channel );
static void RpSetFreqBandVal( uint8_t afterReset );
static uint8_t RpCmpRefVthVal( uint16_t vthVal );
static void RpSetRssiOffsetVal( void );
static void RpSetMiscellaneousIniVal( void );
static void RpInitVar( uint8_t refreshFlg );
static void RpInitRfOnly( void );
static void RpSetRegBeforeIdle( void );
static void RpSetFreqAddReg( uint32_t freq, uint8_t freqBandId );
#ifndef RP_ST_ENV
static void RpSetRfInt( uint8_t enable );
#endif // #ifndef RP_ST_ENV
#if !defined(RP_WISUN_FAN_STACK) && !defined(RP_ST_ENV)
static void RpWait4us( void );
#else
void RpWait4us( void );
#endif /* !defined(RP_WISUN_FAN_STACK) && !defined(RP_ST_ENV) */
static void RpInitRxBuf( void );
static int16_t RpExtChkOptErrDataReq( uint8_t options );
static int16_t RpExtChkOptErrRxStateReq( uint8_t options );
static void RpProgressSamplingTxTime( uint8_t progressSample );
static void RpCalcTotalTxTime( void );
static uint32_t RpBytesToTime( uint16_t psduLength, uint8_t useRxFcsLength );
static void RpRegCcaBandwidth200k( void );
static void RpRegCcaBandwidth150k( void );
static void RpRegAdcVgaModify( void );
static void RpRegTxRxDataRate100kbps( void );
static void RpRegRxDataRate100kbps( void );

/***************************************************************************************************************
 * private variables
 **************************************************************************************************************/
RP_PHY_CB		RpCb;
RP_PHY_ERROR	RpRfStat;
const uint8_t	RpStackVersion[] = "V308_10719";
RP_RX_BUF		RpRxBuf[RP_RX_BUF_NUM];
uint32_t		RpTxTime[RP_MAX_NUM_TX_LMT_BUF];

#if defined(RP_LOG_IRQ) || defined(RP_LOG_EVENT)
	uint32_t RpLogRam[RP_LOG_NUMBER];
	uint16_t RpLogRamCnt;
#endif // #if defined(RP_LOG_IRQ) || defined(RP_LOG_EVENT)

/***************************************************************************************************************
 * program
 **************************************************************************************************************/
/***************************************************************************************************************
 * function name  : RpInit
 * description    : RF Driver Initialization
 * parameters     : pDataIndCallback...Pointer to the RpPdDataIndCallback function
 * 				  : pDataCfmCallback...Pointer to the RpPdDataCfmCallback function
 * 				  : pCcaCfmCallback...Pointer to the RpPlmeCcaCfmCallback function
 * 				  : pEdCfmCallback...Pointer to the RpPlmeEdCfmCallback function
 * 				  : pRxOffIndCallback...Pointer to the RpRxOffIndCallback function
 * 				  : pFatalErrorIndCallback...Pointer to the RpFatalErrorIndCallback function
 * 				  : pWarningIndCallback...Pointer to the RpWarningIndCallback function
 * 				  : pCalcLqiCallback...Pointer to the RpCalcLqiCallback function
 * 				  : pAckCheckCallback...Pointer to the RpAckCheckCallback function
 * return value   : RP_SUCCESS, RP_INVALID_PARAMTER
 **************************************************************************************************************/
int16_t RpInit(RpPdDataIndCallbackT pDataIndCallback, RpPdDataCfmCallbackT pDataCfmCallback,
	   			RpPlmeCcaCfmCallbackT pCcaCfmCallback, RpPlmeEdCfmCallbackT pEdCfmCallback,
	   			RpRxOffIndCallbackT pRxOffIndCallback, RpFatalErrorIndCallbackT pFatalErrorIndCallback,
	   			RpWarningIndCallbackT pWarningIndCallback, RpCalcLqiCallbackT pCalcLqiCallback,
	   			RpAckCheckCallbackT pAckCheckCallback)
{
	#if defined(__RX)
	uint32_t bkupPsw;
	#elif defined(__CCRL__)
	uint8_t  bkupPsw;
	#elif defined(__arm)
	uint8_t bkupPsw[RP_INT_BKUP_BUF_SIZE];
	#endif

	/* Disable interrupt */
	#if defined(__RX) || defined(__CCRL__) || defined(__arm)
	RP_PHY_DI(bkupPsw);
	#else
	RP_PHY_DI();
	#endif

	/* Set the initial value for the log element counter */
	#if defined(RP_LOG_IRQ) || defined(RP_LOG_EVENT)
	RpLogRamCnt = 0;
	#endif // #if defined(RP_LOG_IRQ) || defined(RP_LOG_EVENT)

	/* API function execution Log */
	RpLog_Event( RP_LOG_API_INIT, RP_NULL );

	if ((pDataIndCallback == RP_NULL) || (pDataCfmCallback == RP_NULL) || (pCcaCfmCallback  == RP_NULL)
			|| (pEdCfmCallback  == RP_NULL) || (pRxOffIndCallback == RP_NULL)
			|| (pFatalErrorIndCallback == RP_NULL) || (pWarningIndCallback == RP_NULL) || (pCalcLqiCallback == RP_NULL))
	{
		/* API function execution Log */
		RpLog_Event( RP_LOG_API_INIT | RP_LOG_API_RETERR, RP_INVALID_PARAMETER );

		/* Enable interrupt */
		#if defined(__RX) || defined(__CCRL__) || defined(__arm)
		RP_PHY_EI(bkupPsw);
		#else
		RP_PHY_EI();
		#endif

		return (RP_INVALID_PARAMETER);
	}

	// initialize MCU Peripheral
	RpMcuPeripheralInit();			// including Rp_Powerdown_Sequence

	// RF IC Interrupt Disable
	RpSetMcuInt(RP_FALSE);			// RF interrupts request clear and disable

	// initialize PHY
	RpInitVar(RP_FALSE);

	// register callbacks
	RpCb.callback.pDataIndCallback  = pDataIndCallback;
	RpCb.callback.pDataCfmCallback  = pDataCfmCallback;
	RpCb.callback.pCcaCfmCallback   = pCcaCfmCallback;
	RpCb.callback.pEdCfmCallback    = pEdCfmCallback;
	RpCb.callback.pRxOffIndCallback = pRxOffIndCallback;
	RpCb.callback.pFatalErrorIndCallback = pFatalErrorIndCallback;
	RpCb.callback.pWarningIndCallback = pWarningIndCallback;
	RpCb.callback.pCalcLqiCallback = pCalcLqiCallback;
	if (pAckCheckCallback == RP_NULL)
	{
		RpCb.callback.pAckCheckCallback = RpAckCheckCallback;
	}
	else
	{
		RpCb.callback.pAckCheckCallback = pAckCheckCallback;
	}

	// initialize RF IC
	RpInitRfic();
	RpSetMcuInt(RP_TRUE);			// RF interrupts enable
	RpSetRfInt(RP_TRUE);

	// initialize Rx buffer
	RpInitRxBuf();

	/* API function execution Log */
	RpLog_Event( RP_LOG_API_INIT | RP_LOG_API_RET, RP_SUCCESS );

	/* Enable interrupt */
	#if defined(__RX) || defined(__CCRL__) || defined(__arm)
	RP_PHY_EI(bkupPsw);
	#else
	RP_PHY_EI();
	#endif

	return (RP_SUCCESS);
}

/***************************************************************************************************************
 * function name  : RpResetReq
 * description    : RF driver reset
 * parameters     : none
 * return value   : none
 **************************************************************************************************************/
void RpResetReq( void )
{
	#if defined(__RX)
	uint32_t bkupPsw;
	#elif defined(__CCRL__)
	uint8_t  bkupPsw;
	#elif defined(__arm)
	uint8_t bkupPsw[RP_INT_BKUP_BUF_SIZE];
	#endif

	/* Disable interrupt */
	#if defined(__RX) || defined(__CCRL__) || defined(__arm)
	RP_PHY_DI(bkupPsw);
	#else
	RP_PHY_DI();
	#endif

	/* API function execution Log */
	RpLog_Event( RP_LOG_API_RESET, RP_NULL );

	// Pervious Timer value Re-Setting
	RpPrevSentTimeReSetting();

	// initialize MCU Peripheral
	RpMcuPeripheralInit();			// including Rp_Powerdown_Sequence

	// RF IC Interrupt Disable
	RpSetMcuInt(RP_FALSE);			// RF interrupts request clear and disable

	// initialize PHY
	RpInitVar(RP_TRUE);

	// initialize RF IC
	RpInitRfic();
	RpSetMcuInt(RP_TRUE);			// RF interrupts enable
	RpSetRfInt(RP_TRUE);

	// initialize Rx buffer
	RpInitRxBuf();

	/* API function execution Log */
	RpLog_Event( RP_LOG_API_RESET | RP_LOG_API_RET, RP_NULL );

	/* Enable interrupt */
	#if defined(__RX) || defined(__CCRL__) || defined(__arm)
	RP_PHY_EI(bkupPsw);
	#else
	RP_PHY_EI();
	#endif
}

/***************************************************************************************************************
 * function name  : RpSetRxOffReq
 * description    : Idle Setting Request
 * parameters     : none
 * return value   : RP_SUCESS, RP_BUSY_LOWPOWER
 **************************************************************************************************************/
int16_t RpSetRxOffReq( void )
{
	uint16_t	curStat;
	int16_t	rtnVal = RP_SUCCESS;

	#if defined(__RX)
	uint32_t bkupPsw;
	#elif defined(__CCRL__)
	uint8_t  bkupPsw;
	#elif defined(__arm)
	uint8_t bkupPsw[RP_INT_BKUP_BUF_SIZE];
	#endif

	/* Disable interrupt */
	#if defined(__RX) || defined(__CCRL__) || defined(__arm)
	RP_PHY_DI(bkupPsw);
	#else
	RP_PHY_DI();
	#endif

	/* API function execution Log */
	RpLog_Event( RP_LOG_API_RXOFF, RP_NULL );

	// copy current status
	curStat = RpCb.status;

	if (curStat & RP_PHY_STAT_LOWPOWER)
	{
		rtnVal = RP_BUSY_LOWPOWER;
	}
	else if (curStat & RP_PHY_STAT_TRX_OFF)
	{
		;	// rtnVal = RP_SUCCESS;
	}
	else
	{
		if (curStat & RP_PHY_STAT_RX)
		{
			if (RpRxOffBeforeReplyingAck() == RP_TRUE)
			{
				RpSetStateRxOnToTrxOff();
			}
			else
			{
				//TX(DATA,ACK),CCA,ED
				RpChkTmoutTrxStateOff();
			}
		}
		else if ((curStat & RP_PHY_STAT_TX) && (RP_PHY_STAT_TX_BUSY() == 0))
		{
			RpSetStateRxOnToTrxOff();
		}
		else
		{
			//TX(DATA,ACK),CCA,ED
			RpChkTmoutTrxStateOff();
		}
	}

	/* API function execution Log */
	RpLog_Event( RP_LOG_API_RXOFF | RP_LOG_API_RET, (uint8_t)rtnVal );

	/* Enable interrupt */
	#if defined(__RX) || defined(__CCRL__) || defined(__arm)
	RP_PHY_EI(bkupPsw);
	#else
	RP_PHY_EI();
	#endif

	return (rtnVal);
}

/******************************************************************************
Function Name:       RpRxOffBeforeReplyingAck
Parameters:          none
Return value:        RP_TRUE:successful of Rxoff before Replying Ack
				  RP_FALSE:could not Rxoff cause on Replying Ack
Description:         Rx to TRX_OFF before Replying Ack
******************************************************************************/
uint8_t RpRxOffBeforeReplyingAck( void )
{
	uint8_t stillRxOff = RP_FALSE;

	#if defined(__RX)
	uint32_t bkupPsw;
	#elif defined(__CCRL__)
	uint8_t  bkupPsw;
	#elif defined(__arm)
	uint8_t bkupPsw[RP_INT_BKUP_BUF_SIZE];
	#endif

	/* Disable interrupt */
	#if defined(__RX) || defined(__CCRL__) || defined(__arm)
	RP_PHY_ALL_DI(bkupPsw);
	#else
	RP_PHY_ALL_DI();
	#endif

	if (RP_PHY_STAT_ONACKREPLY() == 0)
	{
		RpRegWrite(BBTXRXRST, RFSTOP);
		stillRxOff = RP_TRUE;
	}

	/* Enable interrupt */
	#if defined(__RX) || defined(__CCRL__) || defined(__arm)
	RP_PHY_ALL_EI(bkupPsw);
	#else
	RP_PHY_ALL_EI();
	#endif

	return (stillRxOff);
}

/***************************************************************************************************************
 * function name  : RpSetRxOnReq
 * description    : Reception ON Setting Request
 * parameters     : options...Options
 * 			      : time...Start time [Unit:Symbol]
 * return value   : RP_SUCESS, RP_INVALID_PARAMETER, RP_RX_ON, RP_BUSY_RX, RP_BUSY_TX, RP_BUSY_LOWPOWER
 *				  : RP_NOT_GET_RXBUF, RP_INVALID_SET_TIME
 **************************************************************************************************************/
int16_t RpSetRxOnReq( uint8_t options, uint32_t time )
{
	uint16_t curStat;
	int16_t	rtnVal = RP_SUCCESS;

	#if defined(__RX)
	uint32_t bkupPsw;
	#elif defined(__CCRL__)
	uint8_t  bkupPsw;
	#elif defined(__arm)
	uint8_t bkupPsw[RP_INT_BKUP_BUF_SIZE];
	#endif

	/* Disable interrupt */
	#if defined(__RX) || defined(__CCRL__) || defined(__arm)
	RP_PHY_DI(bkupPsw);
	#else
	RP_PHY_DI();
	#endif

	/* API function execution Log */
	RpLog_Event( RP_LOG_API_RXON, options );

	if (RpExtChkOptErrRxStateReq(options) == RP_TRUE)
	{
		/* API function execution Log */
		RpLog_Event( RP_LOG_API_RXON | RP_LOG_API_RETERR, RP_INVALID_PARAMETER );

		/* Enable interrupt */
		#if defined(__RX) || defined(__CCRL__) || defined(__arm)
		RP_PHY_EI(bkupPsw);
		#else
		RP_PHY_EI();
		#endif

		return (RP_INVALID_PARAMETER);
	}

	if ( RpCb.pib.phyAntennaDiversityRxEna == RP_TRUE )
	{
		if ( RpCb.pib.phyDataRate == RP_PHY_ANTDV_DATARATE ) // 100kbps only
		{
			if (( RpCb.pib.phySfdDetectionExtend == RP_TRUE ) ||
				( RpCb.pib.phyPreamble4ByteRxMode == RP_FALSE ))
			{
				/* API function execution Log */
				RpLog_Event( RP_LOG_API_RXON | RP_LOG_API_RETERR, RP_INVALID_PARAMETER );

				/* Enable interrupt */
				#if defined(__RX) || defined(__CCRL__) || defined(__arm)
				RP_PHY_EI(bkupPsw);
				#else
				RP_PHY_EI();
				#endif

				return (RP_INVALID_PARAMETER);
			}
		}
	}

	// copy current status
	curStat = RpCb.status;
	if (curStat & RP_PHY_STAT_LOWPOWER)
	{
		rtnVal = RP_BUSY_LOWPOWER;
	}
	else if (curStat & RP_PHY_STAT_TRX_OFF)
	{
		// TRX_OFF -> RX_ON
		rtnVal = RpSetStateRxOn(options, time);
	}
	else if (curStat & RP_PHY_STAT_TX)
	{
		rtnVal = RP_BUSY_TX;
	}
	else if (curStat & RP_PHY_STAT_RX)
	{
		if (RP_PHY_STAT_RX_BUSY())
		{
			rtnVal = RP_BUSY_RX;
		}
		else
		{
			rtnVal = RP_RX_ON;
		}
	}
	else
	{
		// CCA or ED or RX
		rtnVal = RP_BUSY_RX;
	}

	/* API function execution Log */
	RpLog_Event( RP_LOG_API_RXON | RP_LOG_API_RET, (uint8_t)rtnVal );

	/* Enable interrupt */
	#if defined(__RX) || defined(__CCRL__) || defined(__arm)
	RP_PHY_EI(bkupPsw);
	#else
	RP_PHY_EI();
	#endif

	return (rtnVal);
}

/***************************************************************************************************************
 * function name  : RpPdDataReq
 * description    : Frame Transmission Request
 * parameters     : pData...Pointer to the beginning of the area storing the transmission data.
 *				  : len...Size of the transmission data (PSDU size minus FCS length)
 *				  : options...Transmission options
 * 			      : time...Transmission (frame transmission or CSMA-CA) start time [Unit:Symbol]
 * return value   : RP_PENDING, RP_INVALID_PARAMETER, RP_BUSY_RX, RP_BUSY_TX, RP_BUSY_LOWPOWER,
 *				  : RP_INVALID_API_OPTION
 **************************************************************************************************************/
int16_t RpPdDataReq( uint8_t *pData, uint16_t len, uint8_t options, uint32_t time )
{
	uint16_t status, txFlen;
	uint8_t minBe = 0;
	uint8_t ccaDurPibValReq;
	uint32_t futureTime;
	uint8_t ccaBandwidth = RpCb.pib.phyCcaBandwidth;
	uint8_t edBandwidth = RpCb.pib.phyEdBandwidth;
	uint8_t index = RpCb.freqIdIndex;

	#if defined(__RX)
	uint32_t bkupPsw;
	uint32_t bkupPsw2;
	#elif defined(__CCRL__)
	uint8_t  bkupPsw;
	uint8_t  bkupPsw2;
	#elif defined(__arm)
	uint8_t bkupPsw[RP_INT_BKUP_BUF_SIZE];
	uint8_t bkupPsw2[RP_INT_BKUP_BUF_SIZE];
	#endif

	/* Disable interrupt */
	#if defined(__RX) || defined(__CCRL__) || defined(__arm)
	RP_PHY_DI(bkupPsw);
	#else
	RP_PHY_DI();
	#endif

	/* API function execution Log */
	RpLog_Event( RP_LOG_API_DATAREQ, options );

	if (pData == RP_NULL)
	{
		/* API function execution Log */
		RpLog_Event( RP_LOG_API_DATAREQ | RP_LOG_API_RETERR, RP_LOG_API_RETERR_NODATA );

		/* Enable interrupt */
		#if defined(__RX) || defined(__CCRL__) || defined(__arm)
		RP_PHY_EI(bkupPsw);
		#else
		RP_PHY_EI();
		#endif

		return (RP_INVALID_PARAMETER);
	}

	if (RpExtChkErrFrameLen(len, RP_FALSE) == RP_TRUE)
	{
		/* API function execution Log */
		RpLog_Event( RP_LOG_API_DATAREQ | RP_LOG_API_RETERR, RP_LOG_API_RETERR_FRAMELEN );

		/* Enable interrupt */
		#if defined(__RX) || defined(__CCRL__) || defined(__arm)
		RP_PHY_EI(bkupPsw);
		#else
		RP_PHY_EI();
		#endif

		return (RP_INVALID_PARAMETER);
	}

	if (RpExtChkOptErrDataReq(options) == RP_TRUE)
	{
		/* API function execution Log */
		RpLog_Event( RP_LOG_API_DATAREQ | RP_LOG_API_RETERR, RP_LOG_API_RETERR_DATAREQ );

		/* Enable interrupt */
		#if defined(__RX) || defined(__CCRL__) || defined(__arm)
		RP_PHY_EI(bkupPsw);
		#else
		RP_PHY_EI();
		#endif

		return (RP_INVALID_PARAMETER);
	}

	if ((RpCb.pib.phyCcaBandwidth == RP_PHY_CCA_BANDWIDTH_INDEX_200K) ||
		(RpCb.pib.phyCcaBandwidth == RP_PHY_CCA_BANDWIDTH_INDEX_150K))
	{
		if (((options & RP_TX_CSMACA) && ((index != RP_PHY_100K_M10_JP) && (index != RP_PHY_100K_M10_IN)))
			|| (options & RP_TX_CSMACA_RX) || (options & RP_TX_ACK))
		{
			/* API function execution Log */
			RpLog_Event( RP_LOG_API_DATAREQ | RP_LOG_API_RETERR, RP_INVALID_API_OPTION );

			/* Enable interrupt */
#if defined(__RX) || defined(__CCRL__) || defined(__arm)
			RP_PHY_EI(bkupPsw);
#else
			RP_PHY_EI();
#endif
			return (RP_INVALID_API_OPTION);
		}
	}
	else if (RpCb.pib.phyCcaBandwidth == RP_PHY_CCA_BANDWIDTH_INDEX_NARROW)
	{
		if ((options & RP_TX_CSMACA_RX) || (options & RP_TX_ACK))
		{
			/* API function execution Log */
			RpLog_Event( RP_LOG_API_DATAREQ | RP_LOG_API_RETERR, RP_INVALID_API_OPTION );
	
			/* Enable interrupt */
#if defined(__RX) || defined(__CCRL__) || defined(__arm)
			RP_PHY_EI(bkupPsw);
#else
			RP_PHY_EI();
#endif
			return (RP_INVALID_API_OPTION);

		}
	}

	status = RpCb.status;

	if ((status & RP_PHY_STAT_TRX_OFF) == 0)
	{
		/* API function execution Log */
		RpLog_Event( RP_LOG_API_DATAREQ | RP_LOG_API_RETERR, (uint8_t)RpCurStatCheck(status) );

		/* Enable interrupt */
		#if defined(__RX) || defined(__CCRL__) || defined(__arm)
		RP_PHY_EI(bkupPsw);
		#else
		RP_PHY_EI();
		#endif

		return (RpCurStatCheck(status));
	}

	if ((RpCb.pib.phyRegulatoryMode == RP_RMODE_ENABLE) ||
		((RpCb.pib.phyRegulatoryMode == RP_RMODE_ENABLE_ARIB) && (RpCb.pib.phyFreqBandId == RP_PHY_FREQ_BAND_920MHz) &&
		 (RpCb.pib.phyCurrentChannel >= RP_PHY_MIN_CHANNEL_VALID_ARIB_MODE)))
	{
		if (RpCheckLongerThanTotalTxTime(len, options, RP_FALSE) == RP_TRUE)
		{
			/* Callback function execution Log */
			RpLog_Event( RP_LOG_CB, RP_LOG_CB_DATACFM );
			/* Callback function execution */
			INDIRECT_RpPdDataCfmCallback( RP_TRANSMIT_TIME_OVERFLOW, RP_NO_FRMPENBIT_ACK, 0 );

			/* API function execution Log */
			RpLog_Event( RP_LOG_API_DATAREQ | RP_LOG_API_RETERR, RP_LOG_API_RETERR_TOTALTXTIME );

			/* Enable interrupt */
			#if defined(__RX) || defined(__CCRL__) || defined(__arm)
			RP_PHY_EI(bkupPsw);
			#else
			RP_PHY_EI();
			#endif

			return (RP_PENDING);
		}
	}

	if ((options & RP_TX_CCA) && (options & (RP_TX_CSMACA | RP_TX_CSMACA_RX | RP_TX_SLOTTED)))
	{
		/* API function execution Log */
		RpLog_Event( RP_LOG_API_DATAREQ | RP_LOG_API_RETERR, RP_INVALID_API_OPTION );

		/* Enable interrupt */
		#if defined(__RX) || defined(__CCRL__) || defined(__arm)
		RP_PHY_EI(bkupPsw);
		#else
		RP_PHY_EI();
		#endif

		return (RP_INVALID_API_OPTION);
	}

	if (((options & (RP_TX_CSMACA | RP_TX_CSMACA_RX)) == (RP_TX_CSMACA | RP_TX_CSMACA_RX)) || (len > (RP_BB_TX_RAM_SIZE * 2)))
	{
		RpMemcpy(RpCb.tx.data, pData, len);
		pData = RpCb.tx.data;
	}

	RpCb.status = RP_PHY_STAT_TX;
	RpClrAutoRxFunc();		//	clear auto functions
	RpCb.rx.onReplyingAck = RP_FALSE;

	txFlen = (uint16_t)(len + RpCb.pib.phyFcsLength);	// len not includes FCS size(=2 or 4), Length Set
	RpRegBlockWrite(BBTXFLEN, (uint8_t RP_FAR *)&txFlen, sizeof(uint16_t));
	RpCb.reg.bbTxRxMode2 &= (uint8_t)(~RETRN);
	RpRegWrite(BBTXRXMODE2, (uint8_t)(RpCb.reg.bbTxRxMode2));

	if ((options & RP_TX_TIME) || (RpCb.pib.phyRegulatoryMode == RP_RMODE_ENABLE) ||
		((RpCb.pib.phyRegulatoryMode == RP_RMODE_ENABLE_ARIB) && (RpCb.pib.phyFreqBandId == RP_PHY_FREQ_BAND_920MHz)))
	{
		RpCb.reg.bbTimeCon &= (uint8_t)(~(COMP0TRG | COMP0TRGSEL));
		RpRegWrite(BBTIMECON, (uint8_t)(RpCb.reg.bbTimeCon));
	}
	RpCb.reg.bbTxRxMode4 = CCAINTSEL;
	RpRegWrite(BBTXRXMODE4, (uint8_t)(RpCb.reg.bbTxRxMode4));
	RpCb.reg.bbCsmaCon0 |= CSMATRNST;
	RpCb.reg.bbCsmaCon0 &= ~(CSMAST | UNICASTFRM);

	if (options & RP_TX_ACK)
	{
		RpCb.status |= RP_PHY_STAT_TRX_ACK;
		if (RpCb.pib.phyProfileSpecificMode == RP_SPECIFIC_WSUN)
		{
			RpCb.reg.bbCsmaCon0 |= UNICASTFRM;
		}
		RpRegWrite(BBTXRXCON, ACKRCVEN);
	}
	else
	{
		RpRegWrite(BBTXRXCON, 0x00);
	}

	if (options & RP_TX_CSMACA)
	{
		RpCb.status |= RP_PHY_STAT_TX_CCA;

		if (options & RP_TX_SLOTTED)
		{
			RpCb.status |= RP_PHY_STAT_SLOTTED;
			RpCb.reg.bbTxRxMode0 |= BEACON;
			RpRegWrite(BBTXRXMODE0, (uint8_t)(RpCb.reg.bbTxRxMode0));
		}

		if ((options & RP_TX_CSMACA_RX) && (RpAvailableRcvOnCsmaca() == RP_SUCCESS))
		{
			RpCb.status |= RP_PHY_STAT_RXON_BACKOFF;
			RpCb.reg.bbCsmaCon0 |= CSMARCVEN;
		}
		else
		{
			RpCb.reg.bbCsmaCon0 &= ~CSMARCVEN;
		}
		minBe = RpCb.pib.macMinBe & BEMIN;
		RpCb.reg.bbCsmaCon2 |= MACMINBECON;
	}
	else if (options & RP_TX_CCA)
	{
		RpCb.status |= RP_PHY_STAT_CCA;
		RpCb.status |= RP_PHY_STAT_TX_CCA;

		RpCb.reg.bbCsmaCon0 &= ~CSMARCVEN;
		minBe = 0x00;
		RpCb.reg.bbCsmaCon2 |= MACMINBECON;
		
		RpStartTransmitWithCca();
	}
	else
	{
		RpCb.reg.bbCsmaCon2 &= ~MACMINBECON;
	}
	RpRegWrite(BBCSMACON3, (uint8_t)(minBe));
	RpRegWrite(BBCSMACON2, RpCb.reg.bbCsmaCon2);
	RpCb.tx.rtrnTimes = RpCb.pib.macMaxFrameRetries;
	RpCb.tx.ccaTimes = 0x00;

	if (options & RP_TX_AUTO_RCV)
	{
		RpCb.status |= RP_PHY_STAT_TRX_TO_RX_AUTO;
	}

	if (RpCb.status & RP_PHY_STAT_TX_CCA)
	{
		if ((RpCb.pib.phyAntennaDiversityRxEna == RP_TRUE) && (RpCb.status & RP_PHY_STAT_RXON_BACKOFF))
		{
			ccaDurPibValReq = RP_FALSE;

			if ((RpCb.pib.phyRegulatoryMode == RP_RMODE_ENABLE_ARIB) && (RpCb.pib.phyFreqBandId == RP_PHY_FREQ_BAND_920MHz))
			{
				if (RpCb.pib.phyCurrentChannel < RP_PHY_MIN_CHANNEL_VALID_ARIB_MODE)
				{
					ccaDurPibValReq = RP_TRUE;
				}
			}
		}
		else
		{
			ccaDurPibValReq = RP_TRUE;
		}
		RpSetCcaDurationVal(ccaDurPibValReq);

		RpInverseTxAnt(RP_TRUE);
		RpCb.tx.onCsmaCa = RP_TRUE;
		RpSetMaxCsmaBackoffVal();
		RpCb.tx.ccaTimesOneFrame = 0;
	}
	else
	{
		RpInverseTxAnt(RP_FALSE);
		RpCb.tx.onCsmaCa = RP_FALSE;
	}

	if (options & RP_TX_TIME)
	{
		// Timer Compare Reset
		futureTime = RpGetTime() - 1;
		RpRegBlockWrite(BBTCOMP0REG0, (uint8_t RP_FAR *)&futureTime, sizeof(uint32_t));
		RpReadIrq();
		
		// TX with timer trigger
		RpCb.status |= RP_PHY_STAT_WAIT_TMRTRG;
		if ((options & (RP_TX_CCA | RP_TX_CSMACA)) == 0)
		{
			// modify transmit time for RF IC warmup
			time -= RP_PHY_TX_WARM_UP_TIME;	// warmup time
			time &= RP_TIME_MASK;
		}
		RpCb.tx.time = time;
	}
	else
	{
		// TX immediately
		if ((RpCb.pib.phyRegulatoryMode == RP_RMODE_ENABLE) ||
			((RpCb.pib.phyRegulatoryMode == RP_RMODE_ENABLE_ARIB) && (RpCb.pib.phyFreqBandId == RP_PHY_FREQ_BAND_920MHz)))
		{
			RpCb.tx.time = RpGetTime();
		}
	}

	if ((RpCb.status & RP_PHY_STAT_TX_CCA) == RP_FALSE)
	{
		RpChangeFilter();
	}

	/* Disable interrupt */
	#if defined(__RX) || defined(__CCRL__) || defined(__arm)
	RP_PHY_ALL_DI(bkupPsw2);
	#else
	RP_PHY_ALL_DI();
	#endif

	if (RpSetTxTriggerTimer(RpCb.tx.time, RP_FALSE, options) == RP_TRUE)	// TX trigger
	{
		RpCb.status |= RP_PHY_STAT_WAIT_TMRTRG;
	}
	else
	{
		RpCb.reg.bbCsmaCon0 |= CSMAST;
		RpCb.status |= RP_PHY_STAT_BUSY;
	}
	RpRegWrite(BBCSMACON0, RpCb.reg.bbCsmaCon0);	// TX trigger

	if (RpCb.status & RP_PHY_STAT_TX_CCA)
	{
		switch (ccaBandwidth)
		{
			case RP_PHY_CCA_BANDWIDTH_INDEX_225K:
				switch (edBandwidth)
				{
					case RP_PHY_CCA_BANDWIDTH_INDEX_200K:
					case RP_PHY_CCA_BANDWIDTH_INDEX_150K:
						RpRegCcaBandwidth225k();
						RpRegAdcVgaDefault();
						break;
					case RP_PHY_CCA_BANDWIDTH_INDEX_NARROW:
						RpChangeDefaultFilter();
						break;
					default:
						break;
				}
				break;
			case RP_PHY_CCA_BANDWIDTH_INDEX_200K:
				RpRegCcaBandwidth200k();
				RpRegAdcVgaModify();
				break;
			case RP_PHY_CCA_BANDWIDTH_INDEX_150K:
				RpRegCcaBandwidth150k();
				RpRegAdcVgaModify();
				break;
			case RP_PHY_CCA_BANDWIDTH_INDEX_NARROW:
				RpChangeNarrowBandFilter();
				break;
			default:
				break;
		}
	}
	RpSetSfdDetectionExtendWrite(RpCb.status);

	RpCb.reg.bbIntEn[0] |= (TRNFININTEN | TRN0INTEN | TRN1INTEN | CCAINTEN);
	RpRegWrite(BBINTEN0,	(uint8_t)(RpCb.reg.bbIntEn[0]));
	RpCb.tx.len = len;
	RpCb.tx.cnt = 0x00;
	RpCb.tx.pNextData = pData;
	RpTrnxHdrFunc(RP_PHY_BANK_0);
	RpTrnxHdrFunc(RP_PHY_BANK_1);

	/* API function execution Log */
	RpLog_Event( RP_LOG_API_DATAREQ | RP_LOG_API_RET, RP_PENDING );

	#if defined(__arm)
   	RpLedTxOff();
	#endif

	/* Enable interrupt */
	#if defined(__RX) || defined(__CCRL__) || defined(__arm)
	RP_PHY_ALL_EI(bkupPsw2);
	RP_PHY_EI(bkupPsw);
	#else
	RP_PHY_ALL_EI();
	RP_PHY_EI();
	#endif

	return (RP_PENDING);
}

/******************************************************************************
Function Name:		 RpExtChkErrFrameLen
Parameters: 		 len:
                     useRxFcsLength : 0:FCS length is RpCb.pib.phyFcsLengt.
                                    : 1:FCS length is RpCb.rx.fcsLength.
Return value:		 RP_TRUE:Illegal Frame Length
Description:		 Check Illegal Frame Length.
******************************************************************************/
uint8_t
RpExtChkErrFrameLen(uint16_t len, uint8_t useRxFcsLength)
{
	uint16_t totalLen, sduLen;
	uint8_t rtnVal = RP_FALSE;
	uint8_t fcsLength = RpCb.pib.phyFcsLength;

	if (useRxFcsLength == RP_TRUE)
	{
		fcsLength = RpCb.rx.fcsLength;
	}
	if ((len == 0) || (len > (RP_aMaxPHYPacketSize - fcsLength)))
	{
		rtnVal = RP_TRUE;
	}
	else
	{
		if (RpCb.pib.phyRegulatoryMode == RP_RMODE_ENABLE)
		{
			if((((uint32_t)(RpCb.pib.phyRmodeTonMax)) * 1000) < RpBytesToTime(len, useRxFcsLength))
			{
				rtnVal = RP_TRUE;
			}
		}
		else if ((RpCb.pib.phyRegulatoryMode == RP_RMODE_ENABLE_ARIB) && (RpCb.pib.phyFreqBandId == RP_PHY_FREQ_BAND_920MHz))
		{
			totalLen = RpCalcTotalBytes(len, &sduLen, useRxFcsLength);

			if (RpCb.pib.phyFskOpeMode == RP_PHY_FSK_OPEMODE_4)
			{
				if (totalLen > RP_MAXVAL_FRAMELEN_ARIB * 2)
				{
					rtnVal = RP_TRUE;
				}
			}
			else
			{
				if (totalLen > RP_MAXVAL_FRAMELEN_ARIB)
				{
					rtnVal = RP_TRUE;
				}
			}
		}
	}
	return (rtnVal);
}

/******************************************************************************
Function Name:       RpTC0SetReg
Parameters:          csmaStaTC0Ena: TC0 Start CSMA-CA
					 time: transmit time
Return value:		 none
Description:         TC0 register setting.
******************************************************************************/
void
RpTC0SetReg(uint8_t csmaStaEna, uint32_t time)
{
	RpRegBlockWrite(BBTCOMP0REG0, (uint8_t RP_FAR *)&time, sizeof(uint32_t));
	RpCb.reg.bbIntEn[0] |= TIM0INTEN;
	RpRegWrite(BBINTEN0, (uint8_t)(RpCb.reg.bbIntEn[0]));
	RpCb.reg.bbTimeCon &= ~(COMP0TRG | COMP0TRGSEL);
	if (csmaStaEna)
	{
		RpCb.reg.bbTimeCon |= (COMP0TRG | COMP0TRGSEL);	// CSMA-CA starts timer0 trigger
	}
	else
	{
		RpCb.reg.bbTimeCon |= COMP0TRG;					// Tx starts timer0 trigger
	}
	RpRegWrite(BBTIMECON, RpCb.reg.bbTimeCon);
}

/******************************************************************************
Function Name:       RpSetTxTriggerTimer
Parameters:          willTxTime: will transmit time
					 pRealTxTime: if so
					 onRetrn: on Retransmission
					 willTotalTxLength: will transmit total length
Return value:		 need delay time
Description:         set tx timer trigger with check tx interval time.
******************************************************************************/
uint8_t
RpSetTxTriggerTimer(uint32_t willTxTime, uint8_t onRetrn, uint8_t options)
{
	uint8_t  needDelayTime = RP_FALSE;
	uint32_t realTime;
	uint32_t difTime, difTime2, nowTime;
	uint8_t waitTilIntervalTime = RP_FALSE;
	uint8_t csmaStEna;
	uint8_t freqId = RpCb.freqIdIndex;

	if ((RpCb.tx.needTxWaitRegulatoryMode) &&
		((RpCb.pib.phyRegulatoryMode == RP_RMODE_ENABLE) ||
		 ((RpCb.pib.phyRegulatoryMode == RP_RMODE_ENABLE_ARIB) && (RpCb.pib.phyFreqBandId == RP_PHY_FREQ_BAND_920MHz))))
	{
		realTime = RpCb.tx.sentTime;
		if (RpCb.pib.phyRegulatoryMode == RP_RMODE_ENABLE)
		{
			difTime = (uint32_t)(RpCb.pib.phyRmodeToffMin) * (uint32_t)(RpCb.pib.phyDataRate);
		}
		else
		{
			difTime = RpCalcTxInterval(RpCb.tx.sentLen, onRetrn);
		}
		realTime += (uint32_t)difTime;
		realTime &= (uint32_t)RP_TIME_MASK;
		difTime2 = (uint32_t)((willTxTime - realTime /* tx time considered interval time */) & RP_TIME_MASK);
		if ((difTime2 > RP_TIME_LIMIT) || (difTime2 < RP_RETX_TIMER_WASTE_TIME))
		{
			waitTilIntervalTime = RP_TRUE;
		}
	}
	if (waitTilIntervalTime == RP_FALSE)
	{
		realTime = willTxTime;
	}
	if ((waitTilIntervalTime == RP_TRUE) || (options & RP_TX_TIME))
	{
		nowTime = RpGetTime();
		difTime = (realTime - nowTime) & RP_TIME_MASK;

		if ((difTime > RP_TIME_LIMIT) || (difTime < (uint32_t)RpTc0WasteTime[freqId]))
		{
			realTime = (uint32_t)((nowTime + (uint32_t)RpTc0WasteTime[freqId]) & RP_TIME_MASK);
		}
		needDelayTime = RP_TRUE;
	}
	if (needDelayTime == RP_TRUE)
	{
		// set TC0 for TX or CSMA-CA
		csmaStEna = (uint8_t)((RpCb.status & RP_PHY_STAT_TX_CCA) ? RP_TRUE : RP_FALSE);
		RpTC0SetReg(csmaStEna, realTime);			// TX trigger
		nowTime = RpGetTime();
		difTime = (realTime - nowTime) & RP_TIME_MASK;

		if ((difTime > RP_TIME_LIMIT) || (difTime == 0))
		{
			if (RpCheckRfIRQ() == RP_FALSE)
			{
				RpCb.reg.bbIntEn[0] &= ~TIM0INTEN;
				RpRegWrite(BBINTEN0,	(uint8_t)(RpCb.reg.bbIntEn[0]));
				RpCb.reg.bbTimeCon &= ~(COMP0TRG | COMP0TRGSEL);
				RpRegWrite(BBTIMECON, RpCb.reg.bbTimeCon);
				needDelayTime = RP_FALSE;
			}
		}
	}

	return (needDelayTime);
}

/******************************************************************************
Function Name:       RpCalcTxInterval
Parameters:          totalTxlength:
Return value:		 intervall time
Description:         calc tx interval time.
******************************************************************************/
uint32_t
RpCalcTxInterval(uint16_t totalTxLength, uint8_t onRetrn)
{
	uint32_t sendTime = RpCsmaBytesToTime(totalTxLength);
	uint16_t symbol = RpCb.pib.phyDataRate;
	uint8_t fskMode = RpCb.pib.phyFskOpeMode;
	uint8_t freqBandId = RpCb.pib.phyFreqBandId;
	uint32_t difTime = 0;

	if (RpCb.pib.phyCurrentChannel < RP_PHY_MIN_CHANNEL_VALID_ARIB_MODE)
	{
		if (onRetrn == RP_FALSE)
		{
			difTime = ((uint32_t)(RP_PHY_TX_INTERVAL_TIME_LOW_CHANNEL * symbol) / (uint32_t)RP_PHY_DATA_RATE_BPS_UNIT);
		}
	}
	else if (freqBandId == RP_PHY_FREQ_BAND_920MHz)
	{
		if ((fskMode == RP_PHY_FSK_OPEMODE_1) || (fskMode == RP_PHY_FSK_OPEMODE_6))
		{
			if ((sendTime > RP_PHY_SENT_FSK1_MIDDLE_FRAME_LOWER_LIMIT) && (sendTime <= RP_PHY_SENT_FSK1_MIDDLE_FRAME_UPPER_LIMIT))
			{
				difTime = ((uint32_t)(RP_PHY_TX_INTERVAL_TIME_SHORT_FRAME * symbol) / (uint32_t)RP_PHY_DATA_RATE_BPS_UNIT);
			}
			else if ((sendTime > RP_PHY_SENT_FSK1_LONG_FRAME_LOWER_LIMIT) && (sendTime <= RP_PHY_SENT_FSK1_LONG_FRAME_UPPER_LIMIT))
			{
				difTime = ((uint32_t)(((sendTime * RP_PHY_TX_INTERVAL_LONG_FRAME_10TIMES) * symbol)) / (uint32_t)(RP_PHY_DATA_RATE_BPS_UNIT * 10));
			}
		}
		else if ((fskMode == RP_PHY_FSK_OPEMODE_2) || (fskMode == RP_PHY_FSK_OPEMODE_5))
		{
			if ((sendTime > RP_PHY_SENT_FSK2_LONG_FRAME_LOWER_LIMIT) && (sendTime <= RP_PHY_SENT_FSK2_LONG_FRAME_UPPER_LIMIT))
			{
				difTime = ((uint32_t)(RP_PHY_TX_INTERVAL_TIME_SHORT_FRAME * symbol) / (uint32_t)RP_PHY_DATA_RATE_BPS_UNIT);
			}
		}
		else if ((fskMode == RP_PHY_FSK_OPEMODE_3) || (fskMode == RP_PHY_FSK_OPEMODE_4))
		{
			if ((sendTime > RP_PHY_SENT_FSK3_LONG_FRAME_LOWER_LIMIT) && (sendTime <= RP_PHY_SENT_FSK3_LONG_FRAME_UPPER_LIMIT))
			{
				difTime = ((uint32_t)(RP_PHY_TX_INTERVAL_TIME_SHORT_FRAME * symbol) / (uint32_t)RP_PHY_DATA_RATE_BPS_UNIT);
			}
		}
	}

	return (difTime);
}

/***************************************************************************
 * Name			: RpCalcTotalBytes
 * Parameters	: dataPayloadLength
                : useRxFcsLength : 0:FCS length is RpCb.pib.phyFcsLengt.
                :                : 1:FCS length is RpCb.rx.fcsLength.
 * Returns		: Total frame byte
 * Description	:
 **************************************************************************/
uint16_t
RpCalcTotalBytes(uint16_t dataPayloadLength, uint16_t *pSduLen, uint8_t useRxFcsLength)
{
	uint16_t preambleLength = RpCb.pib.phyFskPreambleLength;
	uint16_t tailAndPaddingLength = 0;
	uint8_t fecTxEna = RpCb.pib.phyFskFecTxEna;
	uint8_t fcsLength = RpCb.pib.phyFcsLength;
	uint8_t freqBandId = RpCb.pib.phyFreqBandId;
	uint8_t opeMode = RpCb.pib.phyFskOpeMode;
	uint8_t sfdLength = RP_SFD_LEN_2;
	uint8_t preLength = RP_PHR_LEN;
	uint8_t freqId = RpCb.freqIdIndex;

	if (useRxFcsLength == RP_TRUE)
	{
		fcsLength = RpCb.rx.fcsLength;
	}
	if (fecTxEna == RP_TRUE)
	{
		if (dataPayloadLength % 2)
		{
			tailAndPaddingLength = (1 << (uint16_t)1);
		}
		else
		{
			tailAndPaddingLength = (2 << (uint16_t)1);
		}
		dataPayloadLength <<= (uint16_t)1;
		fcsLength <<= (uint8_t)1;
		preLength <<= (uint8_t)1;
	}

	if (((freqBandId == RP_PHY_FREQ_BAND_920MHz) && (opeMode == RP_PHY_FSK_OPEMODE_4))
			|| ((freqBandId == RP_PHY_FREQ_BAND_863MHz) && (opeMode == RP_PHY_FSK_OPEMODE_3)))
	{
		sfdLength = RP_SFD_LEN_4;
	}

	*pSduLen = (uint16_t)(dataPayloadLength + fcsLength + tailAndPaddingLength);

	return ((uint16_t)(preambleLength + sfdLength + preLength + *pSduLen + RpRampUpDownStableTotalByte[freqId]));
}

/***************************************************************************
 * Name			: RpCsmaBytesToTime
 * Parameters	: txTotalLength
 * Returns		: frame time [usec]
 * Description	:
 **************************************************************************/
static uint32_t RpCsmaBytesToTime( uint16_t txTotalLength )
{
	return ((uint32_t)(txTotalLength) * 8 * 1000 / (uint32_t)(RpCb.pib.phyDataRate));
}

/******************************************************************************
Function Name:       RpTrnxHdrFunc
Parameters:          trnxNo:transmitted rambank number: 0 or 1
Return value:        none
Description:         this function is called trn0/1 interrupt handler.
******************************************************************************/
void
RpTrnxHdrFunc(uint8_t trnxNo)
{
	uint16_t	status;
	uint16_t	len;
	uint8_t	*pData, ramOffset, ramOffsetAddr;
	uint16_t wrAddr;

	status = RpCb.status;
	if (status & RP_PHY_STAT_TX)
	{
		if (RpCb.tx.pNextData != RP_NULL)
		{
			RpRegBlockRead(BBTXCOUNT, (uint8_t *)&len, sizeof(uint16_t));
			if (RpCb.tx.cnt < len)
			{
				RpCb.status |= RP_PHY_STAT_TX_UNDERFLOW;
				RpRfStat.txRamUnderrun++;

				return;
			}
			ramOffset = (uint8_t)(RpCb.tx.cnt % RP_BB_TX_RAM_SIZE);
			if ((RpCb.tx.len - RpCb.tx.cnt) > RP_BB_TX_RAM_SIZE)
			{
				len = (uint16_t)(RP_BB_TX_RAM_SIZE - ramOffset);
				pData = RpCb.tx.pNextData;
				RpCb.tx.pNextData = pData + len;
			}
			else
			{
				len = (uint16_t)(RpCb.tx.len - RpCb.tx.cnt);
				pData = RpCb.tx.pNextData;
				RpCb.tx.pNextData = RP_NULL;
			}
			ramOffsetAddr = (uint8_t)(ramOffset << 3);
			wrAddr = (uint16_t)(trnxNo ? BBTXRAM1 + ramOffsetAddr : BBTXRAM0 + ramOffsetAddr);
			RpRegBlockWrite(wrAddr, (uint8_t RP_FAR *)pData, len);	// Data Set to transmitted RAMx
			RpCb.tx.cnt += len;
			RpRegBlockRead(BBTXCOUNT, (uint8_t *)&len, sizeof(uint16_t));

			if (RpCb.tx.cnt < len)
			{
				RpCb.status |= RP_PHY_STAT_TX_UNDERFLOW;
				RpRfStat.txRamUnderrun++;
			}
		}
	}
}

/***************************************************************************************************************
 * function name  : RpPlmeEdReq
 * description    : ED (Energy Detection) Request Function
 * parameters     : none
 * return value   : RP_PENDING, RP_BUSY_RX, RP_BUSY_TX, RP_BUSY_LOWPOWER
 **************************************************************************************************************/
int16_t RpPlmeEdReq( void )
{
	uint16_t	status;
	int16_t	rtn = RP_PENDING;
	uint8_t edBandwidth = RpCb.pib.phyEdBandwidth;
	uint8_t ccaBandwidth = RpCb.pib.phyCcaBandwidth;
	uint16_t ccaDuration = 31;	// AGC+delay+ED
	uint8_t index = RpCb.freqIdIndex;
	uint8_t reg488h;

	#if defined(__RX)
	uint32_t bkupPsw;
	uint32_t bkupPsw2;
	#elif defined(__CCRL__)
	uint8_t  bkupPsw;
	uint8_t  bkupPsw2;
	#elif defined(__arm)
	uint8_t bkupPsw[RP_INT_BKUP_BUF_SIZE];
	uint8_t bkupPsw2[RP_INT_BKUP_BUF_SIZE];
	#endif

	/* Disable interrupt */
	#if defined(__RX) || defined(__CCRL__) || defined(__arm)
	RP_PHY_DI(bkupPsw);
	#else
	RP_PHY_DI();
	#endif

	/* API function execution Log */
	RpLog_Event( RP_LOG_API_ED, RP_NULL );

	// status check
	status = RpCb.status;

	if ((status & RP_PHY_STAT_TRX_OFF) == 0)
	{
		rtn = RpCurStatCheck(status);
	}
	else
	{
		RpCb.status = (RP_PHY_STAT_ED | RP_PHY_STAT_BUSY);	// set ED Mode Set
		RpClrAutoRxFunc();		//	clear auto functions
		RpSetCcaDurationVal(RP_TRUE);
		RpInverseTxAnt(RP_TRUE);
		RpRegWrite(BBCSMACON3, 0x01);
		RpCb.reg.bbTxRxMode4 = 0x00;
		RpRegWrite(BBTXRXMODE4, (uint8_t)(RpCb.reg.bbTxRxMode4));
		RpCb.reg.bbTxRxMode1 &= ~CCASEL;	// CCA/ED
		RpRegWrite(BBTXRXMODE1, (uint8_t)(RpCb.reg.bbTxRxMode1));
		RpCb.reg.bbIntEn[0] |= CCAINTEN;
		RpRegWrite(BBINTEN0,	(uint8_t)(RpCb.reg.bbIntEn[0]));

		if ((index != RP_PHY_100K_M10_JP) && (index != RP_PHY_100K_M10_IN)) // except for 100kbps m=1
		{
			if ((edBandwidth == RP_PHY_CCA_BANDWIDTH_INDEX_200K) || (edBandwidth == RP_PHY_CCA_BANDWIDTH_INDEX_150K)) // filter change case
			{
				RpRegTxRxDataRate100kbps();
				RpRegBlockWrite(BBCCATIME, (uint8_t RP_FAR *)&ccaDuration, sizeof(uint16_t));
				reg488h = RpRegRead(0x0488 << 3);

				if (reg488h == RP_PHY_REG488H_DEFAULT)
				{
					RpRegRxDataRate100kbps();
				}
			}
			else if ((ccaBandwidth == RP_PHY_CCA_BANDWIDTH_INDEX_200K) || (ccaBandwidth == RP_PHY_CCA_BANDWIDTH_INDEX_150K)) // && ((edBandwidth == RP_PHY_CCA_BANDWIDTH_INDEX_225K) || (edBandwidth == RP_PHY_CCA_BANDWIDTH_INDEX_NARROW))
			{
				reg488h = RpRegRead(0x0488 << 3);

				if (reg488h != RP_PHY_REG488H_DEFAULT)
				{
					RpRegRxDataRateDefault();
				}
			}
			else // ((edBandwidth == RP_PHY_CCA_BANDWIDTH_INDEX_225K) && (ccaBandwidth == RP_PHY_CCA_BANDWIDTH_INDEX_225K))
			{
				// Setting at PIB
			}
		}

		/* Disable interrupt */
		#if defined(__RX) || defined(__CCRL__) || defined(__arm)
		RP_PHY_ALL_DI(bkupPsw2);
		#else
		RP_PHY_ALL_DI();
		#endif

		RpRegWrite(BBTXRXCON, CCATRG);	// cca trigger enable

		switch (edBandwidth)
		{
			case RP_PHY_CCA_BANDWIDTH_INDEX_225K:
				switch (ccaBandwidth)
				{
					case RP_PHY_CCA_BANDWIDTH_INDEX_200K:
					case RP_PHY_CCA_BANDWIDTH_INDEX_150K:
						RpRegCcaBandwidth225k();
						RpRegAdcVgaDefault();
						break;
					case RP_PHY_CCA_BANDWIDTH_INDEX_NARROW:
						RpChangeDefaultFilter();
						break;
					default:
						break;
				}
				break;
			case RP_PHY_CCA_BANDWIDTH_INDEX_200K:
				RpRegCcaBandwidth200k();
				RpRegAdcVgaModify();
				break;
			case RP_PHY_CCA_BANDWIDTH_INDEX_150K:
				RpRegCcaBandwidth150k();
				RpRegAdcVgaModify();
				break;
			case RP_PHY_CCA_BANDWIDTH_INDEX_NARROW:
				RpChangeNarrowBandFilter();
				break;
			default:
				break;
		}

		/* Enable interrupt */
		#if defined(__RX) || defined(__CCRL__) || defined(__arm)
		RP_PHY_ALL_EI(bkupPsw2);
		#else
		RP_PHY_ALL_EI();
		#endif
	}

	/* API function execution Log */
	RpLog_Event( RP_LOG_API_ED | RP_LOG_API_RET, (uint8_t)rtn );

	/* Enable interrupt */
	#if defined(__RX) || defined(__CCRL__) || defined(__arm)
	RP_PHY_EI(bkupPsw);
	#else
	RP_PHY_EI();
	#endif

	return (rtn);
}

/***************************************************************************************************************
 * function name  : RpPlmeCcaReq
 * description    : CCA Request Function
 * parameters     : none
 * return value   : RP_PENDING, RP_BUSY_RX, RP_BUSY_TX, RP_BUSY_LOWPOWER
 **************************************************************************************************************/
int16_t RpPlmeCcaReq( void )
{
	uint16_t	status;
	int16_t	rtn = RP_PENDING;
	uint8_t edBandwidth = RpCb.pib.phyEdBandwidth;
	uint8_t ccaBandwidth = RpCb.pib.phyCcaBandwidth;
	uint8_t index = RpCb.freqIdIndex;
	uint8_t reg488h;

	#if defined(__RX)
	uint32_t bkupPsw;
	uint32_t bkupPsw2;
	#elif defined(__CCRL__)
	uint8_t  bkupPsw;
	uint8_t  bkupPsw2;
	#elif defined(__arm)
	uint8_t bkupPsw[RP_INT_BKUP_BUF_SIZE];
	uint8_t bkupPsw2[RP_INT_BKUP_BUF_SIZE];
	#endif

	/* Disable interrupt */
	#if defined(__RX) || defined(__CCRL__) || defined(__arm)
	RP_PHY_DI(bkupPsw);
	#else
	RP_PHY_DI();
	#endif

	/* API function execution Log */
	RpLog_Event( RP_LOG_API_CCA, RP_NULL );

	// status check
	status = RpCb.status;
	if ((status & RP_PHY_STAT_TRX_OFF) == 0)
	{
		rtn = RpCurStatCheck(status);
	}
	else
	{
		RpCb.status = (RP_PHY_STAT_CCA | RP_PHY_STAT_BUSY);	// set CCA Mode Set
		RpClrAutoRxFunc();		//	clear auto functions
		RpSetCcaDurationVal(RP_TRUE);
		RpInverseTxAnt(RP_TRUE);
		RpRegWrite(BBCSMACON3, 0x01);
		RpCb.reg.bbTxRxMode4 = 0x00;
		RpRegWrite(BBTXRXMODE4, (uint8_t)(RpCb.reg.bbTxRxMode4));
		RpCb.reg.bbTxRxMode1 &= ~CCASEL;	// CCA/ED
		RpRegWrite(BBTXRXMODE1, (uint8_t)(RpCb.reg.bbTxRxMode1));
		RpCb.reg.bbIntEn[0] |= CCAINTEN;
		RpRegWrite(BBINTEN0,	(uint8_t)(RpCb.reg.bbIntEn[0]));

		if ((index != RP_PHY_100K_M10_JP) && (index != RP_PHY_100K_M10_IN)) // except for 100kbps m=1
		{
			if ((ccaBandwidth == RP_PHY_CCA_BANDWIDTH_INDEX_200K) || (ccaBandwidth == RP_PHY_CCA_BANDWIDTH_INDEX_150K))	// filter change case
			{
				RpRegTxRxDataRate100kbps();
				reg488h = RpRegRead(0x0488 << 3);

				if (reg488h == RP_PHY_REG488H_DEFAULT)
				{
					RpRegRxDataRate100kbps();
				}
			}
			else if ((edBandwidth == RP_PHY_CCA_BANDWIDTH_INDEX_200K) || (edBandwidth == RP_PHY_CCA_BANDWIDTH_INDEX_150K)) // && ((ccaBandwidth == RP_PHY_CCA_BANDWIDTH_INDEX_225K) || (ccaBandwidth == RP_PHY_CCA_BANDWIDTH_INDEX_NARROW))
			{
				reg488h = RpRegRead(0x0488 << 3);

				if (reg488h != RP_PHY_REG488H_DEFAULT)
				{
					RpRegRxDataRateDefault();
				}
			}
			else	// ((ccaBandwidth == RP_PHY_CCA_BANDWIDTH_INDEX_225K) && (edBandwidth == RP_PHY_CCA_BANDWIDTH_INDEX_225K))
			{
				// Setting at PIB
			}
		}

		/* Disable interrupt */
		#if defined(__RX) || defined(__CCRL__) || defined(__arm)
		RP_PHY_ALL_DI(bkupPsw2);
		#else
		RP_PHY_ALL_DI();
		#endif

		RpRegWrite(BBTXRXCON, CCATRG);	// cca trigger enable

		switch (ccaBandwidth)
		{
			case RP_PHY_CCA_BANDWIDTH_INDEX_225K:
				switch (edBandwidth)
				{
					case RP_PHY_CCA_BANDWIDTH_INDEX_200K:
					case RP_PHY_CCA_BANDWIDTH_INDEX_150K:
						RpRegCcaBandwidth225k();
						RpRegAdcVgaDefault();
						break;
					case RP_PHY_CCA_BANDWIDTH_INDEX_NARROW:
						RpChangeDefaultFilter();
						break;
					default:
						break;
				}
				break;
			case RP_PHY_CCA_BANDWIDTH_INDEX_200K:
				RpRegCcaBandwidth200k();
				RpRegAdcVgaModify();
				break;
			case RP_PHY_CCA_BANDWIDTH_INDEX_150K:
				RpRegCcaBandwidth150k();
				RpRegAdcVgaModify();
				break;
			case RP_PHY_CCA_BANDWIDTH_INDEX_NARROW:
				RpChangeNarrowBandFilter();
				break;
			default:
				break;
		}

		/* Enable interrupt */
		#if defined(__RX) || defined(__CCRL__) || defined(__arm)
		RP_PHY_ALL_EI(bkupPsw2);
		#else
		RP_PHY_ALL_EI();
		#endif
	}

	/* API function execution Log */
	RpLog_Event( RP_LOG_API_CCA | RP_LOG_API_RET, (uint8_t)rtn );

	/* Enable interrupt */
	#if defined(__RX) || defined(__CCRL__) || defined(__arm)
	RP_PHY_EI(bkupPsw);
	#else
	RP_PHY_EI();
	#endif

	return (rtn);
}

/******************************************************************************
Function Name:       RpSetLowPowerProhibit
Parameters:          none
Return value:        none
Description:         set prohibit-bit to go to low power mode
******************************************************************************/
void
RpSetLowPowerProhibit(void)
{
	RpCb.prohibitLowPower = RP_TRUE;
}

/******************************************************************************
Function Name:       RpClrLowPowerProhibit
Parameters:          none
Return value:        none
Description:         clear prohibit-bit to go to low power mode
******************************************************************************/
void
RpClrLowPowerProhibit(void)
{
	RpCb.prohibitLowPower = RP_FALSE;
}

/******************************************************************************
Function Name:       RpGetLowPowerProhibit
Parameters:          none
Return value:        none
Description:         refer prohibit-bit to go to low power mode
******************************************************************************/
uint8_t
RpGetLowPowerProhibit(void)
{
	uint8_t rtn;

	rtn = RpCb.prohibitLowPower;

	return (rtn);
}

/***************************************************************************************************************
 * function name  : RpSetPowerMode
 * description    : Base Band Unit Power Mode Setting
 * parameters     : nxRf...Specify the power mode of the base band unit.
 * return value   : RP_SUCCESS, RP_SAME_STATUS, RP_INVALID_PARAMETER, RP_BUSY_RX, RP_BUSY_TX, RP_BUSY_OTHER
 **************************************************************************************************************/
int16_t RpSetPowerMode( uint8_t nxtRf )
{
	int16_t rtn;
	uint16_t status;

	#if defined(__RX)
	uint32_t bkupPsw;
	#elif defined(__CCRL__)
	uint8_t  bkupPsw;
	#elif defined(__arm)
	uint8_t bkupPsw[RP_INT_BKUP_BUF_SIZE];
	#endif

	/* Disable interrupt */
	#if defined(__RX) || defined(__CCRL__) || defined(__arm)
	RP_PHY_DI(bkupPsw);
	#else
	RP_PHY_DI();
	#endif

	/* API function execution Log */
	RpLog_Event( RP_LOG_API_SETPOW, RP_NULL );

	if ((nxtRf != RP_RF_IDLE) && (nxtRf != RP_RF_LOWPOWER))
	{
		/* API function execution Log */
		RpLog_Event( RP_LOG_API_SETPOW | RP_LOG_API_RETERR, RP_INVALID_PARAMETER );

		/* Enable interrupt */
		#if defined(__RX) || defined(__CCRL__) || defined(__arm)
		RP_PHY_EI(bkupPsw);
		#else
		RP_PHY_EI();
		#endif

		return (RP_INVALID_PARAMETER);
	}

	status = RpCb.status;
	if (status & RP_PHY_STAT_TRX_OFF)
	{
		if (nxtRf == RP_RF_LOWPOWER)
		{
			if (RpCb.prohibitLowPower == RP_FALSE)
			{
				RpSetMcuInt(RP_FALSE);			// RF interrupts request clear and disable
				RpRegWrite(BBRFCON, RP_RF_POWEROFF);

				if ((RpCb.pib.phyRegulatoryMode == RP_RMODE_ENABLE) ||
					((RpCb.pib.phyRegulatoryMode == RP_RMODE_ENABLE_ARIB) && (RpCb.pib.phyFreqBandId == RP_PHY_FREQ_BAND_920MHz)))
				{
					RpPrevSentTimeReSetting();
					RpCb.reg.bbTimeCon &= ~(TIMEEN | COMP0TRG);	// Timer Count Disable, Comp0 transmit Disable
					RpRegWrite(BBTIMECON, (uint8_t)(RpCb.reg.bbTimeCon));
					RpCb.txLmtTimer.lackTime += RpLimitTimerControl(RP_FALSE);
#if defined(__RL78__)
					if (RpCb.txLmtTimer.lackTime > (uint32_t)1000)
					{
						RpCb.txLmtTimer.lackTime -= (uint32_t)1000;
						RpTxLimitTimerForward(1);
					}
#endif
				}
				RpPowerdownSequence();
				RpCb.status = RP_PHY_STAT_LOWPOWER;
				rtn = RP_SUCCESS;
			}
			else // if(RpCb.prohibitLowPower == RP_TRUE)
			{
				rtn = RP_BUSY_OTHER;
			}
		}
		else // if (nxtrf == RP_RF_IDLE)
		{
			rtn = RP_SAME_STATUS;
		}
	}
	else if	(status & RP_PHY_STAT_LOWPOWER)
	{
		if (nxtRf == RP_RF_IDLE)
		{
			RpInitRfic();				// Restart of MAC
			RpSetMcuInt(RP_TRUE);		// RF interrupts request clear and enable
			RpSetRfInt(RP_TRUE);

			if ((RpCb.pib.phyRegulatoryMode == RP_RMODE_ENABLE) ||
				((RpCb.pib.phyRegulatoryMode == RP_RMODE_ENABLE_ARIB) && (RpCb.pib.phyFreqBandId == RP_PHY_FREQ_BAND_920MHz)))
			{
				RpCb.reg.bbTimeCon |= TIMEEN;	// Timer Count Start, Comp0 transmit Disable Stay
				RpRegWrite(BBTIMECON, (uint8_t)(RpCb.reg.bbTimeCon));
				RpLimitTimerControl(RP_TRUE);
			}
			RpCb.status = RP_PHY_STAT_TRX_OFF;
			rtn = RP_SUCCESS;
		}
		else // if (nxtRf == RP_RF_LOWPOWER)
		{
			rtn = RP_SAME_STATUS;
		}
	}
	else if (status & RP_PHY_STAT_TX)
	{
		rtn = RP_BUSY_TX;
	}
	else
	{
		rtn = RP_BUSY_RX;
	}

	/* API function execution Log */
	RpLog_Event( RP_LOG_API_SETPOW | RP_LOG_API_RET, (uint8_t)rtn );

	/* Enable interrupt */
	#if defined(__RX) || defined(__CCRL__) || defined(__arm)
	RP_PHY_EI(bkupPsw);
	#else
	RP_PHY_EI();
	#endif

	return (rtn);
}

/***************************************************************************************************************
 * function name  : RpGetPowerMode
 * description    : Base Band Unit Power Mode Reference
 * parameters     : none
 * return value   : RP_RF_IDLE, RP_RF_LOWPOWER
 **************************************************************************************************************/
uint16_t RpGetPowerMode( void )
{
	uint16_t lpMode = RP_RF_IDLE;
	uint16_t status;

	#if defined(__RX)
	uint32_t bkupPsw;
	#elif defined(__CCRL__)
	uint8_t  bkupPsw;
	#elif defined(__arm)
	uint8_t bkupPsw[RP_INT_BKUP_BUF_SIZE];
	#endif

	/* Disable interrupt */
	#if defined(__RX) || defined(__CCRL__) || defined(__arm)
	RP_PHY_DI(bkupPsw);
	#else
	RP_PHY_DI();
	#endif

	/* API function execution Log */
	RpLog_Event( RP_LOG_API_GETPOW, RP_NULL );

	status = RpCb.status;
	if (status & RP_PHY_STAT_LOWPOWER)
	{
		lpMode = RP_RF_LOWPOWER;
	}

	/* API function execution Log */
	RpLog_Event( RP_LOG_API_GETPOW | RP_LOG_API_RET, (uint8_t)lpMode );

	/* Enable interrupt */
	#if defined(__RX) || defined(__CCRL__) || defined(__arm)
	RP_PHY_EI(bkupPsw);
	#else
	RP_PHY_EI();
	#endif

	return (lpMode);
}

/***************************************************************************************************************
 * function name  : RpGetTime
 * description    : Present Time Acquisition
 * parameters     : none
 * return value   : Present time of the base band unit
 **************************************************************************************************************/
uint32_t RpGetTime( void )
{
	uint32_t time;
	uint16_t status;

	#if defined(__RX)
	uint32_t bkupPsw;
	#elif defined(__CCRL__)
	uint8_t  bkupPsw;
	#elif defined(__arm)
	uint8_t bkupPsw[RP_INT_BKUP_BUF_SIZE];
	#endif

	/* Disable interrupt */
	#if defined(__RX) || defined(__CCRL__) || defined(__arm)
	RP_PHY_DI(bkupPsw);
	#else
	RP_PHY_DI();
	#endif

	/* get current status */
	status = RpCb.status;

	/* check current status */
	if ( status & RP_PHY_STAT_LOWPOWER )
	{
		time = 0xFFFFFFFF;	// busy lowpower
	}
	else
	{
		RpRegBlockRead(BBTIMEREAD0, (uint8_t *)&time, sizeof(uint32_t));
	}

	/* Enable interrupt */
	#if defined(__RX) || defined(__CCRL__) || defined(__arm)
	RP_PHY_EI(bkupPsw);
	#else
	RP_PHY_EI();
	#endif

	return (time & RP_TIME_MASK);
}

/***************************************************************************************************************
 * function name  : RptConTxNoModu
 * description    : Continuous Unmodulated Transmission <for RF Characteristic Evaluation>
 * parameters     : none
 * return value   : none
 **************************************************************************************************************/
void RptConTxNoModu( void )
{
	uint16_t txFlen = 5;

	#if defined(__RX)
	uint32_t bkupPsw;
	#elif defined(__CCRL__)
	uint8_t  bkupPsw;
	#elif defined(__arm)
	uint8_t bkupPsw[RP_INT_BKUP_BUF_SIZE];
	#endif

	/* Disable interrupt */
	#if defined(__RX) || defined(__CCRL__) || defined(__arm)
	RP_PHY_DI(bkupPsw);
	#else
	RP_PHY_DI();
	#endif

	RpCb.status = RP_PHY_STAT_TX | RP_PHY_STAT_TRX_CONTINUOUS;
	RpRegBlockWrite(BBTXFLEN, (uint8_t RP_FAR *)&txFlen, sizeof(uint16_t));
	RpRegWrite(BBEVAREG, CONTTX | NOMOD);
	RpRegWrite(BBTXRXCON, TRNTRG);				// transmit trigger enable

	#if defined(__arm)
   	RpLedTxOff();
	#endif

	/* Enable interrupt */
	#if defined(__RX) || defined(__CCRL__) || defined(__arm)
	RP_PHY_EI(bkupPsw);
	#else
	RP_PHY_EI();
	#endif
}

/***************************************************************************************************************
 * function name  : RptConTrxStop
 * description    : Continuous Transmission/Reception Stop <for RF Characteristic Evaluation>
 * parameters     : none
 * return value   : none
 **************************************************************************************************************/
void RptConTrxStop( void )
{
	#if defined(__RX)
	uint32_t bkupPsw;
	#elif defined(__CCRL__)
	uint8_t  bkupPsw;
	#elif defined(__arm)
	uint8_t bkupPsw[RP_INT_BKUP_BUF_SIZE];
	#endif

	/* Disable interrupt */
	#if defined(__RX) || defined(__CCRL__) || defined(__arm)
	RP_PHY_DI(bkupPsw);
	#else
	RP_PHY_DI();
	#endif

	RpRxOnStop();
	RpRegWrite(BBEVAREG, 0x00);
	RpSetPowerMode(RP_RF_LOWPOWER);	// for RF reset
	RpSetPowerMode(RP_RF_IDLE);		// for RF reset
	RpCb.status = RP_PHY_STAT_TRX_OFF;

	/* Enable interrupt */
	#if defined(__RX) || defined(__CCRL__) || defined(__arm)
	RP_PHY_EI(bkupPsw);
	#else
	RP_PHY_EI();
	#endif
}

/***************************************************************************************************************
 * function name  : RptPulseTxPrbs9
 * description    : PN9 Continuous Modulated Transmission <for RF Characteristic Evaluation>
 * parameters     : none
 * return value   : none
 **************************************************************************************************************/
void RptPulseTxPrbs9( void )
{
	uint16_t txFlen;

	#if defined(__RX)
	uint32_t bkupPsw;
	#elif defined(__CCRL__)
	uint8_t  bkupPsw;
	#elif defined(__arm)
	uint8_t bkupPsw[RP_INT_BKUP_BUF_SIZE];
	#endif

	/* Disable interrupt */
	#if defined(__RX) || defined(__CCRL__) || defined(__arm)
	RP_PHY_DI(bkupPsw);
	#else
	RP_PHY_DI();
	#endif

	RpRegWrite(BBTXRAM0, 0x00);														// psdu is a 0x00 only
	RpCb.reg.bbSubCon |= DWEN;	// forced DW enable
	RpRegWrite(BBSUBGCON, (uint8_t)(RpCb.reg.bbSubCon));
	txFlen = (uint16_t)(RpCb.pib.phyFcsLength + 1/* need one byte + FCS Len */);	// 0x00(1byte) + FCS Length
	RpRegBlockWrite(BBTXFLEN, (uint8_t RP_FAR *)&txFlen, sizeof(uint16_t));
	RpCb.status = RP_PHY_STAT_TX | RP_PHY_STAT_TRX_CONTINUOUS;
	RpRegWrite(BBEVAREG, CONTTX);
	RpRegWrite(BBTXRXCON, TRNTRG);				// transmit trigger enable

	#if defined(__arm)
   	RpLedTxOff();
	#endif

	/* Enable interrupt */
	#if defined(__RX) || defined(__CCRL__) || defined(__arm)
	RP_PHY_EI(bkupPsw);
	#else
	RP_PHY_EI();
	#endif
}

/******************************************************************************
Function Name:       RpSetAdrfAndAutoAckVal
Parameters:          none
Return value:        none
Description:         set register for Address filter and Auto Ack.
******************************************************************************/
void RpSetAdrfAndAutoAckVal( void )
{
	uint8_t addressFilter1 = RpCb.pib.macAddressFilter1Ena;
	uint8_t addressFilter2 = RpCb.pib.macAddressFilter2Ena;
	uint8_t regConvert = RP_FALSE;
	uint16_t byteRcvNum = RP_PHY_RX_ADDRESS_DECODE_LEN;

	RpCb.rx.softwareAdf = RP_FALSE;
	if (RpCb.callback.pAckCheckCallback != RpAckCheckCallback)
	{
		RpCb.rx.softwareAdf = RP_TRUE;
		RpCb.reg.bbTxRxMode3 &= ~ADRSFILEN;
		RpCb.reg.bbTxRxMode0 &= ~AUTOACKEN;
		RpCb.reg.bbTxRxMode3 &= ~ADFEXTEN;
		RpRegBlockWrite(BBRCVINTCOMP, (uint8_t RP_FAR *)&byteRcvNum, sizeof(byteRcvNum));
	}
	else
	{
		if ((addressFilter1 == RP_FALSE) && (addressFilter2 == RP_FALSE))
		{
			RpCb.reg.bbTxRxMode3 &= ~ADRSFILEN;
			RpCb.reg.bbTxRxMode0 &= ~AUTOACKEN;
		}
		else
		{
			if (((addressFilter1 == RP_TRUE) && (RpCb.pib.macPanId1 == 0xFFFF)) ||
					((addressFilter2 == RP_TRUE) && (RpCb.pib.macPanId2 == 0xFFFF)))
			{
				RpCb.rx.softwareAdf = RP_TRUE;
				RpCb.reg.bbTxRxMode3 &= ~ADRSFILEN;
				RpCb.reg.bbTxRxMode0 &= ~AUTOACKEN;
			}
			else
			{
				RpCb.reg.bbTxRxMode3 |= ADRSFILEN;
				RpCb.reg.bbTxRxMode0 |= AUTOACKEN;
			}
		}

		if ((addressFilter1 == RP_FALSE) && (addressFilter2 == RP_TRUE))
		{
			regConvert = RP_TRUE;
		}
		RpSetAdrfRegConverion(regConvert);
		if ((addressFilter1 == RP_TRUE) && (addressFilter2 == RP_TRUE))
		{
			RpCb.reg.bbTxRxMode3 |= ADFEXTEN;
		}
		else
		{
			RpCb.reg.bbTxRxMode3 &= ~ADFEXTEN;
		}
		if (RpCb.rx.softwareAdf == RP_TRUE)
		{
			RpRegBlockWrite(BBRCVINTCOMP, (uint8_t RP_FAR *)&byteRcvNum, sizeof(byteRcvNum));
		}
	}
	RpRegWrite(BBTXRXMODE0, (uint8_t)(RpCb.reg.bbTxRxMode0));
	RpRegWrite(BBTXRXMODE3, (uint8_t)(RpCb.reg.bbTxRxMode3));
}

/******************************************************************************
Function Name:       RpSetAdrfRegConverion
Parameters:          convert
Return value:        none
Description:         convert Adrf1 and Adrf2 register.
******************************************************************************/
static void RpSetAdrfRegConverion( uint8_t convert )
{
	uint8_t adfCon;

	adfCon = (RpRegRead(BBADFCON) & (~(PANCORD2 | FLMPEND2)));
	if (convert)
	{
		RpRegBlockWrite(BBPANID0, (uint8_t RP_FAR *)&RpCb.pib.macPanId2, sizeof(uint16_t));
		RpRegBlockWrite(BBSHORTAD0, (uint8_t RP_FAR *)&RpCb.pib.macShortAddr2, sizeof(uint16_t));
		RpRegBlockWrite(BBEXTENDAD00, (uint8_t RP_FAR *)&RpCb.pib.macExtendedAddress2, sizeof(uint32_t[2]));
		if (RpCb.pib.macPanCoord2)
		{
			RpCb.reg.bbTxRxMode3 |= PANCORD;
		}
		else
		{
			RpCb.reg.bbTxRxMode3 &= ~PANCORD;
		}
		if (RpCb.pib.macPendBit2)
		{
			RpCb.reg.bbTxRxMode2 |= FLMPEND;
		}
		else
		{
			RpCb.reg.bbTxRxMode2 &= ~FLMPEND;
		}
	}
	else
	{
		RpRegBlockWrite(BBPANID0, (uint8_t RP_FAR *)&RpCb.pib.macPanId1, sizeof(uint16_t));
		RpRegBlockWrite(BBSHORTAD0, (uint8_t RP_FAR *)&RpCb.pib.macShortAddr1, sizeof(uint16_t));
		RpRegBlockWrite(BBEXTENDAD00, (uint8_t RP_FAR *)&RpCb.pib.macExtendedAddress1, sizeof(uint32_t[2]));
		if (RpCb.pib.macPanCoord1)
		{
			RpCb.reg.bbTxRxMode3 |= PANCORD;
		}
		else
		{
			RpCb.reg.bbTxRxMode3 &= ~PANCORD;
		}
		if (RpCb.pib.macPendBit1)
		{
			RpCb.reg.bbTxRxMode2 |= FLMPEND;
		}
		else
		{
			RpCb.reg.bbTxRxMode2 &= ~FLMPEND;
		}
		RpRegBlockWrite(BBPANID1, (uint8_t RP_FAR *)&RpCb.pib.macPanId2, sizeof(uint16_t));
		RpRegBlockWrite(BBSHORTAD1, (uint8_t RP_FAR *)&RpCb.pib.macShortAddr2, sizeof(uint16_t));
		RpRegBlockWrite(BBEXTENDAD10, (uint8_t RP_FAR *)&RpCb.pib.macExtendedAddress2, sizeof(uint32_t[2]));
		if (RpCb.pib.macAddressFilter2Ena == RP_TRUE)
		{
			if (RpCb.pib.macPanCoord2)
			{
				adfCon |= PANCORD2;
			}
			if (RpCb.pib.macPendBit2)
			{
				adfCon |= FLMPEND2;
			}
		}
	}
	RpRegWrite(BBTXRXMODE2, (uint8_t)(RpCb.reg.bbTxRxMode2));
	RpRegWrite(BBTXRXMODE3, (uint8_t)(RpCb.reg.bbTxRxMode3));
	RpRegWrite(BBADFCON, adfCon);
}

/******************************************************************************
Function Name:       RpAvailableRcvRamEnable
Parameters:          none:
Return value:        none:
Description:         receive bank(s) enable
******************************************************************************/
void
RpAvailableRcvRamEnable(void)
{
	uint8_t rcvStRst;

	RpRegWrite(BBTXRXST0, 0x00);
	RpCb.rx.unreadRamPrev = RP_FALSE;
	rcvStRst = (uint8_t)((RpRegRead(BBTXRXST1) & RCVSTOREST) ? RP_TRUE : RP_FALSE);
	// Select Privious RcvDataBank
	if (rcvStRst == RP_FALSE)
	{
		RpCb.reg.bbTxRxMode3 &= ~RCVSTOREBANKSEL;
	}
	else
	{
		RpCb.reg.bbTxRxMode3 |= RCVSTOREBANKSEL;
	}
	RpRegWrite(BBTXRXMODE3, RpCb.reg.bbTxRxMode3);
}

/******************************************************************************
Function Name:       RpCurStatCheck
Parameters:          status:current status
Return value:        RP_BUSY_LOWPOWER:Now Lowpower mode
					 RP_BUSY_RX:No transmit or ED or CCA setting because now RX_ON or CCA or ED
					 RP_BUSY_TX:No transmit or ED or CCA setting because now TX_ON
Description:         Current status check.
******************************************************************************/
static int16_t RpCurStatCheck( uint16_t status )
{
	int16_t rtnVal;

	if (status & RP_PHY_STAT_LOWPOWER)
	{
		rtnVal = RP_BUSY_LOWPOWER;
	}
	else if (status & RP_PHY_STAT_TX)
	{
		rtnVal = RP_BUSY_TX;
	}
	else
	{
		// CCA or ED or RX
		rtnVal =  RP_BUSY_RX;
	}

	return (rtnVal);
}

/******************************************************************************
Function Name:       RpChkTmoutTrxStateOff
Parameters:          stat:none
Return value:        RP_ERR_HARDWARE:Fatal Error has occured
					 RP_SUCCESS:phy_status has be RP_TRX_OFF successfully.
					 RP_PENDING:need to RP_TRX_OFF again because it must be Auto Rx
Description:         tmout_trx_state_off.
******************************************************************************/
static void RpChkTmoutTrxStateOff( void )
{
	uint32_t tmoutCnt;
	uint16_t status;

	#if defined(__RX)
	uint32_t bkupPsw;
	#elif defined(__CCRL__)
	uint8_t  bkupPsw;
	#endif

	status = RpCb.status;
	if (status & RP_PHY_STAT_TX)
	{
		tmoutCnt =  RP_TMOUT_CNT_WHEN_SET_TRX_STATE_OFF_FOR_TX;
	}
	else
	{
		tmoutCnt =  RP_TMOUT_CNT_WHEN_SET_TRX_STATE_OFF;
	}
	while (1)
	{
		#if defined(__RX) || defined(__CCRL__)
		RP_PHY_EI_INV(bkupPsw);	/* Enable interrupt */
		RP_PHY_DI_INV(bkupPsw);	/* Disable interrupt */
		#else
		RP_PHY_EI_INV();		/* Enable interrupt */
		RP_PHY_DI_INV();		/* Disable interrupt */
		#endif

		status = RpCb.status;
		if (status & RP_PHY_STAT_TRX_OFF)
		{
			RpSetStateRxOnToTrxOff();
			break;
		}
		else if ((status & RP_PHY_STAT_RX) && RpRxOffBeforeReplyingAck() == RP_TRUE)
		{
			RpSetStateRxOnToTrxOff();
			break;
		}
		else if ((status & RP_PHY_STAT_TX) && (RP_PHY_STAT_TX_BUSY() == 0))
		{
			RpSetStateRxOnToTrxOff();
			break;
		}
		else
		{
			RpWait4us();
			tmoutCnt -= 4;
			if (!tmoutCnt)
			{
				RpSetStateRxOnToTrxOff();
				break;
			}
		}
	}
}

/******************************************************************************
Function Name:       RpSetStateRxOn
Parameters:          options:RX_ON options
					 time:RX_ON start or end time
Return value:        none
Description:         set to RX_ON
 *		this function can be called in only TRX_OFF state
 *      time: lower 16bits are valid
******************************************************************************/
static int16_t RpSetStateRxOn( uint8_t options, uint32_t time )
{
	uint32_t futureTime;

	#if defined(__RX)
	uint32_t bkupPsw;
	#elif defined(__CCRL__)
	uint8_t  bkupPsw;
	#elif defined(__arm)
	uint8_t bkupPsw[RP_INT_BKUP_BUF_SIZE];
	#endif

	RpCb.rx.pTopRxBuf = RpGetRxBuf();
	if (RpCb.rx.pTopRxBuf == RP_NULL)
	{
		/* Callback function execution Log */
		RpLog_Event( RP_LOG_CB, RP_LOG_CB_WARNING_NOTGETBUF );
		/* Callback function execution */
		INDIRECT_WarningIndCallback( RP_WARN_NOT_GET_RXBUF );

		RpRfStat.failToGetRxBuf++;
		return( RP_NOT_GET_RXBUF );
	}
	RpCb.rx.cnt = RP_PHY_RX_STAT_INIT;
	RpCb.status = RP_PHY_STAT_RX;
	RpCb.statusRxTimeout = RP_STATUS_RX_TIMEOUT_INIT;

	RpCb.rx.onReplyingAck = RP_FALSE;
	RpCb.tx.onCsmaCa = RP_FALSE;
	RpCb.reg.bbTxRxMode0 &= (uint8_t)(~(AUTORCV0 | AUTORCV1 | BEACON));

	RpChangeFilter();

	if (RpCb.pib.phyAntennaDiversityRxEna == RP_TRUE)
	{
		RpSetCcaDurationVal(RP_FALSE);
	}

	/* Start of antenna selection assistance process */
	RpAntSelAssist_StartProc();

	RpInverseTxAnt(RP_TRUE);

	// clear autorcvTx->Rx, clear autorcvRx->Rx, clear autoack reply, clear slotted, clear intrapan enable
	if (options & RP_RX_AUTO_RCV)
	{
		RpCb.status |= RP_PHY_STAT_TRX_TO_RX_AUTO;
	}

	RpCb.reg.bbTxRxMode1 |= CCASEL;	// RSSI
	RpRegWrite(BBTXRXMODE1, (uint8_t)(RpCb.reg.bbTxRxMode1));
	RpAddressFilterSetting((uint8_t)(RpCb.pib.macAddressFilter1Ena | RpCb.pib.macAddressFilter2Ena));
	if (((RpCb.pib.macAddressFilter1Ena == RP_TRUE) || (RpCb.pib.macAddressFilter2Ena == RP_TRUE))
			&& (RpCb.rx.softwareAdf == RP_FALSE))
	{
		if (options & RP_RX_SLOTTED)
		{
			RpCb.status |= RP_PHY_STAT_SLOTTED;
			RpCb.reg.bbTxRxMode0 |= BEACON;
			RpRegWrite(BBTXRXMODE0, (uint8_t)(RpCb.reg.bbTxRxMode0));
		}
	}

	RpSetSfdDetectionExtendWrite(RpCb.status);

	if (options & RP_RX_TIME)
	{
		// Timer Compare Reset
		futureTime = RpGetTime() - 1;
		RpRegBlockWrite(BBTCOMP0REG0, (uint8_t RP_FAR *)&futureTime, sizeof(uint32_t));
		RpReadIrq();

		// modify receive time for RF IC warmup
		time -= RP_PHY_RX_WARM_UP_TIME; // warmup time
		time &= RP_TIME_MASK;
		// RX with timer trigger
		RpCb.status |= RP_PHY_STAT_WAIT_TMRTRG;
		// set TC0 for RX
		RpRegBlockWrite(BBTCOMP0REG0, (uint8_t RP_FAR *)&time, sizeof(uint32_t));
		RpCb.reg.bbIntEn[0] |= TIM0INTEN;

		/* Disable interrupt */
		#if defined(__RX) || defined(__CCRL__) || defined(__arm)
		RP_PHY_ALL_DI(bkupPsw);
		#else
		RP_PHY_ALL_DI();
		#endif

		RpRegWrite(BBINTEN0,	(uint8_t)(RpCb.reg.bbIntEn[0]));

		if (RpChkRxTriggerTimer(time) == RP_TRUE)
		{
			/* Enable interrupt */
			#if defined(__RX) || defined(__CCRL__) || defined(__arm)
			RP_PHY_ALL_EI(bkupPsw);
			#else
			RP_PHY_ALL_EI();
			#endif

			return( RP_INVALID_SET_TIME );
		}
	}
	else
	{
		RpAvailableRcvRamEnable();

		/* Disable interrupt */
		#if defined(__RX) || defined(__CCRL__) || defined(__arm)
		RP_PHY_ALL_DI(bkupPsw);
		#else
		RP_PHY_ALL_DI();
		#endif

		RpRxOnStart();	// RX trigger

		if (options & RP_RX_TMOUT)
		{
			// Timer Compare Reset
			futureTime = RpGetTime() - 1;
			RpRegBlockWrite(BBTCOMP1REG0, (uint8_t RP_FAR *)&futureTime, sizeof(uint32_t));
			RpReadIrq();

			// RX with timeout
			// set TC1
			RpCb.status |= RP_PHY_STAT_RX_TMOUT;
			RpCb.rx.timeout = time;
			RpRegBlockWrite(BBTCOMP1REG0, (uint8_t RP_FAR *)&time, sizeof(uint32_t));
			RpCb.reg.bbIntEn[0] |= TIM1INTEN;
			RpRegWrite(BBINTEN0,	(uint8_t)(RpCb.reg.bbIntEn[0]));
			if (RpChkRxTriggerTimer(time) == RP_TRUE)
			{
				/* Enable interrupt */
				#if defined(__RX) || defined(__CCRL__) || defined(__arm)
				RP_PHY_ALL_EI(bkupPsw);
				#else
				RP_PHY_ALL_EI();
				#endif

				return( RP_INVALID_SET_TIME );
			}
		}
	}

	/* Enable interrupt */
	#if defined(__RX) || defined(__CCRL__) || defined(__arm)
	RP_PHY_ALL_EI(bkupPsw);
	#else
	RP_PHY_ALL_EI();
	#endif

	return (RP_SUCCESS);
}

/******************************************************************************
Function Name:       RpChkRxTriggerTimer
Parameters:          time:RX_ON start or end time
Return value:        none
Description:         Check Rx Trigger Timer
******************************************************************************/
static uint8_t RpChkRxTriggerTimer( uint32_t time )
{
	uint8_t rtn = RP_FALSE;
	uint32_t nowTime, difTime;

	nowTime = RpGetTime();
	difTime = (time - nowTime) & RP_TIME_MASK;
	if ((difTime > RP_TIME_LIMIT) || (difTime == 0))
	{
		if (RpCheckRfIRQ() == RP_FALSE)
		{
			RpSetStateRxOnToTrxOff();

			rtn = RP_TRUE;
		}
	}

	return (rtn);
}

/******************************************************************************
Function Name:       RpRxOnStart
Parameters:          none
Return value:        none;
Description:         start RX
******************************************************************************/
void
RpRxOnStart(void)
{
	RpRegWrite(BBTXRXCON, RCVTRG);	//receive trigger enable
}

/******************************************************************************
Function Name:       RpSetStateRxOnToTrxOff
Parameters:          none
Return value:        none
Description:         set to RX_OFF(RX_ON -> TRX_OFF)
 *		this function can be called in only RX_ON state
******************************************************************************/
void
RpSetStateRxOnToTrxOff(void)
{
	uint16_t status;

	status = RpCb.status;

	RpRxOnStop();							// with rfstop_bbtxrxrst = 1;
	RpClrAutoRxFunc();						// clear auto function

	RpCb.reg.bbIntEn[0] &= (uint8_t)(~(TIM0INTEN | TIM1INTEN | TIM2INTEN | TRNFININTEN | TRN0INTEN | TRN1INTEN | CCAINTEN));
	RpRegWrite(BBINTEN0,	(uint8_t)(RpCb.reg.bbIntEn[0]));

	/* Stop Antenna selection assistance */
	RpAntSelAssist_StopProc( status );

	RpCb.reg.bbTimeCon &= ~COMP0TRG;
	RpRegWrite(BBTIMECON, (uint8_t)(RpCb.reg.bbTimeCon));
	RpRelRxBuf(RpCb.rx.pTopRxBuf);

	RpCb.rx.waitRelRxBufWhenAutoRx = RP_FALSE;
	RpCb.rx.onReplyingAck = RP_FALSE;
	RpCb.tx.onCsmaCa = RP_FALSE;
}

/******************************************************************************
Function Name:       RpRxOnStop
Parameters:          rfstop:true means set to rfstop_bbtxrxrst = 1;
Return value:        none;
Description:         pause RX
******************************************************************************/
void
RpRxOnStop(void)
{
	RpRegWrite(BBTXRXRST, RFSTOP);		// RF Stop
	RpCb.status = RP_PHY_STAT_TRX_OFF;
}

/******************************************************************************
Function Name:       RpClrAutoRxFunc
Parameters:          none
Return value:        none
Description:         clear auto Rx function of H/W
******************************************************************************/
static void RpClrAutoRxFunc( void )
{
	RpCb.reg.bbTxRxMode0 &= (uint8_t)(~(AUTORCV0 | AUTORCV1 | BEACON));
	// clear autorcvTx->Rx, clear autorcvRx->Rx, clear autoack reply, clear slotted, clear intrapan enable
	RpRegWrite(BBTXRXMODE0, (uint8_t)(RpCb.reg.bbTxRxMode0));
	RpReadIrq();
}

/******************************************************************************
Function Name:       RpReadIrq
Parameters:          none
Return value:        irq read
Description:         Read IRQs
******************************************************************************/
uint32_t
RpReadIrq(void)
{
	uint32_t	ireq = RP_BB_NO_IREQ;

	RpRegBlockRead(BBINTREQ0, (uint8_t *)&ireq, 3);
#ifdef RP_LOG_IRQ
	RpLogRam[RpLogRamCnt] = ireq;
	if (RpCb.rx.onReplyingAck == RP_TRUE)
	{
		RpLogRam[RpLogRamCnt] |= 0x80000000;
	}

	if (RpCb.tx.onCsmaCa == RP_TRUE)
	{
		RpLogRam[RpLogRamCnt] |= 0x40000000;
	}

	if ((ireq & (RP_BBRCVFIN_IREQ | RP_BBTRNFIN_IREQ)) == (RP_BBRCVFIN_IREQ | RP_BBTRNFIN_IREQ))
	{
		RpRfStat.txRamUnderrun++;
	}
	RpLogRamCnt++;

	if ( RP_LOG_NUMBER <= RpLogRamCnt )
	{
		RpLogRamCnt = 0;
	}
#endif

	return (ireq);
}

/******************************************************************************
Function Name:       RpAvailableRcvOnCsmaca
Parameters:          none
Return value:        none
Description:         Rx on csmaca of H/W
******************************************************************************/
static int16_t RpAvailableRcvOnCsmaca( void )
{
	int16_t rtn = RP_SUCCESS;

	RpCb.rx.pTopRxBuf  = RpGetRxBuf();
	if (RpCb.rx.pTopRxBuf == RP_NULL)
	{
		/* Callback function execution Log */
		RpLog_Event( RP_LOG_CB, RP_LOG_CB_WARNING_NOTGETBUF );
		/* Callback function execution */
		INDIRECT_WarningIndCallback( RP_WARN_NOT_GET_RXBUF );

		RpRfStat.failToGetRxBuf++;
		rtn = RP_TRX_OFF;
	}
	else
	{
		RpCb.rx.cnt = RP_PHY_RX_STAT_INIT;

		RpAddressFilterSetting((uint8_t)(RpCb.pib.macAddressFilter1Ena | RpCb.pib.macAddressFilter2Ena));
		RpCb.reg.bbTxRxMode1 |= CCASEL; // RSSI
		RpRegWrite(BBTXRXMODE1, (uint8_t)(RpCb.reg.bbTxRxMode1));
		RpAvailableRcvRamEnable();
	}

	return rtn;
}

/******************************************************************************
Function Name:       RpAddressFilterSetting
Parameters:          addressFilterMode
Return value:        none
Description:         Rx on with addressfilter
******************************************************************************/
void
RpAddressFilterSetting(uint8_t addressFilterMode)
{
	if ((addressFilterMode == RP_TRUE) && (RpCb.rx.softwareAdf == RP_FALSE))
	{
		RpCb.reg.bbIntEn[2] &= ~FLINTEN;
		RpCb.reg.bbIntEn[1] |= ADRSINTEN;
	}
	else
	{
		RpCb.reg.bbIntEn[1] &= ~ADRSINTEN;
		RpCb.reg.bbIntEn[2] |= FLINTEN;
	}

	if (RpCb.rx.softwareAdf == RP_TRUE)
	{
		RpCb.reg.bbIntEn[2] |= RCVCUNTINTEN;
	}
	else
	{
		RpCb.reg.bbIntEn[2] &= ~RCVCUNTINTEN;
	}

	RpRegBlockWrite(BBINTEN1,	(uint8_t RP_FAR *)(&RpCb.reg.bbIntEn[1]), 2);
}

/******************************************************************************
Function Name:       RpInitRfic
Parameters:          none
Return value:        none
Description:         RF initialize.
******************************************************************************/
static void RpInitRfic( void )
{
	RpWakeupSequence();
	RpSetRegBeforeIdle();		// Modem Set
	RpInitRfOnly();				// MAC Set
}

/******************************************************************************
Function Name:      RpWrEvaReg1
Parameters:          write data
Return value:        none
Description:         write evaregs.
******************************************************************************/
static void RpWrEvaReg1( uint16_t aData )
{
	aData &= (~0x8000);	// bit15 = 0 write for RFINI00 access
	RpRegBlockWrite(RFINI00, (uint8_t RP_FAR *)&aData, sizeof(uint16_t));
}

/******************************************************************************
Function Name:       RpWrEvaReg2
Parameters:          write data
Return value:        none
Description:         write evaregs.
******************************************************************************/
void
RpWrEvaReg2(uint16_t aData)
{
	aData &= (~0x8000);	// bit15 = 0 write for RFINI10 access
	RpRegBlockWrite(RFINI10, (uint8_t RP_FAR *)&aData, sizeof(uint16_t));
}

/******************************************************************************
Function Name:       RpRdEvaReg2
Parameters:          write data
Return value:        none
Description:         read evaregs.
******************************************************************************/
uint8_t
RpRdEvaReg2(uint16_t aData)
{
	aData |= (0x8000);// bit15 = 1 read for RFINI10 access
	RpRegBlockWrite(RFINI10, (uint8_t RP_FAR *)&aData, sizeof(uint16_t));

	return (RpRegRead(RFINI12));
}

/******************************************************************************
Function Name:       RpSetChannelVal
Parameters:          channel number
Return value:        set result
Description:         set freq channel.
******************************************************************************/
uint8_t RpSetChannelVal( uint8_t channel )
{
	uint8_t freqBandId = RpCb.pib.phyFreqBandId;
	uint8_t rtn = 0xff;

	if (freqBandId == RP_PHY_FREQ_BAND_863MHz)
	{
		rtn = RpSetChannelCommonVal(channel, freqBandId, (const uint32_t *)RpOpeModeIndex863MHz);
	}
	else if (freqBandId == RP_PHY_FREQ_BAND_896MHz)
	{
		rtn = RpSetChannelCommonVal(channel, freqBandId, (const uint32_t *)RpOpeModeIndex896MHz);
	}
	else if (freqBandId == RP_PHY_FREQ_BAND_901MHz)
	{
		rtn = RpSetChannelCommonVal(channel, freqBandId, (const uint32_t *)RpOpeModeIndex901MHz);
	}
	else if (freqBandId == RP_PHY_FREQ_BAND_915MHz)
	{
		rtn = RpSetChannelCommonVal(channel, freqBandId, (const uint32_t *)RpOpeModeIndex915MHz);
	}
	else if (freqBandId == RP_PHY_FREQ_BAND_917MHz)
	{
		rtn = RpSetChannelCommonVal(channel, freqBandId, (const uint32_t *)RpOpeModeIndex917MHz);
	}
	else if (freqBandId == RP_PHY_FREQ_BAND_920MHz)
	{
		rtn = RpSetChannelCommonVal(channel, freqBandId, (const uint32_t *)RpOpeModeIndex920MHz);
	}
	else if (freqBandId == RP_PHY_FREQ_BAND_920MHz_Others)
	{
		rtn = RpSetChannelCommonVal(channel, freqBandId, (const uint32_t *)RpOpeModeIndex920MHzOthers);
	}
	else if (freqBandId == RP_PHY_FREQ_BAND_870MHz)
	{
		rtn = RpSetChannelCommonVal(channel, freqBandId, (const uint32_t *)RpOpeModeIndex870MHz);
	}
	else if (freqBandId == RP_PHY_FREQ_BAND_902MHz)
	{
		rtn = RpSetChannelCommonVal(channel, freqBandId, (const uint32_t *)RpOpeModeIndex902MHz);
	}
	else if (freqBandId == RP_PHY_FREQ_BAND_921MHz)
	{
		rtn = RpSetChannelCommonVal(channel, freqBandId, (const uint32_t *)RpOpeModeIndex921MHz);
	}

	return (rtn);
}

/******************************************************************************
Function Name:       RpSetChannelCommonVal
Parameters:          channel number
                     freqBandId
Return value:        set result
Description:         set freq 920 MHz channel.
******************************************************************************/
static uint8_t RpSetChannelCommonVal( uint8_t channel, uint8_t freqBandId, const uint32_t *pTblPtr )
{
	uint8_t opeMode = RpCb.pib.phyFskOpeMode;
	uint32_t	frequency;
	uint8_t	rtn;
#ifdef R_FREQUENCY_OFFSET_ENABLED
	int32_t phyFrequencyOffset = RpCb.pib.phyFrequencyOffset;
#endif // #ifdef R_FREQUENCY_OFFSET_ENABLED

	frequency =  *(pTblPtr + (RP_TABLE_ITEM * opeMode) + RP_FSTART);
	frequency += (channel * (*(pTblPtr + (RP_TABLE_ITEM * opeMode) + RP_FSPAN)));
	if (frequency > (*(pTblPtr + (RP_TABLE_ITEM * opeMode) + RP_FEND)))
	{
		frequency = *(pTblPtr + (RP_TABLE_ITEM * opeMode) + RP_FEND);
		rtn = (uint8_t) * (pTblPtr + (RP_TABLE_ITEM * opeMode) + RP_ENDINDEX);
	}
	else
	{
		rtn = channel;
	}

#ifdef R_FREQUENCY_OFFSET_ENABLED
	frequency = ((uint32_t)( (int32_t)frequency + phyFrequencyOffset )) & 0x3FFFFFFF;
#endif // #ifdef R_FREQUENCY_OFFSET_ENABLED

	RpCb.pib.phyFrequency = frequency;
	RpRegBlockWrite(BBFREQ, (uint8_t RP_FAR *)&frequency, sizeof(uint32_t));
	RpSetFreqAddReg(frequency, freqBandId);

	if ((RpCb.pib.phyRegulatoryMode == RP_RMODE_ENABLE_ARIB) && (RpCb.pib.phyFreqBandId == RP_PHY_FREQ_BAND_920MHz))
	{
		RpSetCcaDurationTimeUpdate(rtn);
	}

	return (rtn);
}

/******************************************************************************
Function Name:       RpSetCcaDurationTimeUpdate
Parameters:          channel number
Return value:        none
Description:         set cca duration time update.
******************************************************************************/
static void RpSetCcaDurationTimeUpdate( uint8_t channel )
{
	uint8_t opeMode = RpCb.pib.phyFskOpeMode;
	uint8_t freqBandId = RpCb.pib.phyFreqBandId;

	if (freqBandId == RP_PHY_FREQ_BAND_920MHz)
	{
		if (channel < RP_PHY_MIN_CHANNEL_VALID_ARIB_MODE)
		{
			// 5mS
			switch (opeMode)
			{
				case RP_PHY_FSK_OPEMODE_1:
					RpCb.pib.phyCcaDuration = 272;	// 50kbps
					break;
				case RP_PHY_FSK_OPEMODE_2:
					RpCb.pib.phyCcaDuration = 521;	// 100kbps
					break;
				case RP_PHY_FSK_OPEMODE_3:
					RpCb.pib.phyCcaDuration = 1031;	// 200kbps
					break;
				case RP_PHY_FSK_OPEMODE_4:
					RpCb.pib.phyCcaDuration = 2061;	// 400kbps
					break;
				case RP_PHY_FSK_OPEMODE_5:
					RpCb.pib.phyCcaDuration = 777;	// 150kbps
					break;
				case RP_PHY_FSK_OPEMODE_6:
					RpCb.pib.phyCcaDuration = 272;	// 50kbps
					break;
			}
		}
		else
		{
			// 128uS
			switch (opeMode)
			{
				case RP_PHY_FSK_OPEMODE_1:
					RpCb.pib.phyCcaDuration = 28;	// 50kbps
					break;
				case RP_PHY_FSK_OPEMODE_2:
					RpCb.pib.phyCcaDuration = 36;	// 100kbps
					break;
				case RP_PHY_FSK_OPEMODE_3:
					RpCb.pib.phyCcaDuration = 55;	// 200kbps
					break;
				case RP_PHY_FSK_OPEMODE_4:
					RpCb.pib.phyCcaDuration = 110;	// 400kbps
					break;
				case RP_PHY_FSK_OPEMODE_5:
					RpCb.pib.phyCcaDuration = 46;	// 150kbps
					break;
				case RP_PHY_FSK_OPEMODE_6:
					RpCb.pib.phyCcaDuration = 28;	// 50kbps
					break;
			}
		}
	}
}

/******************************************************************************
Function Name:       RpSetFskOpeModeVal
Parameters:          afterReset
Return value:        none
Description:         set opeMode.
******************************************************************************/
void RpSetFskOpeModeVal( uint8_t afterReset )
{
	uint8_t freqBandId = RpCb.pib.phyFreqBandId;
	uint8_t opeMode = RpCb.pib.phyFskOpeMode;
	uint8_t idIndex, tmpChannel;
	uint8_t	tmpTxPower;

	if (((freqBandId >= RP_PHY_FREQ_BAND_863MHz) && (freqBandId <= RP_PHY_FREQ_BAND_920MHz)) ||
		((freqBandId >= RP_PHY_FREQ_BAND_920MHz_Others) && (freqBandId <= RP_PHY_FREQ_BAND_921MHz)))
	{
		if ((opeMode >= RP_PHY_FSK_OPEMODE_1) && (opeMode <= RpBandModeOffset[freqBandId][RP_OPEMODE_LIMIT]))
		{
			idIndex = (uint8_t)RpBandModeOffset[freqBandId][(opeMode-1)];
		}
		else
		{
			opeMode = RpBandModeOffset[freqBandId][RP_OPEMODE_LIMIT];
			idIndex = (uint8_t)RpBandModeOffset[freqBandId][(opeMode-1)];
		}

		RpCb.pib.phyFskOpeMode = opeMode;
		RpCb.freqIdIndex = (uint8_t)idIndex;
		RpCb.pib.phyDataRate = RpFreqBandIdToDataRate[RpCb.freqIdIndex];
		RpSetChannelsSupportedPageAndVal();
		RpSetFreqBandVal(afterReset);

		if (afterReset == RP_FALSE)
		{
			tmpChannel = RpSetChannelVal(RpCb.pib.phyCurrentChannel);
			if (tmpChannel != RP_ERROR)
			{
				RpCb.pib.phyCurrentChannel = tmpChannel;
			}
		}
		else
		{
			if ((RpCb.pib.phyCurrentChannel != RP_RF_PIB_DFLT_CURRENT_CHANNEL) || 
			   (RpCb.pib.phyFreqBandId != RP_RF_HW_DFLT_FREQ_BAND_ID) ||
			   (RpCb.pib.phyFskOpeMode != RP_RF_HW_DFLT_FSK_OPE_MODE))
			{
				tmpChannel = RpSetChannelVal(RpCb.pib.phyCurrentChannel);
				if (tmpChannel != RP_ERROR)
				{
					RpCb.pib.phyCurrentChannel = tmpChannel;
				}
			}
		}

		tmpTxPower = RpSetTxPowerVal(RpCb.pib.phyTransmitPower);
		if (tmpTxPower != RP_ERROR)
		{
			RpCb.pib.phyTransmitPower = tmpTxPower;
		}

		if ((afterReset == RP_FALSE)
			|| (RpCb.pib.phyMrFskSfd != RP_RF_HW_DFLT_MRFSK_SFD)
			|| (RpCb.pib.phyFskOpeMode != RP_RF_HW_DFLT_FSK_OPE_MODE)
			|| (RpCb.pib.phyFreqBandId != RP_RF_HW_DFLT_FREQ_BAND_ID)
			|| (RpCb.pib.phyFskFecTxEna != RP_RF_HW_DFLT_FSK_FEC_TX_ENA)
			|| (RpCb.pib.phyFskFecRxEna != RP_RF_HW_DFLT_FSK_FEC_RX_ENA))
		{
			RpSetMrFskSfdVal();
		}
	}
}

/******************************************************************************
Function Name:       RpSetChannelsSupportedPageAndVal
Parameters:          none
Return value:        none
Description:         set current page and channels supported.
******************************************************************************/
void RpSetChannelsSupportedPageAndVal( void )
{
	uint8_t freqBandId = RpCb.pib.phyFreqBandId;
	uint8_t opeMode = RpCb.pib.phyFskOpeMode;
	uint32_t channelsSupported[2];
	uint8_t modeTblIndex;
	uint8_t channelsSupportedPage = RpCb.pib.phyChannelsSupportedPage;

	channelsSupported[0] = 0;
	channelsSupported[1] = 0;

	if (freqBandId == RP_PHY_FREQ_BAND_863MHz)
	{
		if ((opeMode == RP_PHY_FSK_OPEMODE_1) || (opeMode == RP_PHY_FSK_OPEMODE_2) || (opeMode == RP_PHY_FSK_OPEMODE_3))
		{
			channelsSupportedPage = RP_PHY_CHANNELS_SUPPORTED_INDEX_0;
			modeTblIndex = (opeMode - 1);
		}
		else if (opeMode == RP_PHY_FSK_OPEMODE_4)
		{
			if (channelsSupportedPage > RP_PHY_CHANNELS_SUPPORTED_INDEX_1)
			{
				channelsSupportedPage = RP_PHY_CHANNELS_SUPPORTED_INDEX_1;
			}
			modeTblIndex = RP_PHY_TABLE_INDEX_CHANNELS_SUPPORTED_3 + channelsSupportedPage;
		}
		else if (opeMode == RP_PHY_FSK_OPEMODE_5)
		{
			if (channelsSupportedPage > RP_PHY_CHANNELS_SUPPORTED_INDEX_1)
			{
				channelsSupportedPage = RP_PHY_CHANNELS_SUPPORTED_INDEX_1;
			}
			modeTblIndex = RP_PHY_TABLE_INDEX_CHANNELS_SUPPORTED_5 + channelsSupportedPage;
		}
		else if (opeMode == RP_PHY_FSK_OPEMODE_6)
		{
			if (channelsSupportedPage > RP_PHY_CHANNELS_SUPPORTED_INDEX_1)
			{
				channelsSupportedPage = RP_PHY_CHANNELS_SUPPORTED_INDEX_1;
			}
			modeTblIndex = RP_PHY_TABLE_INDEX_CHANNELS_SUPPORTED_7 + channelsSupportedPage;
		}
		else // if (opeMode >= RP_PHY_FSK_OPEMODE_7)
		{
			if (channelsSupportedPage > RP_PHY_CHANNELS_SUPPORTED_INDEX_1)
			{
				channelsSupportedPage = RP_PHY_CHANNELS_SUPPORTED_INDEX_1;
			}
			modeTblIndex = RP_PHY_TABLE_INDEX_CHANNELS_SUPPORTED_9 + channelsSupportedPage;
		}
		channelsSupported[0] = RpCurrentPageToChannelsSupprotedAtBandId4[modeTblIndex][0];
		channelsSupported[1] = RpCurrentPageToChannelsSupprotedAtBandId4[modeTblIndex][1];
	}
	else if (freqBandId == RP_PHY_FREQ_BAND_896MHz)
	{
		if (opeMode == RP_PHY_FSK_OPEMODE_1)
		{
			modeTblIndex = RP_PHY_TABLE_INDEX_CHANNELS_SUPPORTED_0 + channelsSupportedPage;
		}
		else if (opeMode == RP_PHY_FSK_OPEMODE_2)
		{
			if (channelsSupportedPage > RP_PHY_CHANNELS_SUPPORTED_INDEX_1)
			{
				channelsSupportedPage = RP_PHY_CHANNELS_SUPPORTED_INDEX_1;
			}
			modeTblIndex = RP_PHY_TABLE_INDEX_CHANNELS_SUPPORTED_4 + channelsSupportedPage;
		}
		else // if (opeMode == RP_PHY_FSK_OPEMODE_3)
		{
			channelsSupportedPage = RP_PHY_CHANNELS_SUPPORTED_INDEX_0;
			modeTblIndex = RP_PHY_TABLE_INDEX_CHANNELS_SUPPORTED_6;
		}
		channelsSupported[0] = RpCurrentPageToChannelsSupprotedAtBandId5[modeTblIndex][0];
		channelsSupported[1] = RpCurrentPageToChannelsSupprotedAtBandId5[modeTblIndex][1];
	}
	else if (freqBandId == RP_PHY_FREQ_BAND_901MHz)
	{
		channelsSupportedPage = RP_PHY_CHANNELS_SUPPORTED_INDEX_0;
		modeTblIndex = (opeMode - 1);
		channelsSupported[0] = RpCurrentPageToChannelsSupprotedAtBandId6[modeTblIndex][0];
		channelsSupported[1] = RpCurrentPageToChannelsSupprotedAtBandId6[modeTblIndex][1];
	}
	else if (freqBandId == RP_PHY_FREQ_BAND_915MHz)
	{
		if (opeMode == RP_PHY_FSK_OPEMODE_1)
		{
			if (channelsSupportedPage > RP_PHY_CHANNELS_SUPPORTED_INDEX_2)
			{
				channelsSupportedPage = RP_PHY_CHANNELS_SUPPORTED_INDEX_2;
			}
			modeTblIndex = RP_PHY_TABLE_INDEX_CHANNELS_SUPPORTED_0 + channelsSupportedPage;
		}
		else if ((opeMode == RP_PHY_FSK_OPEMODE_2) || (opeMode == RP_PHY_FSK_OPEMODE_3))
		{
			channelsSupportedPage = RP_PHY_CHANNELS_SUPPORTED_INDEX_0;
			modeTblIndex = (opeMode + 1);
		}
		else // if (opeMode == RP_PHY_FSK_OPEMODE_4)
		{
			if (channelsSupportedPage > RP_PHY_CHANNELS_SUPPORTED_INDEX_2)
			{
				channelsSupportedPage = RP_PHY_CHANNELS_SUPPORTED_INDEX_2;
			}
			modeTblIndex = (opeMode + 1) + channelsSupportedPage;
		}
		channelsSupported[0] = RpCurrentPageToChannelsSupprotedAtBandId7[modeTblIndex][0];
		channelsSupported[1] = RpCurrentPageToChannelsSupprotedAtBandId7[modeTblIndex][1];
	}
	else if (freqBandId == RP_PHY_FREQ_BAND_917MHz)
	{
		channelsSupportedPage = RP_PHY_CHANNELS_SUPPORTED_INDEX_0;
		modeTblIndex = (opeMode - 1);
		channelsSupported[0] = RpCurrentPageToChannelsSupprotedAtBandId8[modeTblIndex][0];
		channelsSupported[1] = RpCurrentPageToChannelsSupprotedAtBandId8[modeTblIndex][1];
	}
	else if (freqBandId == RP_PHY_FREQ_BAND_920MHz)
	{
		channelsSupportedPage = RP_PHY_CHANNELS_SUPPORTED_INDEX_0;
		modeTblIndex = (opeMode - 1);
		channelsSupported[0] = RpCurrentPageToChannelsSupprotedAtBandId9[modeTblIndex][0];
		channelsSupported[1] = RpCurrentPageToChannelsSupprotedAtBandId9[modeTblIndex][1];
	}
	else if (freqBandId == RP_PHY_FREQ_BAND_920MHz_Others)
	{
		if (opeMode == RP_PHY_FSK_OPEMODE_1)
		{
			channelsSupportedPage = RP_PHY_CHANNELS_SUPPORTED_INDEX_0;
			modeTblIndex = RP_PHY_TABLE_INDEX_CHANNELS_SUPPORTED_0;
		}
		else if (opeMode == RP_PHY_FSK_OPEMODE_2)
		{
			if (channelsSupportedPage > RP_PHY_CHANNELS_SUPPORTED_INDEX_2)
			{
				channelsSupportedPage = RP_PHY_CHANNELS_SUPPORTED_INDEX_2;
			}
			modeTblIndex = RP_PHY_TABLE_INDEX_CHANNELS_SUPPORTED_1 + channelsSupportedPage;
		}
		else //if (opeMode == RP_PHY_FSK_OPEMODE_3)
		{
			channelsSupportedPage = RP_PHY_CHANNELS_SUPPORTED_INDEX_0;
			modeTblIndex = RP_PHY_TABLE_INDEX_CHANNELS_SUPPORTED_4;
		}
		channelsSupported[0] = RpCurrentPageToChannelsSupprotedAtBandId14[modeTblIndex][0];
		channelsSupported[1] = RpCurrentPageToChannelsSupprotedAtBandId14[modeTblIndex][1];
	}
	else if (freqBandId == RP_PHY_FREQ_BAND_870MHz)
	{
		channelsSupportedPage = RP_PHY_CHANNELS_SUPPORTED_INDEX_0;
		modeTblIndex = (opeMode - 1);
		channelsSupported[0] = RpCurrentPageToChannelsSupprotedAtBandId15[modeTblIndex][0];
		channelsSupported[1] = RpCurrentPageToChannelsSupprotedAtBandId15[modeTblIndex][1];
	}
	else if (freqBandId == RP_PHY_FREQ_BAND_902MHz)
	{
		if (channelsSupportedPage > RP_PHY_CHANNELS_SUPPORTED_INDEX_2)
		{
			channelsSupportedPage = RP_PHY_CHANNELS_SUPPORTED_INDEX_2;
		}
		modeTblIndex = RP_PHY_TABLE_INDEX_CHANNELS_SUPPORTED_0 + channelsSupportedPage;

		channelsSupported[0] = RpCurrentPageToChannelsSupprotedAtBandId16[modeTblIndex][0];
		channelsSupported[1] = RpCurrentPageToChannelsSupprotedAtBandId16[modeTblIndex][1];
	}
	else if (freqBandId == RP_PHY_FREQ_BAND_921MHz)
	{
		channelsSupportedPage = RP_PHY_CHANNELS_SUPPORTED_INDEX_0;
		modeTblIndex = (opeMode - 1);
		channelsSupported[0] = RpCurrentPageToChannelsSupprotedAtBandId17[modeTblIndex][0];
		channelsSupported[1] = RpCurrentPageToChannelsSupprotedAtBandId17[modeTblIndex][1];
	}

	RpCb.pib.phyChannelsSupportedPage = channelsSupportedPage;
	RpCb.pib.phyChannelsSupported[0] = channelsSupported[0];
	RpCb.pib.phyChannelsSupported[1] = channelsSupported[1];
}

/******************************************************************************
Function Name:       RpSetFreqBandVal
Parameters:          afterReset
Return value:        none
Description:         set freq band.
******************************************************************************/
static void RpSetFreqBandVal( uint8_t afterReset )
{
	uint8_t index = RpCb.freqIdIndex;
	uint8_t antDivRxEna = RpCb.pib.phyAntennaDiversityRxEna;
	uint8_t freqBandId = RpCb.pib.phyFreqBandId;
	uint8_t fecRxEna = RpCb.pib.phyFskFecRxEna;
	const uint8_t *rssiLossPtr = (const uint8_t *)RpFreqBandTblRssiLossPtr[RpConfig.rssiLoss + 1];
	uint8_t arrayChar[5];

	if (index < (sizeof(RpFreqBandTbl) / sizeof(uint8_t) / RP_MAXNUM_OFFSET))
	{
		if (afterReset == RP_FALSE || ((RpCb.pib.phyFreqBandId != RP_RF_HW_DFLT_FREQ_BAND_ID) || (RpCb.pib.phyFskOpeMode != RP_RF_HW_DFLT_FSK_OPE_MODE)))
		{
			if ((RpCb.pib.phyRegulatoryMode == RP_RMODE_ENABLE) ||
				((RpCb.pib.phyRegulatoryMode == RP_RMODE_ENABLE_ARIB) && (RpCb.pib.phyFreqBandId == RP_PHY_FREQ_BAND_920MHz)))
			{
				RpPrevSentTimeReSetting();
			}
			RpCb.reg.bbTimeCon &= ~(TIMEEN);// Timer Count Disable, Comp0 transmit Disable
			RpRegWrite(BBTIMECON, (uint8_t)(RpCb.reg.bbTimeCon));

			RpRegWrite(BBSYMBLRATE, (uint8_t)RpFreqBandTbl[index][RP_SYMBOL_OFFSET_L]);
			RpRegWrite(BBSYMBLRATE_1, (uint8_t)RpFreqBandTbl[index][RP_SYMBOL_OFFSET_H]);
			RpRegWrite(BBMODSET, (uint8_t)RpFreqBandTbl[index][RP_MODESET_OFFSET]);

			RpCb.reg.bbTimeCon |= TIMEEN;	// Timer Count Start, Comp0 transmit Disable Stay
			RpRegWrite(BBTIMECON, (uint8_t)(RpCb.reg.bbTimeCon));

			RpRegWrite((0x00F1 << 3), (uint8_t)RpFreqBandTbl[index][RP_TXCPISL1_OFFSET]);
			RpRegWrite((0x00F2 << 3), (uint8_t)RpFreqBandTbl[index][RP_TXCPISL2_OFFSET]);
			RpRegWrite((0x00F3 << 3), (uint8_t)RpFreqBandTbl[index][RP_TXCR0_OFFSET]);
			RpRegWrite((0x00F4 << 3), (uint8_t)RpFreqBandTbl[index][RP_TXCR1_OFFSET]);
			RpRegWrite((0x0423 << 3), (uint8_t)RpFreqBandTbl[index][RP_0x0423_OFFSET]);
			RpRegWrite((0x0471 << 3), (uint8_t)RpFreqBandTbl[index][RP_0x0471_OFFSET]);
			RpRegWrite((0x047D << 3), (uint8_t)RpFreqBandTbl[index][RP_0x047D_OFFSET]);

			arrayChar[0] = (fecRxEna == RP_FEC_RX_MODE_ENABLE)? (uint8_t)RpFreqBandTbl[index][RP_0x0486_F_OFFSET]: (uint8_t)RpFreqBandTbl[index][RP_0x0486_OFFSET];
			arrayChar[1] = (fecRxEna == RP_FEC_RX_MODE_ENABLE)? (uint8_t)RpFreqBandTbl[index][RP_0x0487_F_OFFSET]: (uint8_t)RpFreqBandTbl[index][RP_0x0487_OFFSET];
			RpRegBlockWrite((0x0486 << 3), (uint8_t RP_FAR *)&(arrayChar[0]), 2);

			RpRegWrite((0x048D << 3), (uint8_t)RpFreqBandTbl[index][RP_0x048D_OFFSET]);
			RpRegWrite((0x048F << 3), (uint8_t)RpFreqBandTbl[index][RP_0x048F_OFFSET]);
			RpRegWrite((0x0405 << 3), (uint8_t)RpFreqBandTbl[index][RP_0x0405_OFFSET]);
			RpRegWrite((0x0505 << 3), (uint8_t)RpFreqBandTbl[index][RP_0x0505_OFFSET]);
			RpRegWrite((0x0403 << 3), (uint8_t)RpFreqBandTbl[index][RP_0x0403_OFFSET]);
			RpRegWrite((0x04D9 << 3), (uint8_t)RpFreqBandTbl[index][RP_0x04D9_OFFSET]);
			RpWrEvaReg2(RpFreqBandTblSerial[index][RP_SER0x23_OFFSET]);
			RpWrEvaReg2(RpFreqBandTblSerial[index][RP_SER0x0A_OFFSET]);
			RpRegWrite(BBPABL, (uint8_t)RpFreqBandTbl[index][RP_PABL_OFFSET]);
			RpRegWrite(BBSHRCON, (uint8_t)RpFreqBandTbl[index][RP_SHRCON_OFFSET]);

			RpRegWrite((0x00F5 << 3), (uint8_t)RpFreqBandTbl2[freqBandId][RP_0x00F5_OFFSET]);
			RpRegWrite((0x00FB << 3), (uint8_t)RpFreqBandTbl2[freqBandId][RP_0x00FB_OFFSET]);
			RpWrEvaReg2(RpFreqBandTblSerial2[freqBandId]);
		}

		RpRegWrite((0x00CC << 3), (uint8_t)RpFreqBandTbl[index][RP_0x00CC_OFFSET]);
		arrayChar[0] = (uint8_t)RpFreqBandTbl[index][RP_0x0430_OFFSET];	// 0x0430
		arrayChar[1] = 0x00;											// 0x0431 (h/W initial)
		arrayChar[2] = (uint8_t)RpFreqBandTbl[index][RP_0x0432_OFFSET];	// 0x0432
		RpRegBlockWrite((0x0430 << 3), (uint8_t RP_FAR *)&(arrayChar[0]), 3);
		RpRegWrite((0x0436 << 3), (uint8_t)RpFreqBandTbl[index][RP_0x0436_OFFSET]);
		RpRegWrite((0x043A << 3), (uint8_t)RpFreqBandTbl[index][RP_0x043A_OFFSET]);
		RpRegBlockWrite((0x0473 << 3), (uint8_t RP_FAR *)&(RpFreqBandTbl[index][RP_0x0473_OFFSET]), 2);
		RpRegWrite((0x047C << 3), (uint8_t)RpFreqBandTbl[index][RP_0x047C_OFFSET]);
		RpRegBlockWrite((0x047E << 3), (uint8_t RP_FAR *)&(RpFreqBandTbl[index][RP_0x047E_OFFSET]), 3);
		RpRegWrite((0x0488 << 3), (uint8_t)RpFreqBandTbl[index][RP_0x0488_OFFSET]);
		RpRegBlockWrite((0x0493 << 3), (uint8_t RP_FAR *)&(RpFreqBandTbl[index][RP_0x0493_OFFSET]), 2);
		RpRegWrite((0x04F4 << 3), (uint8_t)RpFreqBandTbl[index][RP_0x04F4_OFFSET]);

		RpRegBlockWrite((0x0581 << 3), (uint8_t RP_FAR *)&(RpFreqBandTbl[index][RP_0x0581_OFFSET]), 2);
		arrayChar[0] = (uint8_t)RpFreqBandTbl[index][RP_0x04F6_OFFSET];	// 0x04F6
		arrayChar[1] = 0x00;											// 0x04F7 (h/W initial)
		arrayChar[2] = (uint8_t)RpFreqBandTbl[index][RP_0x04F8_OFFSET];	// 0x04F8
		RpRegBlockWrite((0x04F6 << 3), (uint8_t RP_FAR *)&(arrayChar[0]), 3);
		RpRegWrite((0x0415 << 3), (uint8_t)RpFreqBandTbl[index][RP_0x0415_OFFSET]);
		RpWrEvaReg2(RpFreqBandTblSerial[index][RP_SER0x0E_OFFSET]);

		RpCb.pib.refVthVal = 0x0100 | (uint16_t)(*(rssiLossPtr + (((uint16_t)index * RP_MAXNUM_RSSI_LOSS) + RP_LVLVTH_OFFSET)));
		RpSetLvlVthVal();
		RpSetCcaVthVal();

		RpCb.pib.phyRssiLoss = (uint16_t) * (rssiLossPtr + (((uint16_t)index * RP_MAXNUM_RSSI_LOSS) + RP_0x0483_OFFSET));
		RpSetRssiOffsetVal();
		arrayChar[0] = (uint8_t)RpFreqBandTbl[index][RP_0x0454_OFFSET];// 0x0454
		arrayChar[1] = 0xA9;	// 0x0455 (h/W initial)
		arrayChar[2] = (uint8_t)RpFreqBandTbl[index][RP_0x0456_OFFSET];// 0x0456
		RpRegBlockWrite((0x0454 << 3), (uint8_t RP_FAR *)&(arrayChar[0]), 3);

		arrayChar[0] = (fecRxEna == RP_FEC_RX_MODE_ENABLE)? (uint8_t)RpFreqBandTbl[index][RP_0x050F_F_OFFSET]: (uint8_t)RpFreqBandTbl[index][RP_0x050F_OFFSET];
		RpRegBlockWrite((0x050F << 3), (uint8_t RP_FAR *)&(arrayChar[0]), 1);
		arrayChar[0] = (fecRxEna == RP_FEC_RX_MODE_ENABLE)? (uint8_t)RpFreqBandTbl[index][RP_0x058F_F_OFFSET]: (uint8_t)RpFreqBandTbl[index][RP_0x058F_OFFSET];
		RpRegBlockWrite((0x058F << 3), (uint8_t RP_FAR *)&(arrayChar[0]), 1);

		arrayChar[0] = (uint8_t)RpFreqBandTbl[index][RP_0x059F_OFFSET];
		arrayChar[1] = (fecRxEna == RP_FEC_RX_MODE_ENABLE)? (uint8_t)RpFreqBandTbl[index][RP_0x05A0_F_OFFSET]: (uint8_t)RpFreqBandTbl[index][RP_0x05A0_OFFSET];
		arrayChar[2] = (uint8_t)RpFreqBandTbl[index][RP_0x05A1_OFFSET];
		arrayChar[3] = (fecRxEna == RP_FEC_RX_MODE_ENABLE)? (uint8_t)RpFreqBandTbl[index][RP_0x05A2_F_OFFSET]: (uint8_t)RpFreqBandTbl[index][RP_0x05A2_OFFSET];
		RpRegBlockWrite((0x059F << 3), (uint8_t RP_FAR *)&(arrayChar[0]), 4);

		if (antDivRxEna == RP_TRUE)
		{
			RpRegWrite((0x042D << 3), (uint8_t)RpFreqBandTbl[index][RP_0x042D_ANDVON_OFFSET]);

			if ((RpCb.pib.phyRegulatoryMode == RP_RMODE_ENABLE_ARIB) && (RpCb.pib.phyFreqBandId == RP_PHY_FREQ_BAND_920MHz))
			{
				RpRegBlockWrite(BBANTTIMOUT, (uint8_t RP_FAR *)&(RpFreqBandTbl[index][RP_ANTDVT_TIMEOUT_ARIB]), 2);
				RpRegWrite((0x0472 << 3), 0x04);
			}
			else
			{
				RpRegBlockWrite(BBANTTIMOUT, (uint8_t RP_FAR *)&(RpFreqBandTbl[index][RP_ANTDVT_TIMEOUT]), 2);
				RpSetPreamble4ByteRxMode();
			}
		}
		else
		{
			RpRegWrite((0x042D << 3), (uint8_t)RpFreqBandTbl[index][RP_0x042D_OFFSET]);
			RpSetPreamble4ByteRxMode();
		}

		if (afterReset == RP_FALSE)
		{
			RpExecuteCalibration();
		}
	}
}

/******************************************************************************
Function Name:       RpSetTxPowerVal
Parameters:          set tx power
Return value:        set tx power
Description:         set transmit power.
******************************************************************************/
uint8_t RpSetTxPowerVal( uint8_t gainSet )
{
	uint16_t regSerVal;
	uint8_t freqBandId = RpCb.pib.phyFreqBandId;
	uint8_t regSetVal[4];
	uint8_t arrayChar[3];

	if (gainSet < RP_MAXNUM_TXPOWER)
	{
		if ((freqBandId == RP_PHY_FREQ_BAND_863MHz) || (freqBandId == RP_PHY_FREQ_BAND_870MHz))
		{
			if (gainSet >= RP_MAXNUM_TXPOWER_EU)
			{
				gainSet = RP_MAXNUM_TXPOWER_EU - 1;
			}
			regSetVal[0] = RpTxPowerModeSetEu[gainSet][0];
			regSetVal[1] = RpTxPowerModeSetEu[gainSet][1];
			regSetVal[2] = RpTxPowerModeSetEu[gainSet][2];
			regSetVal[3] = RpTxPowerModeSetEu[gainSet][3];
		}
		else if ((freqBandId == RP_PHY_FREQ_BAND_896MHz) || (freqBandId == RP_PHY_FREQ_BAND_901MHz) || (freqBandId == RP_PHY_FREQ_BAND_915MHz) || (freqBandId == RP_PHY_FREQ_BAND_902MHz))
		{
			if (gainSet >= RP_MAXNUM_TXPOWER_US)
			{
				gainSet = RP_MAXNUM_TXPOWER_US - 1;
			}
			regSetVal[0] = RpTxPowerModeSetUs[gainSet][0];
			regSetVal[1] = RpTxPowerModeSetUs[gainSet][1];
			regSetVal[2] = RpTxPowerModeSetUs[gainSet][2];
			regSetVal[3] = RpTxPowerModeSetUs[gainSet][3];
		}
		else if (freqBandId == RP_PHY_FREQ_BAND_917MHz)
		{
			if (gainSet >= RP_MAXNUM_TXPOWER_KOREA)
			{
				gainSet = RP_MAXNUM_TXPOWER_KOREA - 1;
			}
			regSetVal[0] = RpTxPowerModeSetKorea[gainSet][0];
			regSetVal[1] = RpTxPowerModeSetKorea[gainSet][1];
			regSetVal[2] = RpTxPowerModeSetKorea[gainSet][2];
			regSetVal[3] = RpTxPowerModeSetKorea[gainSet][3];
		}
		else if ((freqBandId == RP_PHY_FREQ_BAND_920MHz) || (freqBandId == RP_PHY_FREQ_BAND_920MHz_Others) || (freqBandId == RP_PHY_FREQ_BAND_921MHz))
		{
			if (gainSet >= RP_MAXNUM_TXPOWER_JPN)
			{
				gainSet = (RP_MAXNUM_TXPOWER_JPN - 1);
			}
			regSetVal[0] = RpTxPowerModeSetJpn[gainSet][0];
			regSetVal[1] = RpTxPowerModeSetJpn[gainSet][1];
			regSetVal[2] = RpTxPowerModeSetJpn[gainSet][2];
			regSetVal[3] = RpTxPowerModeSetJpn[gainSet][3];
		}
		// VREG
		RpCb.reg.bbTxCon0 &= (~TXDDCVAR);
		RpCb.reg.bbTxCon0 |= regSetVal[0];
		arrayChar[0] = RpCb.reg.bbTxCon0;
		arrayChar[1] = 0x00;// 0x0091 H/W Initial
		// VLDO CONT
		RpCb.reg.bbTxCon2 &= (~TXLDOTRXVAR);
		RpCb.reg.bbTxCon2 |= regSetVal[1];
		arrayChar[2] = RpCb.reg.bbTxCon2;
		RpRegBlockWrite(BBTXCON0, (uint8_t *)&(arrayChar[0]), 3);
		// TXGF
		RpCb.reg.bb19Er2 &= (~0x1F);
		RpCb.reg.bb19Er2 |= (regSetVal[2]);
		regSerVal = (uint16_t)(0x1900 | RpCb.reg.bb19Er2);
		RpWrEvaReg2(regSerVal);
		// CTN OUT
		RpCb.reg.bb25Er2 &= (~0x1F);
		RpCb.reg.bb25Er2 |= (regSetVal[3]);
		regSerVal = (uint16_t)(0x2500 | RpCb.reg.bb25Er2);
		RpWrEvaReg2(regSerVal);
		// CTN NOT
		RpCb.reg.bb1AEr2 &= (uint8_t)(~0xF0);
		if (gainSet)
		{
			if ((freqBandId == RP_PHY_FREQ_BAND_863MHz) || (freqBandId == RP_PHY_FREQ_BAND_870MHz))
			{
				RpCb.reg.bb1AEr2 |= 0x70;
			}
			else
			{
				RpCb.reg.bb1AEr2 |= 0x60;
			}
		}
		regSerVal = (uint16_t)(0x1A00 | RpCb.reg.bb1AEr2);
		RpWrEvaReg2(regSerVal);
	}
	else
	{
		gainSet = RP_ERROR;
	}

	return (gainSet);
}

/******************************************************************************
Function Name:       RpSetAntennaSwitchVal
Parameters:          none
Return value:        none
Description:         set antenna switch.
******************************************************************************/
void RpSetAntennaSwitchVal( void )
{
	// ANTSW
	RpRegWrite(BBANTSWCON, RpCb.pib.phyAntennaSwitchEna);
}

/******************************************************************************
Function Name:       RpSetAntennaDiversityVal
Parameters:          none
Return value:        none
Description:         set antenna diversity.
******************************************************************************/
void RpSetAntennaDiversityVal( void )
{
	uint8_t antDvrEn = RpCb.pib.phyAntennaDiversityRxEna;
	uint8_t antAckTxSel = RpCb.pib.phyAntennaSelectAckTx;
	uint8_t antAckRxSel = RpCb.pib.phyAntennaSelectAckRx;
	uint8_t antDvrCon, antDvrCon2;

	// Antenna Diversity
	antDvrCon = RpRegRead(BBANTDIV);
	antDvrCon &= (~(ANTDIVEN));
	antDvrCon &= (~(ANTACKRTNDIS | ANTACKRCVDIS));
	antDvrCon2 = RpRegRead(BBANTDIV2);
	antDvrCon2 &= (~(ANTACKRTNSET | ANTACKRCVSET | ACKRTNRVS));
	if (antDvrEn)
	{
		antDvrCon |= ANTDIVEN;
		// AckTx Antenna Select
		if (antAckTxSel == RP_ANT_SEL_ACK_TX_ANT0)
		{
			// non opreation
		}
		else if (antAckTxSel == RP_ANT_SEL_ACK_TX_ANT1)
		{
			antDvrCon2 |= ANTACKRTNSET;
		}
		else
		{
			antDvrCon |= ANTACKRTNDIS;
			if (antAckTxSel == RP_ANT_SEL_ACK_TX_RCVD_INV_ANT)
			{
				antDvrCon2 |= ACKRTNRVS;
			}
		}
		// AckRx Antenna Select
		if (antAckRxSel == RP_ANT_SEL_ACK_RX_ANT0)
		{
			// non opreation
		}
		else if (antAckRxSel == RP_ANT_SEL_ACK_RX_ANT1)
		{
			antDvrCon2 |= ANTACKRCVSET;
		}
		else
		{
			antDvrCon |= ANTACKRCVDIS;
		}

		RpRegWrite(BBANTDIVCON, 0x0D);
	}
	else
	{
		RpRegWrite(BBANTDIVCON, 0x05);

		/* Recovery Antenna select assist */
		RpAntSelAssist_RecoveryProc();
	}
	RpRegWrite(BBANTDIV, antDvrCon);
	RpRegWrite(BBANTDIV2, antDvrCon2);
}

/******************************************************************************
Function Name:       RpSetAntennaSelectTxVal
Parameters:          none
Return value:        none
Description:         set antenna select Tx.
******************************************************************************/
void RpSetAntennaSelectTxVal( void )
{
	uint8_t antDvrEn = RpCb.pib.phyAntennaDiversityRxEna;
	uint8_t antTxSel = RpCb.pib.phyAntennaSelectTx;
	uint8_t antDvrCon;

	if (antDvrEn)
	{
		if (antTxSel == RP_PHY_ANTENNA_UNUSE)
		{
			antTxSel = RP_PHY_ANTENNA_0;
			RpCb.pib.phyAntennaSelectTx = antTxSel;
		}
		
		// Antenna Diversity
		antDvrCon = RpRegRead(BBANTDIV);
		// Tx Antenna Select
		if (antTxSel)
		{
			antDvrCon |= ANTSWTRNSET;
		}
		else
		{
			antDvrCon &= ~ANTSWTRNSET;
		}
		RpRegWrite(BBANTDIV, antDvrCon);
	}
	else if (antTxSel != RP_PHY_ANTENNA_UNUSE)
	{
		// Port Setting(output)
		RpCb.reg.bbGpioDir |= (GPIO1DIR | GPIO2DIR);
		RpRegWrite(BBGPIODIR, RpCb.reg.bbGpioDir);

		// Tx Antenna Select
		if (antTxSel)
		{
			RpCb.reg.bbGpioData &= ~GPIO1DATA;
			RpCb.reg.bbGpioData |= GPIO2DATA;
		}
		else
		{
			RpCb.reg.bbGpioData &= ~GPIO2DATA;
			RpCb.reg.bbGpioData |= GPIO1DATA;
		}
		RpRegWrite(BBGPIODATA, RpCb.reg.bbGpioData);
	}
}

/******************************************************************************
Function Name:       RpSetCcaVthVal
Parameters:          none
Return value:        none
Description:         set cca vth.
******************************************************************************/
void
RpSetCcaVthVal(void)
{
	uint16_t ccaVth;
	uint16_t wk16;

	if (RpCmpRefVthVal(RpCb.pib.phyCcaVth) == RP_TRUE)
	{
		ccaVth = RpCb.pib.phyCcaVth;
	}
	else
	{
		ccaVth = RpCb.pib.refVthVal;
	}

	switch (RpCb.pib.phyCcaBandwidth)
	{
		case RP_PHY_CCA_BANDWIDTH_INDEX_225K:
			wk16 = RpCcaVthOffsetTblDefault[RpCb.freqIdIndex];
			break;
		case RP_PHY_CCA_BANDWIDTH_INDEX_200K:
		case RP_PHY_CCA_BANDWIDTH_INDEX_150K:
			wk16 = 0x0000;
			break;
		case RP_PHY_CCA_BANDWIDTH_INDEX_NARROW:
		default:
			wk16 = RpCcaVthOffsetTblNarrow[RpCb.freqIdIndex];
			break;
	}
	ccaVth += wk16;
	ccaVth += RpCb.pib.phyCcaVthOffset;
	ccaVth &= 0x01FF;

	// BBCCAVTH
	RpRegBlockWrite(BBCCAVTH, (uint8_t RP_FAR *)&ccaVth, sizeof(uint16_t));
}

/******************************************************************************
Function Name:       RpCmpRefVthVal
Parameters:          level filter vth value
Return value:        RP_TRUE:rewrite OK
                     RP_FALSE:rewrite NG
Description:         compare vth and refferance.
******************************************************************************/
static uint8_t RpCmpRefVthVal( uint16_t vthVal )
{
	uint8_t rtn = RP_FALSE;
	int16_t signedVal;
	int16_t signedValRef;

	signedVal = (int16_t)(vthVal);
	signedVal = (signedVal < 0x0100) ? signedVal : (signedVal | 0xfe00);
	signedValRef = (int16_t)(RpCb.pib.refVthVal);
	signedValRef = (signedValRef < 0x0100) ? signedValRef : (signedValRef | 0xfe00);
	if (signedVal >= signedValRef)
	{
		rtn =  RP_TRUE;
	}

	return (rtn);
}

/******************************************************************************
Function Name:       RpSetLvlVthVal
Parameters:          none
Return value:        none
Description:         set lvl vth.
******************************************************************************/
void RpSetLvlVthVal( void )
{
	uint16_t lvlVth;

	if (RpCmpRefVthVal(RpCb.pib.phyLvlFltrVth) == RP_TRUE)
	{
		lvlVth = RpCb.pib.phyLvlFltrVth + RpCb.pib.phyAgcWaitGainOffset;
	}
	else
	{
		lvlVth = RpCb.pib.refVthVal + RpCb.pib.phyAgcWaitGainOffset;
	}

	// BBLVLVTH
	RpRegBlockWrite(BBLVLVTH, (uint8_t RP_FAR *)&lvlVth, sizeof(uint16_t));
}

/******************************************************************************
Function Name:       RpSetCcaDurationVal
Parameters:          pibValReq
Return value:        none
Description:         set cca time.
******************************************************************************/
void
RpSetCcaDurationVal(uint8_t pibValReq)
{
	uint16_t ccaDuration = RpCb.pib.phyCcaDuration;
	uint16_t ccaRealTime;
	uint16_t antDivTime;
	uint16_t antSwitchingTime = RpCb.pib.phyAntennaSwitchingTime;

	if (pibValReq == RP_TRUE)
	{
		ccaRealTime = ccaDuration;
		antDivTime = ccaDuration;
	}
	else
	{
		if (antSwitchingTime == RP_RF_PIB_DFLT_ANTENNA_SWITCHING_TIME)
		{
			if ((RpCb.pib.phyRegulatoryMode == RP_RMODE_ENABLE_ARIB) && (RpCb.pib.phyFreqBandId == RP_PHY_FREQ_BAND_920MHz))
			{
				ccaRealTime = (uint16_t)RpFreqBandTbl[RpCb.freqIdIndex][RP_CCA_ANTDVT_ON_ARIB];
				antDivTime = (uint16_t)RpFreqBandTbl[RpCb.freqIdIndex][RP_CCA_ANTDVT_ON_ARIB];
			}
			else
			{
				ccaRealTime = (uint16_t)RpFreqBandTbl[RpCb.freqIdIndex][RP_CCA_ANTDVT_ON];
				antDivTime = (uint16_t)RpFreqBandTbl[RpCb.freqIdIndex][RP_CCA_ANTDVT_ON];
			}
		}
		else
		{
			ccaRealTime = antSwitchingTime;
			antDivTime = antSwitchingTime;
		}
	}
	// BBCCATIME
	RpRegBlockWrite(BBCCATIME, (uint8_t RP_FAR *)&ccaRealTime, sizeof(uint16_t));
	// Antenna Switching Time
	RpRegBlockWrite(BBANTDIVTIM, (uint8_t RP_FAR *)&antDivTime, sizeof(uint16_t));
}

/******************************************************************************
Function Name:       RpInverseTxAnt
Parameters:          invAntPortEna
Return value:        none
Description:         set cca time.
******************************************************************************/
void
RpInverseTxAnt(uint8_t invAntPortEna)
{
	uint8_t antDvrEn = RpCb.pib.phyAntennaDiversityRxEna;
	uint8_t antTxSel = RpCb.pib.phyAntennaSelectTx;
	uint8_t antDvrCon;

	if (antDvrEn)
	{
		antDvrCon = RpRegRead(BBANTDIV);

		if (antTxSel ^ invAntPortEna)
		{
			antDvrCon |= ANTSWTRNSET;
		}
		else
		{
			antDvrCon &= ~ANTSWTRNSET;
		}
		RpRegWrite(BBANTDIV, antDvrCon);
	}
	else if (antTxSel != RP_PHY_ANTENNA_UNUSE)
	{
#ifdef RP_NOT_INVERT_TX_ANNTENA
		if (antTxSel)
#else
		if (antTxSel ^ invAntPortEna)
#endif
		{
			RpCb.reg.bbGpioData &= ~GPIO1DATA;
			RpCb.reg.bbGpioData |= GPIO2DATA;
		}
		else
		{
			RpCb.reg.bbGpioData &= ~GPIO2DATA;
			RpCb.reg.bbGpioData |= GPIO1DATA;
		}
		RpRegWrite(BBGPIODATA, RpCb.reg.bbGpioData);
	}
}

/******************************************************************************
Function Name:       RpSetPreambleLengthVal
Parameters:          none
Return value:        none
Description:         set preamble length.
******************************************************************************/
void RpSetPreambleLengthVal( void )
{
	// BBPAMBL
	RpRegBlockWrite(BBPAMBL, (uint8_t RP_FAR *)&RpCb.pib.phyFskPreambleLength, sizeof(uint16_t));
}

/******************************************************************************
Function Name:       RpSetMrFskSfdVal
Parameters:          none
Return value:        none
Description:         set SFD.
******************************************************************************/
void RpSetMrFskSfdVal( void )
{
	uint8_t sfdMode = RpCb.pib.phyMrFskSfd, opeMode = RpCb.pib.phyFskOpeMode, freqBandId = RpCb.pib.phyFreqBandId;
	uint32_t sfdVal1, sfdVal2, sfdVal3, sfdVal4;

	// BBSFD(1) - BBSFD4
	if (((freqBandId == RP_PHY_FREQ_BAND_920MHz) && (opeMode == RP_PHY_FSK_OPEMODE_4))
			|| ((freqBandId == RP_PHY_FREQ_BAND_863MHz) && (opeMode == RP_PHY_FSK_OPEMODE_3)))
	{
		sfdVal1 = RP_SFDVAL_MD0_4FSKNOFEC;
		sfdVal2 = RP_SFDVAL_MD0_4FSKFEC;
		sfdVal3 = RP_SFDVAL_MD1_4FSKNOFEC;
		sfdVal4 = RP_SFDVAL_MD1_4FSKFEC;
	}
	else
	{
		sfdVal1 = RP_SFDVAL_MD0_2FSKNOFEC;
		sfdVal2 = RP_SFDVAL_MD0_2FSKFEC;
		sfdVal3 = RP_SFDVAL_MD1_2FSKNOFEC;
		sfdVal4 = RP_SFDVAL_MD1_2FSKFEC;
	}

	if (sfdMode)
	{
		RpCb.reg.bbFecCon |= MRFSKSFD;
	}
	else
	{
		RpCb.reg.bbFecCon &= ~MRFSKSFD;
	}
	RpRegWrite(BBFECCON, (uint8_t)(RpCb.reg.bbFecCon));

	RpRegBlockWrite(BBSFD, (uint8_t RP_FAR *)&sfdVal1, sizeof(uint32_t));
	RpRegBlockWrite(BBSFD2, (uint8_t RP_FAR *)&sfdVal2, sizeof(uint32_t));
	RpRegBlockWrite(BBSFD3, (uint8_t RP_FAR *)&sfdVal3, sizeof(uint32_t));
	RpRegBlockWrite(BBSFD4, (uint8_t RP_FAR *)&sfdVal4, sizeof(uint32_t));
}

/******************************************************************************
Function Name:       RpSetFskScramblePsduVal
Parameters:          none
Return value:        none
Description:         set Scramble PSDU enable.
******************************************************************************/
void RpSetFskScramblePsduVal( void )
{
	if (RpCb.pib.phyFskScramblePsdu)
	{
		RpCb.reg.bbSubCon |= DWEN;
	}
	else
	{
		RpCb.reg.bbSubCon &= ~DWEN;
	}
	RpRegWrite(BBSUBGCON, (uint8_t)(RpCb.reg.bbSubCon));
}

/******************************************************************************
Function Name:       RpSetFcsLengthVal
Parameters:          none
Return value:        none
Description:         set FCS length.
******************************************************************************/
void
RpSetFcsLengthVal(void)
{
	if (RpCb.pib.phyFcsLength == RP_FCS_LENGTH_16BIT)
	{
		RpCb.reg.bbSubCon |= CRCBIT;
	}
	else if (RpCb.pib.phyFcsLength == RP_FCS_LENGTH_32BIT)
	{
		RpCb.reg.bbSubCon &= ~CRCBIT;
	}
	RpRegWrite(BBSUBGCON, (uint8_t)(RpCb.reg.bbSubCon));
}

/******************************************************************************
Function Name:       RpSetAckReplyTimeVal
Parameters:          none
Return value:        none
Description:         set ack reply time.
******************************************************************************/
void RpSetAckReplyTimeVal( void )
{
	uint16_t ackReply = (uint16_t)(RpCb.pib.phyAckReplyTime - RP_ACKTX_WARMUP_TIME);

	// BBACKRTNTIM
	RpRegBlockWrite(BBACKRTNTIM, (uint8_t RP_FAR *)&ackReply, sizeof(uint16_t));
}

/******************************************************************************
Function Name:       RpSetAckWaitDurationVal
Parameters:          none
Return value:        none
Description:         set ack wait duration.
******************************************************************************/
void RpSetAckWaitDurationVal( void )
{
	// BBACKRCVWIT
	RpRegBlockWrite(BBACKRCVWIT, (uint8_t RP_FAR *)&RpCb.pib.phyAckWaitDuration, sizeof(uint16_t));
}

/******************************************************************************
Function Name:       RpSetCsmaBackoffPeriod
Parameters:          seed
Return value:        none
Description:         set backoff period.
******************************************************************************/
void RpSetCsmaBackoffPeriod( void )
{
	RpRegBlockWrite(BBBOFFPERIOD, (uint8_t RP_FAR *)&RpCb.pib.phyCsmaBackoffPeriod, sizeof(uint16_t));
}

/******************************************************************************
Function Name:       RpSetBackOffSeedVal
Parameters:          seed
Return value:        none
Description:         set SFD.
******************************************************************************/
void RpSetBackOffSeedVal( void )
{
	uint8_t seed = RpCb.pib.phyBackOffSeed;

	// BBBOFFPROD0 and BBBOFFPROD2
	RpRegWrite(BBBOFFPROD2, seed);									// Seed set
	RpRegWrite(BBBOFFPROD, (uint8_t)(BOFFPRODEN));	// Backoff periode Auto enaable
}

/******************************************************************************
Function Name:       RpSetRssiOffsetVal
Parameters:          none
Return value:        none
Description:         set rssioffset.
******************************************************************************/
static void RpSetRssiOffsetVal( void )
{
	// RSSIOFFSET
	RpRegWrite((0x0483 << 3), (uint8_t)(RpCb.pib.phyRssiLoss & 0x3F));
}

/******************************************************************************
Function Name:       RpSetFecVal
Parameters:          none
Return value:        none
Description:         set FEC.
******************************************************************************/
void RpSetFecVal( void )
{
	uint8_t fecTxEn = RpCb.pib.phyFskFecTxEna, fecRxEn = RpCb.pib.phyFskFecRxEna, fecMode = RpCb.pib.phyFskFecScheme;
	uint8_t rscCon;

	// BBFECCON
	RpCb.reg.bbSubCon &= ~(FECENRX | FECMODE | INTERLEAVEEN);
	RpCb.reg.bbFecCon &= ~(FECAUTOEN | FECENTX | FECCONACKRTN | FECENAACKRTN | FECCONACKRCV | FECENAACKRCV);
	// MDMCNT40F
	rscCon = RpRegRead(0x040F << 3);
	rscCon &= (~INTERLEAVEEN);
	if (fecTxEn)
	{
		RpCb.reg.bbFecCon |= FECENTX;
		RpCb.reg.bbFecCon |= FECCONACKRTN;
		RpCb.reg.bbFecCon |= FECENAACKRTN;
	}
	if (fecRxEn)
	{
		RpCb.reg.bbSubCon |= FECENRX;
		RpCb.reg.bbFecCon |= FECENAACKRCV;
		if (fecRxEn == RP_FEC_RX_MODE_AUTO_DISTINCT)
		{
			RpCb.reg.bbFecCon |= FECAUTOEN;
		}
		else
		{
			RpCb.reg.bbFecCon |= FECCONACKRCV;
		}
	}
	if (fecMode)
	{
		RpCb.reg.bbSubCon |= FECMODE;
	}
	if (fecTxEn || fecRxEn)
	{
		rscCon |= INTERLEAVEEN;
		RpCb.reg.bbSubCon |= INTERLEAVEEN;
	}

	RpRegWrite(BBSUBGCON, RpCb.reg.bbSubCon);
	RpRegWrite(BBFECCON, RpCb.reg.bbFecCon);
	RpRegWrite(0x040F << 3, rscCon);
}

/******************************************************************************
Function Name:       RpSetMaxCsmaBackoffVal
Parameters:          none
Return value:        none
Description:         set RpSetMaxCsmaBackoff
******************************************************************************/
void RpSetMaxCsmaBackoffVal( void )
{
	// BBCSMACON1
	RpCb.reg.bbCsmaCon1 &= (~NB);
	RpCb.reg.bbCsmaCon1 |= (RpCb.pib.macMaxCsmaBackOff & NB);
	RpRegWrite(BBCSMACON1, RpCb.reg.bbCsmaCon1);
}

/******************************************************************************
Function Name:       RpSetMaxBeVal
Parameters:          none
Return value:        none
Description:         set RpSetMaxBe
******************************************************************************/
void RpSetMaxBeVal( void )
{
	// BBCSMACON2
	RpCb.reg.bbCsmaCon2 &= ~(BEMAX);
	RpCb.reg.bbCsmaCon2 |= (RpCb.pib.macMaxBe & BEMAX);
	RpRegWrite(BBCSMACON2, RpCb.reg.bbCsmaCon2);
}

/******************************************************************************
Function Name:       RpSetMiscellaneousIniVal
Parameters:          none
Return value:        none
Description:         set miscellaneous inital value
******************************************************************************/
static void RpSetMiscellaneousIniVal( void )
{
	// BBTXRXMODE2
	RpCb.reg.bbTxRxMode2 = ENHACKEN;			// Enhanced Ack Enable always set
	RpRegWrite(BBTXRXMODE2, RpCb.reg.bbTxRxMode2);	// Auto CRC,Retry=0

	// BBTXRXMODE3
	RpCb.reg.bbTxRxMode3 |= ADFGENMODE;			// General Mode Address Filter
	RpCb.reg.bbTxRxMode3 |= RCVSTOREBANKSEL;
	RpCb.reg.bbTxRxMode3 |= (RXEN | LVLFILEN);
	RpRegWrite(BBTXRXMODE3, RpCb.reg.bbTxRxMode3);
}

/******************************************************************************
Function Name:       RpSetSpecificModeVal
Parameters:          afterReset
Return value:        none
Description:         set specific mode
******************************************************************************/
void RpSetSpecificModeVal( uint8_t afterReset )
{
	// BBTXRXMODE0
	RpCb.reg.bbTxRxMode0 |= CCATYPE0;			// CCA,Auto Ack Disable,Auto Receive Disable
	if (RpCb.pib.phyProfileSpecificMode != RP_SPECIFIC_WSUN)
	{
		RpCb.reg.bbTxRxMode0 |= INTRAPANEN;
	}
	else
	{
		RpCb.reg.bbTxRxMode0 &= (uint8_t)(~INTRAPANEN);
	}
	RpRegWrite(BBTXRXMODE0, RpCb.reg.bbTxRxMode0);

	// BBCSMACON2
	if (afterReset == RP_FALSE)
	{
		if (RpCb.pib.phyProfileSpecificMode == RP_SPECIFIC_WSUN)
		{
			RpCb.reg.bbCsmaCon2 |= UNICASTFRMEN;
		}
		else
		{
			RpCb.reg.bbCsmaCon2 &= (~UNICASTFRMEN);
		}
		RpSetMaxBeVal();
	}

	// BBTXRXMODE1
	RpCb.reg.bbTxRxMode1 &= (~SQCNUMSUPEN);
	RpCb.reg.bbTxRxMode1 |= ACKRCVPOINT;

	RpRegWrite(BBTXRXMODE1, RpCb.reg.bbTxRxMode1);	// Auto Ack Receive Enable
}

/******************************************************************************
Function Name:       RpSetPreamble4ByteRxMode
Parameters:          none
Return value:        none
Description:         Preamble 4Byte Rx Mode.
******************************************************************************/
void RpSetPreamble4ByteRxMode( void )
{
	uint8_t	val0x0472;

	if (RpCb.pib.phyPreamble4ByteRxMode == RP_TRUE)
	{
		val0x0472 = 0x04;
	}
	else
	{
		val0x0472 = 0x05;
	}

	RpRegWrite((0x0472 << 3), val0x0472);
}

/******************************************************************************
Function Name:       RpSetAgcStartVth
Parameters:          reset
Return value:        none
Description:         AGC Start Threshold Setting
******************************************************************************/
void RpSetAgcStartVth( uint8_t reset )
{
	uint8_t index = RpCb.freqIdIndex;
	uint16_t reg;

	if (reset || (RpCb.pib.phyAgcStartVth == RP_RF_PIB_DFLT_AGC_START_VTH))
	{
		RpCb.pib.phyAgcStartVth = RP_RF_PIB_DFLT_AGC_START_VTH;

		if (RpCb.pib.phyFskFecRxEna == RP_FEC_RX_MODE_ENABLE)
		{
			reg = ((uint16_t)(RpFreqBandTbl[index][RP_0x0458_F_OFFSET]) << 8) | (RpFreqBandTbl[index][RP_0x0457_F_OFFSET]);
		}
		else
		{
			reg = ((uint16_t)(RpFreqBandTbl[index][RP_0x0458_OFFSET]) << 8) | (RpFreqBandTbl[index][RP_0x0457_OFFSET]);
		}
	}
	else
	{
		reg = RpCb.pib.phyAgcStartVth;
	}
	RpRegBlockWrite((0x0457 << 3), (uint8_t RP_FAR *)&reg, sizeof(uint16_t));
}

/******************************************************************************
Function Name:       RpGetAgcStartVth
Parameters:          none
Return value:        none
Description:         AGC Start Threshold Setting
******************************************************************************/
uint16_t RpGetAgcStartVth( void )
{
	uint16_t reg;

	RpRegBlockRead((0x0457 << 3), (uint8_t *)&reg, sizeof(uint16_t));
	return reg;
}

/******************************************************************************
Function Name:       RpSetCcaEdBandwidth
Parameters:          none
Return value:        none
Description:         CCA or ED Bandwidth setting
******************************************************************************/
void RpSetCcaEdBandwidth( void )
{
	if ((RpCb.pib.phyCcaBandwidth == RP_PHY_CCA_BANDWIDTH_INDEX_225K) && (RpCb.pib.phyEdBandwidth == RP_PHY_CCA_BANDWIDTH_INDEX_225K))	// default filter
	{
		RpRegCcaBandwidth225k();
		RpRegAdcVgaDefault();
		RpRegRxDataRateDefault();
		RpChangeDefaultFilter();
	}
}

/******************************************************************************
Function Name:       RpSetAntennaDiversityStartVth
Parameters:          reset
Return value:        none
Description:         Antenna Diversity Start Threshold Setting
******************************************************************************/
void RpSetAntennaDiversityStartVth( uint8_t reset )
{
	uint8_t index = RpCb.freqIdIndex;
	uint16_t reg;

	if (reset || (RpCb.pib.phyAntennaDiversityStartVth == RP_RF_PIB_DFLT_ANTENNA_DIVERSITY_START_VTH))
	{
		RpCb.pib.phyAntennaDiversityStartVth = RP_RF_PIB_DFLT_ANTENNA_DIVERSITY_START_VTH;
		reg = (uint16_t)RpFreqBandTbl[index][RP_ANTDIVVTH_OFFSET] | 0x0100;
	}
	else
	{
		reg = RpCb.pib.phyAntennaDiversityStartVth;
	}
	RpRegBlockWrite(BBANTDIVVTH, (uint8_t RP_FAR *)&reg, sizeof(uint16_t));
}

/******************************************************************************
Function Name:       RpGetAntennaDiversityStartVth
Parameters:          none
Return value:        none
Description:         Antenna Diversity Start Threshold Setting
******************************************************************************/
uint16_t RpGetAntennaDiversityStartVth( void )
{
	uint16_t reg;

	RpRegBlockRead(BBANTDIVVTH, (uint8_t *)&reg, sizeof(uint16_t));
	return reg;
}

/******************************************************************************
Function Name:       RpGetAntennaDiversityStartVth
Parameters:          none
Return value:        none
Description:         Antenna Diversity Start Threshold Setting
******************************************************************************/
uint16_t RpGetAntennaSwitchingTime( void )
{
	uint8_t index = RpCb.freqIdIndex;
	uint16_t reg;

	if (RpCb.pib.phyAntennaSwitchingTime == RP_RF_PIB_DFLT_ANTENNA_SWITCHING_TIME)
	{
		if ((RpCb.pib.phyRegulatoryMode == RP_RMODE_ENABLE_ARIB) && (RpCb.pib.phyFreqBandId == RP_PHY_FREQ_BAND_920MHz))
		{
			reg = (uint16_t)(RpFreqBandTbl[index][RP_CCA_ANTDVT_ON_ARIB]);
		}
		else
		{
			reg = (uint16_t)(RpFreqBandTbl[index][RP_CCA_ANTDVT_ON]);
		}
	}
	else
	{
		reg = RpCb.pib.phyAntennaSwitchingTime;
	}
	return reg;
}

/******************************************************************************
Function Name:       RpSetSfdDetectionExtend
Parameters:          none
Return value:        none
Description:         SFD Detection is Extended
******************************************************************************/
void RpSetSfdDetectionExtend( void )
{
	uint8_t index = RpCb.freqIdIndex;
	uint8_t SfdDetExt = RpCb.pib.phySfdDetectionExtend;
	uint8_t MrFskSfd = RpCb.pib.phyMrFskSfd;
	uint8_t FecTxEna = RpCb.pib.phyFskFecTxEna;
	uint8_t	FecRxEna = RpCb.pib.phyFskFecRxEna;
	uint16_t wk16;

	RpCb.sfdExtend.writeIn = RP_FALSE;
	RpCb.sfdExtend.enableFec = RP_FALSE;
	RpCb.sfdExtend.rxFecAuto = RP_FALSE;

	if ((index != RP_PHY_200K_M03) && (index != RP_PHY_400K_M03))	// except for 4FSK
	{
		if (SfdDetExt == RP_TRUE)
		{
			RpCb.sfdExtend.writeIn = RP_TRUE;

			wk16 = (uint16_t)(RP_SFDVAL_MD0_2FSKNOFEC);
			RpRegBlockWrite(BBSFD_2, (uint8_t RP_FAR *)&(wk16), sizeof(uint16_t));

			if (MrFskSfd == RP_FALSE)
			{					
				if (FecTxEna == RP_FALSE)
				{
					RpCb.sfdExtend.txAddress0 = BBSFD;
					RpCb.sfdExtend.txValue0 = (uint16_t)(RP_SFDVAL_MD0_2FSKNOFEC);
					RpCb.sfdExtend.txAddress1 = BBSFD2;
					RpCb.sfdExtend.txValue1 = (uint16_t)(RP_SFDVAL_MD0_2FSKFEC);
				}
				else
				{
					RpCb.sfdExtend.txAddress0 = BBSFD2;
					RpCb.sfdExtend.txValue0 = (uint16_t)(RP_SFDVAL_MD0_2FSKFEC);
					RpCb.sfdExtend.txAddress1 = BBSFD;
					RpCb.sfdExtend.txValue1 = (uint16_t)(RP_SFDVAL_MD0_2FSKNOFEC);
				}

				if (FecRxEna == RP_FEC_RX_MODE_DISABLE)
				{
					RpCb.sfdExtend.rxAddress0 = BBSFD;
				}
				else if (FecRxEna == RP_FEC_RX_MODE_ENABLE)
				{
					RpCb.sfdExtend.rxAddress0 = BBSFD2;
				}
				else // if (FecRxEna == RP_FEC_RX_MODE_AUTO_DISTINCT)
				{
					RpCb.sfdExtend.rxAddress0 = BBSFD;
					RpCb.sfdExtend.rxAddress1 = BBSFD2;
				}
			}
			else // if (MrFskSfd == RP_TRUE)
			{
				if (FecTxEna == RP_FALSE)
				{
					RpCb.sfdExtend.txAddress0 = BBSFD3;
					RpCb.sfdExtend.txValue0 = (uint16_t)(RP_SFDVAL_MD1_2FSKNOFEC);
					RpCb.sfdExtend.txAddress1 = BBSFD4;
					RpCb.sfdExtend.txValue1 = (uint16_t)(RP_SFDVAL_MD1_2FSKFEC);
				}
				else
				{
					RpCb.sfdExtend.txAddress0 = BBSFD4;
					RpCb.sfdExtend.txValue0 = (uint16_t)(RP_SFDVAL_MD1_2FSKFEC);
					RpCb.sfdExtend.txAddress1 = BBSFD3;
					RpCb.sfdExtend.txValue1 = (uint16_t)(RP_SFDVAL_MD1_2FSKNOFEC);
				}

				if (FecRxEna == RP_FEC_RX_MODE_DISABLE)
				{
					RpCb.sfdExtend.rxAddress0 = BBSFD3;
				}
				else if (FecRxEna == RP_FEC_RX_MODE_ENABLE)
				{
					RpCb.sfdExtend.rxAddress0 = BBSFD4;
				}
				else // if (FecRxEna == RP_FEC_RX_MODE_AUTO_DISTINCT)
				{
					RpCb.sfdExtend.rxAddress0 = BBSFD3;
					RpCb.sfdExtend.rxAddress1 = BBSFD4;
				}
			}

			if ((FecTxEna != RP_FALSE) || (FecRxEna != RP_FALSE))
			{
				RpCb.sfdExtend.enableFec = RP_TRUE;

				if (FecRxEna == RP_FEC_RX_MODE_AUTO_DISTINCT)
				{
					RpCb.sfdExtend.rxFecAuto = RP_TRUE;
				}
			}
		}
		else
		{
			RpSetMrFskSfdVal();
			RpRegWrite(BBSHRCON, 0x02);
		}
	}
	else
	{
		RpSetMrFskSfdVal();
		RpRegWrite(BBSHRCON, 0x06);
	}
}

/******************************************************************************
Function Name:       RpSetSfdDetectionExtendWrite
Parameters:          status
Return value:        none
Description:         The writing in to expand SFD detection
******************************************************************************/
void RpSetSfdDetectionExtendWrite( uint16_t status )
{
	uint16_t txAddress0 = RpCb.sfdExtend.txAddress0;
	uint16_t txAddress1 = RpCb.sfdExtend.txAddress1;
	uint16_t txValue0 = RpCb.sfdExtend.txValue0;
	uint16_t txValue1 = RpCb.sfdExtend.txValue1;
	uint16_t rxAddress0 = RpCb.sfdExtend.rxAddress0;
	uint16_t rxAddress1 = RpCb.sfdExtend.rxAddress1;
	uint8_t rxFecAuto = RpCb.sfdExtend.rxFecAuto;
	uint16_t preamblePattern = RP_SFDVAL_EXT_PREAMBLE;

	if (RpCb.sfdExtend.writeIn == RP_TRUE)
	{
		if (((status & RP_PHY_STAT_TX) == RP_FALSE) && (status & RP_PHY_STAT_RX) && ((RpCb.reg.bbTxRxMode0 & AUTOACKEN) == RP_FALSE))
		{
			RpRegBlockWrite(rxAddress0, (uint8_t RP_FAR *)&preamblePattern, sizeof(uint16_t));
			
			if (rxFecAuto == RP_TRUE)
			{
				RpRegBlockWrite(rxAddress1, (uint8_t RP_FAR *)&preamblePattern, sizeof(uint16_t));
			}
			RpRegWrite(BBSHRCON, 0x06);
		}
		else
		{
			RpRegBlockWrite(txAddress0, (uint8_t RP_FAR *)&txValue0, sizeof(uint16_t));

			if (RpCb.sfdExtend.enableFec == RP_TRUE)
			{
				RpRegBlockWrite(txAddress1, (uint8_t RP_FAR *)&txValue1, sizeof(uint16_t)); 
			}
			RpRegWrite(BBSHRCON, 0x02);
		}
	}
}

/******************************************************************************
Function Name:       RpSetAgcWaitGain
Parameters:          none
Return value:        none
Description:         AGC Wait Gain Setting
******************************************************************************/
void RpSetAgcWaitGain( void )
{
	uint8_t	val0x0440;

	val0x0440 = RP_DEFAULT_AGC_WAIT_GAIN - RpCb.pib.phyAgcWaitGainOffset;
	RpRegWrite((0x0440 << 3), val0x0440);

	RpSetLvlVthVal();
}

/******************************************************************************
Function Name:       RpSetAntennaSwitchEnaTiming
Parameters:          none
Return value:        none
Description:         set antenna switch timing.
******************************************************************************/
void RpSetAntennaSwitchEnaTiming( void )
{
	// AntennaSwitchEnaTiming
	RpCb.pib.phyAntennaSwitchEnaTiming &= 0x03FF;
	RpRegBlockWrite(BBANTSWTIMG,  (uint8_t RP_FAR *)&RpCb.pib.phyAntennaSwitchEnaTiming, sizeof(uint16_t));
}

/******************************************************************************
 * function Name : RpSetGpio0Setting
 * parameters    : none
 * return value  : none
 * description   : Set GPIO0
 *****************************************************************************/
void RpSetGpio0Setting( void )
{
	RpCb.reg.bbGpioData &= ~GPIO0DATA;
	RpCb.reg.bbGpioData |= RpCb.pib.phyGpio0Setting;
	RpCb.reg.bbGpioData &= GPIOXDATA;
	RpRegWrite(BBGPIODATA, RpCb.reg.bbGpioData);

	RpCb.reg.bbGpioDir |= GPIO0DIR;
	RpCb.reg.bbGpioDir &= GPIOXDIR;
	RpRegWrite(BBGPIODIR, RpCb.reg.bbGpioDir);
}

/******************************************************************************
 * function Name : RpSetGpio3Setting
 * parameters    : none
 * return value  : none
 * description   : Set GPIO3
 *****************************************************************************/
void RpSetGpio3Setting( void )
{
	RpCb.reg.bbGpioData &= ~GPIO3DATA;
	RpCb.reg.bbGpioData |= (RpCb.pib.phyGpio3Setting << 3);
	RpCb.reg.bbGpioData &= GPIOXDATA;
	RpRegWrite(BBGPIODATA, RpCb.reg.bbGpioData);

	RpCb.reg.bbGpioDir |= GPIO3DIR;
	RpCb.reg.bbGpioDir &= GPIOXDIR;
	RpRegWrite(BBGPIODIR, RpCb.reg.bbGpioDir);
}

/******************************************************************************
Function Name:       RpPrevSentTimeReSetting
Parameters:          none
Return value:        none
Description:         Set previous timer re-setting.
******************************************************************************/
void RpPrevSentTimeReSetting( void )
{
	uint32_t realTime, nowTime, difTime, difTime2;

	realTime = RpCb.tx.sentTime;
	if (RpCb.pib.phyRegulatoryMode == RP_RMODE_ENABLE)
	{
		difTime = (uint32_t)(RpCb.pib.phyRmodeToffMin) * (uint32_t)(RpCb.pib.phyDataRate);
	}
	else
	{
		difTime = RpCalcTxInterval(RpCb.tx.sentLen, RP_FALSE);
	}
	realTime += difTime;
	realTime &= RP_TIME_MASK;
	nowTime = RpGetTime();
	difTime2 = (uint32_t)((realTime - nowTime/* tx time considered interval time */) & RP_TIME_MASK);
	if ((difTime2 <= RP_TIME_LIMIT) && (difTime2 >= RP_RETX_TIMER_WASTE_TIME))
	{
		RpCb.tx.sentTime = ((0xffffffff - (difTime - difTime2)) & RP_TIME_MASK);
	}
	else
	{
		RpCb.tx.sentTime = 0;
	}
}

/******************************************************************************
Function Name:       RpInitVar
Parameters:          refreshFlg
Return value:        none
Description:         set  default variables.
******************************************************************************/
static void RpInitVar( uint8_t refreshFlg )
{
	uint32_t eddr1[2], eddr2[2];
	uint8_t regulatoryMode;
	uint16_t rmodeTonMax;
	uint16_t rmodeToffMin;
	uint16_t rmodeTcumSmpPeriod;
	uint32_t rmodeTcumLimit;
	uint32_t rmodeTcum;
	uint16_t senLen;
	uint32_t sentTime;
	uint32_t sentElapsedTimeS;
	uint8_t needTxWaitRegulatoryMode;
	uint32_t regulatoryModeElapsedTimeS;
	uint16_t ackLenForRegulatoryMode;
	uint8_t  applyAckTotalTxTime;
	uint8_t  applyAckToffMin;
#ifdef R_FSB_FAN_ENABLED
	uint8_t  isFsbDownlink;
#endif // #ifdef R_FSB_FAN_ENABLED

	if (refreshFlg == RP_FALSE)
	{
		RpMemset(&RpCb, 0, sizeof(RpCb));
		RpMemset(&RpTxTime, 0, sizeof(RpTxTime));
		RpMemset((uint8_t *)RpCb.pib.macExtendedAddress1, 0x00, sizeof(uint32_t[2]));
		RpMemset((uint8_t *)RpCb.pib.macExtendedAddress2, 0x00, sizeof(uint32_t[2]));
		RpCb.pib.phyRegulatoryMode = RP_RF_PIB_DFLT_REGULATORY_MODE;
		RpCb.pib.phyRmodeTonMax = RP_RF_PIB_DFLT_RMODE_TON_MAX;
		RpCb.pib.phyRmodeToffMin = RP_RF_PIB_DFLT_RMODE_TOFF_MIN;
		RpCb.pib.phyRmodeTcumSmpPeriod = RP_RF_PIB_DFLT_RMODE_TCUM_SMP_PERIOD;
		RpCb.pib.phyRmodeTcumLimit = RP_RF_PIB_DFLT_RMODE_TCUM_LIMIT;
		RpCb.pib.phyRmodeTcum = RP_RF_PIB_DFLT_RMODE_TCUM;
	}
	else // if (refreshFlg == RP_TRUE)
	{
		RpMemcpy(eddr1, RpCb.pib.macExtendedAddress1, sizeof(uint32_t[2]));
		RpMemcpy(eddr2, RpCb.pib.macExtendedAddress2, sizeof(uint32_t[2]));
		regulatoryMode = RpCb.pib.phyRegulatoryMode;
		rmodeTonMax = RpCb.pib.phyRmodeTonMax;
		rmodeToffMin = RpCb.pib.phyRmodeToffMin;
		rmodeTcumSmpPeriod = RpCb.pib.phyRmodeTcumSmpPeriod;
		rmodeTcumLimit = RpCb.pib.phyRmodeTcumLimit;
		rmodeTcum = RpCb.pib.phyRmodeTcum;
		senLen = RpCb.tx.sentLen;
		sentTime = RpCb.tx.sentTime;
		sentElapsedTimeS = RpCb.tx.sentElapsedTimeS;
		needTxWaitRegulatoryMode = RpCb.tx.needTxWaitRegulatoryMode;
		regulatoryModeElapsedTimeS = RpCb.tx.regulatoryModeElapsedTimeS;
		ackLenForRegulatoryMode = RpCb.tx.ackLenForRegulatoryMode;
		applyAckTotalTxTime = RpCb.tx.applyAckTotalTxTime;
		applyAckToffMin = RpCb.tx.applyAckToffMin;
#ifdef R_FSB_FAN_ENABLED
		isFsbDownlink = RpCb.tx.isFsbDownlink;
#endif // #ifdef R_FSB_FAN_ENABLED
		RpMemset(&RpCb.tx, 0x00, sizeof(RpCb.tx));
		RpMemset(&RpCb.rx, 0x00, sizeof(RpCb.rx));
		RpMemset(&RpCb.pib, 0x00, sizeof(RpCb.pib));
		RpCb.freqIdIndex = 0x00;
		RpCb.prohibitLowPower = RP_FALSE;
		RpMemset(&RpCb.reg, 0x00, sizeof(RpCb.reg));
		RpMemcpy(RpCb.pib.macExtendedAddress1, eddr1, sizeof(uint32_t[2]));
		RpMemcpy(RpCb.pib.macExtendedAddress2, eddr2, sizeof(uint32_t[2]));
		RpCb.pib.phyRegulatoryMode = regulatoryMode;
		RpCb.pib.phyRmodeTonMax= rmodeTonMax;
		RpCb.pib.phyRmodeToffMin= rmodeToffMin;
		RpCb.pib.phyRmodeTcumSmpPeriod= rmodeTcumSmpPeriod;
		RpCb.pib.phyRmodeTcumLimit= rmodeTcumLimit;
		RpCb.pib.phyRmodeTcum= rmodeTcum;
		RpCb.tx.sentLen = senLen;
		RpCb.tx.sentTime = sentTime;
		RpCb.tx.sentElapsedTimeS = sentElapsedTimeS;
		RpCb.tx.needTxWaitRegulatoryMode = needTxWaitRegulatoryMode;
		RpCb.tx.regulatoryModeElapsedTimeS = regulatoryModeElapsedTimeS;
		RpCb.tx.ackLenForRegulatoryMode = ackLenForRegulatoryMode;
		RpCb.tx.applyAckTotalTxTime = applyAckTotalTxTime;
		RpCb.tx.applyAckToffMin = applyAckToffMin;
#ifdef R_FSB_FAN_ENABLED
		RpCb.tx.isFsbDownlink = isFsbDownlink;
#endif // #ifdef R_FSB_FAN_ENABLED
	}
	RpCb.pib.phyCurrentChannel   		= RP_RF_PIB_DFLT_CURRENT_CHANNEL;
	RpCb.pib.phyChannelsSupportedPage 	= RP_RF_PIB_DFLT_CHANNEL_SUPPORTED_INDEX;
	RpCb.pib.phyChannelsSupported[0]	= RP_RF_PIB_DFLT_SUPPORT_CH_L;
	RpCb.pib.phyChannelsSupported[1]	= RP_RF_PIB_DFLT_SUPPORT_CH_H;
	RpCb.pib.phyTransmitPower	  		= RpConfig.txPowerDefault;
	RpCb.pib.macAddressFilter1Ena		= RP_RF_PIB_DFLT_ADDRESS_FILTER1_ENA;
	RpCb.pib.macPanId1 					= RP_RF_PIB_DFLT_PANID1;
	RpCb.pib.macShortAddr1 				= RP_RF_PIB_DFLT_SHORTAD1;
	RpCb.pib.macPanCoord1 				= RP_RF_PIB_DFLT_PANCOORD1;
	RpCb.pib.macPendBit1 				= RP_RF_PIB_DFLT_PENDBIT1;
	RpCb.pib.macAddressFilter2Ena		= RP_RF_PIB_DFLT_ADDRESS_FILTER2_ENA;
	RpCb.pib.macPanId2 					= RP_RF_PIB_DFLT_PANID2;
	RpCb.pib.macShortAddr2 				= RP_RF_PIB_DFLT_SHORTAD2;
	RpCb.pib.macPanCoord2 				= RP_RF_PIB_DFLT_PANCOORD2;
	RpCb.pib.macPendBit2				= RP_RF_PIB_DFLT_PENDBIT2;
	RpCb.pib.macMaxCsmaBackOff 			= RP_RF_PIB_DFLT_MAXCSMABO;
	RpCb.pib.macMinBe 					= RP_RF_PIB_DFLT_MINBE;
	RpCb.pib.macMaxBe 					= RP_RF_PIB_DFLT_MAXBE;
	RpCb.pib.macMaxFrameRetries 		= RP_RF_PIB_DFLT_MAX_FRAME_RETRIES;
	RpCb.pib.phyCrcErrorUpMsg			= RP_RF_PIB_DFLT_CRCERROR_UPMSG;
	RpCb.pib.phyBackOffSeed 			= RP_RF_PIB_DFLT_BACKOFF_SEED;
	RpCb.pib.phyCcaVth					= RpConfig.ccaVthDefault & (~0xFE00);
	RpCb.pib.phyCcaDuration				= RP_RF_PIB_DFLT_CCA_DURATION;
	RpCb.pib.phyFskFecTxEna				= RP_RF_PIB_DFLT_FSK_FEC_TX_ENA;
	RpCb.pib.phyFskFecRxEna				= RP_RF_PIB_DFLT_FSK_FEC_RX_ENA;
	RpCb.pib.phyFskFecScheme			= RP_RF_PIB_DFLT_FSK_FEC_SCHEME;
	RpCb.pib.phyFskPreambleLength		= RP_RF_PIB_DFLT_FSK_PREAMBLE_LENGTH;
	RpCb.pib.phyMrFskSfd				= RP_RF_PIB_DFLT_MRFSK_SFD;
	RpCb.pib.phyFskScramblePsdu			= RP_RF_PIB_DFLT_FSK_SCRAMBLE_PSDU;
	RpCb.pib.phyFskOpeMode				= RP_RF_PIB_DFLT_FSK_OPE_MODE;
	RpCb.pib.phyFcsLength				= RP_RF_PIB_DFLT_FCS_LENGTH;
	RpCb.pib.phyProfileSpecificMode		= RpConfig.profileSpecificModeDefault;
	RpCb.pib.phyAntennaSwitchEna		= RpConfig.antSwEnaDefault;
	RpCb.pib.phyAntennaDiversityRxEna 	= RpConfig.antDvrEnaDefault;
	if (RpCb.pib.phyAntennaDiversityRxEna == RP_TRUE)
	{
		RpCb.pib.phyAntennaSelectTx		= RP_PHY_ANTENNA_0;
	}
	else
	{
		RpCb.pib.phyAntennaSelectTx		= RP_PHY_ANTENNA_UNUSE;
	}
	RpCb.pib.phyAntennaSelectAckTx		= RP_RF_PIB_DFLT_ANTENNA_SELECT_ACKTX;
	RpCb.pib.phyAntennaSelectAckRx		= RP_RF_PIB_DFLT_ANTENNA_SELECT_ACKRX;
	RpCb.pib.phyAckReplyTime			= RP_RF_PIB_DFLT_ACK_RESPONSE_TIME;
	RpCb.pib.phyAckWaitDuration			= RP_RF_PIB_DFLT_ACK_WAIT_DURATION;
	RpCb.pib.phyRxTimeoutMode			= RP_RF_PIB_DFLT_RXTIMEOUT_MODE;
	RpCb.pib.phyFreqBandId				= RP_RF_PIB_DFLT_FREQ_BAND_ID;
	RpCb.pib.phyLvlFltrVth				= RP_RF_PIB_DFLT_LVL_VTH;
	RpCb.pib.refVthVal					= RP_RF_PIB_DFLT_LVL_VTH;
	RpCb.pib.phyCsmaBackoffPeriod		= RP_RF_PIB_DFLT_BACKOFF_PERIOD;
	RpCb.pib.phyPreamble4ByteRxMode		= RP_RF_PIB_DFLT_PREAMBLE_4BYTE_RX_MODE;
	RpCb.pib.phyAgcStartVth				= RP_RF_PIB_DFLT_AGC_START_VTH;
	RpCb.pib.phyCcaBandwidth			= RP_RF_PIB_DFLT_CCA_BANDWIDTH;
	RpCb.pib.phyEdBandwidth				= RP_RF_PIB_DFLT_ED_BANDWIDTH;
	RpCb.pib.phyAntennaDiversityStartVth	= RP_RF_PIB_DFLT_ANTENNA_DIVERSITY_START_VTH;
	RpCb.pib.phyAntennaSwitchingTime	= RP_RF_PIB_DFLT_ANTENNA_SWITCHING_TIME;
	RpCb.pib.phySfdDetectionExtend		= RP_RF_PIB_DFLT_SFD_DETECTION_EXTEND;
	RpCb.pib.phyAgcWaitGainOffset		= RP_RF_PIB_DFLT_AGC_WAIT_GAIN_OFFSET;
	RpCb.pib.phyCcaVthOffset			= RP_RF_PIB_DFLT_CCA_VTH_OFFSET;
	RpCb.pib.phyAntennaSwitchEnaTiming	= RP_RF_PIB_DFLT_ANTENNA_SWITCH_ENA_TIMING;
	RpCb.pib.phyGpio0Setting			= RP_RF_PIB_DFLT_GPIO0_SETTING;
	RpCb.pib.phyGpio3Setting			= RP_RF_PIB_DFLT_GPIO3_SETTING;
	RpMemset(&RpRfStat, 0x00, sizeof(RP_PHY_ERROR));
	RpCb.status 						= RP_PHY_STAT_TRX_OFF;
	RpCb.statusRxTimeout 				= RP_STATUS_RX_TIMEOUT_INIT;
	RpSetAttr_phyAckWithCca( RP_FALSE );
	RpCb.pib.phyRssiOutputOffset		= RP_RF_PIB_DFLT_RSSI_OUTPUT_OFFSET;
	RpCb.rx.antdvTimerValue				= RP_PHY_ANTDV_TIMERVALUE;
#ifdef R_FREQUENCY_OFFSET_ENABLED
	RpCb.pib.phyFrequency				= RP_RF_HW_DFLT_FREQUENCY;
	RpSetAttr_phyFrequencyOffset( (int32_t)RP_RF_PIB_DFLT_FREQUENCY_OFFSET );
#endif // #ifdef R_FREQUENCY_OFFSET_ENABLED
}

/******************************************************************************
Function Name:       RpInitRfOnly
Parameters:          none
Return value:        none
Description:         set SFR for baseband.
******************************************************************************/
static void RpInitRfOnly( void )
{
	// set regval
	RpCb.reg.bbTimeCon		= RP_RF_HW_DFLT_BBTIMECON;
	RpCb.reg.bbTxRxMode0	= RP_RF_HW_DFLT_BBTXRXMODE0;
	RpCb.reg.bbTxRxMode1	= RP_RF_HW_DFLT_BBTXRXMODE1;
	RpCb.reg.bbTxRxMode2	= RP_RF_HW_DFLT_BBTXRXMODE2;
	RpCb.reg.bbTxRxMode3	= RP_RF_HW_DFLT_BBTXRXMODE3;
	RpCb.reg.bbTxRxMode4	= RP_RF_HW_DFLT_BBTXRXMODE4;
	RpCb.reg.bbCsmaCon0		= RP_RF_HW_DFLT_BBCSMACON0;
	RpCb.reg.bbCsmaCon1		= RP_RF_HW_DFLT_BBCSMACON1;
	RpCb.reg.bbCsmaCon2		= RP_RF_HW_DFLT_BBCSMACON2;
	RpCb.reg.bbGpioData		= RP_RF_HW_DFLT_BBGPIODATA;
	RpCb.reg.bbFecCon		= RP_RF_HW_DFLT_BBFECCON;
	RpCb.reg.bbSubCon		= RP_RF_HW_DFLT_BBSUBGCON;
	RpCb.reg.bbTxCon0		= RP_RF_HW_DFLT_BBTXCON0;
	RpCb.reg.bb19Er2		= RP_RF_HW_DFLT_BB19ER2;
	RpCb.reg.bb25Er2		= RP_RF_HW_DFLT_BB25ER2;
	RpCb.reg.bb1AEr2		= RP_RF_HW_DFLT_BB1AER2;

	// Set Regulatory Mode settings
	if ((RpCb.pib.phyRegulatoryMode == RP_RMODE_ENABLE) ||
		((RpCb.pib.phyRegulatoryMode == RP_RMODE_ENABLE_ARIB) && (RpCb.pib.phyFreqBandId == RP_PHY_FREQ_BAND_920MHz)))
	{
		RpSetMacTxLimitMode();
	}

	// Set Misllanious registers initial settings
	RpSetMiscellaneousIniVal();

	// Set Max csma backoff settings
	if (RpCb.pib.macMaxCsmaBackOff != RP_RF_HW_DFLT_MAXCSMABO)
	{
		RpSetMaxCsmaBackoffVal();
	}
	// Specification Mode
	RpSetSpecificModeVal(RP_TRUE);

	// Address Fileters
	if (RpCb.pib.macPanId1 != RP_RF_HW_DFLT_PANID1)
	{
		RpRegBlockWrite(BBPANID0, (uint8_t RP_FAR *)&RpCb.pib.macPanId1, sizeof(uint16_t));
	}

	if (RpCb.pib.macPanId2 != RP_RF_HW_DFLT_PANID2)
	{
		RpRegBlockWrite(BBPANID1, (uint8_t RP_FAR *)&RpCb.pib.macPanId2, sizeof(uint16_t));
	}

	if (RpCb.pib.macShortAddr1 != RP_RF_HW_DFLT_SHORTAD1)
	{
		RpRegBlockWrite(BBSHORTAD0, (uint8_t RP_FAR *)&RpCb.pib.macShortAddr1, sizeof(uint16_t));
	}

	if (RpCb.pib.macShortAddr2 != RP_RF_HW_DFLT_SHORTAD2)
	{
		RpRegBlockWrite(BBSHORTAD1, (uint8_t RP_FAR *)&RpCb.pib.macShortAddr2, sizeof(uint16_t));
	}

	if (RpCb.pib.macAddressFilter1Ena)
	{
		RpRegBlockWrite(BBEXTENDAD00, (uint8_t RP_FAR *)&RpCb.pib.macExtendedAddress1, sizeof(uint32_t[2]));
	}

	if (RpCb.pib.macAddressFilter2Ena)
	{
		RpRegBlockWrite(BBEXTENDAD10, (uint8_t RP_FAR *)&RpCb.pib.macExtendedAddress2, sizeof(uint32_t[2]));
	}

	// Other Setting
	if (RpCb.pib.phyAntennaDiversityRxEna)
	{
		RpSetAntennaDiversityVal();
	}

	if (RpCb.pib.phyAntennaSelectTx != RP_PHY_ANTENNA_UNUSE)
	{
		RpSetAntennaSelectTxVal();
	}

	if (RpCb.pib.phyAntennaSwitchEna)
	{
		RpSetAntennaSwitchVal();
	}

	if (RpCb.pib.phyPreamble4ByteRxMode)
	{
		RpSetPreamble4ByteRxMode();
	}

	RpSetFskOpeModeVal(RP_TRUE);

	// Timer
	RpCb.reg.bbTimeCon = (TIMEEN | CNTSRCSEL | STAMPTIMSEL);	// Timer Count Start, count source datarate, Comp0 transmit Disable Stay

	RpRegWrite(BBTIMECON, (uint8_t)(RpCb.reg.bbTimeCon));

	if (RpCb.pib.phyFskPreambleLength != RP_RF_HW_DFLT_FSK_PREAMBLE_LENGTH)
	{
		RpSetPreambleLengthVal();
	}

	if (RpCb.pib.phyCsmaBackoffPeriod != RP_RF_HW_DFLT_BACKOFF_PERIOD)
	{
		RpSetCsmaBackoffPeriod();
	}

	RpSetBackOffSeedVal();

	if (RpCb.pib.phyFskScramblePsdu != RP_RF_HW_DFLT_FSK_SCRAMBLE_PSDU)
	{
		RpSetFskScramblePsduVal();
	}

	if (RpCb.pib.phyFcsLength != RP_RF_HW_DFLT_FCS_LENGTH)
	{
		RpSetFcsLengthVal();
	}

	if (RpCb.pib.phyAckWaitDuration != RP_RF_HW_DFLT_ACK_WAIT_DURATION)
	{
		RpSetAckWaitDurationVal();
	}

	if ((RpCb.pib.phyFskFecTxEna != RP_RF_HW_DFLT_FSK_FEC_TX_ENA) || (RpCb.pib.phyFskFecRxEna != RP_RF_HW_DFLT_FSK_FEC_RX_ENA))
	{
		RpSetFecVal();
	}

	if ((RpCb.pib.macAddressFilter1Ena) || (RpCb.pib.macAddressFilter2Ena))
	{
		RpSetAckReplyTimeVal();
	}
	RpSetAdrfAndAutoAckVal();

	if (RpCb.pib.phyAgcStartVth != RP_RF_PIB_DFLT_AGC_START_VTH)
	{
		RpSetAgcStartVth(RP_FALSE);
	}
	else
	{
		RpSetAgcStartVth(RP_TRUE);
	}

	if (RpCb.pib.phyAntennaDiversityStartVth != RP_RF_PIB_DFLT_ANTENNA_DIVERSITY_START_VTH)
	{
		RpSetAntennaDiversityStartVth(RP_FALSE);
	}
	else
	{
		RpSetAntennaDiversityStartVth(RP_TRUE);
	}

	if (RpCb.pib.phySfdDetectionExtend != RP_RF_PIB_DFLT_SFD_DETECTION_EXTEND)
	{
		RpSetSfdDetectionExtend();
	}

	if (RpCb.pib.phyAgcWaitGainOffset != RP_RF_PIB_DFLT_AGC_WAIT_GAIN_OFFSET)
	{
		RpSetAgcWaitGain();
	}

	if (RpCb.pib.phyAntennaSwitchEnaTiming != RP_RF_PIB_DFLT_ANTENNA_SWITCH_ENA_TIMING)
	{
		RpSetAntennaSwitchEnaTiming();
	}

	if (RpCb.pib.phyGpio0Setting != RP_RF_PIB_DFLT_GPIO0_SETTING)
	{
		RpSetGpio0Setting();
	}

	if (RpCb.pib.phyGpio3Setting != RP_RF_PIB_DFLT_GPIO3_SETTING)
	{
		RpSetGpio3Setting();
	}
#ifdef R_FREQUENCY_OFFSET_ENABLED
	if (RpCb.pib.phyFrequencyOffset != RP_RF_PIB_DFLT_FREQUENCY_OFFSET)
	{
		RpSetChannelVal(RpCb.pib.phyCurrentChannel);
	}
#endif // #ifdef R_FREQUENCY_OFFSET_ENABLED
}

/******************************************************************************
Function Name:       RpSetRegBeforeIdle
Parameters:          none
Return value:        none
Description:         set SFR for evalations for modem
******************************************************************************/
static void RpSetRegBeforeIdle( void )
{
	uint16_t tmpWord;
	uint8_t arrayChar[12];

	RpRegWrite(BBAUTORCVCNT, 0x04);
	arrayChar[0] = 0x0E;	// 0x0052
	arrayChar[1] = 0x01;	// 0x0053
	arrayChar[2] = (uint8_t)(RP_RF_TX_WARMUP_TIME);	// 0x0054
	arrayChar[3] = (uint8_t)((RP_RF_TX_WARMUP_TIME & 0xff00) >> 8);// 0x0055
	arrayChar[4] = 0x00;// 0x0056(dummy)
	arrayChar[5] = 0x50;// 0x0057
	arrayChar[6] = 0xE6;// 0x0058
	arrayChar[7] = 0x01;// 0x0059(h/w initilal)
	arrayChar[8] = (uint8_t)(RP_RF_RX_WARMUP_TIME);	// 0x005A
	arrayChar[9] = (uint8_t)((RP_RF_RX_WARMUP_TIME & 0xff00) >> 8);// 0x005B
	arrayChar[10] = arrayChar[8];// 0x005C
	arrayChar[11] = arrayChar[9];// 0x005D
	RpRegBlockWrite(0x0052 << 3, (uint8_t RP_FAR *)&(arrayChar[0]), 12);

	RpRegWrite(0x0078 << 3, 0xE6);

	arrayChar[0] = 0x2C;// 0x007A
	arrayChar[1] = 0x01;// 0x007B
	arrayChar[2] = 0x50;// 0x007C
	arrayChar[3] = 0x01;// 0x007D(h/w initial)
	arrayChar[4] = 0xE6;// 0x007E
	RpRegBlockWrite(0x007A << 3, (uint8_t RP_FAR *)&(arrayChar[0]), 5);

	RpRegWrite(0x0086 << 3, 0x03);
	RpRegWrite(0x008E << 3, 0x73);

	RpCb.reg.bbTxCon2	= 0x7B;

	arrayChar[0] = 0x26;// 0x0094
	arrayChar[1] = 0x08;// 0x0095(dummy, rewrite later)
	arrayChar[2] = 0x73;// 0x0096
	RpRegBlockWrite(0x0094 << 3, (uint8_t RP_FAR *)&(arrayChar[0]), 3);

	RpRegWrite(0x0402 << 3, 0x04);
	tmpWord = 0x505;
	RpRegBlockWrite(0x046F << 3, (uint8_t RP_FAR *)&(tmpWord), sizeof(uint16_t));
	arrayChar[0] = 0x22;// 0x0475
	arrayChar[1] = 0xE0;// 0x0476
	arrayChar[2] = 0xE0;// 0x0477
	arrayChar[3] = 0x00;// 0x0478
	RpRegBlockWrite(0x0475 << 3, (uint8_t RP_FAR *)&(arrayChar[0]), 4);
	tmpWord = 0x0160;
	RpRegBlockWrite(0x047A << 3, (uint8_t RP_FAR *)&(tmpWord), sizeof(uint16_t));
	RpRegWrite(0x0481 << 3, 0x00);
	RpRegWrite(0x0488 << 3, 0x82);
	RpRegWrite(0x04EE << 3, 0x00);
	RpRegWrite(0x04F9 << 3, 0x01);
	RpRegWrite(0x0501 << 3, 0x10);
	tmpWord = 0x0160;
	RpRegBlockWrite(0x050D << 3, (uint8_t RP_FAR *)&(tmpWord), sizeof(uint16_t));
	RpRegWrite(0x0510 << 3, 0x00);
	RpRegWrite(0x0515 << 3, 0x03);
	RpRegWrite(0x0583 << 3, 0x7F);
	RpRegWrite(0x0587 << 3, 0x7F);
	tmpWord = 0x0a0a;
	RpRegBlockWrite(0x05a5 << 3, (uint8_t RP_FAR *)&(tmpWord), sizeof(uint16_t));
	RpRegWrite(0x05AD << 3, 0x40);

	RpWrEvaReg1(0x4404);
	RpWrEvaReg2(0x1F8D);
	RpWrEvaReg2(0x6E00);
	RpWrEvaReg2(0x1088);
	RpWrEvaReg2(0x1108);
	RpWrEvaReg2(0x1207);
	RpWrEvaReg2(0x1306);
	RpWrEvaReg2(0x1405);
}

/******************************************************************************
Function Name:       RpSetFreqAddReg
Parameters:          freq
Return value:        none
Description:         set SFR for FREQ additional registers
******************************************************************************/
static void RpSetFreqAddReg( uint32_t freq, uint8_t freqBandId )
{
#define ARRAY_SIZE(ary)  (sizeof(ary)/sizeof((ary)[0]))

	uint8_t reg0x0095Val = 0x08;
	uint8_t opeMode = RpCb.pib.phyFskOpeMode;

	uint8_t i;
	RP_DDC_TABLE RP_FAR *ptable;
	uint8_t arraySize;

	// Select table, Get arryay size
	if (freqBandId == RP_PHY_FREQ_BAND_863MHz)
	{
		if ((opeMode == RP_PHY_FSK_OPEMODE_1) && (freq >= RP_FREQ_START_863MHz_OPE1))
		{
			ptable = (RP_DDC_TABLE RP_FAR *)&(RpFreq863MHzOpe1DdcTbl[0]);
			arraySize = ARRAY_SIZE(RpFreq863MHzOpe1DdcTbl);
		}
		else if ((opeMode == RP_PHY_FSK_OPEMODE_2) && (freq >= RP_FREQ_START_863MHz_OPE2))
		{
			ptable = (RP_DDC_TABLE RP_FAR *)&(RpFreq863MHzOpe2DdcTbl[0]);
			arraySize = ARRAY_SIZE(RpFreq863MHzOpe2DdcTbl);
		}
		else if ((opeMode == RP_PHY_FSK_OPEMODE_3) && (freq >= RP_FREQ_START_863MHz_OPE3))
		{
			ptable = (RP_DDC_TABLE RP_FAR *)&(RpFreq863MHzOpe3DdcTbl[0]);
			arraySize = ARRAY_SIZE(RpFreq863MHzOpe3DdcTbl);
		}
		else if ((opeMode == RP_PHY_FSK_OPEMODE_4) && (freq >= RP_FREQ_START_863MHz_OPE4))	// Use RpFreq863MHzOpe2DdcTbl
		{
			ptable = (RP_DDC_TABLE RP_FAR *)&(RpFreq863MHzOpe2DdcTbl[0]);
			arraySize = ARRAY_SIZE(RpFreq863MHzOpe2DdcTbl);
		}
		else if ((opeMode == RP_PHY_FSK_OPEMODE_5) && (freq >= RP_FREQ_START_863MHz_OPE5))	// Use RpFreq863MHzOpe2DdcTbl
		{
			ptable = (RP_DDC_TABLE RP_FAR *)&(RpFreq863MHzOpe2DdcTbl[0]);
			arraySize = ARRAY_SIZE(RpFreq863MHzOpe2DdcTbl);
		}
		else if ((opeMode == RP_PHY_FSK_OPEMODE_6) && (freq >= RP_FREQ_START_863MHz_OPE6))	// Use RpFreq863MHzOpe3DdcTbl
		{
			ptable = (RP_DDC_TABLE RP_FAR *)&(RpFreq863MHzOpe3DdcTbl[0]);
			arraySize = ARRAY_SIZE(RpFreq863MHzOpe3DdcTbl);
		}
		else if ((opeMode == RP_PHY_FSK_OPEMODE_7) && (freq >= RP_FREQ_START_863MHz_OPE7))	// Use RpFreq863MHzOpe1DdcTbl
		{
			ptable = (RP_DDC_TABLE RP_FAR *)&(RpFreq863MHzOpe1DdcTbl[0]);
			arraySize = ARRAY_SIZE(RpFreq863MHzOpe1DdcTbl);
		}
		else
		{
			arraySize = 0;
		}
	}
	else if (freqBandId == RP_PHY_FREQ_BAND_896MHz)
	{
		if ((opeMode == RP_PHY_FSK_OPEMODE_1) && (freq >= RP_FREQ_START_896MHz_OPE1))
		{
			ptable = (RP_DDC_TABLE RP_FAR *)&(RpFreq896MHzOpe1DdcTbl[0]);
			arraySize = ARRAY_SIZE(RpFreq896MHzOpe1DdcTbl);
		}
		else if ((opeMode == RP_PHY_FSK_OPEMODE_2) && (freq >= RP_FREQ_START_896MHz_OPE2))
		{
			ptable = (RP_DDC_TABLE RP_FAR *)&(RpFreq896MHzOpe2DdcTbl[0]);
			arraySize = ARRAY_SIZE(RpFreq896MHzOpe2DdcTbl);
		}
		else if ((opeMode == RP_PHY_FSK_OPEMODE_3) && (freq >= RP_FREQ_START_896MHz_OPE3))
		{
			ptable = (RP_DDC_TABLE RP_FAR *)&(RpFreq896MHzOpe3DdcTbl[0]);
			arraySize = ARRAY_SIZE(RpFreq896MHzOpe3DdcTbl);
		}
		else
		{
			arraySize = 0;
		}
	}
	else if (freqBandId == RP_PHY_FREQ_BAND_901MHz)
	{
		if ((opeMode == RP_PHY_FSK_OPEMODE_1) && (freq >= RP_FREQ_START_901MHz_OPE1))
		{
			ptable = (RP_DDC_TABLE RP_FAR *)&(RpFreq901MHzOpe1DdcTbl[0]);
			arraySize = ARRAY_SIZE(RpFreq901MHzOpe1DdcTbl);
		}
		else if ((opeMode == RP_PHY_FSK_OPEMODE_2) && (freq >= RP_FREQ_START_901MHz_OPE2))
		{
			ptable = (RP_DDC_TABLE RP_FAR *)&(RpFreq901MHzOpe2DdcTbl[0]);
			arraySize = ARRAY_SIZE(RpFreq901MHzOpe2DdcTbl);
		}
		else if ((opeMode == RP_PHY_FSK_OPEMODE_3) && (freq >= RP_FREQ_START_901MHz_OPE3))
		{
			ptable = (RP_DDC_TABLE RP_FAR *)&(RpFreq901MHzOpe3DdcTbl[0]);
			arraySize = ARRAY_SIZE(RpFreq901MHzOpe3DdcTbl);
		}
		else
		{
			arraySize = 0;
		}
	}
	else if (freqBandId == RP_PHY_FREQ_BAND_915MHz)
	{
		if ((opeMode == RP_PHY_FSK_OPEMODE_1) && (freq >= RP_FREQ_START_915MHz_OPE1))
		{
			ptable = (RP_DDC_TABLE RP_FAR *)&(RpFreq915MHzOpe1DdcTbl[0]);
			arraySize = ARRAY_SIZE(RpFreq915MHzOpe1DdcTbl);
		}
		else if ((opeMode == RP_PHY_FSK_OPEMODE_2) && (freq >= RP_FREQ_START_915MHz_OPE2))
		{
			ptable = (RP_DDC_TABLE RP_FAR *)&(RpFreq915MHzOpe2DdcTbl[0]);
			arraySize = ARRAY_SIZE(RpFreq915MHzOpe2DdcTbl);
		}
		else if ((opeMode == RP_PHY_FSK_OPEMODE_3) && (freq >= RP_FREQ_START_915MHz_OPE3))
		{
			ptable = (RP_DDC_TABLE RP_FAR *)&(RpFreq915MHzOpe3DdcTbl[0]);
			arraySize = ARRAY_SIZE(RpFreq915MHzOpe3DdcTbl);
		}
		else if ((opeMode == RP_PHY_FSK_OPEMODE_4) && (freq >= RP_FREQ_START_915MHz_OPE4))	// Use RpFreq915MHzOpe1DdcTbl
		{
			ptable = (RP_DDC_TABLE RP_FAR *)&(RpFreq915MHzOpe1DdcTbl[0]);
			arraySize = ARRAY_SIZE(RpFreq915MHzOpe1DdcTbl);
		}
		else
		{
			arraySize = 0;
		}
	}
	else if (freqBandId == RP_PHY_FREQ_BAND_917MHz)
	{
		if ((opeMode == RP_PHY_FSK_OPEMODE_1) && (freq >= RP_FREQ_START_917MHz_OPE1))
		{
			ptable = (RP_DDC_TABLE RP_FAR *)&(RpFreq917MHzOpe1DdcTbl[0]);
			arraySize = ARRAY_SIZE(RpFreq917MHzOpe1DdcTbl);
		}
		else if ((opeMode == RP_PHY_FSK_OPEMODE_2) && (freq >= RP_FREQ_START_917MHz_OPE2))
		{
			ptable = (RP_DDC_TABLE RP_FAR *)&(RpFreq917MHzOpe2DdcTbl[0]);
			arraySize = ARRAY_SIZE(RpFreq917MHzOpe2DdcTbl);
		}
		else if ((opeMode == RP_PHY_FSK_OPEMODE_3) && (freq >= RP_FREQ_START_917MHz_OPE3))
		{
			ptable = (RP_DDC_TABLE RP_FAR *)&(RpFreq917MHzOpe3DdcTbl[0]);
			arraySize = ARRAY_SIZE(RpFreq917MHzOpe3DdcTbl);
		}
		else if ((opeMode == RP_PHY_FSK_OPEMODE_4) && (freq >= RP_FREQ_START_917MHz_OPE4))	// Use RpFreq917MHzOpe1DdcTbl
		{
			ptable = (RP_DDC_TABLE RP_FAR *)&(RpFreq917MHzOpe1DdcTbl[0]);
			arraySize = ARRAY_SIZE(RpFreq917MHzOpe1DdcTbl);
		}
		else
		{
			arraySize = 0;
		}
	}
	else if (freqBandId == RP_PHY_FREQ_BAND_920MHz)
	{
		if ((opeMode == RP_PHY_FSK_OPEMODE_1) && (freq >= RP_FREQ_START_920MHz_OPE1))
		{
			ptable = (RP_DDC_TABLE RP_FAR *)&(RpFreq920MHzOpe1DdcTbl[0]);
			arraySize = ARRAY_SIZE(RpFreq920MHzOpe1DdcTbl);
		}
		else if ((opeMode == RP_PHY_FSK_OPEMODE_2) && (freq >= RP_FREQ_START_920MHz_OPE2))
		{
			ptable = (RP_DDC_TABLE RP_FAR *)&(RpFreq920MHzOpe2DdcTbl[0]);
			arraySize = ARRAY_SIZE(RpFreq920MHzOpe2DdcTbl);
		}
		else if ((opeMode == RP_PHY_FSK_OPEMODE_3) && (freq >= RP_FREQ_START_920MHz_OPE3))
		{
			ptable = (RP_DDC_TABLE RP_FAR *)&(RpFreq920MHzOpe3DdcTbl[0]);
			arraySize = ARRAY_SIZE(RpFreq920MHzOpe3DdcTbl);
		}
		else if ((opeMode == RP_PHY_FSK_OPEMODE_4) && (freq >= RP_FREQ_START_920MHz_OPE4))
		{
			ptable = (RP_DDC_TABLE RP_FAR *)&(RpFreq920MHzOpe4DdcTbl[0]);
			arraySize = ARRAY_SIZE(RpFreq920MHzOpe4DdcTbl);
		}
		else if ((opeMode == RP_PHY_FSK_OPEMODE_5) && (freq >= RP_FREQ_START_920MHz_OPE5))	// Use RpFreq920MHzOpe1DdcTbl
		{
			ptable = (RP_DDC_TABLE RP_FAR *)&(RpFreq920MHzOpe1DdcTbl[0]);
			arraySize = ARRAY_SIZE(RpFreq920MHzOpe1DdcTbl);
		}
		else if ((opeMode == RP_PHY_FSK_OPEMODE_6) && (freq >= RP_FREQ_START_920MHz_OPE6))	// Use RpFreq920MHzOpe1DdcTbl
		{
			ptable = (RP_DDC_TABLE RP_FAR *)&(RpFreq920MHzOpe1DdcTbl[0]);
			arraySize = ARRAY_SIZE(RpFreq920MHzOpe1DdcTbl);
		}
		else
		{
			arraySize = 0;
		}
	}
	else if (freqBandId == RP_PHY_FREQ_BAND_920MHz_Others)
	{
		if ((opeMode == RP_PHY_FSK_OPEMODE_1) && (freq >= RP_FREQ_START_920MHz_Others_OPE1))
		{
			ptable = (RP_DDC_TABLE RP_FAR *)&(RpFreq920MHzOthersOpe1DdcTbl[0]);
			arraySize = ARRAY_SIZE(RpFreq920MHzOthersOpe1DdcTbl);
		}
		else if ((opeMode == RP_PHY_FSK_OPEMODE_2) && (freq >= RP_FREQ_START_920MHz_Others_OPE2))
		{
			ptable = (RP_DDC_TABLE RP_FAR *)&(RpFreq920MHzOthersOpe2DdcTbl[0]);
			arraySize = ARRAY_SIZE(RpFreq920MHzOthersOpe2DdcTbl);
		}
		else if ((opeMode == RP_PHY_FSK_OPEMODE_3) && (freq >= RP_FREQ_START_920MHz_Others_OPE3))
		{
			ptable = (RP_DDC_TABLE RP_FAR *)&(RpFreq920MHzOthersOpe3DdcTbl[0]);
			arraySize = ARRAY_SIZE(RpFreq920MHzOthersOpe3DdcTbl);
		}
		else
		{
			arraySize = 0;
		}
	}
	else if (freqBandId == RP_PHY_FREQ_BAND_870MHz)
	{
		if ((opeMode == RP_PHY_FSK_OPEMODE_1) && (freq >= RP_FREQ_START_870MHz_OPE1))
		{
			ptable = (RP_DDC_TABLE RP_FAR *)&(RpFreq870MHzOpe1DdcTbl[0]);
			arraySize = ARRAY_SIZE(RpFreq870MHzOpe1DdcTbl);
		}
		else if ((opeMode == RP_PHY_FSK_OPEMODE_2) && (freq >= RP_FREQ_START_870MHz_OPE2))	// Use RpFreq870MHzOpe1DdcTbl
		{
			ptable = (RP_DDC_TABLE RP_FAR *)&(RpFreq870MHzOpe1DdcTbl[0]);
			arraySize = ARRAY_SIZE(RpFreq870MHzOpe1DdcTbl);
		}
		else if ((opeMode == RP_PHY_FSK_OPEMODE_3) && (freq >= RP_FREQ_START_870MHz_OPE3))	// Use RpFreq870MHzOpe1DdcTbl
		{
			ptable = (RP_DDC_TABLE RP_FAR *)&(RpFreq870MHzOpe1DdcTbl[0]);
			arraySize = ARRAY_SIZE(RpFreq870MHzOpe1DdcTbl);
		}
		else
		{
			arraySize = 0;
		}
	}
	else if (freqBandId == RP_PHY_FREQ_BAND_902MHz)
	{
		if ((opeMode == RP_PHY_FSK_OPEMODE_1) && (freq >= RP_FREQ_START_902MHz_OPE1))		// Use RpFreq915MHzOpe1DdcTbl
		{
			ptable = (RP_DDC_TABLE RP_FAR *)&(RpFreq915MHzOpe1DdcTbl[0]);
			arraySize = ARRAY_SIZE(RpFreq915MHzOpe1DdcTbl);
		}
		else
		{
			arraySize = 0;
		}
	}
	else if (freqBandId == RP_PHY_FREQ_BAND_921MHz)
	{
		if ((opeMode == RP_PHY_FSK_OPEMODE_1) && (freq >= RP_FREQ_START_921MHz_OPE1))		// Use RpFreq920MHzOthersOpe2DdcTbl
		{
			ptable = (RP_DDC_TABLE RP_FAR *)&(RpFreq920MHzOthersOpe2DdcTbl[0]);
			arraySize = ARRAY_SIZE(RpFreq920MHzOthersOpe2DdcTbl);
		}
		else if ((opeMode == RP_PHY_FSK_OPEMODE_2) && (freq >= RP_FREQ_START_921MHz_OPE2))
		{
			ptable = (RP_DDC_TABLE RP_FAR *)&(RpFreq921MHzOpe2DdcTbl[0]);
			arraySize = ARRAY_SIZE(RpFreq921MHzOpe2DdcTbl);
		}
		else if ((opeMode == RP_PHY_FSK_OPEMODE_3) && (freq >= RP_FREQ_START_921MHz_OPE3))	// Use RpFreq920MHzOthersOpe2DdcTbl
		{
			ptable = (RP_DDC_TABLE RP_FAR *)&(RpFreq920MHzOthersOpe2DdcTbl[0]);
			arraySize = ARRAY_SIZE(RpFreq920MHzOthersOpe2DdcTbl);
		}
		else if ((opeMode == RP_PHY_FSK_OPEMODE_4) && (freq >= RP_FREQ_START_921MHz_OPE4))	// Use RpFreq920MHzOthersOpe2DdcTbl
		{
			ptable = (RP_DDC_TABLE RP_FAR *)&(RpFreq920MHzOthersOpe2DdcTbl[0]);
			arraySize = ARRAY_SIZE(RpFreq920MHzOthersOpe2DdcTbl);
		}
		else
		{
			arraySize = 0;
		}
	}
	else
	{
		arraySize = 0;
	}

	// Get setting value from selected table
	for (i = 0; i < arraySize; i++)
	{
		if (freq <= ptable->freq)
		{
			reg0x0095Val = ptable->reg;
			break;
		}
		ptable++;
	}
	RpRegWrite(0x0095 << 3, reg0x0095Val);

#undef ARRAY_SIZE
}

/******************************************************************************
Function Name:       RpSetRfInt
Parameters:          enable: 0:disable interrupt
                             1:enable interrupt nothing to do
Return value:        none
Description:         control of using RF interrput.
******************************************************************************/
#ifndef RP_ST_ENV
static void RpSetRfInt( uint8_t enable )
#else
void RpSetRfInt( uint8_t enable )
#endif // #ifndef RP_ST_ENV
{
	RpReadIrq();	//dummy read for clearing all interrupts
	if (enable)
	{
		// set RF IC interrupt
		RpCb.reg.bbIntEn[0] = 0x00;
		RpCb.reg.bbIntEn[1] = (RCVFININTEN | RCV0INTEN | RCV1INTEN | RCVSTINTEN | ROVRINTEN | MODESWINTEN | LVLFILINTEN);
		RpCb.reg.bbIntEn[2] = 0x00;
	}
	else
	{
		// disable RF IC interrupt
		RpCb.reg.bbIntEn[0] = 0x00;
		RpCb.reg.bbIntEn[1] = 0x00;
		RpCb.reg.bbIntEn[2] = 0x00;
	}
	RpRegBlockWrite(BBINTEN0, (uint8_t RP_FAR *)RpCb.reg.bbIntEn, sizeof(RpCb.reg.bbIntEn));
}

/******************************************************************************
Function Name:       RpWaitaLittle
Parameters:          none
Return value:        none
Description:         make a little time
******************************************************************************/
#ifdef __CCRL__
	void RpWaitaLittle( void )
#else
	static void RpWaitaLittle( void )
#endif /* __CCRL__ */
{
}

/******************************************************************************
Function Name:       RpWait4us
Parameters:          none
Return value:        none
Description:         make about 4uS
******************************************************************************/
#ifdef __CCRL__
#pragma inline_asm RpWait4us_asm
void RpWait4us_asm( void )
{
#if RP_CPU_CLK == 32
	call !!_RpWaitaLittle
	call !!_RpWaitaLittle
	call !!_RpWaitaLittle
	call !!_RpWaitaLittle
	call !!_RpWaitaLittle
	call !!_RpWaitaLittle
	call !!_RpWaitaLittle
	call !!_RpWaitaLittle
	call !!_RpWaitaLittle
	call !!_RpWaitaLittle
	call !!_RpWaitaLittle
	call !!_RpWaitaLittle
	call !!_RpWaitaLittle
	nop
	nop
#else // if RP_CPU_CLK == 8
	call !!_RpWaitaLittle
	call !!_RpWaitaLittle
	nop
	nop
	nop
	nop
	nop
#endif
}
#endif /* __CCRL__ */

#if !defined(RP_WISUN_FAN_STACK) && !defined(RP_ST_ENV)
static void RpWait4us( void )
#else
void RpWait4us( void )
#endif /* !defined(RP_WISUN_FAN_STACK) && !defined(RP_ST_ENV) */
{
#if defined(__RL78__)
#ifdef __CCRL__
	RpWait4us_asm();

#else /* __CCRL__ */
#if RP_CPU_CLK == 32
	__asm("call !!_RpWaitaLittle");
	__asm("call !!_RpWaitaLittle");
	__asm("call !!_RpWaitaLittle");
	__asm("call !!_RpWaitaLittle");
	__asm("call !!_RpWaitaLittle");
	__asm("call !!_RpWaitaLittle");
	__asm("call !!_RpWaitaLittle");
	__asm("call !!_RpWaitaLittle");
	__asm("call !!_RpWaitaLittle");
	__asm("call !!_RpWaitaLittle");
	__asm("call !!_RpWaitaLittle");
	__asm("call !!_RpWaitaLittle");
	__asm("call !!_RpWaitaLittle");
	__asm("nop");
	__asm("nop");
#else // if RP_CPU_CLK == 8
	__asm("call !!_RpWaitaLittle");
	__asm("call !!_RpWaitaLittle");
	__asm("nop");
	__asm("nop");
	__asm("nop");
	__asm("nop");
	__asm("nop");
#endif
#endif /* __CCRL__ */

#elif defined(__RX)
#if RP_CPU_CLK == 80
	uint16_t i;

	for (i = 0; i < 12; ++i)
	{
		RpWaitaLittle();
		RpWaitaLittle();
		RpWaitaLittle();
		nop();
		nop();
		nop();
	}
#endif
#endif
}

/***************************************************************************************************************
 * function name  : RpRelRxBuf
 * description    : Receive Buffer Release
 * parameters     : pBuf...Starting address of the receive buffer area to be released
 * return value   : RP_SUCCESS, RP_INVALID_PARAMETER
 **************************************************************************************************************/
int16_t RpRelRxBuf( uint8_t *pBuf )
{
	int16_t	i;
	int16_t	rtn = RP_INVALID_PARAMETER;

	#if defined(__RX)
	uint32_t bkupPsw;
	#elif defined(__CCRL__)
	uint8_t  bkupPsw;
	#elif defined(__arm)
	uint8_t bkupPsw[RP_INT_BKUP_BUF_SIZE];
	#endif

	/* Disable interrupt */
	#if defined(__RX) || defined(__CCRL__) || defined(__arm)
	RP_PHY_DI(bkupPsw);
	#else
	RP_PHY_DI();
	#endif

	/* API function execution Log */
	RpLog_Event( RP_LOG_API_RELBUF, RP_NULL );

	for (i = 0; i < RP_RX_BUF_NUM; i++)
	{
		if ((RpRxBuf[i].status == RP_TRUE) && (pBuf == RpRxBuf[i].buf))
		{
			RpRxBuf[i].status = RP_FALSE;

			if (RpCb.rx.waitRelRxBufWhenAutoRx == RP_TRUE)
			{
				RpCb.rx.waitRelRxBufWhenAutoRx = RP_FALSE;

				if (RpCb.status & (RP_PHY_STAT_TRX_TO_RX_AUTO | RP_PHY_STAT_RXON_BACKOFF))
				{
					RpCb.rx.pTopRxBuf = RpRxBuf[i].buf;
					RpRxBuf[i].status = RP_TRUE;
				}
			}
			rtn = RP_SUCCESS;
		}
	}

	/* API function execution Log */
	RpLog_Event( RP_LOG_API_RELBUF | RP_LOG_API_RET, (uint8_t)rtn );

	/* Enable interrupt */
	#if defined(__RX) || defined(__CCRL__) || defined(__arm)
	RP_PHY_EI(bkupPsw);
	#else
	RP_PHY_EI();
	#endif

	return (rtn);
}

/******************************************************************************
Function Name:       RpGetRxBuf
Parameters:          none
Return value:        pointer of got buffer
Description:         Get Rx Buffer
******************************************************************************/
uint8_t *RpGetRxBuf( void )
{
	int16_t	i;

	for (i = 0; i < RP_RX_BUF_NUM; i++)
	{
		if (RpRxBuf[i].status == RP_FALSE)
		{
			RpRxBuf[i].status = RP_TRUE;
			RpCb.rx.waitRelRxBufWhenAutoRx = RP_FALSE;

			return (RpRxBuf[i].buf);
		}
	}

	return (RP_NULL);
}

/******************************************************************************
Function Name:       RpInitRxBuf
Parameters:          none
Return value:        none
Description:         Initialize Rx Buffer
******************************************************************************/
static void RpInitRxBuf( void )
{
	uint8_t cnt;

	for (cnt = 0; cnt < RP_RX_BUF_NUM; ++cnt)
	{
		RpRxBuf[cnt].status = RP_FALSE;
	}
}

/******************************************************************************
Function Name:       RpExtChkOptErrDataReq
Parameters:          options:options of RpPdDataReq
Return value:        0:options is available
				 1:options is not available
Description:         Checks if the options is available or not.
******************************************************************************/
static int16_t RpExtChkOptErrDataReq( uint8_t options )
{
	uint8_t i;

	for (i = 0; i < RP_PHYEXT_DATAREQ_NUM; ++i)
	{
		if ((options & RpOptionChkDataReq[i][0]) == RpOptionChkDataReq[i][1])
		{
			break;
		}
	}

	if (i < RP_PHYEXT_DATAREQ_NUM)
	{
		return (RP_TRUE);
	}
	else
	{
		return (RP_FALSE);
	}
}

/******************************************************************************
Function Name:       RpExtChkOptErrRxStateReq
Parameters:          options:options of RpPlmeTrxStateReq,RP_RX_ON
Return value:        0:options is available
					 1:options is not available
Description:         Checks if the options is available or not.
******************************************************************************/
static int16_t RpExtChkOptErrRxStateReq( uint8_t options )
{
	uint8_t i;

	for (i = 0; i < RP_PHYEXT_RXSTATREQ_NUM; ++i)
	{
		if ((options & RpOptionChkRxStateReq[i][0]) == RpOptionChkRxStateReq[i][1])
		{
			break;
		}
	}

	if (i < RP_PHYEXT_RXSTATREQ_NUM)
	{
		return (RP_TRUE);
	}
	else
	{
		return (RP_FALSE);
	}
}

/******************************************************************************
Function Name:       RpExtChkIdLenGetReq
Parameters:          id:id of RpGetPib
					 vallen:vallen of RpGetPib
Return value:        0:id and vallen are available
					 1:id is not available
					 2:vallen is not availale
Description:         Checks if the id and vallen are available or not.
******************************************************************************/
int16_t RpExtChkIdLenGetReq( uint8_t id, uint8_t valLen, void *pVal )
{
	uint8_t i;

	if (pVal == RP_NULL)
	{
		return (RP_INVALID_PARAMETER);
	}

	for (i = 0; i < RP_PHYEXT_GETID_NUM; ++i)
	{
		if (id == RpIdLenChkGetReq[i][0])
		{
			if (valLen != RpIdLenChkGetReq[i][1])
			{
				i = RP_PHYEXT_GETID_NUM + 1;
			}
			break;
		}
	}

	if	(i < RP_PHYEXT_GETID_NUM)
	{
		return (RP_SUCCESS);
	}
	else if (i == RP_PHYEXT_GETID_NUM)
	{
		return (RP_UNSUPPORTED_ATTRIBUTE);
	}
	else
	{
		return (RP_INVALID_PARAMETER);
	}
}

/******************************************************************************
Function Name:       RpExtChkIdLenSetReq
Parameters:          id:id of RpSetPib
					 valLen:vallen of RpSetPib
					 pVal:val of RpSetPib
Return value:        0:id and vallen and val are available
					 1:id is not available
					 2:vallen or val is not availale
Description:         Checks if the id and vallen and val are available or not.
******************************************************************************/
int16_t RpExtChkIdLenSetReq( uint8_t id, uint8_t valLen, void RP_FAR *pVal )
{
	uint8_t i;

	if (pVal == RP_NULL)
	{
		return (RP_INVALID_PARAMETER);
	}

	for (i = 0; i < RP_PHYEXT_SETID_NUM; ++i)
	{
		if (id == RpIdLenChkSetReq[i][0])
		{
			if (valLen != RpIdLenChkSetReq[i][1])
			{
				i = RP_PHYEXT_SETID_NUM + 1;
			}
			break;
		}
	}

	if (i < RP_PHYEXT_SETID_NUM)
	{
		return (RP_SUCCESS);
	}
	else if (i == RP_PHYEXT_SETID_NUM)
	{
		return (RP_UNSUPPORTED_ATTRIBUTE);
	}
	else
	{
		return (RP_INVALID_PARAMETER);
	}
}

/******************************************************************************
Function Name:       RpMemcpy
					 RpMemcpy(void *s1, const void *s2, uint16_t n);
Return value:        Last dest address pointer
Description:         Memcpy function
******************************************************************************/
void *RpMemcpy( void *s1, const void RP_FAR *s2, uint16_t n )
{
	memcpy( s1, s2, (size_t)n );

	return( s1 );
}

/******************************************************************************
Function Name:      RpMemset
Parameters:         RpMemset(void *s, int c, uint16_t n);
Return value:       Last dest address pointer
Description:        Memset function
******************************************************************************/
void *RpMemset( void *s, int16_t c, uint16_t n )
{
#if defined(__RX)
	memset( s, (int32_t)c, (size_t)n );
#else
	memset( s, c, (size_t)n );
#endif

	return( s );
}

/***************************************************************************************************************
 * function name  : RpTxLimitTimerForward
 * description    : Transmission total limit timer update
 * parameters     : calledByXmsec...Time during a low power consumption mode. [unit:ms]
 * return value   : none
 **************************************************************************************************************/
void RpTxLimitTimerForward( uint32_t calledByXmsec )
{
	uint8_t sample = 0;
	uint32_t calledByXsec;

	#if defined(__RX)
	uint32_t bkupPsw;
	#elif defined(__CCRL__)
	uint8_t  bkupPsw;
	#elif defined(__arm)
	uint8_t bkupPsw[RP_INT_BKUP_BUF_SIZE];
	#endif

	if (calledByXmsec)
	{
		/* Disable interrupt */
		#if defined(__RX) || defined(__CCRL__) || defined(__arm)
		RP_PHY_DI(bkupPsw);
		#else
		RP_PHY_DI();
		#endif

		// Check input range
		if (calledByXmsec > (RpCb.pib.phyRmodeTcumSmpPeriod * RP_MAX_NUM_TX_LMT_BUF * 1000))
		{
			RpCb.txLmtTimer.msecCnt += RpCb.pib.phyRmodeTcumSmpPeriod * RP_MAX_NUM_TX_LMT_BUF * 1000;
			RpCb.txLmtTimer.msecCnt += calledByXmsec % (RpCb.pib.phyRmodeTcumSmpPeriod * 1000);
		}
		else
		{
			RpCb.txLmtTimer.msecCnt += calledByXmsec;
		}

#if defined(__RL78__)
		if (RpCb.txLmtTimer.lackTime)
		{
			RpCb.txLmtTimer.msecCnt += RpCb.txLmtTimer.lackTime / 1000;
			RpCb.txLmtTimer.lackTime = 0;
		}
#endif
		calledByXsec = RpCb.txLmtTimer.msecCnt / 1000;
		RpCb.txLmtTimer.msecCnt = RpCb.txLmtTimer.msecCnt % 1000;

		//  this counter used for arib-std send interval calculation need or not(in rf timer overflowed).
		RpCb.tx.regulatoryModeElapsedTimeS += calledByXsec;
		if((RpCb.tx.regulatoryModeElapsedTimeS - RpCb.tx.sentElapsedTimeS) >= RP_PHY_INTERVAL_CLEAR_MARGIN)
		{
			RpCb.tx.needTxWaitRegulatoryMode = RP_FALSE;
		}

		RpCb.txLmtTimer.secCnt += calledByXsec;
		if (RpCb.txLmtTimer.secCnt >= RpCb.pib.phyRmodeTcumSmpPeriod)
		{
			sample = (uint8_t)(RpCb.txLmtTimer.secCnt / RpCb.pib.phyRmodeTcumSmpPeriod);
			RpCb.txLmtTimer.secCnt = (RpCb.txLmtTimer.secCnt % RpCb.pib.phyRmodeTcumSmpPeriod);
		}

		RpProgressSamplingTxTime(sample);

		/* Enable interrupt */
		#if defined(__RX) || defined(__CCRL__) || defined(__arm)
		RP_PHY_EI(bkupPsw);
		#else
		RP_PHY_EI();
		#endif
	}
}

/******************************************************************************
Function Name:       RpProgressSamplingTxTime
Parameters:          progressSample: Number to advance sampling position.
Return value:        none
Description:         Advance the sampling position for the measurement of the total Tx time at each sampling cycle and
                     remove the Tx time past the observation period from the total Tx time.
******************************************************************************/
static void RpProgressSamplingTxTime( uint8_t progressSample )
{
	while (progressSample--)
	{
		RpCb.txLmtTimer.samplingCnt++;
		if (RpCb.txLmtTimer.samplingCnt >= RP_MAX_NUM_TX_LMT_BUF)
		{
			RpCb.txLmtTimer.samplingCnt = 0;
		}

		if (RpCb.pib.phyRmodeTcum > RpTxTime[RpCb.txLmtTimer.samplingCnt])
		{
			RpCb.pib.phyRmodeTcum -= RpTxTime[RpCb.txLmtTimer.samplingCnt];
		}
		else
		{
			RpCb.pib.phyRmodeTcum = 0;
		}

		RpTxTime[RpCb.txLmtTimer.samplingCnt] = 0;
	}
}

/******************************************************************************
Function Name:       RpCalcTotalTxTime
Parameters:          none
Return value:        none
Description:         Calcurate total Tx time function.
******************************************************************************/
static void RpCalcTotalTxTime( void )
{
	uint8_t	cnt = 0;

	RpCb.pib.phyRmodeTcum = 0;
	do {
		RpCb.pib.phyRmodeTcum += RpTxTime[cnt];
		cnt++;
	} while ( cnt != RP_MAX_NUM_TX_LMT_BUF );
}

/******************************************************************************
Function Name:       RpCheckLongerThanTotalTxTime
Parameters:          txLength: frame length
					 txOpt: ACK req?
                     useRxFcsLength : 0:FCS length is RpCb.pib.phyFcsLengt.
                                    : 1:FCS length is RpCb.rx.fcsLength.
Return value:        RP_TRUE: Reach Tx Limit
					 RP_FALSE:Does not reach Tx Limit
Description:         Calcurate total Tx time function.
******************************************************************************/
uint8_t RpCheckLongerThanTotalTxTime( uint16_t txLength, uint8_t txOpt, uint8_t useRxFcsLength )
{
	uint8_t rtn = RP_FALSE;
	uint8_t maxretries = RpCb.pib.macMaxFrameRetries;
	uint32_t txThisTime;

	txThisTime = RpBytesToTime(txLength, useRxFcsLength);
	if (txOpt & RP_TX_ACK)
	{
		txThisTime += (uint32_t)((uint32_t)txThisTime * (uint32_t)maxretries);
	}
	txThisTime /= 1000;
	txThisTime += 1;
	if ((txThisTime + RpCb.pib.phyRmodeTcum) > RpCb.txLmtTimer.txLimitTime)
	{
		rtn = RP_TRUE;
	}

	return (rtn);
}

/******************************************************************************
Function Name:       RpUpdateTxTime
Parameters:          trntimes
                     isAck : 0:TX frame is not Ack, 1: TX frame is Ack
                     useRxFcsLength : 0:FCS length is RpCb.pib.phyFcsLengt.
                                    : 1:FCS length is RpCb.rx.fcsLength.
Return value:        none
Description:         Add current tx time to total Tx time function.
******************************************************************************/
void RpUpdateTxTime( uint8_t trndTimes, uint8_t isAck, uint8_t useRxFcsLength )
{
	uint16_t txLength = RpCb.tx.len;
	uint32_t txThisTime;

	#if defined(__RX)
	uint32_t bkupPsw;
	#elif defined(__CCRL__)
	uint8_t  bkupPsw;
	#elif defined(__arm)
	uint8_t bkupPsw[RP_INT_BKUP_BUF_SIZE];
	#endif

	if (isAck == RP_TRUE)
	{
		txLength = RpCb.tx.ackLenForRegulatoryMode;
	}

	if ((RpCb.pib.phyRegulatoryMode == RP_RMODE_ENABLE) ||
		((RpCb.pib.phyRegulatoryMode == RP_RMODE_ENABLE_ARIB) && (RpCb.pib.phyFreqBandId == RP_PHY_FREQ_BAND_920MHz) &&
		 (RpCb.pib.phyCurrentChannel >= RP_PHY_MIN_CHANNEL_VALID_ARIB_MODE)))
	{
		txThisTime = RpBytesToTime(txLength, useRxFcsLength);
		txThisTime /= 1000;
		txThisTime += 1;
		txThisTime *= (uint32_t)trndTimes;

		if (isAck == RP_FALSE)
		{
			/* Disable interrupt */
			#if defined(__RX) || defined(__CCRL__) || defined(__arm)
			RP_PHY_DI(bkupPsw);
			#else
			RP_PHY_DI();
			#endif
		}

		if ((txThisTime + RpCb.pib.phyRmodeTcum) <= RpCb.txLmtTimer.txLimitTime)
		{
			RpTxTime[RpCb.txLmtTimer.samplingCnt] += txThisTime;
			RpCb.pib.phyRmodeTcum += txThisTime;
		}

		if (isAck == RP_FALSE)
		{
			/* Enable interrupt */
			#if defined(__RX) || defined(__CCRL__) || defined(__arm)
			RP_PHY_EI(bkupPsw);
			#else
			RP_PHY_EI();
			#endif
		}
	}
}

/******************************************************************************
Function Name:       RpBytesToTime
Parameters:          psduLength:
                     useRxFcsLength : 0:FCS length is RpCb.pib.phyFcsLengt.
                                    : 1:FCS length is RpCb.rx.fcsLength.
Return value:        result time
Description:         translate Bytes to Time function.
******************************************************************************/
static uint32_t RpBytesToTime( uint16_t psduLength, uint8_t useRxFcsLength )
{
	uint16_t sduLen;
	uint16_t totalTxLength = RpCalcTotalBytes(psduLength, &sduLen, useRxFcsLength);

	return (RpCsmaBytesToTime(totalTxLength));
}

/******************************************************************************
Function Name:       RpSetMacTxLimitMode
Parameters:          none
Return value:        none
Description:         RP_PHY_REGULATORY_MODE function.
******************************************************************************/
void RpSetMacTxLimitMode( void )
{
	uint8_t mode;

	#if defined(__RX)
	uint32_t bkupPsw;
	#elif defined(__CCRL__)
	uint8_t  bkupPsw;
	#elif defined(__arm)
	uint8_t bkupPsw[RP_INT_BKUP_BUF_SIZE];
	#endif

	/* Disable interrupt */
	#if defined(__RX) || defined(__CCRL__) || defined(__arm)
	RP_PHY_DI(bkupPsw);
	#else
	RP_PHY_DI();
	#endif

	mode = RpCb.pib.phyRegulatoryMode;
	if ((RpCb.pib.phyRegulatoryMode == RP_RMODE_ENABLE_ARIB) && (RpCb.pib.phyFreqBandId == RP_PHY_FREQ_BAND_920MHz))
	{
		RpSetCcaDurationTimeUpdate(RpCb.pib.phyCurrentChannel);
	}

	if (mode == RP_RMODE_DISABLE)
	{
		RpCb.txLmtTimer.txLimitTime = 0;
		RpMemset((uint8_t *)RpTxTime, 0x00, (RP_MAX_NUM_TX_LMT_BUF * 4));
		RpLimitTimerControl(RP_FALSE);
		RpCb.pib.phyRmodeTcum = 0;
	}
	else
	{
		if (mode == RP_RMODE_ENABLE)
		{
			RpCb.txLmtTimer.txLimitTime = RpCb.pib.phyRmodeTcumLimit;
		}
		else if (mode == RP_RMODE_ENABLE_ARIB)
		{
			RpCb.txLmtTimer.txLimitTime = (uint32_t)(RpCb.pib.phyRmodeTcumSmpPeriod) * (RP_MAX_NUM_TX_LMT_BUF - 1) * 100;
		}
		RpLimitTimerControl(RP_TRUE);
		RpCalcTotalTxTime();
	}

	if (RpCb.pib.phyAntennaDiversityRxEna == RP_TRUE)
	{
		if ((RpCb.pib.phyRegulatoryMode == RP_RMODE_ENABLE_ARIB) && (RpCb.pib.phyFreqBandId == RP_PHY_FREQ_BAND_920MHz))
		{
			RpRegBlockWrite(BBANTTIMOUT, (uint8_t RP_FAR *)&(RpFreqBandTbl[RpCb.freqIdIndex][RP_ANTDVT_TIMEOUT_ARIB]), 2);
			RpRegWrite((0x0472 << 3), 0x04);
		}
		else
		{
			RpRegBlockWrite(BBANTTIMOUT, (uint8_t RP_FAR *)&(RpFreqBandTbl[RpCb.freqIdIndex][RP_ANTDVT_TIMEOUT]), 2);
			RpSetPreamble4ByteRxMode();
		}
	}

	/* Enable interrupt */
	#if defined(__RX) || defined(__CCRL__) || defined(__arm)
	RP_PHY_EI(bkupPsw);
	#else
	RP_PHY_EI();
	#endif
}

#ifdef RP_ENABLE_TxTotalTimeLimitControl
/******************************************************************************
Function Name:       RpTxTotalTimeLimitControl
Parameters:          mode
Return value:        time
Description:         Total Time Special Control function.
******************************************************************************/
uint32_t RpTxTotalTimeLimitControl( uint8_t mode )
{
	uint32_t  rtn = 0;
	uint8_t	samplingCnter, iniSamplingCnt;
	uint32_t realTime, difTime, willTxTime, difTime2;

	#if defined(__RX)
	uint32_t bkupPsw;
	#elif defined(__CCRL__)
	uint8_t  bkupPsw;
	#elif defined(__arm)
	uint8_t bkupPsw[RP_INT_BKUP_BUF_SIZE];
	#endif

	if (RpCb.pib.phyRegulatoryMode != RP_RMODE_DISABLE)
	{
		/* Disable interrupt */
		#if defined(__RX) || defined(__CCRL__) || defined(__arm)
		RP_PHY_DI(bkupPsw);
		#else
		RP_PHY_DI();
		#endif

		switch (mode)
		{
			case RP_PHY_TTLMT_INIT: // Initialization (Zero clears transmission total time)
				RpMemset((uint8_t *)RpTxTime, 0x00, (RP_MAX_NUM_TX_LMT_BUF * 4));
				RpCb.pib.phyRmodeTcum = 0;
				break;
			case RP_PHY_TTLMT_REF_REST_TXABLE_TIME: // The remaining transmission possible time (msec)
				if (RpCb.txLmtTimer.txLimitTime > RpCb.pib.phyRmodeTcum)
				{
					rtn = (RpCb.txLmtTimer.txLimitTime - RpCb.pib.phyRmodeTcum);
				}
				break;
			case RP_PHY_TTLMT_REF_REST_LIMIT_TX_TIME:	// Time until transmission restriction is released (min)
				if (RpCb.txLmtTimer.samplingCnt < RP_MAX_NUM_TX_LMT_BUF)
				{
					iniSamplingCnt = (uint8_t)(RpCb.txLmtTimer.samplingCnt);
				}
				else
				{
					iniSamplingCnt = (uint8_t)(RpCb.txLmtTimer.samplingCnt - RP_MAX_NUM_TX_LMT_BUF);
				}
				for (samplingCnter = 0; samplingCnter < RP_MAX_NUM_TX_LMT_BUF;)
				{
					iniSamplingCnt++;
					if (iniSamplingCnt >= RP_MAX_NUM_TX_LMT_BUF)
					{
						iniSamplingCnt = 0;
					}

					samplingCnter++;

					if (RpTxTime[iniSamplingCnt] != 0)
					{
						break;
					}
				}
				rtn = ((samplingCnter * RpCb.pib.phyRmodeTcumSmpPeriod) / 60) + 1;
				break;
			case RP_PHY_TTLMT_REF_REST_INTERVAL_TIME:	// Time until the downtime release (usec)
				if (RpCb.tx.needTxWaitRegulatoryMode)	// if not need to wait, rtn is 0
				{
					realTime = RpCb.tx.sentTime;
					if (RpCb.pib.phyRegulatoryMode == RP_RMODE_ENABLE)
					{
						difTime = (uint32_t)(RpCb.pib.phyRmodeToffMin) * (uint32_t)(RpCb.pib.phyDataRate);
					}
					else
					{
						difTime = RpCalcTxInterval(RpCb.tx.sentLen, RP_FALSE);
					}
					realTime += (uint32_t)difTime;
					realTime &= (uint32_t)RP_TIME_MASK;
					willTxTime = RpGetTime();
					difTime2 = (uint32_t)((realTime - willTxTime/* tx time considered interval time */) & RP_TIME_MASK);
					if ((difTime2 <= RP_TIME_LIMIT) && (difTime2 <= difTime))
					{
						rtn = ((difTime2 * (uint32_t)RP_PHY_DATA_RATE_BPS_UNIT) / RpCb.pib.phyDataRate);
					}
				}
				break;
			default:
				break;
		}

		/* Enable interrupt */
		#if defined(__RX) || defined(__CCRL__) || defined(__arm)
		RP_PHY_EI(bkupPsw);
		#else
		RP_PHY_EI();
		#endif
	}

	return (rtn);
}
#endif // #ifdef RP_ENABLE_TxTotalTimeLimitControl

#ifdef RP_ENABLE_LimitTimerControlSwitch
/******************************************************************************
Function Name:       RpLimitTimerControlSwitch
Parameters:          timerOn: start timer
Return value:        lack time
Description:         Limitation Timer Control Switch
******************************************************************************/
void RpLimitTimerControlSwitch( uint8_t timerOn )
{
	if ((RpCb.pib.phyRegulatoryMode == RP_RMODE_ENABLE) ||
		((RpCb.pib.phyRegulatoryMode == RP_RMODE_ENABLE_ARIB) && (RpCb.pib.phyFreqBandId == RP_PHY_FREQ_BAND_920MHz)))
	{
		if (timerOn == RP_FALSE)
		{
			RpCb.txLmtTimer.lackTime += RpLimitTimerControl(timerOn);

#if defined(__RL78__)
			if (RpCb.txLmtTimer.lackTime > (uint32_t)1000)
			{
				RpCb.txLmtTimer.lackTime -= (uint32_t)1000;
				RpTxLimitTimerForward(1);
			}
#endif
		}
		else	// (timerOn == RP_TRUE )
		{
			RpLimitTimerControl(timerOn);
		}
	}

	return;
}
#endif // #ifdef RP_ENABLE_LimitTimerControlSwitch

/******************************************************************************
Function Name:       RpRegCcaBandwidth225k
Parameters:          none
Return value:        none
Description:         Register setting for CCA filter (225k)
******************************************************************************/
void RpRegCcaBandwidth225k( void )
{
	uint8_t index = RpCb.freqIdIndex;

	RpRegWrite((0x0432 << 3), (uint8_t)RpFreqBandTbl[index][RP_0x0432_OFFSET]);
	RpRegWrite((0x043A << 3), (uint8_t)RpFreqBandTbl[index][RP_0x043A_OFFSET]);
	RpRegWrite((0x0488 << 3), RP_PHY_REG488H_DEFAULT);
	RpRegWrite((0x0436 << 3), (uint8_t)RpFreqBandTbl[index][RP_0x0436_OFFSET]);
}

/******************************************************************************
Function Name:       RpRegCcaBandwidth200k
Parameters:          none
Return value:        none
Description:         Register setting for CCA filter (200k)
******************************************************************************/
static void RpRegCcaBandwidth200k( void )
{
	RpRegWrite((0x0432 << 3), 0x06);
	RpRegWrite((0x0436 << 3), 0x06);
	RpRegWrite((0x043A << 3), 0xBE);
	RpRegWrite((0x0488 << 3), 0x81);
}

/******************************************************************************
Function Name:       RpRegCcaBandwidth150k
Parameters:          none
Return value:        none
Description:         Register setting for CCA filter (150k)
******************************************************************************/
static void RpRegCcaBandwidth150k( void )
{
	RpRegWrite((0x0432 << 3), 0x08);
	RpRegWrite((0x0436 << 3), 0x08);
	RpRegWrite((0x043A << 3), 0xBE);
	RpRegWrite((0x0488 << 3), 0x81);
}

/******************************************************************************
Function Name:       RpRegAdcVgaDefault
Parameters:          none
Return value:        none
Description:         Register setting for ADC and VGA (default)
******************************************************************************/
void RpRegAdcVgaDefault( void )
{
	uint8_t index = RpCb.freqIdIndex;
	uint8_t fecRxEna = RpCb.pib.phyFskFecRxEna;
	uint8_t arrayChar[2];

	RpRegWrite((0x0471 << 3), (uint8_t)RpFreqBandTbl[index][RP_0x0471_OFFSET]);
	arrayChar[0] = (fecRxEna == RP_FEC_RX_MODE_ENABLE)? (uint8_t)RpFreqBandTbl[index][RP_0x0486_F_OFFSET]: (uint8_t)RpFreqBandTbl[index][RP_0x0486_OFFSET];
	arrayChar[1] = (fecRxEna == RP_FEC_RX_MODE_ENABLE)? (uint8_t)RpFreqBandTbl[index][RP_0x0487_F_OFFSET]: (uint8_t)RpFreqBandTbl[index][RP_0x0487_OFFSET];
	RpRegBlockWrite((0x0486 << 3), (uint8_t RP_FAR *)&(arrayChar[0]), 2);
	RpRegWrite((0x0505 << 3), (uint8_t)RpFreqBandTbl[index][RP_0x0505_OFFSET]);

	if (RpCb.pib.phyAntennaDiversityRxEna)
	{
		RpRegWrite((0x042D << 3), (uint8_t)RpFreqBandTbl[index][RP_0x042D_ANDVON_OFFSET]);
		
		if ((RpCb.pib.phyRegulatoryMode == RP_RMODE_ENABLE_ARIB) && (RpCb.pib.phyFreqBandId == RP_PHY_FREQ_BAND_920MHz))
		{
			RpRegWrite((0x0472 << 3), 0x04);
		}
		else
		{
			RpSetPreamble4ByteRxMode();
		}
	}
	else
	{
		RpRegWrite((0x042D << 3), (uint8_t)RpFreqBandTbl[index][RP_0x042D_OFFSET]);
		RpSetPreamble4ByteRxMode();
	}
}

/******************************************************************************
Function Name:       RpRegAdcVgaModify
Parameters:          none
Return value:        none
Description:         Register setting for ADC and VGA (modify)
******************************************************************************/
static void RpRegAdcVgaModify( void )
{
	RpRegWrite((0x042D << 3), 0x14);
	RpRegWrite((0x0471 << 3), 0x05);
	RpRegWrite((0x0472 << 3), 0x05);
	RpRegWrite((0x0486 << 3), 0x66);
	RpRegWrite((0x0487 << 3), 0x06);
	RpRegWrite((0x0505 << 3), 0x41);
}

/******************************************************************************
Function Name:       RpRegTxRxDataRateDefault
Parameters:          none
Return value:        none
Description:         Register setting for TX and RX (default)
******************************************************************************/
void RpRegTxRxDataRateDefault( void )
{
	uint8_t index = RpCb.freqIdIndex;

	RpRegWrite(BBSYMBLRATE, (uint8_t)RpFreqBandTbl[index][RP_SYMBOL_OFFSET_L]);
	RpRegWrite(BBSYMBLRATE_1, (uint8_t)RpFreqBandTbl[index][RP_SYMBOL_OFFSET_H]);
	RpRegWrite(BBMODSET, (uint8_t)RpFreqBandTbl[index][RP_MODESET_OFFSET]);
}

/******************************************************************************
Function Name:       RpRegTxRxDataRate100kbps
Parameters:          none
Return value:        none
Description:         Register setting for TX and RX (100kbps)
******************************************************************************/
static void RpRegTxRxDataRate100kbps( void )
{
	RpRegWrite(BBSYMBLRATE, 0xF0);
	RpRegWrite(BBSYMBLRATE_1, 0x00);
	RpRegWrite(BBMODSET, 0x42);
}

/******************************************************************************
Function Name:       RpRegRxDataRateDefault
Parameters:          none
Return value:        none
Description:         Register setting for RX (default)
******************************************************************************/
void RpRegRxDataRateDefault( void )
{
	uint8_t index = RpCb.freqIdIndex;
	const uint8_t *rssiLossPtr = (const uint8_t *)RpFreqBandTblRssiLossPtr[RpConfig.rssiLoss + 1];

	RpRegWrite((0x0423 << 3), (uint8_t)RpFreqBandTbl[index][RP_0x0423_OFFSET]);
	RpRegWrite((0x0430 << 3), (uint8_t)RpFreqBandTbl[index][RP_0x0430_OFFSET]);
	RpRegWrite((0x0473 << 3), (uint8_t)RpFreqBandTbl[index][RP_0x0473_OFFSET]);
	RpRegWrite((0x0474 << 3), (uint8_t)RpFreqBandTbl[index][RP_0x0474_OFFSET]);
	RpRegWrite((0x047C << 3), (uint8_t)RpFreqBandTbl[index][RP_0x047C_OFFSET]);
	RpRegWrite((0x047D << 3), (uint8_t)RpFreqBandTbl[index][RP_0x047D_OFFSET]);
	RpRegWrite((0x047E << 3), (uint8_t)RpFreqBandTbl[index][RP_0x047E_OFFSET]);
	RpRegWrite((0x047F << 3), (uint8_t)RpFreqBandTbl[index][RP_0x047F_OFFSET]);
	RpRegWrite((0x048D << 3), (uint8_t)RpFreqBandTbl[index][RP_0x048D_OFFSET]);
	RpRegWrite((0x048F << 3), (uint8_t)RpFreqBandTbl[index][RP_0x048F_OFFSET]);
	RpRegWrite((0x0493 << 3), (uint8_t)RpFreqBandTbl[index][RP_0x0493_OFFSET]);
	RpRegWrite((0x0494 << 3), (uint8_t)RpFreqBandTbl[index][RP_0x0494_OFFSET]);

	RpCb.pib.refVthVal = 0x0100 | (uint16_t)(*(rssiLossPtr + (((uint16_t)index * RP_MAXNUM_RSSI_LOSS) + RP_LVLVTH_OFFSET)));
	RpSetLvlVthVal();
	RpCb.pib.phyRssiLoss = (uint16_t) * (rssiLossPtr + (((uint16_t)index * RP_MAXNUM_RSSI_LOSS) + RP_0x0483_OFFSET));
	RpSetRssiOffsetVal();
}

/******************************************************************************
Function Name:       RpRegRxDataRate100kbps
Parameters:          none
Return value:        none
Description:         Register setting for RX (100kbps)
******************************************************************************/
static void RpRegRxDataRate100kbps( void )
{
	uint8_t index = RP_PHY_100K_M10_JP;
	const uint8_t *rssiLossPtr = (const uint8_t *)RpFreqBandTblRssiLossPtr[RpConfig.rssiLoss + 1];

	RpRegWrite((0x0423 << 3), 0x01);
	RpRegWrite((0x0430 << 3), 0x02);
	RpRegWrite((0x0473 << 3), 0xFB);
	RpRegWrite((0x0474 << 3), 0xA6);
	RpRegWrite((0x047C << 3), 0x1C);
	RpRegWrite((0x047D << 3), 0x0F);
	RpRegWrite((0x047E << 3), 0x03);
	RpRegWrite((0x047F << 3), 0x34);
	RpRegWrite((0x048D << 3), 0x57);
	RpRegWrite((0x048F << 3), 0x55);
	RpRegWrite((0x0493 << 3), 0x20);
	RpRegWrite((0x0494 << 3), 0x55);

	RpCb.pib.refVthVal = 0x0100 | (uint16_t)(*(rssiLossPtr + (((uint16_t)index * RP_MAXNUM_RSSI_LOSS) + RP_LVLVTH_OFFSET)));
	RpSetLvlVthVal();
	RpCb.pib.phyRssiLoss = (uint16_t) * (rssiLossPtr + (((uint16_t)index * RP_MAXNUM_RSSI_LOSS) + RP_0x0483_OFFSET));
	RpSetRssiOffsetVal();
}

/******************************************************************************
Function Name:		 RpLog_Event
Parameters: 		 funtion, option
Return value:		 none
Description:		 Debug log output function
******************************************************************************/
void RpLog_Event( uint8_t function, uint8_t option )
{
#ifdef RP_LOG_EVENT
	RpLogRam[RpLogRamCnt] = 0x01000000;
	RpLogRam[RpLogRamCnt] |= (uint32_t)(((uint16_t)function << 8) | option);
	RpLogRamCnt++;

	if ( RP_LOG_NUMBER <= RpLogRamCnt )
	{
		RpLogRamCnt = 0;
	}
#endif // #ifdef RP_LOG_EVENT
}

/***********************************************************************************************************************
 * Copyright (C) 2014-2021 Renesas Electronics Corporation.
 **********************************************************************************************************************/

