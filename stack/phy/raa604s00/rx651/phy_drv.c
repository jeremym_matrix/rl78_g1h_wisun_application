/***********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products.
 * No other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all 
 * applicable laws, including copyright laws. 
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING THIS SOFTWARE, WHETHER EXPRESS, 
 * IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE 
 * AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
 * TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS 
 * AFFILIATED COMPANIES SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR 
 * ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of 
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the 
 * following link:
 * http://www.renesas.com/disclaimer 
 **********************************************************************************************************************/
/***********************************************************************************************************************
 * file name	: phy_drv.c
 * description	: This is the RF driver's driver code.
 ***********************************************************************************************************************
 * Copyright (C) 2014-2021 Renesas Electronics Corporation.
 **********************************************************************************************************************/
/***************************************************************************************************************
 * includes
 **************************************************************************************************************/
#include <machine.h>
#include <string.h>

#include "iodefine.h"
#include "hardware.h"

#ifdef R_USE_RENESAS_STACK
	#include "int_vec_def.h"
#endif // #ifdef R_USE_RENESAS_STACK

#include "phy.h"
#include "phy_def.h"
#include "phy_drv.h"

/***************************************************************************************************************
 * pragma
 **************************************************************************************************************/
#ifndef R_USE_RENESAS_STACK
	#pragma interrupt INTTM04_Interrupt( save, vect=VECT( PERIB,INTB128 ))
	#pragma interrupt INTPx_Interrupt( save, vect=RP_RFIC_IRQ_INT_VECT )
	#pragma interrupt INTDMA0_Interrupt( save, vect=VECT( DMAC,DMAC0I ))
#endif // #ifndef R_USE_RENESAS_STACK

/***************************************************************************************************************
 * extern definitions
 **************************************************************************************************************/
extern RP_CONFIG_CB RpConfig;

/***************************************************************************************************************
 * macro definitions
 **************************************************************************************************************/
/* Interrupt Level */
#define RP_PHY_INTLEVEL_RFINTP			(RP_PHY_INTLEVEL)	// 
#define RP_PHY_INTLEVEL_RFTimers		(RP_PHY_INTLEVEL)	// 
#define RP_PHY_INTLEVEL_DMADone			(RP_PHY_INTLEVEL)	// 
#define RP_PHY_INTLEVEL_SYSCALLTimer	(4)					// OS system call

#define	RP_CSI_NOP				0xff
#define	RP_CSI_MEM_WR_H(adr)	(adr >> 8)
#define	RP_CSI_MEM_WR_L(adr)	(adr)
#define	RP_CSI_MEM_RD_H(adr)	(adr >> 8)
#define	RP_CSI_MEM_RD_L(adr)	(adr + 0x06)

#define BBTXRXRAMEND	(0x400 << 3)

/***************************************************************************************************************
 * static function prototypes
 **************************************************************************************************************/
static void RpSetMcuIntMaskOnly( uint8_t enable );
static void RpCSI_CSActivate( void );
static void RpCSI_CSDeactivate( void );
static uint8_t RpRegReadWrite( uint8_t wData );
static void RpCSIInitPort( void );
static void RpCSIInitialize( void );
static void RpRFTimerInitialize( void );
#if RP_DMA_WRITE_RAM_ENA
	static void RpDmaTerminationWriteSer( void );
#endif // #if RP_DMA_WRITE_RAM_ENA
#if RP_DMA_READ_RAM_ENA
	static void RpDmaTerminationReadSer( void );
#endif // if RP_DMA_READ_RAM_ENA

/***************************************************************************************************************
 * private variables
 **************************************************************************************************************/
#if RP_DMA_WRITE_RAM_ENA
	uint8_t RpDmaOnWriting = 0;
#endif

#if RP_DMA_READ_RAM_ENA
	uint8_t RpDmaOnReading = 0;
#endif

#ifdef RP_DEBUG_WRITE_MONITOR
	#define RP_DEBUG_RAM_SIZE	1024
	uint8_t RpDebugRamReg[RP_DEBUG_RAM_SIZE];
	uint16_t RpDebugRamRegCnt;
#endif

/***************************************************************************************************************
 * program
 **************************************************************************************************************/
/***************************************************************************************************************
 * function name  : RpGetExtaddr
 * description    : get self IEEE address.
 * parameters     : pExtAddr:the pointer of "reading IEEE address"
 * return value   : none
 **************************************************************************************************************/
void RpGetExtaddr( uint8_t *pExtAddr )
{
#if defined(RM_RAM_EXTADDR)
	extern uint8_t RpLocalExtAddr[];
	memcpy(pExtAddr, RpLocalExtAddr, 8);
#else
	memcpy(pExtAddr, (void *)0x007E0000, 8);
#endif
}

/***************************************************************************************************************
 * function name  : RpSetMcuInt
 * description    : setting INT interrupt
 * parameters     : 0:disable INT interrupt 1:enable INT interrupt
 * return value   : none
 **************************************************************************************************************/
void RpSetMcuInt( uint8_t enable )
{
	if (enable == RP_TRUE)
	{
		RP_RFIC_IRQ_FLT = 0;			// disable digital filter
		RP_RFIC_IRQ_FLT_SMP = 0x0000;	// digital filter sampling clock = PCLKB
		MPC.PWPR.BYTE = 0x00;			// permit PWPRreg PFSWEbit write
		MPC.PWPR.BYTE = 0x40;			// PFSWEbit <- "1": permit PFSreg write
		RP_RFIC_IRQ_ISEL = 1;			// Port => IRQ input mode
		MPC.PWPR.BYTE = 0x80;			// protect PWPRreg
		RP_RFIC_IRQ_FUNC = 1;			// IRQ Port => Peripheral
		RP_RFIC_IRQ_EDGE = 2;			// rising edge
		RP_RFIC_IRQ_FLT = 1;			// enable digital filter
		RP_RFIC_IRQ_INT_PRI = RP_PHY_INTLEVEL_RFINTP;
		RP_RFIC_IRQ_INT_REQ = RP_FALSE;	// IRQ REQ clear
	}
	else
	{
		RP_RFIC_IRQ_INT_REQ = RP_FALSE;	// IRQ REQ clear
	}

	/* enable/disable IRQ interrupt mask */
	RpSetMcuIntMaskOnly(enable);
}

/***************************************************************************************************************
 * function name  : RpSetMcuIntMaskOnly
 * description    : set interrupt mask
 * parameters     : enable
 *                :   0:disble interrupt, 1:enable interrupt
 * return value   : none
 **************************************************************************************************************/
static void RpSetMcuIntMaskOnly( uint8_t enable )
{
	/* accept RF IC interrupt */
	if (enable == RP_TRUE)
	{
		RP_RFIC_IRQ_INT_ENA = RP_TRUE;
	}
	else
	{
		RP_RFIC_IRQ_INT_ENA = RP_FALSE;
	}
}

/***************************************************************************************************************
 * function name  : RpCSI_CSActivate
 * description    : CS line control function.
 * parameters     : 
 * return value   : 
 **************************************************************************************************************/
static void RpCSI_CSActivate( void )
{
#if RP_DMA_WRITE_RAM_ENA
	if (RpDmaOnWriting == RP_TRUE)
	{
		RpDmaTerminationWriteSer();
	}
#endif
#if RP_DMA_READ_RAM_ENA
	if (RpDmaOnReading == RP_TRUE)
	{
		RpDmaTerminationReadSer();
	}
#endif
	RP_CSI_CS_PORT = RP_PORT_LO;     // Set CSN low to start new SPI sequence
}

/***************************************************************************************************************
 * function name  : RpCSI_CSDeactivate
 * description    : CS line control function.
 * parameters     : none
 * return value   : none
 **************************************************************************************************************/
static void RpCSI_CSDeactivate( void )
{
	RP_CSI_CS_PORT = RP_PORT_HI;     // Set CSN high to indicate command should be executed
}

/***************************************************************************************************************
 * function name  : RpRegReadWrite
 * description    : Wait Data Tx or Rx.
 * parameters     : 
 * return value   : 
 **************************************************************************************************************/
static uint8_t RpRegReadWrite( uint8_t wData )
{
	uint8_t rData;
	uint32_t bkupPsw;

	RP_PHY_DI(bkupPsw);
	RP_CSI_RXEND_INT_REQ = 0;
	RP_CSI_TX = wData;
	while (RP_CSI_RXEND_INT_REQ == 0);
	RP_CSI_RXEND_INT_REQ = 0;
	rData = RP_CSI_RX;
	RP_PHY_EI(bkupPsw);

	return rData;
}

/***************************************************************************************************************
 * function name  : RpRegWrite
 * description    : Write a data to RFIC function.
 * parameters     : 
 * return value   : none
 **************************************************************************************************************/
void RpRegWrite( uint16_t adr, uint8_t dat )
{
	RpCSI_CSActivate();

#ifdef RP_DEBUG_WRITE_MONITOR
	RpDebugRamReg[RpDebugRamRegCnt] = 0xfe;
	RpDebugRamRegCnt++;
	RpDebugRamRegCnt &= (RP_DEBUG_RAM_SIZE - 1);
	RpDebugRamReg[RpDebugRamRegCnt] = 0xfd;
	RpDebugRamRegCnt++;
	RpDebugRamRegCnt &= (RP_DEBUG_RAM_SIZE - 1);
#endif

#ifdef RP_TXRX_RAM_BIT_SWAP
	RP_CSI_CTRL = 0x00;
	RP_CSI_SMARTCARDMODE = 0xFA;			// MSB first
	RP_CSI_CTRL = 0xF0;
#endif

	RpRegReadWrite((uint8_t)RP_CSI_MEM_WR_H(adr));
	RpRegReadWrite((uint8_t)RP_CSI_MEM_WR_L(adr));

#ifdef RP_DEBUG_WRITE_MONITOR
	*(uint16_t *)(RpDebugRamReg + RpDebugRamRegCnt) = (uint16_t)(adr >> 3);
	{
		uint8_t test_ram[2];

		test_ram[0] = RpDebugRamReg[RpDebugRamRegCnt];
		test_ram[1] = RpDebugRamReg[RpDebugRamRegCnt + 1];
		RpDebugRamReg[RpDebugRamRegCnt] = test_ram[1];
		RpDebugRamReg[RpDebugRamRegCnt + 1] = test_ram[0];
	}
	RpDebugRamRegCnt += 2;
	RpDebugRamRegCnt &= (RP_DEBUG_RAM_SIZE - 1);
#endif

#ifdef RP_TXRX_RAM_BIT_SWAP
	if ((BBTXRAM0 <= adr) && (adr < BBTXRXRAMEND))
	{
		RP_CSI_CTRL = 0x00;
		RP_CSI_SMARTCARDMODE = 0xF2;		// LSB first
		RP_CSI_CTRL = 0xF0;
	}
#endif

	RpRegReadWrite(dat);

#ifdef RP_DEBUG_WRITE_MONITOR
	RpDebugRamReg[RpDebugRamRegCnt] = dat;
	RpDebugRamRegCnt++;
	RpDebugRamRegCnt &= (RP_DEBUG_RAM_SIZE - 1);
#endif

	RpCSI_CSDeactivate();
}

/***************************************************************************************************************
 * function name  : RpRegRead
 * description    : Read a data from RFIC function.
 * parameters     : 
 * return value   : 
 **************************************************************************************************************/
uint8_t RpRegRead( uint16_t adr )
{
	uint8_t rData;

	RpCSI_CSActivate();

#ifdef RP_TXRX_RAM_BIT_SWAP
	RP_CSI_CTRL = 0x00;
	RP_CSI_SMARTCARDMODE = 0xFA;			// MSB first
	RP_CSI_CTRL = 0xF0;
#endif

	RpRegReadWrite((uint8_t)RP_CSI_MEM_RD_H(adr));
	RpRegReadWrite((uint8_t)RP_CSI_MEM_RD_L(adr));

#ifdef RP_TXRX_RAM_BIT_SWAP
	if ((BBTXRAM0 <= adr) && (adr < BBTXRXRAMEND))
	{
		RP_CSI_CTRL = 0x00;
		RP_CSI_SMARTCARDMODE = 0xF2;		// LSB first
		RP_CSI_CTRL = 0xF0;
	}
#endif

	rData = RpRegReadWrite(RP_CSI_NOP);

	RpCSI_CSDeactivate();

	return  rData;
}

/***************************************************************************************************************
 * function name  : RpCSIInitPort
 * description    : CS line control function.
 * parameters     : none
 * return value   : none
 **************************************************************************************************************/
static void RpCSIInitPort( void )
{
	/* STANDBY,OSCDRVSEL,DON,RFRESETB Low output */
	RP_STANDBY_PORT		= RP_PORT_LO;
	RP_STANDBY_PM		= RP_PORT_OUTPUT;
	RP_OSCDRVSEL_PORT	= RP_PORT_LO;
	RP_OSCDRVSEL_PM		= RP_PORT_OUTPUT;
	RP_DON_PORT			= RP_PORT_LO;
	RP_DON_PM			= RP_PORT_OUTPUT;
	RP_RFRESETB_PORT	= RP_PORT_LO;
	RP_RFRESETB_PM		= RP_PORT_OUTPUT;

	/* MCU - RFIC Serial Setting 4Mbps, CS, RX, TX, CLK */
	RP_CSI_CS_PORT		= RP_PORT_HI;
	RP_CSI_CS_PM		= RP_PORT_OUTPUT;
	RP_CSI_RX_PM		= RP_PORT_INPUT;
	RP_CSI_RX_PULLUP	= 1;
	RP_CSI_TX_PORT		= RP_PORT_HI;
	RP_CSI_TX_PM		= RP_PORT_OUTPUT;
	RP_CSI_CLK_PORT		= RP_PORT_HI;
	RP_CSI_CLK_PM		= RP_PORT_OUTPUT;

	/* INTP */
	RP_INTP_PM			= RP_PORT_INPUT;
	RP_INTP_PULLUP		= 1;
}

/***************************************************************************************************************
 * function name  : RpCheckRfIRQ
 * description    : refer state of IRQ port
 * parameters     : none
 * return value   : IRQ state
 **************************************************************************************************************/
uint8_t RpCheckRfIRQ( void )
{
	uint8_t rtn;

	if (RP_INTP_PORT == RP_INTP_PORT_ACTIVE)
	{
		rtn = RP_TRUE;
	}
	else
	{
		rtn = RP_FALSE;
	}

	return (rtn);
}

/***************************************************************************************************************
 * function name  : RpCSIInitialize
 * description    : Initialize CSI
 * parameters     : none
 * return value   : none
 **************************************************************************************************************/
static void RpCSIInitialize( void )
{
	Rhw_NOP();
	Rhw_NOP();
	Rhw_NOP();
	Rhw_NOP();

	RP_CSI_CTRL = 0x00;
	MPC.PWPR.BYTE = 0x00;											// permit PWPRreg PFSWEbit write
	MPC.PWPR.BYTE = 0x40;											// PFSWE <- "1": permit PFSreg write
	RP_RFIC_TX_FUNC = RP_RFIC_TX_PFS_SET_VAL;						// Port -> TXD
	RP_RFIC_RX_FUNC = RP_RFIC_RX_PFS_SET_VAL;						// Port -> RXD
	RP_RFIC_CLK_FUNC = RP_RFIC_CLK_PFS_SET_VAL;						// Port -> SCK
	RP_RFIC_PORT_SEL |= RP_RFIC_PORT_SEL_SET_VAL;					// Port => Peripheral
	// Port -> TXD
	// Port -> RXD
	// Port -> SCK
	MPC.PWPR.BYTE = 0x80;											// protect PWPRreg

	RP_CSI_CLKPHASE = 0;											// clock delay
	RP_CSI_CLKPOL = 0;												// disable clock nagate
	RP_CSI_MODE = 0x80;												// clock sorce = PCLKB( 48[MHz] ), clock syncronous mode
	RP_CSI_SMARTCARDMODE = 0xFA;									// MSB first
	RP_CSI_BITRATE = 1;												// 5Mbps
	RP_CSI_CLK_PM = 1;												/* SCLK	<-	SCK */
	RP_CSI_RX_PM	 = 0;											/* MISO	->	SI/RxD */
	RP_CSI_TX_PM	 = 1;											/* MOSI	<-	SO/TxD */
	RP_CSI_TXEND_INT_REQ = 0;
	RP_CSI_TXEND_INT_ENA = 0;

	RP_CSI_CTRL = 0xF0;												// enable CSI TX and RX
}

/***************************************************************************************************************
 * function name  : RpRFTimerInitialize
 * description    : Initialize miscellaneous RFTimer
 * parameters     : none
 * return value   : none
 **************************************************************************************************************/
static void RpRFTimerInitialize( void )
{
	/* stop counter */
#ifdef R_USE_RENESAS_STACK
	TPUA.TSTR.BIT.CST2 = 0;
#endif // #ifdef R_USE_RENESAS_STACK
	TPUA.TSTR.BIT.CST4 = 0;

	// -------------------------------------------------
	//   Timer settings(16bit Timer02) for Base and HopStepJump
	// -------------------------------------------------
	TPU2.TCR.BIT.TPSC = 0x03;	/* Set TPU2 as 16 bit counter clocked by 625KHz(40MHz/64, 1.6[uS]) */
	TPU2.TCNT = 0x0000;
	TPU2.TCR.BIT.CCLR = 0x1;	/* TRGA compare clear */
	TPU2.TMDR.BYTE = 0x00;		/* normal */
	TPU2.TGRA = 0xffff;			/* */

#ifdef R_USE_RENESAS_STACK
	ICU.SLIBXR138.BYTE = 24;	/* TPU2 TGI2A (TGRAI:Input capture/Compare match) */
	IR(PERIB,INTB138) = 0;
	IPR(PERIB,INTB138) = RP_PHY_INTLEVEL_SYSCALLTimer;
	IEN(PERIB,INTB138) = 0;
#endif // #ifdef R_USE_RENESAS_STACK

#ifdef R_USE_RENESAS_STACK
	TPU2.TIER.BIT.TGIEA = 1;	/* interrupt enable at first */
#else
	TPU2.TIER.BIT.TGIEA = 0;	/* interrupt disable at first */
#endif // #ifdef R_USE_RENESAS_STACK

#ifndef RP_TX_TOTAL_UNLIMITED
	// -------------------------------------------------
	//   Timer settings(16bit Timer04) for 10% Limitation
	// -------------------------------------------------
#ifdef R_USE_RENESAS_STACK
	ICU.SLIBR160.BYTE = 33;		// TPU4 TGI4A (TGRAI:Input capture/Compare match)
	IR(PERIB,INTB160) = 0;
	IEN(PERIB,INTB160) = 0;
	IPR(PERIB,INTB160) = RP_PHY_INTLEVEL_RFTimers;
#else
	ICU.SLIBXR128.BYTE = 33;	// TPU4 TGI4A (TGRAI:Input capture/Compare match)
	IR(PERIB,INTB128) = 0;
	IEN(PERIB,INTB128) = 0;
	IPR(PERIB,INTB128) = RP_PHY_INTLEVEL_RFTimers;
#endif // #ifdef R_USE_RENESAS_STACK

	TPU4.TCR.BIT.TPSC = 0x06;	/* Set TPU4 as 16 bit counter clocked by 40MHz/1024 =25.6[uS] */
	TPU4.TCNT = 0x0000;
	TPU4.TGRA = 39062;			/* 1s */
	TPU4.TCR.BIT.CCLR = 0x1;	/* TRGA compare clear */
	TPU4.TMDR.BYTE = 0x00;		/* normal */

	TPU4.TIER.BIT.TGIEA = 1;
	RpLimitTimerControl(RP_TRUE);
#endif // #ifndef RP_TX_TOTAL_UNLIMITED
}

/***************************************************************************************************************
 * function name  : RpLimitTimerControl
 * description    : Limitation Timer Control
 * parameters     : 
 * return value   : lack time
 **************************************************************************************************************/
uint32_t RpLimitTimerControl( uint8_t timerOn )
{
	uint32_t	lackTime = 0;

#ifndef RP_TX_TOTAL_UNLIMITED
	TPUA.TSTR.BIT.CST4 = 0;
#ifdef RP_WISUN_FAN_STACK
	IEN(PERIB,INTB160) = 0;
#else
	IEN(PERIB,INTB128) = 0;
#endif
	if (timerOn == RP_TRUE)
	{
#ifdef RP_WISUN_FAN_STACK
		IEN(PERIB,INTB160) = 1;
#else
		IEN(PERIB,INTB128) = 1;
#endif
		TPUA.TSTR.BIT.CST4 = 1;
	}
#endif

	return (lackTime);
}

/***************************************************************************************************************
 * function name  : RpMcuPeripheralInit
 * description    : set port, CSI and timer
 * parameters     : none
 * return value   : none
 **************************************************************************************************************/
void RpMcuPeripheralInit( void )
{
	RpCSIInitPort();
	RpCSIInitialize();
	RpRFTimerInitialize();
}

/***************************************************************************************************************
 * function name  : RpWakeupSequence
 * description    : RF Initial Sequence function.
 * parameters     : 
 * return value   : 
 **************************************************************************************************************/
void RpWakeupSequence( void )
{
	TPUA.TSTR.BIT.CST2 = 0;

	if (RpConfig.rfClkSource == RP_RFCLK_SOURCE_CRYSTAL)
	{
		/* STANDBY = High, 500uS wait */
		TPU2.TGRA = 313 - 2;
		TPU2.TSR.BIT.TGFA = 0;	/* TGI2A compare match occur flag clear */
		RP_STANDBY_PORT = RP_PORT_HI;
		TPUA.TSTR.BIT.CST2 = 1;
		while (TPU2.TSR.BIT.TGFA == 0);
		TPUA.TSTR.BIT.CST2 = 0;
		/* OSDDRVSEL = High, 50uS wait */
		TPU2.TGRA = 32 - 2;
		TPU2.TSR.BIT.TGFA = 0;	/* TGI2A compare match occur flag clear */
		RP_OSCDRVSEL_PORT = RP_PORT_HI;
		TPUA.TSTR.BIT.CST2 = 1;
		while (TPU2.TSR.BIT.TGFA == 0);
		TPUA.TSTR.BIT.CST2 = 0;
	}
	else//#if RpConfig.rfClkSource == RP_RFCLK_SOURCE_CRYSTAL
	{
		RP_STANDBY_PORT = RP_PORT_HI;
		RP_OSCDRVSEL_PORT = RP_PORT_HI;
	}

	/* DON = High, 450uS wait */
	TPU2.TGRA = 282 - 2;
	TPU2.TSR.BIT.TGFA = 0;	/* TGI2A compare match occur flag clear */
	RP_DON_PORT = RP_PORT_HI;
	TPUA.TSTR.BIT.CST2 = 1;
	while (TPU2.TSR.BIT.TGFA == 0);
	TPUA.TSTR.BIT.CST2 = 0;

	/* RFRESETB = High calibration start */
	RP_RFRESETB_PORT = RP_PORT_HI;

	/* RFSIO and RFIRQ pull-up off*/
	RP_CSI_RX_PULLUP	= 0;
	RP_INTP_PULLUP		= 0;
	RpRegWrite(BBRFCON, RFSTART | ANARESETB);			/* Analog Reset */
	RpExecuteCalibration();
}

/***************************************************************************************************************
 * function name  : RpExecuteCalibration
 * description    : Check RF Calibration function.
 * parameters     : none
 * return value   : none
 **************************************************************************************************************/
void RpExecuteCalibration( void )
{
	uint16_t reg;

	RpRegWrite(BBRFCON, RFSTART | ANARESETB | CSONSET);	/* Calibration On */
	RpRegWrite(BBINTEN0,	(uint8_t)((RpRegRead(BBINTEN0)) | CALINTEN));// Caribration Interrupt Ennable
	RpRegRead(BBINTREQ0);								/* dummy read for clear bit of Caribration Interrupt */
	reg = 0x0000;
	RpRegBlockWrite(RFINI10, (uint8_t RP_FAR *)&reg, sizeof(uint16_t));
	reg = 0x0732;
	RpRegBlockWrite(RFINI10, (uint8_t RP_FAR *)&reg, sizeof(uint16_t));
	/* Calibration Int Wait */
	RpRegWrite(BBCAL, CALSTART);	/* Calibration Start */
	while ((RpRegRead(BBINTREQ0) & CALINTREQ) == 0);
	RpRegWrite(BBRFCON, RFSTART | ANARESETB);			/* Calibration Off */
	RpRegWrite(BBINTEN0,	(uint8_t)((RpRegRead(BBINTEN0)) & ~CALINTEN));// Caribration Interrupt Disable
}

/***************************************************************************************************************
 * function name  : RpPowerdownSequence
 * description    : RF Powerdown Sequence function.
 * parameters     : 
 * return value   : 
 **************************************************************************************************************/
void RpPowerdownSequence( void )
{
	/* RFSIO and RFIRQ pull-up on*/
	RP_CSI_RX_PULLUP	= 1;
	RP_INTP_PULLUP		= 1;
	/* STANDBY,OSCDRVSEL,DON,RFRESETB Low output */
	RP_STANDBY_PORT 	= RP_PORT_LO;
	if (RpConfig.rfClkSource == RP_RFCLK_SOURCE_CRYSTAL)
	{
		RP_OSCDRVSEL_PORT 	= RP_PORT_LO;
	}
	RP_DON_PORT 		= RP_PORT_LO;
	RP_RFRESETB_PORT 	= RP_PORT_LO;
}

#if RP_DMA_WRITE_RAM_ENA
/***************************************************************************************************************
 * function name  : RpRegBlockWrite
 * description    : Write meny data to RFIC with DMA function.
 * parameters     : 
 * return value   : 
 **************************************************************************************************************/
void RpRegBlockWrite( uint16_t adr, uint8_t RP_FAR *pDat, uint16_t len )
{
	if (len)
	{
		RpCSI_CSActivate();							// active RF IC chipselect

#ifdef RP_TXRX_RAM_BIT_SWAP
		RP_CSI_CTRL = 0x00;
		RP_CSI_SMARTCARDMODE = 0xFA;				// MSB first
		RP_CSI_CTRL = 0xF0;
#endif
		/* send address info */
		RpRegReadWrite((uint8_t)RP_CSI_MEM_WR_H(adr));
		RpRegReadWrite((uint8_t)RP_CSI_MEM_WR_L(adr));

#ifdef RP_TXRX_RAM_BIT_SWAP
		if ((BBTXRAM0 <= adr) && (adr < BBTXRXRAMEND))
		{
			RP_CSI_CTRL = 0x00;
			RP_CSI_SMARTCARDMODE = 0xF2;		// LSB first
			RP_CSI_CTRL = 0xF0;
		}
#endif

		if (len >= 2)
		{
			/* DMA and CSI setting */
			RP_CSI_CTRL = 0x00;
			DMAC.DMAST.BIT.DMST = 0;					// disable DMA start
			DMAC0.DMCNT.BIT.DTE = 0;					// disable DMA transmit
			ICU.DMRSR0 = RP_DMA_WRITE_START_REQ;		// DMA start request interrupt number
			DMAC0.DMTMD.BIT.DCTG = 1;					// DMA start trigger from peripheral

			IPR(DMAC, DMAC0I) = RP_PHY_INTLEVEL_DMADone;
			IR(DMAC, DMAC0I) = 0;						// clear DMA interrupt request
			IEN(DMAC, DMAC0I) = 1;						// enable DMA interrupt

			DMAC0.DMSAR = (uint8_t *)pDat;				// DMA source address(memory)
			DMAC0.DMAMD.BIT.SM = 2;						// DMA source address mode(increment)
			DMAC0.DMDAR = (uint8_t *)&RP_CSI_TX;		// DMA destinatin address(CSI TXreg)
			DMAC0.DMAMD.BIT.DM = 0;						// DMA source address mode(fix)
			DMAC0.DMCRA = len;							// DMA length

			RP_CSI_TXEND_INT_REQ = 0;					// clear CSI TX end interrupt request
			RP_CSI_TXEMP_INT_REQ = 0;					// clear CSI TX empty interrupt request

			DMAC0.DMSTS.BIT.DTIF = 0;					// disable DMA transmit interrupt
			DMAC0.DMINT.BIT.DTIE = 1;					// enable DMA end interrupt
			DMAC.DMAST.BIT.DMST = 1;					// enable DMA start
			DMAC0.DMCNT.BIT.DTE = 1;					// enable DMA transmit
			RpDmaOnWriting = RP_TRUE;

			RP_CSI_TXEND_INT_ENA = 1;					// enable CSI TX interrupt
			RP_CSI_CTRL = 0xA4;							// enable CSI TX, TX end interrupt

			RpDmaTerminationWriteSer();
		}
		else // if(len == 1)
		{
			RpRegReadWrite(*pDat);
			RpCSI_CSDeactivate();
		}
	}
}
#endif

#if RP_DMA_WRITE_RAM_ENA
/***************************************************************************************************************
 * function name  : RpDmaTerminationWriteSer
 * description    : Wait process of DMA write function.
 * parameters     : 
 * return value   : 
 **************************************************************************************************************/
static void RpDmaTerminationWriteSer( void )
{
	/* wait DMA transmit end */
	while (DMAC0.DMSTS.BIT.DTIF == 0);

	/* disable DMA module */
	DMAC.DMAST.BIT.DMST = 0;					// disable DMA start
	DMAC0.DMCNT.BIT.DTE = 0;					// disable DMA transmit
	DMAC0.DMINT.BIT.DTIE = 0;					// disable DMA end interrupt

	/* wait CSI transmit */
	while (RP_CSI_STATUS_TXEND == 0);

	RP_CSI_TXEND_INT_ENA = 0;					// disable CSI TX interrupt
	RP_CSI_CTRL = 0x00;
	IEN(DMAC, DMAC0I) = 0;						// disable DMA interrupt
	RP_CSI_CTRL = 0xF0;							// enable CSI TX and RX

	RpCSI_CSDeactivate();						// inactive RF IC chipselect
	RpDmaOnWriting = RP_FALSE;
}
#endif//#if RP_DMA_WRITE_RAM_ENA

#if RP_DMA_READ_RAM_ENA
/***************************************************************************************************************
 * function name  : RpRegBlockRead
 * description    : Read meny data from RFIC with DMA function.
 * parameters     : 
 * return value   : 
 **************************************************************************************************************/
void RpRegBlockRead( uint16_t adr, uint8_t *pDat, uint16_t len )
{
	uint32_t bkupPsw;

	if (len)
	{
		RpCSI_CSActivate();							// active RF IC chipselect

#ifdef RP_TXRX_RAM_BIT_SWAP
		RP_CSI_CTRL = 0x00;
		RP_CSI_SMARTCARDMODE = 0xFA;				// MSB first
		RP_CSI_CTRL = 0xF0;
#endif

		/* send address info */
		RpRegReadWrite((uint8_t)RP_CSI_MEM_RD_H(adr));
		RpRegReadWrite((uint8_t)RP_CSI_MEM_RD_L(adr));

#ifdef RP_TXRX_RAM_BIT_SWAP
		if ((BBTXRAM0 <= adr) && (adr < BBTXRXRAMEND))
		{
			RP_CSI_CTRL = 0x00;
			RP_CSI_SMARTCARDMODE = 0xF2;			// LSB first
			RP_CSI_CTRL = 0xF0;
		}
#endif

		if (len >= 2)
		{
			/* DMA and CSI setting */
			RP_CSI_CTRL = 0x00;
			DMAC.DMAST.BIT.DMST = 0;					// disable DMA start
			DMAC0.DMCNT.BIT.DTE = 0;					// disable DMA transmit
			DMAC1.DMCNT.BIT.DTE = 0;
			ICU.DMRSR0 = RP_DMA_WRITE_START_REQ;		// DMA start request interrupt number
			ICU.DMRSR1 = RP_DMA_READ_START_REQ;
			DMAC0.DMTMD.BIT.DCTG = 1;					// DMA start trigger from peripheral
			DMAC1.DMTMD.BIT.DCTG = 1;
			IPR(DMAC, DMAC0I) = RP_PHY_INTLEVEL_DMADone;
			IR(DMAC, DMAC0I) = 0;						// clear DMA interrupt request
			IEN(DMAC, DMAC0I) = 1;						// enable DMA interrupt
			IEN(DMAC, DMAC1I) = 0;						// disable DMA interrupt
			DMAC1.DMSAR = (uint8_t *)&RP_CSI_RX;		// DMA source address(peripheral)
			DMAC1.DMDAR = (uint8_t *)pDat;				// DMA destination address(memory)
			DMAC1.DMAMD.BIT.SM = 0;						// DMA source address mode(fix)
			DMAC1.DMAMD.BIT.DM = 2;						// DMA destination address mode(increment)
			DMAC1.DMCRA = len;							// DMA length
			DMAC0.DMSAR = (uint8_t *)pDat;				// DMA source address(memory)
			DMAC0.DMDAR = (uint8_t *)&RP_CSI_TX;		// DMA destination address(peripheral)
			DMAC0.DMAMD.BIT.SM = 0;						// DMA source address mode(fix)
			DMAC0.DMAMD.BIT.DM = 0;						// DMA destination address mode(fix)
			DMAC0.DMCRA = len;							// DMA length
			RP_CSI_TXEND_INT_REQ = 0;					// clear CSI TX end interrupt request
			RP_CSI_TXEMP_INT_REQ = 0;					// clear CSI TX empty interrupt request

			RP_PHY_ALL_DI(bkupPsw);
			DMAC0.DMSTS.BIT.DTIF = 0;					// disable DMA transmit interrupt
			DMAC0.DMINT.BIT.DTIE = 1;					// enable DMA end interrupt
			DMAC0.DMCNT.BIT.DTE = 1;					// enable DMA transmit
			DMAC1.DMSTS.BIT.DTIF = 0;					// disable DMA transmit interrupt
			DMAC1.DMINT.BIT.DTIE = 1;					// enable DMA end interrupt
			DMAC1.DMCNT.BIT.DTE = 1;					// enable DMA transmit
			DMAC.DMAST.BIT.DMST = 1;					// enable DMA start
			RP_PHY_ALL_EI(bkupPsw);

			RP_CSI_TXEND_INT_ENA = 1;					// enable CSI TX interrupt
			RP_CSI_RXEND_INT_ENA = 1;					// enable CSI RX interrupt

			RpDmaOnReading = RP_TRUE;

			RP_CSI_CTRL = 0xF4;							// enable CSI TX, TX end interrupt

			RpDmaTerminationReadSer();
		}
		else // if(len == 1)
		{
			*pDat = RpRegReadWrite(RP_CSI_NOP);
			RpCSI_CSDeactivate();
		}
	}
}
#endif

#if RP_DMA_READ_RAM_ENA
/***************************************************************************************************************
 * function name  : RpDmaTerminationReadSer
 * description    : Wait process of DMA read function.
 * parameters     : 
 * return value   : 
 **************************************************************************************************************/
static void RpDmaTerminationReadSer( void )
{
	/* wait DMA transmit end */
	while (DMAC0.DMSTS.BIT.DTIF == 0);
	while (DMAC1.DMSTS.BIT.DTIF == 0);

	/* disable DMA module */
	DMAC.DMAST.BIT.DMST = 0;					// disable DMA start
	DMAC0.DMCNT.BIT.DTE = 0;					// disable DMA start
	DMAC0.DMINT.BIT.DTIE = 0;					// disable DMA transmit
	DMAC1.DMCNT.BIT.DTE = 0;					// disable DMA start
	DMAC1.DMINT.BIT.DTIE = 0;					// disable DMA transmit

	/* wait CSI transmit */
	while (RP_CSI_STATUS_TXEND == 0);
	RP_CSI_TXEND_INT_ENA = 0;					// disable CSI TX interrupt
	RP_CSI_RXEND_INT_ENA = 0;					// disable CSI RX interrupt
	RP_CSI_CTRL = 0x00;
	IEN(DMAC, DMAC0I) = 0;						// disable DMA interrupt
	RP_CSI_CTRL = 0xF0;							// enable CSI TX and RX

	RpCSI_CSDeactivate();						// inactive RF IC chipselect
	RpDmaOnReading = RP_FALSE;
}
#endif//#if RP_DMA_READ_RAM_ENA

/***************************************************************************************************************
 * pragma (Interrupt Handle)
 **************************************************************************************************************/
#pragma section IntPRG

/***************************************************************************************************************
 * function name  : INTTM04_Interrupt
 * description    : 10% Limitation Timer interrupt.
 * parameters     : none
 * return value   : none
 **************************************************************************************************************/
void INTTM04_Interrupt( void )
{
#ifndef R_USE_RENESAS_STACK
	setpsw_i();
#endif // #ifndef R_USE_RENESAS_STACK

	RpTxLimitTimerForward(1000);
}

/***************************************************************************************************************
 * function name  : INTPx_Interrupt
 * description    : INT from RFIC interrupt.
 * parameters     : none
 * return value   : none
 **************************************************************************************************************/
void INTPx_Interrupt( void )
{
	setpsw_i();
	RpIntpHdr();
}

/***************************************************************************************************************
 * function name  : INTDMA0_Interrupt
 * description    : DMA0 Complete interrupt.
 * parameters     : none
 * return value   : none
 **************************************************************************************************************/
void INTDMA0_Interrupt( void )
{
}

/***********************************************************************************************************************
 * Copyright (C) 2014-2021 Renesas Electronics Corporation.
 **********************************************************************************************************************/

