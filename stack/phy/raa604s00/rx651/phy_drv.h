/***********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products.
 * No other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all 
 * applicable laws, including copyright laws. 
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING THIS SOFTWARE, WHETHER EXPRESS, 
 * IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE 
 * AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
 * TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS 
 * AFFILIATED COMPANIES SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR 
 * ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of 
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the 
 * following link:
 * http://www.renesas.com/disclaimer 
 **********************************************************************************************************************/
/***********************************************************************************************************************
 * file name	: phy_drv.h
 * description	: This is the RF driver's driver code.
 ***********************************************************************************************************************
 * Copyright (C) 2014-2021 Renesas Electronics Corporation.
 **********************************************************************************************************************/
#ifndef __phy_drv_H
#define __phy_drv_H

/***********************************************************************
 *	Port
 **********************************************************************/
#if defined(R_MCU_PACKAGE_64PIN)
	/* === uart registers (64pin) === */
	#define RP_STANDBY_X	A
	#define RP_STANDBY_Y	7
	#define RP_OSCDRVSEL_X	1
	#define RP_OSCDRVSEL_Y	7
	#define RP_DON_X		5
	#define RP_DON_Y		3
	#define RP_RFRESETB_X	C
	#define RP_RFRESETB_Y	1

	#define RP_CSI_X		B
	#define RP_CSI_RX_X 	RP_CSI_X
	#define RP_CSI_RX_Y 	6
	#define RP_CSI_TX_X 	RP_CSI_X
	#define RP_CSI_TX_Y 	7
	#define RP_CSI_CLK_X	RP_CSI_X
	#define RP_CSI_CLK_Y	5
	#define RP_CSI_CS_X 	A
	#define RP_CSI_CS_Y 	6

	#define RP_INTP_X		4
	#define RP_INTP_Y		2

	#define RP_INTP_IRQ 	IRQ10

	#define RP_SCI_CH		9	// SCI9
#else
	/* === uart registers (100pin) === */
	#define RP_STANDBY_X	A
	#define RP_STANDBY_Y	7
	#define RP_OSCDRVSEL_X	1
	#define RP_OSCDRVSEL_Y	7
	#define RP_DON_X		4
	#define RP_DON_Y		4
	#define RP_RFRESETB_X	A
	#define RP_RFRESETB_Y	2

	#define RP_CSI_X		2
	#define RP_CSI_RX_X		RP_CSI_X
	#define RP_CSI_RX_Y		1
	#define RP_CSI_TX_X		RP_CSI_X
	#define RP_CSI_TX_Y		0
	#define RP_CSI_CLK_X	RP_CSI_X
	#define RP_CSI_CLK_Y	2
	#define RP_CSI_CS_X		A
	#define RP_CSI_CS_Y		6

	#define RP_INTP_X		5
	#define RP_INTP_Y		5

	#define RP_INTP_IRQ		IRQ10

	#define RP_SCI_CH		0	// SCI0
#endif

/***********************************************************
 *	Macro
 **********************************************************/
#define RP_PDR(a,b)			_RP_PDR(a,b)
#define _RP_PDR(a,b)		PORT ## a ## .PDR.BIT.B ## b
#define RP_PCR(a,b)			_RP_PCR(a,b)
#define _RP_PCR(a,b)		PORT ## a ## .PCR.BIT.B ## b
#define RP_PIDR(a,b)		_RP_PIDR(a,b)
#define _RP_PIDR(a,b)		PORT ## a ## .PIDR.BIT.B ## b
#define RP_PODR(a,b)		_RP_PODR(a,b)
#define _RP_PODR(a,b)		PORT ## a ## .PODR.BIT.B ## b
#define RP_PMR_BYTE(a)		_RP_PMR_BYTE(a)
#define _RP_PMR_BYTE(a)		PORT ## a ##.PMR.BYTE
#define RP_PMR(a,b)			_RP_PMR(a,b)
#define _RP_PMR(a,b)		PORT ## a ## .PMR.BIT.B ## b

#define RP_MPC(a,b)			_RP_MPC(a,b)
#define _RP_MPC(a,b)		MPC.P ## a ## b ## PFS.BYTE
#define RP_MPC_ISEL(a,b)	_RP_MPC_ISEL(a,b)
#define _RP_MPC_ISEL(a,b)	MPC.P ## a ## b ## PFS.BIT.ISEL

#define RP_SCI_MDL(a)		_RP_SCI_MDL(a)
#define _RP_SCI_MDL(a)		SCI ## a

#define RP_IR_SCI(a,b)		_RP_IR_SCI(a,b)
#define _RP_IR_SCI(a,b)		IR(SCI ## a ## , ## b ## a ## )
#define RP_EN_SCI(a,b)		_RP_EN_SCI(a,b)
#define _RP_EN_SCI(a,b)		EN(SCI ## a ## , ## b ## a ## )
#define RP_IEN_SCI(a,b)		_RP_IEN_SCI(a,b)
#define _RP_IEN_SCI(a,b)	IEN(SCI ## a ## , ## b ## a ## )

#define RP_IR_ICU(a)		_RP_IR_ICU(a)
#define _RP_IR_ICU(a)		IR(ICU, ## a ## )
#define RP_EN_ICU(a)		_RP_EN_ICU(a)
#define _RP_EN_ICU(a)		EN(ICU, ## a ## )
#define RP_IEN_ICU(a)		_RP_IEN_ICU(a)
#define _RP_IEN_ICU(a)		IEN(ICU, ## a ## )
#define RP_IPR_ICU(a)		_RP_IPR_ICU(a)
#define _RP_IPR_ICU(a)		IPR(ICU, ## a ## )
#define RP_VECT_ICU(a)		_RP_VECT_ICU(a)
#define _RP_VECT_ICU(a)		VECT(ICU, ## a ## )

#define RP_DTCE_SCI(a,b)	_RP_DTCE_SCI(a,b) 
#define _RP_DTCE_SCI(a,b)	DTCE_SCI ## a ## _ ## b ## a

#define RP_SCI_TX			TXI
#define RP_SCI_RX			RXI
#define RP_SCI_TE			TEI

/***********************************************************
 *	Port Level
 **********************************************************/
#define RP_PORT_INPUT			0
#define RP_PORT_OUTPUT			1
#define RP_PORT_HI				1
#define RP_PORT_LO				0
#define RP_PORT_PULLUP_ON		1
#define RP_PORT_PULLUP_OFF		0

/***********************************************************
 *	Ports
 **********************************************************/
#define RP_STANDBY_PM			RP_PDR(RP_STANDBY_X,RP_STANDBY_Y)
#define RP_STANDBY_PORT			RP_PODR(RP_STANDBY_X,RP_STANDBY_Y)
#define RP_OSCDRVSEL_PM			RP_PDR(RP_OSCDRVSEL_X,RP_OSCDRVSEL_Y)
#define RP_OSCDRVSEL_PORT		RP_PODR(RP_OSCDRVSEL_X,RP_OSCDRVSEL_Y)
#define RP_DON_PM				RP_PDR(RP_DON_X,RP_DON_Y)
#define RP_DON_PORT				RP_PODR(RP_DON_X,RP_DON_Y)
#define RP_RFRESETB_PM			RP_PDR(RP_RFRESETB_X,RP_RFRESETB_Y)
#define RP_RFRESETB_PORT		RP_PODR(RP_RFRESETB_X,RP_RFRESETB_Y)

/***********************************************************
 *	CSI Ports
 **********************************************************/
#define RP_CSI_RX_PM			RP_PDR(RP_CSI_RX_X,RP_CSI_RX_Y)
#define RP_CSI_RX_PULLUP		RP_PCR(RP_CSI_RX_X,RP_CSI_RX_Y)
#define RP_CSI_TX_PM			RP_PDR(RP_CSI_TX_X,RP_CSI_TX_Y)
#define RP_CSI_CLK_PM			RP_PDR(RP_CSI_CLK_X,RP_CSI_CLK_Y)
#define RP_CSI_RX_PORT			RP_PIDR(RP_CSI_RX_X,RP_CSI_RX_Y)
#define RP_CSI_TX_PORT			RP_PODR(RP_CSI_TX_X,RP_CSI_TX_Y)
#define RP_CSI_CLK_PORT			RP_PODR(RP_CSI_CLK_X,RP_CSI_CLK_Y)
#define RP_CSI_CS_PM			RP_PDR(RP_CSI_CS_X,RP_CSI_CS_Y)
#define RP_CSI_CS_PORT			RP_PODR(RP_CSI_CS_X,RP_CSI_CS_Y)

/***********************************************************
 *	INTP Port
 **********************************************************/
#define RP_INTP_PORT			RP_PIDR(RP_INTP_X,RP_INTP_Y)
#define RP_INTP_PM				RP_PDR(RP_INTP_X,RP_INTP_Y)
#define RP_INTP_PULLUP			RP_PCR(RP_INTP_X,RP_INTP_Y)
#define RP_INTP_PORT_ACTIVE		RP_PORT_HI
#define RP_INTP_PORT_INACTIVE	RP_PORT_LO

/***********************************************************
 *	CSI Ports Functions
 **********************************************************/
#define RP_RFIC_RX_FUNC				RP_MPC(RP_CSI_RX_X,RP_CSI_RX_Y)
#define RP_RFIC_RX_PFS_SET_VAL		0x0A
#define RP_RFIC_TX_FUNC				RP_MPC(RP_CSI_TX_X,RP_CSI_TX_Y)
#define RP_RFIC_TX_PFS_SET_VAL		0x0A
#define RP_RFIC_CLK_FUNC			RP_MPC(RP_CSI_CLK_X,RP_CSI_CLK_Y)
#define RP_RFIC_CLK_PFS_SET_VAL		0x0A
#define RP_RFIC_PORT_SEL			RP_PMR_BYTE(RP_CSI_X)
#define RP_RFIC_PORT_SEL_SET_VAL	((1 << RP_CSI_RX_Y) | (1 << RP_CSI_TX_Y) | (1 << RP_CSI_CLK_Y))		// set to perioheral
#define RP_RFIC_IRQ_ISEL			RP_MPC_ISEL(RP_INTP_X,RP_INTP_Y)
#define RP_RFIC_IRQ_FUNC			RP_PMR(RP_INTP_X,RP_INTP_Y)

#define RP_RFIC_IRQ_INT_ENA			RP_IEN_ICU(RP_INTP_IRQ)
#define RP_RFIC_IRQ_INT_REQ			RP_IR_ICU(RP_INTP_IRQ)
#define RP_RFIC_IRQ_INT_PRI			RP_IPR_ICU(RP_INTP_IRQ)
#define RP_RFIC_IRQ_INT_VECT		RP_VECT_ICU(RP_INTP_IRQ)

#define RP_RFIC_IRQ_FLT				ICU.IRQFLTE1.BIT.FLTEN10
#define RP_RFIC_IRQ_FLT_SMP			ICU.IRQFLTC0.WORD
#define RP_RFIC_IRQ_EDGE			ICU.IRQCR[10].BIT.IRQMD

/***********************************************************
 *	CSI Registers
 **********************************************************/
#define RP_CSI_RX				RP_SCI_MDL(RP_SCI_CH).RDR
#define RP_CSI_TX				RP_SCI_MDL(RP_SCI_CH).TDR

#define RP_CSI_BITRATE			RP_SCI_MDL(RP_SCI_CH).BRR
#define RP_CSI_MODE				RP_SCI_MDL(RP_SCI_CH).SMR.BYTE
#define RP_CSI_CTRL				RP_SCI_MDL(RP_SCI_CH).SCR.BYTE
#define RP_CSI_STATUS			RP_SCI_MDL(RP_SCI_CH).SSR.BYTE
#define RP_CSI_SMARTCARDMODE	RP_SCI_MDL(RP_SCI_CH).SCMR.BYTE

#define RP_CSI_CLKPOL			RP_SCI_MDL(RP_SCI_CH).SPMR.BIT.CKPOL
#define RP_CSI_CLKPHASE			RP_SCI_MDL(RP_SCI_CH).SPMR.BIT.CKPH
#define RP_CSI_STATUS_TXEND		RP_SCI_MDL(RP_SCI_CH).SSR.BIT.TEND


#define RP_CSI_TXEND_INT_REQ	RP_IR_SCI(RP_SCI_CH,RP_SCI_TX)
#define RP_CSI_TXEMP_INT_REQ	RP_EN_SCI(RP_SCI_CH,RP_SCI_TE)
#define RP_CSI_RXEND_INT_REQ	RP_IR_SCI(RP_SCI_CH,RP_SCI_RX)

#define RP_CSI_TXEND_INT_ENA	RP_IEN_SCI(RP_SCI_CH,RP_SCI_TX)
#define RP_CSI_RXEND_INT_ENA	RP_IEN_SCI(RP_SCI_CH,RP_SCI_RX)

/***********************************************************************
 *	DMA
 **********************************************************************/
#if RP_DMA_WRITE_RAM_ENA
#define RP_DMA_WRITE_START_REQ	RP_DTCE_SCI(RP_SCI_CH,RP_SCI_TX)
#endif
#if RP_DMA_READ_RAM_ENA
#define RP_DMA_READ_START_REQ	RP_DTCE_SCI(RP_SCI_CH,RP_SCI_RX)
#endif

#endif // #ifndef __phy_drv_H
/***********************************************************************************************************************
 * Copyright (C) 2014-2021 Renesas Electronics Corporation.
 **********************************************************************************************************************/

