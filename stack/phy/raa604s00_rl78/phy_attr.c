/***********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products.
 * No other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all 
 * applicable laws, including copyright laws. 
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING THIS SOFTWARE, WHETHER EXPRESS, 
 * IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE 
 * AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
 * TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS 
 * AFFILIATED COMPANIES SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR 
 * ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of 
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the 
 * following link:
 * http://www.renesas.com/disclaimer 
 **********************************************************************************************************************/
/***********************************************************************************************************************
 * file name	: phy_attr.c
 * description	: Attribute control function
 ***********************************************************************************************************************
 * Copyright (C) 2014-2021 Renesas Electronics Corporation.
 **********************************************************************************************************************/
/***************************************************************************************************************
 * includes
 **************************************************************************************************************/
#include "phy.h"
#include "phy_def.h"

#if defined(__arm)
	#include "cpx3.h"
	#include "phy_drv.h"
#endif

/***************************************************************************************************************
 * extern definitions
 **************************************************************************************************************/
extern RP_PHY_CB RpCb;

/***************************************************************************************************************
 * macro definitions
 **************************************************************************************************************/
#define RP_MinValue_phyBackOffSeed	(0x01u)
#define RP_MinValue_macMaxBe		(0x03u)	//RP_MINVAL_MAXBE

#define RP_NUM_FREQ_BAND		(10u)
#define RP_MAX_FSK_OPEMODE		(7u)

/***************************************************************************************************************
 * typedef definitions
 **************************************************************************************************************/
typedef struct {
	uint8_t maxChannel[RP_MAX_FSK_OPEMODE];
} r_maxChannelTbl_t;

/***************************************************************************************************************
 * static function prototypes
 **************************************************************************************************************/
#if !defined(RP_WISUN_FAN_STACK) && !defined(RP_ST_ENV)
	static int16_t RpPlmeSetReq( uint8_t attrId, void RP_FAR *p_attrValue );
#endif // #if !defined(RP_WISUN_FAN_STACK) && !defined(RP_ST_ENV)
static uint8_t RpSetAttr_phyCurrentChannel( uint8_t attrValue );
#ifndef RP_WISUN_FAN_STACK
static uint8_t RpCheckChannelsSupported( uint8_t channel );
static uint8_t RpConvertChannelsSupportedPage( uint8_t channel );
#endif // ifndef RP_WISUN_FAN_STACK
static uint8_t RpSetAttr_phyChannelsSupportedPage( uint8_t attrValue );
static uint8_t RpSetAttr_phyCcaVth( uint16_t attrValue );
static uint8_t RpSetAttr_phyTransmitPower( uint8_t attrValue );
static uint8_t RpSetAttr_phyFskFecRxEna( uint8_t attrValue );
static uint8_t RpSetAttr_phyFskFecTxEna( uint8_t attrValue );
static uint8_t RpSetAttr_phyFskFecScheme( uint8_t attrValue );
static uint8_t RpSetAttr_phyLvlFltrVth( uint16_t attrValue );
static uint8_t RpSetAttr_phyBackOffSeed( uint8_t attrValue );
static uint8_t RpSetAttr_phyCrcErrorUpMsg( uint8_t attrValue );
static uint8_t RpSetAttr_macAddressFilter1Ena( uint8_t attrValue );
static uint8_t RpSetAttr_macShortAddress1( uint16_t attrValue );
static uint8_t RpSetAttr_macExtendedAddress1( void RP_FAR *p_attrValue );
static uint8_t RpSetAttr_macPanId1( uint16_t attrValue );
static uint8_t RpSetAttr_macPanCoord1( uint8_t attrValue );
static uint8_t RpSetAttr_macFramePend1( uint8_t attrValue );
static uint8_t RpSetAttr_macAddressFilter2Ena( uint8_t attrValue );
static uint8_t RpSetAttr_macShortAddress2( uint16_t attrValue );
static uint8_t RpSetAttr_macExtendedAddress2( void RP_FAR *p_attrValue );
static uint8_t RpSetAttr_macPanId2( uint16_t attrValue );
static uint8_t RpSetAttr_macPanCoord2( uint8_t attrValue );
static uint8_t RpSetAttr_macFramePend2( uint8_t attrValue );
static uint8_t RpSetAttr_macMaxCsmaBackOff( uint8_t attrValue );
static uint8_t RpSetAttr_macMinBe( uint8_t attrValue );
static uint8_t RpSetAttr_macMaxBe( uint8_t attrValue );
static uint8_t RpSetAttr_macMaxFrameRetries( uint8_t attrValue );
static uint8_t RpSetAttr_macCsmaBackoffPeriod( uint16_t attrValue );
static uint8_t RpSetAttr_phyCcaDuration( uint16_t attrValue );
static uint8_t RpSetAttr_phyMrFskSfd( uint8_t attrValue );
static uint8_t RpSetAttr_phyFskPreambleLength( uint16_t attrValue );
static uint8_t RpSetAttr_phyFskScramblePsdu( uint8_t attrValue );
static uint8_t RpSetAttr_phyFskOpeMode( uint8_t attrValue );
static uint8_t RpSetAttr_phyFcsLength( uint8_t attrValue );
static uint8_t RpSetAttr_phyAckReplyTime( uint16_t attrValue );
static uint8_t RpSetAttr_phyAckWaitDuration( uint16_t attrValue );
static uint8_t RpSetAttr_phyProfileSpecificMode( uint8_t attrValue );
static uint8_t RpSetAttr_phyAntennaSwitchEna( uint8_t attrValue );
static uint8_t RpSetAttr_phyAntennaDiversityRxEna( uint8_t attrValue );
static uint8_t RpSetAttr_phyAntennaSelectTx( uint8_t attrValue );
static uint8_t RpSetAttr_phyAntennaSelectAckTx( uint8_t attrValue );
static uint8_t RpSetAttr_phyAntennaSelectAckRx( uint8_t attrValue );
static uint8_t RpSetAttr_phyRxTimeoutMode( uint8_t attrValue );
static uint8_t RpSetAttr_phyFreqBandId( uint8_t attrValue );
static uint8_t RpSetAttr_phyRegulatoryMode( uint8_t attrValue );
static uint8_t RpSetAttr_phyPreamble4byteRxMode( uint8_t attrValue );
static uint8_t RpSetAttr_phyAgcStartVth( uint16_t attrValue );
static uint8_t RpSetAttr_phyCcaBandwidth( uint8_t attrValue );
static uint8_t RpSetAttr_phyEdBandwidth( uint8_t attrValue );
static uint8_t RpSetAttr_phyAntennaDiversityStartVth( uint16_t attrValue );
static uint8_t RpSetAttr_phyAntennaSwitchingTime( uint16_t attrValue );
static uint8_t RpSetAttr_phySfdDetectionExtend( uint8_t attrValue );
static uint8_t RpSetAttr_phyAgcWaitGainOffset( uint8_t attrValue );
static uint8_t RpSetAttr_phyCcaVthOffset( uint8_t attrValue );
static uint8_t RpSetAttr_phyAntennaSwitchEnaTiming( uint16_t attrValue );
static uint8_t RpSetAttr_phyGpio0Setting( uint8_t attrValue );
static uint8_t RpSetAttr_phyGpio3Setting( uint8_t attrValue );
static uint8_t RpSetAttr_phyRmodeTonMax( uint16_t attrValue );
static uint8_t RpSetAttr_phyRmodeToffMin( uint16_t attrValue );
static uint8_t RpSetAttr_phyRmodeTcumSmpPeriod( uint16_t attrValue );
static uint8_t RpSetAttr_phyRmodeTcumLimit( uint32_t attrValue );
static uint8_t RpSetAttr_phyRssiOutputOffset( uint8_t attrValue );
#if !defined(RP_WISUN_FAN_STACK) && !defined(RP_ST_ENV)
	static void RpPlmeGetReq( uint8_t id, void *pVal );
#endif // #if !defined(RP_WISUN_FAN_STACK) && !defined(RP_ST_ENV)

/***************************************************************************************************************
 * private variables
 **************************************************************************************************************/
static const uint8_t g_MaxFskOpeModeTbl[RP_NUM_FREQ_BAND] = 
{
	0x07,0x03,0x03,0x04,0x04,0x06,0x03,0x03,0x01,0x04
};

static const r_maxChannelTbl_t g_MaxChannelTbl[RP_NUM_FREQ_BAND] =
{
	/* freqBanId = 0x04 */ { 0x21,0x10,0x10,0x44,0x44,0x44,0x44 },
	/* freqBanId = 0x05 */ { 0xC6,0x62,0x30,0x00,0x00,0x00,0x00 },
	/* freqBanId = 0x06 */ { 0x26,0x12,0x08,0x00,0x00,0x00,0x00 },
	/* freqBanId = 0x07 */ { 0x80,0x3F,0x3F,0x80,0x00,0x00,0x00 },
	/* freqBanId = 0x08 */ { 0x1F,0x0F,0x0F,0x1F,0x00,0x00,0x00 },
	/* freqBanId = 0x09 */ { 0x25,0x24,0x23,0x23,0x24,0x25,0x00 },
	/* freqBanId = 0x0E */ { 0x23,0x80,0x29,0x00,0x00,0x00,0x00 },
	/* freqBanId = 0x0F */ { 0x3A,0x3A,0x3A,0x00,0x00,0x00,0x00 },
	/* freqBanId = 0x10 */ { 0x80,0x00,0x00,0x00,0x00,0x00,0x00 },
	/* freqBanId = 0x11 */ { 0x11,0x11,0x11,0x11,0x00,0x00,0x00 }
};

/***************************************************************************************************************
 * program
 **************************************************************************************************************/
/***************************************************************************************************************
 * function name  : RpSetPibReq
 * description    : Attribute Value Setting Function
 * parameters     : id...Attribute ID
 *                : valLen...Size of the attribute value to be set (byte length)
 *                : pVal...Pointer to the beginning of the area storing the attribute value to be set
 * return value   : RP_SUCCESS, RP_UNSUPPORTED_ATTRIBUTE, RP_INVALID_PARAMETER, RP_BUSY_RX, RP_BUSY_TX
 *				  : RP_BUSY_LOWPOWER
 **************************************************************************************************************/
int16_t RpSetPibReq( uint8_t id, uint8_t valLen, void RP_FAR *pVal )
{
	uint16_t status;
	int16_t rtn = RpExtChkIdLenSetReq(id, valLen, pVal);
	int16_t result;

	#if defined(__RX)
	uint32_t bkupPsw;
	#elif defined(__CCRL__) || defined(__ICCRL78__)
	uint8_t  bkupPsw;
	#elif defined(__arm)
	uint8_t bkupPsw[RP_INT_BKUP_BUF_SIZE];
	#endif

	/* Disable interrupt */
	#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
	RP_PHY_DI(bkupPsw);
	#else
	RP_PHY_DI();
	#endif

	/* API function execution Log */
	RpLog_Event( RP_LOG_API_SETIB, id );

	if (rtn != RP_SUCCESS)
	{
		/* API function execution Log */
		RpLog_Event( RP_LOG_API_SETIB | RP_LOG_API_RETERR, (uint8_t)rtn );

		/* Enable interrupt */
		#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
		RP_PHY_EI(bkupPsw);
		#else
		RP_PHY_EI();
		#endif

		return (rtn);
	}

	status = RpCb.status;

	if (status & RP_PHY_STAT_LOWPOWER)
	{
		/* API function execution Log */
		RpLog_Event( RP_LOG_API_SETIB | RP_LOG_API_RETERR, RP_BUSY_LOWPOWER );

		/* Enable interrupt */
		#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
		RP_PHY_EI(bkupPsw);
		#else
		RP_PHY_EI();
		#endif

		return (RP_BUSY_LOWPOWER);
	}
	else if ((status & RP_PHY_STAT_TRX_OFF) == 0)
	{
		if (status & RP_PHY_STAT_TX)
		{
			/* API function execution Log */
			RpLog_Event( RP_LOG_API_SETIB | RP_LOG_API_RETERR, RP_BUSY_TX );

			/* Enable interrupt */
			#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
			RP_PHY_EI(bkupPsw);
			#else
			RP_PHY_EI();
			#endif

			return (RP_BUSY_TX);
		}
		else
		{
			/* API function execution Log */
			RpLog_Event( RP_LOG_API_SETIB | RP_LOG_API_RETERR, RP_BUSY_RX );

			/* Enable interrupt */
			#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
			RP_PHY_EI(bkupPsw);
			#else
			RP_PHY_EI();
			#endif

			return (RP_BUSY_RX);
		}
	}

	result = RpPlmeSetReq( id, pVal );
	if ( result != RP_SUCCESS )
	{
		/* API function execution Log */
		RpLog_Event( RP_LOG_API_SETIB | RP_LOG_API_RETERR, (uint8_t)result );

		/* Enable interrupt */
		#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
		RP_PHY_EI(bkupPsw);
		#else
		RP_PHY_EI();
		#endif

		return (result);
	}

	/* API function execution Log */
	RpLog_Event( RP_LOG_API_SETIB | RP_LOG_API_RET, RP_SUCCESS );

	/* Enable interrupt */
	#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
	RP_PHY_EI(bkupPsw);
	#else
	RP_PHY_EI();
	#endif

	return (RP_SUCCESS);
}

/***************************************************************************************************************
 * function name  : RpPlmeSetReq
 * description    : Attribute Value Setting Function
 * parameters     : attrId...Attribute ID
 *                : p_attrValue...
 * return value   : result
 **************************************************************************************************************/
#if !defined(RP_WISUN_FAN_STACK) && !defined(RP_ST_ENV)
static int16_t RpPlmeSetReq( uint8_t attrId, void RP_FAR *p_attrValue )
#else
int16_t RpPlmeSetReq( uint8_t attrId, void RP_FAR *p_attrValue )
#endif // #if !defined(RP_WISUN_FAN_STACK) && !defined(RP_ST_ENV)
{
	int16_t result = RP_SUCCESS;
	uint8_t RP_FAR  *p_wk8;
	uint8_t wk8;
	uint16_t wk16;
	uint32_t wk32;

	#if defined(__RX)
	uint32_t bkupPsw;
	#elif defined(__CCRL__) || defined(__ICCRL78__)
	uint8_t bkupPsw;
	#elif defined(__arm)
	uint8_t bkupPsw[RP_INT_BKUP_BUF_SIZE];
	#endif

	p_wk8 = (uint8_t RP_FAR *)p_attrValue;
	wk8 = *p_wk8;
	wk16 = RP_VAL_ARRAY_TO_UINT16( p_wk8 );
	wk32 = RP_VAL_ARRAY_TO_UINT32( p_wk8 );

	/* Disable interrupt */
	#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
	RP_PHY_DI(bkupPsw);
	#else
	RP_PHY_DI();
	#endif

	switch ( attrId )
	{
		case RP_PHY_CURRENT_CHANNEL:
			result = RpSetAttr_phyCurrentChannel( wk8 );
			break;

		case RP_PHY_CHANNELS_SUPPORTED_PAGE:
			result = RpSetAttr_phyChannelsSupportedPage( wk8 );
			break;

		case RP_PHY_CCA_VTH:
			result = RpSetAttr_phyCcaVth( wk16 );
			break;

		case RP_PHY_TRANSMIT_POWER:
			result = RpSetAttr_phyTransmitPower( wk8 );
			break;

		case RP_PHY_FSK_FEC_RX_ENA:
			result = RpSetAttr_phyFskFecRxEna( wk8 );
			break;

		case RP_PHY_FSK_FEC_TX_ENA:
			result = RpSetAttr_phyFskFecTxEna( wk8 );
			break;

		case RP_PHY_FSK_FEC_SCHEME:
			result = RpSetAttr_phyFskFecScheme( wk8 );
			break;

		case RP_PHY_LVLFLTR_VTH:
			result = RpSetAttr_phyLvlFltrVth( wk16 );
			break;

		case RP_PHY_BACKOFF_SEED:
			result = RpSetAttr_phyBackOffSeed( wk8 );
			break;

		case RP_PHY_CRCERROR_UPMSG:
			result = RpSetAttr_phyCrcErrorUpMsg( wk8 );
			break;

		case RP_MAC_ADDRESS_FILTER1_ENA:
			result = RpSetAttr_macAddressFilter1Ena( wk8 );
			break;

		case RP_MAC_SHORT_ADDRESS1:
			result = RpSetAttr_macShortAddress1( wk16 );
			break;

		case RP_MAC_EXTENDED_ADDRESS1:
			result = RpSetAttr_macExtendedAddress1( p_attrValue );
			break;

		case RP_MAC_PANID1:
			result = RpSetAttr_macPanId1( wk16 );
			break;

		case RP_MAC_PAN_COORD1:
			result = RpSetAttr_macPanCoord1( wk8 );
			break;

		case RP_MAC_FRAME_PEND1:
			result = RpSetAttr_macFramePend1( wk8 );
			break;

		case RP_MAC_ADDRESS_FILTER2_ENA:
			result = RpSetAttr_macAddressFilter2Ena( wk8 );
			break;

		case RP_MAC_SHORT_ADDRESS2:
			result = RpSetAttr_macShortAddress2( wk16 );
			break;

		case RP_MAC_EXTENDED_ADDRESS2:
			result = RpSetAttr_macExtendedAddress2( p_attrValue );
			break;

		case RP_MAC_PANID2:
			result = RpSetAttr_macPanId2( wk16 );
			break;

		case RP_MAC_PAN_COORD2:
			result = RpSetAttr_macPanCoord2( wk8 );
			break;

		case RP_MAC_FRAME_PEND2:
			result = RpSetAttr_macFramePend2( wk8 );
			break;

		case RP_MAC_MAXCSMABACKOFF:
			result = RpSetAttr_macMaxCsmaBackOff( wk8 );
			break;

		case RP_MAC_MINBE:
			result = RpSetAttr_macMinBe( wk8 );
			break;

		case RP_MAC_MAXBE:
			result = RpSetAttr_macMaxBe( wk8 );
			break;

		case RP_MAC_MAX_FRAME_RETRIES:
			result = RpSetAttr_macMaxFrameRetries( wk8 );
			break;

		case RP_PHY_CSMA_BACKOFF_PERIOD:
			result = RpSetAttr_macCsmaBackoffPeriod( wk16 );
			break;

		case RP_PHY_CCA_DURATION:
			result = RpSetAttr_phyCcaDuration( wk16 );
			break;

		case RP_PHY_MRFSK_SFD:
			result = RpSetAttr_phyMrFskSfd( wk8 );
			break;

		case RP_PHY_FSK_PREAMBLE_LENGTH:
			result = RpSetAttr_phyFskPreambleLength( wk16 );
			break;

		case RP_PHY_FSK_SCRAMBLE_PSDU:
			result = RpSetAttr_phyFskScramblePsdu( wk8 );
			break;

		case RP_PHY_FSK_OPE_MODE:
			result = RpSetAttr_phyFskOpeMode( wk8 );
			break;

		case RP_PHY_FCS_LENGTH:
			result = RpSetAttr_phyFcsLength( wk8 );
			break;

		case RP_PHY_ACK_REPLY_TIME:
			result = RpSetAttr_phyAckReplyTime( wk16 );
			break;

		case RP_PHY_ACK_WAIT_DURATION:
			result = RpSetAttr_phyAckWaitDuration( wk16 );
			break;

		case RP_PHY_PROFILE_SPECIFIC_MODE:
			result = RpSetAttr_phyProfileSpecificMode( wk8 );
			break;

		case RP_PHY_ANTENNA_SWITCH_ENA:
			result = RpSetAttr_phyAntennaSwitchEna( wk8 );
			break;

		case RP_PHY_ANTENNA_DIVERSITY_RX_ENA:
			result = RpSetAttr_phyAntennaDiversityRxEna( wk8 );
			break;

		case RP_PHY_ANTENNA_SELECT_TX:
			result = RpSetAttr_phyAntennaSelectTx( wk8 );
			break;

		case RP_PHY_ANTENNA_SELECT_ACKTX:
			result = RpSetAttr_phyAntennaSelectAckTx( wk8 );
			break;

		case RP_PHY_ANTENNA_SELECT_ACKRX:
			result = RpSetAttr_phyAntennaSelectAckRx( wk8 );
			break;

		case RP_PHY_RX_TIMEOUT_MODE:
			result = RpSetAttr_phyRxTimeoutMode( wk8 );
			break;

		case RP_PHY_FREQ_BAND_ID:
			result = RpSetAttr_phyFreqBandId( wk8 );
			break;

		case RP_PHY_REGULATORY_MODE:
			result = RpSetAttr_phyRegulatoryMode( wk8 );
			break;

		case RP_PHY_PREAMBLE_4BYTE_RX_MODE:
			result = RpSetAttr_phyPreamble4byteRxMode( wk8 );
			break;

		case RP_PHY_AGC_START_VTH:
			result = RpSetAttr_phyAgcStartVth( wk16 );
			break;

		case RP_PHY_CCA_BANDWIDTH:
			result = RpSetAttr_phyCcaBandwidth( wk8 );
			break;

		case RP_PHY_ED_BANDWIDTH:
			result = RpSetAttr_phyEdBandwidth( wk8 );
			break;

		case RP_PHY_ANTENNA_DIVERSITY_START_VTH:
			result = RpSetAttr_phyAntennaDiversityStartVth( wk16 );
			break;

		case RP_PHY_ANTENNA_SWITCHING_TIME:
			result = RpSetAttr_phyAntennaSwitchingTime( wk16 );
			break;

		case RP_PHY_SFD_DETECTION_EXTEND:
			result = RpSetAttr_phySfdDetectionExtend( wk8 );
			break;

		case RP_PHY_AGC_WAIT_GAIN_OFFSET:
			result = RpSetAttr_phyAgcWaitGainOffset( wk8 );
			break;

		case RP_PHY_CCA_VTH_OFFSET:
			result = RpSetAttr_phyCcaVthOffset( wk8 );
			break;

		case RP_PHY_ANTENNA_SWITCH_ENA_TIMING:
			result = RpSetAttr_phyAntennaSwitchEnaTiming( wk16 );
			break;

		case RP_PHY_GPIO0_SETTING:
			result = RpSetAttr_phyGpio0Setting( wk8 );
			break;

		case RP_PHY_GPIO3_SETTING:
			result = RpSetAttr_phyGpio3Setting( wk8 );
			break;

		case RP_PHY_RMODE_TON_MAX:
			result = RpSetAttr_phyRmodeTonMax( wk16 );
			break;

		case RP_PHY_RMODE_TOFF_MIN:
			result = RpSetAttr_phyRmodeToffMin( wk16 );
			break;

		case RP_PHY_RMODE_TCUM_SMP_PERIOD:
			result = RpSetAttr_phyRmodeTcumSmpPeriod( wk16 );
			break;

		case RP_PHY_RMODE_TCUM_LIMIT:
			result = RpSetAttr_phyRmodeTcumLimit( wk32 );
			break;

		case RP_PHY_ACK_WITH_CCA:
			result = RpSetAttr_phyAckWithCca( wk8 );
			break;

		case RP_PHY_RSSI_OUTPUT_OFFSET:
			result = RpSetAttr_phyRssiOutputOffset( wk8 );
			break;

#ifdef R_FREQUENCY_OFFSET_ENABLED
		case RP_PHY_FREQUENCY_OFFSET:
			result = RpSetAttr_phyFrequencyOffset( (int32_t)wk32 );
			break;
#endif // #ifdef R_FREQUENCY_OFFSET_ENABLED

		// read only
		case RP_PHY_CURRENT_PAGE:
		case RP_PHY_CHANNELS_SUPPORTED:
		case RP_PHY_DATA_RATE:
		case RP_PHY_RMODE_TCUM:
			break;

		default:
			break;
	}

	/* Enable interrupt */
	#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
	RP_PHY_EI(bkupPsw);
	#else
	RP_PHY_EI();
	#endif

	return( result );
}

/* 3.1.1 */
/***************************************************************************************************************
 * function name  : RpSetAttr_phyCurrentChannel
 * description    : Attribute Value Setting Function
 * parameters     : attrValue...The value to be set for the attribute
 * return value   : result
 **************************************************************************************************************/
static uint8_t RpSetAttr_phyCurrentChannel( uint8_t attrValue )
{
	uint8_t result = RP_SUCCESS;
	uint8_t freqBandId = RpCb.pib.phyFreqBandId;
	uint8_t fskOpeMode = RpCb.pib.phyFskOpeMode;
	uint8_t indexFreqBandId, indexFskOpeMode;
	uint8_t res;
	uint8_t maxChannel;

	/* get indexFreqBnadId */
	if (( RP_PHY_FREQ_BAND_863MHz <= freqBandId )
		&&( freqBandId <= RP_PHY_FREQ_BAND_920MHz ))
	{
		indexFreqBandId = freqBandId - 4;
	}
	else if (( RP_PHY_FREQ_BAND_920MHz_Others <= freqBandId )
		&&( freqBandId <= RP_PHY_FREQ_BAND_921MHz ))
	{
		indexFreqBandId = freqBandId - 8;
	}
	else
	{
		result = RP_INVALID_PARAMETER;	// Safty
	}

	/* check parameter */
	if ( result == RP_SUCCESS )
	{
		indexFskOpeMode = fskOpeMode - 1;
		maxChannel = g_MaxChannelTbl[indexFreqBandId].maxChannel[indexFskOpeMode];
		if ( maxChannel < attrValue )
		{
			result = RP_INVALID_PARAMETER;
		}
#ifndef RP_WISUN_FAN_STACK
		else
		{
			/* check support phyCurrentChannel */
			result = RpCheckChannelsSupported( attrValue );
		}
#endif // #ifndef RP_WISUN_FAN_STACK
	}

	/* set(update) attribute */
	if ( result == RP_SUCCESS )
	{
		res = RpSetChannelVal( attrValue );
		if ( res != RP_ERROR )
		{
			RpCb.pib.phyCurrentChannel = attrValue;
		}
		else
		{
			result = RP_INVALID_PARAMETER;	// Safty
		}
	}

	return( result );
}

#ifndef RP_WISUN_FAN_STACK
/***************************************************************************************************************
 * function name  : RpCheckChannelsSupported
 * description    : 
 * parameters     : channel..
 *                : maskChSupported...
 * return value   : retStatus
 **************************************************************************************************************/
static uint8_t RpCheckChannelsSupported( uint8_t channel )
{
	uint8_t result = RP_SUCCESS;
	uint8_t channelsSupportedPage[2];
	uint8_t channel8bit;
	uint32_t channel32bit;
	uint32_t channelsSupported[2];

	channelsSupportedPage[0] = RpCb.pib.phyChannelsSupportedPage;

	channelsSupportedPage[1] = RpConvertChannelsSupportedPage( channel );
	RpSetAttr_phyChannelsSupportedPage( channelsSupportedPage[1] );
	RpMemcpy( &(channelsSupported[0]), RpCb.pib.phyChannelsSupported, sizeof(RpCb.pib.phyChannelsSupported) );

	RpSetAttr_phyChannelsSupportedPage( channelsSupportedPage[0] );

	channel8bit = channel - (channelsSupportedPage[1] * 64);

	/* ChannelNumber bit 0-31 */
	if ( channel8bit < 32 )
	{
		channel32bit = (uint32_t)1 << channel8bit;
		channelsSupported[0] &= channel32bit;
		if ( channelsSupported[0] == 0 )
		{
			result = RP_UNSUPPORTED_CHANNEL;
		}
	}
	/* ChannelNumber bit 32-63 */
	else
	{
		channel8bit -= 32;
		channel32bit = (uint32_t)1 << channel8bit;
		channelsSupported[1] &= channel32bit;
		if ( channelsSupported[1] == 0 )
		{
			result = RP_UNSUPPORTED_CHANNEL;
		}
	}

	return( result );
}

/***************************************************************************************************************
 * function name  : RpConvertChannelsSupportedPage
 * description    : 
 * parameters     : channel..
 * return value   : retStatus
 **************************************************************************************************************/
static uint8_t RpConvertChannelsSupportedPage( uint8_t channel )
{
	uint8_t channelsSupportedPage;

	/* channel 0-63 */
	if ( channel < 64 )
	{
		channelsSupportedPage = 0x00;
	}
	else
	{
		/* channel 64-127 */
		if ( channel < 128 )
		{
			channelsSupportedPage = 0x01;
		}
		else
		{
			/* channel 128-191 */
			if ( channel < 192 )
			{
				channelsSupportedPage = 0x02;
			}
			/* channel 192-255 */
			else
			{
				channelsSupportedPage = 0x03;
			}
		}
	}

	return( channelsSupportedPage );
}
#endif // #ifndef RP_WISUN_FAN_STACK

/* 3.1.4 */
/***************************************************************************************************************
 * function name  : RpSetAttr_phyChannelsSupportedPage
 * description    : Attribute Value Setting Function
 * parameters     : attrValue...The value to be set for the attribute
 * return value   : result
 **************************************************************************************************************/
static uint8_t RpSetAttr_phyChannelsSupportedPage( uint8_t attrValue )
{
	uint8_t result = RP_SUCCESS;

	/* check parameter */
	switch ( attrValue )
	{
		case RP_PHY_CHANNELS_SUPPORTED_INDEX_0:	//0x00: ch0..ch63
		case RP_PHY_CHANNELS_SUPPORTED_INDEX_1:	//0x01: ch64..ch127
		case RP_PHY_CHANNELS_SUPPORTED_INDEX_2:	//0x02: ch128..ch191
		case RP_PHY_CHANNELS_SUPPORTED_INDEX_3:	//0x03: ch192..ch255
			break;
		default:
			result = RP_INVALID_PARAMETER;
			break;
	}

	/* set(update) attribute */
	if ( result == RP_SUCCESS )
	{
		RpCb.pib.phyChannelsSupportedPage = attrValue;
		RpSetChannelsSupportedPageAndVal();
	}

	return( result );
}

/* 3.1.5 */
/***************************************************************************************************************
 * function name  : RpSetAttr_phyCcaVth
 * description    : Attribute Value Setting Function
 * parameters     : attrValue...The value to be set for the attribute
 * return value   : result
 **************************************************************************************************************/
static uint8_t RpSetAttr_phyCcaVth( uint16_t attrValue )
{
	uint8_t result = RP_SUCCESS;

	/* check parameter */
	if (( attrValue < RP_MINVAL_CCAVTH )
		||( RP_MAXVAL_CCAVTH < attrValue ))
	{
		result = RP_INVALID_PARAMETER;
	}

	/* set(update) attribute */
	if ( result == RP_SUCCESS )
	{
		RpCb.pib.phyCcaVth = attrValue;
		RpSetCcaVthVal();
	}

	return( result );
}

/* 3.1.6 */
/***************************************************************************************************************
 * function name  : RpSetAttr_phyTransmitPower
 * description    : Attribute Value Setting Function
 * parameters     : attrValue...The value to be set for the attribute
 * return value   : result
 **************************************************************************************************************/
static uint8_t RpSetAttr_phyTransmitPower( uint8_t attrValue )
{
	uint8_t result = RP_SUCCESS;
	uint8_t maxGainSet;
	uint8_t res;

	/* get maxGain */
	switch ( RpCb.pib.phyFreqBandId )
	{
		case RP_PHY_FREQ_BAND_920MHz:			// (9) 920 - 928 (Japan)
		case RP_PHY_FREQ_BAND_920MHz_Others:	// (14)	920 - 928 (Japan) other settings
		case RP_PHY_FREQ_BAND_921MHz:			// (17)	921 - xxx (CH)
			maxGainSet = 101;
			break;
		case RP_PHY_FREQ_BAND_863MHz:			// (4) 863 - 870 (Europe)
		case RP_PHY_FREQ_BAND_870MHz:			// (15)	870 - xxx (Europe2)
			maxGainSet = 102;
			break;
		case RP_PHY_FREQ_BAND_917MHz:			// (8) 917 - 923.5 (Korea)
			maxGainSet = 102;
			break;
		case RP_PHY_FREQ_BAND_896MHz:			// (5) 896 - 901 (US FCC Part 90)
		case RP_PHY_FREQ_BAND_901MHz:			// (6) 901 - 902 (US FCC Part 24)
		case RP_PHY_FREQ_BAND_915MHz:			// (7) 902 - 928 (US)
		case RP_PHY_FREQ_BAND_902MHz:			// (16)	902 - xxx (PH/MY/AZ)
			maxGainSet = 104;
			break;
		default:
			maxGainSet = 0;	//Safty
			break;
	}

	/* check parameter */
	if ( maxGainSet == 0 )
	{
		result = RP_INVALID_PARAMETER;	// Safty
	}
	else
	{
		if ( maxGainSet < attrValue )
		{
			result = RP_INVALID_PARAMETER;
		}
	}

	/* set(update) attribute */
	if ( result == RP_SUCCESS )
	{
		res = RpSetTxPowerVal( attrValue );
		if ( res != RP_ERROR )
		{
			RpCb.pib.phyTransmitPower = attrValue;
		}
		else
		{
			result = RP_INVALID_PARAMETER;	// Safty
		}
	}

	return( result );
}

/* 3.1.7 */
/***************************************************************************************************************
 * function name  : RpSetAttr_phyFskFecRxEna
 * description    : Attribute Value Setting Function
 * parameters     : attrValue...The value to be set for the attribute
 * return value   : result
 **************************************************************************************************************/
static uint8_t RpSetAttr_phyFskFecRxEna( uint8_t attrValue )
{
	uint8_t result = RP_SUCCESS;

	/* check parameter */
	switch ( attrValue )
	{
		case RP_FEC_RX_MODE_DISABLE: 		//0x00
		case RP_FEC_RX_MODE_ENABLE:			//0x01
		case RP_FEC_RX_MODE_AUTO_DISTINCT:	//0x02
			break;
		default:
			result = RP_INVALID_PARAMETER;
			break;
	}

	/* set(update) attribute */
	if ( result == RP_SUCCESS )
	{
		RpCb.pib.phyFskFecRxEna = attrValue;
		RpSetFecVal();
		RpSetFskOpeModeVal( RP_FALSE );
		RpSetAgcStartVth( RP_TRUE );
		RpSetSfdDetectionExtend();
	}

	return( result );
}

/* 3.1.8 */
/***************************************************************************************************************
 * function name  : RpSetAttr_phyFskFecTxEna
 * description    : Attribute Value Setting Function
 * parameters     : attrValue...The value to be set for the attribute
 * return value   : result
 **************************************************************************************************************/
static uint8_t RpSetAttr_phyFskFecTxEna( uint8_t attrValue )
{
	uint8_t result = RP_SUCCESS;

	/* check parameter */
	switch ( attrValue )
	{
		case 0x00:
		case 0x01:
			break;
		default:
			result = RP_INVALID_PARAMETER;
			break;
	}

	/* set(update) attribute */
	if ( result == RP_SUCCESS )
	{
		RpCb.pib.phyFskFecTxEna = attrValue;
		RpSetFecVal();
		RpSetFskOpeModeVal( RP_FALSE );
		RpSetSfdDetectionExtend();
	}

	return( result );
}

/* 3.1.9 */
/***************************************************************************************************************
 * function name  : RpSetAttr_phyFskFecScheme
 * description    : Attribute Value Setting Function
 * parameters     : attrValue...The value to be set for the attribute
 * return value   : result
 **************************************************************************************************************/
static uint8_t RpSetAttr_phyFskFecScheme( uint8_t attrValue )
{
	uint8_t result = RP_SUCCESS;

	/* check parameter */
	switch ( attrValue )
	{
		case 0x00:	//NRNSC
		case 0x01:	//RSC
			break;
		default:
			result = RP_INVALID_PARAMETER;
			break;
	}

	/* set(update) attribute */
	if ( result == RP_SUCCESS )
	{
		RpCb.pib.phyFskFecScheme = attrValue;
		RpSetFecVal();
	}

	return( result );
}

/* 3.1.10 */
/***************************************************************************************************************
 * function name  : RpSetAttr_phyLvlFltrVth
 * description    : Attribute Value Setting Function
 * parameters     : attrValue...The value to be set for the attribute
 * return value   : result
 **************************************************************************************************************/
static uint8_t RpSetAttr_phyLvlFltrVth( uint16_t attrValue )
{
	uint8_t result = RP_SUCCESS;

	/* check parameter */
	if ( 0x01FF < attrValue )
	{
		result = RP_INVALID_PARAMETER;
	}

	/* set(update) attribute */
	if ( result == RP_SUCCESS )
	{
		RpCb.pib.phyLvlFltrVth = attrValue;
		RpSetLvlVthVal();
	}

	return( result );
}

/* 3.1.11 */
/***************************************************************************************************************
 * function name  : RpSetAttr_phyBackOffSeed
 * description    : Attribute Value Setting Function
 * parameters     : attrValue...The value to be set for the attribute
 * return value   : result
 **************************************************************************************************************/
static uint8_t RpSetAttr_phyBackOffSeed( uint8_t attrValue )
{
	uint8_t result = RP_SUCCESS;

	/* check parameter */
	if ( attrValue < RP_MinValue_phyBackOffSeed )
	{
		result = RP_INVALID_PARAMETER;
	}

	/* set(update) attribute */
	if ( result == RP_SUCCESS )
	{
		RpCb.pib.phyBackOffSeed = attrValue;
		RpRegWrite(BBBOFFPROD, (uint8_t)(BOFFPRODDIS)); // Backoff periode Auto disable
		RpSetBackOffSeedVal();
	}

	return( result );
}

/* 3.1.12 */
/***************************************************************************************************************
 * function name  : RpSetAttr_phyCrcErrorUpMsg
 * description    : Attribute Value Setting Function
 * parameters     : attrValue...The value to be set for the attribute
 * return value   : result
 **************************************************************************************************************/
static uint8_t RpSetAttr_phyCrcErrorUpMsg( uint8_t attrValue )
{
	uint8_t result = RP_SUCCESS;

	/* check parameter */
	switch ( attrValue )
	{
		case 0x00:
		case 0x01:
			break;
		default:
			result = RP_INVALID_PARAMETER;
			break;
	}

	/* set(update) attribute */
	if ( result == RP_SUCCESS )
	{
		RpCb.pib.phyCrcErrorUpMsg = RP_RX_CRCERROR_IGNORE;
		if ( attrValue )
		{
			RpCb.pib.phyCrcErrorUpMsg = RP_RX_CRCERROR_UPMSG;
		}
	}

	return( result );
}

/* 3.1.13 */
/***************************************************************************************************************
 * function name  : RpSetAttr_macAddressFilter1Ena
 * description    : Attribute Value Setting Function
 * parameters     : attrValue...The value to be set for the attribute
 * return value   : result
 **************************************************************************************************************/
static uint8_t RpSetAttr_macAddressFilter1Ena( uint8_t attrValue )
{
	uint8_t result = RP_SUCCESS;

	/* check parameter */
	switch ( attrValue )
	{
		case 0x00:
		case 0x01:
			break;
		default:
			result = RP_INVALID_PARAMETER;
			break;
	}

	/* set(update) attribute */
	if ( result == RP_SUCCESS )
	{
		RpCb.pib.macAddressFilter1Ena = attrValue;
		if ( attrValue )
		{
			RpSetAckReplyTimeVal();
		}
		RpSetAdrfAndAutoAckVal();
	}

	return( result );
}

/* 3.1.14 */
/***************************************************************************************************************
 * function name  : RpSetAttr_macShortAddress1
 * description    : Attribute Value Setting Function
 * parameters     : attrValue...The value to be set for the attribute
 * return value   : result
 **************************************************************************************************************/
static uint8_t RpSetAttr_macShortAddress1( uint16_t attrValue )
{
	uint8_t result = RP_SUCCESS;

	/* set(update) attribute */
	RpCb.pib.macShortAddr1 = attrValue;
	RpSetAdrfAndAutoAckVal();

	return( result );
}

/* 3.1.15 */
/***************************************************************************************************************
 * function name  : RpSetAttr_macExtendedAddress1
 * description    : Attribute Value Setting Function
 * parameters     : p_attrValue...The value to be set for the attribute
 * return value   : result
 **************************************************************************************************************/
static uint8_t RpSetAttr_macExtendedAddress1( void RP_FAR *p_attrValue )
{
	uint8_t result = RP_SUCCESS;

	/* check parameter */
	if ( p_attrValue == RP_NULL )
	{
		result = RP_INVALID_PARAMETER;
	}

	/* set(update) attribute */
	RpMemcpy( RpCb.pib.macExtendedAddress1, p_attrValue, sizeof(RpCb.pib.macExtendedAddress1) );
	RpSetAdrfAndAutoAckVal();

	return( result );
}

/* 3.1.16 */
/***************************************************************************************************************
 * function name  : RpSetAttr_macPanId1
 * description    : Attribute Value Setting Function
 * parameters     : attrValue...The value to be set for the attribute
 * return value   : result
 **************************************************************************************************************/
static uint8_t RpSetAttr_macPanId1( uint16_t attrValue )
{
	uint8_t result = RP_SUCCESS;

	/* set(update) attribute */
	RpCb.pib.macPanId1 = attrValue;
	RpSetAdrfAndAutoAckVal();

	return( result );
}

/* 3.1.17 */
/***************************************************************************************************************
 * function name  : RpSetAttr_macPanCoord1
 * description    : Attribute Value Setting Function
 * parameters     : attrValue...The value to be set for the attribute
 * return value   : result
 **************************************************************************************************************/
static uint8_t RpSetAttr_macPanCoord1( uint8_t attrValue )
{
	uint8_t result = RP_SUCCESS;

	/* check parameter */
	switch ( attrValue )
	{
		case 0x00:
		case 0x01:
			break;
		default:
			result = RP_INVALID_PARAMETER;
			break;
	}

	/* set(update) attribute */
	if ( result == RP_SUCCESS )
	{
		RpCb.pib.macPanCoord1 = attrValue;
		RpSetAdrfAndAutoAckVal();
	}

	return( result );
}

/* 3.1.18 */
/***************************************************************************************************************
 * function name  : RpSetAttr_macFramePend1
 * description    : Attribute Value Setting Function
 * parameters     : attrValue...The value to be set for the attribute
 * return value   : result
 **************************************************************************************************************/
static uint8_t RpSetAttr_macFramePend1( uint8_t attrValue )
{
	uint8_t result = RP_SUCCESS;

	/* check parameter */
	switch ( attrValue )
	{
		case 0x00: //ACK pending bit = 0
		case 0x01: //ACK pending bit = 1
			break;
		default:
			result = RP_INVALID_PARAMETER;
			break;
	}

	/* set(update) attribute */
	if ( result == RP_SUCCESS )
	{
		RpCb.pib.macPendBit1 = attrValue;
		RpSetAdrfAndAutoAckVal();
	}

	return( result );
}

/* 3.1.19 */
/***************************************************************************************************************
 * function name  : RpSetAttr_macAddressFilter2Ena
 * description    : Attribute Value Setting Function
 * parameters     : attrValue...The value to be set for the attribute
 * return value   : result
 **************************************************************************************************************/
static uint8_t RpSetAttr_macAddressFilter2Ena( uint8_t attrValue )
{
	uint8_t result = RP_SUCCESS;

	/* check parameter */
	switch ( attrValue )
	{
		case 0x00:
		case 0x01:
			break;
		default:
			result = RP_INVALID_PARAMETER;
			break;
	}

	/* set(update) attribute */
	if ( result == RP_SUCCESS )
	{
		RpCb.pib.macAddressFilter2Ena = attrValue;
		if ( attrValue )
		{
			RpSetAckReplyTimeVal();
		}
		RpSetAdrfAndAutoAckVal();
	}

	return( result );
}

/* 3.1.20 */
/***************************************************************************************************************
 * function name  : RpSetAttr_macShortAddress2
 * description    : Attribute Value Setting Function
 * parameters     : attrValue...The value to be set for the attribute
 * return value   : result
 **************************************************************************************************************/
static uint8_t RpSetAttr_macShortAddress2( uint16_t attrValue )
{
	uint8_t result = RP_SUCCESS;

	/* set(update) attribute */
	RpCb.pib.macShortAddr2 = attrValue;
	RpSetAdrfAndAutoAckVal();

	return( result );
}

/* 3.1.21 */
/***************************************************************************************************************
 * function name  : RpSetAttr_macExtendedAddress2
 * description    : Attribute Value Setting Function
 * parameters     : p_attrValue...The value to be set for the attribute
 * return value   : result
 **************************************************************************************************************/
static uint8_t RpSetAttr_macExtendedAddress2( void RP_FAR *p_attrValue )
{
	uint8_t result = RP_SUCCESS;

	/* check parameter */
	if ( p_attrValue == RP_NULL )
	{
		result = RP_INVALID_PARAMETER;
	}

	/* set(update) attribute */
	if ( result == RP_SUCCESS )
	{
		RpMemcpy( RpCb.pib.macExtendedAddress2, p_attrValue, sizeof(RpCb.pib.macExtendedAddress2) );
		RpSetAdrfAndAutoAckVal();
	}

	return( result );
}

/* 3.1.22 */
/***************************************************************************************************************
 * function name  : RpSetAttr_macPanId2
 * description    : Attribute Value Setting Function
 * parameters     : attrValue...The value to be set for the attribute
 * return value   : result
 **************************************************************************************************************/
static uint8_t RpSetAttr_macPanId2( uint16_t attrValue )
{
	uint8_t result = RP_SUCCESS;

	/* set(update) attribute */
	RpCb.pib.macPanId2 = attrValue;
	RpSetAdrfAndAutoAckVal();

	return( result );
}

/* 3.1.23 */
/***************************************************************************************************************
 * function name  : RpSetAttr_macPanCoord2
 * description    : Attribute Value Setting Function
 * parameters     : attrValue...The value to be set for the attribute
 * return value   : result
 **************************************************************************************************************/
static uint8_t RpSetAttr_macPanCoord2( uint8_t attrValue )
{
	uint8_t result = RP_SUCCESS;

	/* check parameter */
	switch ( attrValue )
	{
		case 0x00:
		case 0x01:
			break;
		default:
			result = RP_INVALID_PARAMETER;
			break;
	}

	/* set(update) attribute */
	if ( result == RP_SUCCESS )
	{
		RpCb.pib.macPanCoord2 = attrValue;
		RpSetAdrfAndAutoAckVal();
	}

	return( result );
}

/* 3.1.24 */
/***************************************************************************************************************
 * function name  : RpSetAttr_macFramePend2
 * description    : Attribute Value Setting Function
 * parameters     : attrValue...The value to be set for the attribute
 * return value   : result
 **************************************************************************************************************/
static uint8_t RpSetAttr_macFramePend2( uint8_t attrValue )
{
	uint8_t result = RP_SUCCESS;

	/* check parameter */
	switch ( attrValue )
	{
		case 0x00: //ACK pending bit = 0
		case 0x01: //ACK pending bit = 1
			break;
		default:
			result = RP_INVALID_PARAMETER;
			break;
	}

	/* set(update) attribute */
	if ( result == RP_SUCCESS )
	{
		RpCb.pib.macPendBit2 = attrValue;
		RpSetAdrfAndAutoAckVal();
	}

	return( result );
}

/* 3.1.25 */
/***************************************************************************************************************
 * function name  : RpSetAttr_macMaxCsmaBackOff
 * description    : Attribute Value Setting Function
 * parameters     : attrValue...The value to be set for the attribute
 * return value   : result
 **************************************************************************************************************/
static uint8_t RpSetAttr_macMaxCsmaBackOff( uint8_t attrValue )
{
	uint8_t result = RP_SUCCESS;

	/* check parameter */
	if ( RP_MAXVAL_CSMABACKOFF < attrValue )
	{
		result = RP_INVALID_PARAMETER;
	}

	/* set(update) attribute */
	if ( result == RP_SUCCESS )
	{
		RpCb.pib.macMaxCsmaBackOff = attrValue;
		RpSetMaxCsmaBackoffVal();
	}

	return( result );
}

/* 3.1.26 */
/***************************************************************************************************************
 * function name  : RpSetAttr_macMinBe
 * description    : Attribute Value Setting Function
 * parameters     : attrValue...The value to be set for the attribute
 * return value   : result
 **************************************************************************************************************/
static uint8_t RpSetAttr_macMinBe( uint8_t attrValue )
{
	uint8_t result = RP_SUCCESS;
	uint8_t wk8;

	/* check parameter */
	if ( RP_MAXVAL_MINBE < attrValue )
	{
		result = RP_INVALID_PARAMETER;
	}

	/* set(update) attribute */
	if ( result == RP_SUCCESS )
	{
		RpCb.pib.macMinBe = attrValue;
		wk8 = RpRegRead(BBCSMACON3);
		wk8 &= ~BEMIN;
		wk8 |= RpCb.pib.macMinBe & BEMIN;
		RpRegWrite( BBCSMACON3, wk8 );
	}

	return( result );
}

/* 3.1.27 */
/***************************************************************************************************************
 * function name  : RpSetAttr_macMaxBe
 * description    : Attribute Value Setting Function
 * parameters     : attrValue...The value to be set for the attribute
 * return value   : result
 **************************************************************************************************************/
static uint8_t RpSetAttr_macMaxBe( uint8_t attrValue )
{
	uint8_t result = RP_SUCCESS;

	/* check parameter */
	if (( attrValue < RP_MinValue_macMaxBe )
		||( RP_MAXVAL_MAXBE < attrValue ))
	{
		result = RP_INVALID_PARAMETER;
	}

	/* set(update) attribute */
	if ( result == RP_SUCCESS )
	{
		RpCb.pib.macMaxBe = attrValue;
		RpSetMaxBeVal();
	}

	return( result );
}

/* 3.1.28 */
/***************************************************************************************************************
 * function name  : RpSetAttr_macMaxFrameRetries
 * description    : Attribute Value Setting Function
 * parameters     : attrValue...The value to be set for the attribute
 * return value   : result
 **************************************************************************************************************/
static uint8_t RpSetAttr_macMaxFrameRetries( uint8_t attrValue )
{
	uint8_t result = RP_SUCCESS;

	/* check parameter */
	if ( RP_MAXVAL_FRAME_RETRIES < attrValue )
	{
		result = RP_INVALID_PARAMETER;
	}

	/* set(update) attribute */
	if ( result == RP_SUCCESS )
	{
		RpCb.pib.macMaxFrameRetries = attrValue;
	}

	return( result );
}

/* 3.1.29 */
/***************************************************************************************************************
 * function name  : RpSetAttr_macCsmaBackoffPeriod
 * description    : Attribute Value Setting Function
 * parameters     : attrValue...The value to be set for the attribute
 * return value   : result
 **************************************************************************************************************/
static uint8_t RpSetAttr_macCsmaBackoffPeriod( uint16_t attrValue )
{
	uint8_t result = RP_SUCCESS;

	/* check parameter */
	if ( attrValue < RP_MINVAL_BACKOFF_PERIOD )
	{
		result = RP_INVALID_PARAMETER;
	}

	/* set(update) attribute */
	if ( result == RP_SUCCESS )
	{
		RpCb.pib.phyCsmaBackoffPeriod = attrValue;
		RpSetCsmaBackoffPeriod();
	}

	return( result );
}

/* 3.1.30 */
/***************************************************************************************************************
 * function name  : RpSetAttr_phyCcaDuration
 * description    : Attribute Value Setting Function
 * parameters     : attrValue...The value to be set for the attribute
 * return value   : result
 **************************************************************************************************************/
static uint8_t RpSetAttr_phyCcaDuration( uint16_t attrValue )
{
	uint8_t result = RP_SUCCESS;

	/* check parameter */
	if (( attrValue < RP_MINVAL_CCADURATION ) || (RP_MAXVAL_CCADURATION < attrValue))
	{
		result = RP_INVALID_PARAMETER;
	}

	/* set(update) attribute */
	if ( result == RP_SUCCESS )
	{
		RpCb.pib.phyCcaDuration = attrValue;
		RpSetCcaDurationVal( RP_TRUE );
	}

	return( result );
}

/* 3.1.31 */
/***************************************************************************************************************
 * function name  : RpSetAttr_phyMrFskSfd
 * description    : Attribute Value Setting Function
 * parameters     : attrValue...The value to be set for the attribute
 * return value   : result
 **************************************************************************************************************/
static uint8_t RpSetAttr_phyMrFskSfd( uint8_t attrValue )
{
	uint8_t result = RP_SUCCESS;

	/* check parameter */
	switch ( attrValue )
	{
		case 0x00:
		case 0x01:
			break;
		default:
			result = RP_INVALID_PARAMETER;
			break;
	}

	/* set(update) attribute */
	if ( result == RP_SUCCESS )
	{
		RpCb.pib.phyMrFskSfd = attrValue;
		RpSetMrFskSfdVal();
		RpSetSfdDetectionExtend();
	}

	return( result );
}

/* 3.1.32 */
/***************************************************************************************************************
 * function name  : RpSetAttr_phyFskPreambleLength
 * description    : Attribute Value Setting Function
 * parameters     : attrValue...The value to be set for the attribute
 * return value   : result
 **************************************************************************************************************/
static uint8_t RpSetAttr_phyFskPreambleLength( uint16_t attrValue )
{
	uint8_t result = RP_SUCCESS;

	/* check parameter */
	if (( attrValue < RP_MINVAL_PREAMBLELEN )
		||( RP_MAXVAL_PREAMBLELEN < attrValue ))
	{
		result = RP_INVALID_PARAMETER;
	}

	/* set(update) attribute */
	if ( result == RP_SUCCESS )
	{
		RpCb.pib.phyFskPreambleLength = attrValue;
		RpSetPreambleLengthVal();
	}

	return( result );
}

/* 3.1.33 */
/***************************************************************************************************************
 * function name  : RpSetAttr_phyFskScramblePsdu
 * description    : Attribute Value Setting Function
 * parameters     : attrValue...The value to be set for the attribute
 * return value   : result
 **************************************************************************************************************/
static uint8_t RpSetAttr_phyFskScramblePsdu( uint8_t attrValue )
{
	uint8_t result = RP_SUCCESS;

	/* check parameter */
	switch ( attrValue )
	{
		case 0x00:
		case 0x01:
			break;
		default:
			result = RP_INVALID_PARAMETER;
			break;
	}

	/* set(update) attribute */
	if ( result == RP_SUCCESS )
	{
		RpCb.pib.phyFskScramblePsdu = attrValue;
		RpSetFskScramblePsduVal();
	}

	return( result );
}

/* 3.1.34 */
/***************************************************************************************************************
 * function name  : RpSetAttr_phyFskOpeMode
 * description    : Attribute Value Setting Function
 * parameters     : attrValue...The value to be set for the attribute
 * return value   : result
 **************************************************************************************************************/
static uint8_t RpSetAttr_phyFskOpeMode( uint8_t attrValue )
{
	uint8_t result = RP_SUCCESS;
	uint8_t freqBandId;
	uint8_t indexFreqBandId;
	uint8_t maxFskOpeMode;

	/* get indexFreqBandId */
	freqBandId = RpCb.pib.phyFreqBandId;
	switch ( freqBandId )
	{
		case RP_PHY_FREQ_BAND_863MHz:			// (4) 863 - 870 (Europe)
		case RP_PHY_FREQ_BAND_896MHz:			// (5) 896 - 901 (US FCC Part 90)
		case RP_PHY_FREQ_BAND_901MHz:			// (6) 901 - 902 (US FCC Part 24)
		case RP_PHY_FREQ_BAND_915MHz:			// (7) 902 - 928 (US)
		case RP_PHY_FREQ_BAND_917MHz:			// (8) 917 - 923.5 (Korea)
		case RP_PHY_FREQ_BAND_920MHz:			// (9) 920 - 928 (Japan)
			indexFreqBandId = freqBandId - 4;
			break;
		//
		case RP_PHY_FREQ_BAND_920MHz_Others:	// (14)	920 - 928 (Japan) other settings
		case RP_PHY_FREQ_BAND_870MHz:			// (15)	870 - xxx (Europe2)
		case RP_PHY_FREQ_BAND_902MHz:			// (16)	902 - xxx (PH/MY/AZ)
		case RP_PHY_FREQ_BAND_921MHz:			// (17)	921 - xxx (CH)
			indexFreqBandId = freqBandId - 8;
			break;
		//
		default:
			result = RP_INVALID_PARAMETER;	// Safty
			break;
	}

	/* check parameter */
	if ( result == RP_SUCCESS )
	{
		if ( attrValue == 0 )
		{
			result = RP_INVALID_PARAMETER;
		}
		else
		{
			maxFskOpeMode = g_MaxFskOpeModeTbl[indexFreqBandId];
			if ( maxFskOpeMode < attrValue )
			{
				result = RP_INVALID_PARAMETER;
			}
		}
	}

	/* set(update) attribute */
	if ( result == RP_SUCCESS )
	{
		RpCb.pib.phyFskOpeMode = attrValue;
		RpSetFskOpeModeVal( RP_FALSE );
		RpSetAgcStartVth( RP_TRUE );
		RpSetAntennaDiversityStartVth( RP_TRUE );
		RpCb.pib.phyAntennaSwitchingTime = RP_RF_PIB_DFLT_ANTENNA_SWITCHING_TIME;
		RpSetSfdDetectionExtend();
		RpSetAgcWaitGain();
	}

	return( result );
}

/* 3.1.35 */
/***************************************************************************************************************
 * function name  : RpSetAttr_phyFcsLength
 * description    : Attribute Value Setting Function
 * parameters     : attrValue...The value to be set for the attribute
 * return value   : result
 **************************************************************************************************************/
static uint8_t RpSetAttr_phyFcsLength( uint8_t attrValue )
{
	uint8_t result = RP_SUCCESS;

	/* check parameter */
	switch ( attrValue )
	{
		case RP_FCS_LENGTH_16BIT:	//0x02
		case RP_FCS_LENGTH_32BIT:	//0x04
			break;
		default:
			result = RP_INVALID_PARAMETER;
			break;
	}

	/* set(update) attribute */
	if ( result == RP_SUCCESS )
	{
		RpCb.pib.phyFcsLength = attrValue;
		RpSetFcsLengthVal();
	}

	return( result );
}

/* 3.1.36 */
/***************************************************************************************************************
 * function name  : RpSetAttr_phyAckReplyTime
 * description    : Attribute Value Setting Function
 * parameters     : attrValue...The value to be set for the attribute
 * return value   : result
 **************************************************************************************************************/
static uint8_t RpSetAttr_phyAckReplyTime( uint16_t attrValue )
{
	uint8_t result = RP_SUCCESS;

	/* check parameter */
	if ( attrValue < RP_MINVAL_ACKREPLY_TIME )
	{
		result = RP_INVALID_PARAMETER;
	}

	/* set(update) attribute */
	if ( result == RP_SUCCESS )
	{
		RpCb.pib.phyAckReplyTime = attrValue;
		RpSetAckReplyTimeVal();
	}

	return( result );
}

/* 3.1.37 */
/***************************************************************************************************************
 * function name  : RpSetAttr_phyAckWaitDuration
 * description    : Attribute Value Setting Function
 * parameters     : attrValue...The value to be set for the attribute
 * return value   : result
 **************************************************************************************************************/
static uint8_t RpSetAttr_phyAckWaitDuration( uint16_t attrValue )
{
	uint8_t result = RP_SUCCESS;

	/* check parameter */
	if ( attrValue < RP_MINVAL_ACKWAIT_DURATION )
	{
		result = RP_INVALID_PARAMETER;
	}

	/* set(update) attribute */
	if ( result == RP_SUCCESS )
	{
		RpCb.pib.phyAckWaitDuration = attrValue;
		RpSetAckWaitDurationVal();
	}

	return( result );
}

/* 3.1.38 */
/***************************************************************************************************************
 * function name  : RpSetAttr_phyProfileSpecificMode
 * description    : Attribute Value Setting Function
 * parameters     : attrValue...The value to be set for the attribute
 * return value   : result
 **************************************************************************************************************/
static uint8_t RpSetAttr_phyProfileSpecificMode( uint8_t attrValue )
{
	uint8_t result = RP_SUCCESS;

	/* check parameter */
	switch ( attrValue )
	{
		case RP_SPECIFIC_NORMAL: //0x00:
		case RP_SPECIFIC_WSUN:	 //0x01
			break;
		default:
			result = RP_INVALID_PARAMETER;
			break;
	}

	/* set(update) attribute */
	if ( result == RP_SUCCESS )
	{
		RpCb.pib.phyProfileSpecificMode = attrValue;
		RpSetSpecificModeVal( RP_FALSE );
	}

	return( result );
}

/* 3.1.39 */
/***************************************************************************************************************
 * function name  : RpSetAttr_phyAntennaSwitchEna
 * description    : Attribute Value Setting Function
 * parameters     : attrValue...The value to be set for the attribute
 * return value   : result
 **************************************************************************************************************/
static uint8_t RpSetAttr_phyAntennaSwitchEna( uint8_t attrValue )
{
	uint8_t result = RP_SUCCESS;

	/* check parameter */
	switch ( attrValue )
	{
		case 0x00:
		case 0x01:
			break;
		default:
			result = RP_INVALID_PARAMETER;
			break;
	}

	/* set(update) attribute */
	if ( result == RP_SUCCESS )
	{
		RpCb.pib.phyAntennaSwitchEna = attrValue;
		RpSetAntennaSwitchVal();
	}

	return( result );
}

/* 3.1.40 */
/***************************************************************************************************************
 * function name  : RpSetAttr_phyAntennaDiversityRxEna
 * description    : Attribute Value Setting Function
 * parameters     : attrValue...The value to be set for the attribute
 * return value   : result
 **************************************************************************************************************/
static uint8_t RpSetAttr_phyAntennaDiversityRxEna( uint8_t attrValue )
{
	uint8_t result = RP_SUCCESS;

	/* check parameter */
	switch ( attrValue )
	{
		case 0x00:
		case 0x01:
			break;
		default:
			result = RP_INVALID_PARAMETER;
			break;
	}

	/* set(update) attribute */
	if ( result == RP_SUCCESS )
	{
		RpCb.pib.phyAntennaDiversityRxEna = attrValue;
		RpSetAntennaDiversityVal();
		RpSetAntennaSelectTxVal();
		RpSetFskOpeModeVal( RP_FALSE );
	}

	return( result );
}

/* 3.1.41 */
/***************************************************************************************************************
 * function name  : RpSetAttr_phyAntennaSelectTx
 * description    : Attribute Value Setting Function
 * parameters     : attrValue...The value to be set for the attribute
 * return value   : result
 **************************************************************************************************************/
static uint8_t RpSetAttr_phyAntennaSelectTx( uint8_t attrValue )
{
	uint8_t result = RP_SUCCESS;

	/* check parameter */
	switch ( attrValue )
	{
		case 0x00:
		case 0x01:
			break;
		default:
			result = RP_INVALID_PARAMETER;
			break;
	}

	/* set(update) attribute */
	if ( result == RP_SUCCESS )
	{
		RpCb.pib.phyAntennaSelectTx = attrValue;
		RpSetAntennaSelectTxVal();
	}

	return( result );
}

/* 3.1.42 */
/***************************************************************************************************************
 * function name  : RpSetAttr_phyAntennaSelectAckTx
 * description    : Attribute Value Setting Function
 * parameters     : attrValue...The value to be set for the attribute
 * return value   : result
 **************************************************************************************************************/
static uint8_t RpSetAttr_phyAntennaSelectAckTx( uint8_t attrValue )
{
	uint8_t result = RP_SUCCESS;

	/* check parameter */
	switch ( attrValue )
	{
		case RP_ANT_SEL_ACK_TX_ANT0:			//0x00
		case RP_ANT_SEL_ACK_TX_ANT1:			//0x01
		case RP_ANT_SEL_ACK_TX_RCVD_ANT:		//0x02
		case RP_ANT_SEL_ACK_TX_RCVD_INV_ANT:	//0x03
			break;
		default:
			result = RP_INVALID_PARAMETER;
			break;
	}

	/* set(update) attribute */
	if ( result == RP_SUCCESS )
	{
		RpCb.pib.phyAntennaSelectAckTx = attrValue;
		RpSetAntennaDiversityVal();
	}

	return( result );
}

/* 3.1.43 */
/***************************************************************************************************************
 * function name  : RpSetAttr_phyAntennaSelectAckRx
 * description    : Attribute Value Setting Function
 * parameters     : attrValue...The value to be set for the attribute
 * return value   : result
 **************************************************************************************************************/
static uint8_t RpSetAttr_phyAntennaSelectAckRx( uint8_t attrValue )
{
	uint8_t result = RP_SUCCESS;

	/* check parameter */
	switch ( attrValue )
	{
		case RP_ANT_SEL_ACK_RX_ANT0: //0x00:
		case RP_ANT_SEL_ACK_RX_ANT1: //0x01:
		case RP_ANT_SEL_ACK_RX_AUTO: //0x02:
			break;
		default:
			result = RP_INVALID_PARAMETER;
			break;
	}

	/* set(update) attribute */
	if ( result == RP_SUCCESS )
	{
		RpCb.pib.phyAntennaSelectAckRx = attrValue;
		RpSetAntennaDiversityVal();
	}

	return( result );
}

/* 3.1.44 */
/***************************************************************************************************************
 * function name  : RpSetAttr_phyRxTimeoutMode
 * description    : Attribute Value Setting Function
 * parameters     : attrValue...The value to be set for the attribute
 * return value   : result
 **************************************************************************************************************/
static uint8_t RpSetAttr_phyRxTimeoutMode( uint8_t attrValue )
{
	uint8_t result = RP_SUCCESS;

	/* check parameter */
	switch ( attrValue )
	{
		case 0x00:
		case 0x01:
			break;
		default:
			result = RP_INVALID_PARAMETER;
			break;
	}

	/* set(update) attribute */
	if ( result == RP_SUCCESS )
	{
		RpCb.pib.phyRxTimeoutMode = attrValue;
	}

	return( result );
}

/* 3.1.45 */
/***************************************************************************************************************
 * function name  : RpSetAttr_phyFreqBandId
 * description    : Attribute Value Setting Function
 * parameters     : attrValue...The value to be set for the attribute
 * return value   : result
 **************************************************************************************************************/
static uint8_t RpSetAttr_phyFreqBandId( uint8_t attrValue )
{
	uint8_t result = RP_SUCCESS;

	/* check parameter */
	switch ( attrValue )
	{
		case RP_PHY_FREQ_BAND_863MHz: //4:863-870   (Europe)
		case RP_PHY_FREQ_BAND_896MHz: //5:896-901   (US FCC Part 90)
		case RP_PHY_FREQ_BAND_901MHz: //6:901-902   (US FCC Part 24)
		case RP_PHY_FREQ_BAND_915MHz: //7:902-928   (US)
		case RP_PHY_FREQ_BAND_917MHz: //8:917-923.5 (Korea)
		case RP_PHY_FREQ_BAND_920MHz: //9:920-928   (Japan)
		//
		case RP_PHY_FREQ_BAND_920MHz_Others: //14:920-928 (Japan) other settings
		case RP_PHY_FREQ_BAND_870MHz:		 //15:870-xxx (Europe2)
		case RP_PHY_FREQ_BAND_902MHz:		 //16:902-xxx (PH/MY/AZ)
		case RP_PHY_FREQ_BAND_921MHz:		 //17:921-xxx (CH)
			break;
		default:
			result = RP_INVALID_PARAMETER;
			break;
	}

	/* set(update) attribute */
	if ( result == RP_SUCCESS )
	{
		RpCb.pib.phyFreqBandId = attrValue;
		RpSetFskOpeModeVal( RP_FALSE );
		RpSetAgcStartVth( RP_TRUE );
		RpSetAntennaDiversityStartVth( RP_TRUE );
		RpCb.pib.phyAntennaSwitchingTime = RP_RF_PIB_DFLT_ANTENNA_SWITCHING_TIME;
		RpSetSfdDetectionExtend();
		RpSetAgcWaitGain();
	}

	return( result );
}

/* 3.1.47 */
/***************************************************************************************************************
 * function name  : RpSetAttr_phyRegulatoryMode
 * description    : Attribute Value Setting Function
 * parameters     : attrValue...The value to be set for the attribute
 * return value   : result
 **************************************************************************************************************/
static uint8_t RpSetAttr_phyRegulatoryMode( uint8_t attrValue )
{
	uint8_t result = RP_SUCCESS;

	/* check parameter */
	switch ( attrValue )
	{
		case RP_RMODE_DISABLE:		//0x00:
		case RP_RMODE_ENABLE:		//0x01:
		case RP_RMODE_ENABLE_ARIB:	//0x02:
			break;
		default:
			result = RP_INVALID_PARAMETER;
			break;
	}

	/* set(update) attribute */
	if ( result == RP_SUCCESS )
	{
		if ( attrValue != RpCb.pib.phyRegulatoryMode )
		{
			RpCb.pib.phyRegulatoryMode = attrValue;
			RpSetMacTxLimitMode();
		}
	}

	return( result );
}

/* 3.1.48 */
/***************************************************************************************************************
 * function name  : RpSetAttr_phyPreamble4byteRxMode
 * description    : Attribute Value Setting Function
 * parameters     : attrValue...The value to be set for the attribute
 * return value   : result
 **************************************************************************************************************/
static uint8_t RpSetAttr_phyPreamble4byteRxMode( uint8_t attrValue )
{
	uint8_t result = RP_SUCCESS;

	/* check parameter */
	switch ( attrValue )
	{
		case 0x00:
		case 0x01:
			break;
		default:
			result = RP_INVALID_PARAMETER;
			break;
	}

	/* set(update) attribute */
	if ( result == RP_SUCCESS )
	{
		RpCb.pib.phyPreamble4ByteRxMode = attrValue;
		RpSetPreamble4ByteRxMode();
	}

	return( result );
}

/* 3.1.49 */
/***************************************************************************************************************
 * function name  : RpSetAttr_phyAgcStartVth
 * description    : Attribute Value Setting Function
 * parameters     : attrValue...The value to be set for the attribute
 * return value   : result
 **************************************************************************************************************/
static uint8_t RpSetAttr_phyAgcStartVth( uint16_t attrValue )
{
	uint8_t result = RP_SUCCESS;

	/* check parameter */
	if ( 0x387D < attrValue )
	{
		result = RP_INVALID_PARAMETER;
	}
	else
	{
		if ( attrValue < 0x000F )
		{
			if ( attrValue != 0x00 )
			{
				result = RP_INVALID_PARAMETER;
			}
		}
	}

	/* set(update) attribute */
	if ( result == RP_SUCCESS )
	{
		RpCb.pib.phyAgcStartVth = attrValue;
		RpSetAgcStartVth( RP_FALSE );
	}

	return( result );
}

/* 3.1.50 */
/***************************************************************************************************************
 * function name  : RpSetAttr_phyCcaBandwidth
 * description    : Attribute Value Setting Function
 * parameters     : attrValue...The value to be set for the attribute
 * return value   : result
 **************************************************************************************************************/
static uint8_t RpSetAttr_phyCcaBandwidth( uint8_t attrValue )
{
	uint8_t result = RP_SUCCESS;
	uint8_t ccaBandwidth = RpCb.pib.phyCcaBandwidth;
	uint8_t edBandwidth = RpCb.pib.phyEdBandwidth;
	uint8_t freqId = RpCb.freqIdIndex;

	/* check parameter */
	switch ( attrValue )
	{
		case 0x00:
			break;
		case 0x01:
		case 0x02:
			switch ( ccaBandwidth )
			{
				case 0x03:
					result = RP_INVALID_PARAMETER;
					break;
				default:
					break;
			}
			switch ( edBandwidth )
			{
				case 0x03:
					result = RP_INVALID_PARAMETER;
					break;
				default:
					break;
			}
			break;
		case 0x03:
			switch ( ccaBandwidth )
			{
				case 0x01:
				case 0x02:
					result = RP_INVALID_PARAMETER;
					break;
				default:
					break;
			}
			switch ( edBandwidth )
			{
				case 0x01:
				case 0x02:
					result = RP_INVALID_PARAMETER;
					break;
				default:
					break;
			}
			switch ( freqId )
			{
				case 0:
				case 1:
				case 2:
				case 18:
				case 19:
					result = RP_INVALID_PARAMETER;
					break;
				default:
					break;		
			}
			break;
		default:
			result = RP_INVALID_PARAMETER;
			break;
	}

	/* set(update) attribute */
	if ( result == RP_SUCCESS )
	{
		RpCb.pib.phyCcaBandwidth = attrValue;
		RpSetCcaEdBandwidth();
		RpSetCcaVthVal();
	}

	return( result );
}

/* 3.1.51 */
/***************************************************************************************************************
 * function name  : RpSetAttr_phyEdBandwidth
 * description    : Attribute Value Setting Function
 * parameters     : attrValue...The value to be set for the attribute
 * return value   : result
 **************************************************************************************************************/
static uint8_t RpSetAttr_phyEdBandwidth( uint8_t attrValue )
{
	uint8_t result = RP_SUCCESS;
	uint8_t edBandwidth = RpCb.pib.phyEdBandwidth;
	uint8_t ccaBandwidth = RpCb.pib.phyCcaBandwidth;
	uint8_t freqId = RpCb.freqIdIndex;

	/* check parameter */
	switch ( attrValue )
	{
		case 0x00:
			break;
		case 0x01:
		case 0x02:
			switch ( edBandwidth )
			{
				case 0x03:
					result = RP_INVALID_PARAMETER;
					break;
				default:
					break;
			}
			switch ( ccaBandwidth )
			{
				case 0x03:
					result = RP_INVALID_PARAMETER;
					break;
				default:
					break;
			}
			break;
		case 0x03:
			switch ( edBandwidth )
			{
				case 0x01:
				case 0x02:
					result = RP_INVALID_PARAMETER;
					break;
				default:
					break;
			}
			switch ( ccaBandwidth )
			{
				case 0x01:
				case 0x02:
					result = RP_INVALID_PARAMETER;
					break;
				default:
					break;
			}
			switch ( freqId )
			{
				case 0:
				case 1:
				case 2:
				case 18:
				case 19:
					result = RP_INVALID_PARAMETER;
					break;
				default:
					break;		
			}
			break;
		default:
			result = RP_INVALID_PARAMETER;
			break;
	}

	/* set(update) attribute */
	if ( result == RP_SUCCESS )
	{
		RpCb.pib.phyEdBandwidth = attrValue;
		RpSetCcaEdBandwidth();
	}

	return( result );
}

/* 3.1.52 */
/***************************************************************************************************************
 * function name  : RpSetAttr_phyAntennaDiversityStartVth
 * description    : Attribute Value Setting Function
 * parameters     : attrValue...The value to be set for the attribute
 * return value   : result
 **************************************************************************************************************/
static uint8_t RpSetAttr_phyAntennaDiversityStartVth( uint16_t attrValue )
{
	uint8_t result = RP_SUCCESS;

	/* check parameter */
	if ( 0x01FF < attrValue )
	{
		result = RP_INVALID_PARAMETER;
	}

	/* set(update) attribute */
	if ( result == RP_SUCCESS )
	{
		RpCb.pib.phyAntennaDiversityStartVth = attrValue;
		RpSetAntennaDiversityStartVth( RP_FALSE );
	}

	return( result );
}

/* 3.1.53 */
/***************************************************************************************************************
 * function name  : RpSetAttr_phyAntennaSwitchingTime
 * description    : Attribute Value Setting Function
 * parameters     : attrValue...The value to be set for the attribute
 * return value   : result
 **************************************************************************************************************/
static uint8_t RpSetAttr_phyAntennaSwitchingTime( uint16_t attrValue )
{
	uint8_t result = RP_SUCCESS;

	/* set(update) attribute */
	RpCb.pib.phyAntennaSwitchingTime = attrValue;

	return( result );
}

/* 3.1.54 */
/***************************************************************************************************************
 * function name  : RpSetAttr_phySfdDetectionExtend
 * description    : Attribute Value Setting Function
 * parameters     : attrValue...The value to be set for the attribute
 * return value   : result
 **************************************************************************************************************/
static uint8_t RpSetAttr_phySfdDetectionExtend( uint8_t attrValue )
{
	uint8_t result = RP_SUCCESS;

	/* check parameter */
	switch ( attrValue )
	{
		case 0x00:
		case 0x01:
			break;
		default:
			result = RP_INVALID_PARAMETER;
			break;
	}

	/* set(update) attribute */
	if ( result == RP_SUCCESS )
	{
		RpCb.pib.phySfdDetectionExtend = attrValue;
		RpSetSfdDetectionExtend();
	}

	return( result );
}

/* 3.1.55 */
/***************************************************************************************************************
 * function name  : RpSetAttr_phyAgcWaitGainOffset
 * description    : Attribute Value Setting Function
 * parameters     : attrValue...The value to be set for the attribute
 * return value   : result
 **************************************************************************************************************/
static uint8_t RpSetAttr_phyAgcWaitGainOffset( uint8_t attrValue )
{
	uint8_t result = RP_SUCCESS;

	/* check parameter */
	if ( RP_MAXVAL_AGC_WAIT_GAIN_OFFSET < attrValue )
	{
		result = RP_INVALID_PARAMETER;
	}
	
	/* set(update) attribute */
	if ( result == RP_SUCCESS )
	{
		RpCb.pib.phyAgcWaitGainOffset = attrValue;
		RpSetAgcWaitGain();
	}

	return( result );
}

/* 3.1.56 */
/***************************************************************************************************************
 * function name  : RpSetAttr_phyCcaVthOffset
 * description    : Attribute Value Setting Function
 * parameters     : attrValue...The value to be set for the attribute
 * return value   : result
 **************************************************************************************************************/
static uint8_t RpSetAttr_phyCcaVthOffset( uint8_t attrValue )
{
	uint8_t result = RP_SUCCESS;

	/* check parameter */
	if ( RP_MAXVAL_CCA_VTH_OFFSET < attrValue )
	{
		result = RP_INVALID_PARAMETER;
	}

	/* set(update) attribute */
	if ( result == RP_SUCCESS )
	{
		RpCb.pib.phyCcaVthOffset = attrValue;
		RpSetCcaVthVal();
	}

	return( result );
}

/* 3.1.57 */
/***************************************************************************************************************
 * function name  : RpSetAttr_phyAntennaSwitchEnaTiming
 * description    : Attribute Value Setting Function
 * parameters     : attrValue...The value to be set for the attribute
 * return value   : result
 **************************************************************************************************************/
static uint8_t RpSetAttr_phyAntennaSwitchEnaTiming( uint16_t attrValue )
{
	uint8_t result = RP_SUCCESS;

	/* check parameter */
	if (( attrValue < RP_MINVAL_ANTSWENA_TIMING )
		||( RP_MAXVAL_ANTSWENA_TIMING < attrValue ))
	{
		result = RP_INVALID_PARAMETER;
	}

	/* set(update) attribute */
	if ( result == RP_SUCCESS )
	{
		RpCb.pib.phyAntennaSwitchEnaTiming = attrValue;
		RpSetAntennaSwitchEnaTiming();
	}

	return( result );
}

/* 3.1.58 */
/***************************************************************************************************************
 * function name  : RpSetAttr_phyGpio0Setting
 * description    : Attribute Value Setting Function
 * parameters     : attrValue...The value to be set for the attribute
 * return value   : result
 **************************************************************************************************************/
static uint8_t RpSetAttr_phyGpio0Setting( uint8_t attrValue )
{
	uint8_t result = RP_SUCCESS;

	/* check parameter */
	switch ( attrValue )
	{
		case 0x00:
		case 0x01:
			break;
		default:
			result = RP_INVALID_PARAMETER;
			break;
	}

	/* set(update) attribute */
	if ( result == RP_SUCCESS )
	{
		RpCb.pib.phyGpio0Setting = attrValue;
		RpSetGpio0Setting();
	}

	return( result );
}

/* 3.1.59 */
/***************************************************************************************************************
 * function name  : RpSetAttr_phyGpio3Setting
 * description    : Attribute Value Setting Function
 * parameters     : attrValue...The value to be set for the attribute
 * return value   : result
 **************************************************************************************************************/
static uint8_t RpSetAttr_phyGpio3Setting( uint8_t attrValue )
{
	uint8_t result = RP_SUCCESS;

	/* check parameter */
	switch ( attrValue )
	{
		case 0x00:
		case 0x01:
			break;
		default:
			result = RP_INVALID_PARAMETER;
			break;
	}

	/* set(update) attribute */
	if ( result == RP_SUCCESS )
	{
		RpCb.pib.phyGpio3Setting = attrValue;
		RpSetGpio3Setting();
	}

	return( result );
}

/* 3.1.60 */
/***************************************************************************************************************
 * function name  : RpSetAttr_phyRmodeTonMax
 * description    : Attribute Value Setting Function
 * parameters     : attrValue...The value to be set for the attribute
 * return value   : result
 **************************************************************************************************************/
static uint8_t RpSetAttr_phyRmodeTonMax( uint16_t attrValue )
{
	uint8_t result = RP_SUCCESS;

	/* check parameter */
	if ( RP_MAXVAL_RMODE_TON_MAX < attrValue )
	{
		result = RP_INVALID_PARAMETER;
	}

	/* set(update) attribute */
	if ( result == RP_SUCCESS )
	{
		RpCb.pib.phyRmodeTonMax = attrValue;
	}

	return( result );
}

/* 3.1.61 */
/***************************************************************************************************************
 * function name  : RpSetAttr_phyRmodeToffMin
 * description    : Attribute Value Setting Function
 * parameters     : attrValue...The value to be set for the attribute
 * return value   : result
 **************************************************************************************************************/
static uint8_t RpSetAttr_phyRmodeToffMin( uint16_t attrValue )
{
	uint8_t result = RP_SUCCESS;

	/* check parameter */
	if ( RP_MAXVAL_RMODE_TOFF_MIN < attrValue )
	{
		result = RP_INVALID_PARAMETER;
	}

	/* set(update) attribute */
	if ( result == RP_SUCCESS )
	{
		RpCb.pib.phyRmodeToffMin = attrValue;
	}

	return( result );
}

/* 3.1.62 */
/***************************************************************************************************************
 * function name  : RpSetAttr_phyRmodeTcumSmpPeriod
 * description    : Attribute Value Setting Function
 * parameters     : attrValue...The value to be set for the attribute
 * return value   : result
 **************************************************************************************************************/
static uint8_t RpSetAttr_phyRmodeTcumSmpPeriod( uint16_t attrValue )
{
	uint8_t result = RP_SUCCESS;

	/* check parameter */
	if (( attrValue < RP_MINVAL_RMODE_TCUM_SMP_PERIOD )
		||( RP_MAXVAL_RMODE_TCUM_SMP_PERIOD < attrValue ))
	{
		result = RP_INVALID_PARAMETER;
	}

	/* set(update) attribute */
	if ( result == RP_SUCCESS )
	{
		if ( attrValue != RpCb.pib.phyRmodeTcumSmpPeriod )
		{
			RpCb.pib.phyRmodeTcumSmpPeriod = attrValue;
			if ( RpCb.pib.phyRmodeTcumSmpPeriod <= RpCb.txLmtTimer.secCnt )
			{
				RpCb.txLmtTimer.secCnt = RpCb.pib.phyRmodeTcumSmpPeriod - 1;
				RpCb.txLmtTimer.msecCnt = 0;
			}
			if ( RpCb.pib.phyRegulatoryMode == RP_RMODE_ENABLE_ARIB )
			{
				RpSetMacTxLimitMode();
			}
		}
	}

	return( result );
}

/* 3.1.63 */
/***************************************************************************************************************
 * function name  : RpSetAttr_phyRmodeTcumLimit
 * description    : Attribute Value Setting Function
 * parameters     : attrValue...The value to be set for the attribute
 * return value   : result
 **************************************************************************************************************/
static uint8_t RpSetAttr_phyRmodeTcumLimit( uint32_t attrValue )
{
	uint8_t result = RP_SUCCESS;

	/* set(update) attribute */
	if ( attrValue != RpCb.pib.phyRmodeTcumLimit )
	{
		RpCb.pib.phyRmodeTcumLimit = attrValue;
		if ( RpCb.pib.phyRegulatoryMode == RP_RMODE_ENABLE )
		{
			RpSetMacTxLimitMode();
		}
	}

	return( result );
}

/* 3.1.65 */
/***************************************************************************************************************
 * function name  : RpSetAttr_phyAckWithCca
 * description    : Attribute Value Setting Function
 * parameters     : attrValue...The value to be set for the attribute
 * return value   : result
 **************************************************************************************************************/
uint8_t RpSetAttr_phyAckWithCca( uint8_t attrValue )
{
	uint8_t result = RP_SUCCESS;

	/* check parameter */
	switch ( attrValue )
	{
		case 0x00:
		case 0x01:
			break;
		default:
			result = RP_INVALID_PARAMETER;
			break;
	}

	/* set(update) attribute */
	if ( result == RP_SUCCESS )
	{
		RpCb.pib.phyAckWithCca = attrValue;
	}

	return( result );
}

/* 3.1.66 */
/***************************************************************************************************************
 * function name  : RpSetAttr_phyRssiOutputOffset
 * description    : Attribute Value Setting Function
 * parameters     : attrValue...The value to be set for the attribute
 * return value   : result
 **************************************************************************************************************/
static uint8_t RpSetAttr_phyRssiOutputOffset( uint8_t attrValue )
{
	uint8_t result = RP_SUCCESS;

	/* check parameter */
	if ( RP_MAXVAL_RSSI_OUTPUT_OFFSET < attrValue )
	{
		result = RP_INVALID_PARAMETER;
	}

	/* set(update) attribute */
	if ( result == RP_SUCCESS )
	{
		RpCb.pib.phyRssiOutputOffset = attrValue;
	}

	return( result );
}

/* 3.1.67 */
/***************************************************************************************************************
 * function name  : RpSetAttr_phyFrequencyOffset
 * description    : Attribute Value Setting Function
 * parameters     : attrValue...The value to be set for the attribute
 * return value   : result
 **************************************************************************************************************/
#ifdef R_FREQUENCY_OFFSET_ENABLED
uint8_t RpSetAttr_phyFrequencyOffset( int32_t attrValue )
{
	uint8_t result = RP_SUCCESS;

	/* set(update) attribute */
	RpCb.pib.phyFrequencyOffset = attrValue;

	return( result );
}
#endif // #ifdef R_FREQUENCY_OFFSET_ENABLED

/***************************************************************************************************************
 * function name  : RpGetPibReq
 * description    : Attribute Value Acquisition Function
 * parameters     : id...Attribute ID
 *                : valLen...Size of the area storing the attribute value (byte length)
 *                : pVal...Pointer to the beginning of the area storing the attribute value
 * return value   : RP_SUCCESS, RP_UNSUPPORTED_ATTRIBUTE, RP_INVALID_PARAMETER, RP_BUSY_RX, RP_BUSY_TX
 *				  : RP_BUSY_LOWPOWER
 **************************************************************************************************************/
int16_t RpGetPibReq( uint8_t id, uint8_t valLen, void *pVal )
{
	uint16_t status;
	int16_t rtn = RpExtChkIdLenGetReq(id, valLen, pVal);

	#if defined(__RX)
	uint32_t bkupPsw;
	#elif defined(__CCRL__) || defined(__ICCRL78__)
	uint8_t  bkupPsw;
	#elif defined(__arm)
	uint8_t bkupPsw[RP_INT_BKUP_BUF_SIZE];
	#endif

	/* Disable interrupt */
	#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
	RP_PHY_DI(bkupPsw);
	#else
	RP_PHY_DI();
	#endif

	/* API function execution Log */
	RpLog_Event( RP_LOG_API_GETIB, id );

	if (rtn != RP_SUCCESS)
	{
		/* API function execution Log */
		RpLog_Event( RP_LOG_API_GETIB | RP_LOG_API_RETERR, (uint8_t)rtn );

		/* Enable interrupt */
		#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
		RP_PHY_EI(bkupPsw);
		#else
		RP_PHY_EI();
		#endif

		return (rtn);
	}

	status = RpCb.status;

	if (status & RP_PHY_STAT_LOWPOWER)
	{
		/* API function execution Log */
		RpLog_Event( RP_LOG_API_SETIB | RP_LOG_API_RETERR, RP_BUSY_LOWPOWER );

		/* Enable interrupt */
		#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
		RP_PHY_EI(bkupPsw);
		#else
		RP_PHY_EI();
		#endif

		return (RP_BUSY_LOWPOWER);
	}
	else if ((status & RP_PHY_STAT_TRX_OFF) == 0)
	{
		if (status & RP_PHY_STAT_TX)
		{
			/* API function execution Log */
			RpLog_Event( RP_LOG_API_GETIB | RP_LOG_API_RETERR, RP_BUSY_TX );

			/* Enable interrupt */
			#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
			RP_PHY_EI(bkupPsw);
			#else
			RP_PHY_EI();
			#endif

			return (RP_BUSY_TX);
		}
		else
		{
			/* API function execution Log */
			RpLog_Event( RP_LOG_API_GETIB | RP_LOG_API_RETERR, RP_BUSY_RX );

			/* Enable interrupt */
			#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
			RP_PHY_EI(bkupPsw);
			#else
			RP_PHY_EI();
			#endif

			return (RP_BUSY_RX);
		}
	}
	RpPlmeGetReq(id, pVal);

	/* API function execution Log */
	RpLog_Event( RP_LOG_API_GETIB | RP_LOG_API_RET, RP_SUCCESS );

	/* Enable interrupt */
	#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
	RP_PHY_EI(bkupPsw);
	#else
	RP_PHY_EI();
	#endif

	return (RP_SUCCESS);
}

/***************************************************************************************************************
 * function name  : RpPlmeGetReq
 * description    : Attribute Value Acquisition Function
 * parameters     : id...Attribute ID
 *                : pVal...Pointer to the beginning of the area storing the attribute value
 * return value   : none
 **************************************************************************************************************/
#if !defined(RP_WISUN_FAN_STACK) && !defined(RP_ST_ENV)
static void RpPlmeGetReq( uint8_t id, void *pVal )
#else
void RpPlmeGetReq( uint8_t id, void *pVal )
#endif // #if !defined(RP_WISUN_FAN_STACK) && !defined(RP_ST_ENV)
{
	uint8_t *pValU8;

	pValU8 = (uint8_t *)pVal;

	switch (id)
	{
		case RP_PHY_CHANNELS_SUPPORTED:
			RpMemcpy(pValU8, RpCb.pib.phyChannelsSupported, sizeof(RpCb.pib.phyChannelsSupported));
			break;

		case RP_PHY_CURRENT_CHANNEL:
			*pValU8 = RpCb.pib.phyCurrentChannel;
			break;

		case RP_PHY_TRANSMIT_POWER:
			*pValU8 = RpCb.pib.phyTransmitPower;
			break;

		case RP_PHY_CHANNELS_SUPPORTED_PAGE:
			*pValU8 = RpCb.pib.phyChannelsSupportedPage;
			break;

		case RP_PHY_CURRENT_PAGE:
			*pValU8 = RP_RF_PIB_DFLT_CURRENT_PAGE;
			break;

		case RP_PHY_FSK_FEC_TX_ENA:
			*pValU8 = RpCb.pib.phyFskFecTxEna;
			break;

		case RP_PHY_FSK_FEC_RX_ENA:
			*pValU8 = RpCb.pib.phyFskFecRxEna;
			break;

		case RP_PHY_FSK_FEC_SCHEME:
			*pValU8 = RpCb.pib.phyFskFecScheme;
			break;

		case RP_MAC_ADDRESS_FILTER1_ENA:
			*pValU8 = RpCb.pib.macAddressFilter1Ena;
			break;

		case RP_MAC_PANID1:
			RP_VAL_UINT16_TO_ARRAY(RpCb.pib.macPanId1, pValU8);
			break;

		case RP_MAC_SHORT_ADDRESS1:
			RP_VAL_UINT16_TO_ARRAY(RpCb.pib.macShortAddr1, pValU8);
			break;

		case RP_MAC_EXTENDED_ADDRESS1:
			RpMemcpy(pValU8, RpCb.pib.macExtendedAddress1, sizeof(RpCb.pib.macExtendedAddress1));
			break;

		case RP_MAC_PAN_COORD1:
			*pValU8 = RpCb.pib.macPanCoord1;
			break;

		case RP_MAC_FRAME_PEND1:
			*pValU8 = RpCb.pib.macPendBit1;
			break;

		case RP_MAC_ADDRESS_FILTER2_ENA:
			*pValU8 = RpCb.pib.macAddressFilter2Ena;
			break;

		case RP_MAC_PANID2:
			RP_VAL_UINT16_TO_ARRAY(RpCb.pib.macPanId2, pValU8);
			break;

		case RP_MAC_SHORT_ADDRESS2:
			RP_VAL_UINT16_TO_ARRAY(RpCb.pib.macShortAddr2, pValU8);
			break;

		case RP_MAC_EXTENDED_ADDRESS2:
			RpMemcpy(pValU8, RpCb.pib.macExtendedAddress2, sizeof(RpCb.pib.macExtendedAddress2));
			break;

		case RP_MAC_PAN_COORD2:
			*pValU8 = RpCb.pib.macPanCoord2;
			break;

		case RP_MAC_FRAME_PEND2:
			*pValU8 = RpCb.pib.macPendBit2;
			break;

		case RP_MAC_MAXCSMABACKOFF:
			*pValU8 = RpCb.pib.macMaxCsmaBackOff;
			break;

		case RP_MAC_MINBE:
			*pValU8 = RpCb.pib.macMinBe;
			break;

		case RP_MAC_MAXBE:
			*pValU8 = RpCb.pib.macMaxBe;
			break;

		case RP_MAC_MAX_FRAME_RETRIES:
			*pValU8 = RpCb.pib.macMaxFrameRetries;
			break;

		case RP_PHY_CRCERROR_UPMSG:
			*pValU8 = RpCb.pib.phyCrcErrorUpMsg;
			break;

		case RP_PHY_CCA_VTH:
			RP_VAL_UINT16_TO_ARRAY(RpCb.pib.phyCcaVth, pValU8);
			break;

		case RP_PHY_LVLFLTR_VTH:
			RP_VAL_UINT16_TO_ARRAY(RpCb.pib.phyLvlFltrVth, pValU8);
			break;

		case RP_PHY_BACKOFF_SEED:
			*pValU8 = RpCb.pib.phyBackOffSeed;
			break;

		case RP_PHY_CCA_DURATION:
			RP_VAL_UINT16_TO_ARRAY(RpCb.pib.phyCcaDuration, pValU8);
			break;

		case RP_PHY_FSK_PREAMBLE_LENGTH:
			RP_VAL_UINT16_TO_ARRAY(RpCb.pib.phyFskPreambleLength, pValU8);
			break;

		case RP_PHY_MRFSK_SFD:
			*pValU8 = RpCb.pib.phyMrFskSfd;
			break;

		case RP_PHY_FSK_SCRAMBLE_PSDU:
			*pValU8 = RpCb.pib.phyFskScramblePsdu;
			break;

		case RP_PHY_FSK_OPE_MODE:
			*pValU8 = RpCb.pib.phyFskOpeMode;
			break;

		case RP_PHY_FCS_LENGTH:
			*pValU8 = RpCb.pib.phyFcsLength;
			break;

		case RP_PHY_ACK_REPLY_TIME:
			RP_VAL_UINT16_TO_ARRAY(RpCb.pib.phyAckReplyTime, pValU8);
			break;

		case RP_PHY_ACK_WAIT_DURATION:
			RP_VAL_UINT16_TO_ARRAY(RpCb.pib.phyAckWaitDuration, pValU8);
			break;

		case RP_PHY_PROFILE_SPECIFIC_MODE:
			*pValU8 = RpCb.pib.phyProfileSpecificMode;
			break;

		case RP_PHY_ANTENNA_SWITCH_ENA:
			*pValU8 = RpCb.pib.phyAntennaSwitchEna;
			break;

		case RP_PHY_ANTENNA_DIVERSITY_RX_ENA:
			*pValU8 = RpCb.pib.phyAntennaDiversityRxEna;
			break;

		case RP_PHY_ANTENNA_SELECT_TX:
			*pValU8 = RpCb.pib.phyAntennaSelectTx;
			break;

		case RP_PHY_ANTENNA_SELECT_ACKTX:
			*pValU8 = RpCb.pib.phyAntennaSelectAckTx;
			break;

		case RP_PHY_ANTENNA_SELECT_ACKRX:
			*pValU8 = RpCb.pib.phyAntennaSelectAckRx;
			break;

		case RP_PHY_RX_TIMEOUT_MODE:
			*pValU8 = RpCb.pib.phyRxTimeoutMode;
			break;

		case RP_PHY_FREQ_BAND_ID:
			*pValU8 = RpCb.pib.phyFreqBandId;
			break;

		case RP_PHY_DATA_RATE:
			RP_VAL_UINT16_TO_ARRAY(RpCb.pib.phyDataRate, pValU8);
			break;

		case RP_PHY_REGULATORY_MODE:
			*pValU8 = RpCb.pib.phyRegulatoryMode;
			break;

		case RP_PHY_CSMA_BACKOFF_PERIOD:
			RP_VAL_UINT16_TO_ARRAY(RpCb.pib.phyCsmaBackoffPeriod, pValU8);
			break;

		case RP_PHY_PREAMBLE_4BYTE_RX_MODE:
			*pValU8 = RpCb.pib.phyPreamble4ByteRxMode;
			break;

		case RP_PHY_AGC_START_VTH:
			RpCb.pib.phyAgcStartVth = RpGetAgcStartVth();
			RP_VAL_UINT16_TO_ARRAY(RpCb.pib.phyAgcStartVth, pValU8);
			break;

		case RP_PHY_CCA_BANDWIDTH:
			*pValU8 = RpCb.pib.phyCcaBandwidth;
			break;

		case RP_PHY_ED_BANDWIDTH:
			*pValU8 = RpCb.pib.phyEdBandwidth;
			break;

		case RP_PHY_ANTENNA_DIVERSITY_START_VTH:
			RpCb.pib.phyAntennaDiversityStartVth = RpGetAntennaDiversityStartVth();
			RP_VAL_UINT16_TO_ARRAY(RpCb.pib.phyAntennaDiversityStartVth, pValU8);
			break;

		case RP_PHY_ANTENNA_SWITCHING_TIME:
			RpCb.pib.phyAntennaSwitchingTime = RpGetAntennaSwitchingTime();
			RP_VAL_UINT16_TO_ARRAY(RpCb.pib.phyAntennaSwitchingTime, pValU8);
			break;

		case RP_PHY_SFD_DETECTION_EXTEND:
			*pValU8 = RpCb.pib.phySfdDetectionExtend;
			break;

		case RP_PHY_AGC_WAIT_GAIN_OFFSET:
			*pValU8 = RpCb.pib.phyAgcWaitGainOffset;
			break;

		case RP_PHY_CCA_VTH_OFFSET:
			*pValU8 = RpCb.pib.phyCcaVthOffset;
			break;

		case RP_PHY_ANTENNA_SWITCH_ENA_TIMING:
			RP_VAL_UINT16_TO_ARRAY(RpCb.pib.phyAntennaSwitchEnaTiming, pValU8);
			break;

		case RP_PHY_GPIO0_SETTING:
			*pValU8 = RpCb.pib.phyGpio0Setting;
			break;

		case RP_PHY_GPIO3_SETTING:
			*pValU8 = RpCb.pib.phyGpio3Setting;
			break;

		case RP_PHY_RMODE_TON_MAX:
			RP_VAL_UINT16_TO_ARRAY(RpCb.pib.phyRmodeTonMax, pValU8);
			break;

		case RP_PHY_RMODE_TOFF_MIN:
			RP_VAL_UINT16_TO_ARRAY(RpCb.pib.phyRmodeToffMin, pValU8);
			break;

		case RP_PHY_RMODE_TCUM_SMP_PERIOD:
			RP_VAL_UINT16_TO_ARRAY(RpCb.pib.phyRmodeTcumSmpPeriod, pValU8);
			break;

		case RP_PHY_RMODE_TCUM_LIMIT:
			RP_VAL_UINT32_TO_ARRAY(RpCb.pib.phyRmodeTcumLimit, pValU8);
			break;

		case RP_PHY_RMODE_TCUM:
			RP_VAL_UINT32_TO_ARRAY(RpCb.pib.phyRmodeTcum, pValU8);
			break;

		case RP_PHY_ACK_WITH_CCA:
			*pValU8 = RpCb.pib.phyAckWithCca;
			break;
		case RP_PHY_RSSI_OUTPUT_OFFSET:
			*pValU8 = RpCb.pib.phyRssiOutputOffset;
			break;

#ifdef R_FREQUENCY_OFFSET_ENABLED
		case RP_PHY_FREQUENCY:
			RP_VAL_UINT32_TO_ARRAY( RpCb.pib.phyFrequency, pValU8 );
			break;

		case RP_PHY_FREQUENCY_OFFSET:
			RP_VAL_UINT32_TO_ARRAY( RpCb.pib.phyFrequencyOffset, pValU8 );
			break;
#endif // #ifdef R_FREQUENCY_OFFSET_ENABLED

		default:
			break;
	}
}

/***********************************************************************************************************************
 * Copyright (C) 2014-2021 Renesas Electronics Corporation.
 **********************************************************************************************************************/
