/***********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products.
 * No other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all 
 * applicable laws, including copyright laws. 
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING THIS SOFTWARE, WHETHER EXPRESS, 
 * IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE 
 * AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
 * TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS 
 * AFFILIATED COMPANIES SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR 
 * ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of 
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the 
 * following link:
 * http://www.renesas.com/disclaimer 
 **********************************************************************************************************************/
/***********************************************************************************************************************
 * file name	: phy_drv.c
 * description	: This is the RF driver's driver code.
 ***********************************************************************************************************************
 * Copyright (C) 2014-2021 Renesas Electronics Corporation.
 **********************************************************************************************************************/
/***************************************************************************************************************
 * pragma
 **************************************************************************************************************/
#if 0
#pragma NOP
#pragma STOP
#pragma HALT
#pragma EI
#pragma DI
#endif

/***************************************************************************************************************
 * includes
 **************************************************************************************************************/

#if defined (__CCRL__)
#define EI()  __EI()
#define NOP() __nop()
#elif defined(__ICCRL78__)
#include <intrinsics.h>
#define EI()  __enable_interrupt()
#define NOP() __no_operation()
#else
#define EI()  __EI()
#define NOP() __nop()
#endif


#include <ior5f11fll.h>
#include <ior5f11fll_ext.h>

#include "phy_config.h"

#if defined (__CCRL__)
	#pragma interrupt INTPx_Interrupt (vect=INTP3)
	#if (RP_USR_RF_TAU_CH_SELECT == 1)
		#pragma interrupt INTTM_TXLIMIT_Interrupt (vect=INTTM01)
	#else
		#pragma interrupt INTTM_TXLIMIT_Interrupt (vect=INTTM02)
	#endif
#elif defined (__ICCRL78__)
        #pragma vector = INTP3_vect
        __interrupt void INTPx_Interrupt(void);
        #if (RP_USR_RF_TAU_CH_SELECT == 1)
            #pragma vector = INTTM01_vect
            __interrupt void INTTM_TXLIMIT_Interrupt(void);
        #else
            #pragma vector = INTTM02_vect
            __interrupt void INTTM_TXLIMIT_Interrupt(void);
	#endif
#else /* __CCRL__ */
	#if R_USE_RENESAS_STACK
		#pragma interrupt INTP3 INTPx_Interrupt SP=RpIntHdlStk+320
		#if (RP_USR_RF_TAU_CH_SELECT == 1)
			#pragma interrupt INTTM01 INTTM_TXLIMIT_Interrupt SP=RpIntHdlStk+320
		#else
			#pragma interrupt INTTM02 INTTM_TXLIMIT_Interrupt SP=RpIntHdlStk+320
		#endif
	#else
		#pragma interrupt INTP3 INTPx_Interrupt
		#if (RP_USR_RF_TAU_CH_SELECT == 1)
			#pragma interrupt INTTM01 INTTM_TXLIMIT_Interrupt
		#else
			#pragma interrupt INTTM02 INTTM_TXLIMIT_Interrupt
		#endif
	#endif // #if R_USE_RENESAS_STACK
#endif /* __CCRL__ */

#include <string.h>
#include "hardware_sfr.h"
#include "phy.h"
#include "phy_def.h"
#include "phy_drv.h"

/***************************************************************************************************************
 * extern definitions
 **************************************************************************************************************/
extern const RP_CONFIG_CB RpConfig;

/***************************************************************************************************************
 * macro definitions
 **************************************************************************************************************/
#define RpCSI_CSActivate() (RP_CSI_CS_PORT = RP_PORT_LO)
#define RpCSI_CSDeactivate() (RP_CSI_CS_PORT = RP_PORT_HI)
#define BBTXRXRAMEND	(0x400 << 3)

#if RP_DMA_WRITE_RAM_ENA || RP_DMA_READ_RAM_ENA
	#define DTC_VECTBL_ADDRESS 			0xFFD00
	#define DTC_CTRL0_ADDRESS			0xFFD40
	#define DTC_CTRL1_ADDRESS			0xFFD48

	#define DTC_VECTBL_ADDRESS_SHORT		((uint16_t)DTC_VECTBL_ADDRESS)
	#define DTC_VECTBL_ADDRESS_BARSET	((uint8_t)(DTC_VECTBL_ADDRESS_SHORT >> 8))

	/* dtc_vectortable[16] */
	#define RP_DTC_VECTORTABLE16        *((uint8_t __near *)(DTC_VECTBL_ADDRESS_SHORT + 0x10))  // 0xffd10
	/* dtc_controldata_0 */
	#define RP_DTC_CONTROLDATA0_DTCCR   *((uint8_t __near *)(DTC_VECTBL_ADDRESS_SHORT + 0x40))  // 0xffd40
	#define RP_DTC_CONTROLDATA0_DTBLS   *((uint8_t __near *)(DTC_VECTBL_ADDRESS_SHORT + 0x41))  // 0xffd41
	#define RP_DTC_CONTROLDATA0_DTCCT   *((uint8_t __near *)(DTC_VECTBL_ADDRESS_SHORT + 0x42))  // 0xffd42
	#define RP_DTC_CONTROLDATA0_DTRLD   *((uint8_t __near *)(DTC_VECTBL_ADDRESS_SHORT + 0x43))  // 0xffd43
	#define RP_DTC_CONTROLDATA0_DTSAR   *((uint16_t __near *)(DTC_VECTBL_ADDRESS_SHORT + 0x44)) // 0xffd44
	#define RP_DTC_CONTROLDATA0_DTDAR   *((uint16_t __near *)(DTC_VECTBL_ADDRESS_SHORT + 0x46)) // 0xffd46
	/* dtc_controldata_1 */
	#define RP_DTC_CONTROLDATA1_DTCCR   *((uint8_t __near *)(DTC_VECTBL_ADDRESS_SHORT + 0x48))  // 0xffd48
	#define RP_DTC_CONTROLDATA1_DTBLS   *((uint8_t __near *)(DTC_VECTBL_ADDRESS_SHORT + 0x49))  // 0xffd49
	#define RP_DTC_CONTROLDATA1_DTCCT   *((uint8_t __near *)(DTC_VECTBL_ADDRESS_SHORT + 0x4a))  // 0xffd4a
	#define RP_DTC_CONTROLDATA1_DTRLD   *((uint8_t __near *)(DTC_VECTBL_ADDRESS_SHORT + 0x4b))  // 0xffd4b
	#define RP_DTC_CONTROLDATA1_DTSAR   *((uint16_t __near *)(DTC_VECTBL_ADDRESS_SHORT + 0x4c)) // 0xffd4c
	#define RP_DTC_CONTROLDATA1_DTDAR   *((uint16_t __near *)(DTC_VECTBL_ADDRESS_SHORT + 0x4e)) // 0xffd4e

	// DTC Control Register j (DTCCRj) (j = 0 to 23)
	// Data size selection (SZ)
	#define RP_00_DTC_DATA_SIZE_8BITS             (0x00U) // 8 bits
	#define RP_40_DTC_DATA_SIZE_16BITS            (0x40U) // 16 bits
	// Repeat mode interrupt enable bit (RPTINT)
	#define RP_00_DTC_REPEAT_INT_DISABLE          (0x00U) // disable interrupt generation
	#define RP_20_DTC_REPEAT_INT_ENABLE           (0x20U) // enable interrupt generation
	// Chain transfer enable bit (CHNE)
	#define RP_00_DTC_CHAIN_TRANSFER_DISABLE      (0x00U) // disable chain transfers
	#define RP_10_DTC_CHAIN_TRANSFER_ENABLE       (0x10U) // enable chain transfers
	// Destination address control bit (DAMOD)
	#define RP_00_DTC_DEST_ADDR_FIXED             (0x00U) // destination address fixed
	#define RP_08_DTC_DEST_ADDR_INCREMENTED       (0x08U) // destination address incremented
	// Source address control bit (SAMOD)
	#define RP_00_DTC_SOURCE_ADDR_FIXED           (0x00U) // source address fixed
	#define RP_04_DTC_SOURCE_ADDR_INCREMENTED     (0x04U) // source address incremented
	// Repeat area select bit (RPTSEL)
	#define RP_00_DTC_REPEAT_AREA_DEST            (0x00U) // transfer destination is the repeat area
	#define RP_02_DTC_REPEAT_AREA_SOURCE          (0x02U) // transfer source is the repeat area
	// Transfer mode select bit (MODE)
	#define RP_00_DTC_TRANSFER_MODE_NORMAL        (0x00U) // normal mode
	#define RP_01_DTC_TRANSFER_MODE_REPEAT        (0x01U) // repeat mode

	// DTC Activation Enable Register 2 (DTCEN2)
	// DTC activation enable bit (DTCEN27 - DTCEN20)
	#define RP_00_DTC_UART2T_ACTIVATION_DISABLE   (0x00U) // disable activation (UART2 transmission/CSI20/IIC20 transfer)
	#define RP_80_DTC_UART2T_ACTIVATION_ENABLE    (0x80U) // enable activation (UART2 transmission/CSI20/IIC20 transfer)

	/* --- --- */
	#define RP_FF1E_DTCD0_SRC_ADDRESS             (0xFF1EU)
	#define RP_F500_DTCD0_DEST_ADDRESS            (0xF500U)
	#define RP_04_DTCD0_TRANSFER_BYTE             (0x04U)
	#define RP_02_DTCD0_TRANSFER_BLOCKSIZE        (0x02U)
	#define RP_01_DTCD0_TRANSFER_BYTE             (0x01U)
	#define RP_00_DTCD0_TRANSFER_BYTE             (0x00U)
	#define RP_FF48_DTCD0_DST_ADDRESS             (0xFF48U)
	#define RP_FF48_DTCD1_SRC_ADDRESS             (0xFF48U)
#endif// RP_DMA_WRITE_RAM_ENA || RP_DMA_READ_RAM_ENA

#ifdef R_USE_RENESAS_STACK
	#ifndef RP_WKUPTIMER_USE
		#define RP_WKUPTIMER_USE 1
	#endif
	#if RP_WKUPTIMER_USE == 2
		extern uint32_t RpGetTime( void );
	#endif
#endif	// #ifdef R_USE_RENESAS_STACK

#ifdef R_USE_RENESAS_STACK
	#if RP_WKUPTIMER_USE
		#if RP_WKUPTIMER_USE == 2
			#define TAxIC	bbtim2ic
		#elif RP_WKUPTIMER_USE == 1
			#define TAxS	tstart_tracr
			#define TAxMR	tramr
			#define TAxIC	traic
			#define TAxPRE	trapre
			#define TAx	tra
		#endif
		#define RP_IPL_WUP_TSK		2		// interrupt level of Timer(TAx) (used to invoke RpWupTskHdr)
	#endif
#endif	// #ifdef R_USE_RENESAS_STACK

// MAC Address stored in Flash
#define RP_ADDR_EXTADDR	((uint8_t RP_FAR *)0xf1000)

#if ( (RP_PHY_INTLEVEL != 1) && (RP_PHY_INTLEVEL != 2) )
	#error "error: out of range (RP_PHY_INTLEVEL)"
#endif

// Interrupt Level
#define RP_PHY_INTLEVEL_RFINTP			RP_PHY_INTLEVEL
#define RP_PHY_INTLEVEL_RFTimers		RP_PHY_INTLEVEL
#define RP_PHY_INTLEVEL_DMADone			RP_PHY_INTLEVEL

#define RP_PHY_INTLEVEL_RFINTP_b0		(RP_PHY_INTLEVEL_RFINTP & 0x01)
#define RP_PHY_INTLEVEL_RFINTP_b1		((RP_PHY_INTLEVEL_RFINTP & 0x02) >> 1)
#define RP_PHY_INTLEVEL_RFTimers_b0		(RP_PHY_INTLEVEL_RFTimers & 0x01)
#define RP_PHY_INTLEVEL_RFTimers_b1		((RP_PHY_INTLEVEL_RFTimers & 0x02) >> 1)
#define RP_PHY_INTLEVEL_DMADone_b0		(RP_PHY_INTLEVEL_DMADone & 0x01)
#define RP_PHY_INTLEVEL_DMADone_b1		((RP_PHY_INTLEVEL_DMADone & 0x02) >> 1)

// CSI Registers
#define	RP_CSI_STATUS			SSR10L
#define RP_SAU_UNDER_EXECUTE	(0x40)
#define RP_CSI_IRQ				CSIIF20
#define RP_CSI_BUF				SIO20

#define	RP_CSI_NOP				0xff
#define	RP_CSI_MEM_WR_H(adr)	(adr >> 8)
#define	RP_CSI_MEM_WR_L(adr)	(adr)
#define	RP_CSI_MEM_RD_H(adr)	(adr >> 8)
#define	RP_CSI_MEM_RD_L(adr)	(adr + 0x06)

#define RP_REG_WRITE(wData) RP_CSI_IRQ = 0; \
	                       RP_CSI_BUF = wData;\
						   while(RP_CSI_IRQ == 0)
// Wait Wakeup
#if defined(RP_WAKEUP_MCU_HALT)
	#define RP_WAIT_WAKEUP_ACTIVE()  TMMK00 = 0;\
									TS0 |= TAU_CH0_START_TRG_ON;\
									HALT();\
									while (TMIF00 == 0);\
									TT0 |= TAU_CH0_STOP_TRG_ON;\
									TMMK00 = 1
#else
	#define  RP_WAIT_WAKEUP_ACTIVE() TS0 |= TAU_CH0_START_TRG_ON;\
									while (TMIF00 == 0);\
									TT0 |= TAU_CH0_STOP_TRG_ON
#endif

/***************************************************************************************************************
 * static function prototypes
 **************************************************************************************************************/
static void RpSetMcuIntMaskOnly( uint8_t enable );
static void RpCSIInitPort( void );
static void RpCSIInitialize( void );
static void RpRFTimerInitialize( void );
#if RP_DMA_WRITE_RAM_ENA
	static void RpDmaTerminationWriteSer( void );
#endif // #if RP_DMA_WRITE_RAM_ENA
#if RP_DMA_READ_RAM_ENA
	static void RpDmaTerminationReadSer( void );
#endif // #if RP_DMA_READ_RAM_ENA
#if RP_DMA_WRITE_RAM_ENA || RP_DMA_READ_RAM_ENA
	static void RpDmaSerInitial( void );
	static void RpDmaSerStart( uint8_t readmode, uint8_t *data, uint8_t bytes );
	static void RpDmaSerFinishChk( void );
	static void RpDmaSerStop( void );
#endif// RP_DMA_WRITE_RAM_ENA || RP_DMA_READ_RAM_ENA

/***************************************************************************************************************
 * private variables
 **************************************************************************************************************/
#ifdef RP_DEBUG_WRITE_MONITOR
	#define RP_DEBUG_RAM_SIZE	1024
	uint8_t RpDebugRamReg[RP_DEBUG_RAM_SIZE];
	uint16_t RpDebugRamRegCnt;
#endif

#ifdef __CCRL__
	#ifdef R_USE_RENESAS_STACK
		#define RP_INTHDRSTACK_SIZE     (160)
		static uint16_t	RpIntHdlStk[RP_INTHDRSTACK_SIZE];
	#endif // #ifdef R_USE_RENESAS_STACK
#endif // #ifdef __CCRL__

uint8_t	RpClkTick = 0;

/***************************************************************************************************************
 * program
 **************************************************************************************************************/
/***************************************************************************************************************
 * Inline asm
 **************************************************************************************************************/
#ifdef __CCRL__
#ifdef R_USE_RENESAS_STACK

#pragma inline_asm RpIntrSpChange
void RpIntrSpChange(uint16_t pstk)
{
	movw	bc, sp
	movw	sp, ax   ; move SP
	push	bc      ; store original SP to the buffer
	subw	sp, #0x02
}

#pragma inline_asm RpIntrSpBack
void RpIntrSpBack(void)
{
	addw	sp, #0x02
	pop		ax
	movw	sp, ax   ; move SP to the original
}

#endif // #ifdef R_USE_RENESAS_STACK
#endif // #ifdef __CCRL__

/***************************************************************************************************************
 * function name  : RpGetExtaddr
 * description    : get self IEEE address.
 * parameters     : pExtAddr:the pointer of "reading IEEE address"
 * return value   : none
 **************************************************************************************************************/
void RpGetExtaddr( uint8_t *pExtAddr )
{
#if defined(RM_RAM_EXTADDR)	// get extended address from global varialbe
	extern uint32_t RpLocalExtAddr[];

	RpMemcpy(pExtAddr, RpLocalExtAddr, 8);
#else					// get extended address from internal flash (block A)
	DFLCTL = 0x01;		// enable data flash
	RpMemcpy(pExtAddr, RP_ADDR_EXTADDR, 8);
	DFLCTL = 0x00;		// disable data flash
#endif
}

/***************************************************************************************************************
 * function name  : INTPx_Interrupt
 * description    : INTP interrupt handler.
 * parameters     : none
 * return value   : none
 **************************************************************************************************************/
#ifdef __CCRL__
__near void INTPx_Interrupt( void )
#else
__interrupt void INTPx_Interrupt( void )
#endif /* __CCRL__ */
{
	#if R_USE_RENESAS_STACK && defined(__CCRL__)
	RpIntrSpChange((uint16_t)(&RpIntHdlStk[RP_INTHDRSTACK_SIZE]));    // (no need '-1')
	#endif

	EI();
	RpIntpHdr();

	#if R_USE_RENESAS_STACK && defined(__CCRL__)
	RpIntrSpBack();
	#endif
}

/***************************************************************************************************************
 * function name  : INTTM_TXLIMIT_Interrupt
 * description    : 10% Limitation Timer interrupt.
 * parameters     : none
 * return value   : none
 **************************************************************************************************************/
#ifdef __CCRL__
__near void INTTM_TXLIMIT_Interrupt( void )
{
	#ifdef R_USE_RENESAS_STACK
	RpIntrSpChange( (uint16_t)(&RpIntHdlStk[RP_INTHDRSTACK_SIZE]) );    // (no need '-1')
	#endif // #ifdef R_USE_RENESAS_STACK

	EI();
	RpTxLimitTimerForward(1000);

	#ifdef R_USE_RENESAS_STACK
	RpIntrSpBack();
	#endif // #ifdef R_USE_RENESAS_STACK
}
#else
__interrupt void INTTM_TXLIMIT_Interrupt( void )
{
	EI();
	RpTxLimitTimerForward( 1000 );
}
#endif // #ifdef __CCRL__

/***************************************************************************************************************
 * function name  : RpSetMcuInt
 * description    : control of using RF interrput.
 * parameters     : enable: 0:disable interrupt, 1:enable interrupt nothing to do
 * return value   : none
 **************************************************************************************************************/
void RpSetMcuInt( uint8_t enable )
{
	if (enable == RP_TRUE)
	{
		EGP0	|=	0x08;							// INTP3 rising edge(EGP3	= 	1)
		EGN0	&=	~0x08;							// INTP3 rising edge(EGN3	=	0)
		PPR03	= 	RP_PHY_INTLEVEL_RFINTP_b0;
		PPR13	= 	RP_PHY_INTLEVEL_RFINTP_b1;		// INTP3 priority 1
	}
	RpSetMcuIntMaskOnly(enable);
}

/***************************************************************************************************************
 * function name  : RpSetMcuIntMaskOnly
 * description    : set interrupt mask only
 * parameters     : 
 * return value   : 
 **************************************************************************************************************/
static void RpSetMcuIntMaskOnly( uint8_t enable )
{
	// accept RF IC interrupt and moreover previous RF IC interrupt
	PIF3 = RP_FALSE;			// INTP3 REQ clear
	if (enable == RP_TRUE)
	{
		PMK3	= RP_FALSE;		// INTP3 MASK enable
	}
	else
	{
		PMK3	= RP_TRUE;		// INTP3 MASK disable
	}
}

/***************************************************************************************************************
 * function name  : RpRegWrite
 * description    : Write a data to RFIC function.
 * parameters     : 
 * return value   : 
 **************************************************************************************************************/
void RpRegWrite( uint16_t adr, uint8_t dat )
{
	RpCSI_CSActivate();

#ifdef RP_TXRX_RAM_BIT_SWAP
	SCR10 = SAU_RECEPTION_TRANSMISSION | SAU_TIMING_1 | SAU_MSB | SAU_LENGTH_8;
#endif

	SMR10 = SAU_SMRMN_INITIALVALUE | SAU_CLOCK_SELECT_CK01 | SAU_CLOCK_MODE_CKS | SAU_TRIGGER_SOFTWARE | SAU_CSI | SAU_BUFFER_EMPTY;
#ifdef RP_DEBUG_WRITE_MONITOR
	RpDebugRamReg[RpDebugRamRegCnt] = 0xfe;
	RpDebugRamRegCnt++;
	RpDebugRamRegCnt &= (RP_DEBUG_RAM_SIZE - 1);
	RpDebugRamReg[RpDebugRamRegCnt] = 0xfd;
	RpDebugRamRegCnt++;
	RpDebugRamRegCnt &= (RP_DEBUG_RAM_SIZE - 1);
#endif
	RP_REG_WRITE((uint8_t)RP_CSI_MEM_WR_H(adr));
#ifdef RP_DEBUG_WRITE_MONITOR
	RP_CSI_IRQ = 0;
	RP_CSI_BUF = (uint8_t)RP_CSI_MEM_WR_L(adr);
	{
		uint8_t test_ram[2];

		test_ram[0] = (uint8_t)(adr >> 3);
		test_ram[1] = (uint8_t)(adr >> 11);
		RpDebugRamReg[RpDebugRamRegCnt] = test_ram[1];
		RpDebugRamReg[RpDebugRamRegCnt + 1] = test_ram[0];
	}
	RpDebugRamRegCnt += 2;
	RpDebugRamRegCnt &= (RP_DEBUG_RAM_SIZE - 1);
	while (RP_CSI_IRQ == 0);
	RP_CSI_IRQ = 0;
	RP_CSI_BUF = dat;
	RpDebugRamReg[RpDebugRamRegCnt] = dat;
	RpDebugRamRegCnt++;
	RpDebugRamRegCnt &= (RP_DEBUG_RAM_SIZE - 1);
	while (RP_CSI_IRQ == 0);
#else
	RP_REG_WRITE((uint8_t)RP_CSI_MEM_WR_L(adr));

#ifdef RP_TXRX_RAM_BIT_SWAP
	if ((BBTXRAM0 <= adr) && (adr < BBTXRXRAMEND))
	{
		SCR10 = SAU_RECEPTION_TRANSMISSION | SAU_TIMING_1 | SAU_LSB | SAU_LENGTH_8;
	}
#endif

	RP_REG_WRITE(dat);
#endif
	RP_CSI_IRQ = 0;
	while (RP_CSI_STATUS & RP_SAU_UNDER_EXECUTE);
	RpCSI_CSDeactivate();
}

/***************************************************************************************************************
 * function name  : RpRegRead
 * description    : Read a data from RFIC function.
 * parameters     : 
 * return value   : 
 **************************************************************************************************************/
uint8_t RpRegRead( uint16_t adr )
{
	uint8_t rData;

	RpCSI_CSActivate();
#ifdef RP_TXRX_RAM_BIT_SWAP
	SCR10 = SAU_RECEPTION_TRANSMISSION | SAU_TIMING_1 | SAU_MSB | SAU_LENGTH_8;
#endif
	SMR10 = SAU_SMRMN_INITIALVALUE | SAU_CLOCK_SELECT_CK01 | SAU_CLOCK_MODE_CKS | SAU_TRIGGER_SOFTWARE | SAU_CSI | SAU_TRANSFER_END;
	RP_REG_WRITE((uint8_t)RP_CSI_MEM_RD_H(adr));
	RP_REG_WRITE((uint8_t)RP_CSI_MEM_RD_L(adr));
	RP_REG_WRITE(RP_CSI_NOP);

#ifdef RP_TXRX_RAM_BIT_SWAP
	if ((BBTXRAM0 <= adr) && (adr < BBTXRXRAMEND))
	{
		SCR10 = SAU_RECEPTION_TRANSMISSION | SAU_TIMING_1 | SAU_LSB | SAU_LENGTH_8;
	}
#endif

	rData = RP_CSI_BUF;
	RpCSI_CSDeactivate();

	return  rData;
}

/***************************************************************************************************************
 * function name  : RpCSIInitPort
 * description    : CS line control function.
 * parameters     : none
 * return value   : none
 **************************************************************************************************************/
static void RpCSIInitPort( void )
{
	// STANDBY,OSCDRVSEL,DON,RFRESETB Low output
	RP_STANDBY_PORT 	= RP_PORT_LO;
	RP_OSCDRVSEL_PORT 	= RP_PORT_LO;
	RP_OSCDRVSEL_PM 	= RP_PORT_OUTPUT;
	RP_DON_PORT 		= RP_PORT_LO;
	RP_DON_PM 			= RP_PORT_OUTPUT;
	RP_RFRESETB_PORT 	= RP_PORT_LO;
	RP_RFRESETB_PM 		= RP_PORT_OUTPUT;

	// MCU - RFIC Serial Setting 4Mbps, CS, RX, TX, CLK
	RP_CSI_CS_PORT 		= RP_PORT_HI;
	RP_CSI_CS_PM 		= RP_PORT_OUTPUT;
	RP_CSI_RX_PM		= RP_PORT_INPUT;
	RP_CSI_RX_PULLUP	= 1;
	RP_CSI_TX_PORT 		= RP_PORT_HI;
	RP_CSI_TX_PM		= RP_PORT_OUTPUT;
	RP_CSI_CLK_PORT 	= RP_PORT_HI;
	RP_CSI_CLK_PM 		= RP_PORT_OUTPUT;

	// INTP
	RP_INTP_PM			= RP_PORT_INPUT;
	RP_INTP_PULLUP		= 1;

#if defined(MCU_R78G1H)
	// unused ports
	P0_bit.no0 = RP_PORT_LO;
	PM0_bit.no0 = RP_PORT_OUTPUT;
	P0_bit.no1 = RP_PORT_LO;
	PM0_bit.no1 = RP_PORT_OUTPUT;
	P0_bit.no5 = RP_PORT_LO;
	PM0_bit.no5 = RP_PORT_OUTPUT;
	P0_bit.no6 = RP_PORT_LO;
	PM0_bit.no6 = RP_PORT_OUTPUT;
	P1_bit.no7 = RP_PORT_LO;
	PM1_bit.no7 = RP_PORT_OUTPUT;
	P2_bit.no3 = RP_PORT_LO;
	PM2_bit.no3 = RP_PORT_OUTPUT;
	P2_bit.no4 = RP_PORT_LO;
	PM2_bit.no4 = RP_PORT_OUTPUT;
	P2_bit.no5 = RP_PORT_LO;
	PM2_bit.no5 = RP_PORT_OUTPUT;
	P2_bit.no6 = RP_PORT_LO;
	PM2_bit.no6 = RP_PORT_OUTPUT;
	P2_bit.no7 = RP_PORT_LO;
	PM2_bit.no7 = RP_PORT_OUTPUT;
	P4_bit.no1 = RP_PORT_LO;
	PM4_bit.no1 = RP_PORT_OUTPUT;
	P4_bit.no2 = RP_PORT_LO;
	PM4_bit.no2 = RP_PORT_OUTPUT;
	P4_bit.no3 = RP_PORT_LO;
	PM4_bit.no3 = RP_PORT_OUTPUT;
	P4_bit.no4 = RP_PORT_LO;
	PM4_bit.no4 = RP_PORT_OUTPUT;
	P4_bit.no5 = RP_PORT_LO;
	PM4_bit.no5 = RP_PORT_OUTPUT;
	P4_bit.no6 = RP_PORT_LO;
	PM4_bit.no6 = RP_PORT_OUTPUT;
	P4_bit.no7 = RP_PORT_LO;
	PM4_bit.no7 = RP_PORT_OUTPUT;
	P5_bit.no0 = RP_PORT_LO;
	PM5_bit.no0 = RP_PORT_OUTPUT;
	P5_bit.no1 = RP_PORT_LO;
	PM5_bit.no1 = RP_PORT_OUTPUT;
	P5_bit.no2 = RP_PORT_LO;
	PM5_bit.no2 = RP_PORT_OUTPUT;
	P5_bit.no3 = RP_PORT_LO;
	PM5_bit.no3 = RP_PORT_OUTPUT;
	P5_bit.no4 = RP_PORT_LO;
	PM5_bit.no4 = RP_PORT_OUTPUT;
	P5_bit.no5 = RP_PORT_LO;
	PM5_bit.no5 = RP_PORT_OUTPUT;
	P5_bit.no6 = RP_PORT_LO;
	PM5_bit.no6 = RP_PORT_OUTPUT;
	P5_bit.no7 = RP_PORT_LO;
	PM5_bit.no7 = RP_PORT_OUTPUT;
	P6_bit.no4 = RP_PORT_LO;
	PM6_bit.no4 = RP_PORT_OUTPUT;
	P6_bit.no5 = RP_PORT_LO;
	PM6_bit.no5 = RP_PORT_OUTPUT;
	P6_bit.no6 = RP_PORT_LO;
	PM6_bit.no6 = RP_PORT_OUTPUT;
	P6_bit.no7 = RP_PORT_LO;
	PM6_bit.no7 = RP_PORT_OUTPUT;
	P7_bit.no3 = RP_PORT_LO;
	PM7_bit.no3 = RP_PORT_OUTPUT;
	P7_bit.no4 = RP_PORT_LO;
	PM7_bit.no4 = RP_PORT_OUTPUT;
	P8_bit.no3 = RP_PORT_LO;
	PM8_bit.no3 = RP_PORT_OUTPUT;
	P8_bit.no4 = RP_PORT_LO;
	PM8_bit.no4 = RP_PORT_OUTPUT;
	P8_bit.no5 = RP_PORT_LO;
	PM8_bit.no5 = RP_PORT_OUTPUT;
	P8_bit.no6 = RP_PORT_LO;
	PM8_bit.no6 = RP_PORT_OUTPUT;
	P8_bit.no7 = RP_PORT_LO;
	PM8_bit.no7 = RP_PORT_OUTPUT;
	P10_bit.no1 = RP_PORT_LO;
	PM10_bit.no1 = RP_PORT_OUTPUT;
	P10_bit.no2 = RP_PORT_LO;
	PM10_bit.no2 = RP_PORT_OUTPUT;
	P11_bit.no0 = RP_PORT_LO;
	PM11_bit.no0 = RP_PORT_OUTPUT;
	P11_bit.no1 = RP_PORT_LO;
	PM11_bit.no1 = RP_PORT_OUTPUT;
	P14_bit.no5 = RP_PORT_LO;
	PM14_bit.no5 = RP_PORT_OUTPUT;
	P14_bit.no6 = RP_PORT_LO;
	PM14_bit.no6 = RP_PORT_OUTPUT;
	P14_bit.no7 = RP_PORT_LO;
	PM14_bit.no7 = RP_PORT_OUTPUT;
	P15_bit.no0 = RP_PORT_LO;
	PM15_bit.no0 = RP_PORT_OUTPUT;
	P15_bit.no1 = RP_PORT_LO;
	PM15_bit.no1 = RP_PORT_OUTPUT;
	P15_bit.no2 = RP_PORT_LO;
	PM15_bit.no2 = RP_PORT_OUTPUT;
	P15_bit.no3 = RP_PORT_LO;
	PM15_bit.no3 = RP_PORT_OUTPUT;
	P15_bit.no4 = RP_PORT_LO;
	PM15_bit.no4 = RP_PORT_OUTPUT;
#endif
}

/***************************************************************************************************************
 * function name  : RpCheckRfIRQ
 * description    : Check IRQ function.
 * parameters     : none
 * return value   : none
 **************************************************************************************************************/
uint8_t RpCheckRfIRQ( void )
{
	uint8_t rtn;

	if (RP_INTP_PORT == RP_INTP_PORT_ACTIVE)
	{
		rtn = RP_TRUE;
	}
	else
	{
		rtn = RP_FALSE;
	}

	return (rtn);
}

/***************************************************************************************************************
 * function name  : RpCSIInitialize
 * description    : CSI Initialize function.
 * parameters     : none
 * return value   : none
 **************************************************************************************************************/
static void RpCSIInitialize( void )
{
	SAU1EN = 1;	// SAU1 clock supply

	NOP();
	NOP();
	NOP();
	NOP();

#if RP_CPU_CLK == 32
	SPS1 = SAU_CK01_FCLK_1 | (SPS1 & 0x0F);		// base CK01
#else // RP_CPU_CLK == 8
	SPS1 = SAU_CK01_FCLK_0 | (SPS1 & 0x0F);		// base CK01
#endif
	// CSI20 initial setting
	ST1 |= SAU_CH0_STOP_TRG_ON;		// CSI20 disable
	CSIMK20 = 1;					// INTCSI20 disable
	CSIIF20 = 0;					// INTCSI20 IF clear
	SIR10 = SAU_SIRMN_FECTMN | SAU_SIRMN_PECTMN | SAU_SIRMN_OVCTMN;		// error flag clear
	SMR10 = SAU_SMRMN_INITIALVALUE | SAU_CLOCK_SELECT_CK01 | SAU_CLOCK_MODE_CKS | SAU_TRIGGER_SOFTWARE | SAU_CSI | SAU_TRANSFER_END;
	SCR10 = SAU_RECEPTION_TRANSMISSION | SAU_TIMING_1 | SAU_MSB | SAU_LENGTH_8;
	SDR10 = 0x0200; // 32MHz / 2 / 4 = 4Mbps or 8MHz / 4 = 2Mbps
	SO1	&= ~SAU_CH0_CLOCK_OUTPUT_1;		// CSI20 clock initial level
	SO1	&= ~SAU_CH0_DATA_OUTPUT_1;		// CSI20 SO initial level
	SOE1 |= SAU_CH0_OUTPUT_ENABLE;		// CSI20 output enable
}

/***************************************************************************************************************
 * function name  : RpRFTimerInitialize
 * description    : RF Timer Initialize function.
 * parameters     : none
 * return value   : none
 **************************************************************************************************************/
static void RpRFTimerInitialize( void )
{
	// General Timer Setting (TA0 1uS)
	TAU0EN = 1;					// supplies input clock
	TT0 |= TAU_CH0_STOP_TRG_ON;
	TPS0 &= ~0x00f0;			// rewrite CK01
#if RP_CPU_CLK == 32
	TPS0 |= TAU_CK01_FCLK_10;	// rewrite! for 31.25KHz(32uS) CK01
#else // if RP_CPU_CLK == 8
	TPS0 |= TAU_CK01_FCLK_8;	// rewrite! for 31.25KHz(32uS) CK01
#endif
#if (RP_USR_RF_TAU_CH_SELECT == 1)
	if (RpConfig.tmrClkSelect == RP_CLK_LIMIT_TMR_FASTOCO)
#endif
	{
		RpClkTick = 32;
	}// 	if(RpConfig.tmrClkSelect == RP_CLK_LIMIT_TMR_FASTOCO
#if (RP_USR_RF_TAU_CH_SELECT == 1)
	else if (RpConfig.tmrClkSelect == RP_CLK_LIMIT_TMR_32KHz)
	{
		RpClkTick = 30;
	}
#endif
	// Mask channel 0 interrupt
	TMMK00 = 1;		// INTTM00 disabled
	TMIF00 = 0;		// INTTM00 interrupt flag clear
	// Set INTTM00 low priority
	TMPR100 = RP_PHY_INTLEVEL_RFTimers_b1;
	TMPR000 = RP_PHY_INTLEVEL_RFTimers_b0;	// Level_1
	// Channel 0 used as interval timer
	TMR00 = TAU_TMR0_INITIALVALUE | TAU_CLOCK_SELECT_CK01 | TAU_CLOCK_MODE_CKS | TAU_MODE_INTERVAL_TIMER | TAU_START_INT_UNUSED;
	TDR00 = 0xffff;						// default
	TOM0 &= ~TAU_CH0_OUTPUT_COMBIN ;
	TOL0 &= ~TAU_CH0_OUTPUT_LEVEL_L;
	TOE0 &= ~TAU_CH0_OUTPUT_ENABLE;

	// 10% Timer Setting
#if (RP_USR_RF_TAU_CH_SELECT == 1)
	//   Timer settings(16bit Timer01) for 10% Limitation
	TT0 |= TAU_CH1_STOP_TRG_ON;
	// Mask channel 1 interrupt
	TMMK01 = 1;		// INTTM01 disabled
	TMIF01 = 0;		// INTTM01 interrupt flag clear
	// Set INTTM01 low priority
	TMPR001 = RP_PHY_INTLEVEL_RFTimers_b0;
	TMPR101 = RP_PHY_INTLEVEL_RFTimers_b1;
	// Channel 1 used as interval timer
	if (RpConfig.tmrClkSelect == RP_CLK_LIMIT_TMR_FASTOCO)
	{
		TMR01 = TAU_TMR0_INITIALVALUE | TAU_CLOCK_SELECT_CK01 | TAU_CLOCK_MODE_CKS | TAU_MODE_INTERVAL_TIMER | TAU_START_INT_UNUSED;
		TIS0 = 0;
		TDR01 = 31719;//31250; mergine plus 1.5%
	}
	else if (RpConfig.tmrClkSelect == RP_CLK_LIMIT_TMR_32KHz)
	{
		TMR01 = TAU_TMR0_INITIALVALUE | TAU_CLOCK_SELECT_CK00 | TAU_CLOCK_MODE_CKS | TAU_MODE_INTERVAL_TIMER | TAU_START_INT_UNUSED | TAU_CLOCK_MODE_TI0N;
		OSMC &= (~0x10);// WUTMMCK0 = 0(SubClk);	12bitTimer, RTC, TRJ Clock Source is Sub Clock
		TIS0 = 0x05;	// Sub-CLK
		TDR01 = 32768;
	}
	TOM0 &= ~TAU_CH1_OUTPUT_COMBIN ;
	TOL0 &= ~TAU_CH1_OUTPUT_LEVEL_L;
	TOE0 &= ~TAU_CH1_OUTPUT_ENABLE;

#else  /* RP_USR_RF_TAU_CH_SELECT != 1 */
	//   Timer settings(16bit Timer02) for 10% Limitation
	TT0 |= TAU_CH2_STOP_TRG_ON;
	// Mask channel 2 interrupt
	TMMK02 = 1;		// INTTM02 disabled
	TMIF02 = 0;		// INTTM02 interrupt flag clear
	// Set INTTM02 low priority
	TMPR002 = RP_PHY_INTLEVEL_RFTimers_b0;
	TMPR102 = RP_PHY_INTLEVEL_RFTimers_b1;
	// Channel 2 used as interval timer
	TMR02 = TAU_TMR0_INITIALVALUE | TAU_CLOCK_SELECT_CK01 | TAU_CLOCK_MODE_CKS | TAU_MODE_INTERVAL_TIMER | TAU_START_INT_UNUSED;
	TIS0 = 0;
	TDR02 = 31719;//31250; mergine plus 1.5%

	TOM0 &= ~TAU_CH2_OUTPUT_COMBIN ;
	TOL0 &= ~TAU_CH2_OUTPUT_LEVEL_L;
	TOE0 &= ~TAU_CH2_OUTPUT_ENABLE;

#endif  /* RP_USR_RF_TAU_CH_SELECT */
};

/***************************************************************************************************************
 * function name  : RpLimitTimerControl
 * description    : Limitation Timer Control
 * parameters     : timerOn: start timer
 * return value   : lack time
 **************************************************************************************************************/
uint32_t RpLimitTimerControl( uint8_t timerOn )
{
	uint32_t	lackTime = 0;

#if (RP_USR_RF_TAU_CH_SELECT == 1)
	TT0 |= TAU_CH1_STOP_TRG_ON;
	TMMK01 = 1;			// INTTM01 disabled
	if (timerOn == RP_TRUE)
	{
		TMMK01 = 0;		// INTTM01 enabled
		TS0 |= TAU_CH1_START_TRG_ON;
	}
	else
	{
		lackTime = (uint32_t)((uint32_t)(TDR01 - TCR01) * (uint32_t)RpClkTick);
	}

#else /* RP_USR_RF_TAU_CH_SELECT != 1 */
	TT0 |= TAU_CH2_STOP_TRG_ON;
	TMMK02 = 1;			// INTTM02 disabled
	if (timerOn == RP_TRUE)
	{
		TMMK02 = 0;		// INTTM02 enabled
		TS0 |= TAU_CH2_START_TRG_ON;
	}
	else
	{
		lackTime = (uint32_t)((uint32_t)(TDR02 - TCR02) * (uint32_t)RpClkTick);
	}
#endif

	return (lackTime);
}

/***************************************************************************************************************
 * function name  : RpMcuPeripheralInit
 * description    : CS line control function.
 * parameters     : none
 * return value   : none
 **************************************************************************************************************/
void RpMcuPeripheralInit( void )
{
	RpCSIInitPort();
	RpCSIInitialize();
#if RP_DMA_WRITE_RAM_ENA || RP_DMA_READ_RAM_ENA
	RpDmaSerInitial();
#endif
	RpRFTimerInitialize();
}

/***************************************************************************************************************
 * function name  : RpWakeupSequence
 * description    : RF Initial Sequence function.
 * parameters     : none
 * return value   : none
 **************************************************************************************************************/
void RpWakeupSequence( void )
{
	SS1 |= SAU_CH0_START_TRG_ON;	// CSI20 enable

	RP_CSI_BUF = 0xff;
	while (RP_CSI_IRQ == 0);
	RP_CSI_IRQ = 0;

	TT0 |= TAU_CH0_STOP_TRG_ON;

	if (RpConfig.rfClkSource == RP_RFCLK_SOURCE_CRYSTAL)
	{
		// STANDBY = High, 500uS wait
		TMIF00 = 0;		// INTTM00 interrupt flag clear
		TDR00 = 16 - 1/* 500 - 2 */;
		RP_STANDBY_PORT = RP_PORT_HI;
		RP_WAIT_WAKEUP_ACTIVE();
		// OSDDRVSEL = High, 50uS wait
		TMIF00 = 0;		// INTTM00 interrupt flag clear
		TDR00 = 1 - 1/* 50 - 2 */;
		RP_OSCDRVSEL_PORT = RP_PORT_HI;
		RP_WAIT_WAKEUP_ACTIVE();
	}
	else// if RpConfig.rfClkSource == RP_RFCLK_SOURCE_CRYSTAL
	{
		RP_STANDBY_PORT = RP_PORT_HI;
		RP_OSCDRVSEL_PORT = RP_PORT_HI;
	}
	// DON = High, 450uS wait
	TMIF00 = 0;		// INTTM00 interrupt flag clear
	TDR00 = 14 - 1/* 450 - 2 */;
	RP_DON_PORT = RP_PORT_HI;
	RP_WAIT_WAKEUP_ACTIVE();
	// RFRESETB = High calibration start
	RP_RFRESETB_PORT = RP_PORT_HI;
	// RFSIO and RFIRQ pull-up off
	RP_CSI_RX_PULLUP	= 0;
	RP_INTP_PULLUP		= 0;
	RpRegWrite(BBRFCON, RFSTART | ANARESETB);			// Analog Reset
	RpExecuteCalibration();
}

/***************************************************************************************************************
 * function name  : RpExecuteCalibration
 * description    : Check RF Calibration function.
 * parameters     : none
 * return value   : none
 **************************************************************************************************************/
void RpExecuteCalibration( void )
{
#if defined(MCU_R78G1H)
	uint8_t readRomData[4];
	uint8_t mode450KHz = RpRdEvaReg2(0x0E00) & 0x10;

	RpMemcpy(readRomData, (uint8_t RP_FAR *)0x000EFFEC, sizeof(readRomData));

	//	readRomData[0]	0x000EFFEC	0xFA0C
	//	readRomData[1]	0x000EFFED	0xFA0D
	//	readRomData[2]	0x000EFFEE	0xFDA0
	//	readRomData[3]	0x000EFFEF	0xFDA1

	if (mode450KHz)
	{
		RpWrEvaReg2(0x0b00 | readRomData[2]);
	}
	else
	{
		RpWrEvaReg2(0x0b00 | readRomData[3]);
	}
	RpWrEvaReg2(0x0100 | (readRomData[0] & 0x0f));
	RpWrEvaReg2(0x0200 | readRomData[1]);
#else
	uint16_t reg;

	RpRegWrite(BBRFCON, RFSTART | ANARESETB | CSONSET);	// Calibration On
	RpRegWrite(BBINTEN0, (uint8_t)((RpRegRead(BBINTEN0)) | CALINTEN));// Caribration Interrupt Ennable
	RpRegRead(BBINTREQ0);								// dummy read for clear bit of Caribration Interrupt
	reg = 0x0000;
	RpRegBlockWrite(RFINI10, (uint8_t RP_FAR *)&reg, sizeof(uint16_t));
	reg = 0x0732;
	RpRegBlockWrite(RFINI10, (uint8_t RP_FAR *)&reg, sizeof(uint16_t));
	// Calibration Int Wait
	RpRegWrite(BBCAL, CALSTART);	// Calibration Start
	while ((RpRegRead(BBINTREQ0) & CALINTREQ) == 0);
	RpRegWrite(BBRFCON, RFSTART | ANARESETB);			// Calibration Off
	RpRegWrite(BBINTEN0,	(uint8_t)((RpRegRead(BBINTEN0)) & ~CALINTEN));// Caribration Interrupt Disable
#endif
}

/***************************************************************************************************************
 * function name  : RpPowerdownSequence
 * description    : RF Powerdown Sequence function.
 * parameters     : none
 * return value   : none
 **************************************************************************************************************/
void RpPowerdownSequence( void )
{
	// RFSIO and RFIRQ pull-up on
	RP_CSI_RX_PULLUP	= 1;
	RP_INTP_PULLUP		= 1;
	// STANDBY,OSCDRVSEL,DON,RFRESETB Low output
	RP_STANDBY_PORT 	= RP_PORT_LO;
	if (RpConfig.rfClkSource == RP_RFCLK_SOURCE_CRYSTAL)
	{
		RP_OSCDRVSEL_PORT 	= RP_PORT_LO;
	}
	RP_DON_PORT 		= RP_PORT_LO;
	RP_RFRESETB_PORT 	= RP_PORT_LO;
	
	ST1 |= SAU_CH0_STOP_TRG_ON; 	// CSI20 disable
}

#if RP_DMA_WRITE_RAM_ENA || RP_DMA_READ_RAM_ENA
/***************************************************************************************************************
 * function name  : RpDmaSerInitial
 * description    : DTC initial function.
 * parameters     : none
 * return value   : none
 **************************************************************************************************************/
#ifdef __CCRL__
	#pragma address (dtc_vectortable = DTC_VECTBL_ADDRESS)
	uint8_t dtc_vectortable[0x40];
	#pragma address (dtc_controldata_0 = DTC_CTRL0_ADDRESS)
	uint8_t dtc_controldata_0[8];
	#pragma address (dtc_controldata_1 = DTC_CTRL1_ADDRESS)
	uint8_t dtc_controldata_1[8];
#else
	#pragma location=0xFFD00
	uint8_t dtc_vectortable[0x40];
	#pragma location=0xFFD40
	uint8_t dtc_controldata_0[8];
	uint8_t dtc_controldata_1[8];
#endif

static void RpDmaSerInitial( void )
{
	// Enable input clock supply
	DTCEN = 1U;
	// Disable all DTC channels operation
	DTCEN0 = 0x00U;
	DTCEN1 = 0x00U;
	DTCEN2 = 0x00U;
	DTCEN3 = 0x00U;
	DTCEN4 = 0x00U;
	// Set base address
	DTCBAR = DTC_VECTBL_ADDRESS_BARSET;
	// Set CSI20 Interrupt
	CSIPR020 = RP_PHY_INTLEVEL_DMADone_b0;
	CSIPR120 = RP_PHY_INTLEVEL_DMADone_b1;
}
#endif // #if RP_DMA_WRITE_RAM_ENA || RP_DMA_READ_RAM_ENA

#if RP_DMA_WRITE_RAM_ENA || RP_DMA_READ_RAM_ENA

/* Mask Compiler Warning[Pe767]: conversion from pointer to smaller integer */
#pragma diag_suppress=Pe767
/* Reason: The DTC destination / source address registers must hold a 16-bit address
   for data transfer. Therefore the conversion of the pointer to an 16-bit integer
   carring the source and destination address is the desired operation. */

/***************************************************************************************************************
 * function name  : RpDmaSerStart
 * description    : 
 * parameters     : 
 * return value   : none
 **************************************************************************************************************/
static void RpDmaSerStart( uint8_t readmode, uint8_t *data, uint8_t bytes )
{
	uint8_t dummy = 0xff, *nextptr = data + 1;

	RP_DTC_VECTORTABLE16 = 0x40;

	if (readmode)
	{
		// Set DTCD0 for Reading
		RP_DTC_CONTROLDATA0_DTCCR = RP_00_DTC_DATA_SIZE_8BITS | RP_00_DTC_TRANSFER_MODE_NORMAL |
									RP_00_DTC_SOURCE_ADDR_FIXED | RP_08_DTC_DEST_ADDR_INCREMENTED |
									RP_10_DTC_CHAIN_TRANSFER_ENABLE | RP_00_DTC_REPEAT_INT_DISABLE;
		RP_DTC_CONTROLDATA0_DTBLS = RP_01_DTCD0_TRANSFER_BYTE;
		RP_DTC_CONTROLDATA0_DTCCT = bytes;                      // number of read data
		RP_DTC_CONTROLDATA0_DTRLD = bytes;                      // (don't care)
		RP_DTC_CONTROLDATA0_DTSAR = RP_FF48_DTCD1_SRC_ADDRESS;  // SIO20
		RP_DTC_CONTROLDATA0_DTDAR = (uint16_t)data;             // address for read data
		// Set DTCD1 for Writing
		RP_DTC_CONTROLDATA1_DTCCR = RP_00_DTC_DATA_SIZE_8BITS | RP_00_DTC_TRANSFER_MODE_NORMAL |
									RP_00_DTC_SOURCE_ADDR_FIXED | RP_00_DTC_DEST_ADDR_FIXED |
									RP_00_DTC_CHAIN_TRANSFER_DISABLE | RP_00_DTC_REPEAT_INT_DISABLE;
		RP_DTC_CONTROLDATA1_DTBLS = RP_01_DTCD0_TRANSFER_BYTE;
		RP_DTC_CONTROLDATA1_DTCCT = bytes - 1;                  // number of write data
		RP_DTC_CONTROLDATA1_DTRLD = bytes - 1;                  // (don't care)
		RP_DTC_CONTROLDATA1_DTSAR = (uint16_t)&dummy;           // dummy data address
		RP_DTC_CONTROLDATA1_DTDAR = RP_FF48_DTCD0_DST_ADDRESS;  // SIO20
	}
	else
	{
		// Set DTCD0 for Writing
		RP_DTC_CONTROLDATA0_DTCCR = RP_00_DTC_DATA_SIZE_8BITS | RP_00_DTC_TRANSFER_MODE_NORMAL |
									RP_04_DTC_SOURCE_ADDR_INCREMENTED | RP_00_DTC_DEST_ADDR_FIXED |
									RP_00_DTC_CHAIN_TRANSFER_DISABLE | RP_00_DTC_REPEAT_INT_DISABLE;
		RP_DTC_CONTROLDATA0_DTBLS = RP_01_DTCD0_TRANSFER_BYTE;
		RP_DTC_CONTROLDATA0_DTCCT = bytes - 1;                  // number of write data
		RP_DTC_CONTROLDATA0_DTRLD = bytes - 1;                  // (don't care)
		RP_DTC_CONTROLDATA0_DTSAR = (uint16_t)nextptr;          // address for write data
		RP_DTC_CONTROLDATA0_DTDAR = RP_FF48_DTCD0_DST_ADDRESS;  // SIO20
	}

	DTCEN2 |= RP_80_DTC_UART2T_ACTIVATION_ENABLE;
	CSIIF20 = 0;
	CSIMK20 = 1;  // INTCSI20 disable
	RP_CSI_IRQ = 0;
	RP_CSI_BUF = *data;
}
#endif // #if RP_DMA_WRITE_RAM_ENA || RP_DMA_READ_RAM_ENA

#if RP_DMA_WRITE_RAM_ENA || RP_DMA_READ_RAM_ENA
/***************************************************************************************************************
 * function name  : RpDmaSerFinishChk
 * description    : 
 * parameters     : none
 * return value   : none
 **************************************************************************************************************/
static void RpDmaSerFinishChk( void )
{
	while (DTCEN2 & RP_80_DTC_UART2T_ACTIVATION_ENABLE);
}
#endif // #if RP_DMA_WRITE_RAM_ENA || RP_DMA_READ_RAM_ENA

#if RP_DMA_WRITE_RAM_ENA || RP_DMA_READ_RAM_ENA
/***************************************************************************************************************
 * function name  : RpDmaSerStop
 * description    : 
 * parameters     : none
 * return value   : none
 **************************************************************************************************************/
static void RpDmaSerStop( void )
{
	DTCEN2 &= (uint8_t)~RP_80_DTC_UART2T_ACTIVATION_ENABLE;
	while (RP_CSI_STATUS & RP_SAU_UNDER_EXECUTE);
	CSIMK20 = 1;		// INTCSI20 disable
	RpCSI_CSDeactivate();
}
#endif // #if RP_DMA_WRITE_RAM_ENA || RP_DMA_READ_RAM_ENA

#if RP_DMA_WRITE_RAM_ENA
/***************************************************************************************************************
 * function name  : RpRegBlockWrite
 * description    : Write meny data to RFIC with DMA function.
 * parameters     : 
 * return value   : none
 **************************************************************************************************************/
void RpRegBlockWrite( uint16_t adr, uint8_t RP_FAR *dat, uint16_t len )
{
	if (len)
	{
		RpCSI_CSActivate();

#ifdef RP_TXRX_RAM_BIT_SWAP
		SCR10 = SAU_RECEPTION_TRANSMISSION | SAU_TIMING_1 | SAU_MSB | SAU_LENGTH_8;
#endif

		SMR10 = SAU_SMRMN_INITIALVALUE | SAU_CLOCK_SELECT_CK01 | SAU_CLOCK_MODE_CKS | SAU_TRIGGER_SOFTWARE | SAU_CSI | SAU_BUFFER_EMPTY;
		RP_REG_WRITE((uint8_t)RP_CSI_MEM_WR_H(adr));
		RP_REG_WRITE((uint8_t)RP_CSI_MEM_WR_L(adr));

#ifdef RP_TXRX_RAM_BIT_SWAP
		if ((BBTXRAM0 <= adr) && (adr < BBTXRXRAMEND))
		{
			SCR10 = SAU_RECEPTION_TRANSMISSION | SAU_TIMING_1 | SAU_LSB | SAU_LENGTH_8;
		}
#endif

		RP_CSI_IRQ = 0;

		/* check length is more than 4 and dat is in RAM */
		if ((len > 4) && ((uint32_t)(dat) >= 0x000F0000))
		{
			RpDmaSerStart(0, (uint8_t *)dat, (uint8_t)len);
			RpDmaTerminationWriteSer();
		}
		else
		{
			do
			{
				RP_CSI_BUF = *dat;
				while (RP_CSI_IRQ == 0);
				RP_CSI_IRQ = 0;
				dat++;
			}
			while (--len);

			while (RP_CSI_STATUS & RP_SAU_UNDER_EXECUTE);
			RpCSI_CSDeactivate();
		}
	}
}
#endif // #if RP_DMA_WRITE_RAM_ENA

#if RP_DMA_WRITE_RAM_ENA
/***************************************************************************************************************
 * function name  : RpDmaTerminationWriteSer
 * description    : Wait process of DMA write function.
 * parameters     : none
 * return value   : none
 **************************************************************************************************************/
static void RpDmaTerminationWriteSer( void )
{
	RpDmaSerFinishChk();
	RpDmaSerStop();
}
#endif // #if RP_DMA_WRITE_RAM_ENA

#if RP_DMA_READ_RAM_ENA
/***************************************************************************************************************
 * function name  : RpRegBlockRead
 * description    : Read meny data from RFIC with DMA function.
 * parameters     : 
 * return value   : none
 **************************************************************************************************************/
void RpRegBlockRead( uint16_t adr, uint8_t *pDat, uint16_t len )
{
	uint8_t rData;

	if (len)
	{
		RpCSI_CSActivate();

#ifdef RP_TXRX_RAM_BIT_SWAP
		SCR10 = SAU_RECEPTION_TRANSMISSION | SAU_TIMING_1 | SAU_MSB | SAU_LENGTH_8;
#endif

		SMR10 = SAU_SMRMN_INITIALVALUE | SAU_CLOCK_SELECT_CK01 | SAU_CLOCK_MODE_CKS | SAU_TRIGGER_SOFTWARE | SAU_CSI | SAU_TRANSFER_END;
		RP_REG_WRITE((uint8_t)RP_CSI_MEM_RD_H(adr));
		RP_REG_WRITE((uint8_t)RP_CSI_MEM_RD_L(adr));

#ifdef RP_TXRX_RAM_BIT_SWAP
		if ((BBTXRAM0 <= adr) && (adr < BBTXRXRAMEND))
		{
			SCR10 = SAU_RECEPTION_TRANSMISSION | SAU_TIMING_1 | SAU_LSB | SAU_LENGTH_8;
		}
#endif

		if (len > 4)
		{
			RpDmaSerStart(1, pDat, (uint8_t)len);
			RpDmaTerminationReadSer();
		}
		else // if(len >= 1 && len <= 4)
		{
			RP_CSI_IRQ = 0;
			RP_CSI_BUF = RP_CSI_NOP;
			switch (len)
			{
				case 4:
					while (RP_CSI_IRQ == 0);
					rData = RP_CSI_BUF;
					RP_CSI_IRQ = 0;
					RP_CSI_BUF = RP_CSI_NOP;
					*pDat = rData;
					pDat++;
				case 3:
					while (RP_CSI_IRQ == 0);
					rData = RP_CSI_BUF;
					RP_CSI_IRQ = 0;
					RP_CSI_BUF = RP_CSI_NOP;
					*pDat = rData;
					pDat++;
				case 2:
					while (RP_CSI_IRQ == 0);
					rData = RP_CSI_BUF;
					RP_CSI_IRQ = 0;
					RP_CSI_BUF = RP_CSI_NOP;
					*pDat = rData;
					pDat++;
				case 1:
					while (RP_CSI_IRQ == 0);
					*pDat = RP_CSI_BUF;
				default:
					break;
			}
			RpCSI_CSDeactivate();
		}
	}
}
#endif // #if RP_DMA_READ_RAM_ENA

#if RP_DMA_READ_RAM_ENA
/***************************************************************************************************************
 * function name  : RpDmaTerminationReadSer
 * description    : Wait process of DMA read function.
 * parameters     : none
 * return value   : none
 **************************************************************************************************************/
static void RpDmaTerminationReadSer( void )
{
	RpDmaSerFinishChk();
	while (RP_CSI_STATUS & RP_SAU_UNDER_EXECUTE);
	CSIMK20 = 1;		// INTCSI20 disable
	RpCSI_CSDeactivate();
}
#endif // #if RP_DMA_READ_RAM_ENA

/***************************************************************************************************************
 * function name  : RpGetIntLevel
 * description    : Get RFdriver Interrupt Level
 * parameters     : none
 * return value   : Inerruput Level
 **************************************************************************************************************/
uint8_t RpGetInterruptLevel( void )
{
	return( RP_PHY_INTLEVEL );
}

/***********************************************************************************************************************
 * Copyright (C) 2014-2021 Renesas Electronics Corporation.
 **********************************************************************************************************************/
 
