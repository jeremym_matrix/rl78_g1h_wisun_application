/***********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products.
 * No other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all 
 * applicable laws, including copyright laws. 
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING THIS SOFTWARE, WHETHER EXPRESS, 
 * IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE 
 * AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
 * TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS 
 * AFFILIATED COMPANIES SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR 
 * ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of 
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the 
 * following link:
 * http://www.renesas.com/disclaimer 
 **********************************************************************************************************************/
/***********************************************************************************************************************
 * file name	: phy_drv.h
 * description	: This is the RF driver's driver code.
 ***********************************************************************************************************************
 * Copyright (C) 2014-2021 Renesas Electronics Corporation.
 **********************************************************************************************************************/
#ifndef __phy_drv_H
#define __phy_drv_H

/***********************************************************************
 *	Port
 **********************************************************************/
/***********************************************************
 *	Port Level
 **********************************************************/
#define RP_PORT_INPUT				1
#define RP_PORT_OUTPUT				0
#define RP_PORT_HI					1
#define RP_PORT_LO					0

/***********************************************************
 *	Ports
 **********************************************************/
#define RP_STANDBY_PM
#define RP_STANDBY_PORT 			P13_bit.no0
#define RP_OSCDRVSEL_PM				PM1_bit.no0
#define RP_OSCDRVSEL_PORT 			P1_bit.no0
#define RP_DON_PM		 			PM1_bit.no1
#define RP_DON_PORT 				P1_bit.no1
#define RP_RFRESETB_PM				PM1_bit.no2
#define RP_RFRESETB_PORT 			P1_bit.no2

/***********************************************************
 *	CSI Ports
 **********************************************************/
#define	RP_CSI_RX_PM				PM1_bit.no4
#define	RP_CSI_RX_PULLUP			PU1_bit.no4
#define	RP_CSI_TX_PM				PM1_bit.no3
#define	RP_CSI_CLK_PM				PM1_bit.no5
#define	RP_CSI_RX_PORT				P1_bit.no4
#define	RP_CSI_TX_PORT				P1_bit.no3
#define	RP_CSI_CLK_PORT				P1_bit.no5
#define	RP_CSI_CS_PM				PM1_bit.no6
#define	RP_CSI_CS_PORT				P1_bit.no6

/***********************************************************
 *	INTP Port
 **********************************************************/
#define RP_INTP_PORT				P3_bit.no0
#define RP_INTP_PM					PM3_bit.no0
#define RP_INTP_PULLUP				PU3_bit.no0
#define RP_INTP_PORT_ACTIVE			RP_PORT_HI
#define RP_INTP_PORT_INACTIVE		RP_PORT_LO

#endif // #ifndef __phy_drv_H
/***********************************************************************************************************************
 * Copyright (C) 2014-2021 Renesas Electronics Corporation.
 **********************************************************************************************************************/

