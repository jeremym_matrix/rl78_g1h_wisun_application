/*******************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only
 * intended for use with Renesas products. No other uses are authorized.
 * This software is owned by Renesas Electronics Corporation and is protected under
 * all applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES
 * REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY,
 * INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY
 * DISCLAIMED.
 * TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
 * ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
 * FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES
 * FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS
 * AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this
 * software and to discontinue the availability of this software.
 * By using this software, you agree to the additional terms and
 * conditions found by accessing the following link:
 * http://www.renesas.com/disclaimer
 *******************************************************************************/
/*******************************************************************************
 * file name	: phy_fan.c
 * description	: 
 *******************************************************************************
 * Copyright (C) 2018 Renesas Electronics Corporation.
 ******************************************************************************/
#include "phy.h"
#include "phy_def.h"

extern RP_PHY_CB RpCb;

#ifdef RP_WISUN_FAN_STACK	// Req.No8
/******************************************************************************
Function Name:       RpGetPhyStatus
Parameters:          none
Return value:        RP_RX_ON:already RX_ON
                     RP_TRX_OFF:already TRX_OFF of Rxbuf full
                     RP_BUSY_RX:No status setting because now RX_ON or CCA or ED
                     RP_BUSY_TX:No status setting because now TX_ON
                     RP_BUSY_LOWPOWER:Now Lowpower mode
Description:         Checks and returns current status of PHY driver.
******************************************************************************/
int16_t
RpGetPhyStatus(void)
{
	uint16_t curStat;
	int16_t	rtnVal = RP_SUCCESS;
#if defined(__RX)
	uint32_t bkupPsw;
#elif defined(__CCRL__) || defined (__ICCRL78__)
	uint8_t  bkupPsw;
#endif

#if defined(__RX) || defined(__CCRL__) || defined (__ICCRL78__)
	RP_PHY_DI(bkupPsw);
#else
	RP_PHY_DI();
#endif

	// copy current status
	curStat = RpCb.status;
	if (curStat & RP_PHY_STAT_LOWPOWER)
	{
		rtnVal = RP_BUSY_LOWPOWER;
	}
	else if (curStat & RP_PHY_STAT_TRX_OFF)
	{
		rtnVal = RP_TRX_OFF;
	}
	else if (curStat & RP_PHY_STAT_TX)
	{
		rtnVal = RP_BUSY_TX;
	}
	else if (curStat & RP_PHY_STAT_RX)
	{
		if (((RpCb.pib.macAddressFilter1Ena == RP_TRUE) || (RpCb.pib.macAddressFilter2Ena == RP_TRUE)) &&
				RP_PHY_STAT_RX_BUSY())
		{
			rtnVal = RP_BUSY_RX;
		}
		else
		{
			rtnVal = RP_RX_ON;
		}
	}
	else
	{
		// CCA or ED or RX
		rtnVal = RP_BUSY_RX;
	}

#if defined(__RX) || defined(__CCRL__) || defined (__ICCRL78__)
	RP_PHY_EI(bkupPsw);
#else
	RP_PHY_EI();
#endif

	return (rtnVal);
}
#endif

#ifdef RP_WISUN_FAN_STACK	// Req.No9
/******************************************************************************
 * callback program
 *****************************************************************************/
#define RP_EDFLOORLVL		(-75)
#define RP_EDSATURLVL		(RP_EDFLOORLVL + (256/4))
#define RP_RSSIFLOORLVL		(-174)
#define RP_RSSISATURLVL		(+80)

/******************************************************************************
 *	function Name  : RpCalcLqiCallback
 *	parameters     : rssiEdValue : read value of baseband RSSI
 *				   : rssiEdSelect : RP_TRUE: rssi select
 *				   :                RP_FALSE:ED select
 *	return value   : IEEE802.15.4 LQI or ED format
 *	description    : convert rssirslt to the value which format is specifiled
 *				   : in IEEE802.15.4.
 *****************************************************************************/
uint8_t RpCalcLqiCallback(uint16_t rssiEdValue, uint8_t rssiEdSelect)
{
	int16_t schg;
	uint8_t val;

	schg = (int16_t)rssiEdValue;
	schg = (int16_t)((schg < 0x0100) ? schg : (schg | 0xfe00));
	if (rssiEdSelect == RP_FALSE)
	{
		if (schg < RP_EDFLOORLVL)
		{
			val = 0x00;
		}
		else if (schg >= RP_EDSATURLVL)
		{
			val = 0xff;
		}
		else // if(schg >= EDFLOORLVL && schg < EDSATURLVL)
		{
			schg = (int16_t)(schg - RP_EDFLOORLVL);
			val = (uint8_t)((int16_t)(schg * 4));
		}
	}
	else // if (rssiSelect == RP_TRUE)
	{
		if	(schg <  RP_RSSIFLOORLVL)
		{
			// floor level 		= RSSIFLOORLVL(dBm)
			val = 0;
		}
		else if (schg >= RP_RSSISATURLVL)
		{
			// saturation level = RSSISATURLVL(dBm)
			val =  255;
		}
		else // if(schg >= RSSIFLOORLVL && schg < RSSISATURLVL)
		{
			schg = (int16_t)(schg - RP_RSSIFLOORLVL);		// 0 - (RSSISATURLVL - RSSIFLOORLVL)
			//			val = (uint8_t)((uint8_t)schg << 2);
			val = (uint8_t) schg;
		}
	}

	return (val);							// min(-98dBm) = 0, max(-34dBm) = 255
}
#endif

/*******************************************************************************
 * Copyright (C) 2018 Renesas Electronics Corporation.
 ******************************************************************************/

